import { Component, OnInit } from '@angular/core';

import { Platform, NavController } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';

@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html',
  styleUrls: ['app.component.scss']
})
export class AppComponent implements OnInit {
  
  public selectedIndex = 0;
  public appPages = [
    {
      title: 'ขายสินค้า',
      url: '/home',
      icon: 'cube'
    },
    // {
    //   title: 'ใช้โปรโมชั้น',
    //   url: '/promotion',
    //   icon: 'star'
    // },
    {
      title: 'ใบเสร็จรับเงิน',
      url: '/receipt',
      icon: 'receipt'
    },
    {
      title: 'จัดการ ข้อมูลสินค้า',
      url: '/goods',
      icon: 'list'
    },
    // {
    //   title: 'การตั้งค่า',
    //   url: '/setting',
    //   icon: 'settings'
    // },
    // {
    //   title: 'ระบบหลังร้าน',
    //   url: '/backend',
    //   icon: 'bar-chart'
    // },
    // {
    //   title: 'App version',
    //   url: '/app',
    //   icon: 'apps'
    // },
    {
      title: 'ติดต่อผู้พัฒนา',
      url: '/help',
      icon: 'help-circle'
    },
    {
      title: 'เปลี่ยนรหัสผ่าน',
      url: '/changepassword',
      icon: 'reload'
    },
    {
      title: 'ออกจากระบบ',
      url: '/logout',
      icon: 'log-out'
    },
  ];
  // public labels = ['Family', 'Friends', 'Notes', 'Work', 'Travel', 'Reminders'];

  constructor(
    private platform: Platform,
    private splashScreen: SplashScreen,
    private statusBar: StatusBar,
    private nav:NavController
  ) {
    this.initializeApp();
  }

  initializeApp() {
    this.platform.ready().then(() => {
      this.statusBar.styleDefault();
      this.splashScreen.hide();
    });
  }
  business_name:any;
  user:any;
  ngOnInit() {

    var Item = JSON.parse(window.localStorage.getItem('Object'));
    if(Item != null){
      this.business_name = Item.business_name;
      this.user = Item.user;
    }

    const path = window.location.pathname.split('folder/')[1];

    
    if (path !== undefined) {
      this.selectedIndex = this.appPages.findIndex(page => page.title.toLowerCase() === path.toLowerCase());
    }
  }
}
