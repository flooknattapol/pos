import { UtilityService } from './../../service/utility/utility.service';
import { CustomerService, CustomerModel } from './../../service/customer/customer.service';
import { Component, OnInit } from '@angular/core';
import { NavController } from '@ionic/angular';
import { ActivatedRoute ,NavigationExtras} from "@angular/router";
import { BarcodeScanner } from '@ionic-native/barcode-scanner/ngx';

@Component({
  selector: 'app-customer',
  templateUrl: './customer.page.html',
  styleUrls: ['./customer.page.scss'],
})
export class CustomerPage {
  PathIMAGE:any;
  CustomerList:CustomerModel;
  Search:string = "";
  TEL:string = "";
  constructor(private barcodeScanner: BarcodeScanner,private nav:NavController,private CustomerService:CustomerService,private apiHelper:UtilityService) {
    this.PathIMAGE = apiHelper.ImagePath;
   }
   maxsizepage:number = 20;

  ionViewWillEnter() {
    this.getList();
  }
  onAddCustomer(){
    this.nav.navigateForward('/addcustomer');
  }
  onDetail(item){
    let navigationExtras: NavigationExtras= {
      queryParams: {
          item: item,
          flag: true
      }
    };
    this.nav.navigateForward('/customerdetail',navigationExtras);
  }
  loadMore(event){
    setTimeout(() => {
      this.maxsizepage += 20;
      this.getList(event);
      // event.target.complete();
    }, 500);
  }
  getList(event?){
    var data = this.CustomerService.getList(this.Search,this.maxsizepage).then((result: any)=> { 
      if(result.status == true){
        this.CustomerList = new CustomerModel;
        this.CustomerList = result.message;
        if(event){
          event.target.complete();
        }
      }else{
        alert(result.message)
      }
    })
  }
  onOpenScanbarcode(){
    var blScaner = false;
    this.barcodeScanner.scan().then(barcodeData => {
      console.log('Barcode data', barcodeData);
      var _UID = barcodeData.text;
      var data = this.CustomerService.add(_UID).then((result: any)=> { 
        if(result.status == true){
          this.getList();
        }else{
          alert(result.message)
        }
      })
     }).catch(err => {
         console.log('Error', err);
     });
  }
  onAddList(){
    if(this.TEL != ""){
      var data = this.CustomerService.add(this.TEL).then((result: any)=> { 
        if(result.status == true){
          this.getList();
        }else{
          alert(result.message)
        }
      })
    }else{
      alert("กรุณาใส่เบอร์โทรศัพท์")
    }
  }
}
