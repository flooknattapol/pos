import { NavController } from '@ionic/angular';
import { DiscountService, DiscountModel } from './../../service/discount/discount.service';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute ,NavigationExtras} from "@angular/router";

@Component({
  selector: 'app-discount',
  templateUrl: './discount.page.html',
  styleUrls: ['./discount.page.scss'],
})
export class DiscountPage {

  ModelList:DiscountModel[];
  CreateModel:DiscountModel;
  maxsizepage:number = 20;
  constructor(private DiscountService:DiscountService
    ,private nav:NavController,private route: ActivatedRoute) { }

  ionViewWillEnter() {
    this.getDiscount();
  }
  loadMore(event){
    setTimeout(() => {
      this.maxsizepage += 20;
      this.getDiscount(event);
      // event.target.complete();
    }, 500);
  }
  getDiscount(event?){
    var data = this.DiscountService.getList(this.maxsizepage).then((result: any)=> { 
      if(result.status == true){
        this.ModelList = new Array;
        this.ModelList = result.message;
        if(event){
          event.target.complete();
        }
      }else{
        alert(result.message)
      }
    })
  }
  addPage(){
    this.CreateModel = new DiscountModel;
    let navigationExtras: NavigationExtras = {
      queryParams: {
          CreateModel:this.CreateModel
      }
    };  
    this.nav.navigateForward('/discountadd',navigationExtras);
  }
  addEditPage(item){
    let navigationExtras: NavigationExtras = {
      queryParams: {
          CreateModel:item,
      }
    };  
    this.nav.navigateForward('/discountadd',navigationExtras);
  }
}
