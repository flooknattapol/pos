import { UserService } from './../../service/user/user.service';
import { NavController } from '@ionic/angular';
import { Component, OnInit } from '@angular/core';
@Component({
  selector: 'app-changepassword',
  templateUrl: './changepassword.page.html',
  styleUrls: ['./changepassword.page.scss'],
})
export class ChangepasswordPage implements OnInit {

  PASSWORD:any = "";
  NEWPASSWORD:any = "";
  CONFIRMPASSWORD:any = "";
  EMAIL:any = "";
  constructor(private nav:NavController,private UserService:UserService) { }

  ngOnInit() {
    var item = JSON.parse(window.localStorage.getItem('Object'));
    console.log(item);
    this.EMAIL = item.user_email;
  }
  submit(){
    if(this.PASSWORD == "" || this.NEWPASSWORD == "" || this.CONFIRMPASSWORD == "" || this.EMAIL ==""){
      alert("กรุณาใส่ข้อมูลให้ครบถ้วน");
    }else if(this.NEWPASSWORD != this.CONFIRMPASSWORD ){
      alert("พาสเวิร์ดไม่ตรงกัน");
    }else{
      var data = this.UserService.changepassword(this.EMAIL,this.PASSWORD,this.NEWPASSWORD).then((result: any)=> { 
        if(result.status == true){
          alert(result.message);
          this.PASSWORD = "";
          this.NEWPASSWORD = "";
          this.CONFIRMPASSWORD = "";
        }else{
          alert(result.message)
        }
      })
    }
  }
}
