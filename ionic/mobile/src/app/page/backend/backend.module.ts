import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { BackendPageRoutingModule } from './backend-routing.module';

import { BackendPage } from './backend.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    BackendPageRoutingModule
  ],
  declarations: [BackendPage]
})
export class BackendPageModule {}
