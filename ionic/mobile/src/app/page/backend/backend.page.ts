import { UtilityService } from './../../service/utility/utility.service';
import { NavController } from '@ionic/angular';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-backend',
  templateUrl: './backend.page.html',
  styleUrls: ['./backend.page.scss'],
})
export class BackendPage implements OnInit {

  constructor(private nav:NavController,private UtilityService:UtilityService) { }

  ngOnInit() {
    
    window.open(this.UtilityService.apiRoot);  
  }
  
}
