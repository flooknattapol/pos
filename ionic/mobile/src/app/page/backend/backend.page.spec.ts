import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { BackendPage } from './backend.page';

describe('BackendPage', () => {
  let component: BackendPage;
  let fixture: ComponentFixture<BackendPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BackendPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(BackendPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
