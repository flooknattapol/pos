import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { PaymentendPage } from './paymentend.page';

describe('PaymentendPage', () => {
  let component: PaymentendPage;
  let fixture: ComponentFixture<PaymentendPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PaymentendPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(PaymentendPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
