import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { PaymentendPage } from './paymentend.page';

const routes: Routes = [
  {
    path: '',
    component: PaymentendPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class PaymentendPageRoutingModule {}
