import { CustomerModel } from './../../service/customer/customer.service';
import { GoodsModel } from './../../service/goods/goods.service';
import { Component, OnInit } from '@angular/core';
import { NavController } from '@ionic/angular';
import { ActivatedRoute ,NavigationExtras} from "@angular/router";

@Component({
  selector: 'app-paymentend',
  templateUrl: './paymentend.page.html',
  styleUrls: ['./paymentend.page.scss'],
})
export class PaymentendPage implements OnInit {
  TotalOrder:number;
  TempGoodsList:GoodsModel;
  change:number;
  customer:CustomerModel;
  ORDERTYPE:any;
  CATEGORYPAYMENT:any;
  order_id:any;
  blcus:boolean;
  constructor(private nav:NavController,private route: ActivatedRoute) { 
    this.route.queryParams.subscribe(params => {
      this.TotalOrder =params['TotalOrder'];
      this.TempGoodsList = params['GoodsList'];
      this.change = params['change'];
      this.customer = params['customer'];
      this.ORDERTYPE = params['ORDERTYPE'];
      this.CATEGORYPAYMENT = params['CATEGORYPAYMENT'];
      this.order_id = params['order_id'];
      if(this.customer.ID > 0){
        this.blcus = true;
      }else{
        this.blcus = false;
      }
    });
  }

  ngOnInit() {
  }
  onHome(){
    this.nav.navigateBack('/home');
  }
}
