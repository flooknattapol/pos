import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { PaymentendPageRoutingModule } from './paymentend-routing.module';

import { PaymentendPage } from './paymentend.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    PaymentendPageRoutingModule
  ],
  declarations: [PaymentendPage]
})
export class PaymentendPageModule {}
