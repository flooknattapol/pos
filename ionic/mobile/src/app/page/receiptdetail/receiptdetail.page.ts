import { TransectionModel, TransectionService, TransectionDetailModel } from './../../service/transection/transection.service';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute ,NavigationExtras} from "@angular/router";
import { AlertController, NavController } from '@ionic/angular';

@Component({
  selector: 'app-receiptdetail',
  templateUrl: './receiptdetail.page.html',
  styleUrls: ['./receiptdetail.page.scss'],
})
export class ReceiptdetailPage implements OnInit {
  TransectionModel:TransectionModel;
  TransectionDetailModel:TransectionDetailModel[];
  countIsActive:number = 0;
  countDetail:number = 0;
  constructor(private route: ActivatedRoute,private TransectionService:TransectionService,
    public alertController: AlertController
    ,private nav:NavController) {
      this.route.queryParams.subscribe(params => {
        this.TransectionModel = params['item'];
        this.getList(this.TransectionModel);

    });
   }
  getList(item){
  var data = this.TransectionService.getDetail(item).then((result: any)=> { 
    var count = 0;
    if(result.status == true){
      this.TransectionDetailModel = new Array;
      this.TransectionDetailModel = result.message;

      this.TransectionDetailModel.forEach(function (entry, index) {
        if(entry.STATUS == 0){
          count++;
        }
      });
      this.countDetail = this.TransectionDetailModel.length;
      this.countIsActive = count;
    }else{
      alert(result.message)
    }
  })
  }
  ngOnInit() {
  }

  async onCancel() {
    const alert = await this.alertController.create({
      header: 'ยกเลิกรายการ',
      message: 'คุณต้องการยกเลิกรายการนี้หรือไม่ ?',
      buttons: [
        {
          text: 'No',
          role: 'No',
          cssClass: 'secondary',
          handler: (blah) => {
            
          }
        }, {
          text: 'Yes',
          handler: () => {
            this.cancelReceipt()
          }
        }
      ]
    });

    await alert.present();
  }
  cancelReceipt(){
   
      var data = this.TransectionService.delete(this.TransectionModel.ID).then((result: any)=> { 
      if(result.status == true){
       
        if(this.TransectionModel.CUSTOMER_ID == undefined){
    
        }else{
          var message =  {
            "order_id":this.TransectionModel.ORDER_NAME,
            "TotalOrder":this.TransectionModel.TotalOrder,
            "emp":this.TransectionModel.SELLER_ID,
            "user_id":this.TransectionModel.userID,
            "customer_name":this.TransectionModel.CUSTOMER_NAME,
            "type_payment":this.TransectionModel.CATEGORYPAYMENT,
            "list":this.TransectionDetailModel
          }
          var data2 = this.TransectionService.sentReceiptCancel(message).then((result2: any)=> { 
            if(result2.status == 200){
            }
           else{
             alert(result2.message);
           }
          })
        }   
        this.nav.pop();
      }else{
        alert(result.message)
      }
    })
  }
  DeleteItemInList(item:any){
    var itemlist = [];
    itemlist.push(item);
    var data = this.TransectionService.deleteDetail(item.ID).then((result: any)=> { 
      if(result.status == true){
        // this.TransectionModel.TotalOrder -= +item.PRICE;
        if(this.TransectionModel.CUSTOMER_ID == undefined){
    
        }else{
          var message =  {
            "order_id":this.TransectionModel.ORDER_NAME,
            "TotalOrder":item.PRICE,
            "emp":this.TransectionModel.SELLER_ID,
            "user_id":this.TransectionModel.userID,
            "customer_name":this.TransectionModel.CUSTOMER_NAME,
            "type_payment":this.TransectionModel.CATEGORYPAYMENT,
            "list":itemlist
          }
          var data2 = this.TransectionService.sentReceiptCancel(message).then((result2: any)=> { 
            if(result2.status == 200){
            }
           else{
             alert(result2.message);
           }
          })
        }   
        this.getList(this.TransectionModel);
        }else{
        alert(result.message)
      }
    })
  }
  async onDeleteItemInList(item:any) {
    const alert = await this.alertController.create({
      header: 'ยกเลิกรายการ',
      message: 'คุณต้องการยกเลิกรายการนี้หรือไม่ ?',
      buttons: [
        {
          text: 'No',
          role: 'No',
          cssClass: 'secondary',
          handler: (blah) => {
            
          }
        }, {
          text: 'Yes',
          handler: () => {
            this.DeleteItemInList(item)
          }
        }
      ]
    });

    await alert.present();
  }
}
