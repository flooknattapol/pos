import { CustomercategoryService, CustomercategoryModel } from './../../service/customercategory/customercategory.service';
import { CustomerService, CustomerModel } from './../../service/customer/customer.service';
import { Component, OnInit } from '@angular/core';
import { NavController } from '@ionic/angular';

@Component({
  selector: 'app-addcustomer',
  templateUrl: './addcustomer.page.html',
  styleUrls: ['./addcustomer.page.scss'],
})
export class AddcustomerPage implements OnInit {
  CreateModel:CustomerModel;  
  CustomercategoryModel:CustomercategoryModel;
  constructor(private nav:NavController,private CustomerService:CustomerService,private CustomercategoryService:CustomercategoryService) { 
    this.CreateModel = new CustomerModel;
    this.CreateModel.NAME ="";
    this.CreateModel.TEL ="";
    this.CreateModel.EMAIL ="";
    this.CreateModel.NOTE ="";
    this.getComboList();
  }
  ngOnInit() {
  }

  onSave(){
    var bVlid = this.validatecheck();
    if(bVlid == true){
      var data = this.CustomerService.add(this.CreateModel).then((result: any)=> { 
        if(result.status == true){
         this.nav.navigateForward('/customer');
        }else{
          alert(result.message)
        }
      })
    }
  }
  validatecheck(){
    var bResult = true;
    if(this.CreateModel.EMAIL == ""){
			bResult = false;
    }
    if(this.CreateModel.TEL == ""){
			bResult = false;
    }
    if(this.CreateModel.NAME == ""){
			bResult = false;
    }
    if(this.CreateModel.CATEGORY == ""){
			bResult = false;
    }
    return bResult;
  }
  getComboList(){
    var data = this.CustomercategoryService.getComboList().then((result: any)=> { 
      if(result.status == true){
        this.CustomercategoryModel = new CustomercategoryModel;
        this.CustomercategoryModel = result.message;
      }else{
        alert(result.message)
      }
    })
  }
}
