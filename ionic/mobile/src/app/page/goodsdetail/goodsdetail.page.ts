import { UtilityService } from './../../service/utility/utility.service';
import { GoodsModel } from './../../service/goods/goods.service';
import { NavController } from '@ionic/angular';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute ,NavigationExtras} from "@angular/router";

@Component({
  selector: 'app-goodsdetail',
  templateUrl: './goodsdetail.page.html',
  styleUrls: ['./goodsdetail.page.scss'],
})
export class GoodsdetailPage implements OnInit {
  CreateModel:GoodsModel;
  PathIMAGE:any;
  constructor(private nav:NavController,private route: ActivatedRoute,private UtilityService:UtilityService) { 
    this.route.queryParams.subscribe(params => { 
      this.CreateModel = params['CreateModel'];
    });
    this.PathIMAGE = this.UtilityService.ImagePath;
  }

  ngOnInit() {
  }
  onAddPage(){
    let navigationExtras: NavigationExtras = {
      queryParams: {
          CreateModel: this.CreateModel,
      }
    };    
    this.nav.navigateForward('/goodsadd',navigationExtras);
  }
}
