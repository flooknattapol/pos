import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { GoodsaddPage } from './goodsadd.page';

describe('GoodsaddPage', () => {
  let component: GoodsaddPage;
  let fixture: ComponentFixture<GoodsaddPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GoodsaddPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(GoodsaddPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
