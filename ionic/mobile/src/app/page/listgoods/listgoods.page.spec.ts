import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { ListgoodsPage } from './listgoods.page';

describe('ListgoodsPage', () => {
  let component: ListgoodsPage;
  let fixture: ComponentFixture<ListgoodsPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ListgoodsPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(ListgoodsPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
