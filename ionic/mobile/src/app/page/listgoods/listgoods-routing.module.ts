import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ListgoodsPage } from './listgoods.page';

const routes: Routes = [
  {
    path: '',
    component: ListgoodsPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ListgoodsPageRoutingModule {}
