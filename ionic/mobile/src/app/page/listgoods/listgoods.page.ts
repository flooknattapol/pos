import { NavController } from '@ionic/angular';
import { UtilityService } from './../../service/utility/utility.service';
import { GoodsService, GoodsModel, GoodsComboModel } from './../../service/goods/goods.service';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute ,NavigationExtras} from "@angular/router";

@Component({
  selector: 'app-listgoods',
  templateUrl: './listgoods.page.html',
  styleUrls: ['./listgoods.page.scss'],
})
export class ListgoodsPage {
  public ModelList:GoodsModel[];
  public ModelSeach:GoodsModel[];
  GoodsComboList:GoodsComboModel;
  PathIMAGE:any;
  constructor(private GoodsService:GoodsService,private UtilityService:UtilityService,
    private nav:NavController,private route: ActivatedRoute) { 
    this.PathIMAGE = this.UtilityService.ImagePath;
  }
  maxsizepage:number = 20;
  ionViewWillEnter() {
    this.getGoods();
    this.getGoodscategory();
  }
  loadMore(event){
    setTimeout(() => {
      this.maxsizepage += 20;
      this.getGoods(event);
      // event.target.complete();
    }, 500);
  }
  getGoods(event?){
    var data = this.GoodsService.getList(this.maxsizepage).then((result: any)=> { 
      if(result.status == true){
        this.ModelList = new Array;
        this.ModelSeach = new Array;
        this.ModelList = result.message;
        this.ModelSeach = result.message;
        if(event){
          event.target.complete();
        }
      }else{
        alert(result.message)
      }
    })
  }
  getGoodscategory(){
    var data = this.GoodsService.getComboList().then((result: any)=> { 
      if(result.status == true){
        this.GoodsComboList = new GoodsComboModel;
        this.GoodsComboList = result.message;
      }else{
        alert(result.message)
      }
    })
  }
  onFilterList(event){
      var GoodsFilter = new Array;
      this.ModelSeach.forEach(function (entry, index) {
        if(entry.CATEGORY_ID == event.detail.value){
          GoodsFilter.push(entry)
        }
      });
      this.ModelList = [];
      this.ModelList = GoodsFilter;
    
  }
  onAddPage(){
    this.ModelList = new Array;
    let navigationExtras: NavigationExtras = {
      queryParams: {
          CreateModel: this.ModelList,
      }
    };    
    this.nav.navigateForward('/goodsadd',navigationExtras);
  }
  onDetailPage(item:GoodsModel){
    let navigationExtras: NavigationExtras = {
      queryParams: {
          CreateModel: item,
      }
    };    
    this.nav.navigateForward('/goodsdetail',navigationExtras);
  }
}
