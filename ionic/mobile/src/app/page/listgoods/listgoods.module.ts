import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { ListgoodsPageRoutingModule } from './listgoods-routing.module';

import { ListgoodsPage } from './listgoods.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ListgoodsPageRoutingModule
  ],
  declarations: [ListgoodsPage]
})
export class ListgoodsPageModule {}
