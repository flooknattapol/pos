import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { ReceiptcancelPageRoutingModule } from './receiptcancel-routing.module';

import { ReceiptcancelPage } from './receiptcancel.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ReceiptcancelPageRoutingModule
  ],
  declarations: [ReceiptcancelPage]
})
export class ReceiptcancelPageModule {}
