import { NavController } from '@ionic/angular';
import { TransectionService, TransectionModel } from './../../service/transection/transection.service';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute ,NavigationExtras} from "@angular/router";

@Component({
  selector: 'app-receipt',
  templateUrl: './receipt.page.html',
  styleUrls: ['./receipt.page.scss'],
})
export class ReceiptPage{

  TransectionModel:TransectionModel;
  Search:string ="";
  constructor(private TransectionService:TransectionService,private nav:NavController) { 
    
  }
  maxsizepage:number = 20;
  ionViewWillEnter() {
    this.getList();
  }
  loadMore(event){
    setTimeout(() => {
      this.maxsizepage += 20;
      this.getList(event);
      // event.target.complete();
    }, 500);
  }
  getList(event?){
    var data = this.TransectionService.getList(this.Search,this.maxsizepage).then((result: any)=> { 
      if(result.status == true){
        this.TransectionModel = new TransectionModel;
        this.TransectionModel = result.message;
        if(event){
          event.target.complete();
        }
      }else{
        alert(result.message)
      }
    })
  }
  onDetail(item){
    let navigationExtras: NavigationExtras = {
      queryParams: {
          item:item,
      }
    };
    this.nav.navigateForward('/receiptdetail',navigationExtras);
  }
}
