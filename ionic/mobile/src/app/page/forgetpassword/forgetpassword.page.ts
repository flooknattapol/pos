import { UserService } from './../../service/user/user.service';
import { NavController } from '@ionic/angular';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-forgetpassword',
  templateUrl: './forgetpassword.page.html',
  styleUrls: ['./forgetpassword.page.scss'],
})
export class ForgetpasswordPage implements OnInit {

  EMAIL:any;
  constructor(private nav:NavController,private UserService:UserService) { }

  ngOnInit() {

  }
  submit(){
    if(this.EMAIL !=""){
      var data = this.UserService.Forgetpass(this.EMAIL).then((result: any)=> { 
        if(result.status == true){
          alert("ลืมรหัสผ่านเสร็จสิ้น");
          this.nav.pop();
        }else{
          alert(result.message)
        }
      })
    }else{
      alert("กรุณาใส่ Email ");
    }
    
  }
}
