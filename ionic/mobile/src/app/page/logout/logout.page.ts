import { NavController } from '@ionic/angular';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-logout',
  templateUrl: './logout.page.html',
  styleUrls: ['./logout.page.scss'],
})
export class LogoutPage implements OnInit {

  constructor(private nav:NavController) { }

  ngOnInit() {
    window.localStorage['Object'] = JSON.stringify([]);
    window.localStorage['TempGoodsList'] = JSON.stringify([]);
    window.localStorage['TempDiscount'] = JSON.stringify([]);
    this.nav.navigateRoot('/login');
  }

}
