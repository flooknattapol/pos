import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { GoodscategoryPageRoutingModule } from './goodscategory-routing.module';

import { GoodscategoryPage } from './goodscategory.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    GoodscategoryPageRoutingModule
  ],
  declarations: [GoodscategoryPage]
})
export class GoodscategoryPageModule {}
