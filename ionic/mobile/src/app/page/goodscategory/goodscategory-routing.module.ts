import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { GoodscategoryPage } from './goodscategory.page';

const routes: Routes = [
  {
    path: '',
    component: GoodscategoryPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class GoodscategoryPageRoutingModule {}
