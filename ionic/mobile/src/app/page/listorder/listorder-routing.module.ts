import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ListorderPage } from './listorder.page';

const routes: Routes = [
  {
    path: '',
    component: ListorderPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ListorderPageRoutingModule {}
