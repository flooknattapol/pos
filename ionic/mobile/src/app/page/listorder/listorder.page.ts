import { NavController } from '@ionic/angular';
import { TransectionService, TempOrderModel } from './../../service/transection/transection.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-listorder',
  templateUrl: './listorder.page.html',
  styleUrls: ['./listorder.page.scss'],
})
export class ListorderPage implements OnInit {

  
  TempOrderModel:TempOrderModel[];
  constructor(private TransectionService : TransectionService,private nav:NavController) { }

  ngOnInit() {
    this.getList();
  }
  getList(){
    var data = this.TransectionService.getListTempOrder("").then((result: any)=> { 
      if(result.status == true){
        this.TempOrderModel = new Array;
        this.TempOrderModel = result.message;
      }else{
        alert(result.message)
      }
    })
  }
  addOrder(item){
    var data = this.TransectionService.getTempOrder(item.ID).then((result: any)=> { 
      if(result.status == true){
        window.localStorage['TempGoodsList'] = JSON.stringify(result.tempgoods);
        window.localStorage['TempDiscount'] = JSON.stringify(result.tempdiscount);
        this.nav.navigateRoot('/home');
      }else{
        alert(result.message)
      }
    })

  }
  deletetemporder(item){
    var data = this.TransectionService.deleteTempOrder(item.ID).then((result: any)=> { 
      if(result.status == true){
        this.getList();
      }else{
        alert(result.message)
      }
    })

  }
}
