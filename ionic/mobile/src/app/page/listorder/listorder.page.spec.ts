import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { ListorderPage } from './listorder.page';

describe('ListorderPage', () => {
  let component: ListorderPage;
  let fixture: ComponentFixture<ListorderPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ListorderPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(ListorderPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
