import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { ListorderPageRoutingModule } from './listorder-routing.module';

import { ListorderPage } from './listorder.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ListorderPageRoutingModule
  ],
  declarations: [ListorderPage]
})
export class ListorderPageModule {}
