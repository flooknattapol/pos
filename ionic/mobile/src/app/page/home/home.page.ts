import { DiscountService, DiscountModel } from './../../service/discount/discount.service';
import { CustomerModel } from './../../service/customer/customer.service';
import { UtilityService } from './../../service/utility/utility.service';
import { GoodsService, GoodsModel, GoodsComboModel } from './../../service/goods/goods.service';
import { Component, OnInit } from '@angular/core';
import { NavController,Platform,ToastController  } from '@ionic/angular';
import { ActivatedRoute ,NavigationExtras} from "@angular/router";
import { BarcodeScanner } from '@ionic-native/barcode-scanner/ngx';

@Component({
  selector: 'app-home',
  templateUrl: './home.page.html',
  styleUrls: ['./home.page.scss'],
})
export class HomePage {

  GoodsList:GoodsModel[];
  GoodsFilter:GoodsModel[];
  GoodsComboList:GoodsComboModel;
  optionFilter:any;
  public TempGoodsList:GoodsModel[];
  PathIMAGE:any;
  TotalOrder:number;
  num:number = 0;
  tempAmountDiscount:number;
  customer:any;
  CustomerModel:any = "";
  DiscountList:DiscountModel[];
  TempDiscount:DiscountModel[];
  blUseDiscount:boolean = false;
  UID:any;
  vListGoods:boolean = true;
  vListDiscount:boolean = true;
  widthSize:boolean = true;
  TotalCost:number;
  maxsizepage:number = 20;

  constructor(public toastController: ToastController,private barcodeScanner: BarcodeScanner,private nav:NavController,private GoodsService:GoodsService,
    private apiHelper:UtilityService,private route: ActivatedRoute,private platform:Platform
    ,private DiscountService:DiscountService) { 
    
    this.route.queryParams.subscribe(params => {
      this.CustomerModel = new CustomerModel;
      this.CustomerModel = params['item'];
    });
    
    this.PathIMAGE = apiHelper.ImagePath;
    this.TempGoodsList = new  Array();
    this.TempDiscount = new Array();
    this.getGoods();
    this.getDiscount();
    this.getGoodscategory();
  }

  ionViewWillEnter() {
    this.optionFilter = 'all';
    this.blUseDiscount= false;
    this.TotalOrder = 0;
    this.TotalCost = 0
    this.tempAmountDiscount = 0;
    this.num = 0;
    if(JSON.parse( window.localStorage.getItem('TempGoodsList')).length > 0){
      this.TempGoodsList = JSON.parse( window.localStorage.getItem('TempGoodsList'));
      this.num = this.TempGoodsList.length;
    }else{
      this.TempGoodsList = [];
    }
    if(JSON.parse(window.localStorage.getItem('TempDiscount')).length > 0){
      this.TempDiscount = JSON.parse( window.localStorage.getItem('TempDiscount'));
      this.blUseDiscount = true;
      this.num++;
    }else{
      this.TempDiscount = [];
    }
    this.sumdiscount();
    console.log(this.GoodsList);
  
  }
  
  onOpenOrder(){
    this.nav.navigateForward('/listorder');
  }
  onPayment(){
    if(this.CustomerModel != undefined){
      this.customer = this.CustomerModel;
    }else{
      this.customer = "";
    }
    let navigationExtras: NavigationExtras = {
      queryParams: {
          TotalOrder: this.TotalOrder,
          TotalCost:this.TotalCost,
          GoodsList:this.TempGoodsList,
          customer:this.customer,
          discount:this.tempAmountDiscount,
          DiscountList:this.TempDiscount,
          Goodsitem:this.GoodsList,
          Discountitem:this.DiscountList
      }
  };
    if(this.TotalOrder < 0){
      alert("คุณใช้ส่วนลดไม่ถูกต้อง");
    }else if(this.TempGoodsList.length > 0){
      this.nav.navigateForward('/payment',navigationExtras);
    }else{
      alert("ไม่มีรายการ order");
    }
  }
  onAddCustomer(){
    this.nav.navigateForward('customer');
  }
  onCustomerDetail(){
    let navigationExtras: NavigationExtras = {
      queryParams: {
          item:this.CustomerModel,
          flag:false
      }
    };
    this.nav.navigateForward('customerdetail',navigationExtras);

  }
  loadMore(event){
    setTimeout(() => {
      this.maxsizepage += 20;
      this.getGoods(event);
      this.getDiscount(event);
      event.target.complete();
    }, 500);
  }
  getGoods(event?){
    var objectRes = [];
    var data = this.GoodsService.getList(this.maxsizepage).then((result: any)=> { 
      if(result.status == true){

        this.GoodsList = new Array;
        this.GoodsList = result.message;
        this.GoodsFilter = result.message;
        var _goodslist = this.GoodsList;
        this.TempGoodsList.forEach(function (entry1, index1) {
          _goodslist.forEach(function (entry, index) {
            if (entry1.ID === entry.ID && entry.STOCK_IsActive == 1){
              entry.STOCK--;
              console.log(entry)
            }
          });
        });
        console.log(_goodslist);
        this.GoodsList =[];
        this.GoodsList = _goodslist;
      }else{
        alert(result.message)
      }
    })
  }
  getGoodscategory(){
    var data = this.GoodsService.getComboList().then((result: any)=> { 
      if(result.status == true){
        this.GoodsComboList = new GoodsComboModel;
        this.GoodsComboList = result.message;
      }else{
        alert(result.message)
      }
    })
  }
  getDiscount(event?){
    var data = this.DiscountService.getList(this.maxsizepage).then((result: any)=> { 
      if(result.status == true){
        this.DiscountList = new Array;
        this.DiscountList = result.message;
      }else{
        alert(result.message)
      }
    })
  }
  onOpenScanbarcode(){
    var blScaner = false;
    this.barcodeScanner.scan().then(barcodeData => {
      console.log('Barcode data', barcodeData);
      var _UID = barcodeData.text;
      var _blUID = _UID.substring(0,2);
      var itemdata;
      var ArrayData = [];
      if(_blUID == "88"){
        ArrayData = JSON.parse( window.localStorage.getItem('TempGoodsList'));
        this.GoodsList.forEach(function (entry, index) {
          if(_UID === entry.UID)
          {
            blScaner = true;
            window.localStorage.removeItem['TempGoodsList'];
            ArrayData.push(entry);
            itemdata = entry;
          }
        });
        if(blScaner == true){
          if(itemdata.STATUS == 0){
            alert("สินค้าไม่พร้อมขาย")
          }else if(itemdata.STOCK == 0){
            alert("สินค้าหมด")
          }else{
          this.GoodsList.forEach(function (entry, index) {
            if (itemdata.ID === entry.ID  && entry.STOCK_IsActive == 1){
              entry.STOCK--;
            }
          });
          window.localStorage['TempGoodsList'] = JSON.stringify(ArrayData);
          this.TempGoodsList = ArrayData;
          this.num = this.TempGoodsList.length;
          this.sumdiscount();
          }
        }else{
          alert("ไม่พบสินค้า");
        }
      }else if(_blUID == "11"){
        if(this.blUseDiscount == true){
          alert("คุณใช้ส่วนลดเรียบร้อยแล้ว");
        }else{
          ArrayData = JSON.parse( window.localStorage.getItem('TempDiscount'));
          this.DiscountList.forEach(function (entry, index) {
            if(_UID === entry.UID)
            {
              blScaner = true;
              window.localStorage.removeItem['TempDiscount'];
              ArrayData.push(entry);
            }
          });
          if(blScaner == true){
            this.blUseDiscount = true;
            this.TempDiscount = ArrayData;
            window.localStorage['TempDiscount'] = JSON.stringify(ArrayData);
            this.num ++;
            this.sumdiscount();
            
          }else{
            alert("ไม่พบส่วนลด");
          }
        } 
        
      }else if(_blUID == "22"){
        var data = this.DiscountService.getListRelation(_UID).then((result: any)=> { 
          if(result.status == true){
            if(this.blUseDiscount == true){
              alert("คุณใช้ส่วนลดเรียบร้อยแล้ว");
            }else{
              ArrayData = JSON.parse( window.localStorage.getItem('TempDiscount'));
              ArrayData.push(result.message);
              this.blUseDiscount = true;
              this.TempDiscount = ArrayData;
              window.localStorage['TempDiscount'] = JSON.stringify(ArrayData);
              this.num ++;
              this.sumdiscount();
            } 
          }else{
            alert(result.message)
          }
        })    
      }else{
          alert("barcode ไม่ถูกต้อง");
      }
     }).catch(err => {
         console.log('Error', err);
     });
  }
  async onPushOrderByEnter(){
    var _UID = this.UID;
    var _blUID = _UID.substring(0,2);
    var ArrayData = [];
    var itemdata;
    var blScaner;
    if(_blUID == 88){

      ArrayData = JSON.parse( window.localStorage.getItem('TempGoodsList'));
      this.GoodsList.forEach(function (entry, index) {
        if(_UID === entry.UID)
        {
          blScaner = true;
          window.localStorage.removeItem['TempGoodsList'];
          ArrayData.push(entry);
          itemdata = entry;
        }
      });
      if(blScaner == true){
        console.log(itemdata);
        if(itemdata.STATUS == 0){
          alert("สินค้าไม่พร้อมขาย")
        }else if(itemdata.STOCK == 0){
          alert("สินค้าหมด")
        }else{
          this.GoodsList.forEach(function (entry, index) {
            if (itemdata.ID === entry.ID  && entry.STOCK_IsActive == 1){
              entry.STOCK--;
            }
          });
          window.localStorage['TempGoodsList'] = JSON.stringify(ArrayData);
          this.TempGoodsList = ArrayData;
          this.num = this.TempGoodsList.length;
          this.sumdiscount();
          const toast = await this.toastController.create({
            message: 'เพิ่มสินค้าเรียบร้อยแล้ว',
            duration: 2000
          });
          toast.present();
        }
      }else{
        alert("ไม่พบสินค้า");
      }
    }else if(_blUID == 11){
      if(this.blUseDiscount == true){
        alert("คุณใช้ส่วนลดเรียบร้อยแล้ว");
      }else{
        ArrayData = JSON.parse( window.localStorage.getItem('TempDiscount'));
        this.DiscountList.forEach(function (entry, index) {
          if(_UID === entry.UID)
          {
            blScaner = true;
            window.localStorage.removeItem['TempDiscount'];
            ArrayData.push(entry);
          }
        });
        if(blScaner == true){
          this.blUseDiscount = true;
          this.TempDiscount = ArrayData;
          window.localStorage['TempDiscount'] = JSON.stringify(ArrayData);
          this.num ++;
          this.sumdiscount();
          const toast = await this.toastController.create({
            message: 'เพิ่มส่วนลดเรียบร้อยแล้ว',
            duration: 2000
          });
          toast.present();
        }else{
          alert("ไม่พบส่วนลด");
        }
      } 
    }else if(_blUID == "22"){
      var data = this.DiscountService.getListRelation(_UID).then((result: any)=> { 
        if(result.status == true){
          if(this.blUseDiscount == true){
            alert("คุณใช้ส่วนลดเรียบร้อยแล้ว");
          }else{
            ArrayData = JSON.parse( window.localStorage.getItem('TempDiscount'));
            ArrayData.push(result.message);
            this.blUseDiscount = true;
            this.TempDiscount = ArrayData;
            window.localStorage['TempDiscount'] = JSON.stringify(ArrayData);
            this.num ++;
            this.sumdiscount();
          } 
        }else{
          alert(result.message)
        }
      })    
    }else{
        alert("barcode ไม่ถูกต้อง");
    }
   
  }

  async onPushOrder(item:any){
    // this.TotalOrder += +item.PRICE;
    
    if(item.STATUS == 0){
      alert("สินค้าไม่พร้อมขาย");
    }else if(item.STOCK == 0  && item.STOCK_IsActive == 1){
      alert("สินค้าหมด");
    }else{
      if(item.STOCK_IsActive == 1){
        this.GoodsList.forEach(function (entry, index) {
          if (item.ID === entry.ID){
            entry.STOCK--;
          }
        });
      }
      
      window.localStorage.removeItem['TempGoodsList'];
      this.TempGoodsList.unshift(item);
      window.localStorage['TempGoodsList'] = JSON.stringify(this.TempGoodsList);
      this.sumdiscount();
      this.num += 1;
      const toast = await this.toastController.create({
        message: 'เพิ่มสินค้า' + item.NAME +'เรียบร้อยแล้ว',
        duration: 2000
      });
      toast.present();
    }
   
  }
  async onPushDiscount(item:any){

    if(this.blUseDiscount == true){
      alert("คุณใช้ส่วนลดเรียบร้อยแล้ว");
    }else{
      this.blUseDiscount = true;
      window.localStorage.removeItem['TempDiscount'];
      this.TempDiscount.push(item);
      window.localStorage['TempDiscount'] = JSON.stringify(this.TempDiscount);
      this.sumdiscount();
      this.num += 1;
      const toast = await this.toastController.create({
        message: 'เพิ่มส่วนลด' + item.NAME +'เรียบร้อยแล้ว',
        duration: 2000
      });
      toast.present();
    }
 
  }
  
  onDeleteItemInList(item:any){
    var _index;
    var price;
    if(item.STOCK_IsActive == 1){
      this.GoodsList.forEach(function (entry, index) {
        if (item.ID === entry.ID){
          entry.STOCK++;
        }
      });
    }
    
    this.TempGoodsList.forEach(function (entry, index) {
      if (item.ID === entry.ID){
         price = +item.PRICE;
        _index = index;
      }
    });
    window.localStorage.removeItem['TempGoodsList'];
    this.TempGoodsList.splice(_index,1);
    window.localStorage['TempGoodsList'] = JSON.stringify(this.TempGoodsList);
    this.sumdiscount();
    this.num -= 1;
    // this.TotalOrder -= price;
  }
  onDeleteDiscountInList(item:any){
    var _index;
    var price;
    
    if( item.BAHTORPERCENT == 0){
      this.TotalOrder += +this.tempAmountDiscount;
    }else{
      this.TotalOrder += this.tempAmountDiscount;
    }
    this.tempAmountDiscount = 0;
    this.TempDiscount.forEach(function (entry, index) {
      if (item.ID === entry.ID){
         price = +item.PRICE;
        _index = index;
      }
    });
    window.localStorage.removeItem['TempDiscount'];
    this.TempDiscount.splice(_index,1);
    window.localStorage['TempDiscount'] = JSON.stringify(this.TempDiscount);
    this.sumdiscount();
    this.num -= 1;
    this.blUseDiscount = false;
    // this.TotalOrder -= price;
  }
  sumdiscount(){
    var price = 0;
    var cost = 0;
    this.TempGoodsList.forEach(function (entry, index) {
      price = price + Number(entry.PRICE);
      cost = cost + Number(entry.COST);
    });
    this.TotalOrder = price;
    this.TotalCost = cost;
    if(this.TempDiscount.length > 0){
      if( this.TempDiscount[0].BAHTORPERCENT == 0){
        this.TotalOrder -= this.TempDiscount[0].DISCOUNT;
        this.tempAmountDiscount = this.TempDiscount[0].DISCOUNT;
      }else{
        this.tempAmountDiscount = (this.TotalOrder * this.TempDiscount[0].DISCOUNT)/100;;
        this.TotalOrder = this.TotalOrder - this.tempAmountDiscount
      }
    }
    
  }
  onFilterList(event){
    if(event.detail.value == "all"){
      this.vListDiscount = true;
      this.vListGoods = true;
      this.getGoods();
    }else if(event.detail.value == "discount"){
      this.vListDiscount = true;
      this.vListGoods = false;
    }else if(event.detail.value == "goods"){
      this.vListDiscount = false;
      this.vListGoods = true;
      this.getGoods();
    }else{
      var GoodsFilter = new Array;
      this.GoodsFilter.forEach(function (entry, index) {
        if(entry.CATEGORY_ID == event.detail.value){
          GoodsFilter.push(entry)
        }
      });
      this.GoodsList = [];
      this.GoodsList = GoodsFilter;
    }
  }
  onSaveOrder(){
    let navigationExtras: NavigationExtras= {
      queryParams: {
        TempGoodsList:this.TempGoodsList,
        TempDiscount:this.TempDiscount
      }
    };
    this.nav.navigateForward('/createorder',navigationExtras);
  }
}
