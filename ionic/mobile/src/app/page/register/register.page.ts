import { NavController } from '@ionic/angular';
import { BusinessService, BusinessModel, detailModel, comboModel } from './../../service/business/business.service';
import { Component, OnInit } from '@angular/core';
import { FileTransfer, FileUploadOptions, FileTransferObject } from '@ionic-native/file-transfer/ngx';
import { Camera,CameraOptions } from '@ionic-native/camera/ngx';
import { UtilityService } from './../../service/utility/utility.service';

@Component({
  selector: 'app-register',
  templateUrl: './register.page.html',
  styleUrls: ['./register.page.scss'],
})
export class RegisterPage implements OnInit {
  CreateModel:BusinessModel;
  DetailModel:detailModel;
  base64img:string='';
  PathRoot:string="";
  combotype:comboModel[];
  combocategory:comboModel[];
  constructor(private camera: Camera ,private UtilityService:UtilityService
    ,private transfer: FileTransfer,private BusinessService:BusinessService,private nav:NavController) {
    this.CreateModel = new BusinessModel;
    this.CreateModel.CODE = "";
    this.DetailModel = new detailModel;
    this.PathRoot = this.UtilityService.apiRoot;

   }

  ngOnInit() {
    this.getComboCategory();
    this.getComboType();
  }
  upload() {
    const fileTransfer: FileTransferObject = this.transfer.create();
    var dateobj = Date.now();
    var datestr = dateobj.toString(); 
    var filename = 'logo'+datestr+'.jpg'
    let options: FileUploadOptions = {
      fileKey: "photo",
      fileName: filename,
      chunkedMode: false,
      mimeType: "image/jpeg",
      headers: {}
    }
    if(this.base64img != ""){
      fileTransfer.upload(this.base64img,this.PathRoot + '/imageUploadTransferbusiness.php', options).then(data => {
        this.ngAdd(filename);
      }, error => {
        alert("error");
        alert("error" + JSON.stringify(error));
      });
    }else{ 
      alert("กรุณาเลือกรูปภาพ");
    }
  }
  imageCapturedGallery(){
    const options:CameraOptions={
      quality:70,
      destinationType:this.camera.DestinationType.DATA_URL,
      sourceType:this.camera.PictureSourceType.PHOTOLIBRARY,
      saveToPhotoAlbum:false,
      correctOrientation:true
    }
    this.camera.getPicture(options).then((ImageData=>{
       this.base64img="data:image/jpeg;base64,"+ImageData;
    }),error=>{
      console.log(error);
    })
  }
  ngAdd(filename){
    this.DetailModel.NAME = "สำนักงานใหญ่";
    this.DetailModel.TEL = this.CreateModel.TEL;
    this.CreateModel.IMAGE = filename;
    this.CreateModel.detail = new Array;
    this.CreateModel.detail.push(this.DetailModel);
    var bValid = true;
    if(this.CreateModel.NAME == "" || this.CreateModel.NAME == undefined){
      bValid = false;
    }
    if(this.CreateModel.EMAIL == "" || this.CreateModel.EMAIL == undefined){
      bValid = false;
    }
    if(this.CreateModel.CATEGORY_ID == "" || this.CreateModel.CATEGORY_ID == undefined){
      bValid = false;
    }
    if(this.CreateModel.TYPE_ID == "" || this.CreateModel.TYPE_ID == undefined){
      bValid = false;
    }
    if(this.CreateModel.TEL == "" || this.CreateModel.TEL == undefined){
      bValid = false;
    }
    if(this.CreateModel.CONTACT == "" || this.CreateModel.CONTACT == undefined){
      bValid = false;
    }

    if(this.DetailModel.ADDRESS == "" || this.DetailModel.ADDRESS == undefined){
      bValid = false;
    }
    if(this.CreateModel.PASSWORD == "" || this.CreateModel.PASSWORD == undefined){
      bValid = false;
    }
    if(this.CreateModel.PASSWORD2 == "" || this.CreateModel.PASSWORD2 == undefined){
      bValid = false;
    }

    if(bValid == true){
      if(this.CreateModel.PASSWORD == this.CreateModel.PASSWORD2){
        var data = this.BusinessService.save(this.CreateModel).then((result: any)=> { 
          if(result.status == true){
            alert("สมัครเสร็จสิ้น");
            this.nav.pop();
          }else{
            alert(result.message)
          }
        })
      }else{
        alert("พาสเวิร์ดไม่ตรงกัน");
      }
    }else{
      alert("กรุณาใส่ข้อมูลให้ครบถ้วน")
    }
  }
  getComboCategory(){
    var data = this.BusinessService.getCombeCategory().then((result: any)=> { 
      console.log(result)
      if(result.status == true){
        this.combocategory = new Array;
        this.combocategory = result.message
      }else{
        alert(result.message)
      }
    })
  }
  getComboType(){
    var data = this.BusinessService.getCombeType().then((result: any)=> { 
      console.log(result)
      if(result.status == true){
        this.combotype = new Array;
        this.combotype = result.message
      }else{
        alert(result.message)
      }
    })
  }
}
