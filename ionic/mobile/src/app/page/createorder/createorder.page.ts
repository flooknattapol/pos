import { TempOrderModel, TempOrderDetailModel, TempOrderDiscountModel, TransectionService } from './../../service/transection/transection.service';
import { GoodsModel } from './../../service/goods/goods.service';
import { Component, OnInit } from '@angular/core';
import { NavController } from '@ionic/angular';
import { ActivatedRoute ,NavigationExtras} from "@angular/router";
import { UtilityService } from './../../service/utility/utility.service';
import { DiscountModel } from 'src/app/service/discount/discount.service';

@Component({
  selector: 'app-createorder',
  templateUrl: './createorder.page.html',
  styleUrls: ['./createorder.page.scss'],
})
export class CreateorderPage implements OnInit {

  GoodsList:GoodsModel[];
  DiscountList:DiscountModel[];
  ORDER_NAME:string = "";
  TempOrderModel:TempOrderModel;
  TempOrderDetailModel:TempOrderDetailModel[];
  TempOrderDiscountModel:TempOrderDiscountModel[];
  constructor(private nav:NavController,private route: ActivatedRoute,
    private apiHelper:UtilityService,private TransectionService:TransectionService) { 
    this.route.queryParams.subscribe(params => {
      this.GoodsList = params['TempGoodsList'];
      this.DiscountList = params['TempDiscount'];
    });
  }

  ngOnInit() {
  }
  onSave(){
    if(this.ORDER_NAME != ""){
      var Item = JSON.parse(window.localStorage.getItem('Object'))
      this.TempOrderModel = new TempOrderModel();
      this.TempOrderModel.ORDER_NAME = this.ORDER_NAME;
      this.TempOrderModel.SELLER_ID = Item.id;
      this.TempOrderDiscountModel = new Array;
      this.TempOrderDetailModel = new Array;
  
      if(this.GoodsList.length > 0){
        var TempOrderDetailModel = new Array;
        this.GoodsList.forEach(function (entry, index) {
          var Object = {
            ID:0,
            GOODS_ID:entry.ID
          }
          TempOrderDetailModel.push(Object);
        });
      }
      
      if(this.DiscountList.length > 0){
        var TempOrderDiscountModel = new Array;
  
        this.DiscountList.forEach(function (entry, index) {
          var Object = {
            ID:0,
            DISCOUNT_ID:entry.ID
          }
          TempOrderDiscountModel.push(Object);
        });
      }
      this.TempOrderModel.detail = TempOrderDetailModel;
      this.TempOrderModel.discount = TempOrderDiscountModel;
      var data = this.TransectionService.addtemporder(this.TempOrderModel).then((result: any)=> { 
        if(result.status == true){
            window.localStorage['TempGoodsList'] = JSON.stringify([]);
            window.localStorage['TempDiscount'] = JSON.stringify([]);
            this.nav.navigateRoot('/home');
        }else{
          alert(result.message)
        }
      })
    }else{
      alert("กรุณาใส่ order name");
    }
    
  }
}
