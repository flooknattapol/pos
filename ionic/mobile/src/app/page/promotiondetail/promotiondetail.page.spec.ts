import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { PromotiondetailPage } from './promotiondetail.page';

describe('PromotiondetailPage', () => {
  let component: PromotiondetailPage;
  let fixture: ComponentFixture<PromotiondetailPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PromotiondetailPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(PromotiondetailPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
