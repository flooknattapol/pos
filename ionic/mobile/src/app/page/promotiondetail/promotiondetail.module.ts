import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { PromotiondetailPageRoutingModule } from './promotiondetail-routing.module';

import { PromotiondetailPage } from './promotiondetail.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    PromotiondetailPageRoutingModule
  ],
  declarations: [PromotiondetailPage]
})
export class PromotiondetailPageModule {}
