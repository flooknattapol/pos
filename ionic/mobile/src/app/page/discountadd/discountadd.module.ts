import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { DiscountaddPageRoutingModule } from './discountadd-routing.module';

import { DiscountaddPage } from './discountadd.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    DiscountaddPageRoutingModule
  ],
  declarations: [DiscountaddPage]
})
export class DiscountaddPageModule {}
