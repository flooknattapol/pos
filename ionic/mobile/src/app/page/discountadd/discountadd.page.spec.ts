import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { DiscountaddPage } from './discountadd.page';

describe('DiscountaddPage', () => {
  let component: DiscountaddPage;
  let fixture: ComponentFixture<DiscountaddPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DiscountaddPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(DiscountaddPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
