import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { GoodscategorydetailPage } from './goodscategorydetail.page';

describe('GoodscategorydetailPage', () => {
  let component: GoodscategorydetailPage;
  let fixture: ComponentFixture<GoodscategorydetailPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GoodscategorydetailPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(GoodscategorydetailPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
