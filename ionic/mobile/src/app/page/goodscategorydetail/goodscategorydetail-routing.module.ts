import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { GoodscategorydetailPage } from './goodscategorydetail.page';

const routes: Routes = [
  {
    path: '',
    component: GoodscategorydetailPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class GoodscategorydetailPageRoutingModule {}
