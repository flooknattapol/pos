import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { GoodscategorydetailPageRoutingModule } from './goodscategorydetail-routing.module';

import { GoodscategorydetailPage } from './goodscategorydetail.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    GoodscategorydetailPageRoutingModule
  ],
  declarations: [GoodscategorydetailPage]
})
export class GoodscategorydetailPageModule {}
