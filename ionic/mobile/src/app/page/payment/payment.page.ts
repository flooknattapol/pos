import { TsDiscountModel,DiscountModel } from './../../service/discount/discount.service';
import { UtilityService } from './../../service/utility/utility.service';
import { TransectionService, TransectionModel } from './../../service/transection/transection.service';
import { CustomerModel } from './../../service/customer/customer.service';
import { GoodsModel } from './../../service/goods/goods.service';
import { Component, OnInit } from '@angular/core';
import { NavController } from '@ionic/angular';
import { ActivatedRoute ,NavigationExtras} from "@angular/router";
import { ThrowStmt } from '@angular/compiler';
import { BarcodeScanner } from '@ionic-native/barcode-scanner/ngx';

@Component({
  selector: 'app-payment',
  templateUrl: './payment.page.html',
  styleUrls: ['./payment.page.scss'],
})
export class PaymentPage implements OnInit {
  TotalOrder:number;
  TotalCost:number;
  Received:number = 0;
  TempGoodsList:GoodsModel[];
  change:number = 0;
  customer:CustomerModel;
  TransectionModel:TransectionModel;
  TransectionDetailModel:any;
  cus_id:number;
  qrPromp:boolean = false;
  PathIMAGE:any;
  imageQr:any;
  discount:number;
  TempDiscount:DiscountModel[];
  TsDiscountModel:TsDiscountModel;
  tempAmountDiscount:number;
  GoodsList:GoodsModel[];
  ORDERTYPE:any;
  DiscountList:DiscountModel[];
  constructor(private barcodeScanner: BarcodeScanner,private nav:NavController,private route: ActivatedRoute,
    private TransectionService:TransectionService,private apiHelper:UtilityService) { 
    this.route.queryParams.subscribe(params => {
      this.TotalOrder =params['TotalOrder'];
      this.TotalCost =params['TotalCost'];
      this.TempGoodsList = params['GoodsList'];
      this.GoodsList = params['Goodsitem'];
      this.TempDiscount = params['DiscountList'];
      this.customer = params['customer'];
      this.discount = params['discount'];
      this.DiscountList = params['Discountitem'];
      this.PathIMAGE = apiHelper.ImagePath;  
    });
  }

  ngOnInit() {
    if(this.TempGoodsList.length > 0){
      this.TempGoodsList = JSON.parse( window.localStorage.getItem('TempGoodsList'));
    }else{
      this.TempGoodsList = [];
    }
    if(this.TempDiscount.length > 0){
      this.TempDiscount = JSON.parse( window.localStorage.getItem('TempDiscount'));
    }else{
      this.TempDiscount = [];
    }
  }
  onOpenScanbarcode(){
    var blScaner = false;
    this.barcodeScanner.scan().then(barcodeData => {
      console.log('Barcode data', barcodeData);
      var _UID = barcodeData.text;
      var _blUID = _UID.substring(0,2);
      var _price;
      var ArrayData = [];
      if(_blUID == "88"){
        ArrayData = JSON.parse( window.localStorage.getItem('TempGoodsList'));
      
        this.GoodsList.forEach(function (entry, index) {
          if(_UID === entry.UID)
          {
            blScaner = true;
            _price = entry.PRICE;
            window.localStorage.removeItem['TempGoodsList'];
            ArrayData.push(entry);
          }
        });
        if(blScaner == true){
          window.localStorage['TempGoodsList'] = JSON.stringify(ArrayData);
          this.TempGoodsList = ArrayData;
          this.TotalOrder += +_price;
        }else{
          alert("ไม่พบสินค้า");
        }
      }else if(_blUID == "11"){
        if(this.TempDiscount.length > 0){
          alert("คุณใช้ส่วนลดเรียบร้อยแล้ว");
        }else{
          ArrayData = JSON.parse( window.localStorage.getItem('TempDiscount'));
          this.DiscountList.forEach(function (entry, index) {
            if(_UID === entry.UID)
            {
              blScaner = true;
              window.localStorage.removeItem['TempDiscount'];
              ArrayData.push(entry);
            }
          });
          if(blScaner == true){
            this.TempDiscount = ArrayData;
            window.localStorage['TempDiscount'] = JSON.stringify(ArrayData);
            if(this.TempDiscount.length > 0){
              if( this.TempDiscount[0].BAHTORPERCENT == 0){
                this.TotalOrder -= this.TempDiscount[0].DISCOUNT;
                this.tempAmountDiscount = this.TempDiscount[0].DISCOUNT;
              }else{
                this.tempAmountDiscount = (this.TotalOrder * this.TempDiscount[0].DISCOUNT)/100;;
                this.TotalOrder = this.TotalOrder - this.tempAmountDiscount
              }
            }
          }else{
            alert("ไม่พบส่วนลด");
          }
        } 
      }else{
        alert("barcode ไม่ถูกต้อง");
      }
      
     }).catch(err => {
         console.log('Error', err);
     });
  }
  onDeleteItemInList(item:any){
    var _index;
    var price;
    var cost;
    this.TempGoodsList.forEach(function (entry, index) {
      if (item.ID === entry.ID){
         price = +item.PRICE;
         cost =  +item.COST;
        _index = index;
      }
    });
    window.localStorage.removeItem['TempGoodsList'];
    this.TempGoodsList.splice(_index,1);
    window.localStorage['TempGoodsList'] = JSON.stringify(this.TempGoodsList);
    this.TotalOrder -= price;
    this.TotalCost -= cost;
    if(this.TempGoodsList.length == 0){
      window.localStorage['TempGoodsList'] = JSON.stringify([]);
      window.localStorage['TempDiscount'] = JSON.stringify([]);
      this.nav.navigateRoot('/home');
    }
  }
  onDeleteDiscountInList(item:any){
    this.TotalOrder += +this.discount;
    this.TempDiscount = [];
    window.localStorage['TempDiscount'] = JSON.stringify([]);
  }
  onCashPromptpay(){
    var data = this.TransectionService.getQrPromp(this.TotalOrder).then((result: any)=> { 
      this.qrPromp = true;
      this.imageQr = result;
    })
  }
  onCashPromptpaySuccess(){

    this.Received = this.TotalOrder;
    this.change = this.Received - this.TotalOrder;
    if(this.TotalOrder <= this.Received){
      var Item = JSON.parse(window.localStorage.getItem('Object'))
      var dateobj = Date.now();
      var datestr = dateobj.toString(); 
    
      var order_id = Item.id+datestr;
      if(this.customer.ID == undefined){
         this.cus_id = 0;
      }else{
         this.cus_id = this.customer.ID;
      }
      this.TransectionModel = new TransectionModel();
      this.TransectionModel.CUSTOMER_ID = this.cus_id;
      this.TransectionModel.CATEGORYPAYMENT = "พร้อมเพย์";
      this.TransectionModel.ORDER_NAME = order_id;
      this.TransectionModel.SELLER_ID = Item.id;
      this.TransectionModel.TotalOrder = this.TotalOrder;
      this.TransectionModel.TotalCost = this.TotalCost;
      this.TransectionModel.Received = this.Received;
      this.TransectionModel.ORDERTYPE = this.ORDERTYPE;
      this.TransectionModel.Change = this.change;
      var TransectionDetailModel = new Array;
      var listMessageModel = new Array;

      this.TempGoodsList.forEach(function (entry, index) {
        var Object = {
          ID:0,
          PRICE:entry.PRICE,
          COST:entry.COST,
          GOODS_ID:entry.ID
        }
        var ObjectMessage = {
          NAME:entry.NAME,
          PRICE:entry.PRICE,
        }
        listMessageModel.push(ObjectMessage);
        TransectionDetailModel.push(Object);
      });
      this.TransectionModel.Detail = TransectionDetailModel
      this.TsDiscountModel = new TsDiscountModel();
      this.TsDiscountModel.AMOUNT_DISCOUNT =  this.discount;
      this.TransectionModel.Discount = this.TsDiscountModel;
      let navigationExtras: NavigationExtras= {
        queryParams: {
            TotalOrder: this.TotalOrder,
            GoodsList:this.TempGoodsList,
            change:this.change,
            customer:this.customer,
            ORDERTYPE:this.ORDERTYPE,
            CATEGORYPAYMENT:"พร้อมเพย์",
            order_id:order_id
        }
      };
      var message =  {
        "order_id":order_id,
        "TotalOrder":this.TotalOrder,
        "emp":Item.id,
        "user_id":this.customer.userID,
        "customer_name":this.customer.NAME,
        "type_payment":"พร้อมเพย์",
        "list":listMessageModel
      }
      var data = this.TransectionService.add(this.TransectionModel).then((result: any)=> { 
        if(result.status == true){
          if(this.customer.ID == undefined){

          }else{
            var data2 = this.TransectionService.sentReceipt(message).then((result2: any)=> { 
              if(result2.status == 200){
  
              }
             else{
               alert(result2.message);
             }
            })
          }    
          window.localStorage['TempGoodsList'] = JSON.stringify([]);
          window.localStorage['TempDiscount'] = JSON.stringify([]);
          this.nav.navigateForward('/paymentend',navigationExtras);
        }else{
          alert(result.message)
        }
      })
    }
  }
  onCashPromptpayCancel(){
    this.qrPromp = false;
  }
  onCash(){
    
    this.change = this.Received - this.TotalOrder;
    
    if(this.TotalOrder <= this.Received){
      var Item = JSON.parse(window.localStorage.getItem('Object'))
      var dateobj = Date.now();
      var datestr = dateobj.toString(); 
      var order_id = Item.id+datestr;
      if(this.customer.ID == undefined){
         this.cus_id = 0;
      }else{
         this.cus_id = this.customer.ID;
      }
      this.TransectionModel = new TransectionModel();
      this.TransectionModel.CUSTOMER_ID = this.cus_id;
      this.TransectionModel.CATEGORYPAYMENT = "เงินสด";
      this.TransectionModel.ORDER_NAME = order_id;
      this.TransectionModel.SELLER_ID = Item.id;
      this.TransectionModel.TotalOrder = this.TotalOrder;
      this.TransectionModel.TotalCost = this.TotalCost;
      this.TransectionModel.Received = this.Received;
      this.TransectionModel.ORDERTYPE = this.ORDERTYPE;
      this.TransectionModel.Change = this.change;
      var TransectionDetailModel = new Array;
      var listMessageModel = new Array;

      this.TempGoodsList.forEach(function (entry, index) {
        var Object = {
          ID:0,
          PRICE:entry.PRICE,
          COST:entry.COST,
          GOODS_ID:entry.ID
        }
        var ObjectMessage = {
          NAME:entry.NAME,
          PRICE:entry.PRICE,
        }
        listMessageModel.push(ObjectMessage);
        TransectionDetailModel.push(Object);
      });
      this.TransectionModel.Detail = TransectionDetailModel
      this.TsDiscountModel = new TsDiscountModel();
      this.TsDiscountModel.AMOUNT_DISCOUNT =  this.discount;
      this.TransectionModel.Discount = this.TsDiscountModel;
      let navigationExtras: NavigationExtras= {
        queryParams: {
            TotalOrder: this.TotalOrder,
            GoodsList:this.TempGoodsList,
            change:this.change,
            customer:this.customer,
            ORDERTYPE:this.ORDERTYPE,
            CATEGORYPAYMENT:"เงินสด",  
            order_id:order_id
        }
      };
      var message =  {
        "order_id":order_id,
        "TotalOrder":this.TotalOrder,
        "emp":Item.id,
        "user_id":this.customer.userID,
        "customer_name":this.customer.NAME,
        "type_payment":"เงินสด",
        "list":listMessageModel
      }
      var data = this.TransectionService.add(this.TransectionModel).then((result: any)=> { 
        if(result.status == true){
          if(this.customer.ID == undefined){

          }else{
            var data2 = this.TransectionService.sentReceipt(message).then((result2: any)=> { 
          
              if(result2.status == 200){
  
              }
             else{
               alert(result2.message);
             }
            })
          }  
          window.localStorage['TempGoodsList'] = JSON.stringify([]);
          window.localStorage['TempDiscount'] = JSON.stringify([]);
          this.nav.navigateForward('/paymentend',navigationExtras);
        }else{
          alert(result.message)
        }
      })
    }else{
      alert("กรุณาใส่จำนวนเงินรับที่ถูกต้อง");
    }
  }
  onReceive50(){
    this.Received = 50;
  }
  onReceive100(){
    this.Received = 100;
  }
  onReceive200(){
    this.Received = 200;
  }
  onReceive500(){
    this.Received = 500;
  }
  onReceive1000(){
    this.Received = 1000;
  }
}
