import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { CustomerdetailPageRoutingModule } from './customerdetail-routing.module';

import { CustomerdetailPage } from './customerdetail.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    CustomerdetailPageRoutingModule
  ],
  declarations: [CustomerdetailPage]
})
export class CustomerdetailPageModule {}
