import { UtilityService } from './../../service/utility/utility.service';
import { CustomerModel, CustomerService } from './../../service/customer/customer.service';
import { Component, OnInit } from '@angular/core';
import { NavController } from '@ionic/angular';
import { ActivatedRoute ,NavigationExtras} from "@angular/router";

@Component({
  selector: 'app-customerdetail',
  templateUrl: './customerdetail.page.html',
  styleUrls: ['./customerdetail.page.scss'],
})
export class CustomerdetailPage implements OnInit {
    CustomerModel:CustomerModel;
    flag:boolean;
    PathIMAGE:any;
  constructor(private nav:NavController,private route: ActivatedRoute,private apiHelper:UtilityService,private CustomerService:CustomerService) { 
      this.route.queryParams.subscribe(params => {
      this.CustomerModel = new CustomerModel;
      this.CustomerModel =params['item'];
      this.flag = params['flag'];
      this.PathIMAGE = apiHelper.ImagePath; 
    });
  }

  ngOnInit() {
  }
  onSave(){
    let navigationExtras: NavigationExtras= {
      queryParams: {
          item: this.CustomerModel,
      }
    };
    this.nav.navigateForward('/home',navigationExtras);
  }
  onDelete(){
    let navigationExtras: NavigationExtras= {
      queryParams: {
          item: undefined,
      }
    };
    this.nav.navigateForward('/home',navigationExtras);
  }
  onSaveNOTE(){
    console.log(this.CustomerModel)
    var data = this.CustomerService.addCustomer(this.CustomerModel).then((result: any)=> { 
      if(result.status == true){
        alert("บันทึกเสร็จสิ้น");
      }else{
        alert(result.message)
      }
    })
    
  }
}
