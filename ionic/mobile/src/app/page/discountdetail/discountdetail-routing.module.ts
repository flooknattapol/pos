import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { DiscountdetailPage } from './discountdetail.page';

const routes: Routes = [
  {
    path: '',
    component: DiscountdetailPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class DiscountdetailPageRoutingModule {}
