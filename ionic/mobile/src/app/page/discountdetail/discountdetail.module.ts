import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { DiscountdetailPageRoutingModule } from './discountdetail-routing.module';

import { DiscountdetailPage } from './discountdetail.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    DiscountdetailPageRoutingModule
  ],
  declarations: [DiscountdetailPage]
})
export class DiscountdetailPageModule {}
