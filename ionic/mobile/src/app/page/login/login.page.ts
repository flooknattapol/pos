import { AppComponent } from './../../app.component';
import { NavController, MenuController } from '@ionic/angular';
import { UserService, UserLoginmModel } from './../../service/user/user.service';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute ,NavigationExtras} from "@angular/router";

@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})
export class LoginPage implements OnInit {
  CreateModel:UserLoginmModel;
  constructor(public UserService:UserService,private nav:NavController,public menuCtrl:MenuController,private AppComponent:AppComponent) { 
    this.CreateModel = new UserLoginmModel;
    this.CreateModel.EMAIL = "";
    this.CreateModel.PASSWORD = "";
    // this.menuCtrl.enable(false);

  }

  ngOnInit() {
    var item = JSON.parse(window.localStorage.getItem('Object'));
    console.log(item);
    if(item == null){
      window.localStorage['Object'] = JSON.stringify([]);
      item = [];
    }
    if(parseFloat(item.id) > 0){

      if(item.EXPIRED_FLAG == 1 && new Date(item.EXPIRED_DATE)  < new Date())
      {
        
      }else{
        this.nav.navigateRoot("/home"); 
      }
    }
  }
  ngOnRegister(){

  }
  ngOnLogin(){
    var bVlid = this.validatecheck();
    if(bVlid == true)
    {
      var data = this.UserService.Login(this.CreateModel).then((result: any)=> { 
        console.log(result);
        if(result.status == true){
          window.localStorage['Object'] = JSON.stringify(result.data);
          window.localStorage['TempGoodsList'] = JSON.stringify([]);
          window.localStorage['TempDiscount'] = JSON.stringify([]);
          if(result.data.business_isactive == '1'){
            this.AppComponent.ngOnInit();
            this.nav.navigateRoot("/home");  

          }else{
            alert('ร้านค้าถูกปิดใช้งาน');
          }
        }else{
          if(result.statusexpired == false && result.statusexpired != undefined){
            let navigationExtras: NavigationExtras= {
              queryParams: {
                  item: result.data,
              }
            };
            alert(result.message);
            this.nav.navigateForward('/renewbusiness',navigationExtras);
          }else{
            alert(result.message);
          }
        }
      })
    }else{
      alert("ใส่ข้อมูลไม่ครบถ้วน")
    }
  }
  validatecheck(){
    var bResult = true;
    if(this.CreateModel.EMAIL == ""){
			bResult = false;
    }
    if(this.CreateModel.PASSWORD == ""){
			bResult = false;
    }
    return bResult;
  }
  onRegister(){
    this.nav.navigateForward("/register"); 
  }
  onForgetpassword(){
    this.nav.navigateForward("/forgetpassword"); 

  }
  onChangepassword(){
    this.nav.navigateForward("/changepassword"); 

  }
}
