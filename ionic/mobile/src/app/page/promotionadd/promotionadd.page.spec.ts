import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { PromotionaddPage } from './promotionadd.page';

describe('PromotionaddPage', () => {
  let component: PromotionaddPage;
  let fixture: ComponentFixture<PromotionaddPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PromotionaddPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(PromotionaddPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
