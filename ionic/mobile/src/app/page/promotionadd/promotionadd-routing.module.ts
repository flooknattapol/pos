import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { PromotionaddPage } from './promotionadd.page';

const routes: Routes = [
  {
    path: '',
    component: PromotionaddPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class PromotionaddPageRoutingModule {}
