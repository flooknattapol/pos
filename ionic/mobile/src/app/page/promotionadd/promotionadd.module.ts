import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { PromotionaddPageRoutingModule } from './promotionadd-routing.module';

import { PromotionaddPage } from './promotionadd.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    PromotionaddPageRoutingModule
  ],
  declarations: [PromotionaddPage]
})
export class PromotionaddPageModule {}
