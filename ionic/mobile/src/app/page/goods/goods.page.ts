import { Component, OnInit } from '@angular/core';
import { NavController } from '@ionic/angular';

@Component({
  selector: 'app-goods',
  templateUrl: './goods.page.html',
  styleUrls: ['./goods.page.scss'],
})
export class GoodsPage implements OnInit {

  constructor(private nav:NavController) { }

  ngOnInit() {
  }
  onListGoods(){
    this.nav.navigateForward('/listgoods');
  }
  onListCategory(){
    this.nav.navigateForward('/goodscategory');
  }
  onListDiscount(){
    this.nav.navigateForward('/discount');
  }
}
