import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { GoodscategoryaddPageRoutingModule } from './goodscategoryadd-routing.module';

import { GoodscategoryaddPage } from './goodscategoryadd.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    GoodscategoryaddPageRoutingModule
  ],
  declarations: [GoodscategoryaddPage]
})
export class GoodscategoryaddPageModule {}
