import { GoodsComboModel, GoodsService } from './../../service/goods/goods.service';
import { NavController } from '@ionic/angular';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute ,NavigationExtras} from "@angular/router";

@Component({
  selector: 'app-goodscategoryadd',
  templateUrl: './goodscategoryadd.page.html',
  styleUrls: ['./goodscategoryadd.page.scss'],
})
export class GoodscategoryaddPage implements OnInit {
  CreateModel:GoodsComboModel;
  constructor(private nav:NavController,private route: ActivatedRoute,private GoodsService:GoodsService) { 
    this.route.queryParams.subscribe(params => { 
      this.CreateModel = params['CreateModel'];
    });
  }

  ngOnInit() {
  }
  OnSave(){
    var bValid = true;
    if(this.CreateModel.NAME == "" || this.CreateModel.NAME == null  || this.CreateModel.NAME == undefined){
      bValid = false;
    }
    if(bValid == true){
      var Item = JSON.parse(window.localStorage.getItem('Object'))
      this.CreateModel.idSession = Item.id;
      var data = this.GoodsService.savecategory(this.CreateModel).then((result: any)=> { 
        if(result.status == true){
          this.nav.pop();
        }else{
          alert(result.message)
        }
      })
    }else{
      alert("กรุณาใส่ข้อมูลให้ครบถ้วน");
    }
    
  }
}
