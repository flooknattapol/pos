import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  {
    path: '',
    redirectTo: 'login',
    pathMatch: 'full'
  },
  {
    path: 'folder/:id',
    loadChildren: () => import('./folder/folder.module').then( m => m.FolderPageModule)
  },
  {
    path: 'home', 
    loadChildren: () => import('./page/home/home.module').then( m => m.HomePageModule)
  },
  {
    path: 'payment',
    loadChildren: () => import('./page/payment/payment.module').then( m => m.PaymentPageModule)
  },
  {
    path: 'paymentend',
    loadChildren: () => import('./page/paymentend/paymentend.module').then( m => m.PaymentendPageModule)
  },
  {
    path: 'listorder',
    loadChildren: () => import('./page/listorder/listorder.module').then( m => m.ListorderPageModule)
  },
  {
    path: 'customer',
    loadChildren: () => import('./page/customer/customer.module').then( m => m.CustomerPageModule)
  },
  {
    path: 'addcustomer',
    loadChildren: () => import('./page/addcustomer/addcustomer.module').then( m => m.AddcustomerPageModule)
  },
  {
    path: 'customerdetail',
    loadChildren: () => import('./page/customerdetail/customerdetail.module').then( m => m.CustomerdetailPageModule)
  },
  {
    path: 'receipt',
    loadChildren: () => import('./page/receipt/receipt.module').then( m => m.ReceiptPageModule)
  },
  {
    path: 'receiptdetail',
    loadChildren: () => import('./page/receiptdetail/receiptdetail.module').then( m => m.ReceiptdetailPageModule)
  },
  {
    path: 'receiptcancel',
    loadChildren: () => import('./page/receiptcancel/receiptcancel.module').then( m => m.ReceiptcancelPageModule)
  },
  {
    path: 'listgoods',
    loadChildren: () => import('./page/listgoods/listgoods.module').then( m => m.ListgoodsPageModule)
  },
  {
    path: 'goods',
    loadChildren: () => import('./page/goods/goods.module').then( m => m.GoodsPageModule)
  },
  {
    path: 'discount',
    loadChildren: () => import('./page/discount/discount.module').then( m => m.DiscountPageModule)
  },
  {
    path: 'goodscategory',
    loadChildren: () => import('./page/goodscategory/goodscategory.module').then( m => m.GoodscategoryPageModule)
  },
  {
    path: 'setting',
    loadChildren: () => import('./page/setting/setting.module').then( m => m.SettingPageModule)
  },
  {
    path: 'login',
    loadChildren: () => import('./page/login/login.module').then( m => m.LoginPageModule)
  },
  {
    path: 'logout',
    loadChildren: () => import('./page/logout/logout.module').then( m => m.LogoutPageModule)
  },
  {
    path: 'backend',
    loadChildren: () => import('./page/backend/backend.module').then( m => m.BackendPageModule)
  },
  {
    path: 'register',
    loadChildren: () => import('./page/register/register.module').then( m => m.RegisterPageModule)
  },
  {
    path: 'goodsdetail',
    loadChildren: () => import('./page/goodsdetail/goodsdetail.module').then( m => m.GoodsdetailPageModule)
  },
  {
    path: 'goodsadd',
    loadChildren: () => import('./page/goodsadd/goodsadd.module').then( m => m.GoodsaddPageModule)
  },
  {
    path: 'goodscategorydetail',
    loadChildren: () => import('./page/goodscategorydetail/goodscategorydetail.module').then( m => m.GoodscategorydetailPageModule)
  },
  {
    path: 'goodscategoryadd',
    loadChildren: () => import('./page/goodscategoryadd/goodscategoryadd.module').then( m => m.GoodscategoryaddPageModule)
  },
  {
    path: 'discountdetail',
    loadChildren: () => import('./page/discountdetail/discountdetail.module').then( m => m.DiscountdetailPageModule)
  },
  {
    path: 'discountadd',
    loadChildren: () => import('./page/discountadd/discountadd.module').then( m => m.DiscountaddPageModule)
  },
  {
    path: 'help',
    loadChildren: () => import('./page/help/help.module').then( m => m.HelpPageModule)
  },
  {
    path: 'app',
    loadChildren: () => import('./page/app/app.module').then( m => m.AppPageModule)
  },
  {
    path: 'createorder',
    loadChildren: () => import('./page/createorder/createorder.module').then( m => m.CreateorderPageModule)
  },
  {
    path: 'forgetpassword',
    loadChildren: () => import('./page/forgetpassword/forgetpassword.module').then( m => m.ForgetpasswordPageModule)
  },
  {
    path: 'changepassword',
    loadChildren: () => import('./page/changepassword/changepassword.module').then( m => m.ChangepasswordPageModule)
  },
  {
    path: 'menu',
    loadChildren: () => import('./menu/menu.module').then( m => m.MenuPageModule)
  },
  {
    path: 'renewbusiness',
    loadChildren: () => import('./renewbusiness/renewbusiness.module').then( m => m.RenewbusinessPageModule)
  },  {
    path: 'promotion',
    loadChildren: () => import('./page/promotion/promotion.module').then( m => m.PromotionPageModule)
  },
  {
    path: 'promotionadd',
    loadChildren: () => import('./page/promotionadd/promotionadd.module').then( m => m.PromotionaddPageModule)
  },
  {
    path: 'promotiondetail',
    loadChildren: () => import('./page/promotiondetail/promotiondetail.module').then( m => m.PromotiondetailPageModule)
  },


  // {
  //   path: 'backend',
  //   // loadChildren: () => import('./page/setting/setting.module').then( m => m.SettingPageModule)
  // }
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule {}
