import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { RenewbusinessPage } from './renewbusiness.page';

describe('RenewbusinessPage', () => {
  let component: RenewbusinessPage;
  let fixture: ComponentFixture<RenewbusinessPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RenewbusinessPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(RenewbusinessPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
