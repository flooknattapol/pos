import { NavController } from '@ionic/angular';
import { BusinessService } from './../service/business/business.service';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute ,NavigationExtras} from "@angular/router";

@Component({
  selector: 'app-renewbusiness',
  templateUrl: './renewbusiness.page.html',
  styleUrls: ['./renewbusiness.page.scss'],
})
export class RenewbusinessPage implements OnInit {

  CODE:any = "";
  EMAILBUSINESS:any ="";
  item:any;
  constructor(private BusinessService:BusinessService,private route: ActivatedRoute,private nav:NavController) { 
    this.route.queryParams.subscribe(params => { 
      this.item = params['item'];
      console.log(this.item);
    });
  }

  ngOnInit() {

  }
  ngSaveRenewCode(){
    if(this.CODE != ""){
      var data = this.BusinessService.saveRenewCode(this.CODE,this.item.business_id).then((result: any)=> { 
        if(result.status == true){
          alert("วันหมดอายุใหม่" + result.message);
          this.nav.navigateRoot('/login');
        }else{
          alert(result.message)
        }
      })
    }else{
      alert("โปรใส่ข้อมูลให้ครบถ้วน");
    }
  }
}
