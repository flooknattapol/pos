import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { RenewbusinessPage } from './renewbusiness.page';

const routes: Routes = [
  {
    path: '',
    component: RenewbusinessPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class RenewbusinessPageRoutingModule {}
