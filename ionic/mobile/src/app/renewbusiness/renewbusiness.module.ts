import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { RenewbusinessPageRoutingModule } from './renewbusiness-routing.module';

import { RenewbusinessPage } from './renewbusiness.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RenewbusinessPageRoutingModule
  ],
  declarations: [RenewbusinessPage]
})
export class RenewbusinessPageModule {}
