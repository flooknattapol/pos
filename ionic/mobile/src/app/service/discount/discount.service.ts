import { HttpClient } from '@angular/common/http';
import { UtilityService } from './../utility/utility.service';
import { Injectable } from '@angular/core';
@Injectable({
  providedIn: 'root'
})
export class DiscountService {

  constructor(private http:HttpClient,private apiHelper:UtilityService) { }
  urlGet = "getListDiscount";
  urlsave ="adddiscount";
  urlgetListRelation ="getListRelation";
  getList(maxsizepage:number) {
    var Item = JSON.parse(window.localStorage.getItem('Object'))
    var SearchObject =  {
      "PageIndex": 1,
      "PageSize": maxsizepage,
      "SortColumn": "ID",
      "SortOrder": "asc",
      "idSession":Item.id,
      "mSearch": {
        "NAME": ""
      }
    }
    var myJSON = JSON.stringify(SearchObject); 
    return new Promise((resolve,reject) => 
    this.http.get(this.apiHelper.apiUrl + this.urlGet  + "?JsonData=" + myJSON + "&authen=" + this.apiHelper.apiKey + "&version=" + this.apiHelper.apiVersion, {})
    .subscribe((res) => {
      resolve(res);
    },(err)=>{
      reject(err);
    }))
  }
  save(Item) {
    var JsonData =  {
      "ID":Item.ID,
      "NAME":Item.NAME,
      "BAHTORPERCENT":Item.BAHTORPERCENT,
      "DISCOUNT":Item.DISCOUNT,
      "idSession":Item.idSession
    }
    var myJSON = JSON.stringify(JsonData); 
    return new Promise((resolve,reject) => 
    this.http.get(this.apiHelper.apiUrl + this.urlsave  + "?JsonData=" + myJSON + "&authen=" + this.apiHelper.apiKey + "&version=" + this.apiHelper.apiVersion, {})
    .subscribe((res) => {
      resolve(res);
    },(err)=>{
      reject(err);
    }))
  }
  getListRelation(UID) {
    var JsonData =  {
      "UID":UID,
    }
    var myJSON = JSON.stringify(JsonData); 
    return new Promise((resolve,reject) => 
    this.http.get(this.apiHelper.apiUrl + this.urlgetListRelation  + "?JsonData=" + myJSON + "&authen=" + this.apiHelper.apiKey + "&version=" + this.apiHelper.apiVersion, {})
    .subscribe((res) => {
      resolve(res);
    },(err)=>{
      reject(err);
    }))
  }
}
export class DiscountModel{
  ID:any;
  NAME:any;
  DETAIL:any;
  UID:any;
  BARCODE:any;
  DISCOUNT:number;
  BAHTORPERCENT:any;
  BUSINESS_ID:any;
  IsActive:any;
  idSession:any;

}
export class TsDiscountModel{
  ID:any;
  TRANSECTION_ID:any;
  AMOUNT_DISCOUNT:any;
}
