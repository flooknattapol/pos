import { HttpClient } from '@angular/common/http';
import { UtilityService } from './../utility/utility.service';
import { Injectable } from '@angular/core';
@Injectable({
  providedIn: 'root'
})
export class CustomerService {

  constructor(private http:HttpClient,private apiHelper:UtilityService) { }
  urlGet = "getListCustomer";
  urladd = "addCustomerToBusiness";
  urladdcustomer = "addCustomerGet";
  getList(Search:string,maxsizepage:number) {
    var Item = JSON.parse(window.localStorage.getItem('Object'))
    var SearchObject =  {
      "PageIndex": 1,
      "PageSize": maxsizepage,
      "SortColumn": "ID",
      "SortOrder": "DESC",
      "idSession":Item.id,
      "mSearch": {
        "NAME": Search
      }
    }
    var myJSON = JSON.stringify(SearchObject); 
    return new Promise((resolve,reject) => 
    this.http.get(this.apiHelper.apiUrl + this.urlGet  + "?JsonData=" + myJSON + "&authen=" + this.apiHelper.apiKey + "&version=" + this.apiHelper.apiVersion, {})
    .subscribe((res) => {
      resolve(res);
    },(err)=>{
      reject(err);
    })
  )};
  add(TEL) {
    var Item = JSON.parse(window.localStorage.getItem('Object'))
    var JsonData =  {
      "idSession": Item.id,
      "TEL": TEL,
    }
    var myJSON = JSON.stringify(JsonData); 
    return new Promise((resolve,reject) => 
    this.http.get(this.apiHelper.apiUrl + this.urladd  + "?JsonData=" + myJSON + "&authen=" + this.apiHelper.apiKey + "&version=" + this.apiHelper.apiVersion, {})
    .subscribe((res) => {
      resolve(res);
    },(err)=>{
      reject(err);
    }))
  }
  addCustomer(JsonData) {

    var myJSON = JSON.stringify(JsonData); 
    return new Promise((resolve,reject) => 
    this.http.get(this.apiHelper.apiUrl + this.urladdcustomer  + "?JsonData=" + myJSON + "&authen=" + this.apiHelper.apiKey + "&version=" + this.apiHelper.apiVersion, {})
    .subscribe((res) => {
      resolve(res);
    },(err)=>{
      reject(err);
    }))
  }
}
export class CustomerModel{
  NAME:any;
  TEL:any;
  EMAIL:any;
  CATEGORY:any;
  BUSINESS_ID:any;
  AMOUNTBUY:any;
  pictureUrl:any;
  REWARDPOINT:any;
  NOTE:any;
  userID:any;
  IsActive:any;
  ID:any = 0;
}
