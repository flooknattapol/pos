import { TestBed } from '@angular/core/testing';

import { CustomercategoryService } from './customercategory.service';

describe('CustomercategoryService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: CustomercategoryService = TestBed.get(CustomercategoryService);
    expect(service).toBeTruthy();
  });
});
