import { UtilityService } from './../utility/utility.service';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class CustomercategoryService {
  urlComboList = "getComboList"
  constructor(private http:HttpClient,private apiHelper:UtilityService) { }
  getComboList() {
    var Item = JSON.parse(window.localStorage.getItem('Object'))
    var SearchObject =  {
      "idSession":Item.id
    }
    var myJSON = JSON.stringify(SearchObject); 
    return new Promise((resolve,reject) => 
    this.http.get(this.apiHelper.apiUrl + this.urlComboList  + "?JsonData=" + myJSON + "&authen=" + this.apiHelper.apiKey + "&version=" + this.apiHelper.apiVersion, {})
    .subscribe((res) => {
      resolve(res);
    },(err)=>{
      reject(err);
    })
  )};
}
export class CustomercategoryModel{
  ID:any = 0;
  NAME:any;
  DETAIL:any;
  IsActive:any;
}
