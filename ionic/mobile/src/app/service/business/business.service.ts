import { HttpClient } from '@angular/common/http';
import { UtilityService } from './../utility/utility.service';
import { Injectable } from '@angular/core';
@Injectable({
  providedIn: 'root'
})
export class BusinessService {
  urlComboType ="getComboListBusinessType";
  urlComboCombeCategory = "getComboListBusinessCategory";
  urlsave="addbusiness";
  urlrenewcode = "renewbusiness";
  constructor(private http:HttpClient,private apiHelper:UtilityService) { }
  save(JsonData) {
    var myJSON = JSON.stringify(JsonData); 
    return new Promise((resolve,reject) => 
    this.http.get(this.apiHelper.apiUrl + this.urlsave  + "?JsonData=" + myJSON + "&authen=" + this.apiHelper.apiKey + "&version=" + this.apiHelper.apiVersion, {})
    .subscribe((res) => {
      resolve(res);
    },(err)=>{
      reject(err);
    }))
  }
  getCombeType() {
    return new Promise((resolve,reject) => 
    this.http.get(this.apiHelper.apiUrl + this.urlComboType  + "?authen=" + this.apiHelper.apiKey + "&version=" + this.apiHelper.apiVersion, {})
    .subscribe((res) => {
      resolve(res);
    },(err)=>{
      reject(err);
    }))
  }
  getCombeCategory() {
    return new Promise((resolve,reject) => 
    this.http.get(this.apiHelper.apiUrl + this.urlComboCombeCategory  + "?authen=" + this.apiHelper.apiKey + "&version=" + this.apiHelper.apiVersion, {})
    .subscribe((res) => {
      resolve(res);
    },(err)=>{
      reject(err);
    }))
  }
  saveRenewCode(CODE,ID) {
    var JsonData ={
      "CODE":CODE,
      "BUSINESS_ID":ID
    }
    var myJSON = JSON.stringify(JsonData); 
    return new Promise((resolve,reject) => 
    this.http.get(this.apiHelper.apiUrl + this.urlrenewcode  + "?JsonData=" + myJSON + "&authen=" + this.apiHelper.apiKey + "&version=" + this.apiHelper.apiVersion, {})
    .subscribe((res) => {
      resolve(res);
    },(err)=>{
      reject(err);
    }))
  }
}
export class BusinessModel{
  ID:number = 0;
  EMAIL:any;
  TEL:any;
  NAME:any;
  PASSWORD:any;
  PASSWORD2:any;
  TAXID10:any;
  TAXID13:any;
  TYPE_ID:any;
  IMAGE:any;
  CATEGORY_ID:any;
  CONTACT:any; 
  IsActive:any;
  EXPIRED_FLAG:any;
  CODE:any;
  detail:detailModel[];
}
export class detailModel{
  ID:number = 0;
  NAME:any;
  TEL:any;
  ADDRESS:any;
  EMAIL:any;
}
export class comboModel{
  ID:any;
  NAME:any;
}