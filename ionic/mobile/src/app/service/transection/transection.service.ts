import { TsDiscountModel } from './../discount/discount.service';
import { HttpClient ,HttpHeaders} from '@angular/common/http';
import { Injectable } from '@angular/core';
import { UtilityService } from '../utility/utility.service';

@Injectable({
  providedIn: 'root'
})
export class TransectionService {
  
  urladd = "addTransection";
  urladdtemporder = "addTempOrder";
  urlGet = "getListTransection";
  urlGetDetail = "getListDetailTransection";
  urldelete = "deleteTransection";
  urldeleteDetail = "deleteDetailTransection";
  urlGetPromp = "getQrCodePrompay";
  urlGetTempOrder = "getListTempOrder";
  urlgetTemp = "getTempOrder";
  TotalOrder:any;
  Tel:any;
  urldeletetemporder = "deletetemporder";
  constructor(private http:HttpClient,private apiHelper:UtilityService) { }
  getList(Search,maxsizepage:number) {
    var Item = JSON.parse(window.localStorage.getItem('Object'))
    var SearchObject =  {
      "PageIndex": 1,
      "PageSize": maxsizepage,
      "SortColumn": "ID",
      "SortOrder": "DESC",
      "idSession":Item.id,
      "mSearch": {
        "NAME": Search
      }
    }
    var myJSON = JSON.stringify(SearchObject); 
    return new Promise((resolve,reject) => 
    this.http.get(this.apiHelper.apiUrl + this.urlGet  + "?JsonData=" + myJSON + "&authen=" + this.apiHelper.apiKey + "&version=" + this.apiHelper.apiVersion, {})
    .subscribe((res) => {
      resolve(res);
    },(err)=>{
      reject(err);
    }))
  }
  getListTempOrder(Search) {
    var Item = JSON.parse(window.localStorage.getItem('Object'))
    var SearchObject =  {
      "PageIndex": 1,
      "PageSize": 100,
      "SortColumn": "ID",
      "SortOrder": "DESC",
      "idSession":Item.id,
      "mSearch": {
        "NAME": Search
      }
    }
    var myJSON = JSON.stringify(SearchObject); 
    return new Promise((resolve,reject) => 
    this.http.get(this.apiHelper.apiUrl + this.urlGetTempOrder  + "?JsonData=" + myJSON + "&authen=" + this.apiHelper.apiKey + "&version=" + this.apiHelper.apiVersion, {})
    .subscribe((res) => {
      resolve(res);
    },(err)=>{
      reject(err);
    }))
  }
  getDetail(JsonData) {
    var Item = JSON.parse(window.localStorage.getItem('Object'))
    var SearchObject =  {
      "TRANSECTION_ID":JsonData.ID,
    }
    var myJSON = JSON.stringify(SearchObject); 
    return new Promise((resolve,reject) => 
    this.http.get(this.apiHelper.apiUrl + this.urlGetDetail  + "?JsonData=" + myJSON + "&authen=" + this.apiHelper.apiKey + "&version=" + this.apiHelper.apiVersion, {})
    .subscribe((res) => {
      resolve(res);
    },(err)=>{
      reject(err);
    }))
  }
  add(JsonData) {
    var Item = JSON.parse(window.localStorage.getItem('Object'))
    JsonData.idSession = Item.id;
    var myJSON = JSON.stringify(JsonData); 
    return new Promise((resolve,reject) => 
    this.http.get(this.apiHelper.apiUrl + this.urladd  + "?JsonData=" + myJSON + "&authen=" + this.apiHelper.apiKey + "&version=" + this.apiHelper.apiVersion, {})
    .subscribe((res) => {
      resolve(res);
    },(err)=>{
      reject(err);
    }))
  }
  delete(JsonData) {

    JsonData = {
      ID:JsonData
    }
    var myJSON = JSON.stringify(JsonData); 
    return new Promise((resolve,reject) => 
    this.http.get(this.apiHelper.apiUrl + this.urldelete  + "?JsonData=" + myJSON + "&authen=" + this.apiHelper.apiKey + "&version=" + this.apiHelper.apiVersion, {})
    .subscribe((res) => {
      resolve(res);
    },(err)=>{
      reject(err);
    }))
  }
  deleteDetail(JsonData) {

    JsonData = {
      ID:JsonData
    }
    var myJSON = JSON.stringify(JsonData); 
    return new Promise((resolve,reject) => 
    this.http.get(this.apiHelper.apiUrl + this.urldeleteDetail  + "?JsonData=" + myJSON + "&authen=" + this.apiHelper.apiKey + "&version=" + this.apiHelper.apiVersion, {})
    .subscribe((res) => {
      resolve(res);
    },(err)=>{
      reject(err);
    }))
  }
  getQrPromp(item:any){
    var Item = JSON.parse(window.localStorage.getItem('Object'))
    this.Tel = Item.tel;
    this.TotalOrder = item;
    return new Promise((resolve,reject) => 
    this.http.get(this.apiHelper.apiRoot +'prompqr.php' + "?Tel=" + this.Tel + "&TotalOrder=" + this.TotalOrder, {})
    .subscribe((res) => {
      resolve(res);
    },(err)=>{
      reject(err);
    }))
  }
  getTempOrder(JsonData) {
    JsonData = {
      ID:JsonData
    }
    var myJSON = JSON.stringify(JsonData); 
    return new Promise((resolve,reject) => 
    this.http.get(this.apiHelper.apiUrl + this.urlgetTemp  + "?JsonData=" + myJSON + "&authen=" + this.apiHelper.apiKey + "&version=" + this.apiHelper.apiVersion, {})
    .subscribe((res) => {
      resolve(res);
    },(err)=>{
      reject(err);
    }))
  }
  addtemporder(JsonData) {
    var Item = JSON.parse(window.localStorage.getItem('Object'))
    JsonData.idSession = Item.id;
    var myJSON = JSON.stringify(JsonData); 
    return new Promise((resolve,reject) => 
    this.http.get(this.apiHelper.apiUrl + this.urladdtemporder  + "?JsonData=" + myJSON + "&authen=" + this.apiHelper.apiKey + "&version=" + this.apiHelper.apiVersion, {})
    .subscribe((res) => {
      resolve(res);
    },(err)=>{
      reject(err);
    }))
  }
  deleteTempOrder(JsonData) {
    JsonData = {
      ID:JsonData
    }
    var myJSON = JSON.stringify(JsonData); 
    return new Promise((resolve,reject) => 
    this.http.get(this.apiHelper.apiUrl + this.urldeletetemporder  + "?JsonData=" + myJSON + "&authen=" + this.apiHelper.apiKey + "&version=" + this.apiHelper.apiVersion, {})
    .subscribe((res) => {
      resolve(res);
    },(err)=>{
      reject(err);
    }))
  }
  sentReceipt(messageJson) {
    let Headers = {'Authorization':'basic 6eQI/wmouB9+R5mXA814nXnPEfqDPaTy+GVX32cTqTQ='};

    var messagelistdetail = "";
    messageJson.list.forEach(function (entry, index) {
      messagelistdetail += (index+1)+"." + entry.NAME + "  จำนวน 1 รายการ \nราคา " + entry.PRICE + "  บาท\n"
    });
    let current_datetime = new Date()
    var DateEnd = current_datetime.getDate() + "-" + (current_datetime.getMonth() + 1  ) + "-" + current_datetime.getFullYear()
    var message = "ใบเสร็จขายสินค้า\n"+ "วันที่: " + DateEnd +"\nเลขที่ใบเสร็จ : " + messageJson.order_id+ "\nประเภทการชำระเงิน :" + messageJson.type_payment + "\nลูกค้า:"+  messageJson.customer_name+" \nรายการ \n" + messagelistdetail +"\nรวมจำนวนเงิน "+messageJson.TotalOrder + " บาท"
    var JsonData =  {
      "Message":message
    }
    return new Promise((resolve,reject) => 
    this.http.post("https://www.devdeethailand.com/notify_api/NotifyService/" + messageJson.user_id,JsonData, {headers: Headers})
    .subscribe((res) => {
      resolve(res);
    },(err)=>{
      reject(err);
    }))
  }
  sentReceiptCancel(messageJson) {
    let Headers = {'Authorization':'basic 6eQI/wmouB9+R5mXA814nXnPEfqDPaTy+GVX32cTqTQ='};

    var messagelistdetail = "";
    messageJson.list.forEach(function (entry, index) {
      messagelistdetail += (index+1)+"." + entry.GOODS_NAME + "  จำนวน 1 รายการ \nราคา -" + entry.PRICE + "  บาท\n"
    });
    let current_datetime = new Date()
    var DateEnd = current_datetime.getDate() + "-" + (current_datetime.getMonth() + 1  ) + "-" + current_datetime.getFullYear()
    var message = "ยกเลิกขายสินค้า\n"+ "วันที่: " + DateEnd +"\nเลขที่ใบเสร็จ : " + messageJson.order_id+ "\nประเภทการชำระเงิน :" + messageJson.type_payment + "\nลูกค้า:"+  messageJson.customer_name+" \nรายการ \n" + messagelistdetail +"\nรวมจำนวนเงิน "+messageJson.TotalOrder + " บาท"
    var JsonData =  {
      "Message":message
    }
    return new Promise((resolve,reject) => 
    this.http.post("https://www.devdeethailand.com/notify_api/NotifyService/" + messageJson.user_id,JsonData, {headers: Headers})
    .subscribe((res) => {
      resolve(res);
    },(err)=>{
      reject(err);
    }))
  }
}

export class TransectionModel{
  ID:any;
  CUSTOMER_ID:any;
  ORDER_NAME:any;
  CATEGORYPAYMENT:any;
  SELLER_ID:any;  
  TimeStamp:any;
  TotalOrder:any;
  TotalCost:any;
  userID:any;
  Received	:any;
  Change:any;
  Detail:TransectionDetailModel[];
  Discount:TsDiscountModel;
  IsActive:any;
  BUSINESS_ID:any;
  CUSTOMER_NAME:any;
  ORDERTYPE:any;
  STATUS:any;
  AMOUNT_DISCOUNT:number;
  REFERENCE_ORDER_NAME:any;

}
export class TransectionDetailModel{
  ID:any;
  PRICE:any;
  GOODS_ID:any;
  AMOUNT:any;
  GOODS_NAME:any;
  IsActive:any;
  STATUS:any;

}
export class TempOrderModel{
  BUSINESS_ID:any;
  ID:number;
  ORDER_NAME:any;
  detail:TempOrderDetailModel[];
  discount:TempOrderDiscountModel[];
  TimeStamp:any;
  SELLER_ID:any;
  SELLER_NAME:any;

}
export class TempOrderDetailModel{
  TRANSECTION_ID:any;
  ID:number;
  GOODS_ID:any;
  
}
export class TempOrderDiscountModel{
  TRANSECTION_ID:any;
  ID:number;
  DISCOUNT_ID:any;
  
}
