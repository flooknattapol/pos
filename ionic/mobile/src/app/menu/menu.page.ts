import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-menu',
  templateUrl: './menu.page.html',
  styleUrls: ['./menu.page.scss'],
})
export class MenuPage implements OnInit {
  public selectedIndex = 0;
  public appPages = [
    {
      title: 'ขายสินค้า',
      url: '/menu/home',
      icon: 'cube'
    },
    {
      title: 'ใบเสร็จรับเงิน',
      url: '/receipt',
      icon: 'receipt'
    },
    {
      title: 'จัดการ ข้อมูลสินค้า',
      url: '/goods',
      icon: 'list'
    },
    // {
    //   title: 'การตั้งค่า',
    //   url: '/setting',
    //   icon: 'settings'
    // },
    // {
    //   title: 'ระบบหลังร้าน',
    //   url: '/backend',
    //   icon: 'bar-chart'
    // },
    {
      title: 'App version',
      url: '/app',
      icon: 'apps'
    },
    {
      title: 'ติดต่อผู้พัฒนา',
      url: '/help',
      icon: 'help-circle'
    },
    {
      title: 'ออกจากระบบ',
      url: '/logout',
      icon: 'log-out'
    },
  ];
  business_name:any;
  user:any;
  constructor() { 
    
  }

  ngOnInit() {
    var Item = JSON.parse(window.localStorage.getItem('Object'));
    if(Item != null){
      this.business_name = Item.business_name;
      this.user = Item.user;
    }
    const path = window.location.pathname.split('folder/')[1]
    if (path !== undefined) {
      this.selectedIndex = this.appPages.findIndex(page => page.title.toLowerCase() === path.toLowerCase());
    }
  }

}
