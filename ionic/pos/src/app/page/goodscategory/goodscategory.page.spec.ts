import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { GoodscategoryPage } from './goodscategory.page';

describe('GoodscategoryPage', () => {
  let component: GoodscategoryPage;
  let fixture: ComponentFixture<GoodscategoryPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GoodscategoryPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(GoodscategoryPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
