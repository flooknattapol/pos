import { Routes, Router, RouterEvent } from '@angular/router';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-menu',
  templateUrl: './menu.page.html',
  styleUrls: ['./menu.page.scss'],
})
export class MenuPage implements OnInit {

  page:any;
  seletedpath ="";
  constructor(private router:Router) {
     this.router.events.subscribe((event:RouterEvent) => {
       this.seletedpath = event.url;
      //  this.page = [];
     });
   }

  ngOnInit() {
    this.page = [
      {
        title:'main',
        url:'main'
      },
      {
        title:'receipt',
        url:'receipt'
      }
    ]
    console.log(this.page);
  }

}
