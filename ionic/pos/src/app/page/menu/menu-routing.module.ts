import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { MenuPage } from './menu.page';

const routes: Routes = [
  {
    path: '',
    component: MenuPage,
    children:[
      {
        path: 'main',
        loadChildren:'../page/main/main.module'
      },
      {
        path: 'payment',
        loadChildren:'../page/payment/payment.module'
      },
      {
        path: 'receipt',
        loadChildren:'../page/receipt/receipt.module'
      },
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class MenuPageRoutingModule {}
