import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ReceiptdetailPage } from './receiptdetail.page';

const routes: Routes = [
  {
    path: '',
    component: ReceiptdetailPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ReceiptdetailPageRoutingModule {}
