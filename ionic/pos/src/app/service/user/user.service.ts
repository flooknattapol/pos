import { UtilityService } from './../utility/utility.service';
import { Injectable } from '@angular/core';
@Injectable({
  providedIn: 'root'
})
export class UserService {

  constructor(private base_url:UtilityService) { 

  }
  public Login(JsonData:UserLoginModel){
    console.log(JsonData);
  }
}
export class UserModel{
  ID:any;
  TEL:any;
  IMAGE:any;
  EMAIL:any;
  BUSINESS_ID:any;
  BRANCH:any;
}
export class UserLoginModel{
  EMAIL:any;
  PASSWORD:any;
}
