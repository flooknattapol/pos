import { UserService, UserLoginModel } from './../service/user/user.service';
import { Component, OnInit } from '@angular/core';
import { NavController } from '@ionic/angular';
@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})
export class LoginPage implements OnInit {

  CrateModel:UserLoginModel;
  constructor(private UserService:UserService,
    private navCtrl:NavController
    ) { 
    this.CrateModel = new UserLoginModel();
    this.CrateModel.EMAIL = "";
    this.CrateModel.PASSWORD = "";
    }

  ngOnInit() {

  }
  ngOnLogin(){
    this.navCtrl.navigateRoot('/menu')

    // var bVlid = this.validatecheck();
    // if(bVlid == true){
    //   this.UserService.Login(this.CrateModel);
    // }else{
    //   console.log("ใส่ข้อมูลไม่ครบถ้วน")
    // }
      // .subscribe(arg => this.property = arg);
  }
  ngOnRegister(){
    // this.UserService.Login();
  }
  validatecheck(){
    var bResult = true;
    if(this.CrateModel.EMAIL == ""){
			bResult = false;
    }
    if(this.CrateModel.PASSWORD == ""){
			bResult = false;
    }
    return bResult;
  }
}
