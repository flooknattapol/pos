import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  {
    path: '',
    redirectTo: 'login',
    pathMatch: 'full'
  },
  {
    path: 'login',
    loadChildren: () => import('./login/login.module').then( m => m.LoginPageModule)
  },
  {
    path: 'goods',
    loadChildren: () => import('./page/goods/goods.module').then( m => m.GoodsPageModule)
  },
  {
    path: 'main',
    loadChildren: () => import('./page/main/main.module').then( m => m.MainPageModule)
  },
  {
    path: 'payment',
    loadChildren: () => import('./page/payment/payment.module').then( m => m.PaymentPageModule)
  },
  {
    path: 'receipt',
    loadChildren: () => import('./page/receipt/receipt.module').then( m => m.ReceiptPageModule)
  },
  {
    path: 'receiptdetail',
    loadChildren: () => import('./page/receiptdetail/receiptdetail.module').then( m => m.ReceiptdetailPageModule)
  },
  {
    path: 'goodscategory',
    loadChildren: () => import('./page/goodscategory/goodscategory.module').then( m => m.GoodscategoryPageModule)
  },
  {
    path: 'discount',
    loadChildren: () => import('./page/discount/discount.module').then( m => m.DiscountPageModule)
  },
  {
    path: 'register',
    loadChildren: () => import('./page/register/register.module').then( m => m.RegisterPageModule)
  },
  {
    path: 'menu',
    loadChildren: () => import('./page/menu/menu.module').then( m => m.MenuPageModule)
  }
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }
