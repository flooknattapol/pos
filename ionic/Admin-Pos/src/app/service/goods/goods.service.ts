import { HttpClient } from '@angular/common/http';
import { UtilityService } from './../utility/utility.service';
import { Injectable } from '@angular/core';
@Injectable({
  providedIn: 'root'
})
export class GoodsService {
  urlGet = "getGoodsByAdmin"
  constructor(private http:HttpClient,private apiHelper:UtilityService) { }
  getList(id) {
    var SearchObject =  {
      "business_id":id,
    }
    var myJSON = JSON.stringify(SearchObject); 
    return new Promise((resolve,reject) => 
    this.http.get(this.apiHelper.apiUrl + this.urlGet  + "?JsonData=" + myJSON + "&authen=" + this.apiHelper.apiKey + "&version=" + this.apiHelper.apiVersion, {})
    .subscribe((res) => {
      resolve(res);
    },(err)=>{
      reject(err);
    }))
  }
}
export class GoodsModel{
  ID:number;
  BARCODE:any;
  BUSINESS_ID:any;
  CATEGORYNAME:any;
  CATEGORY_ID:any;
  COMPOSITE:any;
  COST:any;
  DETAIL:any;
  IMAGE:any;
  IsActive:any;
  NAME:any;
  PRICE:any;
  SKU:any;
  SOLDBUY:any;
  STATUS:any;
  STOCK:any;
  UID:any;
  idSession:any;
}