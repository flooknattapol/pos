import { HttpClient } from '@angular/common/http';
import { UtilityService } from './../utility/utility.service';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class TransectionService {
  urlGet = "getListTransection";
  urlGetDetail = "getListDetailTransection";
  constructor(private http:HttpClient,private apiHelper:UtilityService) { }
  getList(id,maxsize:number) {
    var SearchObject =  {
      "PageIndex": 1,
      "PageSize": maxsize,
      "SortColumn": "ID",
      "SortOrder": "DESC",
      "idBusiness":id,
      "mSearch": {
        "NAME": ""
      }
    }
    var myJSON = JSON.stringify(SearchObject); 
    return new Promise((resolve,reject) => 
    this.http.get(this.apiHelper.apiUrl + this.urlGet  + "?JsonData=" + myJSON + "&authen=" + this.apiHelper.apiKey + "&version=" + this.apiHelper.apiVersion, {})
    .subscribe((res) => {
      resolve(res);
    },(err)=>{
      reject(err);
    }))
  }
  getDetail(JsonData) {
    var Item = JSON.parse(window.localStorage.getItem('ObjectDashborad'))
    var SearchObject =  {
      "TRANSECTION_ID":JsonData.ID,
    }
    var myJSON = JSON.stringify(SearchObject); 
    return new Promise((resolve,reject) => 
    this.http.get(this.apiHelper.apiUrl + this.urlGetDetail  + "?JsonData=" + myJSON + "&authen=" + this.apiHelper.apiKey + "&version=" + this.apiHelper.apiVersion, {})
    .subscribe((res) => {
      resolve(res);
    },(err)=>{
      reject(err);
    }))
  }
}
export class TransectionModel{
  ID:any;
  CUSTOMER_ID:any;
  ORDER_NAME:any;
  CATEGORYPAYMENT:any;
  SELLER_ID:any;  
  TimeStamp:any;
  TotalOrder:any;
  TotalCost:any;
  Received	:any;
  Change:any;
  Detail:TransectionDetailModel[];
  Discount:TsDiscountModel;
  IsActive:any;
  BUSINESS_ID:any;
  CUSTOMER_NAME:any;
  ORDERTYPE:any;
  STATUS:any;
  AMOUNT_DISCOUNT:number;
  REFERENCE_ORDER_NAME:any;

}
export class TransectionDetailModel{
  ID:any;
  PRICE:any;
  GOODS_ID:any;
  AMOUNT:any;
  GOODS_NAME:any;
  IsActive:any;
  STATUS:any;

}
export class TsDiscountModel{
  ID:any;
  TRANSECTION_ID:any;
  AMOUNT_DISCOUNT:any;
}