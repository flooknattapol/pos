import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { UtilityService } from '../utility/utility.service';
@Injectable({
  providedIn: 'root'
})
export class BusinessService {
  public urlGet = "getAllBusiness";
  public urlGetCode = "getBusinessCode";
  public urlSaveCode = "saveBusinessCode";
  constructor(private http:HttpClient,private apiHelper:UtilityService) { }
  getList(search,maxsizepage:number) {
    var SearchObject =  {
      "PageIndex": 1,
      "PageSize": maxsizepage,
      "SortColumn": "ID", 
      "SortOrder": "DESC",
      "mSearch": {
        "NAME": search
      }
    }
    var myJSON = JSON.stringify(SearchObject); 
    return new Promise((resolve,reject) => 
    this.http.get(this.apiHelper.apiUrl + this.urlGet  + "?JsonData=" + myJSON + "&authen=" + this.apiHelper.apiKey + "&version=" + this.apiHelper.apiVersion, {})
    .subscribe((res) => {
      resolve(res);
    },(err)=>{
      reject(err);
    }))
  }
  getListCode() {
    var SearchObject =  {
      "PageIndex": 1,
      "PageSize": 1000,
      "SortColumn": "ID",
      "SortOrder": "DESC",
      "mSearch": {
        "NAME": ""
      }
    }
    var myJSON = JSON.stringify(SearchObject); 
    return new Promise((resolve,reject) => 
    this.http.get(this.apiHelper.apiUrl + this.urlGetCode  + "?JsonData=" + myJSON + "&authen=" + this.apiHelper.apiKey + "&version=" + this.apiHelper.apiVersion, {})
    .subscribe((res) => {
      resolve(res);
    },(err)=>{
      reject(err);
    }))
  }
  SaveCode(EXPIRED_DATE) {
    var SearchObject = {
    "EXPIRED_DATE":EXPIRED_DATE
    }
    var myJSON = JSON.stringify(SearchObject); 
    return new Promise((resolve,reject) => 
    this.http.get(this.apiHelper.apiUrl + this.urlSaveCode  + "?JsonData=" + myJSON + "&authen=" + this.apiHelper.apiKey + "&version=" + this.apiHelper.apiVersion, {})
    .subscribe((res) => {
      resolve(res);
    },(err)=>{
      reject(err);
    }))
  }
}
export class BusinessModel{
  ID:number;
  UID:any;
  EMAIL:any;
  NAME:any;
  IMAGE:any;
  CONTACT:any;
  CATEGORY_ID:number;
  CATEGORY_NAME;
  TYPE_ID:number;
  TYPE_NAME:any;
  TAXID10:any;
  TAXID13:any;

}
export class BusinessCodeModel{
  ID:number;
  CODE:number;
  EXPIRED_DATE:any;
  IsActive:number;
}