import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { TabsPage } from './tabs.page';

const routes: Routes = [
  {
    path: 'tabs',
    component: TabsPage,
    children: [
    
      {
        path: 'tab1',
        loadChildren: () => import('../page/listbusinessreceipt/listbusinessreceipt.module').then( m => m.ListbusinessreceiptPageModule)
      },
      {
        path: 'tab2',
        loadChildren: () => import('../page/listbusinessgoods/listbusinessgoods.module').then( m => m.ListbusinessgoodsPageModule)
      },
      {
        path: 'tab3',
        loadChildren: () => import('../page/listbusiness/listbusiness.module').then( m => m.ListbusinessPageModule)
      },
      {
        path: 'tab4',
        loadChildren: () => import('../page/businesscode/businesscode.module').then( m => m.BusinesscodePageModule)
      },
      {
        path: '',
        redirectTo: '/tabs/tab3',
        pathMatch: 'full'
      }
    ]
  },
  {
    path: '',
    redirectTo: '/tabs/tab2',
    pathMatch: 'full'
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class TabsPageRoutingModule {}
