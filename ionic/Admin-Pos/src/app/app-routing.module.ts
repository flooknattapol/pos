import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  {
    path: '',
    loadChildren: () => import('./tabs/tabs.module').then(m => m.TabsPageModule)
  },
  {
    path: 'listgoodsinbusiness',
    loadChildren: () => import('./page/listgoodsinbusiness/listgoodsinbusiness.module').then( m => m.ListgoodsinbusinessPageModule)
  },
  {
    path: 'listbusiness',
    loadChildren: () => import('./page/listbusiness/listbusiness.module').then( m => m.ListbusinessPageModule)
  },
  {
    path: 'businessdetail',
    loadChildren: () => import('./page/businessdetail/businessdetail.module').then( m => m.BusinessdetailPageModule)
  },
  {
    path: 'listbusinessgoods',
    loadChildren: () => import('./page/listbusinessgoods/listbusinessgoods.module').then( m => m.ListbusinessgoodsPageModule)
  },
  {
    path: 'goodsdetailinbusiness',
    loadChildren: () => import('./page/goodsdetailinbusiness/goodsdetailinbusiness.module').then( m => m.GoodsdetailinbusinessPageModule)
  },
  {
    path: 'listreceiptinbusiness',
    loadChildren: () => import('./page/listreceiptinbusiness/listreceiptinbusiness.module').then( m => m.ListreceiptinbusinessPageModule)
  },
  {
    path: 'listbusinessreceipt',
    loadChildren: () => import('./page/listbusinessreceipt/listbusinessreceipt.module').then( m => m.ListbusinessreceiptPageModule)
  },
  {
    path: 'receiptdetailinbusiness',
    loadChildren: () => import('./page/receiptdetailinbusiness/receiptdetailinbusiness.module').then( m => m.ReceiptdetailinbusinessPageModule)
  },
  {
    path: 'businesscode',
    loadChildren: () => import('./page/businesscode/businesscode.module').then( m => m.BusinesscodePageModule)
  },
  {
    path: 'businesscodeadd',
    loadChildren: () => import('./page/businesscodeadd/businesscodeadd.module').then( m => m.BusinesscodeaddPageModule)
  }
];
@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule {}
