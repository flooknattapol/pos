import { UtilityService } from './../../service/utility/utility.service';
import { BusinessModel } from './../../service/business/business.service';
import { NavController } from '@ionic/angular';
import { TransectionService, TransectionModel } from './../../service/transection/transection.service';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute ,NavigationExtras} from "@angular/router";
@Component({
  selector: 'app-listreceiptinbusiness',
  templateUrl: './listreceiptinbusiness.page.html',
  styleUrls: ['./listreceiptinbusiness.page.scss'],
})
export class ListreceiptinbusinessPage {
  ListModel:TransectionModel[];
  Search:string ="";
  CreateModel:BusinessModel;
  PathIMAGE:any;
  maxsizepage:number = 20;

  constructor(private route: ActivatedRoute,private TransectionService:TransectionService,private nav:NavController,private UtilityService:UtilityService) {
    this.route.queryParams.subscribe(params => { 
      this.CreateModel = params['CreateModel'];
      console.log(this.CreateModel)
      this.PathIMAGE = this.UtilityService.ImagePath;
    });
   }

  ionViewWillEnter() {
    this.getList();
  }
  loadMore(event){
    setTimeout(() => {
      this.maxsizepage += 20;
      console.log(event);
      this.getList(event);
      console.log('Done');
      // event.target.complete();
    }, 500);
  }
  getList(event?){
    var data = this.TransectionService.getList(this.CreateModel.ID,this.maxsizepage).then((result: any)=> { 
      console.log(result);
      if(result.status == true){
        this.ListModel = new Array;
        this.ListModel = result.message;
        if(event){
          event.target.complete();
        }
      }else{
        alert(result.message)
      }
    })
  }
  onDetail(item){
    let navigationExtras: NavigationExtras = {
      queryParams: {
        item:item,
      }
    };
    this.nav.navigateForward('/receiptdetailinbusiness',navigationExtras);
  }

}
