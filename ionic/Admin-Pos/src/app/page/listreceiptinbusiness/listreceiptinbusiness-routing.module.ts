import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ListreceiptinbusinessPage } from './listreceiptinbusiness.page';

const routes: Routes = [
  {
    path: '',
    component: ListreceiptinbusinessPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ListreceiptinbusinessPageRoutingModule {}
