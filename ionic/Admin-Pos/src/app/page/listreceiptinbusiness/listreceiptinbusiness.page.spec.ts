import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { ListreceiptinbusinessPage } from './listreceiptinbusiness.page';

describe('ListreceiptinbusinessPage', () => {
  let component: ListreceiptinbusinessPage;
  let fixture: ComponentFixture<ListreceiptinbusinessPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ListreceiptinbusinessPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(ListreceiptinbusinessPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
