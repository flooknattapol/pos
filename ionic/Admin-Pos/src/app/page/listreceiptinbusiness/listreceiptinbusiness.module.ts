import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { ListreceiptinbusinessPageRoutingModule } from './listreceiptinbusiness-routing.module';

import { ListreceiptinbusinessPage } from './listreceiptinbusiness.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ListreceiptinbusinessPageRoutingModule
  ],
  declarations: [ListreceiptinbusinessPage]
})
export class ListreceiptinbusinessPageModule {}
