import { UtilityService } from './../../service/utility/utility.service';
import { GoodsService, GoodsModel } from './../../service/goods/goods.service';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute ,NavigationExtras} from "@angular/router";
import { BusinessService, BusinessModel } from './../../service/business/business.service';
import { NavController } from '@ionic/angular';

@Component({
  selector: 'app-listgoodsinbusiness',
  templateUrl: './listgoodsinbusiness.page.html',
  styleUrls: ['./listgoodsinbusiness.page.scss'],
})
export class ListgoodsinbusinessPage{
  CreateModel:BusinessModel;
  ModelList:GoodsModel[];
  PathIMAGE:any;
  constructor(private route: ActivatedRoute,private GoodsService:GoodsService,private UtilityService:UtilityService,private nav:NavController) {
    this.route.queryParams.subscribe(params => { 
      this.CreateModel = params['CreateModel'];
      console.log(this.CreateModel)
      this.PathIMAGE = this.UtilityService.ImagePath;
    });
   }

  ionViewWillEnter() {
    this.getList();
  }
  getList(){
    var data = this.GoodsService.getList(this.CreateModel.ID).then((result: any)=> { 
      console.log(result);
      if(result.status == true){
        this.ModelList = new Array;
        this.ModelList = result.message;
      }else{
        alert(result.message)
      }
    })
  } 
  onDetailPage(item){
    let navigationExtras: NavigationExtras = {
      queryParams: {
          CreateModel: item,
      }
    };    
    this.nav.navigateForward('/goodsdetailinbusiness',navigationExtras);
  }
}
