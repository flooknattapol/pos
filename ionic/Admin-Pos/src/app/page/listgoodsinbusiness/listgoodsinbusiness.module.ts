import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { ListgoodsinbusinessPageRoutingModule } from './listgoodsinbusiness-routing.module';

import { ListgoodsinbusinessPage } from './listgoodsinbusiness.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ListgoodsinbusinessPageRoutingModule
  ],
  declarations: [ListgoodsinbusinessPage]
})
export class ListgoodsinbusinessPageModule {}
