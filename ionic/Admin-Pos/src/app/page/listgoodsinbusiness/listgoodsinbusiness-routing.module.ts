import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ListgoodsinbusinessPage } from './listgoodsinbusiness.page';

const routes: Routes = [
  {
    path: '',
    component: ListgoodsinbusinessPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ListgoodsinbusinessPageRoutingModule {}
