import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { ListgoodsinbusinessPage } from './listgoodsinbusiness.page';

describe('ListgoodsinbusinessPage', () => {
  let component: ListgoodsinbusinessPage;
  let fixture: ComponentFixture<ListgoodsinbusinessPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ListgoodsinbusinessPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(ListgoodsinbusinessPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
