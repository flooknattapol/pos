import { TransectionService, TransectionModel, TransectionDetailModel } from './../../service/transection/transection.service';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute ,NavigationExtras} from "@angular/router";
@Component({
  selector: 'app-receiptdetailinbusiness',
  templateUrl: './receiptdetailinbusiness.page.html',
  styleUrls: ['./receiptdetailinbusiness.page.scss'],
})
export class ReceiptdetailinbusinessPage implements OnInit {
  ListModel:TransectionModel;
  DetailModel:TransectionDetailModel[];
  constructor(private TransectionService:TransectionService,private route: ActivatedRoute) { 
    this.route.queryParams.subscribe(params => {
      this.ListModel = params['item'];
      this.getList(this.ListModel);

  });
  }

  ngOnInit() {
  }
  getList(item){
    var data = this.TransectionService.getDetail(item).then((result: any)=> { 
      console.log(result);
      if(result.status == true){
        this.DetailModel = new Array;
        this.DetailModel = result.message;
      }else{
        alert(result.message)
      }
    })
    }
}
