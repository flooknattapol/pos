import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { ReceiptdetailinbusinessPage } from './receiptdetailinbusiness.page';

describe('ReceiptdetailinbusinessPage', () => {
  let component: ReceiptdetailinbusinessPage;
  let fixture: ComponentFixture<ReceiptdetailinbusinessPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ReceiptdetailinbusinessPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(ReceiptdetailinbusinessPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
