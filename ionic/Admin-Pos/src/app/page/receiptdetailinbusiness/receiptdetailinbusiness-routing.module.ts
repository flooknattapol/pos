import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ReceiptdetailinbusinessPage } from './receiptdetailinbusiness.page';

const routes: Routes = [
  {
    path: '',
    component: ReceiptdetailinbusinessPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ReceiptdetailinbusinessPageRoutingModule {}
