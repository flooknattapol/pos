import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { ReceiptdetailinbusinessPageRoutingModule } from './receiptdetailinbusiness-routing.module';

import { ReceiptdetailinbusinessPage } from './receiptdetailinbusiness.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ReceiptdetailinbusinessPageRoutingModule
  ],
  declarations: [ReceiptdetailinbusinessPage]
})
export class ReceiptdetailinbusinessPageModule {}
