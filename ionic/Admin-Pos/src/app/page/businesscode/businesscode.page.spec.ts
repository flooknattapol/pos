import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { BusinesscodePage } from './businesscode.page';

describe('BusinesscodePage', () => {
  let component: BusinesscodePage;
  let fixture: ComponentFixture<BusinesscodePage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BusinesscodePage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(BusinesscodePage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
