import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { BusinesscodePageRoutingModule } from './businesscode-routing.module';

import { BusinesscodePage } from './businesscode.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    BusinesscodePageRoutingModule
  ],
  declarations: [BusinesscodePage]
})
export class BusinesscodePageModule {}
