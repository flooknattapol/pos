import { NavController } from '@ionic/angular';
import { BusinessService, BusinessCodeModel } from './../../service/business/business.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-businesscode',
  templateUrl: './businesscode.page.html',
  styleUrls: ['./businesscode.page.scss'],
})
export class BusinesscodePage {
  ModelList:BusinessCodeModel[];
  constructor(private BusinessService: BusinessService,private nav:NavController) {
    
   }

  ionViewWillEnter() {
    this.getList();
  }
  getList(){
    var data = this.BusinessService.getListCode().then((result: any)=> { 
      console.log(result);
      if(result.status == true){
        this.ModelList = new Array;
        this.ModelList = result.message;
      }else{
        alert(result.message)
      }
    })
  }
  onAddPage(){
    this.nav.navigateForward('/businesscodeadd');
  }
  
}
