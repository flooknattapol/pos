import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { BusinesscodePage } from './businesscode.page';

const routes: Routes = [
  {
    path: '',
    component: BusinesscodePage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class BusinesscodePageRoutingModule {}
