import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { ListbusinessPageRoutingModule } from './listbusiness-routing.module';

import { ListbusinessPage } from './listbusiness.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ListbusinessPageRoutingModule
  ],
  declarations: [ListbusinessPage]
})
export class ListbusinessPageModule {}
