import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ListbusinessPage } from './listbusiness.page';

const routes: Routes = [
  {
    path: '',
    component: ListbusinessPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ListbusinessPageRoutingModule {}
