import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { ListbusinessPage } from './listbusiness.page';

describe('ListbusinessPage', () => {
  let component: ListbusinessPage;
  let fixture: ComponentFixture<ListbusinessPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ListbusinessPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(ListbusinessPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
