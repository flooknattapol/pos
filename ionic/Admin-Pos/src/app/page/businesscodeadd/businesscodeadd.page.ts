import { NavController } from '@ionic/angular';
import { BusinessService } from './../../service/business/business.service';
import { Component, OnInit } from '@angular/core';
import { DatePicker } from '@ionic-native/date-picker/ngx';

@Component({
  selector: 'app-businesscodeadd',
  templateUrl: './businesscodeadd.page.html',
  styleUrls: ['./businesscodeadd.page.scss'],
})
export class BusinesscodeaddPage implements OnInit {
  EXPIRED_DATE:any;
  constructor(private datePicker: DatePicker,private BusinessService:BusinessService,private nav:NavController) { 
    let current_datetime = new Date()
    this.EXPIRED_DATE = current_datetime.getFullYear() + "-" + (current_datetime.getMonth() + 2  ) + "-" + current_datetime.getDate()

  }

  ngOnInit() {
  }
  selectdate(){
    this.datePicker.show({
      date: new Date(),
      mode: 'date',
      androidTheme: this.datePicker.ANDROID_THEMES.THEME_HOLO_DARK
    }).then(
      date =>{
          console.log('Got date: ', date)
         this.EXPIRED_DATE = date.getFullYear() + "-" + (date.getMonth() + 1) + "-" + date.getDate();
      } ,
      err => console.log('Error occurred while getting date: ', err)
    );
  }
  onSave(){
    var data = this.BusinessService.SaveCode(this.EXPIRED_DATE).then((result: any)=> { 
      if(result.status == true){
        this.nav.navigateRoot('/businesscode');
      }else{
        alert(result.message)
      }
    })
  }
}
