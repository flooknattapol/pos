import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { BusinesscodeaddPage } from './businesscodeadd.page';

const routes: Routes = [
  {
    path: '',
    component: BusinesscodeaddPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class BusinesscodeaddPageRoutingModule {}
