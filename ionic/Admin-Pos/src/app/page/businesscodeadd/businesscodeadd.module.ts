import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { BusinesscodeaddPageRoutingModule } from './businesscodeadd-routing.module';

import { BusinesscodeaddPage } from './businesscodeadd.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    BusinesscodeaddPageRoutingModule
  ],
  declarations: [BusinesscodeaddPage]
})
export class BusinesscodeaddPageModule {}
