import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { BusinesscodeaddPage } from './businesscodeadd.page';

describe('BusinesscodeaddPage', () => {
  let component: BusinesscodeaddPage;
  let fixture: ComponentFixture<BusinesscodeaddPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BusinesscodeaddPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(BusinesscodeaddPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
