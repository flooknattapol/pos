import { Component, OnInit } from '@angular/core';
import { NavController } from '@ionic/angular';
import { ActivatedRoute ,NavigationExtras} from "@angular/router";
import { UtilityService } from './../../service/utility/utility.service';
import { BusinessService, BusinessModel } from './../../service/business/business.service';


@Component({
  selector: 'app-listbusinessreceipt',
  templateUrl: './listbusinessreceipt.page.html',
  styleUrls: ['./listbusinessreceipt.page.scss'],
})
export class ListbusinessreceiptPage implements OnInit {
  ModelList:BusinessModel[];
  pathimg:string = "";
  search:any = "";
  maxsizepage:number = 20;
  constructor(private BusinessService:BusinessService
    ,private nav:NavController
    ,private UtilityService:UtilityService) {
      this.pathimg = this.UtilityService.ImagePath;
     }

  ngOnInit() {
  }

  ionViewWillEnter() {
    this.getList();
  }
  loadMore(event){
    setTimeout(() => {
      this.maxsizepage += 20;
      console.log(event);
      this.getList(event);
      console.log('Done');
      // event.target.complete();
    }, 500);
  }
  getList(event?){
    var data = this.BusinessService.getList(this.search,this.maxsizepage).then((result: any)=> { 
      console.log(result);
      if(result.status == true){
        this.ModelList = new Array;
        this.ModelList = result.message;
        if(event){
          event.target.complete();
        }
      }else{
        alert(result.message)
      }
    })
  }
  getDetail(item){
    let navigationExtras: NavigationExtras = {
      queryParams: {
          CreateModel: item,
      }
    }; 
    this.nav.navigateForward('/listreceiptinbusiness',navigationExtras);
  }
}
