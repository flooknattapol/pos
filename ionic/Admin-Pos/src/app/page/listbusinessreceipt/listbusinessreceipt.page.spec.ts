import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { ListbusinessreceiptPage } from './listbusinessreceipt.page';

describe('ListbusinessreceiptPage', () => {
  let component: ListbusinessreceiptPage;
  let fixture: ComponentFixture<ListbusinessreceiptPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ListbusinessreceiptPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(ListbusinessreceiptPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
