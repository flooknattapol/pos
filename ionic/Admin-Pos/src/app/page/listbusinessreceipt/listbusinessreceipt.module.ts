import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { ListbusinessreceiptPageRoutingModule } from './listbusinessreceipt-routing.module';

import { ListbusinessreceiptPage } from './listbusinessreceipt.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ListbusinessreceiptPageRoutingModule
  ],
  declarations: [ListbusinessreceiptPage]
})
export class ListbusinessreceiptPageModule {}
