import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ListbusinessreceiptPage } from './listbusinessreceipt.page';

const routes: Routes = [
  {
    path: '',
    component: ListbusinessreceiptPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ListbusinessreceiptPageRoutingModule {}
