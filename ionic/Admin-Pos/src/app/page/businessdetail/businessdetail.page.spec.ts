import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { BusinessdetailPage } from './businessdetail.page';

describe('BusinessdetailPage', () => {
  let component: BusinessdetailPage;
  let fixture: ComponentFixture<BusinessdetailPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BusinessdetailPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(BusinessdetailPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
