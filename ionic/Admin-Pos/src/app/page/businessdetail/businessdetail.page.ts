import { Component, OnInit } from '@angular/core';
import { ActivatedRoute ,NavigationExtras} from "@angular/router";
import { UtilityService } from './../../service/utility/utility.service';
import { BusinessService, BusinessModel } from './../../service/business/business.service';
import { NavController } from '@ionic/angular';

@Component({
  selector: 'app-businessdetail',
  templateUrl: './businessdetail.page.html',
  styleUrls: ['./businessdetail.page.scss'],
})
export class BusinessdetailPage implements OnInit {
  CreateModel:BusinessModel;
  PathIMAGE:string="";
  constructor(private BusinessService:BusinessService
    ,private nav:NavController
    ,private route: ActivatedRoute
    ,private UtilityService:UtilityService) {
      this.route.queryParams.subscribe(params => { 
        this.CreateModel = params['CreateModel'];
        console.log(this.CreateModel)
      });
      this.PathIMAGE = this.UtilityService.ImagePath;
     }

  ngOnInit() {
  }

}
