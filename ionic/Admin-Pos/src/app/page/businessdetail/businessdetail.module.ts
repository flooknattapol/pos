import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { BusinessdetailPageRoutingModule } from './businessdetail-routing.module';

import { BusinessdetailPage } from './businessdetail.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    BusinessdetailPageRoutingModule
  ],
  declarations: [BusinessdetailPage]
})
export class BusinessdetailPageModule {}
