import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { BusinessdetailPage } from './businessdetail.page';

const routes: Routes = [
  {
    path: '',
    component: BusinessdetailPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class BusinessdetailPageRoutingModule {}
