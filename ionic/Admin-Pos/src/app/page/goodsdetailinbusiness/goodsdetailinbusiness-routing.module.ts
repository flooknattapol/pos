import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { GoodsdetailinbusinessPage } from './goodsdetailinbusiness.page';

const routes: Routes = [
  {
    path: '',
    component: GoodsdetailinbusinessPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class GoodsdetailinbusinessPageRoutingModule {}
