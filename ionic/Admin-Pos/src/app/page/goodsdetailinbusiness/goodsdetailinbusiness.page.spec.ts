import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { GoodsdetailinbusinessPage } from './goodsdetailinbusiness.page';

describe('GoodsdetailinbusinessPage', () => {
  let component: GoodsdetailinbusinessPage;
  let fixture: ComponentFixture<GoodsdetailinbusinessPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GoodsdetailinbusinessPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(GoodsdetailinbusinessPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
