import { Component, OnInit } from '@angular/core';
import { GoodsModel } from 'src/app/service/goods/goods.service';
import { ActivatedRoute ,NavigationExtras} from "@angular/router";
import { UtilityService } from './../../service/utility/utility.service';

@Component({
  selector: 'app-goodsdetailinbusiness',
  templateUrl: './goodsdetailinbusiness.page.html',
  styleUrls: ['./goodsdetailinbusiness.page.scss'],
})
export class GoodsdetailinbusinessPage implements OnInit {
  CreateModel:GoodsModel;
  PathIMAGE:any;
  constructor(private route: ActivatedRoute,private UtilityService:UtilityService) {
    this.route.queryParams.subscribe(params => { 
      this.CreateModel = params['CreateModel'];
      console.log(this.CreateModel)
    });
    this.PathIMAGE = this.UtilityService.ImagePath;
   }

  ngOnInit() {
  }

}
