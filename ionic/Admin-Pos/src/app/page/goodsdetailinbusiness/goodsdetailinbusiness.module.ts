import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { GoodsdetailinbusinessPageRoutingModule } from './goodsdetailinbusiness-routing.module';

import { GoodsdetailinbusinessPage } from './goodsdetailinbusiness.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    GoodsdetailinbusinessPageRoutingModule
  ],
  declarations: [GoodsdetailinbusinessPage]
})
export class GoodsdetailinbusinessPageModule {}
