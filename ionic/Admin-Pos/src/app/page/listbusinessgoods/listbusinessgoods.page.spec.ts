import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { ListbusinessgoodsPage } from './listbusinessgoods.page';

describe('ListbusinessgoodsPage', () => {
  let component: ListbusinessgoodsPage;
  let fixture: ComponentFixture<ListbusinessgoodsPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ListbusinessgoodsPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(ListbusinessgoodsPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
