import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ListbusinessgoodsPage } from './listbusinessgoods.page';

const routes: Routes = [
  {
    path: '',
    component: ListbusinessgoodsPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ListbusinessgoodsPageRoutingModule {}
