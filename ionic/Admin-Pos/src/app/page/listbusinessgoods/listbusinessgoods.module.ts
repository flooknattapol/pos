import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { ListbusinessgoodsPageRoutingModule } from './listbusinessgoods-routing.module';

import { ListbusinessgoodsPage } from './listbusinessgoods.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ListbusinessgoodsPageRoutingModule
  ],
  declarations: [ListbusinessgoodsPage]
})
export class ListbusinessgoodsPageModule {}
