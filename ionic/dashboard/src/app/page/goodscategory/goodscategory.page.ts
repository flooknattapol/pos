import { NavController } from '@ionic/angular';
import { GoodsService, GoodsComboModel } from './../../service/goods/goods.service';
import { UtilityService } from './../../service/utility/utility.service';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute ,NavigationExtras} from "@angular/router";

@Component({
  selector: 'app-goodscategory',
  templateUrl: './goodscategory.page.html',
  styleUrls: ['./goodscategory.page.scss'],
})
export class GoodscategoryPage {
  ModelList:GoodsComboModel[];
  PathIMAGE:any;
  CreateModel:GoodsComboModel;
  constructor(private UtilityService:UtilityService,private GoodsService:GoodsService
    ,private nav:NavController,private route: ActivatedRoute) { 
    this.PathIMAGE = this.UtilityService.ImagePath;
  }

  ionViewWillEnter() {
    this.getGoodscategory();
  }
 getGoodscategory(){
    var data = this.GoodsService.getComboList().then((result: any)=> { 
      if(result.status == true){
        console.log(result);
        this.ModelList = new Array;
        this.ModelList = result.message;
        console.log(this.ModelList);
      }else{
        alert(result.message)
      }
    })
  }
  addEditPage(item){
    let navigationExtras: NavigationExtras = {
      queryParams: {
          CreateModel:item,
      }
    };  
    this.nav.navigateForward('/goodscategoryadd',navigationExtras);
  }
  addPage(){
    this.CreateModel = new GoodsComboModel;
    let navigationExtras: NavigationExtras = {
      queryParams: {
          CreateModel:this.CreateModel
      }
    };  
    this.nav.navigateForward('/goodscategoryadd',navigationExtras);
  }
}
