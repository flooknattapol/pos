import { DiscountService,DiscountModel } from './../../service/discount/discount.service';
import { NavController } from '@ionic/angular';
import { UtilityService } from './../../service/utility/utility.service';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute ,NavigationExtras} from "@angular/router";

@Component({
  selector: 'app-voucher',
  templateUrl: './voucher.page.html',
  styleUrls: ['./voucher.page.scss'],
})
export class VoucherPage {
  PathIMAGE:any;
  maxsizepage:number = 20;
  ModelList:DiscountModel[];
  constructor(private UtilityService:UtilityService , private nav:NavController,private DiscountService:DiscountService) {
    this.PathIMAGE = this.UtilityService.ImagePath;
   }

   ionViewWillEnter() {
    this.getList();
  }
  getList(event?){
    var data = this.DiscountService.getListCampaign(this.maxsizepage).then((result: any)=> { 
      if(result.status == true){
        this.ModelList = new Array;
        this.ModelList = result.message;
        this.ModelList.forEach(function (entry, index) {
          if(entry.STATUS == 1){
            entry.STATUS = true;
          }else{
            entry.STATUS = false;
          }
          var datenow = new Date();
          entry.disabledtogle = false;
          if(new Date(entry.DATE_START_CAMPAIGN)  <= datenow){
            entry.disabledtogle  = true;
          }
        });
        if(event){
          event.target.complete();
        }
      }else{
        alert(result.message)
      }
    })
  }
  editSTATUS(item){
  
    if(item.STATUS == true){
      item.STATUS = 1;
    }else{
      item.STATUS = 0;
    }   
    var userlocal = JSON.parse(window.localStorage.getItem('ObjectDashborad'))
    item.idSession = userlocal.id;
      var data = this.DiscountService.save(item).then((result: any)=> { 
      console.log(result);
      if(result.status == true){
        this.nav.pop();
      }else{
        alert(result.message)
      }
    })
    
        
    
  }
 
  loadMore(event){
    setTimeout(() => {
      this.maxsizepage += 20;
      console.log(event);
      this.getList(event);
      console.log('Done');
      // event.target.complete();
    }, 500);
  }
  onAddPage(){
    this.nav.navigateForward('/voucheradd');
  }
  onDetailPage(item){
    let navigationExtras: NavigationExtras = {
      queryParams: {
          CreateModel: item,
      }
    };    
    this.nav.navigateForward('/voucherdetail',navigationExtras);
  }
}
