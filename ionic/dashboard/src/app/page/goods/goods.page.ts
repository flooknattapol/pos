import { Component, OnInit } from '@angular/core';
import { UtilityService } from './../../service/utility/utility.service';
import { GoodsService, GoodsModel, GoodsComboModel } from './../../service/goods/goods.service';
import { ActivatedRoute ,NavigationExtras} from "@angular/router";
import { NavController } from '@ionic/angular';

@Component({
  selector: 'app-goods',
  templateUrl: './goods.page.html',
  styleUrls: ['./goods.page.scss'],
})
export class GoodsPage implements OnInit {

  public ModelList:GoodsModel[];
  public ModelSeach:GoodsModel[];
  GoodsComboList:GoodsComboModel;
  PathIMAGE:any;
  maxsizepage:number = 20;

  constructor(private GoodsService:GoodsService,private UtilityService:UtilityService,
    private nav:NavController,private route: ActivatedRoute) { 
    this.PathIMAGE = this.UtilityService.ImagePath;
  }

  ngOnInit() {
  }
  ionViewWillEnter() {
    this.getGoods();
    this.getGoodscategory();
  }
  loadMore(event){
    setTimeout(() => {
      this.maxsizepage += 20;
      console.log(event);
      this.getGoods(event);
      console.log('Done');
      // event.target.complete();
    }, 500);
  }
  getGoods(event?){
    var data = this.GoodsService.getList(this.maxsizepage).then((result: any)=> { 
      if(result.status == true){
        this.ModelList = new Array;
        this.ModelSeach = new Array;
        this.ModelList = result.message;
        this.ModelSeach = result.message;
        if(event){
          event.target.complete();
        }
      }else{
        alert(result.message)
      }
    })
  }
  getGoodscategory(){
    var data = this.GoodsService.getComboList().then((result: any)=> { 
      if(result.status == true){
        console.log(result);
        this.GoodsComboList = new GoodsComboModel;
        this.GoodsComboList = result.message;
        console.log(this.GoodsComboList);
      }else{
        alert(result.message)
      }
    })
  }
  onFilterList(event){
      var GoodsFilter = new Array;
      this.ModelSeach.forEach(function (entry, index) {
        if(entry.CATEGORY_ID == event.detail.value){
          GoodsFilter.push(entry)
        }
      });
      console.log(GoodsFilter);
      this.ModelList = [];
      this.ModelList = GoodsFilter;
    
  }
  onAddPage(){
    this.ModelList = new Array;
    let navigationExtras: NavigationExtras = {
      queryParams: {
          CreateModel: this.ModelList,
      }
    };    
    this.nav.navigateForward('/goodsadd',navigationExtras);
  }
  onDetailPage(item:GoodsModel){
    let navigationExtras: NavigationExtras = {
      queryParams: {
          CreateModel: item,
      }
    };    
    this.nav.navigateForward('/goodsdetail',navigationExtras);
  }
}
