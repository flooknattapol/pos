import { NavController } from '@ionic/angular';
import { DiscountService, DiscountModel } from './../../service/discount/discount.service';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute ,NavigationExtras} from "@angular/router";

@Component({
  selector: 'app-discount',
  templateUrl: './discount.page.html',
  styleUrls: ['./discount.page.scss'],
})

export class DiscountPage {
  maxsizepage:number = 20;
  ModelList:DiscountModel[];
  CreateModel:DiscountModel;
  constructor(private DiscountService:DiscountService
    ,private nav:NavController,private route: ActivatedRoute) { }

  ionViewWillEnter() {
    this.getDiscount();
  }
  loadMore(event){
    setTimeout(() => {
      this.maxsizepage += 20;
      console.log(event);
      this.getDiscount(event);
      console.log('Done');
      // event.target.complete();
    }, 500);
  }
  getDiscount(event?){
    var data = this.DiscountService.getList(this.maxsizepage).then((result: any)=> { 
      if(result.status == true){
        this.ModelList = new Array;
        var res = [];
        var item = [];
        res = result.message;
        res.forEach(function (entry, index) {
          if(entry.TYPE_ID == 1){
            item.push(entry);
          }
        });
        this.ModelList = item;
        if(event){
          event.target.complete();
        }
      }else{
        alert(result.message)
      }
    })
  }
  addPage(){
    this.CreateModel = new DiscountModel;
    let navigationExtras: NavigationExtras = {
      queryParams: {
          CreateModel:this.CreateModel
      }
    };  
    this.nav.navigateForward('/discountadd',navigationExtras);
  }
  addEditPage(item){
    let navigationExtras: NavigationExtras = {
      queryParams: {
          CreateModel:item,
      }
    };  
    this.nav.navigateForward('/discountadd',navigationExtras);
  }
}
