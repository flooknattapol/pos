import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { EmployeedetailPage } from './employeedetail.page';

const routes: Routes = [
  {
    path: '',
    component: EmployeedetailPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class EmployeedetailPageRoutingModule {}
