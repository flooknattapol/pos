import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { EmployeedetailPageRoutingModule } from './employeedetail-routing.module';

import { EmployeedetailPage } from './employeedetail.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    EmployeedetailPageRoutingModule
  ],
  declarations: [EmployeedetailPage]
})
export class EmployeedetailPageModule {}
