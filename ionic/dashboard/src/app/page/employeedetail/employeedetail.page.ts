import { ListModel } from './../../service/employee/employee.service';
import { NavController } from '@ionic/angular';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute ,NavigationExtras} from "@angular/router";
import { UtilityService } from './../../service/utility/utility.service';
@Component({
  selector: 'app-employeedetail',
  templateUrl: './employeedetail.page.html',
  styleUrls: ['./employeedetail.page.scss'],
})
export class EmployeedetailPage implements OnInit {
  CreateModel:ListModel;
  PathIMAGE:any;
  constructor(private nav:NavController,private route: ActivatedRoute,private UtilityService:UtilityService) {
    this.route.queryParams.subscribe(params => { 
      this.CreateModel = params['CreateModel'];
      console.log(this.CreateModel)
    });
    this.PathIMAGE = this.UtilityService.ImagePath;
   }

  ngOnInit() {
  }
  onAddPage(){
    let navigationExtras: NavigationExtras = {
      queryParams: {
          CreateModel: this.CreateModel,
      }
    };    
    this.nav.navigateForward('/employeeadd',navigationExtras);
  }
}
