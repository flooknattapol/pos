import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { GoodsaddPage } from './goodsadd.page';

const routes: Routes = [
  {
    path: '',
    component: GoodsaddPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class GoodsaddPageRoutingModule {}
