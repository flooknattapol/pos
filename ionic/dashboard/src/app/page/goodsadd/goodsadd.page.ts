import { UtilityService } from './../../service/utility/utility.service';
import { GoodsModel, GoodsService, GoodsComboModel } from './../../service/goods/goods.service';
import { NavController } from '@ionic/angular';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute ,NavigationExtras} from "@angular/router";
import { Camera,CameraOptions } from '@ionic-native/camera/ngx';
import { FileTransfer, FileUploadOptions, FileTransferObject } from '@ionic-native/file-transfer/ngx';

@Component({
  selector: 'app-goodsadd',
  templateUrl: './goodsadd.page.html',
  styleUrls: ['./goodsadd.page.scss'],
})
export class GoodsaddPage implements OnInit {
  CreateModel:GoodsModel;
  base64img:string='';
  PathIMAGE:string="";
  PathRoot:string="";
  GoodsComboList:GoodsComboModel;
  constructor(private nav:NavController,private route: ActivatedRoute,private camera: Camera
    ,private transfer: FileTransfer,private GoodsService:GoodsService
    ,private UtilityService:UtilityService) {
    this.route.queryParams.subscribe(params => { 
      console.log(params);
      this.CreateModel = params['CreateModel'];

    });
      this.PathIMAGE = this.UtilityService.ImagePath;
      this.PathRoot = this.UtilityService.apiRoot;
   }

  ngOnInit() {
    console.log(this.CreateModel);
    this.getGoodscategory();
  }
  getGoodscategory(){
    var data = this.GoodsService.getComboList().then((result: any)=> { 
      if(result.status == true){
        console.log(result);
        this.GoodsComboList = new GoodsComboModel;
        this.GoodsComboList = result.message;
        console.log(this.GoodsComboList);
      }else{
        alert(result.message)
      }
    })
  }

  imageCapturedGallery(){
    console.log("image");
    const options:CameraOptions={
      quality:70,
      destinationType:this.camera.DestinationType.DATA_URL,
      sourceType:this.camera.PictureSourceType.PHOTOLIBRARY,
      saveToPhotoAlbum:false,
      correctOrientation:true
    }
    this.camera.getPicture(options).then((ImageData=>{
       this.base64img="data:image/jpeg;base64,"+ImageData;
    }),error=>{
      console.log(error);
    })
  }
  clear(){
    this.base64img='';
  }
  upload() {
    console.log("beforetest");
    const fileTransfer: FileTransferObject = this.transfer.create();
    var dateobj = Date.now();
    var datestr = dateobj.toString(); 
    var filename = 'goods'+datestr+'.jpg'
    console.log(filename);
    let options: FileUploadOptions = {
      fileKey: "photo",
      fileName: filename,
      chunkedMode: false,
      mimeType: "image/jpeg",
      headers: {}
    }
    if(this.base64img != ""){
      fileTransfer.upload(this.base64img,this.PathRoot + '/imageUploadTransfer.php', options).then(data => {
        this.OnSave(filename);
      }, error => {
        alert("error");
        alert("error" + JSON.stringify(error));
      });
    }else if(this.CreateModel.IMAGE != ""){
      this.OnSave(filename);
    }else{ 
      alert("กรุณาเลือกรูปภาพ");
      // this.OnSave(filename);
    }
  }
  OnSave(filename){
    var bValid = true;
    if(this.base64img != ""){
      this.CreateModel.IMAGE = filename;
    }
    if(this.CreateModel.NAME == "" || this.CreateModel.NAME == null  || this.CreateModel.NAME == undefined){
      bValid = false;
    }
    if(this.CreateModel.PRICE == "" && this.CreateModel.PRICE == null || this.CreateModel.PRICE == undefined){
      bValid = false;
    }
    // if(this.CreateModel.SKU == "" && this.CreateModel.SKU == null || this.CreateModel.SKU == undefined){
    //   bValid = false;
    // }
    if(this.CreateModel.COST == "" && this.CreateModel.COST == null || this.CreateModel.COST == undefined){
      bValid = false;
    }
    if(this.CreateModel.CATEGORY_ID == "" && this.CreateModel.CATEGORY_ID == null || this.CreateModel.CATEGORY_ID == undefined){
      bValid = false;
    }
    if(this.CreateModel.IMAGE == "" && this.CreateModel.IMAGE == null || this.CreateModel.IMAGE == undefined){
      bValid = false;
    }
    if(this.CreateModel.STATUS == "" && this.CreateModel.STATUS == null || this.CreateModel.STATUS == undefined){
      bValid = false;
    }
    if(bValid == true){
      var Item = JSON.parse(window.localStorage.getItem('ObjectDashborad'))
      this.CreateModel.idSession = Item.id;
     
      var data = this.GoodsService.save(this.CreateModel).then((result: any)=> { 
        console.log(result);
        if(result.status == true){
          this.nav.pop();
        }else{
          alert(result.message)
        }
      })
    }else{
      alert("กรุณาใส่ข้อมูลให้ครบถ้วน");
    }
    
  }
}
