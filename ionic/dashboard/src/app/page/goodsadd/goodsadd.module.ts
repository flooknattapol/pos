import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { GoodsaddPageRoutingModule } from './goodsadd-routing.module';

import { GoodsaddPage } from './goodsadd.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    GoodsaddPageRoutingModule
  ],
  declarations: [GoodsaddPage]
})
export class GoodsaddPageModule {}
