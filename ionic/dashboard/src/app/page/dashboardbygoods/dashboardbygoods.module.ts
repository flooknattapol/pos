import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { DashboardbygoodsPageRoutingModule } from './dashboardbygoods-routing.module';

import { DashboardbygoodsPage } from './dashboardbygoods.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    DashboardbygoodsPageRoutingModule
  ],
  declarations: [DashboardbygoodsPage]
})
export class DashboardbygoodsPageModule {}
