import { DashboardbygoodsService, HeaderModel, ListGoodsModel } from './../../service/dashboardbygoods/dashboardbygoods.service';
import { DashboardService, ListModel } from './../../service/dashboard/dashboard.service';
import { Component, OnInit } from '@angular/core';
import { Chart } from 'chart.js';
import { DatePicker } from '@ionic-native/date-picker/ngx';

@Component({
  selector: 'app-dashboardbygoods',
  templateUrl: './dashboardbygoods.page.html',
  styleUrls: ['./dashboardbygoods.page.scss'],
})
export class DashboardbygoodsPage implements OnInit {
  ListModel:ListModel[];
  HeaderModel:HeaderModel[];
  ListGoodsModel:ListGoodsModel[];
  DateStart:any;
  DateEnd:any;
  maxsizepage:number = 20;
  constructor(private datePicker: DatePicker,private DashboardService:DashboardService,private DashboardbygoodsService:DashboardbygoodsService) {
    this.ListModel = new Array();
    this.HeaderModel = new Array();
    this.ListGoodsModel = new Array();
    let current_datetime = new Date()
    this.DateStart = current_datetime.getFullYear() + "-" + (current_datetime.getMonth()) + "-" + current_datetime.getDate()
    this.DateEnd = current_datetime.getFullYear() + "-" + (current_datetime.getMonth() + 1) + "-" + current_datetime.getDate()
   }

  ngOnInit() {
    this.mycharts();
    this.Heder();
    this.getList();
  }
  loadMore(event){
    console.log('Done');
    setTimeout(() => {
      this.maxsizepage += 20;
      console.log(event);
      this.mycharts(event);
      this.getList(event);
      // event.target.complete();
    }, 500);
  }
  mycharts(event?) {
    var data = this.DashboardService.getGraph(this.DateStart,this.DateEnd,this.maxsizepage).then((result: any)=> { 
      if(result.status == true){
        this.ListModel = result.message;
        var label = new Array();
        var data = new Array();
        var coloR = new Array();
        this.ListModel.forEach(function (entry, index) {
          label.push(entry.Day);
          data.push(entry.Total);
          var r = Math.floor(Math.random() * 255);
            var g = Math.floor(Math.random() * 255);
            var b = Math.floor(Math.random() * 255);
            var colorData = "rgb(" + r + "," + g + "," + b + ")";
          coloR.push(colorData);
          if(event){
            event.target.complete();
          }
        });
      // console.log( this.label);
      var ctx = (<any>document.getElementById('canvas')).getContext('2d');
      var chart = new Chart(ctx, {
      // The type of chart we want to create
      type: 'bar',
 
      // The data for our dataset
      data: {
        labels: label,
        datasets: [{
          label: "สรุปยอดขายตามสินค้า",
          backgroundColor: coloR,
          data: data,
          borderWidth: 1
        }]
      },
      options: {
        scales: {
          yAxes: [{
            ticks: {
              reverse: false,
              min: 0,
              stepSize: 1000,
            },
          }]
        }
      }
    });
      }else{
        alert(result.message);
      }
    })
  }
  Heder(){
    var data = this.DashboardbygoodsService.getHeder(this.DateStart,this.DateEnd).then((result: any)=> { 
      console.log(result);
      this.HeaderModel = result;
    })
  }
  getList(event?){
    var data = this.DashboardbygoodsService.getList(this.DateStart,this.DateEnd,this.maxsizepage).then((result: any)=> { 
      if(result.status === true){
        this.ListGoodsModel = result.message;
        if(event){
          event.target.complete();
        }
      }else{
        alert(result.message)
      }
    })
  }
  selectdatestart(){
    this.datePicker.show({
      date: new Date(),
      mode: 'date',
      androidTheme: this.datePicker.ANDROID_THEMES.THEME_HOLO_DARK
    }).then(
      date =>{
          console.log('Got date: ', date)
         this.DateStart = date.getFullYear() + "-" + (date.getMonth() + 1) + "-" + date.getDate();
         this.mycharts();
         this.Heder();
        this.getList();
      } ,
      err => console.log('Error occurred while getting date: ', err)
    );
  }
  selectdateend(){
    this.datePicker.show({
      date: new Date(),
      mode: 'date',
      androidTheme: this.datePicker.ANDROID_THEMES.THEME_HOLO_DARK
    }).then(
      date => {
        console.log('Got date: ', date)
       this.DateEnd = date.getFullYear() + "-" + (date.getMonth() + 1) + "-" + date.getDate();
      this.mycharts();
      this.Heder();
      this.getList();
      } ,
      err => console.log('Error occurred while getting date: ', err)
    );
  }
}
