import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { DashboardbygoodsPage } from './dashboardbygoods.page';

const routes: Routes = [
  {
    path: '',
    component: DashboardbygoodsPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class DashboardbygoodsPageRoutingModule {}
