import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { MenugoodsPage } from './menugoods.page';

describe('MenugoodsPage', () => {
  let component: MenugoodsPage;
  let fixture: ComponentFixture<MenugoodsPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MenugoodsPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(MenugoodsPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
