import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { MenugoodsPage } from './menugoods.page';

const routes: Routes = [
  {
    path: '',
    component: MenugoodsPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class MenugoodsPageRoutingModule {}
