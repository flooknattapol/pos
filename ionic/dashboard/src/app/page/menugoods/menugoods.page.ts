import { Component, OnInit } from '@angular/core';
import { NavController } from '@ionic/angular';

@Component({
  selector: 'app-menugoods',
  templateUrl: './menugoods.page.html',
  styleUrls: ['./menugoods.page.scss'],
})
export class MenugoodsPage implements OnInit {

  constructor(private nav:NavController) { }

  ngOnInit() {
  }
  onListGoods(){
    this.nav.navigateForward('/goods');
  }
  onListCategory(){
    this.nav.navigateForward('/goodscategory');
  }
  onListDiscount(){
    this.nav.navigateForward('/discount');
  }
}
