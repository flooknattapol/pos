import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { MenugoodsPageRoutingModule } from './menugoods-routing.module';

import { MenugoodsPage } from './menugoods.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    MenugoodsPageRoutingModule
  ],
  declarations: [MenugoodsPage]
})
export class MenugoodsPageModule {}
