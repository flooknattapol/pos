import { CustomerModel, CustomerService } from './../../service/customer/customer.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-announce',
  templateUrl: './announce.page.html',
  styleUrls: ['./announce.page.scss'],
})
export class AnnouncePage implements OnInit {
  TEXT:any;
  CustomerModel:CustomerModel[];
  constructor(private CustomerService:CustomerService) { 
    this.TEXT = "";
  }

  ngOnInit() {
    this.getList();
  }
  getList(){
    var data = this.CustomerService.getList().then((result: any)=> { 
      if(result.status == true){
        this.CustomerModel = new Array;
        this.CustomerModel = result.message;
        
      }else{
        alert(result.message)
      }
    })
  }

  sendNotify(){
      if(this.TEXT != ""){
        this.CustomerModel.forEach(function (entry, index) {
          if(entry.userID != ""){
            var data = this.CustomerService.sentNotify(this.TEXT,entry.userID).then((result: any)=> { 
              if(result.status == 200){
                
              }
             else{
               alert(result.message);
             }
            })
          }   
        });
      } 
  }
}
