import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { DashboardbycategoryPage } from './dashboardbycategory.page';

describe('DashboardbycategoryPage', () => {
  let component: DashboardbycategoryPage;
  let fixture: ComponentFixture<DashboardbycategoryPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DashboardbycategoryPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(DashboardbycategoryPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
