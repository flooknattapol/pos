import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { DashboardbycategoryPageRoutingModule } from './dashboardbycategory-routing.module';

import { DashboardbycategoryPage } from './dashboardbycategory.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    DashboardbycategoryPageRoutingModule
  ],
  declarations: [DashboardbycategoryPage]
})
export class DashboardbycategoryPageModule {}
