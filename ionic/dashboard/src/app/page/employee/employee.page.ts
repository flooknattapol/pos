import { NavController } from '@ionic/angular';
import { EmployeeService, ListModel } from './../../service/employee/employee.service';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute ,NavigationExtras} from "@angular/router";
import { UtilityService } from './../../service/utility/utility.service';
@Component({
  selector: 'app-employee',
  templateUrl: './employee.page.html',
  styleUrls: ['./employee.page.scss'],
})
export class EmployeePage  {

  PathIMAGE:any;
  ListModel:ListModel[]
  constructor(private EmployeeService:EmployeeService ,private UtilityService:UtilityService,
    private nav:NavController,private route: ActivatedRoute) {
    this.PathIMAGE = this.UtilityService.ImagePath;
    this.ListModel = new Array;
   }

   ionViewWillEnter() {
    this.getList();
  }
  getList(){
    var data = this.EmployeeService.getList().then((result: any)=> { 
      console.log(result);
      if(result.status == true){
        this.ListModel = result.message;
      }else{
        alert(result.message)
      }
    })
  }
  onDetailPage(item){
    let navigationExtras: NavigationExtras = {
      queryParams: {
          CreateModel: item,
      }
    };    
    this.nav.navigateForward('/employeedetail',navigationExtras);
  }
  onAddPage(){
    this.ListModel = new Array;
    let navigationExtras: NavigationExtras = {
      queryParams: {
          CreateModel: this.ListModel,
      }
    };    
    this.nav.navigateForward('/employeeadd',navigationExtras);
  }
}
