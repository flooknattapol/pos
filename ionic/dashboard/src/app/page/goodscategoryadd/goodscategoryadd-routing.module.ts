import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { GoodscategoryaddPage } from './goodscategoryadd.page';

const routes: Routes = [
  {
    path: '',
    component: GoodscategoryaddPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class GoodscategoryaddPageRoutingModule {}
