import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { GoodscategoryaddPage } from './goodscategoryadd.page';

describe('GoodscategoryaddPage', () => {
  let component: GoodscategoryaddPage;
  let fixture: ComponentFixture<GoodscategoryaddPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GoodscategoryaddPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(GoodscategoryaddPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
