import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { DiscountaddPage } from './discountadd.page';

const routes: Routes = [
  {
    path: '',
    component: DiscountaddPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class DiscountaddPageRoutingModule {}
