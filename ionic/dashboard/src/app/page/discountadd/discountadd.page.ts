import { DiscountService } from './../../service/discount/discount.service';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute ,NavigationExtras} from "@angular/router";
import { NavController } from '@ionic/angular';
import { DiscountModel } from 'src/app/service/discount/discount.service';

@Component({
  selector: 'app-discountadd',
  templateUrl: './discountadd.page.html',
  styleUrls: ['./discountadd.page.scss'],
})
export class DiscountaddPage implements OnInit {
  CreateModel:DiscountModel;
  constructor(private nav:NavController,private route: ActivatedRoute,private DiscountService:DiscountService) {
    this.route.queryParams.subscribe(params => { 
      console.log(params);
      this.CreateModel = params['CreateModel'];
      console.log(this.CreateModel);
    });
   }

  ngOnInit() {
  }
  OnSave(){
    var bValid = true;
    if(this.CreateModel.NAME == "" || this.CreateModel.NAME == null  || this.CreateModel.NAME == undefined){
      bValid = false;
    }
    if(this.CreateModel.DISCOUNT == 0 || this.CreateModel.NAME == null  || this.CreateModel.NAME == undefined){
      bValid = false;
    }
    if(this.CreateModel.BAHTORPERCENT == "" || this.CreateModel.NAME == null  || this.CreateModel.NAME == undefined){
      bValid = false;
    }
    if(bValid == true){
      var Item = JSON.parse(window.localStorage.getItem('ObjectDashborad'))
      this.CreateModel.idSession = Item.id;
      var data = this.DiscountService.save(this.CreateModel).then((result: any)=> { 
        console.log(result);
        if(result.status == true){
          this.nav.pop();
        }else{
          alert(result.message)
        }
      })
    }else{
      alert("กรุณาใส่ข้อมูลให้ครบถ้วน");
    }
    
  }
}
