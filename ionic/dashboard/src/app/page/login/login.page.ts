import { AppComponent } from './../../app.component';
import { NavController, MenuController } from '@ionic/angular';
import { UserService, UserLoginmModel } from './../../service/user/user.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})
export class LoginPage implements OnInit {
  CreateModel:UserLoginmModel;
  constructor(public UserService:UserService,private nav:NavController,public menuCtrl:MenuController,private AppComponent:AppComponent) { 
    this.CreateModel = new UserLoginmModel;
    this.CreateModel.EMAIL = "";
    this.CreateModel.PASSWORD = "";
    //  this.menuCtrl.enable(false);
  }

  ngOnInit() {

    var item = JSON.parse(window.localStorage.getItem('ObjectDashborad'));
    if(item == null){
      window.localStorage['ObjectDashborad'] = JSON.stringify([]);
      item = [];
    }
    console.log(item);
    if(parseFloat(item.id) > 0){
      var datenow = new Date();
      if(item.EXPIRED_FLAG == 1 && new Date(item.EXPIRED_DATE)  < datenow){
        
      }else{
        if(item.business_isactive == '1'){
          this.nav.navigateRoot("/dashboard"); 
        }else{
          alert("ร้านค้าถูกปิดใช้งาน");
        }
      }
    }
  }
  ngOnRegister(){

  }
  ngOnLogin(){
    var bVlid = this.validatecheck();
    if(bVlid == true)
    {
      var data = this.UserService.Login(this.CreateModel).then((result: any)=> { 
        if(result.status == true){
          window.localStorage['ObjectDashborad'] = JSON.stringify(result.data);
          window.localStorage['TempGoodsList'] = JSON.stringify([]);
          window.localStorage['TempDiscount'] = JSON.stringify([]);
          if(result.data.business_isactive == '1'){
            this.AppComponent.ngOnInit();
            this.nav.navigateRoot("/dashboard");  
          }else{
            alert('ร้านค้าถูกปิดใช้งาน');
          }
        }else{
          alert(result.message)
        }
      })
    }else{
      alert("ใส่ข้อมูลไม่ครบถ้วน")
    }
  }
  validatecheck(){
    var bResult = true;
    if(this.CreateModel.EMAIL == ""){
			bResult = false;
    }
    if(this.CreateModel.PASSWORD == ""){
			bResult = false;
    }
    return bResult;
  }
  onForgetpassword(){
    this.nav.navigateForward("/forgetpassword"); 

  }
  onChangepassword(){
    this.nav.navigateForward("/changepassword"); 

  }
 
}
