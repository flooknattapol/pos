import { BusinessService, BusinessModel } from './../../service/business/business.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-renewbusiness',
  templateUrl: './renewbusiness.page.html',
  styleUrls: ['./renewbusiness.page.scss'],
})
export class RenewbusinessPage implements OnInit {
  CODE:any = "";
  CreateModel:BusinessModel;
  diffDays:number;
  constructor(private BusinessService:BusinessService) {
      this.CreateModel = new BusinessModel;
   }
  ngOnInit() {
    this.getItem();
  }
  getItem(){
    var data = this.BusinessService.get().then((result: any)=> { 
      if(result.status == true){
        this.CreateModel = result.message;
        let current_datetime = new Date()

        var DateEnd = current_datetime.getFullYear() + "-" + (current_datetime.getMonth() + 1) + "-" + current_datetime.getDate()
        var date = [];
        date = this.CreateModel.EXPIRED_DATE.split("-");
        var sDate = new Date(date[0],date[1]-1,date[2]); 
        var diff = Math.abs(sDate.getTime() - current_datetime.getTime());
        this.diffDays = Math.ceil(diff / (1000 * 3600 * 24)); 
      }else{
        alert(result.message);
      }
      this
    });
  }
  ngSaveRenewCode(){
    var item = JSON.parse(window.localStorage.getItem('ObjectDashborad'));
    console.log(item);
    if(this.CODE != ""){
      var data = this.BusinessService.saveRenewCode(this.CODE,item.business_id).then((result: any)=> { 
        if(result.status == true){
          alert("ต่ออายุเสร็จสิ้น")
          this.CODE = "";
          this.getItem();
        }else{
          alert(result.message);
        }
        this
      });
    }else{
      alert("กรุณาใส่ข้อมูลให้ครบถ้วน");
    }
    
  }
}
