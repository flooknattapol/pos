import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { VoucheraddPage } from './voucheradd.page';

const routes: Routes = [
  {
    path: '',
    component: VoucheraddPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class VoucheraddPageRoutingModule {}
