import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { VoucheraddPageRoutingModule } from './voucheradd-routing.module';

import { VoucheraddPage } from './voucheradd.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    VoucheraddPageRoutingModule
  ],
  declarations: [VoucheraddPage]
})
export class VoucheraddPageModule {}
