import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { VoucheraddPage } from './voucheradd.page';

describe('VoucheraddPage', () => {
  let component: VoucheraddPage;
  let fixture: ComponentFixture<VoucheraddPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ VoucheraddPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(VoucheraddPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
