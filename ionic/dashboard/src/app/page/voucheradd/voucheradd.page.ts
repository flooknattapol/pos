import { UtilityService } from './../../service/utility/utility.service';
import { NavController } from '@ionic/angular';
import { DiscountService, DiscountModel } from './../../service/discount/discount.service';
import { Component, OnInit } from '@angular/core';
import { DatePicker } from '@ionic-native/date-picker/ngx';
import { Camera,CameraOptions } from '@ionic-native/camera/ngx';
import { FileTransfer, FileUploadOptions, FileTransferObject } from '@ionic-native/file-transfer/ngx';
@Component({
  selector: 'app-voucheradd',
  templateUrl: './voucheradd.page.html',
  styleUrls: ['./voucheradd.page.scss'],
})
export class VoucheraddPage implements OnInit {
  CreateModel:DiscountModel;
  EXPIRATIONDATE:any;
  base64img:string='';
  PathRoot:any;
  constructor(private camera: Camera
    ,private transfer: FileTransfer,private datePicker: DatePicker,
    private DiscountService:DiscountService,private nav:NavController
    ,private UtilityService:UtilityService) { 
    this.CreateModel = new DiscountModel;
    this.PathRoot = this.UtilityService.apiRoot;
    let current_datetime = new Date()
    this.CreateModel.TOTALDATE = 7;
    this.CreateModel.DATE_START_CAMPAIGN = current_datetime.getFullYear() + "-" + (current_datetime.getMonth() + 1 ) + "-" + current_datetime.getDate()
    this.CreateModel.DATE_END_CAMPAIGN = current_datetime.getFullYear() + "-" + (current_datetime.getMonth() + 2 ) + "-" + current_datetime.getDate()

  }

  ngOnInit() {
  }
  upload() {
    console.log("beforetest");
    const fileTransfer: FileTransferObject = this.transfer.create();
    var dateobj = Date.now();
    var datestr = dateobj.toString(); 
    var filename = 'discount'+datestr+'.jpg'
    console.log(filename);
    let options: FileUploadOptions = {
      fileKey: "photo",
      fileName: filename,
      chunkedMode: false,
      mimeType: "image/jpeg",
      headers: {}
    }
    if(this.base64img != ""){
      fileTransfer.upload(this.base64img,this.PathRoot + '/imageUploadTransfer.php', options).then(data => {
        this.OnSave(filename);
      }, error => {
        alert("error");
        alert("error" + JSON.stringify(error));
      });
    }else{ 
      alert("กรุณาเลือกรูปภาพ");
      // this.OnSave(filename);
    }
  }
  OnSave(filename){
    var bValid = true;
    if(this.CreateModel.NAME == "" || this.CreateModel.NAME == null  || this.CreateModel.NAME == undefined){
      bValid = false;
    }
    if(this.CreateModel.DISCOUNT == 0 || this.CreateModel.DISCOUNT == null  || this.CreateModel.DISCOUNT == undefined){
      bValid = false;
    }
    if(this.CreateModel.EXCHANGE_REWARD == 0 || this.CreateModel.EXCHANGE_REWARD == null  || this.CreateModel.EXCHANGE_REWARD == undefined){
      bValid = false;
    }

    console.log(this.CreateModel);
    if(bValid == true){
      var Item = JSON.parse(window.localStorage.getItem('ObjectDashborad'))
      this.CreateModel.idSession = Item.id;
      this.CreateModel.TYPE_ID = 2;
      this.CreateModel.IMAGE = filename;
      this.CreateModel.BAHTORPERCENT = 0;
      this.CreateModel.STATUS = 1;
      var data = this.DiscountService.save(this.CreateModel).then((result: any)=> { 
        console.log(result);
        if(result.status == true){
          this.nav.pop();
        }else{
          alert(result.message)
        }
      })
    }else{
      alert("กรุณาใส่ข้อมูลให้ครบถ้วน");
    }
    
  }
  selectdatestart(){
    this.datePicker.show({
      date: new Date(),
      mode: 'date',
      androidTheme: this.datePicker.ANDROID_THEMES.THEME_HOLO_DARK
    }).then(
      date =>{
          console.log('Got date: ', date)
         this.CreateModel.DATE_START_CAMPAIGN = date.getFullYear() + "-" + (date.getMonth() + 1) + "-" + date.getDate();
      } ,
      err => console.log('Error occurred while getting date: ', err)
    );
  }
  selectdateend(){
    this.datePicker.show({
      date: new Date(),
      mode: 'date',
      androidTheme: this.datePicker.ANDROID_THEMES.THEME_HOLO_DARK
    }).then(
      date =>{
          console.log('Got date: ', date)
         this.CreateModel.DATE_END_CAMPAIGN = date.getFullYear() + "-" + (date.getMonth() + 1) + "-" + date.getDate();
      } ,
      err => console.log('Error occurred while getting date: ', err)
    );
  }
  imageCapturedGallery(){
    console.log("image");
    const options:CameraOptions={
      quality:70,
      destinationType:this.camera.DestinationType.DATA_URL,
      sourceType:this.camera.PictureSourceType.PHOTOLIBRARY,
      saveToPhotoAlbum:false,
      correctOrientation:true
    }
    this.camera.getPicture(options).then((ImageData=>{
       this.base64img="data:image/jpeg;base64,"+ImageData;
    }),error=>{
      console.log(error);
    })
  }
}
