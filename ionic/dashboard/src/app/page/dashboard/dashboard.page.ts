import { DashboardService, HeaderModel, ListModel } from './../../service/dashboard/dashboard.service';
import { Component, OnInit } from '@angular/core';
import { DatePicker } from '@ionic-native/date-picker/ngx';
import { Chart } from 'chart.js';
@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.page.html',
  styleUrls: ['./dashboard.page.scss'],
})
export class DashboardPage implements OnInit {
  Header:HeaderModel;
  ListModel:ListModel[];
  label:any[];
  data:any[];
  DateStart:any;
  DateEnd:any;
  maxsizepage:number = 20;
  constructor(private datePicker: DatePicker,private DashboardService:DashboardService) { 
    this.Header = new HeaderModel();
    this.ListModel = new Array();
    let current_datetime = new Date()
    this.DateStart = current_datetime.getFullYear() + "-" + (current_datetime.getMonth() ) + "-" + current_datetime.getDate()
    this.DateEnd = current_datetime.getFullYear() + "-" + (current_datetime.getMonth() + 1  ) + "-" + current_datetime.getDate()
  }

  ngOnInit() {
    this.mycharts();
  }
  loadMore(event){
    console.log('Done');
    setTimeout(() => {
      this.maxsizepage += 20;
      console.log(event);
      this.mycharts(event);
      // event.target.complete();
    }, 500);
  }
  mycharts(event?) {

    console.log(this.DateStart,this.DateEnd)
    var coloR = [];
    var data = this.DashboardService.getHeder(this.DateStart,this.DateEnd).then((result: any)=> { 
      this.Header = result[0];
      console.log(this.Header)
    })
    var data = this.DashboardService.getGraph(this.DateStart,this.DateEnd,this.maxsizepage).then((result: any)=> { 
      if(result.status == true){
        this.ListModel = result.message;
        var label = new Array();
        var data = new Array();
        this.ListModel.forEach(function (entry, index) {
          label.push(entry.Day);
          data.push(entry.Total);
            var r = Math.floor(Math.random() * 255);
            var g = Math.floor(Math.random() * 255);
            var b = Math.floor(Math.random() * 255);
          var colorData = "rgb(" + r + "," + g + "," + b + ")";
          coloR.push(colorData);
          if(event){
            event.target.complete();
          }
        });
      // console.log( this.label);
      var ctx = (<any>document.getElementById('canvas-chart')).getContext('2d');
      var chart = new Chart(ctx, {
      // The type of chart we want to create
      type: 'line',
 
      // The data for our dataset
      data: {
        labels: label,
        datasets: [{
          label: "สรุปยอดขาย",
          data: data,
          backgroundColor: coloR,
          color:coloR,
          borderWidth: 1
        }]
      },
      options: {
        scales: {
          yAxes: [{
            ticks: {
              reverse: false,
              min: 0,
              stepSize: 1000
            },
          }]
        }
      }
    });
      }else{
        alert(result.message);
      }
    })
    
  }
  selectdatestart(){
    this.datePicker.show({
      date: new Date(),
      mode: 'date',
      androidTheme: this.datePicker.ANDROID_THEMES.THEME_HOLO_DARK
    }).then(
      date =>{
          console.log('Got date: ', date)
         this.DateStart = date.getFullYear() + "-" + (date.getMonth() + 1) + "-" + date.getDate();
         this.mycharts();
      } ,
      err => console.log('Error occurred while getting date: ', err)
    );
  }
  selectdateend(){
    this.datePicker.show({
      date: new Date(),
      mode: 'date',
      androidTheme: this.datePicker.ANDROID_THEMES.THEME_HOLO_DARK
    }).then(
      date => {
        console.log('Got date: ', date)
       this.DateEnd = date.getFullYear() + "-" + (date.getMonth() + 1) + "-" + date.getDate();
       this.mycharts();
      } ,
      err => console.log('Error occurred while getting date: ', err)
    );
  }
  dynamicColors = function() {
		var r = Math.floor(Math.random() * 255);
		var g = Math.floor(Math.random() * 255);
		var b = Math.floor(Math.random() * 255);
		return "rgb(" + r + "," + g + "," + b + ")";
	 }
}
