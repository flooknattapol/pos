import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { EmployeeaddPage } from './employeeadd.page';

const routes: Routes = [
  {
    path: '',
    component: EmployeeaddPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class EmployeeaddPageRoutingModule {}
