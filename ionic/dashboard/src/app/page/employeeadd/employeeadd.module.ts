import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { EmployeeaddPageRoutingModule } from './employeeadd-routing.module';

import { EmployeeaddPage } from './employeeadd.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    EmployeeaddPageRoutingModule
  ],
  declarations: [EmployeeaddPage]
})
export class EmployeeaddPageModule {}
