import { ListModel, EmployeeService, BranchModel, RoleModel } from './../../service/employee/employee.service';
import { Component, OnInit } from '@angular/core';
import { NavController } from '@ionic/angular';
import { ActivatedRoute ,NavigationExtras} from "@angular/router";
import { Camera,CameraOptions } from '@ionic-native/camera/ngx';
import { FileTransfer, FileUploadOptions, FileTransferObject } from '@ionic-native/file-transfer/ngx';
import { UtilityService } from './../../service/utility/utility.service';

@Component({
  selector: 'app-employeeadd',
  templateUrl: './employeeadd.page.html',
  styleUrls: ['./employeeadd.page.scss'],
})
export class EmployeeaddPage implements OnInit {
  CreateModel:ListModel;
  base64img:string='';
  PathIMAGE:string="";
  PathRoot:string="";
  BranchComboList:BranchModel[];
  RoleComboList:RoleModel[];
  constructor(private nav:NavController,private route: ActivatedRoute,private camera: Camera
    ,private transfer: FileTransfer,private EmployeeService:EmployeeService
    ,private UtilityService:UtilityService) {
      this.route.queryParams.subscribe(params => { 
        this.CreateModel = params['CreateModel'];
  
      });
        this.PathIMAGE = this.UtilityService.ImagePath;
        this.PathRoot = this.UtilityService.apiRoot;
        this.BranchComboList = new Array;
        this.RoleComboList = new Array;
     }

  ngOnInit() {
    this.getRoleCombo();
    this.getBranchCombo();
  }
  imageCapturedGallery(){
    console.log("image");
    const options:CameraOptions={
      quality:70,
      destinationType:this.camera.DestinationType.DATA_URL,
      sourceType:this.camera.PictureSourceType.PHOTOLIBRARY,
      saveToPhotoAlbum:false,
      correctOrientation:true
    }
    this.camera.getPicture(options).then((ImageData=>{
       this.base64img="data:image/jpeg;base64,"+ImageData;
    }),error=>{
      console.log(error);
    })
  }
  clear(){
    this.base64img='';
  }
  upload() {
    console.log("beforetest");
    const fileTransfer: FileTransferObject = this.transfer.create();
    var dateobj = Date.now();
    var datestr = dateobj.toString(); 
    var filename = 'user'+datestr+'.jpg'
    console.log(filename);
    let options: FileUploadOptions = {
      fileKey: "photo",
      fileName: filename,
      chunkedMode: false,
      mimeType: "image/jpeg",
      headers: {}
    }
    if(this.base64img != ""){
      fileTransfer.upload(this.base64img,this.PathRoot + '/imageUploadTransferuser.php', options).then(data => {
        this.OnSave(filename);
      }, error => {
        alert("error");
        alert("error" + JSON.stringify(error));
      });
    }else if(this.CreateModel.IMAGE != ""){
      this.OnSave(filename);
    }else{ 
      alert("กรุณาเลือกรูปภาพ");
      // this.OnSave(filename);
    }
  }
  OnSave(filename){
    var bValid = true;
    if(this.base64img != ""){
      this.CreateModel.IMAGE = filename;
    }
    if(this.CreateModel.NAME == "" || this.CreateModel.NAME == null  || this.CreateModel.NAME == undefined){
      bValid = false;
    }
    if(this.CreateModel.TEL == "" && this.CreateModel.TEL == null || this.CreateModel.TEL == undefined){
      bValid = false;
    }
    if(this.CreateModel.EMAIL == "" && this.CreateModel.EMAIL == null || this.CreateModel.EMAIL == undefined){
      bValid = false;
    }
    if(bValid == true){
      var Item = JSON.parse(window.localStorage.getItem('ObjectDashborad'))
      this.CreateModel.idSession = Item.id;
     console.log(this.CreateModel);
      var data = this.EmployeeService.save(this.CreateModel).then((result: any)=> { 
        console.log(result);
        if(result.status == true){
          this.nav.pop();
        }else{
          alert(result.message)
        }
      })
    }else{
      alert("กรุณาใส่ข้อมูลให้ครบถ้วน");
    }
    
  }
  getRoleCombo(){
    var data = this.EmployeeService.getRoleComboList().then((result: any)=> { 
      if(result.status == true){
          this.RoleComboList = result.message;
        console.log()
      }else{
        alert(result.message)
      }
    })
  }
  getBranchCombo(){
    var data = this.EmployeeService.getBranchComboList().then((result: any)=> { 
      if(result.status == true){
        this.BranchComboList = result.message;
      }else{
        alert(result.message)
      }
    })
  }
}
