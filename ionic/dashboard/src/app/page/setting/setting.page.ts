import { AppComponent } from './../../app.component';
import { BusinessService, BusinessModel } from './../../service/business/business.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-setting',
  templateUrl: './setting.page.html',
  styleUrls: ['./setting.page.scss'],
})
export class SettingPage implements OnInit {

  CreateModel:BusinessModel;
  CountTransection:boolean;
  constructor(private BusinessService:BusinessService,private AppComponent : AppComponent) { 
    this.CreateModel = new BusinessModel;
    this.CountTransection = false;
  }

  ngOnInit() {
    this.getItem();
  }
  getItem(){
    var data = this.BusinessService.get().then((result: any)=> { 
      console.log(result)
      if(result.status == true){
        this.CreateModel = result.message;
        if(result.message2 > 0){
          this.CountTransection = true;
        }
      }else{
        alert(result.message);
      }
      this
    });
  }
  save(){
    var bvalid = true;
    if(this.CreateModel.EMAIL == ""){
      bvalid = false;
    }
    if(this.CreateModel.NAME == ""){
      bvalid = false;
    }
    if(this.CreateModel.SETTING_BOOLEAN_POINT == 1 && this.CreateModel.SETTING_BAHTTOPOINT == null){
      bvalid = false;
    }
    if(bvalid == true){
      var data = this.BusinessService.save(this.CreateModel).then((result: any)=> { 
        if(result.status == true){
          var item = JSON.parse(window.localStorage.getItem('ObjectDashborad'));
          var business_setting_blpoint = item.business_setting_blpoint;
          item.business_setting_blpoint  = result.data.SETTING_BOOLEAN_POINT;
          window.localStorage['ObjectDashborad'] = JSON.stringify(item);
          console.log(this.CreateModel.SETTING_BOOLEAN_POINT , business_setting_blpoint)
          if(this.CreateModel.SETTING_BOOLEAN_POINT != business_setting_blpoint){
            this.AppComponent.ngOnInit();
          }
          alert("บันทึกเสร็จสิ้น");
        }else{
          alert(result.message);
        }
        this
      });
    }else{
      alert("กรุณาใส่ข้อมูลให้ครบถ้วน")
    }
  }
}
