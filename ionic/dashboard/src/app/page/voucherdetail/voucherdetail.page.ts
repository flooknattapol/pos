import { DiscountModel } from 'src/app/service/discount/discount.service';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute ,NavigationExtras} from "@angular/router";
import { UtilityService } from './../../service/utility/utility.service';

@Component({
  selector: 'app-voucherdetail',
  templateUrl: './voucherdetail.page.html',
  styleUrls: ['./voucherdetail.page.scss'],
})
export class VoucherdetailPage implements OnInit {
  CreateModel:DiscountModel;
  PathIMAGE:any;
  constructor(private route: ActivatedRoute,private UtilityService:UtilityService) { 
    this.route.queryParams.subscribe(params => { 
      this.CreateModel = params['CreateModel'];
      console.log(this.CreateModel)
    });
    this.PathIMAGE = this.UtilityService.ImagePath;
  }

  ngOnInit() {
  }

}
