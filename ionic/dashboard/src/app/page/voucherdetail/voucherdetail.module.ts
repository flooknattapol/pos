import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { VoucherdetailPageRoutingModule } from './voucherdetail-routing.module';

import { VoucherdetailPage } from './voucherdetail.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    VoucherdetailPageRoutingModule
  ],
  declarations: [VoucherdetailPage]
})
export class VoucherdetailPageModule {}
