import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { VoucherdetailPage } from './voucherdetail.page';

const routes: Routes = [
  {
    path: '',
    component: VoucherdetailPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class VoucherdetailPageRoutingModule {}
