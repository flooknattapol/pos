import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { VoucherdetailPage } from './voucherdetail.page';

describe('VoucherdetailPage', () => {
  let component: VoucherdetailPage;
  let fixture: ComponentFixture<VoucherdetailPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ VoucherdetailPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(VoucherdetailPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
