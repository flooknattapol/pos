import { ListModel, DetailModel, ReportreciptService } from './../../service/reportrecipt/reportrecipt.service';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute ,NavigationExtras} from "@angular/router";

@Component({
  selector: 'app-reportreceiptdetail',
  templateUrl: './reportreceiptdetail.page.html',
  styleUrls: ['./reportreceiptdetail.page.scss'],
})
export class ReportreceiptdetailPage implements OnInit {
  ListModel:ListModel;
  DetailModel:DetailModel;
  constructor(private route: ActivatedRoute,private ReportreciptService:ReportreciptService) { 
    this.route.queryParams.subscribe(params => {
      this.ListModel = params['item'];
      this.getList(this.ListModel);

  });
  }

  ngOnInit() {
  }

  getList(item){
    var data = this.ReportreciptService.getDetail(item).then((result: any)=> { 
      console.log(result);
      if(result.status == true){
        this.DetailModel = new DetailModel;
        this.DetailModel = result.message;
      }else{
        alert(result.message)
      }
    })
    }
}
