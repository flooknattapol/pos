import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { ReportreceiptdetailPageRoutingModule } from './reportreceiptdetail-routing.module';

import { ReportreceiptdetailPage } from './reportreceiptdetail.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ReportreceiptdetailPageRoutingModule
  ],
  declarations: [ReportreceiptdetailPage]
})
export class ReportreceiptdetailPageModule {}
