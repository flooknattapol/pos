import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ReportreceiptdetailPage } from './reportreceiptdetail.page';

const routes: Routes = [
  {
    path: '',
    component: ReportreceiptdetailPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ReportreceiptdetailPageRoutingModule {}
