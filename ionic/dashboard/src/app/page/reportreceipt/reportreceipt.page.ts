import { ReportreciptService ,ListModel} from './../../service/reportrecipt/reportrecipt.service';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute ,NavigationExtras} from "@angular/router";
import { NavController } from '@ionic/angular';
import { DatePicker } from '@ionic-native/date-picker/ngx';
@Component({
  selector: 'app-reportreceipt',
  templateUrl: './reportreceipt.page.html',
  styleUrls: ['./reportreceipt.page.scss'],
})
export class ReportreceiptPage implements OnInit {
  ListModel:ListModel[];
  Total:number;
  refund:number;
  DateStart:any;
  DateEnd:any;
  NAME:any;
  maxsizepage:number = 20;
  constructor(private datePicker: DatePicker,private ReportreciptService:ReportreciptService,private nav:NavController) {
    let current_datetime = new Date()
    this.DateStart = current_datetime.getFullYear() + "-" + (current_datetime.getMonth()) + "-" + current_datetime.getDate()
    this.DateEnd = current_datetime.getFullYear() + "-" + (current_datetime.getMonth() + 1) + "-" + current_datetime.getDate()
    this.NAME = "";
   }

  ngOnInit() {
    this.getList();
  }
  loadMore(event){
    setTimeout(() => {
      this.maxsizepage += 20;
      console.log(event);
      this.getList(event);
      console.log('Done');
      // event.target.complete();
    }, 500);
  }
  getList(event?){
    
    var data = this.ReportreciptService.getList(this.DateStart,this.DateEnd,this.NAME,this.maxsizepage).then((result: any)=> { 
      console.log(result);
      if(result.status == true){
        this.ListModel = new Array;
        this.ListModel = result.message;

        var Total = 0;
				var refund = 0;
				this.ListModel.forEach(function (entry, index) {
						Total++;
						if(entry.IsActive == 0){
							refund ++;
						}
        });
        this.Total = Total;
        this.refund = refund;
        if(event){
          event.target.complete();
        }
      }else{
        alert(result.message)
      }
    })
   
  }
  selectdatestart(){
    this.datePicker.show({
      date: new Date(),
      mode: 'date',
      androidTheme: this.datePicker.ANDROID_THEMES.THEME_HOLO_DARK
    }).then(
      date =>{
          console.log('Got date: ', date)
         this.DateStart = date.getFullYear() + "-" + (date.getMonth() + 1) + "-" + date.getDate();
         this.getList();
      } ,
      err => console.log('Error occurred while getting date: ', err)
    );
  }
  selectdateend(){
    this.datePicker.show({
      date: new Date(),
      mode: 'date',
      androidTheme: this.datePicker.ANDROID_THEMES.THEME_HOLO_DARK
    }).then(
      date => {
        console.log('Got date: ', date)
       this.DateEnd = date.getFullYear() + "-" + (date.getMonth() + 1) + "-" + date.getDate();
       this.getList();
      } ,
      err => console.log('Error occurred while getting date: ', err)
    );
  }
  onDetail(item){
    let navigationExtras: NavigationExtras = {
      queryParams: {
          item:item,
      }
    };
    this.nav.navigateForward('/reportreceiptdetail',navigationExtras);
  }
}
