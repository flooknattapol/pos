import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ReportreceiptPage } from './reportreceipt.page';

const routes: Routes = [
  {
    path: '',
    component: ReportreceiptPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ReportreceiptPageRoutingModule {}
