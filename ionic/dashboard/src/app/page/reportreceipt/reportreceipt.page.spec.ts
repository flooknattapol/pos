import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { ReportreceiptPage } from './reportreceipt.page';

describe('ReportreceiptPage', () => {
  let component: ReportreceiptPage;
  let fixture: ComponentFixture<ReportreceiptPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ReportreceiptPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(ReportreceiptPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
