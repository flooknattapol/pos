import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { ReportreceiptPageRoutingModule } from './reportreceipt-routing.module';

import { ReportreceiptPage } from './reportreceipt.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ReportreceiptPageRoutingModule
  ],
  declarations: [ReportreceiptPage]
})
export class ReportreceiptPageModule {}
