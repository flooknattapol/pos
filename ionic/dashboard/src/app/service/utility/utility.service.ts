import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class UtilityService {

  // public apiUrl = 'http://localhost/dev-pos/web/api/';
  // public ImagePath = 'http://localhost/dev-pos/web/';
  // public apiRoot = 'http://localhost/dev-pos/web/';
  public apiUrl = 'https://www.devdeethailand.com/dev-pos/web/api/';
  public apiRoot = 'https://www.devdeethailand.com/dev-pos/web/';
  public ImagePath = 'https://www.devdeethailand.com/dev-pos/web/';
  public apiKey = 'POS2020DEVDEETHAILAND.COM'; // <-- Enter your own key here!
  public apiVersion = '20.05.19.002';
 
  /**
   * Constructor of the Service with Dependency Injection
   * @param http The standard Angular HttpClient to make requests
   */
  constructor() { }
 
  public getApiUrl():string{
    return this.apiUrl;
  }

  public getApiKey():string{
    return this.apiKey;
  }

  public getApiVersion():string{
    return this.apiVersion;
  }
  public getImage():string{
    return this.ImagePath;
  }
  public getApiRoot():string{
    return this.apiRoot;
  }
}
