import { HttpClient } from '@angular/common/http';
import { UtilityService } from './../utility/utility.service';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class DiscountService {

  constructor(private http:HttpClient,private apiHelper:UtilityService) { }
  urlGet = "getListDiscount";
  urlsave ="adddiscount";
  urlGetCampaing = "getListDiscountCampaign";
  getList(maxsizepage) {
    var Item = JSON.parse(window.localStorage.getItem('ObjectDashborad'))
    var SearchObject =  {
      "PageIndex": 1,
      "PageSize": maxsizepage,
      "SortColumn": "ID",
      "SortOrder": "asc",
      "idSession":Item.id,
      "mSearch": {
        "NAME": ""
      }
    }
    var myJSON = JSON.stringify(SearchObject); 
    return new Promise((resolve,reject) => 
    this.http.get(this.apiHelper.apiUrl + this.urlGet  + "?JsonData=" + myJSON + "&authen=" + this.apiHelper.apiKey + "&version=" + this.apiHelper.apiVersion, {})
    .subscribe((res) => {
      resolve(res);
    },(err)=>{
      reject(err);
    }))
  }
  getListCampaign(maxsizepage) {
    var Item = JSON.parse(window.localStorage.getItem('ObjectDashborad'))
    var SearchObject =  {
      "PageIndex": 1,
      "PageSize": maxsizepage,
      "SortColumn": "ID",
      "SortOrder": "asc",
      "idSession":Item.id,
      "mSearch": {
        "NAME": ""
      }
    }
    var myJSON = JSON.stringify(SearchObject); 
    return new Promise((resolve,reject) => 
    this.http.get(this.apiHelper.apiUrl + this.urlGetCampaing  + "?JsonData=" + myJSON + "&authen=" + this.apiHelper.apiKey + "&version=" + this.apiHelper.apiVersion, {})
    .subscribe((res) => {
      resolve(res);
    },(err)=>{
      reject(err);
    }))
  }
  save(Item) {
    var JsonData =  {
      "ID":Item.ID,
      "NAME":Item.NAME,
      "BAHTORPERCENT":Item.BAHTORPERCENT,
      "DISCOUNT":Item.DISCOUNT,
      "TYPE_ID":Item.TYPE_ID,
      "EXCHANGE_REWARD":Item.EXCHANGE_REWARD,
      "EXPIRATIONDATE":Item.EXPIRATIONDATE,
      "STATUS":Item.STATUS,
      "DATE_START_CAMPAIGN":Item.DATE_START_CAMPAIGN,
      "DATE_END_CAMPAIGN":Item.DATE_END_CAMPAIGN,
      "TOTALDATE":Item.TOTALDATE,
      "IMAGE":Item.IMAGE,
      "idSession":Item.idSession
    }
    var myJSON = JSON.stringify(JsonData); 
    console.log(myJSON);
    return new Promise((resolve,reject) => 
    this.http.get(this.apiHelper.apiUrl + this.urlsave  + "?JsonData=" + myJSON + "&authen=" + this.apiHelper.apiKey + "&version=" + this.apiHelper.apiVersion, {})
    .subscribe((res) => {
      resolve(res);
    },(err)=>{
      reject(err);
    }))
  }
}
export class DiscountModel{
  ID:any;
  NAME:any;
  DETAIL:any;
  UID:any;
  BARCODE:any;
  DISCOUNT:number;
  BAHTORPERCENT:any;
  BUSINESS_ID:any;
  IsActive:any;
  idSession:any;
  TYPE_ID:number;
  EXCHANGE_REWARD:number;
  EXPIRATIONDATE:any;
  IMAGE:any;
  STATUS:any;
  DATE_START_CAMPAIGN:any;
  DATE_END_CAMPAIGN:any;
  TOTALDATE:number;
  disabledtogle:boolean;
}
export class TsDiscountModel{
  ID:any;
  TRANSECTION_ID:any;
  AMOUNT_DISCOUNT:any;
}
