import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { UtilityService } from './../utility/utility.service';

@Injectable({
  providedIn: 'root'
})
export class BusinessService {
  urlGet = "getBusinessSetting";
  urlSave = "saveBusinessSetting";
  urlrenewcode = "renewbusiness";
  constructor(private http:HttpClient,private apiHelper:UtilityService) { 

  }
  get() {
    var Item = JSON.parse(window.localStorage.getItem('ObjectDashborad'))
    var SearchObject =  {
      "idSession":Item.id,
    }
    var myJSON = JSON.stringify(SearchObject); 
    return new Promise((resolve,reject) => 
    this.http.get(this.apiHelper.apiUrl + this.urlGet  + "?JsonData=" + myJSON + "&authen=" + this.apiHelper.apiKey + "&version=" + this.apiHelper.apiVersion, {})
    .subscribe((res) => {
      resolve(res);
    },(err)=>{
      reject(err);
    }))
  }
  save(JsonData) {
    console.log(JsonData);
    var myJSON = JSON.stringify(JsonData); 
    return new Promise((resolve,reject) => 
    this.http.get(this.apiHelper.apiUrl + this.urlSave  + "?JsonData=" + myJSON + "&authen=" + this.apiHelper.apiKey + "&version=" + this.apiHelper.apiVersion, {})
    .subscribe((res) => {
      resolve(res);
    },(err)=>{
      reject(err);
    }))
  }
  saveRenewCode(CODE,ID) {
    var JsonData ={
      "CODE":CODE,
      "BUSINESS_ID":ID
    }
    var myJSON = JSON.stringify(JsonData); 
    return new Promise((resolve,reject) => 
    this.http.get(this.apiHelper.apiUrl + this.urlrenewcode  + "?JsonData=" + myJSON + "&authen=" + this.apiHelper.apiKey + "&version=" + this.apiHelper.apiVersion, {})
    .subscribe((res) => {
      resolve(res);
    },(err)=>{
      reject(err);
    }))
  }
}
export class BusinessModel{
  NAME:any;
  ID:any;
  EMAIL:any;
  EXPIRED_DATE:any;
  SETTING_BOOLEAN_POINT:number;
  SETTING_BAHTTOPOINT:any;
  SETTING_REFUND_POINT:any;
}
