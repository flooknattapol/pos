import { resolve } from 'url';
import { UtilityService } from './../utility/utility.service';
import { Injectable } from '@angular/core';
import { HttpClient,HttpHeaders } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  public apiLogin = "Login";
  public urlforgetpass = "Forgetpassworduser";
  public urlchangepassword = "Changepassworduser"
  constructor(private apiHelper:UtilityService,private http:HttpClient) {
   }
   Login(JsonData:UserLoginmModel) {
    var myJSON = JSON.stringify(JsonData); 
    return new Promise((resolve,reject) => 
    this.http.get(this.apiHelper.apiUrl + this.apiLogin + "?JsonData=" + myJSON + "&authen=" + this.apiHelper.apiKey + "&version=" + this.apiHelper.apiVersion, {})
    .subscribe((res) => {
      resolve(res);
    },(err)=>{
      reject(err);
    })
  )};
  Forgetpass(EMAIL) {
    var JsonData = {
      "EMAIL":EMAIL
    }
    var myJSON = JSON.stringify(JsonData); 
    console.log(myJSON);
    return new Promise((resolve,reject) => 
    this.http.get(this.apiHelper.apiUrl + this.urlforgetpass  + "?JsonData=" + myJSON + "&authen=" + this.apiHelper.apiKey + "&version=" + this.apiHelper.apiVersion, {})
    .subscribe((res) => {
      resolve(res);
    },(err)=>{
      reject(err);
    }))
  }
  changepassword(EMAIL,PASSWORD,NEWPASSWORD) {
    var JsonData = {
      "EMAIL":EMAIL,
      "PASSWORD":PASSWORD,
      "NEWPASSWORD":NEWPASSWORD
    }
    var myJSON = JSON.stringify(JsonData); 
    console.log(myJSON);
    return new Promise((resolve,reject) => 
    this.http.get(this.apiHelper.apiUrl + this.urlchangepassword + "?JsonData=" + myJSON + "&authen=" + this.apiHelper.apiKey + "&version=" + this.apiHelper.apiVersion, {})
    .subscribe((res) => {
      resolve(res);
    },(err)=>{
      reject(err);
    }))
  }
}
export class UserLoginmModel{
  EMAIL:any;
  PASSWORD:any;
}
