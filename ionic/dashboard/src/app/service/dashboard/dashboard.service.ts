import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { UtilityService } from './../utility/utility.service';
@Injectable({
  providedIn: 'root'
})
export class DashboardService {
  urlGetHeader = "getHeaderDashboard";
  urlGetGraph = "getGraphDashboard"

  constructor(private http:HttpClient,private apiHelper:UtilityService) { 

  }
  getGraph(DateStart:any,DateEnd:any,maxsizepage:number) {
    var Item = JSON.parse(window.localStorage.getItem('ObjectDashborad'))
    var SearchObject =  {
      "PageIndex": 1,
      "PageSize": maxsizepage,
      "SortColumn": "ID",
      "SortOrder": "asc",
      "idSession":Item.id,
      "mSearch": {
        "DateStart":DateStart,
        "DateEnd":DateEnd
      }
    }
    var myJSON = JSON.stringify(SearchObject); 
    return new Promise((resolve,reject) => 
    this.http.get(this.apiHelper.apiUrl + this.urlGetGraph  + "?JsonData=" + myJSON + "&authen=" + this.apiHelper.apiKey + "&version=" + this.apiHelper.apiVersion, {})
    .subscribe((res) => {
      resolve(res);
    },(err)=>{
      reject(err);
    }))
  }
  getHeder(DateStart:any,DateEnd:any) {
    var Item = JSON.parse(window.localStorage.getItem('ObjectDashborad'))
    var SearchObject =  {
      "idSession":Item.id,
      "DateStart":DateStart,
      "DateEnd":DateEnd
    }
    console.log(SearchObject)
    var myJSON = JSON.stringify(SearchObject); 
    return new Promise((resolve,reject) => 
    this.http.get(this.apiHelper.apiUrl + this.urlGetHeader  + "?JsonData=" + myJSON + "&authen=" + this.apiHelper.apiKey + "&version=" + this.apiHelper.apiVersion, {})
    .subscribe((res) => {
      resolve(res);
    },(err)=>{
      reject(err);
    }))
  }
  
}
export class HeaderModel{
  Total:number;
  Refund:number;
  Discount:number;
  Cost:number;
  TotalCostRefund:number;

}
export class ListModel{
  Cost:number;
  Day:any;
  Discount:number;
  Refund:number;
  Total:number;
  TotalCostRefund:number;
}
