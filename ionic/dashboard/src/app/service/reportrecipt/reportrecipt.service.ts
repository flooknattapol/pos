import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { UtilityService } from '../utility/utility.service';
import { TsDiscountModel } from './../discount/discount.service';

@Injectable({
  providedIn: 'root'
})
export class ReportreciptService {
  urlGet = "getListTransection";
  urlGetDetail = "getListDetailTransection";
  constructor(private http:HttpClient,private apiHelper:UtilityService) { }
  getList(DateStart:any,DateEnd:any,NAME:any,maxsizepage:number) {
    var Item = JSON.parse(window.localStorage.getItem('ObjectDashborad'))
    var SearchObject =  {
      "PageIndex": 1,
      "PageSize": maxsizepage,
      "SortColumn": "ID",
      "SortOrder": "DESC",
      "idSession":Item.id,
      "mSearch": {
        "NAME": NAME,
        "DateStart":DateStart,
        "DateEnd":DateEnd
      }
    }
    var myJSON = JSON.stringify(SearchObject); 
    return new Promise((resolve,reject) => 
    this.http.get(this.apiHelper.apiUrl + this.urlGet  + "?JsonData=" + myJSON + "&authen=" + this.apiHelper.apiKey + "&version=" + this.apiHelper.apiVersion, {})
    .subscribe((res) => {
      resolve(res);
    },(err)=>{
      reject(err);
    }))
  }
  getDetail(JsonData) {
    var Item = JSON.parse(window.localStorage.getItem('ObjectDashborad'))
    var SearchObject =  {
      "TRANSECTION_ID":JsonData.ID,
    }
    var myJSON = JSON.stringify(SearchObject); 
    return new Promise((resolve,reject) => 
    this.http.get(this.apiHelper.apiUrl + this.urlGetDetail  + "?JsonData=" + myJSON + "&authen=" + this.apiHelper.apiKey + "&version=" + this.apiHelper.apiVersion, {})
    .subscribe((res) => {
      resolve(res);
    },(err)=>{
      reject(err);
    }))
  }
}
export class ListModel{
  ID:any;
  CUSTOMER_ID:any;
  ORDER_NAME:any;
  CATEGORYPAYMENT:any;
  SELLER_ID:any;  
  TimeStamp:any;
  TotalOrder:any;
  TotalCost:any;
  Received	:any;
  Change:any;
  Detail:DetailModel[];
  Discount:TsDiscountModel;
  IsActive:any;
  BUSINESS_ID:any;
  CUSTOMER_NAME:any;
  ORDERTYPE:any;
  AMOUNT_DISCOUNT:number;

}
export class DetailModel{
  ID:any;
  PRICE:any;
  GOODS_ID:any;
  AMOUNT:any;
  GOODS_NAME:any;

}
