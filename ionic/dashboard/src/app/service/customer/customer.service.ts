import { HttpClient } from '@angular/common/http';
import { UtilityService } from './../utility/utility.service';
import { Injectable } from '@angular/core';
@Injectable({
  providedIn: 'root'
})
export class CustomerService {
  urlGet = "getListCustomer";
  constructor(private http:HttpClient,private apiHelper:UtilityService) { }
  getList() {
    var Item = JSON.parse(window.localStorage.getItem('Object'))
    var SearchObject =  {
      "PageIndex": 1,
      "PageSize": 1000,
      "SortColumn": "ID",
      "SortOrder": "DESC",
      "idSession":Item.id,
      "mSearch": {
        "NAME": ""
      }
    }
    var myJSON = JSON.stringify(SearchObject); 
    return new Promise((resolve,reject) => 
    this.http.get(this.apiHelper.apiUrl + this.urlGet  + "?JsonData=" + myJSON + "&authen=" + this.apiHelper.apiKey + "&version=" + this.apiHelper.apiVersion, {})
    .subscribe((res) => {
      resolve(res);
    },(err)=>{
      reject(err);
    })
  )};
  sentNotify(messageJson,userID) {
    let Headers = {'Authorization':'basic 6eQI/wmouB9+R5mXA814nXnPEfqDPaTy+GVX32cTqTQ='};

    var message = "เทสประกาศ"
    var JsonData =  {
      "Message":message
    }
    return new Promise((resolve,reject) => 
    this.http.post("https://www.devdeethailand.com/notify_api/NotifyService/" + userID,JsonData, {headers: Headers})
    .subscribe((res) => {
      resolve(res);
    },(err)=>{
      reject(err);
    }))
  }
}

export class CustomerModel{
  NAME:any;
  TEL:any;
  EMAIL:any;
  CATEGORY:any;
  BUSINESS_ID:any;
  AMOUNTBUY:any;
  pictureUrl:any;
  REWARDPOINT:any;
  NOTE:any;
  userID:any;
  IsActive:any;
  ID:any = 0;
}