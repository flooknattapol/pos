import { HttpClient } from '@angular/common/http';
import { UtilityService } from './../utility/utility.service';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class GoodsService {

  constructor(private http:HttpClient,private apiHelper:UtilityService) { }
  urlGet = "getListGoods"
  urlGetComboList = "getComboListGoods";
  urlsave ="saveGoods";
  urlsavecategory ="saveGoodscategory";

  getList(maxsizepage) {
    var Item = JSON.parse(window.localStorage.getItem('ObjectDashborad'))
    var SearchObject =  {
      "PageIndex": 1,
      "PageSize": maxsizepage,
      "SortColumn": "ID",
      "SortOrder": "asc",
      "idSession":Item.id,
      "mSearch": {
        "NAME": ""
      }
    }
    var myJSON = JSON.stringify(SearchObject); 
    return new Promise((resolve,reject) => 
    this.http.get(this.apiHelper.apiUrl + this.urlGet  + "?JsonData=" + myJSON + "&authen=" + this.apiHelper.apiKey + "&version=" + this.apiHelper.apiVersion, {})
    .subscribe((res) => {
      resolve(res);
    },(err)=>{
      reject(err);
    }))
  }
  getComboList() {
    var Item = JSON.parse(window.localStorage.getItem('ObjectDashborad'))
    var SearchObject =  {
      "idSession":Item.id,
    }
    console.log(SearchObject)
    var myJSON = JSON.stringify(SearchObject); 
    return new Promise((resolve,reject) => 
    this.http.get(this.apiHelper.apiUrl + this.urlGetComboList  + "?JsonData=" + myJSON + "&authen=" + this.apiHelper.apiKey + "&version=" + this.apiHelper.apiVersion, {})
    .subscribe((res) => {
      resolve(res);
    },(err)=>{
      reject(err);
    }))
  }
  save(Item) {
    var JsonData =  {
      "ID":Item.ID,
      "NAME":Item.NAME,
      "PRICE":Item.PRICE,
      "COST":Item.COST,
      "IMAGE":Item.IMAGE,
      "SKU":Item.SKU,
      "CATEGORY_ID":Item.CATEGORY_ID,
      "STOCK_IsActive":Item.STOCK_IsActive,
      "idSession":Item.idSession
    }
    var myJSON = JSON.stringify(JsonData); 
    console.log(myJSON);
    return new Promise((resolve,reject) => 
    this.http.get(this.apiHelper.apiUrl + this.urlsave  + "?JsonData=" + myJSON + "&authen=" + this.apiHelper.apiKey + "&version=" + this.apiHelper.apiVersion, {})
    .subscribe((res) => {
      resolve(res);
    },(err)=>{
      reject(err);
    }))
  }
  savecategory(Item) {
    var JsonData =  {
      "ID":Item.ID,
      "NAME":Item.NAME,
      "DETAIL":Item.DETAIL,
      "idSession":Item.idSession
    }
    var myJSON = JSON.stringify(JsonData); 
    console.log(myJSON);
    return new Promise((resolve,reject) => 
    this.http.get(this.apiHelper.apiUrl + this.urlsavecategory  + "?JsonData=" + myJSON + "&authen=" + this.apiHelper.apiKey + "&version=" + this.apiHelper.apiVersion, {})
    .subscribe((res) => {
      resolve(res);
    },(err)=>{
      reject(err);
    }))
  }
}
export class GoodsModel{
  ID:number;
  BARCODE:any;
  BUSINESS_ID:any;
  CATEGORYNAME:any;
  CATEGORY_ID:any;
  COMPOSITE:any;
  COST:any;
  DETAIL:any;
  IMAGE:any;
  IsActive:any;
  NAME:any;
  PRICE:any;
  SKU:any;
  SOLDBUY:any;
  STATUS:any;
  STOCK:any;
  UID:any;
  idSession:any;
  STOCK_IsActive:any;
}
export class GoodsComboModel{
  ID:number;
  NAME:any;
  IsActive:any;
  BUSINESS_ID:number;
  DETAIL:any;
  idSession:any;
}
