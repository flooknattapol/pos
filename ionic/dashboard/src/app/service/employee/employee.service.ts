import { HttpClient } from '@angular/common/http';
import { UtilityService } from './../utility/utility.service';
import { Injectable } from '@angular/core';


@Injectable({
  providedIn: 'root'
})
export class EmployeeService {

  constructor(private http:HttpClient,private apiHelper:UtilityService) { }
  urlGet = "getListEmployee"
  urlsave ="saveEmployee";
  urlGetRoleComboList = "getComboListRole";
  urlGetBranchComboList ="getComboListBranch";
  getList() {
    var Item = JSON.parse(window.localStorage.getItem('ObjectDashborad'))
    var SearchObject =  {
      "PageIndex": 1,
      "PageSize": 20,
      "SortColumn": "ID",
      "SortOrder": "asc",
      "idSession":Item.id,
      "mSearch": {
        "NAME": ""
      }
    }
    var myJSON = JSON.stringify(SearchObject); 
    return new Promise((resolve,reject) => 
    this.http.get(this.apiHelper.apiUrl + this.urlGet  + "?JsonData=" + myJSON + "&authen=" + this.apiHelper.apiKey + "&version=" + this.apiHelper.apiVersion, {})
    .subscribe((res) => {
      resolve(res);
    },(err)=>{
      reject(err);
    }))
  }
  
  save(item) {
    var JsonData =  {
      "ID":item.ID,
      "NAME":item.NAME,
      "EMAIL":item.EMAIL,
      "ROLE_ID":item.ROLE_ID,
      "BRANCH_ID":item.BRANCH_ID,
      "TEL":item.TEL,
      "idSession":item.idSession
    }
    var myJSON = JSON.stringify(JsonData); 
    console.log(myJSON);
    return new Promise((resolve,reject) => 
    this.http.get(this.apiHelper.apiUrl + this.urlsave  + "?JsonData=" + myJSON + "&authen=" + this.apiHelper.apiKey + "&version=" + this.apiHelper.apiVersion, {})
    .subscribe((res) => {
      resolve(res);
    },(err)=>{
      reject(err);
    }))
  }
  getRoleComboList() {
    var Item = JSON.parse(window.localStorage.getItem('ObjectDashborad'))
    var SearchObject =  {
      "idSession":Item.id,
    }
    console.log(SearchObject)
    var myJSON = JSON.stringify(SearchObject); 
    return new Promise((resolve,reject) => 
    this.http.get(this.apiHelper.apiUrl + this.urlGetRoleComboList  + "?JsonData=" + myJSON + "&authen=" + this.apiHelper.apiKey + "&version=" + this.apiHelper.apiVersion, {})
    .subscribe((res) => {
      resolve(res);
    },(err)=>{
      reject(err);
    }))
  }
  getBranchComboList() {
    var Item = JSON.parse(window.localStorage.getItem('ObjectDashborad'))
    var SearchObject =  {
      "idSession":Item.id,
    }
    console.log(SearchObject)
    var myJSON = JSON.stringify(SearchObject); 
    return new Promise((resolve,reject) => 
    this.http.get(this.apiHelper.apiUrl + this.urlGetBranchComboList  + "?JsonData=" + myJSON + "&authen=" + this.apiHelper.apiKey + "&version=" + this.apiHelper.apiVersion, {})
    .subscribe((res) => {
      resolve(res);
    },(err)=>{
      reject(err);
    }))
  }
}
export class ListModel{
  ID:number = 0;
  NAME:any;
  TEL:any;
  EMAIL:any;
  IMAGE:any;
  ROLE_ID:any;
  ROLE_NAME:any;
  ID_POSITION:any;
  BRANCH_ID:any;
  BARNCH_NAME:any;
  idSession:any;
}

export class RoleModel{
  ID:any;
  NAME:any;
  BUSINESS_ID:number;
}
export class BranchModel{
  ID:any;
  NAME:any;
  BUSINESS_ID:number;
}