import { Component, OnInit } from '@angular/core';

import { Platform } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';

@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html',
  styleUrls: ['app.component.scss']
})
export class AppComponent implements OnInit {
  public selectedIndex = 0;
  public appPages = [
    {
      title: 'รายงานการขาย',
      url: '/dashboard',
      icon: 'ellipse'
    },
    {
      title: 'รายงานการขายตามสินค้า',
      url: '/dashboardbygoods',
      icon: 'ellipse'
    },
    {
      title: 'ใบเสร็จการขาย',
      url: '/reportreceipt',
      icon: 'ellipse'
    },
    {
      title: 'รายการสินค้า',
      url: '/menugoods',
      icon: 'ellipse'
    },
    {
      title: 'ประกาศ',
      url: '/announce',
      icon: 'ellipse'
    },
    // {
    //   title: 'เคมเปญแลกแต้ม',
    //   url: '/voucher',
    //   icon: 'ellipse'
    // },
    {
      title: 'พนักงาน',
      url: '/employee',
      icon: 'ellipse'
    }
    ,
    {
      title: 'ต่ออายุร้านค้า',
      url: '/renewbusiness',
      icon: 'refresh'
    }
    ,
    {
      title: 'ตั้งค่า',
      url: '/setting',
      icon: 'settings'
    },
    {
      title: 'เปลี่ยนรหัสผ่าน',
      url: '/changepassword',
      icon: 'ellipse'
    },
    {
      title: 'ออกจากระบบ',
      url: '/logout',
      icon: 'log-out'
    }
  ];

  constructor(
    private platform: Platform,
    private splashScreen: SplashScreen,
    private statusBar: StatusBar
  ) {
    this.initializeApp();
  }

  initializeApp() {
    this.platform.ready().then(() => {
      this.statusBar.styleDefault();
      this.splashScreen.hide();
    });
  }
  business_name:any;
  user:any;
  ngOnInit() {
    var Item = JSON.parse(window.localStorage.getItem('ObjectDashborad'));
    if(Item != null){
      this.business_name = Item.business_name;
      this.user = Item.user;
    }
    console.log("ada");
    if(Item.business_setting_blpoint == 0){
      var _index = null;
      this.appPages.forEach(function (entry, index) {
        if(entry.title == 'เคมเปญแลกแต้ม'){
          _index = index;
        }
      });
      if(_index != null){
        this.appPages.splice(_index, 1);
      }
    }else{
      var _status = true
      this.appPages.forEach(function (entry, index) {
        if(entry.title == 'เคมเปญแลกแต้ม'){
          _status = false;
        }
      });
      var object = {
        title: 'เคมเปญแลกแต้ม',
        url: '/voucher',
        icon: 'ellipse'
      }
      if(_status == true){
        this.appPages.splice(5, 0, object);
      }
    }
    const path = window.location.pathname.split('folder/')[1];
    if (path !== undefined) {
      this.selectedIndex = this.appPages.findIndex(page => page.title.toLowerCase() === path.toLowerCase());
    }
  }

}
