import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  {
    path: '',
    redirectTo: '/login',
    pathMatch: 'full'
  },
  {
    path: 'folder/:id',
    loadChildren: () => import('./folder/folder.module').then( m => m.FolderPageModule)
  },
  {
    path: 'dashboard',
    loadChildren: () => import('./page/dashboard/dashboard.module').then( m => m.DashboardPageModule)
  },
  {
    path: 'dashboardbygoods',
    loadChildren: () => import('./page/dashboardbygoods/dashboardbygoods.module').then( m => m.DashboardbygoodsPageModule)
  },
  {
    path: 'dashboardbycategory',
    loadChildren: () => import('./page/dashboardbycategory/dashboardbycategory.module').then( m => m.DashboardbycategoryPageModule)
  },
  {
    path: 'reportreceipt',
    loadChildren: () => import('./page/reportreceipt/reportreceipt.module').then( m => m.ReportreceiptPageModule)
  },
  {
    path: 'menugoods',
    loadChildren: () => import('./page/menugoods/menugoods.module').then( m => m.MenugoodsPageModule)
  },
  {
    path: 'goods',
    loadChildren: () => import('./page/goods/goods.module').then( m => m.GoodsPageModule)
  },
  {
    path: 'goodsdetail',
    loadChildren: () => import('./page/goodsdetail/goodsdetail.module').then( m => m.GoodsdetailPageModule)
  },
  {
    path: 'goodscategory',
    loadChildren: () => import('./page/goodscategory/goodscategory.module').then( m => m.GoodscategoryPageModule)
  },
  {
    path: 'discount',
    loadChildren: () => import('./page/discount/discount.module').then( m => m.DiscountPageModule)
  },
  {
    path: 'goodsadd',
    loadChildren: () => import('./page/goodsadd/goodsadd.module').then( m => m.GoodsaddPageModule)
  },
  {
    path: 'goodscategoryadd',
    loadChildren: () => import('./page/goodscategoryadd/goodscategoryadd.module').then( m => m.GoodscategoryaddPageModule)
  },
  {
    path: 'discountadd',
    loadChildren: () => import('./page/discountadd/discountadd.module').then( m => m.DiscountaddPageModule)
  },
  {
    path: 'reportreceiptdetail',
    loadChildren: () => import('./page/reportreceiptdetail/reportreceiptdetail.module').then( m => m.ReportreceiptdetailPageModule)
  },
  {
    path: 'login',
    loadChildren: () => import('./page/login/login.module').then( m => m.LoginPageModule)
  },
  {
    path: 'logout',
    loadChildren: () => import('./page/logout/logout.module').then( m => m.LogoutPageModule)
  },  {
    path: 'forgetpassword',
    loadChildren: () => import('./page/forgetpassword/forgetpassword.module').then( m => m.ForgetpasswordPageModule)
  },
  {
    path: 'changepassword',
    loadChildren: () => import('./page/changepassword/changepassword.module').then( m => m.ChangepasswordPageModule)
  },
  {
    path: 'employee',
    loadChildren: () => import('./page/employee/employee.module').then( m => m.EmployeePageModule)
  },
  {
    path: 'employeeadd',
    loadChildren: () => import('./page/employeeadd/employeeadd.module').then( m => m.EmployeeaddPageModule)
  },
  {
    path: 'employeedetail',
    loadChildren: () => import('./page/employeedetail/employeedetail.module').then( m => m.EmployeedetailPageModule)
  },
  {
    path: 'setting',
    loadChildren: () => import('./page/setting/setting.module').then( m => m.SettingPageModule)
  },
  {
    path: 'renewbusiness',
    loadChildren: () => import('./page/renewbusiness/renewbusiness.module').then( m => m.RenewbusinessPageModule)
  },
  {
    path: 'voucher',
    loadChildren: () => import('./page/voucher/voucher.module').then( m => m.VoucherPageModule)
  },
  {
    path: 'voucheradd',
    loadChildren: () => import('./page/voucheradd/voucheradd.module').then( m => m.VoucheraddPageModule)
  },
  {
    path: 'voucherdetail',
    loadChildren: () => import('./page/voucherdetail/voucherdetail.module').then( m => m.VoucherdetailPageModule)
  },
  {
    path: 'announce',
    loadChildren: () => import('./page/announce/announce.module').then( m => m.AnnouncePageModule)
  }

];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule {}
