<?php
header("Content-type:application/json; charset=UTF-8");          
header("Cache-Control: no-store, no-cache, must-revalidate");         
header("Cache-Control: post-check=0, pre-check=0", false);    
if(isset($_SERVER['HTTP_ORIGIN'])) {
header("Access-Control-Allow-Origin: {$_SERVER['HTTP_ORIGIN']}");
header('Access-Control-Allow-Credentials: true');
 //       header('Access-Control-Max-Age: 86400');    // cache for 1 day
}
// Access-Control headers are received during OPTIONS requests
if($_SERVER['REQUEST_METHOD'] == 'OPTIONS') {
if(isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_METHOD']))
header("Access-Control-Allow-Methods: GET, POST, OPTIONS");         
if(isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']))
header("Access-Control-Allow-Headers: {$_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']}");
exit(0);
}
// ส่วนการกำหนด header ให้กำหนดตามด้านบน ไม่เช่นนั้น app จะไม่สามารถส่งค่ามาได้
require_once("dbconnect.php");
$postdata = file_get_contents("php://input"); // ค่าที่ส่งมาจะถูกเก็บที่ไฟล์ 
if(isset($postdata)) { // ถ้ามีคาที่ส่งมา
$_POST = json_decode($postdata,true); // แปลงค่าที่ส่งมาเป็นตัวแปร array $_POST
}
$_error_msg = null;
$_success_msg = null;
if(
    isset($_POST['regid']) &&  $_POST['regid']!="" && 
    isset($_POST['uid']) &&  $_POST['uid']!=""
){ // 
     
     
    $_res_regID = $mysqli->real_escape_string(trim($_POST['regid']));
    $_res_UID = $mysqli->real_escape_string(trim($_POST['uid']));
 
    if(isset($_error_msg) && $_error_msg!=""){
        $json_data[]=array(  
            "success" => $_success_msg,
            "error" => $_error_msg
        );          
    }else{
 
        // เช็คในตารางก่อนว่า เครื่องมือถือนั้นๆ ได้เคยบันทึกข้อมูลมาก่อนแล้วหรือไม่ อิงจาก UUID
        $sql="
        SELECT notify_regid,notify_uid,notify_status FROM tbl_notify WHERE notify_uid = '".$_res_UID."'
        ";  
        $result = $mysqli->query($sql);
        if($result && $result->num_rows>0){ // ถ้าเคยบันทึกแล้ว
            $row = $result->fetch_assoc(); // ดึงค่ามาตรวจสอบ
            $_success_msg=1;
            if($row['notify_regid'] != $_res_regID){ // ถ้าค่าเดิม กับค่าหใม่ไม่เหมือนกัน ให้อัพเดทค่าใหม่
                $sql2 = "
                UPDATE tbl_notify SET 
                notify_regid = '".$_res_regID."' ,
                notify_status = '1' ,
                notify_updateadd = '".date('Y-m-d H:i:s')."'       
                WHERE notify_uid = '".$_res_UID."'
                ";
                $result2 = $mysqli->query($sql2);
                if($result2 && $mysqli->affected_rows>0){ // ถ้าอัพเดทสำเร็จ
                    $_success_msg=1;
                    $json_data[]=array(  
                        "success" => $_success_msg,
                        "error" => $_error_msg,
                        "regid" => $_res_regID,
                        "uid"=> $_res_UID,
                        "status"=> 1 
                    );              
                }else{ // ถ้าอัพเดทไม่สำเร็จ
                    $json_data[]=array(  
                        "success" => $_success_msg,
                        "error" => $_error_msg
                    );                  
                }
            }else{  // ถ้าค่าใหม่ เท่ากับค่าเดิม ใช้ค่าเดิม
                $json_data[]=array(  
                    "success" => $_success_msg,
                    "error" => $_error_msg,
                    "regid" => $row['notify_regid'],
                    "uid" => $row['notify_uid'],
                    "status" => $row['notify_status']
                );      
            }
        }else{ // ถ้ายังไม่มีข้อมูล ให้บันทีกข้อมูลใหม่
            $sql2 = "
            INSERT INTO tbl_notify SET 
            notify_uid = '".$_res_UID."' ,
            notify_regid = '".$_res_regID."' ,
            notify_status = '1' ,
            notify_dateadd = '".date('Y-m-d H:i:s')."'     
            ";
            $result2 = $mysqli->query($sql2);
            if($result2 && $mysqli->affected_rows>0){ // ถ้าบันทึกขอ้มูลสำเร็จ
                $_success_msg=1;
                $json_data[]=array(  
                    "success" => $_success_msg,
                    "error" => $_error_msg,
                    "regid" => $_res_regID,
                    "uid"=> $_res_UID,
                    "status"=> 1 
                );              
            }else{ // ถ้าบันทึกข้อมูลล้มเหลว
                $json_data[]=array(  
                    "success" => $_success_msg,
                    "error" => $_error_msg
                );                  
            }
        }
         
    }
    //
         
}else{ // ไม่มีการส่งข้อมูลใดๆ เข้ามา
    $_error_msg = "เกิดข้อผิดพลาด ไม่มีค่าส่งมา";
    $json_data[]=array(  
        "success" => $_success_msg,
        "error" => $_error_msg
    );              
}
// แปลง array เป็นรูปแบบ json string
if(isset($json_data)){
    $json= json_encode($json_data);  
    if(isset($_GET['callback']) && $_GET['callback']!=""){  
    echo $_GET['callback']."(".$json.");";      
    }else{  
    echo $json;  
    }  
}
?>