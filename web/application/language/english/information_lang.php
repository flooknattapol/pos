<?php	
$lang['welcome_message'] = 'Type message in German';
$lang['Dashboard'] = 'Dashboard';
$lang['Dormitory'] = 'Dormitory';
$lang['Rooms'] = 'Rooms';
$lang['Customer'] = 'Customer';
$lang['Invoice'] = 'Invoice';
$lang['MaCost'] = 'MA Cost';
$lang['Report'] = 'Report';
$lang['DormitoryName'] = 'Dormitory Name';
$lang['Branch'] = 'Branch';
$lang['Address'] = 'Address';
$lang['Save'] = 'Save';
$lang['Cancel'] = 'Cancel';
$lang['Edit'] = 'Edit';
$lang['Delete'] = 'Delete';
$lang['Contact'] = 'Contact';
$lang['Email'] = 'Email';
$lang['Tel'] = 'Tel';
$lang['Fax'] = 'Fax';
$lang['DormitoryList'] = 'Dormitory List';
$lang['Option'] = 'Option';
$lang['RoomType'] = 'Room Type';
$lang['RoomItem'] = 'Room Item';
$lang['RoomInDorm'] = 'Room In Dorm';
$lang['ViewDatail'] = 'View Details';
$lang['RoomTypeCode'] = 'Type Code';
$lang['RoomTypeName'] = 'Type Name';
$lang['RoomPrice'] = 'Prices';
$lang['Note'] = 'Note'; 
$lang['RoomTypeList'] = 'Room Type List';
$lang['RoomItemCode'] = 'Item Code';
$lang['RoomItemName'] = 'Item Name';
$lang['ItemPrice'] = 'Prices';
$lang['RoomItemList'] = 'Room Item List';
$lang['PleaseSelectDorm'] = 'Please select dormitory';
$lang['Next'] = 'Next';
$lang['RoomNumber'] = 'Room Number';
$lang['RoomFloor'] = 'Floor';
$lang['RoomInDormList'] = 'Room in Dormitory List';
$lang['CustomerName'] = 'Name';
$lang['CustomerSurName'] = 'SurName';
$lang['CustomerIDCard'] = 'ID Card';
$lang['CustomerAddress1'] = 'Address 1';
$lang['CustomerAddress2'] = 'Address 2';
$lang['Picture'] = 'Picture';
$lang['Deposit'] = 'Deposit';
$lang['NameSurName'] = 'Name & Surname';
$lang['Day'] = 'Day';
$lang['Week'] = 'Week';
$lang['Month'] = 'Month';
$lang['Year'] = 'Year';
$lang['Booking'] = 'Booking';
$lang['CheckInDate'] = 'Check In';
$lang['CheckOutDate'] = 'Check Out';
$lang['CustomerType'] = 'Customer Type'; 
$lang['DepositMonth'] = 'DepositMonth';
$lang['OldCustomer'] = 'Old customer'; 
$lang['NewCustomer'] = 'New customer';
$lang['FileInput'] = 'File input';
$lang['PictureIDCard'] = 'PictureIDCard';
$lang['Price'] = 'Price';
$lang['ContactDate'] = 'Contact Date';
$lang['ContactPerson'] = 'Contact Person';
$lang['Relatetion'] = 'Relatetion';
$lang['QtyPerson'] = 'Qty Person';
$lang['OptionNote'] = 'Option Note';
$lang['DisCountNote'] = 'DisCount Note';
$lang['Insurrance'] = 'Insurrance';
$lang['RoomStatus'] = 'Status';
$lang['RoomManage'] = 'Manage';
$lang['RoomAll'] = 'All';
$lang['RoomFull'] = 'Room Full';
$lang['RoomEmpty'] = 'Room Empty';
$lang['Vat'] = 'Vat';
$lang['Quatation'] = 'Quatation';
$lang['Invoice'] = 'Invoice';
$lang['Receipt'] = 'Receipt';
$lang['System'] = 'System';
$lang['Company'] = 'Company';
$lang['CompanyAddress'] = 'Company Address';
$lang['CompanyID'] = 'Company ID (13 digit)';
$lang['CompanyID2'] = 'Company ID';
$lang['CusCompany'] = 'Customer Company';
$lang['CusCompanyAddress'] = 'Customer Company Address';
$lang['CusCompanyID'] = 'Customer Company ID (13 digit)';
$lang['CusCompanyID2'] = 'Customer Company ID';
$lang['VatBook'] = 'Vat Book';
$lang['BookNo'] =  'Book No.';
$lang['NumNo'] =  'Num No.';
$lang['CompanyNameEx'] =  'ผู้มีหน้าที่หักภาษีณ ที่จ่าย';
$lang['CompanyNameEx2'] =  'ผู้ถูกหักภาษีณ ที่จ่าย';
$lang['withHoldingTax'] =  'ภาษีที่หักและนำส่งไว้';
$lang['PayPrices'] =  'จำนวนเงินที่จ่าย';
$lang['PayDate'] =  'วันเดือนหรือปีภาษีที่จ่าย';
$lang['PayDate'] =  'วันเดือนหรือปีภาษีที่จ่าย';
$lang['ListOfWHT'] =  'รายการภาษีหัก ณ ที่จ่าย';
$lang['Date'] =  'วันที่';
$lang['DueDate'] =  'Due Date';
$lang['CustomerID'] =  'Customer ID';
$lang['InvoiceRef'] =  'Invoice Ref';
$lang['DateRef'] =  'Date Ref';
$lang['Company'] =  'Company';
$lang['CompanyDetail'] =  'Company Detail';
$lang['Contact'] =  'Contact';
$lang['Address1'] =  'Address1';
$lang['Address2'] =  'Address2';
$lang['Address3'] =  'Address3';
$lang['taxid'] =  'taxid';
$lang['website'] =  'website';
$lang['savesuccess'] = 'save success';
$lang['error'] = 'error';
$lang['Project'] = 'Project';
$lang['CompanyCode'] = 'Company Code';
$lang['ListOfCustomer'] = 'List Of Customer';
$lang['DoYouWantToDelete'] = 'Do you want to delete ?';
$lang['NoCancel'] = 'No';
$lang['Yes'] = 'Yes';
$lang['Close'] = 'Close';
$lang['Confirmation'] = 'Confirmation';
$lang['Information'] = 'Information';
$lang['Search'] = 'Search';
$lang['ResetSearch'] = 'Reset Search';
$lang['Search'] = 'Search';
$lang['Add'] = 'Add';
$lang['Total'] = 'Total';
$lang['Records'] = 'Records';
$lang['ResultsPerPage'] = 'ResultsPerPage';
$lang['Previous'] = 'Previous';
$lang['Next'] = 'Next';
$lang['ProjectName'] = 'Project Name';
$lang['budget'] = 'Budget';
$lang['note'] = 'Note';
$lang['projectyear'] = 'Project Year';
$lang['ListOfProject'] = 'List of Project';
$lang['Require'] = 'Require';
$lang['Unit'] = 'Unit';
$lang['DESCRIPTION'] = 'DESCRIPTION';
$lang['Quantity'] = 'Qty';
$lang['Price'] = 'Price';
$lang['Amount'] = 'Amount';
$lang['ITEM'] = 'ITEM';
$lang['SubTotal'] = 'Sub Total';
$lang['VAT'] = 'VAT 7%';
$lang['Total'] = 'Total';
$lang['sub_alphabet'] = 'รวมเงินภาษีที่หักนำส่ง (ตัวอักษร) ';
$lang['SignName'] = 'ลงชื่อผู้จ่ายเงิน';
$lang['SignDate'] = 'วัน เดือน ปี ที่ออกหนังสือรับรอง';
$lang['ListOfQuatation'] = 'List of quatation';
$lang['Print'] = 'Print';
$lang['Payment'] = 'Payment term and condition';
$lang['SocialFund'] = 'Social Fund';
$lang['Fund'] = 'Fund';
$lang['Income'] = 'Income';
$lang['revenue'] = 'revenue';
$lang['expenditure'] = 'expenditure';
$lang['Item'] = 'รายละเอียด';
$lang['RequireMoreZero'] = 'ต้องมีค่ามากกว่า 0';
$lang['ListOfIncome'] = 'รายการรายรับ รายจ่าย';
$lang['Status'] = 'Status';
$lang['StatusWorking'] = 'กำลังดำเนินการ';
$lang['StatusPending'] = 'ยกเลิก';
$lang['StatusSuccess'] = 'อนุมัติ';
$lang['PleaseSelectStatus'] = 'กรุณาเลือกสถานะ';
$lang['ReportTotal']= 'รายงานแบบรวมทุก Project';
$lang['ReportSeperate']= 'รายงานแบบแยก Project';
$lang['Payer']= 'ผู้จ่ายเงิน';
$lang['Position']= 'ตำแหน่ง';
$lang['Level']= 'ระดับ';
$lang['New']= 'New';
$lang['Detail']= 'Detail';
$lang['Code']= 'รหัส';
$lang['Name']= 'ชื่อ';
$lang['EmpType']= 'ประเภทพนักงาน';
$lang['Create']= 'สร้าง';
$lang['PoReference']= 'PO Reference';
$lang['Po']= 'Purchase order';
$lang['PoNum']= 'PO number';
$lang['Home']='Home';