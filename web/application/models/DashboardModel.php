<?php
  
class DashboardModel extends CI_Model {
	
	private $id = 'ID';
 
    public function __construct() {
        parent::__construct();
    }
	
	
	public function getHeader($dataPost){
		$idSession = isset($dataPost['idSession']) ? $dataPost['idSession'] : "";

		// print_r($dataPost);die();
		$this->load->model('UserModel', '', TRUE);
		if($idSession != ""){
			$idsession = $idSession; 
		}else{
			$idsession = $this->session->userdata('id');
		}
		$userPosition = $this->UserModel->getPostion($idsession);
		
		// print_r($dataPost);die();
		$sql = "SELECT date(ts_transection.TimeStamp)as Day,
		SUM(detail.TotalOrder) as Total,
		SUM(detail.TotalCost) as Cost,
		SUM(tbrefund.TotalRefund) as Refund,
		SUM(tbdiscount.TotalDiscount) as Discount,
        SUM(tbcost.TotalCostRefund)as TotalCostRefund
		FROM ts_transection
				LEFT JOIN (SELECT ts_transection_detail.*,
						   SUM(ts_transection_detail.PRICE) as TotalOrder,
						   SUM(ts_transection_detail.COST) AS TotalCost 
						   FROM ts_transection_detail WHERE ts_transection_detail.IsActive = 1 
						   GROUP BY ts_transection_detail.TRANSECTION_ID) detail 
						   ON ts_transection.ID = detail.TRANSECTION_ID
				LEFT JOIN (SELECT report_refund.*,
						   SUM(report_refund.AMOUNT) as TotalRefund
						   FROM report_refund
						   GROUP BY report_refund.TRANSECTION_ID) tbrefund
						   ON ts_transection.ID = tbrefund.TRANSECTION_ID
				LEFT JOIN (SELECT ts_discount.*,
						   SUM(ts_discount.AMOUNT_DISCOUNT) as TotalDiscount
						   FROM ts_discount
						   GROUP BY ts_discount.TRANSECTION_ID) tbdiscount
						   ON ts_transection.ID = tbdiscount.TRANSECTION_ID
				LEFT JOIN (SELECT ts_transection_detail.*,
						   SUM(ts_transection_detail.COST) AS TotalCostRefund
						   FROM ts_transection_detail WHERE ts_transection_detail.IsActive = 0
						   GROUP BY ts_transection_detail.TRANSECTION_ID) tbcost 
						   ON ts_transection.ID = tbcost.TRANSECTION_ID
				WHERE BUSINESS_ID = '".$userPosition['BUSINESS_ID']."'
				AND date(ts_transection.TimeStamp) BETWEEN '".$dataPost['DateStart']."' AND '".$dataPost['DateEnd']."'
				";
		$query = $this->db->query($sql);
		return  $query->result_array();
	}
	public function getDashboardNameList($dataPost,$idSession){

		$this->load->model('UserModel', '', TRUE);
		if($idSession != ""){
			$idsession = $idSession; 
		}else{
			$idsession = $this->session->userdata('id');
		}
		$userPosition = $this->UserModel->getPostion($idsession);
		
		$sql = "SELECT date(ts_transection.TimeStamp)as Day,
		SUM(detail.TotalOrder) as Total,
		SUM(detail.TotalCost) as Cost,
		SUM(tbrefund.TotalRefund) as Refund,
		SUM(tbdiscount.TotalDiscount) as Discount,
        SUM(tbcost.TotalCostRefund)as TotalCostRefund
		FROM ts_transection
				LEFT JOIN (SELECT ts_transection_detail.*,
						   SUM(ts_transection_detail.PRICE) as TotalOrder,
						   SUM(ts_transection_detail.COST) AS TotalCost 
						   FROM ts_transection_detail WHERE ts_transection_detail.IsActive = 1 
						   GROUP BY ts_transection_detail.TRANSECTION_ID) detail 
						   ON ts_transection.ID = detail.TRANSECTION_ID
				LEFT JOIN (SELECT report_refund.*,
						   SUM(report_refund.AMOUNT) as TotalRefund
						   FROM report_refund
						   GROUP BY report_refund.TRANSECTION_ID) tbrefund
						   ON ts_transection.ID = tbrefund.TRANSECTION_ID
				LEFT JOIN (SELECT ts_discount.*,
						   SUM(ts_discount.AMOUNT_DISCOUNT) as TotalDiscount
						   FROM ts_discount
						   GROUP BY ts_discount.TRANSECTION_ID) tbdiscount
						   ON ts_transection.ID = tbdiscount.TRANSECTION_ID
				LEFT JOIN (SELECT ts_transection_detail.*,
						   SUM(ts_transection_detail.COST) AS TotalCostRefund
						   FROM ts_transection_detail WHERE ts_transection_detail.IsActive = 0 
						   GROUP BY ts_transection_detail.TRANSECTION_ID) tbcost 
						   ON ts_transection.ID = tbcost.TRANSECTION_ID
				WHERE BUSINESS_ID = '".$userPosition['BUSINESS_ID']."'
				AND date(ts_transection.TimeStamp) BETWEEN '".$dataPost['DateStart']."' AND '".$dataPost['DateEnd']."'
				GROUP BY date(ts_transection.TimeStamp)
				ORDER BY Day ASC";

		$query = $this->db->query($sql);
		return  $query->result_array();
	}
	public function getTotal($dataPost,$idSession ){
		$this->load->model('UserModel', '', TRUE);
		if($idSession != ""){
			$idsession = $idSession; 
		}else{
			$idsession = $this->session->userdata('id');
		}
		$userPosition = $this->UserModel->getPostion($idsession);
		
		$sql = "SELECT date(ts_transection.TimeStamp)as Day,
		SUM(detail.TotalOrder) as Total,
		SUM(detail.TotalCost) as Cost,
		SUM(tbrefund.TotalRefund) as Refund,
		SUM(tbdiscount.TotalDiscount) as Discount,
        SUM(tbcost.TotalCostRefund)as TotalCostRefund
		FROM ts_transection
				LEFT JOIN (SELECT ts_transection_detail.*,
						   SUM(ts_transection_detail.PRICE) as TotalOrder,
						   SUM(ts_transection_detail.COST) AS TotalCost 
						   FROM ts_transection_detail WHERE ts_transection_detail.IsActive = 1 
						   GROUP BY ts_transection_detail.TRANSECTION_ID) detail 
						   ON ts_transection.ID = detail.TRANSECTION_ID
				LEFT JOIN (SELECT report_refund.*,
						   SUM(report_refund.AMOUNT) as TotalRefund
						   FROM report_refund
						   GROUP BY report_refund.TRANSECTION_ID) tbrefund
						   ON ts_transection.ID = tbrefund.TRANSECTION_ID
				LEFT JOIN (SELECT ts_discount.*,
						   SUM(ts_discount.AMOUNT_DISCOUNT) as TotalDiscount
						   FROM ts_discount
						   GROUP BY ts_discount.TRANSECTION_ID) tbdiscount
						   ON ts_transection.ID = tbdiscount.TRANSECTION_ID
				LEFT JOIN (SELECT ts_transection_detail.*,
						   SUM(ts_transection_detail.COST) AS TotalCostRefund
						   FROM ts_transection_detail WHERE ts_transection_detail.IsActive = 0 
						   GROUP BY ts_transection_detail.TRANSECTION_ID) tbcost 
						   ON ts_transection.ID = tbcost.TRANSECTION_ID
				WHERE BUSINESS_ID = '".$userPosition['BUSINESS_ID']."'
				AND date(ts_transection.TimeStamp) BETWEEN '".$dataPost['DateStart']."' AND '".$dataPost['DateEnd']."'
				GROUP BY date(ts_transection.TimeStamp)
				ORDER BY Day ASC";

		$query = $this->db->query($sql);
		
		return  $query->num_rows() ;
	}
	public function getGraph($dataPost)
	{

		try {
			$PageIndex =  isset($dataPost['PageIndex']) ? $dataPost['PageIndex'] : 1;
			$PageSize =  isset($dataPost['PageSize']) ? $dataPost['PageSize'] : 20;
			$direction =  isset($dataPost['SortColumn']) ? $dataPost['SortColumn'] : "";
			$SortOrder = isset($dataPost['SortOrder']) ? $dataPost['SortOrder'] : "asc";
			$idSession = isset($dataPost['idSession']) ? $dataPost['idSession'] : "";
			$dataModel = isset($dataPost['mSearch']) ? $dataPost['mSearch'] : "";

			$offset = ($PageIndex - 1) * $PageSize;

			$result['status'] = true;
			$result['message'] = $this->getDashboardNameList($dataModel,$idSession);
			$result['totalRecords'] = $this->getTotal($dataModel,$idSession);
			$result['toTalPage'] = ceil($result['totalRecords'] / $PageSize);

		} catch (Exception $ex) {
			$result['status'] = false;
			$result['message'] = "exception: " . $ex;
		}

		return  $result;
	}
}
?>