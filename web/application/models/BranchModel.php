<?php
  
class BranchModel extends CI_Model {
	
    private $tbl_name = 'ma_branch';
	private $id = 'ID';
 
    public function __construct() {
        parent::__construct();
    }
	
	public function getQuatationDetailById($id){
		$this->db->where($this->id, $id);
		return $this->db->get($this->tbl_name);
	}
	
	public function insert($modelData){
		 
	 	$this->db->insert($this->tbl_name, $modelData); 
		return $this->db->insert_id(); 
    }
     
    public function update($id, $modelData){
        $this->db->where($this->id, $id);
        return $this->db->update($this->tbl_name, $modelData);
    }
	public function updateItem($dataModel){	
		

		$dataShipment=array(
			"id"=>$dataModel['id']
			 );
		$query = $this->db->select('id, item_deliver')->where($dataShipment)->get('quatation_detail')->row_array(); 
		$dataModel['DeliverItem'] = $query['item_deliver'] + $dataModel['DeliverItem'];
		$this->db->set('item_deliver',$dataModel['DeliverItem']);
		$this->db->where('id',$dataModel['id']);
		$this->db->update('quatation_detail');
		return $dataModel;
	}
	public function updateStockLog($dataModel,$id){
		// $dataProject=array(
		// 	"id"=>$id
		// 	 );
		//$query = $this->db->select('id, Stock')->where($dataProject)->get('project')->row_array(); 

		$sql = "SELECT * FROM project WHERE id = '".$id."'";
		$query = $this->db->query($sql)->row_array();
		echo  $dataModel['DeliverItem'];
		echo $sql;
		$query['Stock'] = $query['Stock'] - $dataModel['DeliverItem'];
		echo $query['Stock'];
		$this->db->set('Stock',$query['Stock']);
		$this->db->where('id',$id);
		$this->db->update('project');
		return $dataModel;
	}
	public function logdeliver($dataModel){	
		
		$data = array(
			'item_deliver' => $dataModel['DeliverItem'],
			'date_deliver' => $dataModel['Date_deliver'],
			'code_order' => $dataModel['IssueOrder'],
			'id_order' => $dataModel['id'],
			'id_customer	' => $dataModel['cus_id']
		);
		
		$this->db->insert('logdeliver',$data);
		return $dataModel;
	}
	public function listUpdate($id, $listData ,$ValueInList,$position,$idcheck){

		$this->load->model('UserPositionModel','',TRUE); 
		$this->load->model('UserModel','',TRUE); 
		$nResult  = 0;
		foreach ($listData as $key => $data)
		{
			if($data['CreateDate'] != null && $data['CreateDate'] != "0000-00-00"){
				$data['CreateDate'] = date('Y-m-d', strtotime($data['CreateDate']));
			}
			$data['BUSINESS_ID'] = $id;
			if ($data['ID'] == 0) {  
				$data['UID'] = uniqid();
				$data['UID'] = md5($data['UID']);
				 $nResult = $this->insert($data); 

				 if($nResult > 0 && $idcheck == 0){
					
					$position['BRANCH_ID'] = $nResult;
					$nResult = $this->UserPositionModel->insert($position);
				 }
		    }
		    else {  
		    	$nResult = $this->update($data['ID'], $data);
		    }		
		}
		foreach ($ValueInList as $key => $data)
		{
			if($data['ID'] != 0){
				$this->db->where('ID',$data['ID']);
				$this->db->delete('ma_branch');
			}
		}
		//echo $nResult; die();
        return $nResult;
    }
	// public function DeleteValueInList($listData){

	// 	print_r($listData);
	// 	$nResult  = 0;

	// 	foreach ($listData as $key => $data)
	// 	{
	// 		if($data['id'] != 0){
	// 			$this->db->where('id',$data['id']);
	// 			$this->db->delete('quatation_detail');
	// 		}
	// 	}
    //     return $nResult;
    // }
 
	public function getQuatationDetailModel($id){
        //return $this->db->count_all($this->tbl_name);
        
        //$this->db->select('id','name','contact','address1','address2','address3','tel','email','taxid','website');
		$this->db->where('id', $id);
        $query =  $this->db->get($this->tbl_name);
		
		return $query->result_array();
    }
	
	public function getQuatationDetailListById($id){
		
		$sql  = "SELECT quatation_detail.*,project.name FROM quatation_detail 
		LEFT JOIN project ON project.id = quatation_detail.item_no
		WHERE quatation_detail.qt_id = '".$id."'";
        $query =  $this->db->query($sql);

		return $query->result_array();
    }
	public function getItemProduct($id){
		$sql = "SELECT * FROM project WHERE id = '$id'";
		$query = $this->db->query($sql);		 

		return  $query->result_array();
	}
	public function LogDeliverList($id){
		$sql = "SELECT * FROM logdeliver WHERE id_order = '$id'";
		$query = $this->db->query($sql);		 

		return  $query->result_array();
	}
	public function getSearchQuery($sql, $dataModel){
		
		//print_r($dataModel);
		
		/*if(isset($dataModel['name']) && $dataModel['name'] != ""){
		 	$sql .= " and name like '%".$this->db->escape_str( $dataModel['name'])."%' ";
		}
		
		if(isset($dataModel['branch']) && $dataModel['branch'] != ""){
		 	$sql .= " and branch like '%".$this->db->escape_str( $dataModel['branch'])."%' ";
		}
		
		if(isset($dataModel['contact']) && $dataModel['contact'] != ""){
		 	$sql .= " and contact like '%".$this->db->escape_str( $dataModel['contact'])."%' ";
		}
		
		if(isset($dataModel['email']) && $dataModel['email'] != ""){
		 	$sql .= " and email like '%".$this->db->escape_str( $dataModel['email'])."%' ";
		}*/
		
		return $sql;
	}
	
	public function getTotal($dataModel ){
		
		$sql = "SELECT * FROM ". $this->tbl_name  ." WHERE deleteflag = 0  ";
				
		$sql =  $this->getSearchQuery($sql, $dataModel);
		
		$query = $this->db->query($sql);		 
		
		return  $query->num_rows() ;
	}
	
	public function getQuatationDetailModelList($dataModel, $limit = 10, $offset = 0, $order = '', $direction = 'asc'){
		
		$sql = "SELECT * FROM ". $this->tbl_name . " WHERE deleteflag = 0  "; 
		
		$sql =  $this->getSearchQuery($sql, $dataModel);	
		
		 
		// if($order != ""){
			// $this->db->order_by($order, $direction);
		// }else{
			// $this->db->order_by($this->id ,$direction); 
		// }
		
		if($order != ""){
			$sql .= " ORDER BY ".$order." ".$direction;
		}else{
			$sql .= " ORDER BY ".$this->id." ".$direction;
		}
		
		$sql .= " LIMIT ".$offset.", ".$limit;
		 
		$query = $this->db->query($sql);
		return  $query->result_array();
	}
 
	public function getBranchComboList($id){
		
		if($id > 0){
			$sql = "SELECT * FROM ". $this->tbl_name . " WHERE IsActive = 1 AND BUSINESS_ID = '".$id."'";
		}else{
			$sql = "SELECT * FROM ". $this->tbl_name . " WHERE IsActive = 1";
		}
		$query = $this->db->query($sql);
		// echo $sql;
		// print_r($query);
		// die();
		return  $query->result_array();
	}
	###controller###
	public function getComboList($dataPost){
		try{ 
			$idSession = isset($dataPost['idSession']) ? $dataPost['idSession'] : "";
			$this->load->model('UserModel', '', TRUE);
			if($idSession != ""){
				$idsession = $idSession; 
			}else{
				$idsession = $this->session->userdata('id');
			}
			$result['status'] = true;
			$result['message'] = $this->getBranchComboList($idsession);
		}catch(Exception $ex){
			$result['status'] = false;
			$result['message'] = "exception: ".$ex;
		}
		
		return $result;	
	}
}
?>