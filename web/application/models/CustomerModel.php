<?php
  
class CustomerModel extends CI_Model {
	
    private $tbl_name = 'ma_customer';
	private $id = 'ID';
 
    public function __construct() {
        parent::__construct();
    }
	
	public function getCustomerNameById($id){
		$this->db->where($this->id, $id);
		return $this->db->get($this->tbl_name);
	}
	
	public function insert($modelData){
		 
	 	$this->db->insert($this->tbl_name, $modelData); 
		return $this->db->insert_id(); 
    }
     
    public function update($id, $modelData){
        $this->db->where($this->id, $id);
        return $this->db->update($this->tbl_name, $modelData);
    }
	
	public function getCustomerNameAllList(){
        //return $this->db->count_all($this->tbl_name);
        
        $this->db->select('ID','NAME','SKU','PRICE','UNIT');
		$this->db->where('IsActive', 1);
        $query =  $this->db->get($this->tbl_name);
		
		return $query->result_array();
    }
	
	public function getCustomerModel($id){
        //return $this->db->count_all($this->tbl_name);
        
        //$this->db->select('id','name','contact','address1','address2','address3','tel','email','taxid','website');
		$this->db->where('IsActive', 1);
		$this->db->where($this->id, $id);
        $query =  $this->db->get($this->tbl_name);
		
		return $query->result_array();
    }
	
	public function getSearchQuery($sql, $dataModel){
		

		if(isset($dataModel['NAME']) && $dataModel['NAME'] != ""){
			$sql .= " and ma_customer.NAME like '%".$this->db->escape_str( $dataModel['NAME'])."%' ";
		}
		return $sql;
	}
	
	public function getTotal($dataModel,$idSession){

		$this->load->model('UserModel', '', TRUE);
		if($idSession != ""){
			$idsession = $idSession; 
		}else{
			$idsession = $this->session->userdata('id');
		}
		$userPosition = $this->UserModel->getPostion($idsession);
		
		if($this->session->userdata('user') == "admin"){
			$sql = "SELECT ma_customer.* , ma_customer_category.NAME As CATEGORYNAME FROM ma_customer
			LEFT JOIN ma_customer_category ON ma_customer.CATEGORY_ID = ma_customer_category.ID
			WHERE ma_customer.IsActive = 1";
		}else{
			$sql = "SELECT ma_customer.* , ma_customer_relation.BUSINESS_ID ,ma_customer_relation.REWARDPOINT, ma_customer_category.NAME As CATEGORYNAME FROM ma_customer
			LEFT JOIN ma_customer_category ON ma_customer.CATEGORY_ID = ma_customer_category.ID
			RIGHT JOIN ma_customer_relation ON ma_customer.ID = ma_customer_relation.CUSTOMER_ID
			WHERE ma_customer_relation.IsActive = 1  AND ma_customer.IsActive = 1  AND ma_customer_relation.BUSINESS_ID = '".$userPosition['BUSINESS_ID']."'"; 	
		}				
		$sql =  $this->getSearchQuery($sql, $dataModel);
		
		$query = $this->db->query($sql);		 
		
		return  $query->num_rows() ;
	}
	
	public function getCustomerNameList($dataModel, $limit = 10, $offset = 0, $order = '', $direction = 'asc',$idSession){
		
		$this->load->model('UserModel', '', TRUE);
		if($idSession != ""){
			$idsession = $idSession; 
		}else{
			$idsession = $this->session->userdata('id');
		}
		$userPosition = $this->UserModel->getPostion($idsession);
		
		if($this->session->userdata('user') == "admin"){
		$sql = "SELECT ma_customer.* , ma_customer_category.NAME As CATEGORYNAME FROM ma_customer
		LEFT JOIN ma_customer_category ON ma_customer.CATEGORY_ID = ma_customer_category.ID
		WHERE ma_customer.IsActive = 1";
		}else{
			$sql = "SELECT ma_customer.* , ma_customer_relation.BUSINESS_ID ,ma_customer_relation.REWARDPOINT, ma_customer_category.NAME As CATEGORYNAME FROM ma_customer
			LEFT JOIN ma_customer_category ON ma_customer.CATEGORY_ID = ma_customer_category.ID
			RIGHT JOIN ma_customer_relation ON ma_customer.ID = ma_customer_relation.CUSTOMER_ID
			WHERE ma_customer_relation.IsActive = 1  AND ma_customer.IsActive = 1  AND ma_customer_relation.BUSINESS_ID = '".$userPosition['BUSINESS_ID']."'"; 	
		}
		$sql =  $this->getSearchQuery($sql, $dataModel);	
		// die();
		if($order != ""){
			$sql .= " ORDER BY ".$order." ".$direction;
		}else{
			$sql .= " ORDER BY ".$this->id." ".$direction;
		}
		
		$sql .= " LIMIT $offset, $limit";
		
		
		 
		$query = $this->db->query($sql);
		return  $query->result_array();
	}
	
	public function deleteCustomername($id){
		// echo $id; die();
		$result = false;
		try{
			$query = $this->getCustomerNameById($id);
			// echo $id;die();
			// $modelData;			
			foreach ($query->result() as $row)
			{
			   		
				$modelData = array( 
					//'update_date' => date("Y-m-d H:i:s"),
					//'update_user' => $this->session->userdata('user_name'),
					// 'Edit'=>$this->session->userdata('user'),
					'IsActive' => 0 //$row->Customer_IsActive 
				); 
			}
			// echo $this->tbl_name;
			// print_r($modelData);die();
			if($this->session->userdata('user') == "admin"){
				$this->db->set('IsActive',0);
				$this->db->where("ID", $id);
				return $this->db->update("ma_customer");
			}
			else{
				$this->db->set('IsActive',0);
				$this->db->where("CUSTOMER_ID", $id);
				return $this->db->update("ma_customer_relation");
			}
		
			//return $this->update($id, $modelData);
			//$sql = "Delete FROM ". $this->tbl_name; 
			//return  $this->db->query($sql);
			
		}catch(Exception $ex){
			return $result;
		}
	}
	public function getCustomerComboList(){
		
		$this->load->model('UserModel', '', TRUE);
		if(! $this->session->userdata('validated')){

		}else{
			$idsession = $this->session->userdata('id');
		}
		$userPosition = $this->UserModel->getPostion($idsession);

		$sql = "SELECT * FROM  ma_customer
		WHERE IsActive = 1  AND BUSINESS_ID = '".$userPosition['BUSINESS_ID']."'"; 
		
		$query = $this->db->query($sql);
		return  $query->result_array();
	}
	#### controller ###
	public function add($dataPost )
	{
		$nResult = 0;
		try {
			$idSession = isset($dataPost['idSession']) ? $dataPost['idSession'] : "";
			$this->load->model('UserModel', '', TRUE);
			if($idSession != ""){
				$idsession = $idSession; 
			}else{
				$idsession = $this->session->userdata('id');
			}
			$userPosition = $this->UserModel->getPostion($idsession);

			$data['ID'] =  isset($dataPost['ID']) ? $dataPost['ID'] : 0;
			$data['NAME'] =  isset($dataPost['NAME']) ? $dataPost['NAME'] : "";
			$data['TEL'] =  isset($dataPost['TEL']) ? $dataPost['TEL'] : "";
			$data['EMAIL'] =  isset($dataPost['EMAIL']) ? $dataPost['EMAIL'] : "";
			$data['NOTE'] =  isset($dataPost['NOTE']) ? $dataPost['NOTE'] : "";
			$data['userID'] =  isset($dataPost['userID']) ? $dataPost['userID'] : "";
			$data['CATEGORY_ID'] =  isset($dataPost['CATEGORY_ID']) ? $dataPost['CATEGORY_ID'] : "1";
			$data['IsActive'] = 1;
			$data['pictureUrl'] =  isset($dataPost['pictureUrl']) ? $dataPost['pictureUrl'] : "";
			$data['IMAGE'] =  isset($dataPost['IMAGE']) ? $dataPost['IMAGE'] : "";
			
			$sql = "SELECT * FROM ma_customer WHERE TEL = '".$data['TEL']."'";
			$num = $this->db->query($sql)->num_rows();
			if ($data['ID'] == 0) {
				if($num  == 0){
					$nResult = $this->insert($data);
					if ($nResult > 0) {
						$result['status'] = true;
						$result['message'] = $this->lang->line("savesuccess");
					
					} else {
						$result['status'] = false;
						$result['message'] = $this->lang->line("error");
					}
				}else{
					$result['status'] = false;
					$result['message'] = "เบอร์โทรศัพนี้ถูกใช้งานไปแล้ว";
				}
			} else {
				$nResult = $this->update($data['ID'], $data);
				if ($nResult > 0) {
					$result['status'] = true;
					$result['message'] = $this->lang->line("savesuccess");
				
				} else {
					$result['status'] = false;
					$result['message'] = $this->lang->line("error");
				}
			}
					
		} catch (Exception $ex) {
			$result['status'] = false;
			$result['message'] = "exception: " . $ex;
		}

		return  $result;
	}

	public function delete($dataPost )
	{
		try {
			$id =  isset($dataPost['ID']) ? $dataPost['ID'] : 0; // $this->input->post('ap_id');

			$bResult = $this->deleteCustomername($id);

			if ($bResult) {
				$result['status'] = true;
				$result['message'] = $this->lang->line("savesuccess");
			} else {
				$result['status'] = false;
				$result['message'] = $this->lang->line("error_faliure");
			}
		} catch (Exception $ex) {
			$result['status'] = false;
			$result['message'] = "exception: " . $ex;
		}

		return  $result;
	}
	public function addtobusiness($dataPost )
	{
		try {
			$idSession = isset($dataPost['idSession']) ? $dataPost['idSession'] : "";
			$this->load->model('UserModel', '', TRUE);
			if($idSession != ""){
				$idsession = $idSession; 
			}else{
				$idsession = $this->session->userdata('id');
			}
			$userPosition = $this->UserModel->getPostion($idsession);

			$TEL =  isset($dataPost['TEL']) ? $dataPost['TEL'] : "";


			$sql = "SELECT * FROM ma_customer WHERE TEL = '".$TEL."'";
			$res = $this->db->query($sql)->row_array();
			if($res != ""){
				$sql = "SELECT * FROM ma_customer_relation WHERE CUSTOMER_ID = '".$res['ID']."' AND BUSINESS_ID = '".$userPosition['BUSINESS_ID']."'";
				$num = $this->db->query($sql)->num_rows();
				if($num == 0){

					$data['CUSTOMER_ID'] = $res['ID'];
					$data['BUSINESS_ID'] = $userPosition['BUSINESS_ID'];
					$this->db->insert("ma_customer_relation", $data); 
					$bResult = $this->db->insert_id(); 
					if ($bResult > 0) {
						$result['status'] = true;
						$result['message'] = $this->lang->line("savesuccess");
					} else {
						$result['status'] = false;
						$result['message'] = $this->lang->line("error");
					}
				}else{
					$result['status'] = false;
					$result['message'] = "คุณอยู่ในรายชื่ออยู่แล้ว";
				}
			}else{
				$result['status'] = false;
				$result['message'] = "ไม่พบชื่อลูกค้า";
			}
			// if ($bResult) {
			// 	$result['status'] = true;
			// 	$result['message'] = $this->lang->line("savesuccess");
			// } else {
			// 	$result['status'] = false;
			// 	$result['message'] = $this->lang->line("error_faliure");
			// }
		} catch (Exception $ex) {
			$result['status'] = false;
			$result['message'] = "exception: " . $ex;
		}

		return  $result;
	}

	public function getList($dataPost )
	{

		try {

			$PageIndex =  isset($dataPost['PageIndex']) ? $dataPost['PageIndex'] : 1;
			$PageSize =  isset($dataPost['PageSize']) ? $dataPost['PageSize'] : 20;
			$direction =  isset($dataPost['SortColumn']) ? $dataPost['SortColumn'] : "";
			$SortOrder = isset($dataPost['SortOrder']) ? $dataPost['SortOrder'] : "asc";
			$dataModel = isset($dataPost['mSearch']) ? $dataPost['mSearch'] : "";
			$idSession = isset($dataPost['idSession']) ? $dataPost['idSession'] : "";


			$offset = ($PageIndex - 1) * $PageSize;

			$result['status'] = true;
			$result['message'] = $this->getCustomerNameList($dataModel, $PageSize, $offset, $direction, $SortOrder,$idSession );
			$result['totalRecords'] = $this->getTotal($dataModel,$idSession );
			$result['toTalPage'] = ceil($result['totalRecords'] / $PageSize);

		} catch (Exception $ex) {
			$result['status'] = false;
			$result['message'] = "exception: " . $ex;
		}

		return  $result;
	}
	public function getComboList(){
		try{ 
			$result['status'] = true;
			$result['message'] = $this->getCustomerComboList();
		}catch(Exception $ex){
			$result['status'] = false;
			$result['message'] = "exception: ".$ex;
		}
		
		return $result;	
	}
}
?>