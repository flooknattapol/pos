<?php
  
class GoodsTypeModel extends CI_Model {
	
    private $tbl_name = 'ma_goodstype';
	private $id = 'ID';
 
    public function __construct() {
        parent::__construct();
    }
	
	public function getGoodsTypeNameById($id){
		$this->db->where($this->id, $id);
		return $this->db->get($this->tbl_name);
	}
	
	public function insert($modelData){
		 
	 	$this->db->insert($this->tbl_name, $modelData); 
		return $this->db->insert_id(); 
    }
     
    public function update($id, $modelData){
        $this->db->where($this->id, $id);
        return $this->db->update($this->tbl_name, $modelData);
    }
	
	public function getGoodsTypeNameAllList(){
        //return $this->db->count_all($this->tbl_name);
        
        $this->db->select('ID','NAME','SKU','PRICE','UNIT');
		$this->db->where('IsActive', 1);
        $query =  $this->db->get($this->tbl_name);
		
		return $query->result_array();
    }
	
	public function getGoodsTypeModel($id){
        //return $this->db->count_all($this->tbl_name);
        
        //$this->db->select('id','name','contact','address1','address2','address3','tel','email','taxid','website');
		$this->db->where('IsActive', 1);
		$this->db->where($this->id, $id);
        $query =  $this->db->get($this->tbl_name);
		
		return $query->result_array();
    }
	
	public function getSearchQuery($sql, $dataModel){
		
		//print_r($dataModel);
		// 	if(isset($dataModel['code']) && $dataModel['code'] != ""){
		// 		$sql .= " and emp_code like '%".$this->db->escape_str( $dataModel['code'])."%' ";
		//    }
		
		// 	if(isset($dataModel['code_id']) && $dataModel['code_id'] != ""){
		// 	 	$sql .= " and id_card_number like '%".$this->db->escape_str( $dataModel['code_id'])."%' ";
		// 	}
		
		if(isset($dataModel['Member_Name']) && $dataModel['Member_Name'] != ""){
		 	$sql .= " and ma_member.NAME like '%".$this->db->escape_str( $dataModel['Member_Name'])."%' ";
		}
		if(isset($dataModel['NAME']) && $dataModel['NAME'] != ""){
			$sql .= " and ma_goodsType.NAME like '%".$this->db->escape_str( $dataModel['NAME'])."%' ";
	   	}
	   	if(isset($dataModel['SKU']) && $dataModel['SKU'] != ""){
		$sql .= " and ma_goodsType.SKU like '%".$this->db->escape_str( $dataModel['SKU'])."%' ";
   		}
		// 	if(isset($dataModel['lastname']) && $dataModel['lastname'] != ""){
		// 	 	$sql .= " and lastname_th like '%".$this->db->escape_str( $dataModel['lastname'])."%' ";
		// 	}
			
		// 	if($dataModel['flag']==1){
		// 		$sql .= " and  if(end_date!=0,end_date,NOW()+1) > NOW() ";
		//    }
		// echo $sql;
		return $sql;
	}
	
	public function getTotal($dataModel ){
		
		$sql = "SELECT ma_goodstype.*
		FROM ". $this->tbl_name . " 
		WHERE ma_goodstype.IsActive = 1"; 

		// $sql = "SELECT * FROM ". $this->tbl_name  ." WHERE IsActive = 1  ";
				
		$sql =  $this->getSearchQuery($sql, $dataModel);
		
		$query = $this->db->query($sql);		 
		
		return  $query->num_rows() ;
	}
	
	public function getGoodsTypeNameList($dataModel, $limit = 10, $offset = 0, $order = '', $direction = 'asc'){
		
		
		$sql = "SELECT ma_goodstype.*
		FROM ". $this->tbl_name . " 
		WHERE ma_goodstype.IsActive = 1"; 

		if($order != ""){
			$sql .= " ORDER BY ".$order." ".$direction;
		}else{
			$sql .= " ORDER BY ".$this->id." ".$direction;
		}
		
		$sql .= " LIMIT $offset, $limit";
		
		$sql =  $this->getSearchQuery($sql, $dataModel);	
		
		 
		$query = $this->db->query($sql);
		return  $query->result_array();
	}
	
	public function deleteGoodsTypename($id){
		// echo $id; die();
		$result = false;
		try{
			$query = $this->getGoodsTypeNameById($id);
			// echo $id;die();
			// $modelData;			
			foreach ($query->result() as $row)
			{
			   		
				$modelData = array( 
					//'update_date' => date("Y-m-d H:i:s"),
					//'update_user' => $this->session->userdata('user_name'),
					// 'Edit'=>$this->session->userdata('user'),
					'IsActive' => 0 //$row->GoodsType_IsActive 
				); 
			}
			// echo $this->tbl_name;
			// print_r($modelData);die();
			$this->db->set('IsActive',0);
			$this->db->where($this->id, $id);
        	return $this->db->update($this->tbl_name);
			//return $this->update($id, $modelData);
			//$sql = "Delete FROM ". $this->tbl_name; 
			//return  $this->db->query($sql);
			
		}catch(Exception $ex){
			return $result;
		}
	}
	public function getTypeGoodsTypeComboList(){
		
		$sql = "SELECT * FROM  ma_goodstype
		WHERE IsActive = 1  "; 
		
		$query = $this->db->query($sql);
		return  $query->result_array();
	}
	public function GetGoodsTypeByMember($MEMBER_ID){
		$sql = "SELECT ma_goodsType.* FROM ma_goodsType
		WHERE ma_goodsType.MEMBER_ID = '".$MEMBER_ID."'
		";
		$query = $this->db->query($sql);
		return $query->result_array();
	}
	
}
?>