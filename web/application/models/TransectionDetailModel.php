<?php
  
class TransectionDetailModel extends CI_Model {
	
    private $tbl_name = 'ts_transection_detail';
	private $id = 'ID';
 
    public function __construct() {
        parent::__construct();
    }
	
	public function getTransectionDetailNameById($id){
		$this->db->where($this->id, $id );
		return $this->db->get($this->tbl_name);
	}
	
	public function insert($modelData){
		 
	 	$this->db->insert($this->tbl_name, $modelData); 
		return $this->db->insert_id(); 
    }
     
    public function update($id, $modelData){
        $this->db->where($this->id, $id);
        return $this->db->update($this->tbl_name, $modelData);
    }
	public function listUpdate($id, $listData){

		$nResult  = 0;

		//print_r($listData);
		foreach ($listData as $key => $data)
		{
		
			$data['TRANSECTION_ID'] = $id;
			if ($data['ID'] == 0) {  
				
				 $nResult = $this->insert($data);
				$this->db->set('STOCK', 'STOCK-1', FALSE);
				$this->db->where('ID', $data['GOODS_ID']);
				$this->db->where('STOCK_IsActive', 1);
				$this->db->update('ma_goods');
				//  print_r($data);die(); 
		    }
		    else {  
		    	$nResult = $this->update($data['ID'], $data);
		    }		
		}
        return $nResult;
	}
	public function listUpdateTempdetail($id, $listData){

		$nResult  = 0;

		//print_r($listData);
		foreach ($listData as $key => $data)
		{
		
			$data['TRANSECTION_ID'] = $id;
			if ($data['ID'] == 0) {  
    		 	$this->db->insert("temp_order_detail", $data); 
				$nResult =  $this->db->insert_id(); 
		    }  	
		}
        return $nResult;
	}
	public function listUpdateTempdiscount($id, $listData){

		$nResult  = 0;

		//print_r($listData);
		foreach ($listData as $key => $data)
		{
		
			$data['TRANSECTION_ID'] = $id;
			if ($data['ID'] == 0) {  
    		 	$this->db->insert("temp_order_discount", $data); 
				$nResult =  $this->db->insert_id(); 
		    }  	
		}
        return $nResult;
    }
	public function getDetail($TRANSECTION_ID){

		$sql = "SELECT ts_transection_detail.*,ma_goods.NAME as GOODS_NAME FROM  ts_transection_detail
		LEFT JOIN ma_goods ON ts_transection_detail.GOODS_ID = ma_goods.ID
		WHERE  TRANSECTION_ID = '".$TRANSECTION_ID."'"; 
		
		$query = $this->db->query($sql);
		return  $query->result_array();
	}
	public function deleteTransectionAllList($id){
		// echo $id; die();
		$result = false;
		try{
			$sql = "SELECT * FROM ts_transection_detail
			WHERE TRANSECTION_ID = '".$id."'
			";
			$res = $this->db->query($sql)->result_array();
			foreach($res as $row){
				$data['TRANSECTION_ID'] = $id;
				$data['AMOUNT'] = $row['PRICE'];
				$this->db->insert('report_refund',$data);

				$this->db->set('STOCK', 'STOCK+1', FALSE);
				$this->db->where('ID', $row['GOODS_ID']);
				$this->db->update('ma_goods');
			}
			$this->db->set('IsActive',0);
			$this->db->where('TRANSECTION_ID', $id);
        	return $this->db->update($this->tbl_name);
			
		}catch(Exception $ex){
		}
	}
	public function deleteTransectionDetail($id){
		// echo $id; die();
		$result = false;
		try{
			$sql  ="SELECT * FROM ts_transection_detail
			WHERE ID = '".$id."'
			";
			$res = $this->db->query($sql)->row_array();
			// print_r($res);die();

			// $sqlupdate = "UPDATE ts_transection set TotalOrder = TotalOrder-".$res['PRICE']." 
			// WHERE ts_transection.ID = '".$res['TRANSECTION_ID']."'";

			// $resupdate = $this->db->query($sqlupdate);

			$sqldetail = "SELECT * FROM ts_transection_detail WHERE ID = '".$id."'";
			$resdetail = $this->db->query($sqldetail)->row_array();

			$sql = "SELECT * FROM ts_transection WHERE ID = '".$resdetail['TRANSECTION_ID']."'";
			$res = $this->db->query($sql)->row_array();

			$refund['TRANSECTION_ID'] = $res['ID'];
			$refund['AMOUNT'] = $resdetail['PRICE'];
			$this->db->insert('report_refund',$refund);

			$sqlMaxId = "SELECT MAX(ID)as Max_id FROM ts_transection";
			$maxId = $this->db->query($sqlMaxId)->row_array();
			// echo $maxId['Max_id'];die();
			$today = date("Ymd"); 
			$order_id = $id.$today;
			$res['REFERENCE_ORDER_NAME'] = $res['ORDER_NAME'];
			$res['IsActive'] = 0;
			$res['ORDER_NAME'] = $order_id;
			$res['ID'] = $maxId['Max_id'] + 1;
			$res['TotalOrder'] = $resdetail['PRICE'];
			$res['TotalCost'] = $resdetail['COST'];
			$res['STATUS'] = 0;
			$this->db->insert("ts_transection", $res); 

			// $resdetail['TRANSECTION_ID'] = $res['ID'];
			// // print_r($resdetail);die();
			$detail['TRANSECTION_ID'] = $res['ID'];
			$detail['GOODS_ID'] = $resdetail['GOODS_ID'];
			$detail['PRICE'] = $resdetail['PRICE'];
			$detail['COST'] = $resdetail['COST'];
			$detail['STATUS'] = 0;
			$detail['IsActive'] = 0;

			$this->db->insert("ts_transection_detail", $detail); 	

			$this->db->set('STOCK', 'STOCK+1', FALSE);
			$this->db->where('ID', $resdetail['GOODS_ID']);
			$this->db->where('STOCK_IsActive', 1);
			$this->db->update('ma_goods');	

			$this->db->set('STATUS',0);
			$this->db->set('IsActive',1);
			$this->db->where($this->id, $id);
        	return $this->db->update($this->tbl_name);
			
			
		}catch(Exception $ex){
			return $result;
		}
	}
	#### controller ###getListDetail
	public function getListDetail($dataPost){
		try{ 
			$TRANSECTION_ID = isset($dataPost['TRANSECTION_ID']) ? $dataPost['TRANSECTION_ID'] : "";
			$result['status'] = true;
			$result['message'] = $this->getDetail($TRANSECTION_ID);
		}catch(Exception $ex){
			$result['status'] = false;
			$result['message'] = "exception: ".$ex;
		}
		
		return $result;	
	}
	public function delete($dataPost )
	{
		try {
			$id =  isset($dataPost['ID']) ? $dataPost['ID'] : 0; // $this->input->post('ap_id');

			$this->load->model('TransectionDetailModel', '', TRUE);
			$bResult = $this->deleteTransectionDetail($id);
			if ($bResult) {
				$result['status'] = true;
				$result['message'] = $this->lang->line("savesuccess");
			} else {
				$result['status'] = false;
				$result['message'] = $this->lang->line("error_faliure");
			}
		} catch (Exception $ex) {
			$result['status'] = false;
			$result['message'] = "exception: " . $ex;
		}

		return  $result;
	}
}
?>