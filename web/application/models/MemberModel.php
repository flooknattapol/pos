<?php
  
class MemberModel extends CI_Model {
	
    private $tbl_name = 'ma_member';
	private $id = 'ID';
 
    public function __construct() {
        parent::__construct();
    }
	
	public function getMemberNameById($id){
		$this->db->where($this->id, $id);
		return $this->db->get($this->tbl_name);
	}
	
	public function insert($modelData){
		 
	 	$this->db->insert($this->tbl_name, $modelData); 
		return $this->db->insert_id(); 
    }
     
    public function update($id, $modelData){
        $this->db->where($this->id, $id);
        return $this->db->update($this->tbl_name, $modelData);
    }
	
	public function getMemberNameAllList(){
        //return $this->db->count_all($this->tbl_name);
        
        $this->db->select('ID','userUID');
		$this->db->where('IsActive', 1);
        $query =  $this->db->get($this->tbl_name);
		
		return $query->result_array();
	}
	
	public function addQrCode($nResult){

		$this->load->library('MyQrCode');
		$nResult['QRURL'] = $this->myqrcode->SaveQRCode("Type1_".$nResult['ID'] );
		return $nResult;
	}

	public function loadQrCode($arrayData){

		$arrayResult = array();

		foreach($arrayData as $key => $value){
			$arrayResult[] = $this->addQrCode($value);
		}

		// print_r($arrayResult);die();
		return $arrayResult;
	}
	
	public function getMemberModel($id){
        //return $this->db->count_all($this->tbl_name);
        
        //$this->db->select('id','name','contact','address1','address2','address3','tel','email','taxid','website');
		$this->db->where('IsActive', 1);
		$this->db->where($this->id, $id);
        $query =  $this->db->get($this->tbl_name);
		
		return $query->result_array();
	}
	
	public function getMemberModelEx($id){
		$data = $this->getMemberModel($id);		
		return $this->loadQrCode($data);
    }
	
	public function getSearchQuery($sql, $dataModel){
		
		//print_r($dataModel);
		// 	if(isset($dataModel['code']) && $dataModel['code'] != ""){
		// 		$sql .= " and emp_code like '%".$this->db->escape_str( $dataModel['code'])."%' ";
		//    }
		
		// 	if(isset($dataModel['code_id']) && $dataModel['code_id'] != ""){
		// 	 	$sql .= " and id_card_number like '%".$this->db->escape_str( $dataModel['code_id'])."%' ";
		// 	}
		
		if(isset($dataModel['NAME']) && $dataModel['NAME'] != ""){
		 	$sql .= " WHERE NAME like '%".$this->db->escape_str( $dataModel['NAME'])."%' ";
		}
		
		// 	if(isset($dataModel['lastname']) && $dataModel['lastname'] != ""){
		// 	 	$sql .= " and lastname_th like '%".$this->db->escape_str( $dataModel['lastname'])."%' ";
		// 	}
			
		// 	if($dataModel['flag']==1){
		// 		$sql .= " and  if(end_date!=0,end_date,NOW()+1) > NOW() ";
		//    }
		// echo $sql;
		return $sql;
	}
	
	public function getTotal($dataModel ){
		
		$sql = "SELECT * FROM ". $this->tbl_name  ."";
				
		$sql =  $this->getSearchQuery($sql, $dataModel);
		
		$query = $this->db->query($sql);		 
		
		return  $query->num_rows() ;
	}
	public function Login($email,$pass){
		$sql = "SELECT * FROM ". $this->tbl_name . "
		WHERE EMAIL = '".$email."' AND PASSWORD = '".$pass."'
		";
		return $res = $this->db->query($sql)->row_array();
	}
	public function GetShop($EVENT_ID){
		$sql = "SELECT ma_event_member.*,ma_member.* FROM ma_event_member
		LEFT JOIN ma_member ON ma_event_member.MEMBER_ID = ma_member.ID
		WHERE ma_event_member.EVENT_ID = '".$EVENT_ID."' AND ma_event_member.VENDOR = 1
		";
		$query = $this->db->query($sql);
		return $query->result_array();
	}
	public function getMemberNameList($dataModel, $limit = 10, $offset = 0, $order = '', $direction = 'asc'){
		
		$sql = "SELECT * FROM ". $this->tbl_name . ""; 
		
		$sql =  $this->getSearchQuery($sql, $dataModel);	
		
		 
		// if($order != ""){
			// $this->db->order_by($order, $direction);
		// }else{
			// $this->db->order_by($this->id ,$direction); 
		// }
		
		if($order != ""){
			$sql .= " ORDER BY ".$order." ".$direction;
		}else{
			$sql .= " ORDER BY ".$this->id." ".$direction;
		}
		
		$sql .= " LIMIT $offset, $limit";
		
		//print($sql );
		 
		$query = $this->db->query($sql);
		return  $query->result_array();
	}

	public function getMemberNameListEx($dataModel, $limit = 10, $offset = 0, $order = '', $direction = 'asc'){
		
		$data = $this->getMemberNameList($dataModel, $limit, $offset, $order, $direction);
		return $this->loadQrCode($data);
	}

	public function getMemberComboList(){
		
		$sql = "SELECT * FROM ". $this->tbl_name . " WHERE IsActive = '1'"; 
		$query = $this->db->query($sql);
		return  $query->result_array();
	}
	public function deleteMembername($id){
		// echo $id; die();
		$result = false;
		try{
			$query = $this->getMemberNameById($id);
			// $modelData;			
			foreach ($query->result() as $row)
			{
			   		
				$modelData = array( 
					//'update_date' => date("Y-m-d H:i:s"),
					//'update_user' => $this->session->userdata('user_name'),
					'Edit'=>$this->session->userdata('user'),
					'IsActive' => 0 //$row->Member_IsActive 
				); 
			}
			
			$this->db->where($this->id, $id);
        	return $this->db->update($this->tbl_name, $modelData);
			
		}catch(Exception $ex){
			return $result;
		}
	}
		
	public function getMemberVoucherDetailById($id){
		
		$sql = "SELECT ts_voucher_tag.*,ma_event.NAME As event_NAME FROM ts_voucher_tag 
		LEFT JOIN ma_event ON ts_voucher_tag.EVENT_ID = ma_event.ID
		WHERE ts_voucher_tag.MEMBER_ID = '".$id."'
		";
		$query = $this->db->query($sql);
        // $this->db->where('ID', $id);
        // $query =  $this->db->get('ts_voucher_tag');
		
		return $query->result_array();
	}
	public function getMemberFriendName($id){
		
		$sql = "SELECT * FROM ". $this->tbl_name  ." 
		WHERE ID = '".$id."'
		";
		$query = $this->db->query($sql)->row_array();
		return $query['NAME'];
    }
}
?>