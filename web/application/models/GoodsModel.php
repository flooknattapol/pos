<?php
  
class GoodsModel extends CI_Model {
	
    private $tbl_name = 'ma_goods';
	private $id = 'ID';
 
    public function __construct() {
        parent::__construct();
    }
	
	public function getGoodsNameById($id){
		$this->db->where($this->id, $id);
		return $this->db->get($this->tbl_name);
	}
	
	public function insert($modelData){
		 
	 	$this->db->insert($this->tbl_name, $modelData); 
		return $this->db->insert_id(); 
    }
     
    public function update($id, $modelData){
        $this->db->where($this->id, $id);
        return $this->db->update($this->tbl_name, $modelData);
    }
	
	public function getGoodsNameAllList(){
        //return $this->db->count_all($this->tbl_name);
        
        $this->db->select('ID','NAME','SKU','PRICE','UNIT');
		$this->db->where('IsActive', 1);
        $query =  $this->db->get($this->tbl_name);
		
		return $query->result_array();
    }
	
	public function getGoodsModel($id){
        //return $this->db->count_all($this->tbl_name);
        
        //$this->db->select('id','name','contact','address1','address2','address3','tel','email','taxid','website');
		$this->db->where('IsActive', 1);
		$this->db->where($this->id, $id);
        $query =  $this->db->get($this->tbl_name);
		
		return $query->result_array();
    }
	
	public function getSearchQuery($sql, $dataModel){
		
		
		if(isset($dataModel['NAME']) && $dataModel['NAME'] != ""){
			$sql .= " and ma_goods.NAME like '%".$this->db->escape_str( $dataModel['NAME'])."%' ";
	   	}

		return $sql;
	}
	
	public function getTotal($dataModel,$idSession ){
		$this->load->model('UserModel', '', TRUE);

		if($idSession != ""){
			$idsession = $idSession; 
		}else{
			$idsession = $this->session->userdata('id');
		}
		$userPosition = $this->UserModel->getPostion($idsession);
		$sql = "SELECT ma_goods.*,ma_goods_category.NAME AS CATEGORYNAME FROM ". $this->tbl_name . " 
		LEFT JOIN ma_goods_category ON ma_goods.CATEGORY_ID = ma_goods_category.ID
		WHERE ma_goods.IsActive = 1  AND ma_goods.BUSINESS_ID = '".$userPosition['BUSINESS_ID']."'"; 

				
		$sql =  $this->getSearchQuery($sql, $dataModel);
		
		$query = $this->db->query($sql);		 
		
		return  $query->num_rows() ;
	}
	
	public function getGoodsNameList($dataModel, $limit = 10, $offset = 0, $order = '', $direction = 'asc',$idSession){
		$this->load->model('UserModel', '', TRUE);
		// echo "idses".$idSession;
		if($idSession != ""){
			$idsession = $idSession; 
		}else{
			$idsession = $this->session->userdata('id');
		}
		$userPosition = $this->UserModel->getPostion($idsession);
		
		$sql = "SELECT ma_goods.*,ma_goods_category.NAME AS CATEGORYNAME FROM ". $this->tbl_name . " 
		LEFT JOIN ma_goods_category ON ma_goods.CATEGORY_ID = ma_goods_category.ID
		WHERE ma_goods.IsActive = 1  AND ma_goods.BUSINESS_ID = '".$userPosition['BUSINESS_ID']."'"; 
		$sql =  $this->getSearchQuery($sql, $dataModel);	

		if($order != ""){
			$sql .= " ORDER BY ma_goods.".$order." ".$direction;
		}else{
			$sql .= " ORDER BY ma_goods.".$this->id." ".$direction;
		}
		
		$sql .= " LIMIT $offset, $limit";
		
		// echo $sql;die();
		
		 
		$query = $this->db->query($sql);
		return  $query->result_array();
	}
	public function deleteGoodsname($id){
		// echo $id; die();
		$result = false;
		try{
			$query = $this->getGoodsNameById($id);
			// echo $id;die();
			// $modelData;			
			foreach ($query->result() as $row)
			{
			   		
				$modelData = array( 
					//'update_date' => date("Y-m-d H:i:s"),
					//'update_user' => $this->session->userdata('user_name'),
					// 'Edit'=>$this->session->userdata('user'),
					'IsActive' => 0 //$row->Goods_IsActive 
				); 
			}
			// echo $this->tbl_name;
			// print_r($modelData);die();
			$this->db->set('IsActive',0);
			$this->db->where($this->id, $id);
        	return $this->db->update($this->tbl_name);
			//return $this->update($id, $modelData);
			//$sql = "Delete FROM ". $this->tbl_name; 
			//return  $this->db->query($sql);
			
		}catch(Exception $ex){
			return $result;
		}
	}
	public function loadBarcode($arrayData){

		$arrayResult = array();
		foreach($arrayData as $key => $value){
			$arrayData[$key]['Barcode'] = $this->set_barcode(time().md5($value['ID']));
		}
		return $arrayData;
	}
	public function getListAll($dataPost){
		
		// print_r($dataPost);
		$business_id =  isset($dataPost['business_id']) ? $dataPost['business_id'] : "";

		$this->load->model('UserModel', '', TRUE);
		if($business_id != ""){
			$business_id  = $business_id;
		}else{
			$idsession = $this->session->userdata('id');
			$userPosition = $this->UserModel->getPostion($idsession);
			$business_id  = $userPosition['BUSINESS_ID'];
		}

		$sql = "SELECT * FROM  ma_goods
		WHERE IsActive = 1  AND BUSINESS_ID = '".$business_id."'"; 
		
		// echo $sql;die();
		$query = $this->db->query($sql);
		return  $query->result_array();
	}
	public function getGoodsComboList(){
		
		$sql = "SELECT * FROM  ma_goods
		WHERE IsActive = 1  "; 
		
		$query = $this->db->query($sql);
		return  $query->result_array();
	}
	#### controller ###
	public function add($dataPost )
	{	
		$nResult = 0;
		try {
			$idSession = isset($dataPost['idSession']) ? $dataPost['idSession'] : "";
			// echo $idSession;die();
			$this->load->model('UserModel', '', TRUE);
			if($idSession != ""){
				$idsession = $idSession; 
			}else{
				$idsession = $this->session->userdata('id');
			}
			$userPosition = $this->UserModel->getPostion($idsession);

			$data['ID'] =  isset($dataPost['ID']) ? $dataPost['ID'] : 0;
			$data['NAME'] =  isset($dataPost['NAME']) ? $dataPost['NAME'] : "";
			$data['DETAIL'] =  isset($dataPost['DETAIL']) ? $dataPost['DETAIL'] : "";
			$data['PRICE'] =  isset($dataPost['PRICE']) ? $dataPost['PRICE'] : "";
			$data['COST'] =  isset($dataPost['COST']) ? $dataPost['COST'] : "";
			$data['IMAGE'] =  isset($dataPost['IMAGE']) ? $dataPost['IMAGE'] : "";
			$data['SKU'] =  isset($dataPost['SKU']) ? $dataPost['SKU'] : "";
			$data['BARCODE'] =  isset($dataPost['BARCODE']) ? $dataPost['BARCODE'] : "";
			$data['CATEGORY_ID'] =  isset($dataPost['CATEGORY_ID']) ? $dataPost['CATEGORY_ID'] : 0;
			$data['STOCK_IsActive'] =  isset($dataPost['STOCK_IsActive']) ? $dataPost['STOCK_IsActive'] : 0;
			$data['STATUS'] =  isset($dataPost['STATUS']) ? $dataPost['STATUS'] : 1;
			$data['BUSINESS_ID'] = $userPosition['BUSINESS_ID'];
			$data['IsActive'] = 1;

			if ($data['ID'] == 0) {
				$str = time();
				$nResult = $this->insert($data);
				if ($nResult > 0) {
					$data['UID'] = "88".$str.$nResult;
					$data['BARCODE'] = $this->set_barcode("88".$str.$nResult);
					$data['ID'] = $nResult;
					$nResult = $this->update($nResult, $data);
				}
				
			} else {
				$str = time();
				$data['UID'] = "88".$str.$data['ID'];
				$data['BARCODE'] = $this->set_barcode("88".$str.$data['ID']);
				$nResult = $this->update($data['ID'], $data);
			}
			if ($nResult > 0) {
				$result['status'] = true;
				$result['message'] = $this->lang->line("savesuccess");
			
			} else {
				$result['status'] = false;
				$result['message'] = $this->lang->line("error");
			}
			
		} catch (Exception $ex) {
			$result['status'] = false;
			$result['message'] = "exception: " . $ex;
		}

		return  $result;
	}

	public function delete($dataPost )
	{
		try {
			$id =  isset($dataPost['ID']) ? $dataPost['ID'] : 0; // $this->input->post('ap_id');

			$bResult = $this->deleteGoodsname($id);

			if ($bResult) {
				$result['status'] = true;
				$result['message'] = $this->lang->line("savesuccess");
			} else {
				$result['status'] = false;
				$result['message'] = $this->lang->line("error_faliure");
			}
		} catch (Exception $ex) {
			$result['status'] = false;
			$result['message'] = "exception: " . $ex;
		}

		return  $result;
	}
	public function getComboList(){
		try{ 
			$result['status'] = true;
			$result['message'] = $this->getGoodsComboList();
		}catch(Exception $ex){
			$result['status'] = false;
			$result['message'] = "exception: ".$ex;
		}
		
		return $result;	
	}
	public function getList($dataPost)
	{

		try {
			$PageIndex =  isset($dataPost['PageIndex']) ? $dataPost['PageIndex'] : 1;
			$PageSize =  isset($dataPost['PageSize']) ? $dataPost['PageSize'] : 20;
			$direction =  isset($dataPost['SortColumn']) ? $dataPost['SortColumn'] : "";
			$SortOrder = isset($dataPost['SortOrder']) ? $dataPost['SortOrder'] : "asc";
			$idSession = isset($dataPost['idSession']) ? $dataPost['idSession'] : "";
			$dataModel = isset($dataPost['mSearch']) ? $dataPost['mSearch'] : "";

			$offset = ($PageIndex - 1) * $PageSize;
			$result['status'] = true;
			$result['message'] = $this->getGoodsNameList($dataModel, $PageSize, $offset, $direction, $SortOrder,$idSession);
			$result['totalRecords'] = $this->getTotal($dataModel,$idSession);
			$result['toTalPage'] = ceil($result['totalRecords'] / $PageSize);

			// foreach($result['message'] as $key => $value){
			// 	$str = time();
			// 	$str1 = substr($str,0,3);
			// 	$str2 = substr($str,4,7);
			// 	$str3 = substr($str,8,10);
			// 	$result['message'][$key]['BARCODE'] = $this->set_barcode($str.$value['ID']);
			// }

		} catch (Exception $ex) {
			$result['status'] = false;
			$result['message'] = "exception: " . $ex;
		}

		return  $result;
	}
	public function getListByAdmin($dataPost){
		try{ 
			$result['status'] = true;
			$result['message'] = $this->getListAll($dataPost);
		}catch(Exception $ex){
			$result['status'] = false;
			$result['message'] = "exception: ".$ex;
		}
		
		return $result;	
	}
	function set_barcode($code){
		$this->load->library('zend');
		$this->zend->load('Zend/Barcode');
		$file = Zend_Barcode::draw('code128', 'image', array('text' => $code,'factor'=>3), array());
		$code = time().$code;
		$store_image = imagepng($file,FCPATH ."upload/barcode/{$code}.png");
		// echo $file,FCPATH ."upload\barcode/{$code}.png";die();
		return $code.'.png';
	}
}
?>