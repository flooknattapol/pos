<?php
  
class EventMemberModel extends CI_Model {
	
    private $tbl_name = 'ma_event_member';
	private $id = 'ID';
 
    public function __construct() {
        parent::__construct();
    }
	
	public function getEventMemberNameById($id){
		$this->db->where($this->id, $id);
		return $this->db->get($this->tbl_name);
	}
	
	public function insert($modelData){
		 
	 	$this->db->insert($this->tbl_name, $modelData); 
		return $this->db->insert_id(); 
    }
     
    public function update($id, $modelData){
		// print_r($modelData);die();
        $this->db->where($this->id, $id);
        return $this->db->update($this->tbl_name, $modelData);
    }
	
	public function getEventMemberNameAllList(){
        //return $this->db->count_all($this->tbl_name);
        
        $this->db->select('ID','EVENT_ID','MEMBER_ID','VENDOR','BUYYER');
		$this->db->where('IsActive', 1);
        $query =  $this->db->get($this->tbl_name);
		
		return $query->result_array();
    }
	
	public function getEventMemberModel($id){
        //return $this->db->count_all($this->tbl_name);
        
        //$this->db->select('id','name','contact','address1','address2','address3','tel','email','taxid','website');
		$this->db->where('IsActive', 1);
		$this->db->where($this->id, $id);
        $query =  $this->db->get($this->tbl_name);
		
		return $query->result_array();
    }
	
	public function getSearchQuery($sql, $dataModel){
		
		//print_r($dataModel);
		// 	if(isset($dataModel['code']) && $dataModel['code'] != ""){
		// 		$sql .= " and emp_code like '%".$this->db->escape_str( $dataModel['code'])."%' ";
		//    }
		
		// 	if(isset($dataModel['code_id']) && $dataModel['code_id'] != ""){
		// 	 	$sql .= " and id_card_number like '%".$this->db->escape_str( $dataModel['code_id'])."%' ";
		// 	}
		
		if(isset($dataModel['NAME']) && $dataModel['NAME'] != ""){
		 	$sql .= " and ma_event.NAME like '%".$this->db->escape_str( $dataModel['NAME'])."%' ";
		}
		
		if(isset($dataModel['MEMBER_ID']) && $dataModel['MEMBER_ID'] != ""){
			$sql .= " and ma_event_member.MEMBER_ID = '".$dataModel['MEMBER_ID']."' ";
		}
			
		// 	if($dataModel['flag']==1){
		// 		$sql .= " and  if(end_date!=0,end_date,NOW()+1) > NOW() ";
		//    }
		// echo $sql;
		return $sql;
	}
	
	public function getTotal($dataModel ){
		
		$sql = "SELECT ma_event_member.*,ma_event.NAME,ma_member.NAME  as Member_Name,ma_event.IMAGE as IMG_EVENT ,
		ma_event.DESCRIPTION ,
		ma_event.DATE_START,
		ma_event.DATE_END,
		ma_event.LOCATION,
		ma_event.WEBSITE,
		ts_voucher_tag.VOUCHER
		FROM ma_event_member
	   LEFT JOIN ma_member ON ma_event_member.MEMBER_ID = ma_member.ID
	   LEFT JOIN ma_event ON ma_event_member.EVENT_ID = ma_event.ID
	   LEFT JOIN ts_voucher_tag ON ma_event_member.MEMBER_ID = ts_voucher_tag.MEMBER_ID AND ma_event_member.EVENT_ID = ts_voucher_tag.EVENT_ID
	   WHERE ma_event_member.IsActive = 1"; 

		// $sql = "SELECT * FROM ". $this->tbl_name  ." WHERE IsActive = 1  ";
				
		$sql =  $this->getSearchQuery($sql, $dataModel);
		
		$query = $this->db->query($sql);		 
		
		return  $query->num_rows() ;
	}
	
	public function getEventMemberNameList($dataModel, $limit = 10, $offset = 0, $order = '', $direction = 'asc'){
		
		$sql = "SELECT ma_event_member.*,ma_event.NAME,ma_member.NAME  as Member_Name,ma_event.IMAGE as IMG_EVENT ,
		ma_event.DESCRIPTION ,
		ma_event.DATE_START,
		ma_event.DATE_END,
		ma_event.LOCATION,
		ma_event.WEBSITE,
		ts_voucher_tag.VOUCHER
		FROM ma_event_member
	   LEFT JOIN ma_member ON ma_event_member.MEMBER_ID = ma_member.ID
	   LEFT JOIN ma_event ON ma_event_member.EVENT_ID = ma_event.ID
	   LEFT JOIN ts_voucher_tag ON ma_event_member.MEMBER_ID = ts_voucher_tag.MEMBER_ID AND ma_event_member.EVENT_ID = ts_voucher_tag.EVENT_ID
	   WHERE ma_event_member.IsActive = 1"; 
		
		$sql =  $this->getSearchQuery($sql, $dataModel);	
		
		 
		// if($order != ""){
			// $this->db->order_by($order, $direction);
		// }else{
			// $this->db->order_by($this->id ,$direction); 
		// }
		
		if($order != ""){
			$sql .= " ORDER BY ".$order." ".$direction;
		}else{
			$sql .= " ORDER BY ".$this->id." ".$direction;
		}
		
		
		$sql .= " LIMIT $offset, $limit";
		
		//print($sql );
		 
		$query = $this->db->query($sql);
		return  $query->result_array();
	}
	public function getEventMemberNameListById($dataModel, $limit = 10, $offset = 0, $order = '', $direction = 'asc'){
		
		// print_r($dataModel);die();
		$sql = "SELECT DISTINCT ma_event.*,ts_voucher_tag.TOTAL,ts_voucher_tag.VOUCHER FROM ma_event
		LEFT JOIN ts_voucher_tag ON ma_event.ID = ts_voucher_tag.EVENT_ID AND ts_voucher_tag.MEMBER_ID = '".$dataModel['MEMBER_ID']."'
		WHERE ma_event.IsActive >= 0
		
		";

		if(isset($dataModel['EVENT_NAME']) && $dataModel['EVENT_NAME'] != ""){
			$sql .= " and ma_event.NAME like '%".$this->db->escape_str( $dataModel['EVENT_NAME'])."%' ";
		}
		$sql .="ORDER BY ma_event.date_start desc";
		// echo $sql;die();
	// 	$sql = "SELECT ma_event_member.*,ma_event.NAME,ma_member.NAME  as Member_Name,ma_event.IMAGE as IMG_EVENT ,
	// 	ma_event.DESCRIPTION ,
	// 	ma_event.DATE_START,
	// 	ma_event.DATE_END,
	// 	ma_event.LOCATION,
	// 	ma_event.WEBSITE,
	// 	ts_voucher_tag.VOUCHER
	// 	FROM ma_event_member
	//    RIGHT JOIN ma_member ON ma_event_member.MEMBER_ID = ma_member.ID
	//    RIGHT JOIN ma_event ON ma_event_member.EVENT_ID = ma_event.ID
	//    LEFT JOIN ts_voucher_tag ON ma_event_member.MEMBER_ID = ts_voucher_tag.MEMBER_ID AND ma_event_member.EVENT_ID = ts_voucher_tag.EVENT_ID
	//    WHERE ma_event_member.IsActive = 1"; 
				// LEFT JOIN ts_voucher_tag ON
		// $sql =  $this->getSearchQuery($sql, $dataModel);	
		
		 
		// if($order != ""){
			// $this->db->order_by($order, $direction);
		// }else{
			// $this->db->order_by($this->id ,$direction); 
		// }
		
		// if($order != ""){
		// 	$sql .= " ORDER BY ".$order." ".$direction;
		// }else{
		// 	$sql .= " ORDER BY ".$this->id." ".$direction;
		// }
		// // echo $sql;die();
		// $sql .= " LIMIT $offset, $limit";
		
		//print($sql );
		 
		$query = $this->db->query($sql);
		return  $query->result_array();
	}
	public function deleteEventMembername($id){
		// echo $id; die();
		$result = false;
		try{
			$query = $this->getEventMemberNameById($id);
			// $modelData;			
			foreach ($query->result() as $row)
			{
			   		
				$modelData = array( 
					//'update_date' => date("Y-m-d H:i:s"),
					//'update_user' => $this->session->userdata('user_name'),
					'Edit'=>$this->session->userdata('user'),
					'IsActive' => 0 //$row->EventMember_IsActive 
				); 
			}
			
			$this->db->where($this->id, $id);
        	return $this->db->update($this->tbl_name, $modelData);
			//return $this->update($id, $modelData);
			//$sql = "Delete FROM ". $this->tbl_name; 
			//return  $this->db->query($sql);
			
		}catch(Exception $ex){
			return $result;
		}
    }
}
?>