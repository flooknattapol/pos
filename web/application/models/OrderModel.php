<?php
  use  setasign\Fpdi;
class OrderModel extends CI_Model {
	
    private $tbl_name = 'ma_order';
	private $id = 'ID';
 
    public function __construct() {
        parent::__construct();
    }
	
	public function getBusinessNameById($id){
		$this->db->where($this->id, $id);
		return $this->db->get($this->tbl_name);
	}
	
	
	public function insert($modelData){
		 
	 	$this->db->insert($this->tbl_name, $modelData); 
		return $this->db->insert_id(); 
    }
     
    public function update($id, $modelData){
        $this->db->where($this->id, $id);
        return $this->db->update($this->tbl_name, $modelData);
    }
	
	public function getBusinessNameAllList(){
        //return $this->db->count_all($this->tbl_name);
        
        $this->db->select('id','name','contact','address1','tel','email','taxid','website');
		//$this->db->where('User_delete_flag', 0);
        $query =  $this->db->get($this->tbl_name);
		
		return $query->result_array();
    }
	
	public function getOrderModel(){
        //return $this->db->count_all($this->tbl_name);
        
        //$this->db->select('id','name','contact','address1','address2','address3','tel','email','taxid','website');
		//$this->db->where('User_delete_flag', 0);
        $query =  $this->db->get($this->tbl_name);
		
		return $query->result_array();
    }
	
	public function getSearchQuery($sql, $dataModel){
		
		//print_r($dataModel);
		
		// if(isset($dataModel['book_no']) && $dataModel['book_no'] != ""){
		 	// $sql .= " and book_no like '%".$this->db->escape_str( $dataModel['book_no'])."%' ";
		// }
		
		// if(isset($dataModel['num_no']) && $dataModel['num_no'] != ""){
		 	// $sql .= " and num_no like '%".$this->db->escape_str( $dataModel['num_no'])."%' ";
		// }
		
		if(isset($dataModel['NAME']) && $dataModel['NAME'] != ""){
		 	$sql .= " and ma_order.ORDERNAME like '%".$this->db->escape_str( $dataModel['NAME'])."%' ";
		}
		
		return $sql;
	}
	
	public function getTotal($dataModel ){
		
		$sql = "SELECT ma_order.*,ma_vendor.NAME as VENDOR_NAME ,ma_vendor.ID as VENDOR_ID FROM ma_order
		LEFT JOIN ma_vendor ON ma_order.VENDOR_ID = ma_vendor.ID
		WHERE ma_order.IsActive >=0";
				
		$sql =  $this->getSearchQuery($sql, $dataModel);
		
		$query = $this->db->query($sql);		 
		
		return  $query->num_rows() ;
	}
	
	public function getOrderNameList($dataModel, $limit = 10, $offset = 0, $order = '', $direction = 'asc'){
		
		$sql = "SELECT ma_order.*,ma_vendor.NAME as VENDOR_NAME ,ma_vendor.ID as VENDOR_ID 
		,ma_user.NAME AS EMPLOYEE_NAME
		FROM ma_order
		LEFT JOIN ma_vendor ON ma_order.VENDOR_ID = ma_vendor.ID
		LEFT JOIN ma_user ON ma_order.EMPLOYEE_ID = ma_user.ID

		WHERE ma_order.IsActive >=0 ";

		$sql =  $this->getSearchQuery($sql, $dataModel);

		if($order != ""){
			$sql .= " ORDER BY ma_order.".$order." ".$direction;
		}else{
			$sql .= " ORDER BY ma_order.".$this->id." ".$direction;
		}
		$query = $this->db->query($sql);
		
		return  $query->result_array();
	}		
	
	
	
	public function deleteOrdername($id){
		$result = false;
		try{
			$query = $this->getBusinessNameById($id);
			//$modelData;			
			foreach ($query->result() as $row)
			{
			   		
				$modelData = array( 
					//'update_date' => date("Y-m-d H:i:s"),
					//'update_user' => $this->session->userdata('user_name'),
					'IsActive' => 0 //$row->Project_delete_flag 
				); 
			}
			
			$this->db->where($this->id, $id);
        	return $this->db->update($this->tbl_name, $modelData);
			
		}catch(Exception $ex){
			return $result;
		}
    }
	public function undeleteBusinessname($id){
		$result = false;
		try{
			$query = $this->getBusinessNameById($id);
			//$modelData;			
			foreach ($query->result() as $row)
			{
			   		
				$modelData = array( 
					//'update_date' => date("Y-m-d H:i:s"),
					//'update_user' => $this->session->userdata('user_name'),
					'IsActive' => 1 //$row->Project_delete_flag 
				); 
			}
			
			$this->db->where($this->id, $id);
        	return $this->db->update($this->tbl_name, $modelData);
			
		}catch(Exception $ex){
			return $result;
		}
    }
	public function getBusinessComboList(){
		
		$sql = "SELECT * FROM ". $this->tbl_name . " WHERE IsActive = 1  ";
		$query = $this->db->query($sql);
		return  $query->result_array();
	}
	public function getOrderDetail($id){
		
		$sql = "SELECT ma_order_detail.*,ma_order_detail.ITEMAMOUNT as ITEMAMOUNT_IMPROT FROM ma_order_detail 
		WHERE ma_order_detail.ORDER_ID = '".$id."'
		";
		$query = $this->db->query($sql);
		return $query->result_array();
	}
	#### controller #####
	public function getList($dataPost){
		try{

	  		$PageIndex =  isset($dataPost['PageIndex'])?$dataPost['PageIndex']: 1;
			$PageSize =  isset($dataPost['PageSize'])?$dataPost['PageSize']: 20;
			$direction =  isset($dataPost['SortColumn'])?$dataPost['SortColumn']: "";
			$SortOrder = isset($dataPost['SortOrder'])?$dataPost['SortOrder']: "asc";
			$dataModel = isset($dataPost['mSearch'])?$dataPost['mSearch']: "";

			$offset = ($PageIndex - 1) * $PageSize;
			 
			$result['status'] = true;
			$result['message'] = $this->getOrderNameList($dataModel , $PageSize, $offset, $direction, $SortOrder );
			$result['totalRecords'] = $this->getTotal($dataModel);
			$result['Session'] = $this->session->userdata('user');
			$result['toTalPage'] = ceil( $result['totalRecords'] / $PageSize);						 	
		}catch(Exception $ex){
			$result['status'] = false;
			$result['message'] = "exception: ".$ex;
		}
		return $result;	
	}
	public function delete($dataPost){
		try{

			$id =  isset($dataPost['ID'])?$dataPost['ID']:0;
			$bResult = $this->deleteOrdername($id);
			 
			if($bResult){
				$result['status'] = true;
				$result['message'] = $this->lang->line("savesuccess");
			}else{
				$result['status'] = false;
				$result['message'] = $this->lang->line("error_faliure");
			}
			
		}catch(Exception $ex){
			$result['status'] = false;
			$result['message'] = "exception: ".$ex;
		}
		return $result;	

	}
	public function undelete($dataPost){
		try{

			$id =  isset($dataPost['ID'])?$dataPost['ID']:0;
			$bResult = $this->undeleteBusinessname($id);
			 
			if($bResult){
				$result['status'] = true;
				$result['message'] = $this->lang->line("savesuccess");
			}else{
				$result['status'] = false;
				$result['message'] = $this->lang->line("error_faliure");
			}
			
		}catch(Exception $ex){
			$result['status'] = false;
			$result['message'] = "exception: ".$ex;
		}
		return $result;	

	}
	public function add($dataPost) {
		
	  	try{
			$nResult = 0;
			$this->load->model('OrderDetailModel','',TRUE); 

			$idSession = isset($dataPost['idSession']) ? $dataPost['idSession'] : "";
			$this->load->model('UserModel', '', TRUE);
			if($idSession != ""){
				$idsession = $idSession; 
			}else{
				$idsession = $this->session->userdata('id');
			}
			$userPosition = $this->UserModel->getPostion($idsession);

	  		$data['ID'] =  isset($dataPost['ID'])?$dataPost['ID']: 0; 
			$data['ORDERNAME'] =  isset($dataPost['ORDERNAME'])?$dataPost['ORDERNAME']: "";
			$data['BUSINESS_ID'] =  $userPosition['BUSINESS_ID'];
			$data['EMPLOYEE_ID'] =  $userPosition['USER_ID'];
			$data['DATE_ORDER'] =  isset($dataPost['DATE_ORDER'])?$dataPost['DATE_ORDER']: "";
			$data['ENDDATE_ORDER'] =  isset($dataPost['ENDDATE_ORDER'])?$dataPost['ENDDATE_ORDER']: "";
			$data['NOTE'] =  isset($dataPost['NOTE'])?$dataPost['NOTE']: "";
			$data['VENDOR_ID'] = isset($dataPost['VENDOR_ID'])?$dataPost['VENDOR_ID']:0;
			$data['IsActive'] =  1;

			$detail = isset($dataPost['detail'])?$dataPost['detail']: ""; 
			$ValueInList = isset($dataPost['DeleteValueInList'])?$dataPost['DeleteValueInList']: ""; 
			if ($data['ID'] == 0) {  
				$date = new DateTime();
				$data['ORDERNAME'] = $date->format('YmdHis');
				$nResult = $this->insert($data);
				$master_id = $nResult;
				if(is_array($detail) && $master_id > 0){
					$nDetail = $this->OrderDetailModel->listUpdate($master_id,  $detail ,$ValueInList);
				}

				if($nResult > 0){ 
					$result['status'] = true;
					$result['message'] = $this->lang->line("savesuccess");
				}else{
					$result['status'] = false;
					$result['message'] = $this->lang->line("error");
				}
				
				 	
			}else {  
				$nResult = $this->update($data['ID'], $data);
				if(is_array($detail) && $data['ID'] > 0){
					$nDetail = $this->OrderDetailModel->listUpdate($data['ID'],  $detail ,$ValueInList);
				}
				if($nResult > 0){ 
					$result['status'] = true;
					$result['message'] = $this->lang->line("savesuccess");
				}else{
					$result['status'] = false;
					$result['message'] = $this->lang->line("error");
				} 

			}
			
    	}catch(Exception $ex){
    		$result['status'] = false;
			$result['message'] = "exception: ".$ex;
    	}
	    
		return $result;
	}
	
	public function getComboList(){
		try{ 
			$result['status'] = true;
			$result['message'] = $this->getBusinessComboList();
		}catch(Exception $ex){
			$result['status'] = false;
			$result['message'] = "exception: ".$ex;
		}
		
		return $result;	
	}
	public function getDetail($dataPost)
	{
		try {
			$id =  isset($dataPost['ID']) ? $dataPost['ID'] : 0;
			$result['status'] = true;
			$result['message'] = $this->getOrderDetail($id);
		} catch (Exception $ex) {
			$result['status'] = false;
			$result['message'] = "exception: " . $ex;
		}

		return $result;	
	}
	public function printPDF(){
		
		define('FPDF_FONTPATH',APPPATH .'fpdf/font/');
		require(APPPATH .'fpdf/fpdf.php'); 
		require_once(APPPATH .'fpdi/autoload.php');
		 
		
		try {
			$this->load->model('OrderModel','',TRUE); 
			
			$id = isset($_GET['id'])?$_GET['id']: 0;
			   
			$query = $this->OrderModel->getuserNameById($id);			
			$userDatas = $query->result_array();
			$userData = $userDatas[0];
			
			//print_r($customerDetail);
			
			$filename = $_SERVER["DOCUMENT_ROOT"]. '/materia/user_Template.pdf';
			$pdf_name = $userData['num_no'].".pdf"; 
			$pdf = new Fpdi\FPDI('p','mm','A4');			
			$pdf -> AddPage(); 

			$pdf->setSourceFile($filename); 
			$tplIdx = $pdf->importPage(1);
			// use the imported page and place it at point 10,10 with a width of 100 mm
			$pdf->useTemplate($tplIdx, 1, 1, 210);
			// now write some text above the imported page
			
			$pdf->AddFont('AngsanaNew','','angsa.php');
			$pdf->AddFont('AngsanaNew','B','angsab.php');
			$pdf->AddFont('AngsanaNew','I','angsai.php');
			$pdf->SetFont('AngsanaNew','',12);
			
			//$pdf->SetFont('Arial');
			$pdf->SetTextColor(0,0,0);
			
			  
			$sign_date = new DateTime($userData['sign_date']);
			
			$tab1 = 18;
			$tab2 = 20;
			$tab3 = 30;
			$tab3Ex = 70;
			$tab4 = 120;
			$tab5 = 158;
			$tab6 = 182;
			$taxid1 = 133;
			$taxid2 = 140;
			$taxid3 = 144;
			$taxid4 = 148;
			$taxid5 = 152;
			$taxid6 = 160;
			$taxid7 = 164;
			$taxid8 = 168;
			$taxid9 = 172;
			$taxid10 = 176;
			$taxid11 = 183;
			$taxid12 = 187;
			$taxid13 = 194;
			$tabEnd = 188;
			$tabSocial = 130;
			$tabFund = 180;
			$lineStart = 20;
			$lineBr = 6;
			
			//...
			//บรรทัด 1
			$pdf->SetXY($tabEnd, $lineStart );
			$pdf->Write(0, iconv( 'UTF-8','cp874' ,  $userData['book_no']));
			$lineStart += $lineBr;
			
			//บรรทัด 2
			$pdf->SetXY($tabEnd, $lineStart );
			$pdf->Write(0, iconv( 'UTF-8','cp874' ,  $userData['num_no']));
			$lineStart += $lineBr;
			
			//บรรทัด 3
			$pdf->SetXY($taxid1, $lineStart );
			$pdf->Write(0, iconv( 'UTF-8','cp874' ,  $userData['pay_id'][0]));
			$pdf->SetXY($taxid2, $lineStart );
			$pdf->Write(0, iconv( 'UTF-8','cp874' ,  $userData['pay_id'][1]));
			$pdf->SetXY($taxid3, $lineStart );
			$pdf->Write(0, iconv( 'UTF-8','cp874' ,  $userData['pay_id'][2]));
			$pdf->SetXY($taxid4, $lineStart );
			$pdf->Write(0, iconv( 'UTF-8','cp874' ,  $userData['pay_id'][3]));
			$pdf->SetXY($taxid5, $lineStart );
			$pdf->Write(0, iconv( 'UTF-8','cp874' ,  $userData['pay_id'][4]));
			$pdf->SetXY($taxid6, $lineStart );
			$pdf->Write(0, iconv( 'UTF-8','cp874' ,  $userData['pay_id'][5]));
			$pdf->SetXY($taxid7, $lineStart );
			$pdf->Write(0, iconv( 'UTF-8','cp874' ,  $userData['pay_id'][6]));
			$pdf->SetXY($taxid8, $lineStart );
			$pdf->Write(0, iconv( 'UTF-8','cp874' ,  $userData['pay_id'][7]));
			$pdf->SetXY($taxid9, $lineStart );
			$pdf->Write(0, iconv( 'UTF-8','cp874' ,  $userData['pay_id'][8]));
			$pdf->SetXY($taxid10, $lineStart );
			$pdf->Write(0, iconv( 'UTF-8','cp874' ,  $userData['pay_id'][9]));
			$pdf->SetXY($taxid11, $lineStart );
			$pdf->Write(0, iconv( 'UTF-8','cp874' ,  $userData['pay_id'][10]));
			$pdf->SetXY($taxid12, $lineStart );
			$pdf->Write(0, iconv( 'UTF-8','cp874' ,  $userData['pay_id'][11]));
			$pdf->SetXY($taxid13, $lineStart );
			$pdf->Write(0, iconv( 'UTF-8','cp874' ,  $userData['pay_id'][12]));
			$lineStart += $lineBr;
			
			//บรรทัด 4
			$pdf->SetXY($tab3, $lineStart );
			$pdf->Write(0, iconv( 'UTF-8','cp874' ,  $userData['pay_name']));
			$lineStart += $lineBr + 3;
			
			//บรรทัด 5
			$pdf->SetXY($tab3, $lineStart );
			$pdf->Write(0, iconv( 'UTF-8','cp874' ,  $userData['pay_address']));
			$lineStart += $lineBr + 4;
			 
			//บรรทัด 6
			$pdf->SetXY($taxid1, $lineStart );
			$pdf->Write(0, iconv( 'UTF-8','cp874' ,  $userData['com_id'][0]));
			$pdf->SetXY($taxid2, $lineStart );
			$pdf->Write(0, iconv( 'UTF-8','cp874' ,  $userData['com_id'][1]));
			$pdf->SetXY($taxid3, $lineStart );
			$pdf->Write(0, iconv( 'UTF-8','cp874' ,  $userData['com_id'][2]));
			$pdf->SetXY($taxid4, $lineStart );
			$pdf->Write(0, iconv( 'UTF-8','cp874' ,  $userData['com_id'][3]));
			$pdf->SetXY($taxid5, $lineStart );
			$pdf->Write(0, iconv( 'UTF-8','cp874' ,  $userData['com_id'][4]));
			$pdf->SetXY($taxid6, $lineStart );
			$pdf->Write(0, iconv( 'UTF-8','cp874' ,  $userData['com_id'][5]));
			$pdf->SetXY($taxid7, $lineStart );
			$pdf->Write(0, iconv( 'UTF-8','cp874' ,  $userData['com_id'][6]));
			$pdf->SetXY($taxid8, $lineStart );
			$pdf->Write(0, iconv( 'UTF-8','cp874' ,  $userData['com_id'][7]));
			$pdf->SetXY($taxid9, $lineStart );
			$pdf->Write(0, iconv( 'UTF-8','cp874' ,  $userData['com_id'][8]));
			$pdf->SetXY($taxid10, $lineStart );
			$pdf->Write(0, iconv( 'UTF-8','cp874' ,  $userData['com_id'][9]));
			$pdf->SetXY($taxid11, $lineStart );
			$pdf->Write(0, iconv( 'UTF-8','cp874' ,  $userData['com_id'][10]));
			$pdf->SetXY($taxid12, $lineStart );
			$pdf->Write(0, iconv( 'UTF-8','cp874' ,  $userData['com_id'][11]));
			$pdf->SetXY($taxid13, $lineStart );
			$pdf->Write(0, iconv( 'UTF-8','cp874' ,  $userData['com_id'][12]));
			$lineStart += $lineBr + 1;
			
			//บรรทัด 7
			$pdf->SetXY($tab3, $lineStart );
			$pdf->Write(0, iconv( 'UTF-8','cp874' ,  $userData['com_name']));
			$lineStart += $lineBr + 3;
			
			//บรรทัด 8
			$pdf->SetXY($tab3, $lineStart );
			$pdf->Write(0, iconv( 'UTF-8','cp874' ,  $userData['com_address']));
			$lineStart += (5*$lineBr) + 4;
			
			//บรรทัด 9
			$pdf->SetXY($tab4, $lineStart );
			$pdf->Cell(10,0,$userData['salary_year'],0,0,'R');
			$pdf->SetXY($tab5, $lineStart );
			$pdf->Cell(10,0,$userData['salary_pay'],0,0,'R'); 
			$pdf->SetXY($tab6, $lineStart );
			$pdf->Cell(10,0,$userData['salary_wht'],0,0,'R'); 
			$lineStart += $lineBr ;
			
			//บรรทัด 10
			$pdf->SetXY($tab4, $lineStart );
			$pdf->Cell(10,0,$userData['fee_year'],0,0,'R');
			$pdf->SetXY($tab5, $lineStart );
			$pdf->Cell(10,0,$userData['fee_pay'],0,0,'R'); 
			$pdf->SetXY($tab6, $lineStart );
			$pdf->Cell(10,0,$userData['fee_wht'],0,0,'R'); 
			$lineStart += $lineBr -1;
			
			//บรรทัด 11
			$pdf->SetXY($tab4, $lineStart );
			$pdf->Cell(10,0,$userData['copyfee_year'],0,0,'R');
			$pdf->SetXY($tab5, $lineStart );
			$pdf->Cell(10,0,$userData['copyfee_pay'],0,0,'R'); 
			$pdf->SetXY($tab6, $lineStart );
			$pdf->Cell(10,0,$userData['copyfee_wht'],0,0,'R'); 
			$lineStart += $lineBr -1 ;
			
			//บรรทัด 12
			$pdf->SetXY($tab4, $lineStart );
			$pdf->Cell(10,0,$userData['interest_year'],0,0,'R');
			$pdf->SetXY($tab5, $lineStart );
			$pdf->Cell(10,0,$userData['interest_pay'],0,0,'R'); 
			$pdf->SetXY($tab6, $lineStart );
			$pdf->Cell(10,0,$userData['interest_wht'],0,0,'R'); 
			$lineStart += (3*$lineBr) ;
			
			//บรรทัด 13
			$pdf->SetXY($tab4, $lineStart );
			$pdf->Cell(10,0,$userData['interest1_year'],0,0,'R');
			$pdf->SetXY($tab5, $lineStart );
			$pdf->Cell(10,0,$userData['interest1_pay'],0,0,'R'); 
			$pdf->SetXY($tab6, $lineStart );
			$pdf->Cell(10,0,$userData['interest1_wht'],0,0,'R'); 
			$lineStart += $lineBr ;
			
			//บรรทัด 14
			$pdf->SetXY($tab4, $lineStart );
			$pdf->Cell(10,0,$userData['interest2_year'],0,0,'R');
			$pdf->SetXY($tab5, $lineStart );
			$pdf->Cell(10,0,$userData['interest2_pay'],0,0,'R'); 
			$pdf->SetXY($tab6, $lineStart );
			$pdf->Cell(10,0,$userData['interest2_wht'],0,0,'R'); 
			$lineStart += $lineBr ;
			
			//บรรทัด 15
			$pdf->SetXY($tab4, $lineStart );
			$pdf->Cell(10,0,$userData['interest3_year'],0,0,'R');
			$pdf->SetXY($tab5, $lineStart );
			$pdf->Cell(10,0,$userData['interest3_pay'],0,0,'R'); 
			$pdf->SetXY($tab6, $lineStart );
			$pdf->Cell(10,0,$userData['interest3_wht'],0,0,'R'); 
			$lineStart += $lineBr ;
			
			//บรรทัด 16
			$pdf->SetXY($tab4, $lineStart );
			$pdf->Cell(10,0,$userData['interest4_year'],0,0,'R');
			$pdf->SetXY($tab5, $lineStart );
			$pdf->Cell(10,0,$userData['interest4_pay'],0,0,'R'); 
			$pdf->SetXY($tab6, $lineStart );
			$pdf->Cell(10,0,$userData['interest4_wht'],0,0,'R'); 
			$lineStart += $lineBr + 4;
			 
			//บรรทัด 17
			$pdf->SetXY($tab4, $lineStart );
			$pdf->Cell(10,0,$userData['interest21_year'],0,0,'R');
			$pdf->SetXY($tab5, $lineStart );
			$pdf->Cell(10,0,$userData['interest21_pay'],0,0,'R'); 
			$pdf->SetXY($tab6, $lineStart );
			$pdf->Cell(10,0,$userData['interest21_wht'],0,0,'R'); 
			$lineStart += $lineBr + 4;
			
			//บรรทัด 18
			$pdf->SetXY($tab4, $lineStart );
			$pdf->Cell(10,0,$userData['interest22_year'],0,0,'R');
			$pdf->SetXY($tab5, $lineStart );
			$pdf->Cell(10,0,$userData['interest22_pay'],0,0,'R'); 
			$pdf->SetXY($tab6, $lineStart );
			$pdf->Cell(10,0,$userData['interest22_wht'],0,0,'R'); 
			$lineStart += $lineBr + 4 ;
			
			//บรรทัด 19
			$pdf->SetXY($tab4, $lineStart );
			$pdf->Cell(10,0,$userData['interest23_year'],0,0,'R');
			$pdf->SetXY($tab5, $lineStart );
			$pdf->Cell(10,0,$userData['interest23_pay'],0,0,'R'); 
			$pdf->SetXY($tab6, $lineStart );
			$pdf->Cell(10,0,$userData['interest23_wht'],0,0,'R'); 
			$lineStart += $lineBr;
			
			//บรรทัด 20
			$pdf->SetXY($tab4, $lineStart );
			$pdf->Cell(10,0,$userData['interest24_year'],0,0,'R');
			$pdf->SetXY($tab5, $lineStart );
			$pdf->Cell(10,0,$userData['interest24_pay'],0,0,'R'); 
			$pdf->SetXY($tab6, $lineStart );
			$pdf->Cell(10,0,$userData['interest24_wht'],0,0,'R'); 
			$lineStart += $lineBr - 1;
			
			//บรรทัด 21
			$pdf->SetXY($tab3Ex, $lineStart ); 
			$pdf->Write(0, iconv( 'UTF-8','cp874' ,  $userData['interest25_comment'])); 
			$pdf->SetXY($tab4, $lineStart );
			$pdf->Cell(10,0,$userData['interest25_year'],0,0,'R');
			$pdf->SetXY($tab5, $lineStart );
			$pdf->Cell(10,0,$userData['interest25_pay'],0,0,'R'); 
			$pdf->SetXY($tab6, $lineStart );
			$pdf->Cell(10,0,$userData['interest25_wht'],0,0,'R'); 
			$lineStart += (3*$lineBr) ;
			
			//บรรทัด 22
			$pdf->SetXY($tab4, $lineStart );
			$pdf->Cell(10,0,$userData['revenue_year'],0,0,'R');
			$pdf->SetXY($tab5, $lineStart );
			$pdf->Cell(10,0,$userData['revenue_pay'],0,0,'R'); 
			$pdf->SetXY($tab6, $lineStart );
			$pdf->Cell(10,0,$userData['revenue_wht'],0,0,'R'); 
			$lineStart += $lineBr + 1 ;
			
			//บรรทัด 23
			$pdf->SetXY($tab3Ex, $lineStart ); 
			$pdf->Write(0, iconv( 'UTF-8','cp874' ,  $userData['other_comment'])); 
			$pdf->SetXY($tab4, $lineStart );
			$pdf->Cell(10,0,$userData['other_year'],0,0,'R');
			$pdf->SetXY($tab5, $lineStart );
			$pdf->Cell(10,0,$userData['other_pay'],0,0,'R'); 
			$pdf->SetXY($tab6, $lineStart );
			$pdf->Cell(10,0,$userData['other_wht'],0,0,'R'); 
			$lineStart += $lineBr + 2;
			
			//บรรทัด 24 
			$pdf->SetXY($tab5, $lineStart );
			$pdf->Cell(10,0,$userData['total_pay'],0,0,'R'); 
			$pdf->SetXY($tab6, $lineStart );
			$pdf->Cell(10,0,$userData['total_wht'],0,0,'R'); 
			$lineStart += $lineBr ;
			 
			//บรรทัด 25
			$pdf->SetXY($tab3Ex, $lineStart );
			$pdf->Write(0, iconv( 'UTF-8','cp874' ,  $userData['total_alphabet'])); 
			$lineStart += $lineBr ;
			
			//บรรทัด 26
			$pdf->SetXY($tabSocial, $lineStart );
			$pdf->Write(0, iconv( 'UTF-8','cp874' , '0000')); 
			$pdf->SetXY($tabFund, $lineStart );
			$pdf->Write(0, iconv( 'UTF-8','cp874' ,  '0000')); 
			$lineStart += (3*$lineBr) + 2;
			
			//บรรทัด 27
			$pdf->SetXY($tabSocial, $lineStart );
			$pdf->Write(0, iconv( 'UTF-8','cp874' ,  $userData['sign_name'])); 
			$lineStart += $lineBr - 1;
			
			$pdf->SetXY($tabSocial - 5, $lineStart );
			$pdf->Write(0, iconv( 'UTF-8','cp874' ,   $sign_date->format('d'))); 
			$pdf->SetXY($tabSocial + 5, $lineStart );
			$pdf->Write(0, iconv( 'UTF-8','cp874' ,   $sign_date->format('m'))); 
			$pdf->SetXY($tabSocial + 22, $lineStart );
			$pdf->Write(0, iconv( 'UTF-8','cp874' ,   $sign_date->format('Y'))); 
			$lineStart += $lineBr + 2;
			
			// Output
			$pdf->Output($_SERVER["DOCUMENT_ROOT"].'/application/uploads/'. $pdf_name, 'I'); //D = download // I , F , S
			
		} catch (Exception $e) {
			echo 'Caught exception: ',  $e->getMessage(), "\n";
		}
	}
}
?>