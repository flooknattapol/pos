<?php
  
class SettingModel extends CI_Model {
	
    private $tbl_name = 'ma_business';
	private $id = 'ID';
 
    public function __construct() {
        parent::__construct();
    }
	
	public function getSettingNameById($id){
		$this->db->where($this->id, $id);
		return $this->db->get($this->tbl_name);
	}
	
	public function insert($modelData){
		 
	 	$this->db->insert($this->tbl_name, $modelData); 
		return $this->db->insert_id(); 
    }
     
    public function update($id, $modelData){
        $this->db->where($this->id, $id);
        return $this->db->update($this->tbl_name, $modelData);
    }
	
	public function getSettingNameAllList(){
        //return $this->db->count_all($this->tbl_name);
        
        $this->db->select('ID','NAME','SKU','PRICE','UNIT');
		$this->db->where('IsActive', 1);
        $query =  $this->db->get($this->tbl_name);
		
		return $query->result_array();
    }
	
	public function getSettingModel($id){
        //return $this->db->count_all($this->tbl_name);
        
        //$this->db->select('id','name','contact','address1','address2','address3','tel','email','taxid','website');
		$this->db->where('IsActive', 1);
		$this->db->where($this->id, $id);
        $query =  $this->db->get($this->tbl_name);
		
		return $query->result_array();
    }
	
	
	public function getSettingNameList($idSession){
		
		$this->load->model('UserModel', '', TRUE);
		// echo "idses".$idSession;
		if($idSession != ""){
			$idsession = $idSession; 
		}else{
			$idsession = $this->session->userdata('id');
		}
		$userPosition = $this->UserModel->getPostion($idsession);
		
		$sql = "SELECT ma_business.*
		FROM ". $this->tbl_name . " 
		WHERE ma_business.IsActive = 1 AND ID = '".$userPosition['BUSINESS_ID']."'"; 
		$query = $this->db->query($sql);
		return  $query->row_array();
	}
	public function getTranectionTosell($id){
			
		$sql = "SELECT ts_transection.*
		FROM ts_transection
		WHERE ts_transection.BUSINESS_ID = '".$id."'"; 
		 
		$query = $this->db->query($sql);
		return  $query->num_rows();
	}
	public function deleteSettingname($id){
		// echo $id; die();
		$result = false;
		try{
			$query = $this->getSettingNameById($id);
			// echo $id;die();
			// $modelData;			
			foreach ($query->result() as $row)
			{
			   		
				$modelData = array( 
					//'update_date' => date("Y-m-d H:i:s"),
					//'update_user' => $this->session->userdata('user_name'),
					// 'Edit'=>$this->session->userdata('user'),
					'IsActive' => 0 //$row->Setting_IsActive 
				); 
			}
			// echo $this->tbl_name;
			// print_r($modelData);die();
			$this->db->set('IsActive',0);
			$this->db->where($this->id, $id);
        	return $this->db->update($this->tbl_name);
			//return $this->update($id, $modelData);
			//$sql = "Delete FROM ". $this->tbl_name; 
			//return  $this->db->query($sql);
			
		}catch(Exception $ex){
			return $result;
		}
	}


	public function get($dataPost)
	{
		try {
			$idSession = isset($dataPost['idSession']) ? $dataPost['idSession'] : "";
			$result['status'] = true;
			$result['message'] = $this->getSettingNameList($idSession);
			$result['message2'] = $this->getTranectionTosell($result['message']['ID']);


		} catch (Exception $ex) {
			$result['status'] = false;
			$result['message'] = "exception: " . $ex;
		}

		return  $result;
	}
	public function add($dataPost )
	{
		$nResult = 0;
		try {

			$data['ID'] =  isset($dataPost['ID']) ? $dataPost['ID'] : 0;
			$data['EMAIL'] =  isset($dataPost['EMAIL']) ? $dataPost['EMAIL'] : "";
			$data['NAME'] =  isset($dataPost['NAME']) ? $dataPost['NAME'] : "";
			$data['SETTING_BAHTTOPOINT'] =  isset($dataPost['SETTING_BAHTTOPOINT']) ? $dataPost['SETTING_BAHTTOPOINT'] :0;
			$data['SETTING_BOOLEAN_POINT'] =  isset($dataPost['SETTING_BOOLEAN_POINT']) ? $dataPost['SETTING_BOOLEAN_POINT'] :0;
			$data['SETTING_REFUND_POINT'] =  isset($dataPost['SETTING_REFUND_POINT']) ? $dataPost['SETTING_REFUND_POINT'] :0;
			if ($data['ID'] > 0) {
				if(	$data['SETTING_BAHTTOPOINT']  == 0){
					$data['SETTING_REFUND_POINT'] = 0;
				}
				$nResult = $this->update($data['ID'], $data);	
			}
			if ($nResult > 0) {
				$result['status'] = true;
				$result['message'] = $this->lang->line("savesuccess");
				$result['data'] = $data;
			
			} else {
				$result['status'] = false;
				$result['message'] = $this->lang->line("error");
			}
			
		} catch (Exception $ex) {
			$result['status'] = false;
			$result['message'] = "exception: " . $ex;
		}

		return  $result;
	}
}
?>