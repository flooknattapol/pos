<?php
  
class BusinessCodeModel extends CI_Model {
	
    private $tbl_name = 'ma_business_code';
	private $id = 'ID';
 
    public function __construct() {
        parent::__construct();
    }
	
	public function getBusinessCodeNameById($id){
		$this->db->where($this->id, $id);
		return $this->db->get($this->tbl_name);
	}
	
	public function insert($modelData){
		 
	 	$this->db->insert($this->tbl_name, $modelData); 
		return $this->db->insert_id(); 
    }
     
    public function update($id, $modelData){
        $this->db->where($this->id, $id);
        return $this->db->update($this->tbl_name, $modelData);
    }
	
	public function getBusinessCodeNameAllList(){
        //return $this->db->count_all($this->tbl_name);
        
        $this->db->select('ID','NAME','SKU','PRICE','UNIT');
		$this->db->where('IsActive', 1);
        $query =  $this->db->get($this->tbl_name);
		
		return $query->result_array();
    }
	
	public function getBusinessCodeModel($id){
        //return $this->db->count_all($this->tbl_name);
        
        //$this->db->select('id','name','contact','address1','address2','address3','tel','email','taxid','website');
		$this->db->where('IsActive', 1);
		$this->db->where($this->id, $id);
        $query =  $this->db->get($this->tbl_name);
		
		return $query->result_array();
    }
	
	public function getSearchQuery($sql, $dataModel){
		
		//print_r($dataModel);
		// 	if(isset($dataModel['code']) && $dataModel['code'] != ""){
		// 		$sql .= " and emp_code like '%".$this->db->escape_str( $dataModel['code'])."%' ";
		//    }
		
		// 	if(isset($dataModel['code_id']) && $dataModel['code_id'] != ""){
		// 	 	$sql .= " and id_card_number like '%".$this->db->escape_str( $dataModel['code_id'])."%' ";
		// 	}
		
		// if(isset($dataModel['Member_Name']) && $dataModel['Member_Name'] != ""){
		//  	$sql .= " and ma_member.NAME like '%".$this->db->escape_str( $dataModel['Member_Name'])."%' ";
		// }
		if(isset($dataModel['NAME']) && $dataModel['NAME'] != ""){
			$sql .= " and ma_business_code.NAME like '%".$this->db->escape_str( $dataModel['NAME'])."%' ";
	   	}
	   	// if(isset($dataModel['SKU']) && $dataModel['SKU'] != ""){
		// $sql .= " and ma_business_code.SKU like '%".$this->db->escape_str( $dataModel['SKU'])."%' ";
   		// }
		// 	if(isset($dataModel['lastname']) && $dataModel['lastname'] != ""){
		// 	 	$sql .= " and lastname_th like '%".$this->db->escape_str( $dataModel['lastname'])."%' ";
		// 	}
			
		// 	if($dataModel['flag']==1){
		// 		$sql .= " and  if(end_date!=0,end_date,NOW()+1) > NOW() ";
		//    }
		// echo $sql;
		return $sql;
	}
	
	public function getTotal($dataModel ){
		
		$sql = "SELECT ma_business_code.*
		FROM ". $this->tbl_name . " 
		WHERE ma_business_code.IsActive = 1"; 

				
		$sql =  $this->getSearchQuery($sql, $dataModel);
		
		$query = $this->db->query($sql);		 
		
		return  $query->num_rows() ;
	}
	
	public function getBusinessCodeNameList($dataModel, $limit = 10, $offset = 0, $order = '', $direction = 'asc'){
		
		
		$sql = "SELECT ma_business_code.*
		FROM ". $this->tbl_name . " 
		WHERE ma_business_code.IsActive = 1"; 
		$sql =  $this->getSearchQuery($sql, $dataModel);	

		if($order != ""){
			$sql .= " ORDER BY ".$order." ".$direction;
		}else{
			$sql .= " ORDER BY ".$this->id." ".$direction;
		}
		
		$sql .= " LIMIT $offset, $limit";
		
		
		 
		$query = $this->db->query($sql);
		return  $query->result_array();
	}
	
	public function deleteBusinessCodename($id){
		// echo $id; die();
		$result = false;
		try{
			$query = $this->getBusinessCodeNameById($id);
			// echo $id;die();
			// $modelData;			
			foreach ($query->result() as $row)
			{
			   		
				$modelData = array( 
					//'update_date' => date("Y-m-d H:i:s"),
					//'update_user' => $this->session->userdata('user_name'),
					// 'Edit'=>$this->session->userdata('user'),
					'IsActive' => 0 //$row->BusinessCode_IsActive 
				); 
			}
			// echo $this->tbl_name;
			// print_r($modelData);die();
			$this->db->set('IsActive',0);
			$this->db->where($this->id, $id);
        	return $this->db->update($this->tbl_name);
			//return $this->update($id, $modelData);
			//$sql = "Delete FROM ". $this->tbl_name; 
			//return  $this->db->query($sql);
			
		}catch(Exception $ex){
			return $result;
		}
	}
	public function getBusinessCodeComboList(){
		
		$sql = "SELECT * FROM  ma_business_code
		WHERE IsActive = 1  "; 
		
		$query = $this->db->query($sql);
		return  $query->result_array();
	}
	
	### Controller #####
	public function add($dataPost)
	{
		
		$nResult = 0;

		try {
			$data['ID'] =  isset($dataPost['ID']) ? $dataPost['ID'] : 0;
			$data['EXPIRED_DATE'] =  isset($dataPost['EXPIRED_DATE']) ? $dataPost['EXPIRED_DATE'] : "";

			$alphabet = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890';
			$code = array(); //remember to declare $pass as an array
			$alphaLength = strlen($alphabet) - 1; //put the length -1 in cache
			for ($i = 0; $i < 8; $i++) {
				$n = rand(0, $alphaLength);
				$code[] = $alphabet[$n];
			}
			$data['CODE'] =implode($code);
			$data['IsActive'] = 1;
		
			if ($data['ID'] == 0) {
				$nResult = $this->insert($data);
			} else {
				$nResult = $this->update($data['ID'], $data);
			}

			if ($nResult > 0) {
				$result['status'] = true;
				$result['message'] = $this->lang->line("savesuccess");
			
			} else {
				$result['status'] = false;
				$result['message'] = $this->lang->line("error");
			}
		} catch (Exception $ex) {
			$result['status'] = false;
			$result['message'] = "exception: " . $ex;
		}

		return $result;
	}
	public function delete($dataPost)
	{
		try {
			$id =  isset($dataPost['ID']) ? $dataPost['ID'] : 0; // $this->input->post('ap_id');
			$bResult = $this->deleteBusinessCodename($id);
			if ($bResult) {
				$result['status'] = true;
				$result['message'] = $this->lang->line("savesuccess");
			} else {
				$result['status'] = false;
				$result['message'] = $this->lang->line("error_faliure");
			}
		} catch (Exception $ex) {
			$result['status'] = false;
			$result['message'] = "exception: " . $ex;
		}

		return $result;
	}
	public function upload_file()
    {

        $date = date('Y-m-d-H-i-s');

        $todayfile = 'imgEvent' . '_' . $date;
        $config['upload_path'] = FCPATH . 'upload/BusinessCode';
        $config['allowed_categorys'] = 'png';
        $config['max_size'] = 1000000;
        $config['max_width'] = 1024;
        $config['max_height'] = 768;
        $config['file_name'] = $todayfile;
        $file = explode('.', $_FILES['filegeneralinfo']['name']);
        $this->load->library('upload', $config);
        $this->upload->initialize($config);
        $status = true;
        if (!$this->upload->do_upload('filegeneralinfo')) {
            $status = false;
            echo json_encode(array('status' => $status,  'message' => $this->upload->display_errors(), 'path' => $config['upload_path']));
        } else {
			$data = array('upload_data' => $this->upload->data());
			echo json_encode(array('status' => $status, 'message' => $data['upload_data']['file_name'], 'path' => $config['upload_path']));
        }
	}
	public function getList($dataPost)
	{

		try {

			$PageIndex =  isset($dataPost['PageIndex']) ? $dataPost['PageIndex'] : 1;
			$PageSize =  isset($dataPost['PageSize']) ? $dataPost['PageSize'] : 20;
			$direction =  isset($dataPost['SortColumn']) ? $dataPost['SortColumn'] : "";
			$SortOrder = isset($dataPost['SortOrder']) ? $dataPost['SortOrder'] : "asc";
			$dataModel = isset($dataPost['mSearch']) ? $dataPost['mSearch'] : "";

			$offset = ($PageIndex - 1) * $PageSize;

			$result['status'] = true;
			$result['message'] = $this->getBusinessCodeNameList($dataModel, $PageSize, $offset, $direction, $SortOrder);
			$result['totalRecords'] = $this->getTotal($dataModel);
			$result['toTalPage'] = ceil($result['totalRecords'] / $PageSize);

			//$result['message'] = $this->BusinessCodeModel->getBusinessCodeModel(); 

		} catch (Exception $ex) {
			$result['status'] = false;
			$result['message'] = "exception: " . $ex;
		}

		return $result;
	}
	public function getComboList()
	{

		try {
			$result['status'] = true;
			$result['message'] = $this->getBusinessCodeComboList();
		} catch (Exception $ex) {
			$result['status'] = false;
			$result['message'] = "exception: " . $ex;
		}

		return $result;
	}
}
?>