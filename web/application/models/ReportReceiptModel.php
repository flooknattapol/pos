<?php
  
class ReportReceiptModel extends CI_Model {
	
	private $id = 'ID';
 
    public function __construct() {
        parent::__construct();
    }
	
	
	
	public function GetToregister($Id_Event){
		
		$sql = "SELECT * FROM ma_event_member WHERE EVENT_ID = '".$Id_Event."'  AND  BUYER = '1'";	
		$query = $this->db->query($sql);		 
		return  $query->num_rows() ;
	}
	public function GetVendor($Id_Event){
		
		$sql = "SELECT * FROM ma_event_member WHERE EVENT_ID = '".$Id_Event."'  AND  VENDOR = '1'";	
		$query = $this->db->query($sql);		 
		return  $query->num_rows() ;
	}
	public function GetSummaryToThis(){
		$sql = "SELECT * FROM ma_event WHERE IsActive = 1
		ORDER by DATE_START DESC"; 
		$query = $this->db->query($sql);
		return  $query->row_array();
	}
	public function GetTotalShopSale($m_id){
		
		$sql = "SELECT ts_relation_stamp.*,COUNT(ts_relation_stamp.VENDOR_ID) as CountStampVendor 
		,ma_member.VENDOR_NAME as Vendor_name
		FROM ts_relation_stamp
		LEFT JOIN ma_member ON  ts_relation_stamp.VENDOR_ID = ma_member.ID
		WHERE EVENT_ID = '".$m_id."'
		GROUP by ts_relation_stamp.VENDOR_ID,ts_relation_stamp.EVENT_ID"; 
		$query = $this->db->query($sql);
		return  $query->result_array();
	}
	public function GetPercentTypeGoods($m_id){
		
		$sql = "SELECT ts_relation_stamp.*,ma_goodstype.NAME,COUNT(ma_goods.TYPE) as CountStampTypeGoods
		FROM ts_relation_stamp
       	LEFT JOIN ma_goods ON ts_relation_stamp.GOODS_ID = ma_goods.ID
        LEFT JOIN ma_goodstype ON ma_goods.TYPE = ma_goodstype.ID
		WHERE EVENT_ID = '".$m_id."'
		GROUP by ts_relation_stamp.EVENT_ID,ma_goods.TYPE"; 
		$query = $this->db->query($sql);
		return  $query->result_array();
	}
	
	// public function deleteEventname($id){
	// 	$result = false;
	// 	try{
	// 		$query = $this->getEventNameById($id);		
	// 		foreach ($query->result() as $row)
	// 		{
			   		
	// 			$modelData = array( 
	// 				'Edit'=>$this->session->userdata('user'),
	// 				'IsActive' => 0 //$row->Event_IsActive 
	// 			); 
	// 		}
			
	// 		$this->db->where($this->id, $id);
    //     	return $this->db->update($this->tbl_name, $modelData);
	// 	}catch(Exception $ex){
	// 		return $result;
	// 	}
    // }
}
?>