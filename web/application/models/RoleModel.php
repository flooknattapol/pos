<?php
  
class RoleModel extends CI_Model {
	
    private $tbl_name = 'ma_role';
	private $id = 'ID';
 
    public function __construct() {
        parent::__construct();
    }
	
	public function getRoleNameById($id){
		$this->db->where($this->id, $id);
		return $this->db->get($this->tbl_name);
	}
	
	public function insert($modelData){
		 
	 	$this->db->insert($this->tbl_name, $modelData); 
		return $this->db->insert_id(); 
    }
     
    public function update($id, $modelData){
        $this->db->where($this->id, $id);
        return $this->db->update($this->tbl_name, $modelData);
    }
	
	public function getRoleNameAllList(){
        //return $this->db->count_all($this->tbl_name);
        
        $this->db->select('ID','NAME','SKU','PRICE','UNIT');
		$this->db->where('IsActive', 1);
        $query =  $this->db->get($this->tbl_name);
		
		return $query->result_array();
    }
	
	public function getRoleModel($id){
        //return $this->db->count_all($this->tbl_name);
        
        //$this->db->select('id','name','contact','address1','address2','address3','tel','email','taxid','website');
		$this->db->where('IsActive', 1);
		$this->db->where($this->id, $id);
        $query =  $this->db->get($this->tbl_name);
		
		return $query->result_array();
    }
	
	public function getSearchQuery($sql, $dataModel){
		
		//print_r($dataModel);
		// 	if(isset($dataModel['code']) && $dataModel['code'] != ""){
		// 		$sql .= " and emp_code like '%".$this->db->escape_str( $dataModel['code'])."%' ";
		//    }
		
		// 	if(isset($dataModel['code_id']) && $dataModel['code_id'] != ""){
		// 	 	$sql .= " and id_card_number like '%".$this->db->escape_str( $dataModel['code_id'])."%' ";
		// 	}
		
		if(isset($dataModel['Member_Name']) && $dataModel['Member_Name'] != ""){
		 	$sql .= " and ma_member.NAME like '%".$this->db->escape_str( $dataModel['Member_Name'])."%' ";
		}
		if(isset($dataModel['NAME']) && $dataModel['NAME'] != ""){
			$sql .= " and ma_Role.NAME like '%".$this->db->escape_str( $dataModel['NAME'])."%' ";
	   	}
	   	if(isset($dataModel['SKU']) && $dataModel['SKU'] != ""){
		$sql .= " and ma_Role.SKU like '%".$this->db->escape_str( $dataModel['SKU'])."%' ";
   		}
		// 	if(isset($dataModel['lastname']) && $dataModel['lastname'] != ""){
		// 	 	$sql .= " and lastname_th like '%".$this->db->escape_str( $dataModel['lastname'])."%' ";
		// 	}
			
		// 	if($dataModel['flag']==1){
		// 		$sql .= " and  if(end_date!=0,end_date,NOW()+1) > NOW() ";
		//    }
		// echo $sql;
		return $sql;
	}
	
	public function getTotal($dataModel ){
		
		$this->load->model('UserModel', '', TRUE);
		if(! $this->session->userdata('validated')){

		}else{
			$idsession = $this->session->userdata('id');
		}
		$userPosition = $this->UserModel->getPostion($idsession);
		
		$sql = "SELECT ma_role.*
		,COUNT(ma_role.ID) as totalemp
		FROM ma_role
       	LEFT JOIN  ma_user_position ON ma_role.ID = ma_user_position.ROLE_ID
		WHERE ma_role.IsActive = 1 AND ma_role.BUSINESS_ID = '".$userPosition['BUSINESS_ID']."'
        GROUP BY ma_role.ID";
		$sql =  $this->getSearchQuery($sql, $dataModel);
		
		$query = $this->db->query($sql);		 
		
		return  $query->num_rows() ;
	}
	
	public function getRoleNameList($dataModel, $limit = 10, $offset = 0, $order = '', $direction = 'asc'){
		
		$this->load->model('UserModel', '', TRUE);
		if(! $this->session->userdata('validated')){

		}else{
			$idsession = $this->session->userdata('id');
		}
		$userPosition = $this->UserModel->getPostion($idsession);

		// $sql = "SELECT ma_role.*
		// FROM ". $this->tbl_name . " 
		// WHERE ma_role.IsActive = 1 AND ma_role.BUSINESS_ID = '".$userPosition['BUSINESS_ID']."'"; 


		$sql = "SELECT ma_role.*
		,COUNT(ma_role.ID) as totalemp
		FROM ma_role
       	LEFT JOIN  ma_user_position ON ma_role.ID = ma_user_position.ROLE_ID
		WHERE ma_role.IsActive = 1 AND ma_role.BUSINESS_ID = '".$userPosition['BUSINESS_ID']."'
		GROUP BY ma_role.ID";
		
		if($order != ""){
			$sql .= " ORDER BY ".$order." ".$direction;
		}else{
			$sql .= " ORDER BY ".$this->id." ".$direction;
		}
		
		$sql .= " LIMIT $offset, $limit";
		
		$sql =  $this->getSearchQuery($sql, $dataModel);	
		
		 
		$query = $this->db->query($sql);
		return  $query->result_array();
	}
	
	public function deleteRolename($id){
		// echo $id; die();
		$result = false;
		try{
			$query = $this->getRoleNameById($id);
			// echo $id;die();
			// $modelData;			
			foreach ($query->result() as $row)
			{
			   		
				$modelData = array( 
					//'update_date' => date("Y-m-d H:i:s"),
					//'update_user' => $this->session->userdata('user_name'),
					// 'Edit'=>$this->session->userdata('user'),
					'IsActive' => 0 //$row->Role_IsActive 
				); 
			}
			// echo $this->tbl_name;
			// print_r($modelData);die();
			$this->db->set('IsActive',0);
			$this->db->where($this->id, $id);
        	return $this->db->update($this->tbl_name);
			//return $this->update($id, $modelData);
			//$sql = "Delete FROM ". $this->tbl_name; 
			//return  $this->db->query($sql);
			
		}catch(Exception $ex){
			return $result;
		}
	}
	public function getRoleComboList($id){
		
		if($id > 0){
			$sql = "SELECT * FROM ". $this->tbl_name . " WHERE IsActive = 1 AND BUSINESS_ID = '".$id."'";
		}else{
			$sql = "SELECT * FROM ". $this->tbl_name . " WHERE IsActive = 1";
		}
		$query = $this->db->query($sql);
		return  $query->result_array();
	}


	###controller####
	#### controller ###
	public function add($dataPost )
	{
		$nResult = 0;
		try {
			$this->load->model('UserModel', '', TRUE);
			if(! $this->session->userdata('validated')){

			}else{
				$idsession = $this->session->userdata('id');
			}
			$userPosition = $this->UserModel->getPostion($idsession);
			$data['ID'] =  isset($dataPost['ID']) ? $dataPost['ID'] : 0;
			$data['NAME'] =  isset($dataPost['NAME']) ? $dataPost['NAME'] : "";
			$data['PERMISSION_DASHBOARD_IsActive'] =  isset($dataPost['PERMISSION_DASHBOARD_IsActive']) ? $dataPost['PERMISSION_DASHBOARD_IsActive'] : "";
			$data['PERMISSION_POS_IsActive'] =  isset($dataPost['PERMISSION_POS_IsActive']) ? $dataPost['PERMISSION_POS_IsActive'] : "";
			$data['PERMISSION_POS'] =  isset($dataPost['PERMISSION_POS']) ? $dataPost['PERMISSION_POS'] : "";
			$data['PERMISSION_DASHBOARD'] =  isset($dataPost['PERMISSION_DASHBOARD']) ? $dataPost['PERMISSION_DASHBOARD'] : "";
			$data['BUSINESS_ID'] = $userPosition['BUSINESS_ID'];
			$data['IsActive'] = 1;

		
			if ($data['ID'] == 0) {
				$nResult = $this->insert($data);
			} else {
				$nResult = $this->update($data['ID'], $data);
			}
			if ($nResult > 0) {
				$result['status'] = true;
				$result['message'] = $this->lang->line("savesuccess");
			
			} else {
				$result['status'] = false;
				$result['message'] = $this->lang->line("error");
			}
			
		} catch (Exception $ex) {
			$result['status'] = false;
			$result['message'] = "exception: " . $ex;
		}

		return  $result;
	}

	public function delete($dataPost )
	{
		try {
			$id =  isset($dataPost['ID']) ? $dataPost['ID'] : 0; // $this->input->post('ap_id');

			$bResult = $this->deleteRolename($id);

			if ($bResult) {
				$result['status'] = true;
				$result['message'] = $this->lang->line("savesuccess");
			} else {
				$result['status'] = false;
				$result['message'] = $this->lang->line("error_faliure");
			}
		} catch (Exception $ex) {
			$result['status'] = false;
			$result['message'] = "exception: " . $ex;
		}

		return  $result;
	}
	

	public function getList($dataPost )
	{

		try {

			$PageIndex =  isset($dataPost['PageIndex']) ? $dataPost['PageIndex'] : 1;
			$PageSize =  isset($dataPost['PageSize']) ? $dataPost['PageSize'] : 20;
			$direction =  isset($dataPost['SortColumn']) ? $dataPost['SortColumn'] : "";
			$SortOrder = isset($dataPost['SortOrder']) ? $dataPost['SortOrder'] : "asc";
			$dataModel = isset($dataPost['mSearch']) ? $dataPost['mSearch'] : "";

			$offset = ($PageIndex - 1) * $PageSize;

			$result['status'] = true;
			$result['message'] = $this->getRoleNameList($dataModel, $PageSize, $offset, $direction, $SortOrder);
			$result['totalRecords'] = $this->getTotal($dataModel);
			$result['toTalPage'] = ceil($result['totalRecords'] / $PageSize);

		} catch (Exception $ex) {
			$result['status'] = false;
			$result['message'] = "exception: " . $ex;
		}

		return  $result;
	}
	public function getComboList($dataPost){
		try{ 
			$idSession = isset($dataPost['idSession']) ? $dataPost['idSession'] : "";
			$this->load->model('UserModel', '', TRUE);
			if($idSession != ""){
				$idsession = $idSession; 
			}else{
				$idsession = $this->session->userdata('id');
			}
			// echo $id;die();
			$result['status'] = true;
			$result['message'] = $this->getRoleComboList($idsession);
		}catch(Exception $ex){
			$result['status'] = false;
			$result['message'] = "exception: ".$ex;
		}
		
		return $result;	
	}
}
?>