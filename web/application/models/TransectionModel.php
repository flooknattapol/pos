<?php
  
class TransectionModel extends CI_Model {
	
    private $tbl_name = 'ts_transection';
	private $id = 'ID';
 
    public function __construct() {
        parent::__construct();
    }
	
	public function getTransectionNameById($id){
		$this->db->where($this->id, $id);
		return $this->db->get($this->tbl_name);
	}
	
	public function insert($modelData){
		 
	 	$this->db->insert($this->tbl_name, $modelData); 
		return $this->db->insert_id(); 
    }
	public function inserttemporder($modelData){
		 
		$this->db->insert("temp_order", $modelData); 
	   return $this->db->insert_id(); 
   }
    public function update($id, $modelData){
        $this->db->where($this->id, $id);
        return $this->db->update($this->tbl_name, $modelData);
    }
	public function getSearchQuery($sql, $dataModel){
		
		if(isset($dataModel['NAME']) && $dataModel['NAME'] != ""){
			$sql .= " and ts_transection.ORDER_NAME like '%".$this->db->escape_str( $dataModel['NAME'])."%' ";
	   	}
		if(isset($dataModel['DateEnd']) && $dataModel['DateEnd'] != "" && isset($dataModel['DateStart']) && $dataModel['DateStart'] != ""){
			$sql .= " AND date(ts_transection.TimeStamp) BETWEEN '".$dataModel['DateStart']."' AND '".$dataModel['DateEnd']."'";
		}
		

		return $sql;
	}
	public function getTotal($dataModel,$idSession ,$idBusiness){
		if($idBusiness != ""){
			$userPosition['BUSINESS_ID'] = $idBusiness;
		}else{
			$this->load->model('UserModel', '', TRUE);
			if($idSession != ""){
				$idsession = $idSession; 
			}else{
				$idsession = $this->session->userdata('id');
			}
			$userPosition = $this->UserModel->getPostion($idsession);
		}
		$sql = "SELECT * FROM ". $this->tbl_name . " 
		WHERE BUSINESS_ID = '".$userPosition['BUSINESS_ID']."'"; 
		$sql =  $this->getSearchQuery($sql, $dataModel);
		
		$query = $this->db->query($sql);		 
		
		return  $query->num_rows() ;
	}
	
	public function getTransectionNameList($dataModel, $limit = 10, $offset = 0, $order = '', $direction = 'asc',$idSession,$idBusiness){
		
		if($idBusiness != ""){
			$userPosition['BUSINESS_ID'] = $idBusiness;
		}else{
			$this->load->model('UserModel', '', TRUE);
			if($idSession != ""){
				$idsession = $idSession; 
			}else{
				$idsession = $this->session->userdata('id');
			}
			$userPosition = $this->UserModel->getPostion($idsession);
		}
		
		
		$sql = "SELECT ts_transection.*,ts_discount.AMOUNT_DISCOUNT,ma_customer.userID,
		CASE
			WHEN ts_transection.CUSTOMER_ID > 0 THEN ma_customer.NAME
			ELSE 'ลูกค้าทั่วไป'
		END AS 'CUSTOMER_NAME'
		FROM ". $this->tbl_name . " 
		LEFT JOIN ma_customer ON ts_transection.CUSTOMER_ID = ma_customer.ID
		LEFT JOIN ts_discount ON ts_transection.ID = ts_discount.TRANSECTION_ID
		WHERE ts_transection.BUSINESS_ID = '".$userPosition['BUSINESS_ID']."'"; 
		$sql =  $this->getSearchQuery($sql, $dataModel);	

		if($order != ""){
			$sql .= " ORDER BY ".$order." ".$direction;
		}else{
			$sql .= " ORDER BY ".$this->id." ".$direction;
		}
		
		$sql .= " LIMIT $offset, $limit";
		// echo $sql;die();
		$query = $this->db->query($sql);
		return  $query->result_array();
	}
	public function getListTempOrderNameList($dataModel, $limit = 10, $offset = 0, $order = '', $direction = 'asc',$idSession){
		$this->load->model('UserModel', '', TRUE);
		if($idSession != ""){
			$idsession = $idSession; 
		}else{
			$idsession = $this->session->userdata('id');
		}
		$userPosition = $this->UserModel->getPostion($idsession);
		
		$sql = "SELECT temp_order.*,ma_user.NAME AS SELLER_NAME
		FROM temp_order
		LEFT JOIN ma_user ON temp_order.SELLER_ID = ma_user.ID
		WHERE temp_order.BUSINESS_ID = '".$userPosition['BUSINESS_ID']."'"; 

		if($order != ""){
			$sql .= " ORDER BY ".$order." ".$direction;
		}else{
			$sql .= " ORDER BY ".$this->id." ".$direction;
		}
		
		$sql .= " LIMIT $offset, $limit";
		// echo $sql;die();
		$query = $this->db->query($sql);
		return  $query->result_array();
	}
	public function getTotalTempOrder($dataModel,$idSession ){
		$this->load->model('UserModel', '', TRUE);

		if($idSession != ""){
			$idsession = $idSession; 
		}else{
			$idsession = $this->session->userdata('id');
		}
		$userPosition = $this->UserModel->getPostion($idsession);
		$sql = "SELECT temp_order.*
		FROM temp_order
		WHERE temp_order.BUSINESS_ID = '".$userPosition['BUSINESS_ID']."'"; 
		
		$query = $this->db->query($sql);		 
		
		return  $query->num_rows() ;
	}
	public function deleteTransection($id){
		// echo $id; die();
		$result = false;
		try{
			$query = $this->getTransectionNameById($id);
			// echo $id;die();
			// $modelData;			
			foreach ($query->result() as $row)
			{
			   		
				$modelData = array( 
					//'update_date' => date("Y-m-d H:i:s"),
					//'update_user' => $this->session->userdata('user_name'),
					// 'Edit'=>$this->session->userdata('user'),
					'IsActive' => 0 //$row->Customer_IsActive 
				); 
			}
			// echo $this->tbl_name;
			// print_r($modelData);die();
			$this->db->set('IsActive',0);
			$this->db->where($this->id, $id);
        	return $this->db->update($this->tbl_name);
			//return $this->update($id, $modelData);
			//$sql = "Delete FROM ". $this->tbl_name; 
			//return  $this->db->query($sql);
			
		}catch(Exception $ex){
			return $result;
		}
	}
	public function addDiscount($id,$List){
		// print_r($data);die();
		$data['TRANSECTION_ID'] = $id;
		$data['AMOUNT_DISCOUNT'] = $List['AMOUNT_DISCOUNT'];
		return $this->db->insert('ts_discount',$data);
	}
	#### controller ###
	public function add($dataPost)
	{
		$nResult = 0;
		try {
			$idSession = isset($dataPost['idSession']) ? $dataPost['idSession'] : "";

			$this->load->model('UserModel', '', TRUE);
			if($idSession != ""){
				$idsession = $idSession; 
			}else{
				$idsession = $this->session->userdata('id');
			}
			$userPosition = $this->UserModel->getPostion($idsession);

			$data['ID'] =  isset($dataPost['ID']) ? $dataPost['ID'] : 0;
			$data['CUSTOMER_ID'] =  isset($dataPost['CUSTOMER_ID']) ? $dataPost['CUSTOMER_ID'] : "";
			$data['ORDER_NAME'] =  isset($dataPost['ORDER_NAME']) ? $dataPost['ORDER_NAME'] : "";
			$data['CATEGORYPAYMENT'] =  isset($dataPost['CATEGORYPAYMENT']) ? $dataPost['CATEGORYPAYMENT'] : "";
			$data['SELLER_ID'] =  isset($dataPost['SELLER_ID']) ? $dataPost['SELLER_ID'] : "";
			$data['TotalCost'] =  isset($dataPost['TotalCost']) ? $dataPost['TotalCost'] : "";
			$data['TotalOrder'] =  isset($dataPost['TotalOrder']) ? $dataPost['TotalOrder'] : "";
			$data['ORDERTYPE'] =  isset($dataPost['ORDERTYPE']) ? $dataPost['ORDERTYPE'] : "";
			$data['Received'] =  isset($dataPost['Received']) ? $dataPost['Received'] : "";
			$data['Change'] =  isset($dataPost['Change']) ? $dataPost['Change'] : "";
			$data['BUSINESS_ID'] = $userPosition['BUSINESS_ID'];
			$data['IsActive'] = 1;
			$Detail = isset($dataPost['Detail']) ? $dataPost['Detail'] : "";
			$Discount = isset($dataPost['Discount']) ? $dataPost['Discount'] : "";


			$sqlbusieness = "SELECT * FROM ma_business WHERE ID = '".$userPosition['BUSINESS_ID']."'";
			$resbusiness = $this->db->query($sqlbusieness)->row_array();
			
			if ($data['ID'] == 0) {
				$nResult = $this->insert($data);
				if($resbusiness['SETTING_BOOLEAN_POINT'] == 1){
					if($data['CUSTOMER_ID'] > 0){
						$sql = "SELECT * FROM ma_customer_relation
								WHERE CUSTOMER_ID = '".$data['CUSTOMER_ID']."' AND BUSINESS_ID = '".$data['BUSINESS_ID']."'";
						$res = $this->db->query($sql)->row_array();
		
						$point = $resbusiness['SETTING_BAHTTOPOINT']*$data['TotalOrder'];
						$newpoint = $res['REWARDPOINT']+$point;
						$this->db->set('REWARDPOINT', $newpoint );
						$this->db->where('CUSTOMER_ID', $data['CUSTOMER_ID']);
						$this->db->where('BUSINESS_ID', $data['BUSINESS_ID']);
						$this->db->update('ma_customer_relation'); 
		
						$dataReward['TRANSECTION_ID']= $nResult;
						$dataReward['BUSINESS_ID']= $userPosition['BUSINESS_ID'];
						$dataReward['TotalOrder']= $data['TotalOrder'];
						$dataReward['POINTREWARD']= $point;
						$dataReward['CUSTOMER_ID']= $data['CUSTOMER_ID'];
		
						$this->db->insert('ts_pointreward',$dataReward);
		
					}
				}
				
				$this->load->model('TransectionDetailModel', '', TRUE);
				$master_id = $nResult;
				if(is_array($Detail) && $master_id > 0){
					$nDetail = $this->TransectionDetailModel->listUpdate($master_id,  $Detail);
				}
				if(is_array($Discount) && $master_id > 0){
					$nDetail = $this->addDiscount($master_id,  $Discount);
				}
			}
			
			if ($nResult > 0) {
				$result['status'] = true;
				$result['message'] = $this->lang->line("savesuccess");
			
			} else {
				$result['status'] = false;
				$result['message'] = $this->lang->line("error");
			}
			
		} catch (Exception $ex) {
			$result['status'] = false;
			$result['message'] = "exception: " . $ex;
		}

		return  $result;
	}
	public function addTempOrder($dataPost)
	{
		$nResult = 0;
		try {
			$idSession = isset($dataPost['idSession']) ? $dataPost['idSession'] : "";

			$this->load->model('UserModel', '', TRUE);
			if($idSession != ""){
				$idsession = $idSession; 
			}else{
				$idsession = $this->session->userdata('id');
			}
			$userPosition = $this->UserModel->getPostion($idsession);

			$data['ID'] =  isset($dataPost['ID']) ? $dataPost['ID'] : 0;
			$data['ORDER_NAME'] =  isset($dataPost['ORDER_NAME']) ? $dataPost['ORDER_NAME'] : "";
			$data['BUSINESS_ID'] = $userPosition['BUSINESS_ID'];
			$detail = isset($dataPost['detail']) ? $dataPost['detail'] : "";
			$discount = isset($dataPost['discount']) ? $dataPost['discount'] : "";

			if ($data['ID'] == 0) {
				$nResult = $this->inserttemporder($data);
				$this->load->model('TransectionDetailModel', '', TRUE);
				$master_id = $nResult;
				if(is_array($detail) && $master_id > 0){
					$nDetail = $this->TransectionDetailModel->listUpdateTempdetail($master_id,  $detail);
				}
				if(is_array($discount) && $master_id > 0){
					$nDetail = $this->TransectionDetailModel->listUpdateTempdiscount($master_id,  $discount);
				}
			}
			
			if ($nResult > 0) {
				$result['status'] = true;
				$result['message'] = $this->lang->line("savesuccess");
			
			} else {
				$result['status'] = false;
				$result['message'] = $this->lang->line("error");
			}
			
		} catch (Exception $ex) {
			$result['status'] = false;
			$result['message'] = "exception: " . $ex;
		}

		return  $result;
	}
	public function getList($dataPost)
	{

		try {
			// print_r($dataPost);die();
			$PageIndex =  isset($dataPost['PageIndex']) ? $dataPost['PageIndex'] : 1;
			$PageSize =  isset($dataPost['PageSize']) ? $dataPost['PageSize'] : 20;
			$direction =  isset($dataPost['SortColumn']) ? $dataPost['SortColumn'] : "";
			$SortOrder = isset($dataPost['SortOrder']) ? $dataPost['SortOrder'] : "asc";
			$idSession = isset($dataPost['idSession']) ? $dataPost['idSession'] : "";
			$idBusiness = isset($dataPost['idBusiness']) ? $dataPost['idBusiness'] : "";
			$dataModel = isset($dataPost['mSearch']) ? $dataPost['mSearch'] : "";

			$offset = ($PageIndex - 1) * $PageSize;

			$result['status'] = true;
			$result['message'] = $this->getTransectionNameList($dataModel, $PageSize, $offset, $direction, $SortOrder,$idSession,$idBusiness);
			$result['totalRecords'] = $this->getTotal($dataModel,$idSession,$idBusiness);
			$result['toTalPage'] = ceil($result['totalRecords'] / $PageSize);

		} catch (Exception $ex) {
			$result['status'] = false;
			$result['message'] = "exception: " . $ex;
		}

		return  $result;
	}
	public function getListTempOrder($dataPost)
	{

		try {
			// print_r($dataPost);die();
			$PageIndex =  isset($dataPost['PageIndex']) ? $dataPost['PageIndex'] : 1;
			$PageSize =  isset($dataPost['PageSize']) ? $dataPost['PageSize'] : 20;
			$direction =  isset($dataPost['SortColumn']) ? $dataPost['SortColumn'] : "";
			$SortOrder = isset($dataPost['SortOrder']) ? $dataPost['SortOrder'] : "asc";
			$idSession = isset($dataPost['idSession']) ? $dataPost['idSession'] : "";
			$dataModel = isset($dataPost['mSearch']) ? $dataPost['mSearch'] : "";

			$offset = ($PageIndex - 1) * $PageSize;

			$result['status'] = true;
			$result['message'] = $this->getListTempOrderNameList($dataModel, $PageSize, $offset, $direction, $SortOrder,$idSession);
			$result['totalRecords'] = $this->getTotalTempOrder($dataModel,$idSession);
			$result['toTalPage'] = ceil($result['totalRecords'] / $PageSize);

		} catch (Exception $ex) {
			$result['status'] = false;
			$result['message'] = "exception: " . $ex;
		}

		return  $result;
	}
	public function delete($dataPost )
	{
		try {
			$id =  isset($dataPost['ID']) ? $dataPost['ID'] : 0; // $this->input->post('ap_id');

			$this->load->model('TransectionDetailModel', '', TRUE);
			// $bResult = $this->TransectionDetailModel->deleteTransectionAllList($id);
			// $bResult = $this->deleteTransection($id);

			$sql = "SELECT * FROM ts_transection WHERE ID = '".$id."'";
			$res = $this->db->query($sql)->row_array();

			$sqlbusieness = "SELECT * FROM ma_business WHERE ID = '".$res['BUSINESS_ID']."'";
			$resbusiness = $this->db->query($sqlbusieness)->row_array();

			if($resbusiness['SETTING_REFUND_POINT'] == 1){
				if($res['CUSTOMER_ID'] > 0){
					$sqlrefundpoint = "SELECT * FROM ts_pointreward WHERE TRANSECTION_ID = '".$id."'";
					$resrefunpoint = $this->db->query($sqlrefundpoint)->row_array();

					$sqlcustomer = "SELECT * FROM ma_customer_relation WHERE CUSTOMER_ID = '".$resrefunpoint['CUSTOMER_ID']."'";
					$rescustomer = $this->db->query($sqlcustomer)->row_array();
					
					if($resrefunpoint != "" && $rescustomer != ""){
						$newpoint = $rescustomer['REWARDPOINT'] - $resrefunpoint['POINTREWARD'];
						$this->db->set('REWARDPOINT',$newpoint);
						$this->db->where('CUSTOMER_ID',$resrefunpoint['CUSTOMER_ID']);
						$this->db->where('BUSINESS_ID',$resrefunpoint['BUSINESS_ID']);
						$this->db->update('ma_customer_relation');

						$resrefunpointnew['POINTREWARD'] = $resrefunpoint['POINTREWARD']*-1;
						$resrefunpointnew['BUSINESS_ID'] = $resrefunpoint['BUSINESS_ID'];
						$resrefunpointnew['TRANSECTION_ID'] = $resrefunpoint['TRANSECTION_ID'];
						$resrefunpointnew['CUSTOMER_ID'] = $resrefunpoint['CUSTOMER_ID'];
						$resrefunpointnew['TotalOrder'] = $resrefunpoint['TotalOrder'];
						$this->db->insert('ts_pointreward',$resrefunpointnew);
					}
				}
			}

			$refund['TRANSECTION_ID'] = $res['ID'];
			$refund['AMOUNT'] = $res['TotalOrder'];
			$this->db->insert('report_refund',$refund);
			
			$this->db->set('STATUS',0);
			$this->db->set('IsActive',1);
			$this->db->where($this->id, $id);
			$this->db->update($this->tbl_name);
			
			$sqlMaxId = "SELECT MAX(ID)as Max_id FROM ts_transection";
			$maxId = $this->db->query($sqlMaxId)->row_array();
			$today = date("Ymd"); 
			$order_id = $id.$today;
			$res['REFERENCE_ORDER_NAME'] = $res['ORDER_NAME'];
			$res['IsActive'] = 0;
			$res['STATUS'] = 0;
			$res['ORDER_NAME'] = $order_id;
			$res['ID'] = $maxId['Max_id'] + 1;
			$this->db->insert("ts_transection", $res); 



			$sqldetail = "SELECT * FROM ts_transection_detail WHERE TRANSECTION_ID = '".$id."'";
			$resdetail = $this->db->query($sqldetail)->result_array();
			foreach($resdetail as $rs){

				$this->db->set('STATUS',0);
				$this->db->set('IsActive',1);
				$this->db->where($this->id, $rs['ID']);
				$this->db->update("ts_transection_detail");
				
				$detail['TRANSECTION_ID'] = $res['ID'];
				$detail['GOODS_ID'] = $rs['GOODS_ID'];
				$detail['PRICE'] = $rs['PRICE'];
				$detail['COST'] = $rs['COST'];
				$detail['IsActive'] = 0;
				$detail['STATUS'] = 0;
				$this->db->insert("ts_transection_detail", $detail); 
				$bResult = $this->db->insert_id();

				$this->db->set('STOCK', 'STOCK+1', FALSE);
				$this->db->where('ID', $rs['GOODS_ID']);
				$this->db->where('STOCK_IsActive', 1);
				$this->db->update('ma_goods');	
			}
			if ($bResult) {
				$result['status'] = true;
				$result['message'] = $this->lang->line("savesuccess");
			} else {
				$result['status'] = false;
				$result['message'] = $this->lang->line("error_faliure");
			}
		} catch (Exception $ex) {
			$result['status'] = false;
			$result['message'] = "exception: " . $ex;
		}

		return  $result;
	}
	public function getTempOrder($dataPost )
	{
		try {
			$id =  isset($dataPost['ID']) ? $dataPost['ID'] : 0; // $this->input->post('ap_id');
			// echo $id;die();
		
			$sqldetail = "SELECT * FROM temp_order_detail WHERE TRANSECTION_ID = '".$id."'";
			$resDetail = $this->db->query($sqldetail)->result_array();
			$this->db->where('id', $id);
			$this->db->delete('temp_order');
			
			$tempgoods =  array();
			$tempdiscount =  array();
			if (count($resDetail) > 0) {
				foreach($resDetail as $rsdt){
					$sql = "SELECT * FROM ma_goods WHERE ID = '".$rsdt['GOODS_ID']."'";
					$resgoods = $this->db->query($sql)->row_array();
					array_push($tempgoods, $resgoods);
				}
				$result['tempgoods'] = $tempgoods;
				$this->db->where('TRANSECTION_ID', $id);
				$this->db->delete('temp_order_detail');
				$sqldiscount = "SELECT * FROM temp_order_discount WHERE TRANSECTION_ID = '".$id."'";
				$resDiscount = $this->db->query($sqldiscount)->result_array();
				if(count($resDiscount) > 0){
					foreach($resDiscount as $rsdt){
						$sql = "SELECT * FROM ma_discount WHERE ID = '".$rsdt['DISCOUNT_ID']."'";
						$resdiscountdata = $this->db->query($sql)->row_array();
						array_push($tempdiscount, $resdiscountdata);
					}
					$result['tempdiscount'] = $tempdiscount;
					$this->db->where('TRANSECTION_ID', $id);
					$this->db->delete('temp_order_discount');
				}else{
					$result['tempdiscount'] = [];
				}
				$result['status'] = true;
			} else {
				$result['status'] = false;
				$result['message'] = $this->lang->line("error_faliure");
			}
		} catch (Exception $ex) {
			$result['status'] = false;
			$result['message'] = "exception: " . $ex;
		}

		return  $result;
	}
	public function deletetemporder($dataPost )
	{
		try {
			$id =  isset($dataPost['ID']) ? $dataPost['ID'] : 0; // $this->input->post('ap_id');
			// echo $id;die();
		
			
			$this->db->where('id', $id);
			$this->db->delete('temp_order');
			$this->db->where('TRANSECTION_ID', $id);
			$this->db->delete('temp_order_detail');
			$this->db->where('TRANSECTION_ID', $id);
			$this->db->delete('temp_order_discount');
			$result['status'] = true;
			
		} catch (Exception $ex) {
			$result['status'] = false;
			$result['message'] = "exception: " . $ex;
		}

		return  $result;
	}
	
}
?>