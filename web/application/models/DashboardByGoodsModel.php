<?php
  
class DashboardByGoodsModel extends CI_Model {
	
	private $id = 'ID';
 
    public function __construct() {
        parent::__construct();
    }
	
	public function getSearchQuery($sql, $dataModel){
		

		if(isset($dataModel['DateEnd']) && $dataModel['DateEnd'] != "" && isset($dataModel['DateStart']) && $dataModel['DateStart'] != ""){
			$sql .= " and ts_transection.TimeStamp >= '".$dataModel['DateStart']."' and ts_transection.TimeStamp <= '".$dataModel['DateEnd']."'";
		}

		return $sql;
	}
	public function getHeader($dataPost){

		$idSession = isset($dataPost['idSession']) ? $dataPost['idSession'] : "";

		// print_r($dataPost);die();
		$this->load->model('UserModel', '', TRUE);
		if($idSession != ""){
			$idsession = $idSession; 
		}else{
			$idsession = $this->session->userdata('id');
		}
		$userPosition = $this->UserModel->getPostion($idsession);
		
 
		$sql = "SELECT ma_goods.NAME As GOODS_NAME,SUM(ts_transection_detail.PRICE) AS TotalGoods,ts_transection.*,ts_transection_detail.* FROM ts_transection
		RIGHT JOIN ts_transection_detail ON ts_transection.ID = ts_transection_detail.TRANSECTION_ID
		LEFT JOIN ma_goods ON ts_transection_detail.GOODS_ID = ma_goods.ID
		WHERE ts_transection.BUSINESS_ID = '".$userPosition['BUSINESS_ID']."'AND ts_transection.IsActive = 1
		AND date(ts_transection.TimeStamp) BETWEEN '".$dataPost['DateStart']."' AND '".$dataPost['DateEnd']."'
		GROUP BY ts_transection_detail.GOODS_ID
		ORDER BY TotalGoods DESC
		LIMIT 5";
		// $sql =  $this->getSearchQuery($sql, $dataModel);

		$query = $this->db->query($sql);
		return  $query->result_array();
	}
	public function getDashboardByGoodsNameList($dataPost,$idSession){
		$this->load->model('UserModel', '', TRUE);
		if($idSession != ""){
			$idsession = $idSession; 
		}else{
			$idsession = $this->session->userdata('id');
		}
		$userPosition = $this->UserModel->getPostion($idsession);
		$sql = "SELECT date(ts_transection.TimeStamp)as Date,ma_goods.NAME As GOODS_NAME,ma_goods_category.NAME,count(ts_transection_detail.GOODS_ID) AS TotalGoods,SUM(ts_transection_detail.PRICE) AS TotalGoodsPrice,SUM(ts_transection_detail.COST) AS TotalCOST,ts_transection.*,ts_transection_detail.* FROM ts_transection
		RIGHT JOIN ts_transection_detail ON ts_transection.ID = ts_transection_detail.TRANSECTION_ID
		LEFT JOIN ma_goods ON ts_transection_detail.GOODS_ID = ma_goods.ID
		LEFT JOIN ma_goods_category ON ma_goods.CATEGORY_ID = ma_goods_category.ID
		WHERE ts_transection.BUSINESS_ID = '".$userPosition['BUSINESS_ID']."' 
		AND date(ts_transection.TimeStamp) BETWEEN '".$dataPost['DateStart']."' AND '".$dataPost['DateEnd']."'
		GROUP BY ts_transection_detail.GOODS_ID , ts_transection.IsActive
		ORDER BY Date DESC";
		
		$query = $this->db->query($sql);
		return  $query->result_array();
	}
	public function getTotal($dataPost,$idSession ){
		$this->load->model('UserModel', '', TRUE);
		if($idSession != ""){
			$idsession = $idSession; 
		}else{
			$idsession = $this->session->userdata('id');
		}
		$userPosition = $this->UserModel->getPostion($idsession);
 
		$sql = "SELECT date(ts_transection.TimeStamp)as Date,ma_goods.NAME As GOODS_NAME,ma_goods_category.NAME,count(ts_transection_detail.GOODS_ID) AS TotalGoods,SUM(ts_transection_detail.PRICE) AS TotalGoodsPrice,SUM(ts_transection_detail.COST) AS TotalCOST,ts_transection.*,ts_transection_detail.* FROM ts_transection
		RIGHT JOIN ts_transection_detail ON ts_transection.ID = ts_transection_detail.TRANSECTION_ID
		LEFT JOIN ma_goods ON ts_transection_detail.GOODS_ID = ma_goods.ID
		LEFT JOIN ma_goods_category ON ma_goods.CATEGORY_ID = ma_goods_category.ID
		WHERE ts_transection.BUSINESS_ID = '".$userPosition['BUSINESS_ID']."' 
		AND date(ts_transection.TimeStamp) BETWEEN '".$dataPost['DateStart']."' AND '".$dataPost['DateEnd']."'
		GROUP BY ts_transection_detail.GOODS_ID , ts_transection.IsActive
		ORDER BY Date DESC";
		
		$query = $this->db->query($sql);
		return  $query->num_rows() ;
	}
	public function getList($dataPost)
	{

		try {
			// print_r($dataPost);die();
			$PageIndex =  isset($dataPost['PageIndex']) ? $dataPost['PageIndex'] : 1;
			$PageSize =  isset($dataPost['PageSize']) ? $dataPost['PageSize'] : 20;
			$direction =  isset($dataPost['SortColumn']) ? $dataPost['SortColumn'] : "";
			$SortOrder = isset($dataPost['SortOrder']) ? $dataPost['SortOrder'] : "asc";
			$idSession = isset($dataPost['idSession']) ? $dataPost['idSession'] : "";
			$dataModel = isset($dataPost['mSearch']) ? $dataPost['mSearch'] : "";

			$offset = ($PageIndex - 1) * $PageSize;

			$result['status'] = true;
			$result['message'] = $this->getDashboardByGoodsNameList($dataModel,$idSession);
			$result['totalRecords'] = $this->getTotal($dataModel,$idSession);
			$result['toTalPage'] = ceil($result['totalRecords'] / $PageSize);

		} catch (Exception $ex) {
			$result['status'] = false;
			$result['message'] = "exception: " . $ex;
		}

		return  $result;
	}
	public function getDashboardByGoodsNameListRefund($dataPost,$idSession){
		$this->load->model('UserModel', '', TRUE);
		if($idSession != ""){
			$idsession = $idSession; 
		}else{
			$idsession = $this->session->userdata('id');
		}
		$userPosition = $this->UserModel->getPostion($idsession);
		$sql = "SELECT date(ts_transection.TimeStamp)as Date,ma_goods.NAME As GOODS_NAME,ma_goods_category.NAME,count(ts_transection_detail.GOODS_ID) AS TotalGoods,SUM(ts_transection_detail.PRICE) AS TotalGoodsPrice,SUM(ts_transection_detail.COST) AS TotalCOST,ts_transection.*,ts_transection_detail.* FROM ts_transection
		RIGHT JOIN ts_transection_detail ON ts_transection.ID = ts_transection_detail.TRANSECTION_ID
		LEFT JOIN ma_goods ON ts_transection_detail.GOODS_ID = ma_goods.ID
		LEFT JOIN ma_goods_category ON ma_goods.CATEGORY_ID = ma_goods_category.ID
		WHERE ts_transection.BUSINESS_ID = '".$userPosition['BUSINESS_ID']."' AND ts_transection.IsActive = 0
		AND date(ts_transection.TimeStamp) BETWEEN '".$dataPost['DateStart']."' AND '".$dataPost['DateEnd']."'
		GROUP BY ts_transection_detail.GOODS_ID
		ORDER BY Date DESC";
		
		$query = $this->db->query($sql);
		return  $query->result_array();
	}
	public function getTotalRefund($dataPost,$idSession ){
		$this->load->model('UserModel', '', TRUE);
		if($idSession != ""){
			$idsession = $idSession; 
		}else{
			$idsession = $this->session->userdata('id');
		}
		$userPosition = $this->UserModel->getPostion($idsession);
 
		$sql = "SELECT date(ts_transection.TimeStamp)as Date,ma_goods.NAME As GOODS_NAME,ma_goods_category.NAME,count(ts_transection_detail.GOODS_ID) AS TotalGoods,SUM(ts_transection_detail.PRICE) AS TotalGoodsPrice,SUM(ts_transection_detail.COST) AS TotalCOST,ts_transection.*,ts_transection_detail.* FROM ts_transection
		RIGHT JOIN ts_transection_detail ON ts_transection.ID = ts_transection_detail.TRANSECTION_ID
		LEFT JOIN ma_goods ON ts_transection_detail.GOODS_ID = ma_goods.ID
		LEFT JOIN ma_goods_category ON ma_goods.CATEGORY_ID = ma_goods_category.ID
		WHERE ts_transection.BUSINESS_ID = '".$userPosition['BUSINESS_ID']."' AND ts_transection.IsActive = 0
		AND date(ts_transection.TimeStamp) BETWEEN '".$dataPost['DateStart']."' AND '".$dataPost['DateEnd']."'
		GROUP BY ts_transection_detail.GOODS_ID
		ORDER BY Date DESC";
		
		$query = $this->db->query($sql);
		return  $query->num_rows() ;
	}
	public function getListRefund($dataPost)
	{

		try {
			// print_r($dataPost);die();
			$PageIndex =  isset($dataPost['PageIndex']) ? $dataPost['PageIndex'] : 1;
			$PageSize =  isset($dataPost['PageSize']) ? $dataPost['PageSize'] : 20;
			$direction =  isset($dataPost['SortColumn']) ? $dataPost['SortColumn'] : "";
			$SortOrder = isset($dataPost['SortOrder']) ? $dataPost['SortOrder'] : "asc";
			$idSession = isset($dataPost['idSession']) ? $dataPost['idSession'] : "";
			$dataModel = isset($dataPost['mSearch']) ? $dataPost['mSearch'] : "";

			$offset = ($PageIndex - 1) * $PageSize;

			$result['status'] = true;
			$result['message'] = $this->getDashboardByGoodsNameListRefund($dataModel,$idSession);
			$result['totalRecords'] = $this->getTotalRefund($dataModel,$idSession);
			$result['toTalPage'] = ceil($result['totalRecords'] / $PageSize);

		} catch (Exception $ex) {
			$result['status'] = false;
			$result['message'] = "exception: " . $ex;
		}

		return  $result;
	}
}
?>