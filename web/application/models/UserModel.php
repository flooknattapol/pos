<?php
use  setasign\Fpdi;

class UserModel extends CI_Model {
	
    private $tbl_name = 'ma_user';
	private $id = 'ID';
 
    public function __construct() {
        parent::__construct();
    }
	
	public function getUserNameById($id){
		$this->db->where($this->id, $id);
		return $this->db->get($this->tbl_name);
	}
	
	public function getPostion($idsession){
		$sql = "SELECT * FROM ma_user_position WHERE USER_ID = '".$idsession."'";
		return  $this->db->query($sql)->row_array();
	}
    public function validate($username, $password){
        // grab user input
       // $username = $this->security->xss_clean($this->input->post('uname'));
        //$password = $this->security->xss_clean($this->input->post('pswsss'));
        
        // Prep the query
        $this->db->where('EMAIL', $username);
        $this->db->where('PASSWORD', md5($password));
        
        // Run the query
        $query = $this->db->get( $this->tbl_name );
        // Let's check if there are any results
        if($query->num_rows() == 1)
        {
			
			// If there is a user, then create session data
			
			$row = $query->row();
			$sql = "SELECT ma_user_position.*,ma_business.ID as BUSINESS_ID,ma_business.NAME as BUSINESS_NAME,ma_business.IsActive as BUSINESS_IsActive
			,ma_business.TEL as BUSINESS_TEL ,ma_business.SETTING_BOOLEAN_POINT as SETTING_BOOLEAN_POINT,ma_business.EXPIRED_DATE as EXPIRED_DATE,ma_business.EXPIRED_FLAG as EXPIRED_FLAG
			FROM ma_user_position
			LEFT JOIN ma_business ON ma_user_position.BUSINESS_ID = ma_business.ID
			WHERE ma_user_position.USER_ID = '".$row->ID."'
			";
			$res = $this->db->query($sql)->row_array();
			$data = array(
						'id' => $row->ID,
						'user' => $row->NAME,
						'user_email' => $row->EMAIL, 
						'validated' => true,
						'site_lang' => "thai",
						'business_id' => $res['BUSINESS_ID'],
						'business_name' => $res['BUSINESS_NAME'],
						'business_isactive' => $res['BUSINESS_IsActive'],
						'business_setting_blpoint' => $res['SETTING_BOOLEAN_POINT'],
						'EXPIRED_DATE' => $res['EXPIRED_DATE'],
						'EXPIRED_FLAG' => $res['EXPIRED_FLAG'],
						'tel' => $res['BUSINESS_TEL']
						);
			if(date("Y-m-d") > $res['EXPIRED_DATE'] &&  $res['EXPIRED_FLAG'] == 1){
				$result['status'] = false;
				$result['statusexpired'] = false;
				$result['data'] = $data;
				$result['message'] = "ร้านค้าหมดอายุการใช้งาน";
			}else{
				$this->session->set_userdata($data);

				$result['status'] = true;
				$result['data'] = $data;
			}
            return $result;
        }else{
			//echo "count ". $query->num_rows(). "no found ".  md5($password). $username;
		}
        // If the previous process did not validate
		// then return false.
		$result['status'] = false;
		$result['message'] = "E-Mail หรือ รหัสผ่านไม่ถูกต้อง";

        return $result;
    }
	
	public function insert($modelData){
		 
	 	$this->db->insert($this->tbl_name, $modelData); 
		return $this->db->insert_id(); 
    }
     
    public function update($id, $modelData){
        $this->db->where($this->id, $id);
        return $this->db->update($this->tbl_name, $modelData);
    }
	
	public function getUserNameAllList(){
        //return $this->db->count_all($this->tbl_name);
        
        $this->db->select('id','name','contact','address1','tel','email','taxid','website');
		//$this->db->where('User_delete_flag', 0);
        $query =  $this->db->get($this->tbl_name);
		
		return $query->result_array();
    }
	
	public function getUserModel(){
        //return $this->db->count_all($this->tbl_name);
        
        //$this->db->select('id','name','contact','address1','address2','address3','tel','email','taxid','website');
		//$this->db->where('User_delete_flag', 0);
        $query =  $this->db->get($this->tbl_name);
		
		return $query->result_array();
    }
	
	public function getSearchQuery($sql, $dataModel){
		
		//print_r($dataModel);
		
		// if(isset($dataModel['book_no']) && $dataModel['book_no'] != ""){
		 	// $sql .= " and book_no like '%".$this->db->escape_str( $dataModel['book_no'])."%' ";
		// }
		
		// if(isset($dataModel['num_no']) && $dataModel['num_no'] != ""){
		 	// $sql .= " and num_no like '%".$this->db->escape_str( $dataModel['num_no'])."%' ";
		// }
		
		if(isset($dataModel['NAME']) && $dataModel['NAME'] != ""){
		 	$sql .= " and NAME like '%".$this->db->escape_str( $dataModel['NAME'])."%' ";
		}
		
		return $sql;
	}
	
	public function getTotal($dataModel ){
		
		$sql = "SELECT * FROM ". $this->tbl_name  ." WHERE IsActive >= 0  ";
				
		$sql =  $this->getSearchQuery($sql, $dataModel);
		
		$query = $this->db->query($sql);		 
		
		return  $query->num_rows() ;
	}
	
	public function getUserNameList($dataModel, $limit = 10, $offset = 0, $order = '', $direction = 'asc'){
		
		$sql = "SELECT * FROM ". $this->tbl_name. " WHERE IsActive >= 0";
		  
		// if($order != ""){
			// $this->db->order_by($order, $direction);
		// }else{
			// $this->db->order_by($this->id ,$direction); 
		// }
		
		$sql =  $this->getSearchQuery($sql, $dataModel);

		if($order != ""){
			$sql .= " ORDER BY ".$order." ".$direction;
		}else{
			$sql .= " ORDER BY ".$this->id." ".$direction;
		}
		
		$query = $this->db->query($sql);
		//$query = $this->db->query($sql, array( "%".$dataModel['User_name']."%"));// $dataModel);
		
		return  $query->result_array();
	}		
	
	public function deleteUsername($id){
		$result = false;
		try{
			$query = $this->getUserNameById($id);
			//$modelData;			
			foreach ($query->result() as $row)
			{
			   		
				$modelData = array( 
					//'update_date' => date("Y-m-d H:i:s"),
					//'update_user' => $this->session->userdata('user_name'),
					'IsActive' => 0 //$row->Project_delete_flag 
				); 
			}
			
			$this->db->where($this->id, $id);
        	return $this->db->update($this->tbl_name, $modelData);
			
		}catch(Exception $ex){
			return $result;
		}
    }
	public function undeleteUsername($id){
		$result = false;
		try{
			$query = $this->getUserNameById($id);
			//$modelData;			
			foreach ($query->result() as $row)
			{
			   		
				$modelData = array( 
					//'update_date' => date("Y-m-d H:i:s"),
					//'update_user' => $this->session->userdata('user_name'),
					'IsActive' => 1 //$row->Project_delete_flag 
				); 
			}
			
			$this->db->where($this->id, $id);
        	return $this->db->update($this->tbl_name, $modelData);
			
		}catch(Exception $ex){
			return $result;
		}
    }
	public function getUserComboList(){
		
		$sql = "SELECT id, 	name FROM ". $this->tbl_name . " WHERE IsActive = 1  ";
		$query = $this->db->query($sql);
		return  $query->result_array();
	}
	
	###controller####
	public function add($dataPost) {
		// $this->output->set_content_type('application/json');
		$nResult = 0;
		
	  	try{
			$data['ID'] =  isset($dataPost['ID'])?$dataPost['ID']: 0; 
			$data['EMAIL'] =  isset($dataPost['EMAIL'])?$dataPost['EMAIL']: "";
			$data['TEL'] =  isset($dataPost['TEL'])?$dataPost['TEL']: "";
			$data['NAME'] =  isset($dataPost['NAME'])?$dataPost['NAME']: "";
			$password =  isset($dataPost['PASSWORD'])?$dataPost['PASSWORD']: "123";
			$data['PASSWORD'] =  md5($password);

			

				if ($data['ID'] == 0) {  
					$sql = "SELECT * FROM ma_user WHERE  EMAIL = '".$data['EMAIL']."'";
					$numrows = $this->db->query($sql)->num_rows();
					if($numrows == 0){
					$nResult = $this->insert($data);
					}else{
						$result['status'] = false;
						$result['message'] = "E-Mail นี้ถูกใช้ไปแล้ว";
					}
				}
				else {  
					$nResult = $this->update($data['ID'], $data);
				}
				
				if($nResult > 0){ 
					$result['status'] = true;
					$result['message'] = $this->lang->line("savesuccess");
				}else{
					$result['status'] = false;
					$result['message'] = $this->lang->line("error");
				} 
				
			
    	}catch(Exception $ex){
    		$result['status'] = false;
			$result['message'] = "exception: ".$ex;
    	}
	    
		return $result;	
    }
	public function changepassword($dataPost) {
		// $this->output->set_content_type('application/json');
		$nResult = 0;
		
	  	try{
	  		
	  		$data['ID'] =  isset($dataPost['ID'])?$dataPost['ID']: 0; 
			$data['PASSWORD']  =  isset($dataPost['PASSWORD'])?$dataPost['PASSWORD']: "";
			$OldPass =  md5(isset($dataPost['OldPass'])?$dataPost['OldPass']: "");
			$NewPass =  md5(isset($dataPost['NewPass'])?$dataPost['NewPass']: "");


			// echo $OldPass. " // ". $data['PASSWORD'];die();
			if($OldPass == $data['PASSWORD']){
				if ($data['ID'] > 0) {  
					$data['PASSWORD'] = $NewPass;
					$nResult = $this->update($data['ID'], $data);
				}
				
				if($nResult > 0){ 
					$result['status'] = true;
					$result['message'] = $this->lang->line("savesuccess");
				}else{
					$result['status'] = false;
					$result['message'] = $this->lang->line("error");
				} 
			}else{
				$result['status'] = false;
				$result['message'] = "รหัสผ่านไม่ถูกต้อง";
			}
    		
			
    	}catch(Exception $ex){
    		$result['status'] = false;
			$result['message'] = "exception: ".$ex;
    	}
	    
		return $result;	
    }
	public function delete($dataPost){
		try{

			$id =  isset($dataPost['ID'])?$dataPost['ID']:0;// $this->input->post('ap_id');
			
			$bResult = $this->deleteusername($id);
			 
			if($bResult){
				$result['status'] = true;
				$result['message'] = $this->lang->line("savesuccess");
			}else{
				$result['status'] = false;
				$result['message'] = $this->lang->line("error_faliure");
			}
			
		}catch(Exception $ex){
			$result['status'] = false;
			$result['message'] = "exception: ".$ex;
		}
		
		return $result;	
	}
	public function undelete($dataPost){
		try{

			$id =  isset($dataPost['ID'])?$dataPost['ID']:0;// $this->input->post('ap_id');
			
			$bResult = $this->undeleteusername($id);
			 
			if($bResult){
				$result['status'] = true;
				$result['message'] = $this->lang->line("savesuccess");
			}else{
				$result['status'] = false;
				$result['message'] = $this->lang->line("error_faliure");
			}
			
		}catch(Exception $ex){
			$result['status'] = false;
			$result['message'] = "exception: ".$ex;
		}
		
		return $result;	
	}
	public function getList($dataPost){
	 
		try{
			
	  		$PageIndex =  isset($dataPost['PageIndex'])?$dataPost['PageIndex']: 1;
			$PageSize =  isset($dataPost['PageSize'])?$dataPost['PageSize']: 20;
			$direction =  isset($dataPost['SortColumn'])?$dataPost['SortColumn']: "";
			$SortOrder = isset($dataPost['SortOrder'])?$dataPost['SortOrder']: "asc";
			$dataModel = isset($dataPost['mSearch'])?$dataPost['mSearch']: "";

			$offset = ($PageIndex - 1) * $PageSize;
			 
			$result['status'] = true;
			$result['message'] = $this->getuserNameList($dataModel , $PageSize, $offset, $direction, $SortOrder );
			$result['totalRecords'] = $this->getTotal($dataModel);
			$result['Session'] = $this->session->userdata('user');
			$result['toTalPage'] = ceil( $result['totalRecords'] / $PageSize);
			
			//$result['message'] = $this->userModel->getuserModel(); 
			 
		}catch(Exception $ex){
			$result['status'] = false;
			$result['message'] = "exception: ".$ex;
		}
		
		return $result;	
	}
	
	public function getComboList(){
	 
		try{ 
			$result['status'] = true;
			$result['message'] = $this->getuserComboList();
		}catch(Exception $ex){
			$result['status'] = false;
			$result['message'] = "exception: ".$ex;
		}
		
		return $result;	
	}
	
	public function printPDF(){
		//$this->pdi();
		
		define('FPDF_FONTPATH',APPPATH .'fpdf/font/');
		require(APPPATH .'fpdf/fpdf.php'); 
		require_once(APPPATH .'fpdi/autoload.php');
		 
		
		try {
			$this->load->model('userModel','',TRUE); 
			
			$id = isset($_GET['id'])?$_GET['id']: 0;
			   
			$query = $this->userModel->getuserNameById($id);			
			$userDatas = $query->result_array();
			$userData = $userDatas[0];
			
			//print_r($customerDetail);
			
			$filename = $_SERVER["DOCUMENT_ROOT"]. '/materia/user_Template.pdf';
			$pdf_name = $userData['num_no'].".pdf"; 
			$pdf = new Fpdi\FPDI('p','mm','A4');			
			$pdf -> AddPage(); 

			$pdf->setSourceFile($filename); 
			$tplIdx = $pdf->importPage(1);
			// use the imported page and place it at point 10,10 with a width of 100 mm
			$pdf->useTemplate($tplIdx, 1, 1, 210);
			// now write some text above the imported page
			
			$pdf->AddFont('AngsanaNew','','angsa.php');
			$pdf->AddFont('AngsanaNew','B','angsab.php');
			$pdf->AddFont('AngsanaNew','I','angsai.php');
			$pdf->SetFont('AngsanaNew','',12);
			
			//$pdf->SetFont('Arial');
			$pdf->SetTextColor(0,0,0);
			
			  
			$sign_date = new DateTime($userData['sign_date']);
			
			$tab1 = 18;
			$tab2 = 20;
			$tab3 = 30;
			$tab3Ex = 70;
			$tab4 = 120;
			$tab5 = 158;
			$tab6 = 182;
			$taxid1 = 133;
			$taxid2 = 140;
			$taxid3 = 144;
			$taxid4 = 148;
			$taxid5 = 152;
			$taxid6 = 160;
			$taxid7 = 164;
			$taxid8 = 168;
			$taxid9 = 172;
			$taxid10 = 176;
			$taxid11 = 183;
			$taxid12 = 187;
			$taxid13 = 194;
			$tabEnd = 188;
			$tabSocial = 130;
			$tabFund = 180;
			$lineStart = 20;
			$lineBr = 6;
			
			//...
			//บรรทัด 1
			$pdf->SetXY($tabEnd, $lineStart );
			$pdf->Write(0, iconv( 'UTF-8','cp874' ,  $userData['book_no']));
			$lineStart += $lineBr;
			
			//บรรทัด 2
			$pdf->SetXY($tabEnd, $lineStart );
			$pdf->Write(0, iconv( 'UTF-8','cp874' ,  $userData['num_no']));
			$lineStart += $lineBr;
			
			//บรรทัด 3
			$pdf->SetXY($taxid1, $lineStart );
			$pdf->Write(0, iconv( 'UTF-8','cp874' ,  $userData['pay_id'][0]));
			$pdf->SetXY($taxid2, $lineStart );
			$pdf->Write(0, iconv( 'UTF-8','cp874' ,  $userData['pay_id'][1]));
			$pdf->SetXY($taxid3, $lineStart );
			$pdf->Write(0, iconv( 'UTF-8','cp874' ,  $userData['pay_id'][2]));
			$pdf->SetXY($taxid4, $lineStart );
			$pdf->Write(0, iconv( 'UTF-8','cp874' ,  $userData['pay_id'][3]));
			$pdf->SetXY($taxid5, $lineStart );
			$pdf->Write(0, iconv( 'UTF-8','cp874' ,  $userData['pay_id'][4]));
			$pdf->SetXY($taxid6, $lineStart );
			$pdf->Write(0, iconv( 'UTF-8','cp874' ,  $userData['pay_id'][5]));
			$pdf->SetXY($taxid7, $lineStart );
			$pdf->Write(0, iconv( 'UTF-8','cp874' ,  $userData['pay_id'][6]));
			$pdf->SetXY($taxid8, $lineStart );
			$pdf->Write(0, iconv( 'UTF-8','cp874' ,  $userData['pay_id'][7]));
			$pdf->SetXY($taxid9, $lineStart );
			$pdf->Write(0, iconv( 'UTF-8','cp874' ,  $userData['pay_id'][8]));
			$pdf->SetXY($taxid10, $lineStart );
			$pdf->Write(0, iconv( 'UTF-8','cp874' ,  $userData['pay_id'][9]));
			$pdf->SetXY($taxid11, $lineStart );
			$pdf->Write(0, iconv( 'UTF-8','cp874' ,  $userData['pay_id'][10]));
			$pdf->SetXY($taxid12, $lineStart );
			$pdf->Write(0, iconv( 'UTF-8','cp874' ,  $userData['pay_id'][11]));
			$pdf->SetXY($taxid13, $lineStart );
			$pdf->Write(0, iconv( 'UTF-8','cp874' ,  $userData['pay_id'][12]));
			$lineStart += $lineBr;
			
			//บรรทัด 4
			$pdf->SetXY($tab3, $lineStart );
			$pdf->Write(0, iconv( 'UTF-8','cp874' ,  $userData['pay_name']));
			$lineStart += $lineBr + 3;
			
			//บรรทัด 5
			$pdf->SetXY($tab3, $lineStart );
			$pdf->Write(0, iconv( 'UTF-8','cp874' ,  $userData['pay_address']));
			$lineStart += $lineBr + 4;
			 
			//บรรทัด 6
			$pdf->SetXY($taxid1, $lineStart );
			$pdf->Write(0, iconv( 'UTF-8','cp874' ,  $userData['com_id'][0]));
			$pdf->SetXY($taxid2, $lineStart );
			$pdf->Write(0, iconv( 'UTF-8','cp874' ,  $userData['com_id'][1]));
			$pdf->SetXY($taxid3, $lineStart );
			$pdf->Write(0, iconv( 'UTF-8','cp874' ,  $userData['com_id'][2]));
			$pdf->SetXY($taxid4, $lineStart );
			$pdf->Write(0, iconv( 'UTF-8','cp874' ,  $userData['com_id'][3]));
			$pdf->SetXY($taxid5, $lineStart );
			$pdf->Write(0, iconv( 'UTF-8','cp874' ,  $userData['com_id'][4]));
			$pdf->SetXY($taxid6, $lineStart );
			$pdf->Write(0, iconv( 'UTF-8','cp874' ,  $userData['com_id'][5]));
			$pdf->SetXY($taxid7, $lineStart );
			$pdf->Write(0, iconv( 'UTF-8','cp874' ,  $userData['com_id'][6]));
			$pdf->SetXY($taxid8, $lineStart );
			$pdf->Write(0, iconv( 'UTF-8','cp874' ,  $userData['com_id'][7]));
			$pdf->SetXY($taxid9, $lineStart );
			$pdf->Write(0, iconv( 'UTF-8','cp874' ,  $userData['com_id'][8]));
			$pdf->SetXY($taxid10, $lineStart );
			$pdf->Write(0, iconv( 'UTF-8','cp874' ,  $userData['com_id'][9]));
			$pdf->SetXY($taxid11, $lineStart );
			$pdf->Write(0, iconv( 'UTF-8','cp874' ,  $userData['com_id'][10]));
			$pdf->SetXY($taxid12, $lineStart );
			$pdf->Write(0, iconv( 'UTF-8','cp874' ,  $userData['com_id'][11]));
			$pdf->SetXY($taxid13, $lineStart );
			$pdf->Write(0, iconv( 'UTF-8','cp874' ,  $userData['com_id'][12]));
			$lineStart += $lineBr + 1;
			
			//บรรทัด 7
			$pdf->SetXY($tab3, $lineStart );
			$pdf->Write(0, iconv( 'UTF-8','cp874' ,  $userData['com_name']));
			$lineStart += $lineBr + 3;
			
			//บรรทัด 8
			$pdf->SetXY($tab3, $lineStart );
			$pdf->Write(0, iconv( 'UTF-8','cp874' ,  $userData['com_address']));
			$lineStart += (5*$lineBr) + 4;
			
			//บรรทัด 9
			$pdf->SetXY($tab4, $lineStart );
			$pdf->Cell(10,0,$userData['salary_year'],0,0,'R');
			$pdf->SetXY($tab5, $lineStart );
			$pdf->Cell(10,0,$userData['salary_pay'],0,0,'R'); 
			$pdf->SetXY($tab6, $lineStart );
			$pdf->Cell(10,0,$userData['salary_wht'],0,0,'R'); 
			$lineStart += $lineBr ;
			
			//บรรทัด 10
			$pdf->SetXY($tab4, $lineStart );
			$pdf->Cell(10,0,$userData['fee_year'],0,0,'R');
			$pdf->SetXY($tab5, $lineStart );
			$pdf->Cell(10,0,$userData['fee_pay'],0,0,'R'); 
			$pdf->SetXY($tab6, $lineStart );
			$pdf->Cell(10,0,$userData['fee_wht'],0,0,'R'); 
			$lineStart += $lineBr -1;
			
			//บรรทัด 11
			$pdf->SetXY($tab4, $lineStart );
			$pdf->Cell(10,0,$userData['copyfee_year'],0,0,'R');
			$pdf->SetXY($tab5, $lineStart );
			$pdf->Cell(10,0,$userData['copyfee_pay'],0,0,'R'); 
			$pdf->SetXY($tab6, $lineStart );
			$pdf->Cell(10,0,$userData['copyfee_wht'],0,0,'R'); 
			$lineStart += $lineBr -1 ;
			
			//บรรทัด 12
			$pdf->SetXY($tab4, $lineStart );
			$pdf->Cell(10,0,$userData['interest_year'],0,0,'R');
			$pdf->SetXY($tab5, $lineStart );
			$pdf->Cell(10,0,$userData['interest_pay'],0,0,'R'); 
			$pdf->SetXY($tab6, $lineStart );
			$pdf->Cell(10,0,$userData['interest_wht'],0,0,'R'); 
			$lineStart += (3*$lineBr) ;
			
			//บรรทัด 13
			$pdf->SetXY($tab4, $lineStart );
			$pdf->Cell(10,0,$userData['interest1_year'],0,0,'R');
			$pdf->SetXY($tab5, $lineStart );
			$pdf->Cell(10,0,$userData['interest1_pay'],0,0,'R'); 
			$pdf->SetXY($tab6, $lineStart );
			$pdf->Cell(10,0,$userData['interest1_wht'],0,0,'R'); 
			$lineStart += $lineBr ;
			
			//บรรทัด 14
			$pdf->SetXY($tab4, $lineStart );
			$pdf->Cell(10,0,$userData['interest2_year'],0,0,'R');
			$pdf->SetXY($tab5, $lineStart );
			$pdf->Cell(10,0,$userData['interest2_pay'],0,0,'R'); 
			$pdf->SetXY($tab6, $lineStart );
			$pdf->Cell(10,0,$userData['interest2_wht'],0,0,'R'); 
			$lineStart += $lineBr ;
			
			//บรรทัด 15
			$pdf->SetXY($tab4, $lineStart );
			$pdf->Cell(10,0,$userData['interest3_year'],0,0,'R');
			$pdf->SetXY($tab5, $lineStart );
			$pdf->Cell(10,0,$userData['interest3_pay'],0,0,'R'); 
			$pdf->SetXY($tab6, $lineStart );
			$pdf->Cell(10,0,$userData['interest3_wht'],0,0,'R'); 
			$lineStart += $lineBr ;
			
			//บรรทัด 16
			$pdf->SetXY($tab4, $lineStart );
			$pdf->Cell(10,0,$userData['interest4_year'],0,0,'R');
			$pdf->SetXY($tab5, $lineStart );
			$pdf->Cell(10,0,$userData['interest4_pay'],0,0,'R'); 
			$pdf->SetXY($tab6, $lineStart );
			$pdf->Cell(10,0,$userData['interest4_wht'],0,0,'R'); 
			$lineStart += $lineBr + 4;
			 
			//บรรทัด 17
			$pdf->SetXY($tab4, $lineStart );
			$pdf->Cell(10,0,$userData['interest21_year'],0,0,'R');
			$pdf->SetXY($tab5, $lineStart );
			$pdf->Cell(10,0,$userData['interest21_pay'],0,0,'R'); 
			$pdf->SetXY($tab6, $lineStart );
			$pdf->Cell(10,0,$userData['interest21_wht'],0,0,'R'); 
			$lineStart += $lineBr + 4;
			
			//บรรทัด 18
			$pdf->SetXY($tab4, $lineStart );
			$pdf->Cell(10,0,$userData['interest22_year'],0,0,'R');
			$pdf->SetXY($tab5, $lineStart );
			$pdf->Cell(10,0,$userData['interest22_pay'],0,0,'R'); 
			$pdf->SetXY($tab6, $lineStart );
			$pdf->Cell(10,0,$userData['interest22_wht'],0,0,'R'); 
			$lineStart += $lineBr + 4 ;
			
			//บรรทัด 19
			$pdf->SetXY($tab4, $lineStart );
			$pdf->Cell(10,0,$userData['interest23_year'],0,0,'R');
			$pdf->SetXY($tab5, $lineStart );
			$pdf->Cell(10,0,$userData['interest23_pay'],0,0,'R'); 
			$pdf->SetXY($tab6, $lineStart );
			$pdf->Cell(10,0,$userData['interest23_wht'],0,0,'R'); 
			$lineStart += $lineBr;
			
			//บรรทัด 20
			$pdf->SetXY($tab4, $lineStart );
			$pdf->Cell(10,0,$userData['interest24_year'],0,0,'R');
			$pdf->SetXY($tab5, $lineStart );
			$pdf->Cell(10,0,$userData['interest24_pay'],0,0,'R'); 
			$pdf->SetXY($tab6, $lineStart );
			$pdf->Cell(10,0,$userData['interest24_wht'],0,0,'R'); 
			$lineStart += $lineBr - 1;
			
			//บรรทัด 21
			$pdf->SetXY($tab3Ex, $lineStart ); 
			$pdf->Write(0, iconv( 'UTF-8','cp874' ,  $userData['interest25_comment'])); 
			$pdf->SetXY($tab4, $lineStart );
			$pdf->Cell(10,0,$userData['interest25_year'],0,0,'R');
			$pdf->SetXY($tab5, $lineStart );
			$pdf->Cell(10,0,$userData['interest25_pay'],0,0,'R'); 
			$pdf->SetXY($tab6, $lineStart );
			$pdf->Cell(10,0,$userData['interest25_wht'],0,0,'R'); 
			$lineStart += (3*$lineBr) ;
			
			//บรรทัด 22
			$pdf->SetXY($tab4, $lineStart );
			$pdf->Cell(10,0,$userData['revenue_year'],0,0,'R');
			$pdf->SetXY($tab5, $lineStart );
			$pdf->Cell(10,0,$userData['revenue_pay'],0,0,'R'); 
			$pdf->SetXY($tab6, $lineStart );
			$pdf->Cell(10,0,$userData['revenue_wht'],0,0,'R'); 
			$lineStart += $lineBr + 1 ;
			
			//บรรทัด 23
			$pdf->SetXY($tab3Ex, $lineStart ); 
			$pdf->Write(0, iconv( 'UTF-8','cp874' ,  $userData['other_comment'])); 
			$pdf->SetXY($tab4, $lineStart );
			$pdf->Cell(10,0,$userData['other_year'],0,0,'R');
			$pdf->SetXY($tab5, $lineStart );
			$pdf->Cell(10,0,$userData['other_pay'],0,0,'R'); 
			$pdf->SetXY($tab6, $lineStart );
			$pdf->Cell(10,0,$userData['other_wht'],0,0,'R'); 
			$lineStart += $lineBr + 2;
			
			//บรรทัด 24 
			$pdf->SetXY($tab5, $lineStart );
			$pdf->Cell(10,0,$userData['total_pay'],0,0,'R'); 
			$pdf->SetXY($tab6, $lineStart );
			$pdf->Cell(10,0,$userData['total_wht'],0,0,'R'); 
			$lineStart += $lineBr ;
			 
			//บรรทัด 25
			$pdf->SetXY($tab3Ex, $lineStart );
			$pdf->Write(0, iconv( 'UTF-8','cp874' ,  $userData['total_alphabet'])); 
			$lineStart += $lineBr ;
			
			//บรรทัด 26
			$pdf->SetXY($tabSocial, $lineStart );
			$pdf->Write(0, iconv( 'UTF-8','cp874' , '0000')); 
			$pdf->SetXY($tabFund, $lineStart );
			$pdf->Write(0, iconv( 'UTF-8','cp874' ,  '0000')); 
			$lineStart += (3*$lineBr) + 2;
			
			//บรรทัด 27
			$pdf->SetXY($tabSocial, $lineStart );
			$pdf->Write(0, iconv( 'UTF-8','cp874' ,  $userData['sign_name'])); 
			$lineStart += $lineBr - 1;
			
			$pdf->SetXY($tabSocial - 5, $lineStart );
			$pdf->Write(0, iconv( 'UTF-8','cp874' ,   $sign_date->format('d'))); 
			$pdf->SetXY($tabSocial + 5, $lineStart );
			$pdf->Write(0, iconv( 'UTF-8','cp874' ,   $sign_date->format('m'))); 
			$pdf->SetXY($tabSocial + 22, $lineStart );
			$pdf->Write(0, iconv( 'UTF-8','cp874' ,   $sign_date->format('Y'))); 
			$lineStart += $lineBr + 2;
			
			// Output
			$pdf->Output($_SERVER["DOCUMENT_ROOT"].'/application/uploads/'. $pdf_name, 'I'); //D = download // I , F , S
			
		} catch (Exception $e) {
			echo 'Caught exception: ',  $e->getMessage(), "\n";
		}
	}
	public function ForgetPass($dataPost)
	{
		try {

			$EMAIL =  isset($dataPost['EMAIL']) ? $dataPost['EMAIL'] : ""; 

			$sql = "SELECT * FROM ma_user
			WHERE EMAIL = '".$EMAIL."'
			";

			$alphabet = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890';
			$pass = array(); //remember to declare $pass as an array
			$alphaLength = strlen($alphabet) - 1; //put the length -1 in cache
			for ($i = 0; $i < 8; $i++) {
				$n = rand(0, $alphaLength);
				$pass[] = $alphabet[$n];
			}
			$newPass = implode($pass);
			$newPassMd5 = md5($newPass);

			$Ac = $this->db->query($sql)->row_array();
			$data['PASSWORD'] = $newPassMd5;
			// echo $newPassMd5;
			// echo $newPass;die();
			
			if ($Ac != "") {
				$config = array(
					'protocol'  => 'smtp',
					'smtp_host' => 'mail.devdeethailand.com',
					'smtp_port' => '25',
					'smtp_user' => 'support@devdeethailand.com',
					'smtp_pass' => '1234',
					'mailtype'  => 'html', 
					'charset'   => 'utf-8',
					// 'smtp_crypto'   => 'tls'
				);
				$this->load->library('email',$config);

				$this->email->set_newline("\r\n");
				$this->email->from('support@devdeethailand.com', 'D - POS');
				$subject = 'Forgot password';
				// echo $link_detail,$subject,$email,$user_id;
				$this->email->to($EMAIL);
				$this->email->subject($subject);
				$body = "Password new : ".$newPass ;
				$this->email->message($body);
				// $this->email->send();
				if (!$this->email->send()) {
					echo($this->email->print_debugger());
					$result['status']=false;
				} else {
					$result['status']=true;
					$result['message'] = "ส่งรหัสผ่านเรียบร้อยแล้ว";
					$nResult = $this->update($Ac['ID'],$data);
				}

			} else {
				$result['status'] = false;
				$result['message'] = "อีเมลล์ไม่ถูกต้อง";
			}
		} catch (Exception $ex) {
			$result['status'] = false;
			$result['message'] = "exception: " . $ex;
		}

		return $result;
	}
	public function ChangePass($dataPost)
	{
		try {

			$EMAIL =  isset($dataPost['EMAIL']) ? $dataPost['EMAIL'] : ""; 
			$PASSWORD =  isset($dataPost['PASSWORD']) ? $dataPost['PASSWORD'] : ""; 
			$NEWPASSWORD =  isset($dataPost['NEWPASSWORD']) ? $dataPost['NEWPASSWORD'] : ""; 

			$sql = "SELECT * FROM ma_user
			WHERE EMAIL = '".$EMAIL."' AND PASSWORD = '".md5($PASSWORD)."'
			";

			$Ac = $this->db->query($sql)->row_array();
			if($Ac != ""){
				$data['PASSWORD'] = md5($NEWPASSWORD);
				$nResult = $this->update($Ac['ID'],$data);
				$result['status']=true;
				$result['message'] = "เปลี่ยนรหัสผ่านเรียบร้อยแล้ว";
				
			}else{
				$result['status'] = false;
				$result['message'] = "Email หรือ รหัสผ่านไม่ถูกต้อง";
			}

		} catch (Exception $ex) {
			$result['status'] = false;
			$result['message'] = "exception: " . $ex;
		}

		return $result;
	}
}
?>