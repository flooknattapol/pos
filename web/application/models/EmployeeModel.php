<?php
  
class EmployeeModel extends CI_Model {
	
    private $tbl_name = 'ma_user_position';
	private $id = 'ID';
 
    public function __construct() {
        parent::__construct();
    }
	
	public function getEmployeeNameById($id){
		$this->db->where($this->id, $id);
		return $this->db->get($this->tbl_name);
	}
	
	public function insert($modelData){
		 
	 	$this->db->insert($this->tbl_name, $modelData); 
		return $this->db->insert_id(); 
    }
     
    public function update($id, $modelData){
        $this->db->where($this->id, $id);
        return $this->db->update($this->tbl_name, $modelData);
    }
	
	public function getEmployeeNameAllList(){
        //return $this->db->count_all($this->tbl_name);
        
        $this->db->select('ID','NAME','SKU','PRICE','UNIT');
		$this->db->where('IsActive', 1);
        $query =  $this->db->get($this->tbl_name);
		
		return $query->result_array();
    }
	
	public function getEmployeeModel($id){
        //return $this->db->count_all($this->tbl_name);
        
        //$this->db->select('id','name','contact','address1','address2','address3','tel','email','taxid','website');
		$this->db->where('IsActive', 1);
		$this->db->where($this->id, $id);
        $query =  $this->db->get($this->tbl_name);
		
		return $query->result_array();
    }
	
	public function getSearchQuery($sql, $dataModel){

		
		if(isset($dataModel['NAME']) && $dataModel['NAME'] != ""){
			$sql .= " and ma_user.NAME like '%".$this->db->escape_str( $dataModel['NAME'])."%' ";
	   	}
	   	

		return $sql;
	}
	
	public function getTotal($dataModel,$idSession ){
		$this->load->model('UserModel', '', TRUE);

		if($idSession != ""){
			$idsession = $idSession; 
		}else{
			$idsession = $this->session->userdata('id');
		}
		$userPosition = $this->UserModel->getPostion($idsession);

		// print_r($userPosition);die();
		$sql = "SELECT ma_user_position.*,ma_user.*,ma_user_position.ID as ID_POSITION,ma_role.NAME AS ROLE_NAME
		FROM ma_user_position
		LEFT JOIN ma_user ON ma_user_position.USER_ID = ma_user.ID
		LEFT JOIN ma_role ON ma_user_position.ROLE_ID = ma_role.ID
		WHERE ma_user_position.IsActive = 1 AND ma_user_position.BUSINESS_ID = '".$userPosition['BUSINESS_ID']."' 
		"; 
				
		$sql =  $this->getSearchQuery($sql, $dataModel);
		
		$query = $this->db->query($sql);		 
		
		return  $query->num_rows() ;
	}
	
	public function getEmployeeNameList($dataModel, $limit = 10, $offset = 0, $order = '', $direction = 'asc',$idSession){
		
		$this->load->model('UserModel', '', TRUE);

		if($idSession != ""){
			$idsession = $idSession; 
		}else{
			$idsession = $this->session->userdata('id');
		}
		$userPosition = $this->UserModel->getPostion($idsession);

		// print_r($userPosition);die();
		$sql = "SELECT ma_user_position.*,ma_user.*,ma_user_position.ID as ID_POSITION,ma_role.NAME AS ROLE_NAME,ma_branch.NAME as BARNCH_NAME
		FROM ma_user_position
		LEFT JOIN ma_user ON ma_user_position.USER_ID = ma_user.ID
		LEFT JOIN ma_role ON ma_user_position.ROLE_ID = ma_role.ID
		LEFT JOIN ma_branch ON ma_user_position.BRANCH_ID = ma_branch.ID
		WHERE ma_user_position.IsActive = 1 AND ma_user_position.BUSINESS_ID = '".$userPosition['BUSINESS_ID']."' 
		"; 
		$sql =  $this->getSearchQuery($sql, $dataModel);	

		if($order != ""){
			$sql .= " ORDER BY ma_user.".$order." ".$direction;
		}else{
			$sql .= " ORDER BY ma_user_position.".$this->id." ".$direction;
		}
		
		$sql .= " LIMIT $offset, $limit";
		
		// echo $sql;die();
		
		 
		$query = $this->db->query($sql);
		return  $query->result_array();
	}
	
	public function deleteEmployeename($id){
		// echo $id; die();
		$result = false;
		try{
			$query = $this->getEmployeeNameById($id);
			// echo $id;die();
			// $modelData;			
			foreach ($query->result() as $row)
			{
			   		
				$modelData = array( 
					//'update_date' => date("Y-m-d H:i:s"),
					//'update_user' => $this->session->userdata('user_name'),
					// 'Edit'=>$this->session->userdata('user'),
					'IsActive' => 0 //$row->Employee_IsActive 
				); 
			}
			// echo $this->tbl_name;
			// print_r($modelData);die();
			$this->db->set('IsActive',0);
			$this->db->where($this->id, $id);
        	return $this->db->update($this->tbl_name);
			//return $this->update($id, $modelData);
			//$sql = "Delete FROM ". $this->tbl_name; 
			//return  $this->db->query($sql);
			
		}catch(Exception $ex){
			return $result;
		}
	}
	public function getcategoryEmployeeComboList(){
		
		$sql = "SELECT * FROM  ma_Employee
		WHERE IsActive = 1  "; 
		
		$query = $this->db->query($sql);
		return  $query->result_array();
	}
	public function GetEmployeeByMember($MEMBER_ID){
		$sql = "SELECT ma_Employee.* FROM ma_Employee
		WHERE ma_Employee.MEMBER_ID = '".$MEMBER_ID."'
		";
		$query = $this->db->query($sql);
		return $query->result_array();
	}
	#### controller ###
	public function add($dataPost )
	{
		// $this->output->set_content_category('application/json');
		$nResult = 0;

		try {

			$this->load->model('UserModel', '', TRUE);
			$this->load->model('UserPositionModel', '', TRUE);

			$data['ID'] =  isset($dataPost['ID']) ? $dataPost['ID'] : 0;
			$data['NAME'] =  isset($dataPost['NAME']) ? $dataPost['NAME'] : "";
			$data['TEL'] =  isset($dataPost['TEL']) ? $dataPost['TEL'] : "";
			$data['EMAIL'] =  isset($dataPost['EMAIL']) ? $dataPost['EMAIL'] : "";
			$data['IMAGE'] =  isset($dataPost['IMAGE']) ? $dataPost['IMAGE'] : "";
			$data['PASSWORD'] = md5(123);
			$data['IsActive'] = 1;
			
			$idSession = isset($dataPost['idSession']) ? $dataPost['idSession'] : "";
			$this->load->model('UserModel', '', TRUE);
			if($idSession != ""){
				$idsession = $idSession; 
			}else{
				$idsession = $this->session->userdata('id');
			}
			$userPosition = $this->UserModel->getPostion($idsession);
			$dataPos['ROLE_ID']  = isset($dataPost['ROLE_ID']) ? $dataPost['ROLE_ID'] : "";
			$dataPos['ID']  = isset($dataPost['ID_POSITION']) ? $dataPost['ID_POSITION'] : "";
			$dataPos['BUSINESS_ID']  = $userPosition['BUSINESS_ID'];
			$dataPos['BRANCH_ID']  = isset($dataPost['BRANCH_ID']) ? $dataPost['BRANCH_ID'] : "";
			$dataPos['IsActive'] = 1;

		
			if ($data['ID'] == 0) {

				$sql = "SELECT * FROM ma_user WHERE EMAIL = '".$data['EMAIL']."'";
				$res = $this->db->query($sql)->num_rows();
				if($res > 0){
					$result['status'] = false;
					$result['message'] = "E-Mail : ".$data['EMAIL']." ถูกใช้งานไปแล้ว";
				}else{
					$nResult = $this->UserModel->insert($data);
					$dataPos['USER_ID'] = $nResult;
					$nResult = $this->UserPositionModel->insert($dataPos);
					if ($nResult > 0) {
						$result['status'] = true;
						$result['message'] = $this->lang->line("savesuccess");
					
					} else {
						$result['status'] = false;
						$result['message'] = $this->lang->line("error");
					}
				}
			} else {
				$nResult = $this->UserModel->update($data['ID'], $data);
				$nResult = $this->UserPositionModel->update($dataPos['ID'], $dataPos);
				if ($nResult > 0) {
					$result['status'] = true;
					$result['message'] = $this->lang->line("savesuccess");
				
				} else {
					$result['status'] = false;
					$result['message'] = $this->lang->line("error");
				}

			}
			
		} catch (Exception $ex) {
			$result['status'] = false;
			$result['message'] = "exception: " . $ex;
		}

		return  $result;
	}

	public function delete($dataPost )
	{
		try {
			$id =  isset($dataPost['ID']) ? $dataPost['ID'] : 0; // $this->input->post('ap_id');

			$bResult = $this->deleteEmployeename($id);

			if ($bResult) {
				$result['status'] = true;
				$result['message'] = $this->lang->line("savesuccess");
			} else {
				$result['status'] = false;
				$result['message'] = $this->lang->line("error_faliure");
			}
		} catch (Exception $ex) {
			$result['status'] = false;
			$result['message'] = "exception: " . $ex;
		}

		return  $result;
	}
	

	public function getList($dataPost )
	{

		try {

			$PageIndex =  isset($dataPost['PageIndex']) ? $dataPost['PageIndex'] : 1;
			$PageSize =  isset($dataPost['PageSize']) ? $dataPost['PageSize'] : 20;
			$direction =  isset($dataPost['SortColumn']) ? $dataPost['SortColumn'] : "";
			$SortOrder = isset($dataPost['SortOrder']) ? $dataPost['SortOrder'] : "asc";
			$dataModel = isset($dataPost['mSearch']) ? $dataPost['mSearch'] : "";
			$idSession = isset($dataPost['idSession']) ? $dataPost['idSession'] : "";

			$offset = ($PageIndex - 1) * $PageSize;

			$result['status'] = true;
			$result['message'] = $this->EmployeeModel->getEmployeeNameList($dataModel, $PageSize, $offset, $direction, $SortOrder,$idSession);
			$result['totalRecords'] = $this->EmployeeModel->getTotal($dataModel,$idSession);
			$result['toTalPage'] = ceil($result['totalRecords'] / $PageSize);

		} catch (Exception $ex) {
			$result['status'] = false;
			$result['message'] = "exception: " . $ex;
		}

		return  $result;
	}
}
?>