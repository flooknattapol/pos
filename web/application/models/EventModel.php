<?php
  
class EventModel extends CI_Model {
	
    private $tbl_name = 'ma_event';
	private $id = 'ID';
 
    public function __construct() {
        parent::__construct();
    }
	
	public function getEventNameById($id){
		$this->db->where($this->id, $id);
		return $this->db->get($this->tbl_name);
	}
	
	public function insert($modelData){
		 
	 	$this->db->insert($this->tbl_name, $modelData); 
		return $this->db->insert_id(); 
    }
     
    public function update($id, $modelData){
        $this->db->where($this->id, $id);
        return $this->db->update($this->tbl_name, $modelData);
    }
	
	public function getEventNameAllList(){
        //return $this->db->count_all($this->tbl_name);
        
        $this->db->select('ID','NAME','DESCRIPTION','LOCATION','WEBSITE');
		$this->db->where('IsActive', 1);
        $query =  $this->db->get($this->tbl_name);
		
		return $query->result_array();
    }
	public function addQrCode($nResult){

		$this->load->library('MyQrCode');
		$nResult['QRURL'] = $this->myqrcode->SaveQRCode("Type3_".$nResult['ID'] );
		return $nResult;
	}

	public function loadQrCode($arrayData){

		$arrayResult = array();

		foreach($arrayData as $key => $value){
			$arrayResult[] = $this->addQrCode($value);
		}

		// print_r($arrayResult);die();
		return $arrayResult;
	}
	public function getEventModel($id){
        //return $this->db->count_all($this->tbl_name);
        
        //$this->db->select('id','name','contact','address1','address2','address3','tel','email','taxid','website');
		$this->db->where('IsActive', 1);
		$this->db->where($this->id, $id);
        $query =  $this->db->get($this->tbl_name);
		
		return $query->result_array();
    }
	
	public function getSearchQuery($sql, $dataModel){
		
		//print_r($dataModel);
		// 	if(isset($dataModel['code']) && $dataModel['code'] != ""){
		// 		$sql .= " and emp_code like '%".$this->db->escape_str( $dataModel['code'])."%' ";
		//    }
		
		// 	if(isset($dataModel['code_id']) && $dataModel['code_id'] != ""){
		// 	 	$sql .= " and id_card_number like '%".$this->db->escape_str( $dataModel['code_id'])."%' ";
		// 	}
		
		if(isset($dataModel['NAME']) && $dataModel['NAME'] != ""){
		 	$sql .= " and NAME like '%".$this->db->escape_str( $dataModel['NAME'])."%' ";
		}
		
		// 	if(isset($dataModel['lastname']) && $dataModel['lastname'] != ""){
		// 	 	$sql .= " and lastname_th like '%".$this->db->escape_str( $dataModel['lastname'])."%' ";
		// 	}
			
		// 	if($dataModel['flag']==1){
		// 		$sql .= " and  if(end_date!=0,end_date,NOW()+1) > NOW() ";
		//    }
		// echo $sql;
		return $sql;
	}
	
	public function getTotal($dataModel ){
		
		$sql = "SELECT * FROM ". $this->tbl_name  ." WHERE IsActive = 1  ";
				
		$sql =  $this->getSearchQuery($sql, $dataModel);
		
		$query = $this->db->query($sql);		 
		
		return  $query->num_rows() ;
	}
	
	public function getEventNameList($dataModel, $limit = 10, $offset = 0, $order = '', $direction = 'asc'){
		
		$sql = "SELECT * FROM ". $this->tbl_name . " WHERE IsActive = 1  "; 
		
		$sql =  $this->getSearchQuery($sql, $dataModel);	
		
		 
		// if($order != ""){
			// $this->db->order_by($order, $direction);
		// }else{
			// $this->db->order_by($this->id ,$direction); 
		// }
		
		if($order != ""){
			$sql .= " ORDER BY ".$order." ".$direction;
		}else{
			$sql .= " ORDER BY ".$this->id." ".$direction;
		}
		
		$sql .= " LIMIT $offset, $limit";
		
		//print($sql );
		 
		$query = $this->db->query($sql);
		return  $query->result_array();
	}
	public function getEventNameListByQR($dataModel, $limit = 10, $offset = 0, $order = '', $direction = 'asc'){
		
		$data = $this->getEventNameList($dataModel, $limit, $offset, $order, $direction);
		return $this->loadQrCode($data);
	}
	public function getEventComboList(){
		
		$sql = "SELECT * FROM ". $this->tbl_name . " WHERE IsActive = 1  "; 
		$query = $this->db->query($sql);
		return  $query->result_array();
	}
	public function deleteEventname($id){
		$result = false;
		try{
			$query = $this->getEventNameById($id);		
			foreach ($query->result() as $row)
			{
			   		
				$modelData = array( 
					'Edit'=>$this->session->userdata('user'),
					'IsActive' => 0 //$row->Event_IsActive 
				); 
			}
			
			$this->db->where($this->id, $id);
        	return $this->db->update($this->tbl_name, $modelData);
		}catch(Exception $ex){
			return $result;
		}
    }
}
?>