<?php
  
class UserPositionModel extends CI_Model {
	
    private $tbl_name = 'ma_user_position';
	private $id = 'id';
 
    public function __construct() {
        parent::__construct();
    }
	
	public function getUserPositionNameById($id){
		$this->db->where($this->id, $id);
		return $this->db->get($this->tbl_name);
	}
	
	 
    public function validate($UserPositionname, $password){
        // grab UserPosition input
       // $UserPositionname = $this->security->xss_clean($this->input->post('uname'));
        //$password = $this->security->xss_clean($this->input->post('pswsss'));
        
        // Prep the query
        $this->db->where('EMAIL', $UserPositionname);
        $this->db->where('PASSWORD', md5($password));
        
        // Run the query
        $query = $this->db->get( $this->tbl_name );
        // Let's check if there are any results
        if($query->num_rows() == 1)
        {
            // If there is a UserPosition, then create session data
            $row = $query->row();
			//print_r($row);
            $data = array(
                    'id' => $row->ID,
                    'UserPosition' => $row->NAME, 
					'validated' => true,
					'site_lang' => "thai"
                    );
            $this->session->set_UserPositiondata($data);
            return true;
        }else{
			//echo "count ". $query->num_rows(). "no found ".  md5($password). $UserPositionname;
		}
        // If the previous process did not validate
        // then return false.
        return false;
    }
	
	public function insert($modelData){
		 
	 	$this->db->insert($this->tbl_name, $modelData); 
		return $this->db->insert_id(); 
    }
     
    public function update($id, $modelData){
        $this->db->where($this->id, $id);
        return $this->db->update($this->tbl_name, $modelData);
    }
	
	public function getUserPositionNameAllList(){
        //return $this->db->count_all($this->tbl_name);
        
        $this->db->select('id','name','contact','address1','tel','email','taxid','website');
		//$this->db->where('UserPosition_delete_flag', 0);
        $query =  $this->db->get($this->tbl_name);
		
		return $query->result_array();
    }
	
	public function getUserPositionModel(){
        //return $this->db->count_all($this->tbl_name);
        
        //$this->db->select('id','name','contact','address1','address2','address3','tel','email','taxid','website');
		//$this->db->where('UserPosition_delete_flag', 0);
        $query =  $this->db->get($this->tbl_name);
		
		return $query->result_array();
    }
	
	public function getSearchQuery($sql, $dataModel){
		
		//print_r($dataModel);
		
		// if(isset($dataModel['book_no']) && $dataModel['book_no'] != ""){
		 	// $sql .= " and book_no like '%".$this->db->escape_str( $dataModel['book_no'])."%' ";
		// }
		
		// if(isset($dataModel['num_no']) && $dataModel['num_no'] != ""){
		 	// $sql .= " and num_no like '%".$this->db->escape_str( $dataModel['num_no'])."%' ";
		// }
		
		if(isset($dataModel['UserPosition']) && $dataModel['UserPosition'] != ""){
		 	$sql .= " and UserPosition like '%".$this->db->escape_str( $dataModel['UserPosition'])."%' ";
		}
		
		return $sql;
	}
	
	public function getTotal($dataModel ){
		
		$sql = "SELECT * FROM ". $this->tbl_name  ." WHERE IsActive = 1  ";
				
		$sql =  $this->getSearchQuery($sql, $dataModel);
		
		$query = $this->db->query($sql);		 
		
		return  $query->num_rows() ;
	}
	
	public function getUserPositionNameList($dataModel, $limit = 10, $offset = 0, $order = '', $direction = 'asc'){
		
		$sql = "SELECT * FROM ". $this->tbl_name. " WHERE IsActive = 1";
		  
		// if($order != ""){
			// $this->db->order_by($order, $direction);
		// }else{
			// $this->db->order_by($this->id ,$direction); 
		// }
		
		$sql =  $this->getSearchQuery($sql, $dataModel);

		if($order != ""){
			$sql .= " ORDER BY ".$order." ".$direction;
		}else{
			$sql .= " ORDER BY ".$this->id." ".$direction;
		}
		
		$query = $this->db->query($sql);
		//$query = $this->db->query($sql, array( "%".$dataModel['UserPosition_name']."%"));// $dataModel);
		
		return  $query->result_array();
	}		
	
	public function deleteUserPositionname($id){
		$result = false;
		try{
			$query = $this->getUserPositionNameById($id);
			//$modelData;			
			foreach ($query->result() as $row)
			{
			   		
				$modelData = array( 
					//'update_date' => date("Y-m-d H:i:s"),
					//'update_UserPosition' => $this->session->UserPositiondata('UserPosition_name'),
					'IsActive' => 0 //$row->Project_delete_flag 
				); 
			}
			
			$this->db->where($this->id, $id);
        	return $this->db->update($this->tbl_name, $modelData);
			
		}catch(Exception $ex){
			return $result;
		}
    }
	
	public function getUserPositionComboList(){
		
		$sql = "SELECT id, 	name FROM ". $this->tbl_name . " WHERE IsActive = 1  ";
		$query = $this->db->query($sql);
		return  $query->result_array();
	}
	
	public function getUserPositionDetail($id){
		
		$sql = "SELECT ma_user_position.* FROM ma_user_position 
		WHERE ma_user_position.USER_ID = '".$id."'
		";
		$query = $this->db->query($sql);
		return $query->result_array();
	}
	#### controller####
	public function getDetail($dataPost)
	{
		try {
			$id =  isset($dataPost['ID']) ? $dataPost['ID'] : 0;
			$result['status'] = true;
			$result['message'] = $this->getUserPositionDetail($id);
		} catch (Exception $ex) {
			$result['status'] = false;
			$result['message'] = "exception: " . $ex;
		}

		return $result;	
	}
}
?>