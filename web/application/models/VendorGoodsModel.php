<?php
  
class VendorGoodsModel extends CI_Model {
	
    private $tbl_name = 'ts_vendor_sale';
	private $id = 'ID';
 
    public function __construct() {
        parent::__construct();
    }
	
	public function getVendorGoodsNameById($id){
		$this->db->where($this->id, $id);
		return $this->db->get($this->tbl_name);
	}
	
	public function insert($modelData){
		 
	 	$this->db->insert($this->tbl_name, $modelData); 
		return $this->db->insert_id(); 
    }
     
    public function update($id, $modelData){
        $this->db->where($this->id, $id);
        return $this->db->update($this->tbl_name, $modelData);
    }
	
	public function getVendorGoodsNameAllList(){
        //return $this->db->count_all($this->tbl_name);
        
        $this->db->select('ID','MEMBER_ID','EVENT_ID','VOUCHER','VENDOR_ID','GOODS_ID');
		$this->db->where('IsActive', 1);
        $query =  $this->db->get($this->tbl_name);
		
		return $query->result_array();
    }
	
	public function getVendorGoodsModel($id){
        //return $this->db->count_all($this->tbl_name);
        
        //$this->db->select('id','name','contact','address1','address2','address3','tel','email','taxid','website');
		$this->db->where('IsActive', 1);
		$this->db->where($this->id, $id);
        $query =  $this->db->get($this->tbl_name);
		
		return $query->result_array();
    }
	
	public function getSearchQuery($sql, $dataModel){
		
		//print_r($dataModel);
		// 	if(isset($dataModel['code']) && $dataModel['code'] != ""){
		// 		$sql .= " and emp_code like '%".$this->db->escape_str( $dataModel['code'])."%' ";
		//    }
		
		// 	if(isset($dataModel['code_id']) && $dataModel['code_id'] != ""){
		// 	 	$sql .= " and id_card_number like '%".$this->db->escape_str( $dataModel['code_id'])."%' ";
		// 	}
		
		if(isset($dataModel['Vendor_NAME']) && $dataModel['Vendor_NAME'] != ""){
		 	$sql .= " and t3.NAME like '%".$this->db->escape_str( $dataModel['Vendor_NAME'])."%' ";
		}
		if(isset($dataModel['Goods_NAME']) && $dataModel['Goods_NAME'] != ""){
			$sql .= " and ma_goods.NAME like '%".$this->db->escape_str( $dataModel['Goods_NAME'])."%' ";
	   	}
	   	if(isset($dataModel['Event_NAME']) && $dataModel['Event_NAME'] != ""){
			$sql .= " and ma_event.NAME like '%".$this->db->escape_str( $dataModel['Event_NAME'])."%' ";
		}
		// 	if(isset($dataModel['lastname']) && $dataModel['lastname'] != ""){
		// 	 	$sql .= " and lastname_th like '%".$this->db->escape_str( $dataModel['lastname'])."%' ";
		// 	}
			
		// 	if($dataModel['flag']==1){
		// 		$sql .= " and  if(end_date!=0,end_date,NOW()+1) > NOW() ";
		//    }
		// echo $sql;
		return $sql;
	}
	
	public function getTotal($dataModel ){
		
		$sql = "SELECT ts_vendor_sale.* ,t3.NAME As Vendor_NAME,ma_event.NAME as Event_NAME, ma_goods.NAME As Goods_NAME 
		,ma_goodstype.NAME as TYPE_GOODS
		,t3.VENDOR_NAME as VENDORSHOP
		FROM ts_vendor_sale
        LEFT JOIN ma_member as t3 ON ts_vendor_sale.VENDOR_ID = t3.ID 
		LEFT JOIN ma_event ON ts_vendor_sale.EVENT_ID = ma_event.ID
		LEFT JOIN ma_goods ON ts_vendor_sale.GOODS_ID = ma_goods.ID
		LEFT JOIN ma_goodstype ON ma_goods.TYPE = ma_goodstype.ID
		WHERE ts_vendor_sale.IsActive = 1 "; 
				
		$sql =  $this->getSearchQuery($sql, $dataModel);
		
		$query = $this->db->query($sql);		 
		
		return  $query->num_rows() ;
	}
	
	public function getVendorGoodsNameList($dataModel, $limit = 10, $offset = 0, $order = '', $direction = 'asc'){
		
		$sql = "SELECT ts_vendor_sale.* ,t3.NAME As Vendor_NAME,ma_event.NAME as Event_NAME, ma_goods.NAME As Goods_NAME 
		,ma_goodstype.NAME as TYPE_GOODS
		,t3.VENDOR_NAME as VENDORSHOP
		FROM ts_vendor_sale
        LEFT JOIN ma_member as t3 ON ts_vendor_sale.VENDOR_ID = t3.ID 
		LEFT JOIN ma_event ON ts_vendor_sale.EVENT_ID = ma_event.ID
		LEFT JOIN ma_goods ON ts_vendor_sale.GOODS_ID = ma_goods.ID
		LEFT JOIN ma_goodstype ON ma_goods.TYPE = ma_goodstype.ID
		WHERE ts_vendor_sale.IsActive = 1 "; 
		
		$sql =  $this->getSearchQuery($sql, $dataModel);	
		
		 
		// if($order != ""){
			// $this->db->order_by($order, $direction);
		// }else{
			// $this->db->order_by($this->id ,$direction); 
		// }
		
		if($order != ""){
			$sql .= " ORDER BY ".$order." ".$direction;
		}else{
			$sql .= " ORDER BY ".$this->id." ".$direction;
		}
		
		$sql .= " LIMIT $offset, $limit";
		
		//print($sql );
		 
		$query = $this->db->query($sql);
		return  $query->result_array();
	}
	public function getVendorGoodsComboList(){
		
		$sql = "SELECT * FROM ". $this->tbl_name . " WHERE IsActive = 1  "; 
		$query = $this->db->query($sql);
		return  $query->result_array();
	}
	public function deleteVendorGoodsname($id){
		// echo $id; die();
		$result = false;
		try{
			$query = $this->getVendorGoodsNameById($id);
			// $modelData;			
			foreach ($query->result() as $row)
			{
			   		
				$modelData = array( 
					//'update_date' => date("Y-m-d H:i:s"),
					//'update_user' => $this->session->userdata('user_name'),
					'Edit'=>$this->session->userdata('user'),
					'IsActive' => 0 //$row->VendorGoods_IsActive 
				); 
			}
			
			$this->db->where($this->id, $id);
        	return $this->db->update($this->tbl_name, $modelData);
			
		}catch(Exception $ex){
			return $result;
		}
    }
}
?>