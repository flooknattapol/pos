<?php
  
class DiscountModel extends CI_Model {
	
    private $tbl_name = 'ma_discount';
	private $id = 'ID';
 
    public function __construct() {
        parent::__construct();
    }
	
	public function getDiscountNameById($id){
		$this->db->where($this->id, $id);
		return $this->db->get($this->tbl_name);
	}
	
	public function insert($modelData){
		 
	 	$this->db->insert($this->tbl_name, $modelData); 
		return $this->db->insert_id(); 
    }
     
    public function update($id, $modelData){
        $this->db->where($this->id, $id);
        return $this->db->update($this->tbl_name, $modelData);
    }
	
	public function getDiscountNameAllList(){
        //return $this->db->count_all($this->tbl_name);
        
        $this->db->select('ID','NAME','SKU','PRICE','UNIT');
		$this->db->where('IsActive', 1);
        $query =  $this->db->get($this->tbl_name);
		
		return $query->result_array();
    }
	
	public function getDiscountModel($id){
        //return $this->db->count_all($this->tbl_name);
        
        //$this->db->select('id','name','contact','address1','address2','address3','tel','email','taxid','website');
		$this->db->where('IsActive', 1);
		$this->db->where($this->id, $id);
        $query =  $this->db->get($this->tbl_name);
		
		return $query->result_array();
    }
	
	public function getSearchQuery($sql, $dataModel){
		
		
		if(isset($dataModel['NAME']) && $dataModel['NAME'] != ""){
			$sql .= " and ma_discount.NAME like '%".$this->db->escape_str( $dataModel['NAME'])."%' ";
	   	}

		return $sql;
	}
	
	public function getTotal($dataModel ,$idSession){
		$this->load->model('UserModel', '', TRUE);

		if($idSession != ""){
			$idsession = $idSession; 
		}else{
			$idsession = $this->session->userdata('id');
		}
		$userPosition = $this->UserModel->getPostion($idsession);
		$sql = "SELECT ma_discount.* FROM ". $this->tbl_name . " 
		WHERE ma_discount.IsActive >= 0  AND ma_discount.BUSINESS_ID = '".$userPosition['BUSINESS_ID']."' AND ma_discount.TYPE_ID = '1'"; 

				
		$sql =  $this->getSearchQuery($sql, $dataModel);
		
		$query = $this->db->query($sql);		 
		
		return  $query->num_rows() ;
	}
	
	public function getDiscountNameList($dataModel, $limit = 10, $offset = 0, $order = '', $direction = 'asc',$idSession){
		$this->load->model('UserModel', '', TRUE);
		if($idSession != ""){
			$idsession = $idSession; 
		}else{
			$idsession = $this->session->userdata('id');
		}
		$userPosition = $this->UserModel->getPostion($idsession);
		
		$sql = "SELECT ma_discount.* FROM ". $this->tbl_name . " 
		WHERE ma_discount.IsActive >= 0  AND ma_discount.BUSINESS_ID = '".$userPosition['BUSINESS_ID']."' AND ma_discount.TYPE_ID = '1'"; 
		$sql =  $this->getSearchQuery($sql, $dataModel);

		if($order != ""){
			$sql .= " ORDER BY ma_discount.".$order." ".$direction;
		}else{
			$sql .= " ORDER BY ma_discount.".$this->id." ".$direction;
		}
		
		$sql .= " LIMIT $offset, $limit";
		
		
		 
		$query = $this->db->query($sql);
		return  $query->result_array();
	}
	public function getTotalCampaign($dataModel ,$idSession){
		$this->load->model('UserModel', '', TRUE);

		if($idSession != ""){
			$idsession = $idSession; 
		}else{
			$idsession = $this->session->userdata('id');
		}
		$userPosition = $this->UserModel->getPostion($idsession);
		$sql = "SELECT ma_discount.* FROM ". $this->tbl_name . " 
		WHERE ma_discount.IsActive >= 0  AND ma_discount.BUSINESS_ID = '".$userPosition['BUSINESS_ID']."' 
		AND ma_discount.TYPE_ID = '2'
		AND ma_discount.DATE_END_CAMPAIGN >= date(now())"; 

				
		$sql =  $this->getSearchQuery($sql, $dataModel);
		
		$query = $this->db->query($sql);		 
		
		return  $query->num_rows() ;
	}
	
	public function getDiscountNameListCampaign($dataModel, $limit = 10, $offset = 0, $order = '', $direction = 'asc',$idSession){
		$this->load->model('UserModel', '', TRUE);
		if($idSession != ""){
			$idsession = $idSession; 
		}else{
			$idsession = $this->session->userdata('id');
		}
		$userPosition = $this->UserModel->getPostion($idsession);
		
		$sql = "SELECT ma_discount.* FROM ". $this->tbl_name . " 
		WHERE ma_discount.IsActive >= 0  AND ma_discount.BUSINESS_ID = '".$userPosition['BUSINESS_ID']."' 
		AND ma_discount.TYPE_ID = '2' 
		AND ma_discount.DATE_END_CAMPAIGN >= date(now())"; 
		$sql =  $this->getSearchQuery($sql, $dataModel);

		if($order != ""){
			$sql .= " ORDER BY ma_discount.".$order." ".$direction;
		}else{
			$sql .= " ORDER BY ma_discount.".$this->id." ".$direction;
		}
		
		$sql .= " LIMIT $offset, $limit";
		
		 
		$query = $this->db->query($sql);
		return  $query->result_array();
	}
	public function deleteDiscountname($id){
		// echo $id; die();
		$result = false;
		try{
			$query = $this->getDiscountNameById($id);
			// echo $id;die();
			// $modelData;			
			foreach ($query->result() as $row)
			{
			   		
				$modelData = array( 
					//'update_date' => date("Y-m-d H:i:s"),
					//'update_user' => $this->session->userdata('user_name'),
					// 'Edit'=>$this->session->userdata('user'),
					'IsActive' => 0 //$row->Discount_IsActive 
				); 
			}
			// echo $this->tbl_name;
			// print_r($modelData);die();
			$this->db->set('IsActive',0);
			$this->db->where($this->id, $id);
        	return $this->db->update($this->tbl_name);
			//return $this->update($id, $modelData);
			//$sql = "Delete FROM ". $this->tbl_name; 
			//return  $this->db->query($sql);
			
		}catch(Exception $ex){
			return $result;
		}
	}
	public function undeleteDiscountname($id){
		// echo $id; die();
		$result = false;
		try{
			$query = $this->getDiscountNameById($id);
			// echo $id;die();
			// $modelData;			
			foreach ($query->result() as $row)
			{
			   		
				$modelData = array( 
					//'update_date' => date("Y-m-d H:i:s"),
					//'update_user' => $this->session->userdata('user_name'),
					// 'Edit'=>$this->session->userdata('user'),
					'IsActive' => 1 //$row->Discount_IsActive 
				); 
			}
			// echo $this->tbl_name;
			// print_r($modelData);die();
			$this->db->set('IsActive',1);
			$this->db->where($this->id, $id);
        	return $this->db->update($this->tbl_name);
			//return $this->update($id, $modelData);
			//$sql = "Delete FROM ". $this->tbl_name; 
			//return  $this->db->query($sql);
			
		}catch(Exception $ex){
			return $result;
		}
	}
	public function getDiscountComboList(){
		
		
		$this->load->model('UserModel', '', TRUE);
		if(! $this->session->userdata('validated')){

		}else{
			$idsession = $this->session->userdata('id');
		}
		$userPosition = $this->UserModel->getPostion($idsession);

		$sql = "SELECT * FROM  ma_discount
		WHERE IsActive = 1  AND BUSINESS_ID = '".$userPosition['BUSINESS_ID']."'"; 
		
		$query = $this->db->query($sql);
		return  $query->result_array();
	}
	
	#### controller ###
	public function add($dataPost )
	{
		$nResult = 0;
		try {
			$idSession = isset($dataPost['idSession']) ? $dataPost['idSession'] : "";
			// echo $idSession;die();
			$this->load->model('UserModel', '', TRUE);
			if($idSession != ""){
				$idsession = $idSession; 
			}else{
				$idsession = $this->session->userdata('id');
			}
			$userPosition = $this->UserModel->getPostion($idsession);


			$data['ID'] =  isset($dataPost['ID']) ? $dataPost['ID'] : 0;
			$data['NAME'] =  isset($dataPost['NAME']) ? $dataPost['NAME'] : "";
			$data['DETAIL'] =  isset($dataPost['DETAIL']) ? $dataPost['DETAIL'] : "";
			$data['DISCOUNT'] =  isset($dataPost['DISCOUNT']) ? $dataPost['DISCOUNT'] : "";
			$data['BAHTORPERCENT'] =  isset($dataPost['BAHTORPERCENT']) ? $dataPost['BAHTORPERCENT'] : "";
			$data['BUSINESS_ID'] = $userPosition['BUSINESS_ID'];
			$data['EXPIRATIONDATE'] =  isset($dataPost['EXPIRATIONDATE']) ? $dataPost['EXPIRATIONDATE'] : "";
			$data['IsActive'] = 1;
			$data['IMAGE'] =  isset($dataPost['IMAGE']) ? $dataPost['IMAGE'] : "";
			$data['STATUS'] =  isset($dataPost['STATUS']) ? $dataPost['STATUS'] : "";
			$data['DATE_END_CAMPAIGN'] =  isset($dataPost['DATE_END_CAMPAIGN']) ? $dataPost['DATE_END_CAMPAIGN'] : "";
			$data['DATE_START_CAMPAIGN'] =  isset($dataPost['DATE_START_CAMPAIGN']) ? $dataPost['DATE_START_CAMPAIGN'] : "";
			$data['TOTALDATE'] =  isset($dataPost['TOTALDATE']) ? $dataPost['TOTALDATE'] : 0;
			$data['TYPE_ID'] =  isset($dataPost['TYPE_ID']) ? $dataPost['TYPE_ID'] : "";
			$data['EXCHANGE_REWARD'] =  isset($dataPost['EXCHANGE_REWARD']) ? $dataPost['EXCHANGE_REWARD'] : "";
			$str = time();
			if ($data['ID'] == 0) {
				$nResult = $this->insert($data);
				if ($nResult > 0) {
					$data['UID'] = "11".$str.$nResult;
					$data['BARCODE'] = $this->set_barcode("11".$str.$nResult);
					$data['ID'] = $nResult;
					$nResult = $this->update($nResult, $data);
				}
			} else {
				$data['UID'] = "11".$str.$data['ID'];
				$data['BARCODE'] = $this->set_barcode("11".$str.$data['ID']);
				$nResult = $this->update($data['ID'], $data);	
			}
			if ($nResult > 0) {
				$result['status'] = true;
				$result['message'] = $this->lang->line("savesuccess");
			
			} else {
				$result['status'] = false;
				$result['message'] = $this->lang->line("error");
			}
			
		} catch (Exception $ex) {
			$result['status'] = false;
			$result['message'] = "exception: " . $ex;
		}

		return  $result;
	}

	public function delete($dataPost )
	{
		try {
			$id =  isset($dataPost['ID']) ? $dataPost['ID'] : 0; // $this->input->post('ap_id');

			$bResult = $this->deleteDiscountname($id);

			if ($bResult) {
				$result['status'] = true;
				$result['message'] = $this->lang->line("savesuccess");
			} else {
				$result['status'] = false;
				$result['message'] = $this->lang->line("error_faliure");
			}
		} catch (Exception $ex) {
			$result['status'] = false;
			$result['message'] = "exception: " . $ex;
		}

		return  $result;
	}
	
	public function undelete($dataPost)
	{
		try {
			$id =  isset($dataPost['ID']) ? $dataPost['ID'] : 0; // $this->input->post('ap_id');

			$bResult = $this->undeleteDiscountname($id);

			if ($bResult) {
				$result['status'] = true;
				$result['message'] = $this->lang->line("savesuccess");
			} else {
				$result['status'] = false;
				$result['message'] = $this->lang->line("error_faliure");
			}
		} catch (Exception $ex) {
			$result['status'] = false;
			$result['message'] = "exception: " . $ex;
		}

		return  $result;
	}
	public function getList($dataPost )
	{

		try {

			$PageIndex =  isset($dataPost['PageIndex']) ? $dataPost['PageIndex'] : 1;
			$PageSize =  isset($dataPost['PageSize']) ? $dataPost['PageSize'] : 20;
			$direction =  isset($dataPost['SortColumn']) ? $dataPost['SortColumn'] : "";
			$SortOrder = isset($dataPost['SortOrder']) ? $dataPost['SortOrder'] : "asc";
			$dataModel = isset($dataPost['mSearch']) ? $dataPost['mSearch'] : "";
			$idSession = isset($dataPost['idSession']) ? $dataPost['idSession'] : "";

			$offset = ($PageIndex - 1) * $PageSize;

			$result['status'] = true;
			$result['message'] = $this->getDiscountNameList($dataModel, $PageSize, $offset, $direction, $SortOrder,$idSession);
			$result['totalRecords'] = $this->getTotal($dataModel,$idSession);
			$result['toTalPage'] = ceil($result['totalRecords'] / $PageSize);

		} catch (Exception $ex) {
			$result['status'] = false;
			$result['message'] = "exception: " . $ex;
		}

		return  $result;
	}
	public function getListCampaign($dataPost )
	{

		try {

			$PageIndex =  isset($dataPost['PageIndex']) ? $dataPost['PageIndex'] : 1;
			$PageSize =  isset($dataPost['PageSize']) ? $dataPost['PageSize'] : 20;
			$direction =  isset($dataPost['SortColumn']) ? $dataPost['SortColumn'] : "";
			$SortOrder = isset($dataPost['SortOrder']) ? $dataPost['SortOrder'] : "asc";
			$dataModel = isset($dataPost['mSearch']) ? $dataPost['mSearch'] : "";
			$idSession = isset($dataPost['idSession']) ? $dataPost['idSession'] : "";

			$offset = ($PageIndex - 1) * $PageSize;

			$result['status'] = true;
			$result['message'] = $this->getDiscountNameListCampaign($dataModel, $PageSize, $offset, $direction, $SortOrder,$idSession);
			$result['totalRecords'] = $this->getTotalCampaign($dataModel,$idSession);
			$result['toTalPage'] = ceil($result['totalRecords'] / $PageSize);

		} catch (Exception $ex) {
			$result['status'] = false;
			$result['message'] = "exception: " . $ex;
		}

		return  $result;
	}
	public function getComboList(){
		try{ 
			$result['status'] = true;
			$result['message'] = $this->getDiscountComboList();
		}catch(Exception $ex){
			$result['status'] = false;
			$result['message'] = "exception: ".$ex;
		}
		
		return $result;	
	}
	function set_barcode($code){
		$this->load->library('zend');
		$this->zend->load('Zend/Barcode');
		$file = Zend_Barcode::draw('code128', 'image', array('text' => $code,'factor'=>3), array());
		$code = time().$code;
		$store_image = imagepng($file,FCPATH ."upload/barcode/{$code}.png");
		// echo $file,FCPATH ."upload\barcode/{$code}.png";die();
		return $code.'.png';
	}
	public function getListVoucher($dataPost){
		
		$business_id =  isset($dataPost['business_id']) ? $dataPost['business_id'] : "";

		$sql = "SELECT ma_discount.*,ma_discount.ID as VOUCHER_ID FROM  ". $this->tbl_name . "
		WHERE ma_discount.IsActive = 1 AND ma_discount.TYPE_ID = 2 AND ma_discount.BUSINESS_ID = '".$business_id."'"; 

		$query = $this->db->query($sql);
		return  $query->result_array();
	}
	public function getListRelation($dataPost){
		
		$UID =  isset($dataPost['UID']) ? $dataPost['UID'] : "";

		$sql = "SELECT ma_discount_relation.* FROM ma_discount_relation
		WHERE ma_discount_relation.IsActive = 1  AND ma_discount_relation.UID = '".$UID."'"; 
		$query = $this->db->query($sql)->row_array();

		if ($query !=  "") {

			$sqldiscount = "SELECT ma_discount.* FROM ma_discount
			WHERE ma_discount.IsActive = 1  AND ma_discount.ID = '".$query['DISCOUNT_ID']."'"; 
			$res = $this->db->query($sqldiscount)->row_array();
		
			
			if($res != ""){

				$this->db->set('IsActive',0);
				$this->db->where('UID',$UID);
				$this->db->update('ma_discount_relation');

				$result['status'] = true;
				$result['message'] = $res;

				
			}else{
				$result['status'] = false;
				$result['message'] = $this->lang->line("errora");
			}
		} else {
			$result['status'] = false;
			$result['message'] = $this->lang->line("errorb");
		}	

		return  $result;
	}
	
	public function addDiscountRelation($dataPost )
	{
		$nResult = 0;
		try {
			$userID =  isset($dataPost['userID']) ? $dataPost['userID'] : "";

			if($userID != ""){
				$sqlcustomer = "SELECT * FROM ma_customer WHERE userID = '".$userID."'";
				$rescustomer = $this->db->query($sqlcustomer)->row_array();
				
				if($rescustomer != ""){
					$idvoucher =  isset($dataPost['VOUCHER_ID']) ? $dataPost['VOUCHER_ID'] : 0;
					if($idvoucher > 0){
						$sqlvoucher = "SELECT * FROM ma_discount WHERE ID = '".$idvoucher."'";
						$resvoucher = $this->db->query($sqlvoucher)->row_array();

						$data['CUSTOMER_ID'] = $rescustomer['ID'];
						$data['DISCOUNT_ID'] = $resvoucher['ID'];
						$str = time();
						$data['UID'] = "22".$str.$nResult;
						$data['BARCODE'] = $this->set_barcode("22".$str.$nResult);
						$data['EXPIRATIONDATE'] = $NewDate=Date('F d, Y', strtotime("+".$resvoucher['TOTALDATE']." days"));
						$this->db->insert('ma_discount_relation', $data); 
						$nResult =  $this->db->insert_id(); 
						if ($nResult > 0) {
							$result['status'] = true;
							$result['message'] = $this->lang->line("savesuccess");
						
						} else {
							$result['status'] = false;
							$result['message'] = $this->lang->line("error");
						}
					}else{
						$result['status'] = false;
						$result['message'] = $this->lang->line("error")."c";
					}
					
				}else{
				$result['status'] = false;
				$result['message'] = $this->lang->line("error")."b";
				}
			}else{
				$result['status'] = false;
				$result['message'] = $this->lang->line("error")."a";
			}
			
			
			
		} catch (Exception $ex) {
			$result['status'] = false;
			$result['message'] = "exception: " . $ex;
		}

		return  $result;
	}
	public function getListVoucherInCustomer($dataPost){
		
		$userID =  isset($dataPost['userID']) ? $dataPost['userID'] : "";

		if($userID != ""){
			$sqlcustomer = "SELECT * FROM ma_customer WHERE userID = '".$userID."'";
			$rescustomer = $this->db->query($sqlcustomer)->row_array();
			// print_r($rescustomer);die();
			if($rescustomer != ""){
				$sql = "SELECT ma_discount_relation.*,ma_discount.*  FROM  ma_discount_relation
				LEFT JOIN ma_discount ON ma_discount_relation.DISCOUNT_ID = ma_discount.ID
				WHERE  ma_discount_relation.CUSTOMER_ID = '".$rescustomer['ID']."' AND ma_discount_relation.IsActive = 1
				"; 
				$query = $this->db->query($sql);
				return  $query->result_array();
			}
		}

	}
	public function getListPointReward($dataPost){
		
		$userID =  isset($dataPost['userID']) ? $dataPost['userID'] : "";

		if($userID != ""){
			$sqlcustomer = "SELECT * FROM ma_customer WHERE userID = '".$userID."'";
			$rescustomer = $this->db->query($sqlcustomer)->row_array();
			// print_r($rescustomer);die();
			if($rescustomer != ""){
				$sql = "SELECT ma_customer_relation.*,ma_business.*  FROM  ma_customer_relation
				LEFT JOIN ma_business ON ma_customer_relation.BUSINESS_ID = ma_business.ID
				WHERE  ma_customer_relation.CUSTOMER_ID = '".$rescustomer['ID']."' AND ma_business.IsActive = 1
				"; 
				// echo $sql;die();
				$query = $this->db->query($sql);
				return  $query->result_array();
			}
		}

	}
}
?>