<?php
  
class OrderDetailModel extends CI_Model {
	
    private $tbl_name = 'ma_order_detail';
	private $id = 'ID';
 
    public function __construct() {
        parent::__construct();
    }
	
	public function getQuatationDetailById($id){
		$this->db->where($this->id, $id);
		return $this->db->get($this->tbl_name);
	}
	
	public function insert($modelData){
		 
	 	$this->db->insert($this->tbl_name, $modelData); 
		return $this->db->insert_id(); 
    }
     
    public function update($id, $modelData){
        $this->db->where($this->id, $id);
        return $this->db->update($this->tbl_name, $modelData);
    }
	
	public function listUpdate($id, $listData ,$ValueInList){

		$nResult  = 0;
		foreach ($listData as $key => $data)
		{
			$dataNew['ID'] = $data['ID'];
			$dataNew['GOODS_ID'] = $data['GOODS_ID'];
			$dataNew['ORDER_ID'] = $id;
			$dataNew['ITEMAMOUNT'] = $data['ITEMAMOUNT'];
			$dataNew['COST'] = $data['COST'];
			$dataNew['TOTAL'] = $data['TOTAL'];

			if ($data['ID'] == 0) {  
				
				$nResult = $this->insert($dataNew); 
		    }
		    else {  
		    	$nResult = $this->update($data['ID'], $dataNew);
		    }		
		}
		foreach ($ValueInList as $key => $data)
		{
			if($data['ID'] != 0){
				$this->db->where('ID',$data['ID']);
				$this->db->delete('ma_branch');
			}
		}
		//echo $nResult; die();
        return $nResult;
    }

	public function getQuatationDetailModel($id){
        //return $this->db->count_all($this->tbl_name);
        
        //$this->db->select('id','name','contact','address1','address2','address3','tel','email','taxid','website');
		$this->db->where('id', $id);
        $query =  $this->db->get($this->tbl_name);
		
		return $query->result_array();
    }
	
	public function getQuatationDetailListById($id){
		
		$sql  = "SELECT quatation_detail.*,project.name FROM quatation_detail 
		LEFT JOIN project ON project.id = quatation_detail.item_no
		WHERE quatation_detail.qt_id = '".$id."'";
        $query =  $this->db->query($sql);

		return $query->result_array();
    }
	public function getItemProduct($id){
		$sql = "SELECT * FROM project WHERE id = '$id'";
		$query = $this->db->query($sql);		 

		return  $query->result_array();
	}
	public function LogDeliverList($id){
		$sql = "SELECT * FROM logdeliver WHERE id_order = '$id'";
		$query = $this->db->query($sql);		 

		return  $query->result_array();
	}
	public function getSearchQuery($sql, $dataModel){
		
		//print_r($dataModel);
		
		/*if(isset($dataModel['name']) && $dataModel['name'] != ""){
		 	$sql .= " and name like '%".$this->db->escape_str( $dataModel['name'])."%' ";
		}
		
		if(isset($dataModel['branch']) && $dataModel['branch'] != ""){
		 	$sql .= " and branch like '%".$this->db->escape_str( $dataModel['branch'])."%' ";
		}
		
		if(isset($dataModel['contact']) && $dataModel['contact'] != ""){
		 	$sql .= " and contact like '%".$this->db->escape_str( $dataModel['contact'])."%' ";
		}
		
		if(isset($dataModel['email']) && $dataModel['email'] != ""){
		 	$sql .= " and email like '%".$this->db->escape_str( $dataModel['email'])."%' ";
		}*/
		
		return $sql;
	}
	
	public function getTotal($dataModel ){
		
		$sql = "SELECT * FROM ". $this->tbl_name  ." WHERE deleteflag = 0  ";
				
		$sql =  $this->getSearchQuery($sql, $dataModel);
		
		$query = $this->db->query($sql);		 
		
		return  $query->num_rows() ;
	}
	
	public function getQuatationDetailModelList($dataModel, $limit = 10, $offset = 0, $order = '', $direction = 'asc'){
		
		$sql = "SELECT * FROM ". $this->tbl_name . " WHERE deleteflag = 0  "; 
		
		$sql =  $this->getSearchQuery($sql, $dataModel);	
		
		 
		// if($order != ""){
			// $this->db->order_by($order, $direction);
		// }else{
			// $this->db->order_by($this->id ,$direction); 
		// }
		
		if($order != ""){
			$sql .= " ORDER BY ".$order." ".$direction;
		}else{
			$sql .= " ORDER BY ".$this->id." ".$direction;
		}
		
		$sql .= " LIMIT ".$offset.", ".$limit;
		 
		$query = $this->db->query($sql);
		return  $query->result_array();
	}
 
	public function getBranchComboList($id){
		
		if($id > 0){
			$sql = "SELECT * FROM ". $this->tbl_name . " WHERE IsActive = 1 AND BUSINESS_ID = '".$id."'";
		}else{
			$sql = "SELECT * FROM ". $this->tbl_name . " WHERE IsActive = 1";
		}
		$query = $this->db->query($sql);
		// echo $sql;
		// print_r($query);
		// die();
		return  $query->result_array();
	}
	###controller###
	public function getComboList($dataPost){
		try{ 
			$idSession = isset($dataPost['idSession']) ? $dataPost['idSession'] : "";
			$this->load->model('UserModel', '', TRUE);
			if($idSession != ""){
				$idsession = $idSession; 
			}else{
				$idsession = $this->session->userdata('id');
			}
			$result['status'] = true;
			$result['message'] = $this->getBranchComboList($idsession);
		}catch(Exception $ex){
			$result['status'] = false;
			$result['message'] = "exception: ".$ex;
		}
		
		return $result;	
	}
}
?>