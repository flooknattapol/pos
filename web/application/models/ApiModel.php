<?php
  
class ApiModel extends CI_Model {
	
	public function getCountry(){
		
		$sql = "SELECT distinct t2.country,if(t2.img_file='','https://img.icons8.com/cotton/2x/globe.png',t2.img_file) as img_file  FROM  t_result t1 
		LEFT JOIN t_country t2 on t1.country_id = t2.id 
		WHERE t1.delete_flag = 0 and t1.country_id != 0";//. " WHERE User_delete_flag = 0 AND User_name LIKE ?";
	
		
		$query = $this->db->query($sql);
		//$query = $this->db->query($sql, array( "%".$dataModel['User_name']."%"));// $dataModel);
		
		return  $query->result_array();
	}	
	public function updateFullCountry($country,$full){
		
		$sql = "UPDATE t_country set full_name = '".$full."' WHERE country ='".$country."'";//. " WHERE User_delete_flag = 0 AND User_name LIKE ?";
	
		
		
		//$query = $this->db->query($sql, array( "%".$dataModel['User_name']."%"));// $dataModel);
		
		return  $query = $this->db->query($sql);
	}		
	
    public function getCategoryByCountry($country){
		
		$sql = "SELECT distinct t3.id,t3.category,if(t3.img_file='','https://cdn.pixabay.com/photo/2019/12/08/04/55/box-4680467_960_720.png',t3.img_file) as img_file FROM t_result t1 
        LEFT JOIN t_country t2 on t1.country_id = t2.id 
        LEFT JOIN t_category t3 on t1.category_id = t3.id 
        WHERE t2.country Like '%".$country."%'";//. " WHERE User_delete_flag = 0 AND User_name LIKE ?";
	
		
		$query = $this->db->query($sql);
		//$query = $this->db->query($sql, array( "%".$dataModel['User_name']."%"));// $dataModel);
		
		return  $query->result_array();
    }	
    public function getSub1ByCountryAndCate($country,$category){
		
		$sql = "SELECT distinct t4.id,t4.sub_category1,if(t4.img_file='','https://cdn.pixabay.com/photo/2019/12/08/04/55/box-4680467_960_720.png',t4.img_file) as img_file FROM t_result t1 
        LEFT JOIN t_country t2 on t1.country_id = t2.id 
        LEFT JOIN t_category t3 on t1.category_id = t3.id 
        LEFT JOIN t_sub_category1 t4 on t1.sub_category1_id = t4.id 
        WHERE t2.full_name Like '%".$country."%' and t3.category Like '%".$category."%'";//. " WHERE User_delete_flag = 0 AND User_name LIKE ?";
	
		
		$query = $this->db->query($sql);
		//$query = $this->db->query($sql, array( "%".$dataModel['User_name']."%"));// $dataModel);
		
		return  $query->result_array();
	}	
	public function getSub2($country,$category,$sub_category1){
		
		$sql = "SELECT distinct t1.* FROM t_result t1 
        LEFT JOIN t_country t2 on t1.country_id = t2.id 
        LEFT JOIN t_category t3 on t1.category_id = t3.id 
        LEFT JOIN t_sub_category1 t4 on t1.sub_category1_id = t4.id 
		WHERE t2.full_name Like '%".$country."%' and t3.category Like '%".$category."%' and t4.sub_category1 Like '%".$sub_category1."%'
		ORDER BY t1.allow_flag desc";//. " WHERE User_delete_flag = 0 AND User_name LIKE ?";

		
		$query = $this->db->query($sql);
		//$query = $this->db->query($sql, array( "%".$dataModel['User_name']."%"));// $dataModel);
		
		return  $query->result_array();
	}	
	public function getResult($country,$category,$sub_category1,$sub_category2){
		
		$sql = "SELECT distinct t1.* FROM t_result t1 
        LEFT JOIN t_country t2 on t1.country_id = t2.id 
        LEFT JOIN t_category t3 on t1.category_id = t3.id 
        LEFT JOIN t_sub_category1 t4 on t1.sub_category1_id = t4.id 
        WHERE t2.full_name Like '%".$country."%' and t3.category Like '%".$category."%' and t4.sub_category1 Like '%".$sub_category1."%' and t1.sub_category2 Like '%".$sub_category2."%'";//. " WHERE User_delete_flag = 0 AND User_name LIKE ?";
	
		
		$query = $this->db->query($sql);
		//$query = $this->db->query($sql, array( "%".$dataModel['User_name']."%"));// $dataModel);
		
		return  $query->result_array();
	}
	public function getNoneStandard(){
		$path= base_url('').'upload/Nonestandardservice/';
		$sql = "SELECT img_file,CONCAT('".$path."',pdf_file) as pdf_file,none_standard_service as text,'None standard service' as header
		FROM t_none_standard_service 
		WHERE delete_flag = 0 ";
    
	
		
		$query = $this->db->query($sql);
		//$query = $this->db->query($sql, array( "%".$dataModel['User_name']."%"));// $dataModel);
		
		return  $query->result_array();
	}
	public function getGeneralInfo(){
		$path= base_url('').'upload/generalinfo/';
		
		$sql = "SELECT img_file,CONCAT('".$path."',pdf_file) as pdf_file,general_info as text,'Information' as header
		FROM t_general_info 
		WHERE delete_flag = 0 ";
    
	
		
		$query = $this->db->query($sql);
		//$query = $this->db->query($sql, array( "%".$dataModel['User_name']."%"));// $dataModel);
		
		return  $query->result_array();
	}				
	public function getSub2ByCountryAndCate($country,$category){
		
		$sql = "SELECT distinct t1.* FROM t_result t1 
        LEFT JOIN t_country t2 on t1.country_id = t2.id 
        LEFT JOIN t_category t3 on t1.category_id = t3.id 
		WHERE t2.full_name Like '%".$country."%' and t3.category Like '%".$category."%'";//. " WHERE User_delete_flag = 0 AND User_name LIKE ?";

		
		$query = $this->db->query($sql);
		//$query = $this->db->query($sql, array( "%".$dataModel['User_name']."%"));// $dataModel);
		
		return  $query->result_array();
	}	
	public function checkRegister($line_user_id){
		
		$sql = "SELECT * FROM t_register 
		WHERE delete_flag = 0 and line_user_id Like '%".$line_user_id."%'";//. " WHERE User_delete_flag = 0 AND User_name LIKE ?";

		
		$query = $this->db->query($sql);
		//$query = $this->db->query($sql, array( "%".$dataModel['User_name']."%"));// $dataModel);
		
		return  $query->result_array();
	}	
	
}
?>