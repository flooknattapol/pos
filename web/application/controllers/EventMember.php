<?php
defined('BASEPATH') or exit('No direct script access allowed');

class EventMember extends CI_Controller
{
	public function __construct()
	{
		date_default_timezone_set('Asia/Bangkok');
		parent::__construct();
		if (!$this->session->userdata('validated')) {
			redirect('login');
		}
	}

	public function index()
	{
		$this->load->view('share/head');
		$this->load->view('share/sidebar');
		$this->load->view('eventmember/eventmember_view');
		$this->load->view('share/footer');
	}

	public function addEventMember()
	{
		// $this->output->set_content_type('application/json');
		$nResult = 0;

		try {

			$this->load->model('EventMemberModel', '', TRUE);
			$this->load->model('VendorModel', '', TRUE);

			$dataPost = json_decode($this->input->raw_input_stream, true);

			/*print_r($_POST);
			print_r($this->input->post()); 
			echo $this->input->raw_input_stream;*/

			//$dateRecord = date("Y-m-d H:i:s"); 
			$data['ID'] =  isset($dataPost['ID']) ? $dataPost['ID'] : 0;
			$data['MEMBER_ID'] =  isset($dataPost['MEMBER_ID']) ? $dataPost['MEMBER_ID'] : "";
			$data['EVENT_ID'] = isset($dataPost['EVENT_ID']) ? $dataPost['EVENT_ID'] : "";
			// $data['VENDOR_NAME'] = isset($dataPost['VENDOR_NAME']) ? $dataPost['VENDOR_NAME'] : "";
			$data['VENDOR'] = isset($dataPost['VENDOR']) ? $dataPost['VENDOR'] : 0;
			$data['BUYER'] = isset($dataPost['BUYER']) ? $dataPost['BUYER'] : 0;
			$itemGoods = isset($dataPost['DetailGoods']) ? $dataPost['DetailGoods'] : "";
		
			if ($data['ID'] == 0) {
				$data['Create'] = $this->session->userdata('user');
				$data['Edit'] = $this->session->userdata('user');
				$nResult = $this->EventMemberModel->insert($data);
			} else {
				$data['Edit'] = $this->session->userdata('user');
				$nResult = $this->EventMemberModel->update($data['ID'], $data);
			}

			

			foreach($itemGoods as $item){
				$res['SKU'] = $item['SKU'];
				$res['MEMBER_ID'] = $item['MEMBER_ID'];
				$res['NAME'] = $item['NAME'];
				$res['TYPE'] = $item['TYPE'];
				$res['IMAGE'] = $item['IMAGE'];
				$res['UNIT'] = $item['UNIT'];
				$res['PRICE	'] = $item['PRICE'];
				$res['qty_privilege'] = $item['qty_privilege'];
				$res['discount	'] = $item['discount'];
				
				if($item['EventNotActive'] != ""){
					$res['EventNotActive'] = json_encode($item['EventNotActive'],true);
				}else{
					$res['EventNotActive'] = $item['EventNotActive'];
				}


				// echo $res['EventNotActive']."<br>";
				// print_r($res);die();
				// if($item['GoodsActive'] == true){

				// }
				// if($item['EventNotActive'] != ""){
				// 	$event = json_decode($item['EventNotActive'],true);
				// 	$reseventstr = "[";
				// 	foreach($event as $ev){
				// 		$reseventstr .= '{"EVENT_ID":"'.$ev['EVENT_ID'].'"},';
				// 	}
				// 	if($item['GoodsActive'] == true){
				// 		$reseventstr .= '{"EVENT_ID":"'.$data['EVENT_ID'].'"},';
				// 		$reseventstr = substr($reseventstr, 0, -1);  
				// 		$reseventstr .= "]";
				// 	}else{
				// 		$reseventstr = substr($reseventstr, 0, -1);  
				// 		$reseventstr .= "]";
				// 	}
				// 	$res['EventNotActive'] = $reseventstr;
				// }
				// else{
				// 	if($item['GoodsActive'] == true){
				// 		$reseventstr = "[";
				// 		$reseventstr .= '{"EVENT_ID":"'.$data['EVENT_ID'].'"},';
				// 		$reseventstr = substr($reseventstr, 0, -1);  
				// 		$reseventstr .= "]";
				// 		$res['EventNotActive'] = $reseventstr;

				// 		// echo $reseventstr." ".$data['EVENT_ID'];die();
				// 	}
				// }
				
				if ($item['ID'] == 0) {
					$nResult = $this->VendorModel->insertdetailgoods($res);
				} else {
					$nResult = $this->VendorModel->updatedetailgoods($item['ID'], $res);
				}
				
			}
			// die();
			if ($nResult > 0) {
				$result['status'] = true;
				$result['message'] = $this->lang->line("savesuccess");
			
			} else {
				$result['status'] = false;
				$result['message'] = $this->lang->line("error");
			}
		} catch (Exception $ex) {
			$result['status'] = false;
			$result['message'] = "exception: " . $ex;
		}

		echo json_encode($result, JSON_UNESCAPED_UNICODE);
	}
	public function upload_file()
    {

        $date = date('Y-m-d-H-i-s');

        $todayfile = 'imgEvent' . '_' . $date;
        $config['upload_path'] = FCPATH . 'upload/Goods';
        $config['allowed_types'] = '*';
        $config['max_size'] = 0;
        $config['max_width'] = 0;
        $config['max_height'] = 0;
        $config['file_name'] = $todayfile;
        $file = explode('.', $_FILES['filegeneralinfo']['name']);
        $this->load->library('upload', $config);
        $this->upload->initialize($config);
        $status = true;
        if (!$this->upload->do_upload('filegeneralinfo')) {
            $status = false;
            echo json_encode(array('status' => $status,  'message' => $this->upload->display_errors(), 'path' => $config['upload_path']));
        } else {
			$data = array('upload_data' => $this->upload->data());
			echo json_encode(array('status' => $status, 'message' => $data['upload_data']['file_name'], 'path' => $config['upload_path']));
        }
	}
	public function deleteEventMember()
	{
		try {
			$this->load->model('EventMemberModel', '', TRUE);
			$dataPost = json_decode($this->input->raw_input_stream, true);
			$id =  isset($dataPost['ID']) ? $dataPost['ID'] : 0; // $this->input->post('ap_id');

			$bResult = $this->EventMemberModel->deleteEventMembername($id);

			if ($bResult) {
				$result['status'] = true;
				$result['message'] = $this->lang->line("savesuccess");
			} else {
				$result['status'] = false;
				$result['message'] = $this->lang->line("error_faliure");
			}
		} catch (Exception $ex) {
			$result['status'] = false;
			$result['message'] = "exception: " . $ex;
		}

		echo json_encode($result, JSON_UNESCAPED_UNICODE);
	}

	public function getEventMemberModelList()
	{

		try {
			$this->load->model('EventMemberModel', '', TRUE);

			$data['m_id'] =  $this->input->post('mab_m_id');
			
			$limit =  $this->input->post('limit');
			$offset =  $this->input->post('offset');
			$order = $this->input->post('order');
			$direction = $this->input->post('direction');
			 
			$result = $this->EventMemberModel->getEventMemberNameAllList($data, $limit , $offset, $order, $direction);

			$result['status'] = true;
			$result['message'] = $this->lang->line("savesuccess");
		} catch (Exception $ex) {
			$result['status'] = false;
			$result['message'] = "exception: " . $ex;
		}

		echo json_encode($result, JSON_UNESCAPED_UNICODE);
	}


	public function getEventMemberModel()
	{

		try {
			$this->load->model('EventMemberModel', '', TRUE);
			$dataPost = json_decode($this->input->raw_input_stream, true);

			//print_r($_POST);
			//print_r($this->input->post()); 
			//echo $this->input->raw_input_stream;  

			//$dateRecord = date("Y-m-d H:i:s"); 
			$PageIndex =  isset($dataPost['PageIndex']) ? $dataPost['PageIndex'] : 1;
			$PageSize =  isset($dataPost['PageSize']) ? $dataPost['PageSize'] : 20;
			$direction =  isset($dataPost['SortColumn']) ? $dataPost['SortColumn'] : "";
			$SortOrder = isset($dataPost['SortOrder']) ? $dataPost['SortOrder'] : "asc";
			$dataModel = isset($dataPost['mSearch']) ? $dataPost['mSearch'] : "";

			$offset = ($PageIndex - 1) * $PageSize;

			$result['status'] = true;
			$result['message'] = $this->EventMemberModel->getEventMemberNameList($dataModel, $PageSize, $offset, $direction, $SortOrder);
			$result['totalRecords'] = $this->EventMemberModel->getTotal($dataModel);
			$result['toTalPage'] = ceil($result['totalRecords'] / $PageSize);

			//$result['message'] = $this->EventMemberModel->getEventMemberModel(); 

		} catch (Exception $ex) {
			$result['status'] = false;
			$result['message'] = "exception: " . $ex;
		}

		echo json_encode($result, JSON_UNESCAPED_UNICODE);
	}

	public function getEventMemberComboList()
	{

		try {
			$this->load->model('EventMemberModel', '', TRUE);
			$result['status'] = true;
			$result['message'] = $this->EventMemberModel->getEventMemberComboList();
		} catch (Exception $ex) {
			$result['status'] = false;
			$result['message'] = "exception: " . $ex;
		}

		echo json_encode($result, JSON_UNESCAPED_UNICODE);
	}
	public function getEventMemberComboListVat()
	{

		try {
			$this->load->model('EventMemberModel', '', TRUE);
			$result['status'] = true;
			$result['message'] = $this->EventMemberModel->getEventMemberComboListVat();
		} catch (Exception $ex) {
			$result['status'] = false;
			$result['message'] = "exception: " . $ex;
		}

		echo json_encode($result, JSON_UNESCAPED_UNICODE);
	}
	public function uploadProfile()
	{
		$todayFile = date("YmdHis");
		// $config['upload_path']          = base_url('uploads');// $_SERVER["DOCUMENT_ROOT"]. '/uploads/';
		// echo FCPATH;
		$config['upload_path']          = FCPATH . 'upload/profile';
		// $config['upload_path']          = '/Applications/MAMP/htdocs/training/uploads/attendance_list/';
		$config['allowed_types']        = 'jpg|png';
		$config['max_size']             = 1000000;
		$config['max_width']            = 3000;
		$config['max_height']           = 3000;
		$config['file_name']   = $todayFile;


		$this->load->library('upload', $config);
		$this->upload->initialize($config);
		// echo json_encode($config);
		$status = true;

		if (!$this->upload->do_upload('file_name')) {
			$status = false;
			echo json_encode(array('status' => $status,  'message' => $this->upload->display_errors(), 'path' => $config['upload_path']));
		} else {
			$data = array('upload_data' => $this->upload->data());
			// $downloadFile=$todayFile.".".$fileData[1];
			// $this->loadExcelToDB($todayFile."."$fileData[1]);
			echo json_encode(array('status' => $status, 'message' => $data['upload_data']['file_name'], 'path' => $config['upload_path']));
		}
	}
	public function uploadContract()
	{
		$todayFile = date("YmdHis");
		// $config['upload_path']          = base_url('uploads');// $_SERVER["DOCUMENT_ROOT"]. '/uploads/';
		// echo FCPATH;
		$config['upload_path']          = FCPATH . 'upload/contract';
		// $config['upload_path']          = '/Applications/MAMP/htdocs/training/uploads/attendance_list/';
		$config['allowed_types']        = 'pdf|xlsx|docx';
		$config['max_size']             = 1000000;
		$config['max_width']            = 3000;
		$config['max_height']           = 3000;
		$config['file_name']   = $todayFile;


		$this->load->library('upload', $config);
		$this->upload->initialize($config);
		// echo json_encode($config);
		$status = true;

		if (!$this->upload->do_upload('file_name')) {
			$status = false;
			echo json_encode(array('status' => $status,  'message' => $this->upload->display_errors(), 'path' => $config['upload_path']));
		} else {
			$data = array('upload_data' => $this->upload->data());
			// $downloadFile=$todayFile.".".$fileData[1];
			// $this->loadExcelToDB($todayFile."."$fileData[1]);
			echo json_encode(array('status' => $status, 'message' => $data['upload_data']['file_name'], 'path' => $config['upload_path']));
		}
	}
	public function uploadIdcard()
	{
		$todayFile = date("YmdHis");
		// $config['upload_path']          = base_url('uploads');// $_SERVER["DOCUMENT_ROOT"]. '/uploads/';
		// echo FCPATH;
		$config['upload_path']          = FCPATH . 'upload/id_card';
		// $config['upload_path']          = '/Applications/MAMP/htdocs/training/uploads/attendance_list/';
		$config['allowed_types']        = 'jpg|png';
		$config['max_size']             = 1000000;
		$config['max_width']            = 3000;
		$config['max_height']           = 3000;
		$config['file_name']   = $todayFile;


		$this->load->library('upload', $config);
		$this->upload->initialize($config);
		// echo json_encode($config);
		$status = true;

		if (!$this->upload->do_upload('file_name')) {
			$status = false;
			echo json_encode(array('status' => $status,  'message' => $this->upload->display_errors(), 'path' => $config['upload_path']));
		} else {
			$data = array('upload_data' => $this->upload->data());
			// $downloadFile=$todayFile.".".$fileData[1];
			// $this->loadExcelToDB($todayFile."."$fileData[1]);
			echo json_encode(array('status' => $status, 'message' => $data['upload_data']['file_name'], 'path' => $config['upload_path']));
		}
	}
	public function uploadHouseRegis()
	{
		$todayFile = date("YmdHis");
		// $config['upload_path']          = base_url('uploads');// $_SERVER["DOCUMENT_ROOT"]. '/uploads/';
		// echo FCPATH;
		$config['upload_path']          = FCPATH . 'upload/house_registration';
		// $config['upload_path']          = '/Applications/MAMP/htdocs/training/uploads/attendance_list/';
		$config['allowed_types']        = 'jpg|png';
		$config['max_size']             = 1000000;
		$config['max_width']            = 3000;
		$config['max_height']           = 3000;
		$config['file_name']   = $todayFile;


		$this->load->library('upload', $config);
		$this->upload->initialize($config);
		// echo json_encode($config);
		$status = true;

		if (!$this->upload->do_upload('file_name')) {
			$status = false;
			echo json_encode(array('status' => $status,  'message' => $this->upload->display_errors(), 'path' => $config['upload_path']));
		} else {
			$data = array('upload_data' => $this->upload->data());
			// $downloadFile=$todayFile.".".$fileData[1];
			// $this->loadExcelToDB($todayFile."."$fileData[1]);
			echo json_encode(array('status' => $status, 'message' => $data['upload_data']['file_name'], 'path' => $config['upload_path']));
		}
	}
	public function uploadBookbank()
	{
		$todayFile = date("YmdHis");
		// $config['upload_path']          = base_url('uploads');// $_SERVER["DOCUMENT_ROOT"]. '/uploads/';
		// echo FCPATH;
		$config['upload_path']          = FCPATH . 'upload/book_bank';
		// $config['upload_path']          = '/Applications/MAMP/htdocs/training/uploads/attendance_list/';
		$config['allowed_types']        = 'jpg|png';
		$config['max_size']             = 1000000;
		$config['max_width']            = 3000;
		$config['max_height']           = 3000;
		$config['file_name']   = $todayFile;


		$this->load->library('upload', $config);
		$this->upload->initialize($config);
		// echo json_encode($config);
		$status = true;

		if (!$this->upload->do_upload('file_name')) {
			$status = false;
			echo json_encode(array('status' => $status,  'message' => $this->upload->display_errors(), 'path' => $config['upload_path']));
		} else {
			$data = array('upload_data' => $this->upload->data());
			// $downloadFile=$todayFile.".".$fileData[1];
			// $this->loadExcelToDB($todayFile."."$fileData[1]);
			echo json_encode(array('status' => $status, 'message' => $data['upload_data']['file_name'], 'path' => $config['upload_path']));
		}
	}
}
