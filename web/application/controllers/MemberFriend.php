<?php
defined('BASEPATH') or exit('No direct script access allowed');

class MemberFriend extends CI_Controller
{
	public function __construct()
	{
		date_default_timezone_set('Asia/Bangkok');
		parent::__construct();
		if (!$this->session->userdata('validated')) {
			redirect('login');
		}
	}

	public function index()
	{
		$this->load->view('share/head');
		$this->load->view('share/sidebar');
		$this->load->view('memberfriend/memberfriend_view');
		$this->load->view('share/footer');
	}

	public function addMemberFriend()
	{
		// $this->output->set_content_type('application/json');
		$nResult = 0;

		try {

			$this->load->model('MemberFriendModel', '', TRUE);

			$dataPost = json_decode($this->input->raw_input_stream, true);

			/*print_r($_POST);
			print_r($this->input->post()); 
			echo $this->input->raw_input_stream;*/
			// print_r($dataPost);die();
			//$dateRecord = date("Y-m-d H:i:s"); 
			$data['ID'] =  isset($dataPost['ID']) ? $dataPost['ID'] : 0;
			$data['MEMBER_ID'] =  isset($dataPost['MEMBER_ID']) ? $dataPost['MEMBER_ID'] : "";
			$data['FRIEND_ID'] = isset($dataPost['FRIEND_ID']) ? $dataPost['FRIEND_ID'] : "";

			// print_r($data['FRIEND_ID']);
			// echo implode( $data['FRIEND_ID'] );die(); 

			$sql = "SELECT * FROM ts_relationship WHERE MEMBER_ID = '".$data['MEMBER_ID']."'";
			$res = $this->db->query($sql)->row_array();

			// print_r($res);die();
			// if(count($res) == 0){
			// 	$modelinsert['MEMBER_ID'] = $data['MEMBER_ID'];
			// 	$this->db->insert('ts_relationship',$modelinsert);
			// }
			$friendstr = "[";
			foreach($data['FRIEND_ID'] as $dt){
				$friendstr .= '{"MEMBER_ID":"'.$dt['ID'].'"},';
			}
			$resfrdstr = substr($friendstr, 0, -1);  
			$resfrdstr .= "]";
			// echo $resfrdstr;die();
			$data['FRIEND_ID'] = $resfrdstr;
			if (count($res) == 0) {
				$nResult = $this->MemberFriendModel->insert($data);
			} else {
				$nResult = $this->MemberFriendModel->update($data['ID'], $data);
			}

			if ($nResult > 0) {
				$result['status'] = true;
				$result['message'] = $this->lang->line("savesuccess");
			
			} else {
				$result['status'] = false;
				$result['message'] = $this->lang->line("error");
			}
			
			
		} catch (Exception $ex) {
			$result['status'] = false;
			$result['message'] = "exception: " . $ex;
		}

		echo json_encode($result, JSON_UNESCAPED_UNICODE);
	}

	public function deleteMemberFriend()
	{
		try {
			$this->load->model('MemberFriendModel', '', TRUE);
			$dataPost = json_decode($this->input->raw_input_stream, true);
			$id =  isset($dataPost['id']) ? $dataPost['id'] : 0; // $this->input->post('ap_id');

			$bResult = $this->MemberFriendModel->deleteMemberFriendname($id);

			if ($bResult) {
				$result['status'] = true;
				$result['message'] = $this->lang->line("savesuccess");
			} else {
				$result['status'] = false;
				$result['message'] = $this->lang->line("error_faliure");
			}
		} catch (Exception $ex) {
			$result['status'] = false;
			$result['message'] = "exception: " . $ex;
		}

		echo json_encode($result, JSON_UNESCAPED_UNICODE);
	}

	public function getMemberFriendModelList()
	{

		try {
			$this->load->model('MemberFriendModel', '', TRUE);

			$data['m_id'] =  $this->input->post('mab_m_id');
			
			$limit =  $this->input->post('limit');
			$offset =  $this->input->post('offset');
			$order = $this->input->post('order');
			$direction = $this->input->post('direction');
			 
			$result = $this->MemberFriendModel->getMemberFriendNameAllList($data, $limit , $offset, $order, $direction);

			$result['status'] = true;
			$result['message'] = $this->lang->line("savesuccess");
		} catch (Exception $ex) {
			$result['status'] = false;
			$result['message'] = "exception: " . $ex;
		}

		echo json_encode($result, JSON_UNESCAPED_UNICODE);
	}


	public function getMemberFriendModel()
	{

		try {
			$this->load->model('MemberFriendModel', '', TRUE);
			$this->load->model('MemberModel', '', TRUE);
			$dataPost = json_decode($this->input->raw_input_stream, true);

			//print_r($_POST);
			//print_r($this->input->post()); 
			//echo $this->input->raw_input_stream;  

			//$dateRecord = date("Y-m-d H:i:s"); 
			$PageIndex =  isset($dataPost['PageIndex']) ? $dataPost['PageIndex'] : 1;
			$PageSize =  isset($dataPost['PageSize']) ? $dataPost['PageSize'] : 20;
			$direction =  isset($dataPost['SortColumn']) ? $dataPost['SortColumn'] : "";
			$SortOrder = isset($dataPost['SortOrder']) ? $dataPost['SortOrder'] : "asc";
			$dataModel = isset($dataPost['mSearch']) ? $dataPost['mSearch'] : "";

			$offset = ($PageIndex - 1) * $PageSize;

			$result['status'] = true;
			$data = $this->MemberFriendModel->getMemberFriendNameList($dataModel, $PageSize, $offset, $direction, $SortOrder);

			$dataJson = array();
			$friendname = "";
			foreach($data as $key=>$row){
				$friend = json_decode($row['FRIEND_ID'],true);
				for($i=0;$i< count($friend);$i++){
					$name = $this->MemberModel->getMemberFriendName($friend[$i]['MEMBER_ID']);
					if($i>0){
					$friendname .= ",";
					}
					$friendname .= $name;
					
				}
				$data[$key]['friendname'] = $friendname;
			}
		
			$result['message'] = $data;
			// die();
			$result['totalRecords'] = $this->MemberFriendModel->getTotal($dataModel);
			$result['toTalPage'] = ceil($result['totalRecords'] / $PageSize);

			//$result['message'] = $this->MemberFriendModel->getMemberFriendModel(); 

		} catch (Exception $ex) {
			$result['status'] = false;
			$result['message'] = "exception: " . $ex;
		}

		echo json_encode($result, JSON_UNESCAPED_UNICODE);
	}

	public function getMemberFriendComboList()
	{

		try {
			$this->load->model('MemberFriendModel', '', TRUE);
			$result['status'] = true;
			$result['message'] = $this->MemberFriendModel->getMemberFriendComboList();
		} catch (Exception $ex) {
			$result['status'] = false;
			$result['message'] = "exception: " . $ex;
		}

		echo json_encode($result, JSON_UNESCAPED_UNICODE);
	}
	public function getMemberFriendComboListVat()
	{

		try {
			$this->load->model('MemberFriendModel', '', TRUE);
			$result['status'] = true;
			$result['message'] = $this->MemberFriendModel->getMemberFriendComboListVat();
		} catch (Exception $ex) {
			$result['status'] = false;
			$result['message'] = "exception: " . $ex;
		}

		echo json_encode($result, JSON_UNESCAPED_UNICODE);
	}
	public function uploadProfile()
	{
		$todayFile = date("YmdHis");
		// $config['upload_path']          = base_url('uploads');// $_SERVER["DOCUMENT_ROOT"]. '/uploads/';
		// echo FCPATH;
		$config['upload_path']          = FCPATH . 'upload/profile';
		// $config['upload_path']          = '/Applications/MAMP/htdocs/training/uploads/attendance_list/';
		$config['allowed_types']        = 'jpg|png';
		$config['max_size']             = 1000000;
		$config['max_width']            = 3000;
		$config['max_height']           = 3000;
		$config['file_name']   = $todayFile;


		$this->load->library('upload', $config);
		$this->upload->initialize($config);
		// echo json_encode($config);
		$status = true;

		if (!$this->upload->do_upload('file_name')) {
			$status = false;
			echo json_encode(array('status' => $status,  'message' => $this->upload->display_errors(), 'path' => $config['upload_path']));
		} else {
			$data = array('upload_data' => $this->upload->data());
			// $downloadFile=$todayFile.".".$fileData[1];
			// $this->loadExcelToDB($todayFile."."$fileData[1]);
			echo json_encode(array('status' => $status, 'message' => $data['upload_data']['file_name'], 'path' => $config['upload_path']));
		}
	}
	public function uploadContract()
	{
		$todayFile = date("YmdHis");
		// $config['upload_path']          = base_url('uploads');// $_SERVER["DOCUMENT_ROOT"]. '/uploads/';
		// echo FCPATH;
		$config['upload_path']          = FCPATH . 'upload/contract';
		// $config['upload_path']          = '/Applications/MAMP/htdocs/training/uploads/attendance_list/';
		$config['allowed_types']        = 'pdf|xlsx|docx';
		$config['max_size']             = 1000000;
		$config['max_width']            = 3000;
		$config['max_height']           = 3000;
		$config['file_name']   = $todayFile;


		$this->load->library('upload', $config);
		$this->upload->initialize($config);
		// echo json_encode($config);
		$status = true;

		if (!$this->upload->do_upload('file_name')) {
			$status = false;
			echo json_encode(array('status' => $status,  'message' => $this->upload->display_errors(), 'path' => $config['upload_path']));
		} else {
			$data = array('upload_data' => $this->upload->data());
			// $downloadFile=$todayFile.".".$fileData[1];
			// $this->loadExcelToDB($todayFile."."$fileData[1]);
			echo json_encode(array('status' => $status, 'message' => $data['upload_data']['file_name'], 'path' => $config['upload_path']));
		}
	}
	public function uploadIdcard()
	{
		$todayFile = date("YmdHis");
		// $config['upload_path']          = base_url('uploads');// $_SERVER["DOCUMENT_ROOT"]. '/uploads/';
		// echo FCPATH;
		$config['upload_path']          = FCPATH . 'upload/id_card';
		// $config['upload_path']          = '/Applications/MAMP/htdocs/training/uploads/attendance_list/';
		$config['allowed_types']        = 'jpg|png';
		$config['max_size']             = 1000000;
		$config['max_width']            = 3000;
		$config['max_height']           = 3000;
		$config['file_name']   = $todayFile;


		$this->load->library('upload', $config);
		$this->upload->initialize($config);
		// echo json_encode($config);
		$status = true;

		if (!$this->upload->do_upload('file_name')) {
			$status = false;
			echo json_encode(array('status' => $status,  'message' => $this->upload->display_errors(), 'path' => $config['upload_path']));
		} else {
			$data = array('upload_data' => $this->upload->data());
			// $downloadFile=$todayFile.".".$fileData[1];
			// $this->loadExcelToDB($todayFile."."$fileData[1]);
			echo json_encode(array('status' => $status, 'message' => $data['upload_data']['file_name'], 'path' => $config['upload_path']));
		}
	}
	public function uploadHouseRegis()
	{
		$todayFile = date("YmdHis");
		// $config['upload_path']          = base_url('uploads');// $_SERVER["DOCUMENT_ROOT"]. '/uploads/';
		// echo FCPATH;
		$config['upload_path']          = FCPATH . 'upload/house_registration';
		// $config['upload_path']          = '/Applications/MAMP/htdocs/training/uploads/attendance_list/';
		$config['allowed_types']        = 'jpg|png';
		$config['max_size']             = 1000000;
		$config['max_width']            = 3000;
		$config['max_height']           = 3000;
		$config['file_name']   = $todayFile;


		$this->load->library('upload', $config);
		$this->upload->initialize($config);
		// echo json_encode($config);
		$status = true;

		if (!$this->upload->do_upload('file_name')) {
			$status = false;
			echo json_encode(array('status' => $status,  'message' => $this->upload->display_errors(), 'path' => $config['upload_path']));
		} else {
			$data = array('upload_data' => $this->upload->data());
			// $downloadFile=$todayFile.".".$fileData[1];
			// $this->loadExcelToDB($todayFile."."$fileData[1]);
			echo json_encode(array('status' => $status, 'message' => $data['upload_data']['file_name'], 'path' => $config['upload_path']));
		}
	}
	public function uploadBookbank()
	{
		$todayFile = date("YmdHis");
		// $config['upload_path']          = base_url('uploads');// $_SERVER["DOCUMENT_ROOT"]. '/uploads/';
		// echo FCPATH;
		$config['upload_path']          = FCPATH . 'upload/book_bank';
		// $config['upload_path']          = '/Applications/MAMP/htdocs/training/uploads/attendance_list/';
		$config['allowed_types']        = 'jpg|png';
		$config['max_size']             = 1000000;
		$config['max_width']            = 3000;
		$config['max_height']           = 3000;
		$config['file_name']   = $todayFile;


		$this->load->library('upload', $config);
		$this->upload->initialize($config);
		// echo json_encode($config);
		$status = true;

		if (!$this->upload->do_upload('file_name')) {
			$status = false;
			echo json_encode(array('status' => $status,  'message' => $this->upload->display_errors(), 'path' => $config['upload_path']));
		} else {
			$data = array('upload_data' => $this->upload->data());
			// $downloadFile=$todayFile.".".$fileData[1];
			// $this->loadExcelToDB($todayFile."."$fileData[1]);
			echo json_encode(array('status' => $status, 'message' => $data['upload_data']['file_name'], 'path' => $config['upload_path']));
		}
	}
	public function getFriendMemberList()
	{

		try {
			$dataPost = json_decode($this->input->raw_input_stream, true);
			$this->load->model('MemberFriendModel', '', TRUE);
			// $MEMBER_ID =  isset($dataPost['MEMBER_ID']) ? $dataPost['MEMBER_ID'] : 0;
			$MEMBER_ID =  isset($dataPost['MEMBER_ID']) ? $dataPost['MEMBER_ID'] : 0;

			$sql = "SELECT * FROM ts_relationship WHERE MEMBER_ID = '".$MEMBER_ID."'";
			$res = $this->db->query($sql)->row_array();
			$friendList = json_decode($res['FRIEND_ID'] ,true);

			$rsList = array();
			;
			foreach($friendList as $frd){
				// echo $frd['MEMBER_ID'];die();
				$member = $this->MemberFriendModel->getFriendMemberList($frd['MEMBER_ID']);
				array_push($rsList,$member);
				// print_r($member);die();
			}
			
			// print_r($rsList);die();
			$result['status'] = true;
			$result['message'] = $rsList;
		} catch (Exception $ex) {
			$result['status'] = false;
			$result['message'] = "exception: " . $ex;
		}

		echo json_encode($result, JSON_UNESCAPED_UNICODE);
	}
}
