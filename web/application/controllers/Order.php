<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Order extends CI_Controller {
	public function __construct() {
		parent::__construct();
		if(! $this->session->userdata('validated')){
			redirect('login');
		}
    }
	 
	public function index()
	{
		$this->load->view('share/head');
		$this->load->view('share/sidebar');
		$this->load->view('order/order_view'); 
		$this->load->view('share/footer');
	}
	
	
	public function add() {

	  	try{
			$this->load->model('OrderModel','',TRUE); 
			$dataPost = json_decode( $this->input->raw_input_stream , true);
			$result = $this->OrderModel->add($dataPost);
	  					
    	}catch(Exception $ex){
    		$result['status'] = false;
			$result['message'] = "exception: ".$ex;
    	}
	    
		echo json_encode($result, JSON_UNESCAPED_UNICODE);
    }
	
	public function delete(){ #edit
		try{
			$this->load->model('OrderModel','',TRUE);
			$dataPost = json_decode( $this->input->raw_input_stream , true);
			$result = $this->OrderModel->delete($dataPost);	
			 	
		}catch(Exception $ex){
			$result['status'] = false;
			$result['message'] = "exception: ".$ex;
		}
		
		echo json_encode($result, JSON_UNESCAPED_UNICODE);
	}
	public function undelete(){ #edit
		try{
			$this->load->model('OrderModel','',TRUE);
			$dataPost = json_decode( $this->input->raw_input_stream , true);
			$result = $this->OrderModel->undelete($dataPost);	
			 	
		}catch(Exception $ex){
			$result['status'] = false;
			$result['message'] = "exception: ".$ex;
		}
		
		echo json_encode($result, JSON_UNESCAPED_UNICODE);
	}
	public function getDetail()
	{

		try {
			$this->load->model('OrderModel', '', TRUE);
			$dataPost = json_decode($this->input->raw_input_stream, true);
			$result = $this->OrderModel->getDetail($dataPost);	

		} catch (Exception $ex) {
			$result['status'] = false;
			$result['message'] = "exception: " . $ex;
		}

		echo json_encode($result, JSON_UNESCAPED_UNICODE);
	}
	
	public function getList(){#edit
	 
		try{
			$this->load->model('OrderModel','',TRUE); 
			$dataPost = json_decode( $this->input->raw_input_stream , true);
			$result = $this->OrderModel->getList($dataPost);
		}catch(Exception $ex){
			$result['status'] = false;
			$result['message'] = "exception: ".$ex;
		}
		
		echo json_encode($result, JSON_UNESCAPED_UNICODE);		
	}
	
	public function getComboList(){
	 
		try{ 
			$this->load->model('OrderModel','',TRUE);
			$result = $this->OrderModel->getComboList();
		}catch(Exception $ex){
			$result['status'] = false;
			$result['message'] = "exception: ".$ex;
		}
		
		echo json_encode($result, JSON_UNESCAPED_UNICODE);		
	}
	
	public function upload_file()
    {

        $date = date('Y-m-d-H-i-s');
        $todayfile = 'imgOrder' . '_' . $date;
        $config['upload_path'] = FCPATH . 'upload/Order';
        $config['allowed_types'] = '*';
        $config['max_size'] = 0;
        $config['max_width'] = 0;
        $config['max_height'] = 0;
        $config['file_name'] = $todayfile;
        $file = explode('.', $_FILES['filegeneralinfo']['name']);
        $this->load->library('upload', $config);
        $this->upload->initialize($config);
        $status = true;
        if (!$this->upload->do_upload('filegeneralinfo')) {
            $status = false;
            echo json_encode(array('status' => $status,  'message' => $this->upload->display_errors(), 'path' => $config['upload_path']));
        } else {
			$data = array('upload_data' => $this->upload->data());
			echo json_encode(array('status' => $status, 'message' => $data['upload_data']['file_name'], 'path' => $config['upload_path']));
        }
	}
}
