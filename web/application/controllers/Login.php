<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends CI_Controller {
	public function __construct() {
        parent::__construct();
		$this->load->library('session');
        //$this->lang->load("message","thai");
		
		/*if(!isset($_SESSION['u_user'])){
			redirect('/login', 'refresh');
		}*/
    }
	 
	public function index()
	{ 
		$data['msgError'] = '';
		$username = $this->security->xss_clean($this->input->post('uname'));
        $password = $this->security->xss_clean($this->input->post('pswsss'));
		
		//echo $username. " :  " .  $password. " : " . md5($password);
		
		if($username != '' && $password  != ''){
			
			$this->load->model('UserModel');
			// Validate the user can login
			$result = $this->UserModel->validate($username, $password);
			//echo $result;
			// Now we verify the result
			if(! $result['status']){
				// If user did not validate, then show them login page again
				$data['msgError'] =  $result['message'];
			}else{
				// If user did validate, 
				// Send them to members area
				if($this->session->userdata('user') == "admin"){
					redirect('Business');
				}else{
					redirect('Dashboard');
				}
				
			}  
		}
		
		$this->load->view('login/login_view', $data);
	}
	public function logout()
	{ 
		$data = array(
                    'id' => 0,
                    'user' => "", 
					'user_name' =>  "", 
					'role' =>  0, 
                    'validated' => false
                    );
		$this->session->set_userdata($data);
		
		$this->load->view('login/login_view', $data);
	}
	public function ForgotPass(){
		$this->load->view('login/forgotpass_view');
	}
	public function forgetpassforsentmail()
	{

		try {
			// $this->load->model('MemberModel', '', TRUE);
			$dataPost = json_decode($this->input->raw_input_stream, true);

			$EMAIL =  isset($dataPost['EMAIL']) ? $dataPost['EMAIL'] : "";

			$sql = "SELECT * FROM ma_user WHERE EMAIL = '".$EMAIL."'";
			$res = $this->db->query($sql)->row_array();

			$alphabet = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890';
			$pass = array(); //remember to declare $pass as an array
			$alphaLength = strlen($alphabet) - 1; //put the length -1 in cache
			for ($i = 0; $i < 8; $i++) {
				$n = rand(0, $alphaLength);
				$pass[] = $alphabet[$n];
			}		
			$passnew = implode($pass);

		

			$passsave = md5($passnew);

			$this->db->set('PASSWORD',$passsave );
			$this->db->where('EMAIL',$EMAIL);
			$this->db->update('ma_user');
			if(count($res) > 0){
				$this->load->library('email');
				$config = array(
					'protocol'  => 'smtp',
					'smtp_host' => 'ssl://smtp.googlemail.com',
					'smtp_port' => 465,
					'smtp_user' => 'flookplayzz@gmail.com',
					'smtp_pass' => 'sathaflook1',
					'mailtype'  => 'html',
					'charset'   => 'utf-8'
				);
				
				$this->email->initialize($config);
				$this->email->set_mailtype("html");
				$this->email->set_newline("\r\n");

				$this->email->from('flookplayzz@gmail.com', 'URVR System');
				$subject = 'Forgot password';
				// echo $link_detail,$subject,$email,$user_id;
				$this->email->to($EMAIL);
				$this->email->subject($subject);
				$body = "Password new : ".$passnew ;
				$this->email->message($body);
				// $this->email->send();
				if (!$this->email->send()) {
					echo($this->email->print_debugger());
					$result['status']=false;
				} else {
					$result['status']=true;
				
					
				}
			}else{
				// print_r($dataPost);die();
				$result['status'] = true;
				$result['message'] = "Username หรือ Email ไม่ถูกต้อง";
			}
			
		} catch (Exception $ex) {
			$result['status'] = false;
			$result['message'] = "exception: " . $ex;
		}

		echo json_encode($result, JSON_UNESCAPED_UNICODE);
	}
	function sendMail()
	{
		$config = array(
			'protocol' => 'smtp',
			'smtp_host' => 'ssl://smtp.gmail.com',
			'smtp_port' => 465,
			'smtp_user' => 'flookplayzz@gmail.com',
			'smtp_pass' => 'sathaflook1',
			'mailtype' => 'html',
			'charset' => 'iso-8859-1'
		);

		$message = '';
		$this->load->library('email');
		$this->email->initialize($config); 
		$this->email->set_newline("\r\n");
		$this->email->from('flookplayzz@gmail.com'); // change it to yours
		$this->email->to('flooknattapol1@hotmail.com');// change it to yours
		$this->email->subject('Resume from JobsBuddy for your Job posting');
		$this->email->message($message);
		if($this->email->send())
		{
		echo 'Email sent.';
		}
		else
		{
			echo($this->email->print_debugger());
		}

	}
	
}
