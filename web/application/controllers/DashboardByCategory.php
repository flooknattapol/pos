<?php
defined('BASEPATH') or exit('No direct script access allowed');

use \setasign\Fpdi\Fpdi;

class DashboardByCategory extends CI_Controller
{
	public function __construct()
	{
		date_default_timezone_set('Asia/Bangkok');
		parent::__construct();
		if (!$this->session->userdata('validated')) {
			redirect('login');
		}
	}

	public function index()
	{
		
		//QRcode::png('PHP QR Code :)'); die();

		$this->load->view('share/head');
		$this->load->view('share/sidebar');
		$this->load->view('dashboardbycategory/dashboardbycategory_view');
		$this->load->view('share/footer');
	}
	public function getDashboardByCategoryAlll()
	{
		try {
			$this->load->model('DashboardByCategoryModel', '', TRUE);
			$dataPost = json_decode($this->input->raw_input_stream, true);
			$id =  isset($dataPost['ID']) ? $dataPost['ID'] : 0; // $this->input->post('ap_id');

			$data['SummaryToThis'] = $this->DashboardByCategoryModel->GetSummaryToThis();
			$data['CountToregister'] = $this->DashboardByCategoryModel->GetToregister($data['SummaryToThis']['ID']);
			$data['CountVendor'] = $this->DashboardByCategoryModel->GetVendor($data['SummaryToThis']['ID']);

			if($id > 0){
				$m_id = $id;
			}else{
				$m_id = $data['SummaryToThis']['ID'];
			}
			$data['TotalShopSale'] =  $this->DashboardByCategoryModel->GetTotalShopSale($m_id);
			$data['PercentTypeGoods'] =  $this->DashboardByCategoryModel->GetPercentTypeGoods($m_id);
			$result['message'] = $data;
			$result['status'] = true;
				

		} catch (Exception $ex) {
			$result['status'] = false;
			$result['message'] = "exception: " . $ex;
		}

		echo json_encode($result, JSON_UNESCAPED_UNICODE);
	}
	
}
