<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Businesscategory extends CI_Controller
{
	public function __construct()
	{
		date_default_timezone_set('Asia/Bangkok');
		parent::__construct();
		if (!$this->session->userdata('validated')) {
			redirect('login');
		}
	}

	public function index()
	{
		$this->load->view('share/head');
		$this->load->view('share/sidebar');
		$this->load->view('businesscategory/businesscategory_view');
		$this->load->view('share/footer');
	}

	
	public function add()
	{
		try {
			$this->load->model('BusinesscategoryModel', '', TRUE);
			$dataPost = json_decode($this->input->raw_input_stream, true);
			$result = 	$this->BusinesscategoryModel->add($dataPost );
		} catch (Exception $ex) {
			$result['status'] = false;
			$result['message'] = "exception: " . $ex;
		}

		echo json_encode($result, JSON_UNESCAPED_UNICODE);
	}

	public function delete()
	{
		
		try {
			$this->load->model('BusinesscategoryModel', '', TRUE);
			$dataPost = json_decode($this->input->raw_input_stream, true);
			$result = 	$this->BusinesscategoryModel->delete($dataPost );
		} catch (Exception $ex) {
			$result['status'] = false;
			$result['message'] = "exception: " . $ex;
		}

		echo json_encode($result, JSON_UNESCAPED_UNICODE);
	}

	public function getList()
	{
		try {
			$this->load->model('BusinesscategoryModel', '', TRUE);
			$dataPost = json_decode($this->input->raw_input_stream, true);
			$result = 	$this->BusinesscategoryModel->getList($dataPost);
		} catch (Exception $ex) {
			$result['status'] = false;
			$result['message'] = "exception: " . $ex;
		}

		echo json_encode($result, JSON_UNESCAPED_UNICODE);
	}

	public function getComboList()
	{

		try {
			$this->load->model('BusinesscategoryModel', '', TRUE);
			$dataPost = json_decode($this->input->raw_input_stream, true);
			$result = 	$this->BusinesscategoryModel->getComboList($dataPost);
		} catch (Exception $ex) {
			$result['status'] = false;
			$result['message'] = "exception: " . $ex;
		}

		echo json_encode($result, JSON_UNESCAPED_UNICODE);
	}
}
