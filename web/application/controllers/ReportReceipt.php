<?php
defined('BASEPATH') or exit('No direct script access allowed');

use \setasign\Fpdi\Fpdi;

class ReportReceipt extends CI_Controller
{
	public function __construct()
	{
		date_default_timezone_set('Asia/Bangkok');
		parent::__construct();
		if (!$this->session->userdata('validated')) {
			redirect('login');
		}
	}

	public function index()
	{
		
		//QRcode::png('PHP QR Code :)'); die();
		$this->load->view('share/head');
		$this->load->view('share/sidebar');
		$this->load->view('reportreceipt/reportreceipt_view');
		$this->load->view('share/footer');
	}
	public function getList(){
	 
		try{
			$this->load->model('TransectionModel','',TRUE); 
			$dataPost = json_decode( $this->input->raw_input_stream , true);

			$result = $this->TransectionModel->getList($dataPost);
		}catch(Exception $ex){
			$result['status'] = false;
			$result['message'] = "exception: ".$ex;
		}
		
		echo json_encode($result, JSON_UNESCAPED_UNICODE);		
	}
	public function getDetail(){
	 
		try{
			$this->load->model('TransectionDetailModel','',TRUE); 
			$dataPost = json_decode( $this->input->raw_input_stream , true);

			$result = $this->TransectionDetailModel->getListDetail($dataPost);
		}catch(Exception $ex){
			$result['status'] = false;
			$result['message'] = "exception: ".$ex;
		}
		
		echo json_encode($result, JSON_UNESCAPED_UNICODE);		
	}
}