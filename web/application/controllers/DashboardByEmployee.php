<?php
defined('BASEPATH') or exit('No direct script access allowed');

use \setasign\Fpdi\Fpdi;

class DashboardByEmployee extends CI_Controller
{
	public function __construct()
	{
		date_default_timezone_set('Asia/Bangkok');
		parent::__construct();
		if (!$this->session->userdata('validated')) {
			redirect('login');
		}
	}

	public function index()
	{
		
		//QRcode::png('PHP QR Code :)'); die();

		$this->load->view('share/head');
		$this->load->view('share/sidebar');
		$this->load->view('dashboardbyemployee/dashboardbyemployee_view');
		$this->load->view('share/footer');
	}
	public function getDashboardByEmployeeAlll()
	{
		try {
			$this->load->model('DashboardByEmployeeModel', '', TRUE);
			$dataPost = json_decode($this->input->raw_input_stream, true);
			$id =  isset($dataPost['ID']) ? $dataPost['ID'] : 0; // $this->input->post('ap_id');

			$data['SummaryToThis'] = $this->DashboardByEmployeeModel->GetSummaryToThis();
			$data['CountToregister'] = $this->DashboardByEmployeeModel->GetToregister($data['SummaryToThis']['ID']);
			$data['CountVendor'] = $this->DashboardByEmployeeModel->GetVendor($data['SummaryToThis']['ID']);

			if($id > 0){
				$m_id = $id;
			}else{
				$m_id = $data['SummaryToThis']['ID'];
			}
			$data['TotalShopSale'] =  $this->DashboardByEmployeeModel->GetTotalShopSale($m_id);
			$data['PercentTypeGoods'] =  $this->DashboardByEmployeeModel->GetPercentTypeGoods($m_id);
			$result['message'] = $data;
			$result['status'] = true;
				

		} catch (Exception $ex) {
			$result['status'] = false;
			$result['message'] = "exception: " . $ex;
		}

		echo json_encode($result, JSON_UNESCAPED_UNICODE);
	}
	
}
