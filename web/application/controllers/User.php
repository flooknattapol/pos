<?php
defined('BASEPATH') OR exit('No direct script access allowed');
use  setasign\Fpdi;
class User extends CI_Controller {
	public function __construct() {
        parent::__construct();
		if(! $this->session->userdata('validated')){
            redirect('login');
        }
    }
	 
	public function index()
	{
		$this->load->view('share/head');
		$this->load->view('share/sidebar');
		$this->load->view('user/user_view'); 
		$this->load->view('share/footer');
	}
	
	public function add()
	{
		try {
			$this->load->model('UserModel', '', TRUE);
			$dataPost = json_decode($this->input->raw_input_stream, true);
			$result = 	$this->UserModel->add($dataPost );
		} catch (Exception $ex) {
			$result['status'] = false;
			$result['message'] = "exception: " . $ex;
		}

		echo json_encode($result, JSON_UNESCAPED_UNICODE);
	}
	public function getDetail()
	{

		try {
			$this->load->model('UserPositionModel', '', TRUE);
			$dataPost = json_decode($this->input->raw_input_stream, true);
			$result = $this->UserPositionModel->getDetail($dataPost);	

		} catch (Exception $ex) {
			$result['status'] = false;
			$result['message'] = "exception: " . $ex;
		}

		echo json_encode($result, JSON_UNESCAPED_UNICODE);
	}
	public function delete()
	{
		try {
			$this->load->model('UserModel', '', TRUE);
			$dataPost = json_decode($this->input->raw_input_stream, true);
			$result = 	$this->UserModel->delete($dataPost );
		} catch (Exception $ex) {
			$result['status'] = false;
			$result['message'] = "exception: " . $ex;
		}

		echo json_encode($result, JSON_UNESCAPED_UNICODE);
	}
	public function undelete()
	{
		try {
			$this->load->model('UserModel', '', TRUE);
			$dataPost = json_decode($this->input->raw_input_stream, true);
			$result = 	$this->UserModel->undelete($dataPost );
		} catch (Exception $ex) {
			$result['status'] = false;
			$result['message'] = "exception: " . $ex;
		}

		echo json_encode($result, JSON_UNESCAPED_UNICODE);
	}
	public function getList()
	{
		try {
			$this->load->model('UserModel', '', TRUE);
			$dataPost = json_decode($this->input->raw_input_stream, true);
			$result = 	$this->UserModel->getList($dataPost);
		} catch (Exception $ex) {
			$result['status'] = false;
			$result['message'] = "exception: " . $ex;
		}

		echo json_encode($result, JSON_UNESCAPED_UNICODE);
	}

	public function getComboList()
	{

		try {
			$this->load->model('UserModel', '', TRUE);
			$dataPost = json_decode($this->input->raw_input_stream, true);
			$result = 	$this->UserModel->getComboList($dataPost);
		} catch (Exception $ex) {
			$result['status'] = false;
			$result['message'] = "exception: " . $ex;
		}

		echo json_encode($result, JSON_UNESCAPED_UNICODE);
	}
	
}
