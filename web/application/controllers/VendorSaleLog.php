<?php
defined('BASEPATH') or exit('No direct script access allowed');

class VendorSaleLog extends CI_Controller
{
	public function __construct()
	{
		date_default_timezone_set('Asia/Bangkok');
		parent::__construct();
		if (!$this->session->userdata('validated')) {
			redirect('login');
		}
	}

	public function index()
	{
		$this->load->view('share/head');
		$this->load->view('share/sidebar');
		$this->load->view('vendorsalelog/vendorsalelog_view');
		$this->load->view('share/footer');
	}

	public function addVendorSaleLog()
	{
		// $this->output->set_content_type('application/json');
		$nResult = 0;

		try {

			$this->load->model('VendorSaleLogModel', '', TRUE);

			$dataPost = json_decode($this->input->raw_input_stream, true);

			/*print_r($_POST);
			print_r($this->input->post()); 
			echo $this->input->raw_input_stream;*/

			//$dateRecord = date("Y-m-d H:i:s"); 
			$data['id'] =  isset($dataPost['id']) ? $dataPost['id'] : 0;
			$data['name'] =  isset($dataPost['name']) ? $dataPost['name'] : "";
			$data['username'] = isset($dataPost['username']) ? $dataPost['username'] : "";
			$data['line_user_id'] = isset($dataPost['line_user_id']) ? $dataPost['line_user_id'] : "";
			$data['user_type'] = isset($dataPost['user_type']) ? $dataPost['user_type'] : "local";
			
		
			if ($data['id'] == 0) {
				$nResult = $this->VendorSaleLogModel->insert($data);
			} else {
				$nResult = $this->VendorSaleLogModel->update($data['id'], $data);
			}

			if ($nResult > 0) {
				$result['status'] = true;
				$result['message'] = $this->lang->line("savesuccess");
			
			} else {
				$result['status'] = false;
				$result['message'] = $this->lang->line("error");
			}
		} catch (Exception $ex) {
			$result['status'] = false;
			$result['message'] = "exception: " . $ex;
		}

		echo json_encode($result, JSON_UNESCAPED_UNICODE);
	}

	public function deleteVendorSaleLog()
	{
		try {
			$this->load->model('VendorSaleLogModel', '', TRUE);
			$dataPost = json_decode($this->input->raw_input_stream, true);
			$id =  isset($dataPost['id']) ? $dataPost['id'] : 0; // $this->input->post('ap_id');

			$bResult = $this->VendorSaleLogModel->deleteVendorSaleLogname($id);

			if ($bResult) {
				$result['status'] = true;
				$result['message'] = $this->lang->line("savesuccess");
			} else {
				$result['status'] = false;
				$result['message'] = $this->lang->line("error_faliure");
			}
		} catch (Exception $ex) {
			$result['status'] = false;
			$result['message'] = "exception: " . $ex;
		}

		echo json_encode($result, JSON_UNESCAPED_UNICODE);
	}

	public function getVendorSaleLogModelList()
	{

		try {
			$this->load->model('VendorSaleLogModel', '', TRUE);

			$data['m_id'] =  $this->input->post('mab_m_id');
			
			$limit =  $this->input->post('limit');
			$offset =  $this->input->post('offset');
			$order = $this->input->post('order');
			$direction = $this->input->post('direction');
			 
			$result = $this->VendorSaleLogModel->getVendorSaleLogNameAllList($data, $limit , $offset, $order, $direction);

			$result['status'] = true;
			$result['message'] = $this->lang->line("savesuccess");
		} catch (Exception $ex) {
			$result['status'] = false;
			$result['message'] = "exception: " . $ex;
		}

		echo json_encode($result, JSON_UNESCAPED_UNICODE);
	}


	public function getVendorSaleLogModel()
	{

		try {
			$this->load->model('VendorSaleLogModel', '', TRUE);
			$dataPost = json_decode($this->input->raw_input_stream, true);

			//print_r($_POST);
			//print_r($this->input->post()); 
			//echo $this->input->raw_input_stream;  

			//$dateRecord = date("Y-m-d H:i:s"); 
			$PageIndex =  isset($dataPost['PageIndex']) ? $dataPost['PageIndex'] : 1;
			$PageSize =  isset($dataPost['PageSize']) ? $dataPost['PageSize'] : 20;
			$direction =  isset($dataPost['SortColumn']) ? $dataPost['SortColumn'] : "";
			$SortOrder = isset($dataPost['SortOrder']) ? $dataPost['SortOrder'] : "asc";
			$dataModel = isset($dataPost['mSearch']) ? $dataPost['mSearch'] : "";

			$offset = ($PageIndex - 1) * $PageSize;

			$result['status'] = true;
			$result['message'] = $this->VendorSaleLogModel->getVendorSaleLogNameList($dataModel, $PageSize, $offset, $direction, $SortOrder);
			$result['totalRecords'] = $this->VendorSaleLogModel->getTotal($dataModel);
			$result['toTalPage'] = ceil($result['totalRecords'] / $PageSize);

			//$result['message'] = $this->VendorSaleLogModel->getVendorSaleLogModel(); 

		} catch (Exception $ex) {
			$result['status'] = false;
			$result['message'] = "exception: " . $ex;
		}

		echo json_encode($result, JSON_UNESCAPED_UNICODE);
	}

	public function getVendorSaleLogComboList()
	{

		try {
			$this->load->model('VendorSaleLogModel', '', TRUE);
			$result['status'] = true;
			$result['message'] = $this->VendorSaleLogModel->getVendorSaleLogComboList();
		} catch (Exception $ex) {
			$result['status'] = false;
			$result['message'] = "exception: " . $ex;
		}

		echo json_encode($result, JSON_UNESCAPED_UNICODE);
	}
	public function getVendorSaleLogComboListVat()
	{

		try {
			$this->load->model('VendorSaleLogModel', '', TRUE);
			$result['status'] = true;
			$result['message'] = $this->VendorSaleLogModel->getVendorSaleLogComboListVat();
		} catch (Exception $ex) {
			$result['status'] = false;
			$result['message'] = "exception: " . $ex;
		}

		echo json_encode($result, JSON_UNESCAPED_UNICODE);
	}
	public function uploadProfile()
	{
		$todayFile = date("YmdHis");
		// $config['upload_path']          = base_url('uploads');// $_SERVER["DOCUMENT_ROOT"]. '/uploads/';
		// echo FCPATH;
		$config['upload_path']          = FCPATH . 'upload/profile';
		// $config['upload_path']          = '/Applications/MAMP/htdocs/training/uploads/attendance_list/';
		$config['allowed_types']        = 'jpg|png';
		$config['max_size']             = 1000000;
		$config['max_width']            = 3000;
		$config['max_height']           = 3000;
		$config['file_name']   = $todayFile;


		$this->load->library('upload', $config);
		$this->upload->initialize($config);
		// echo json_encode($config);
		$status = true;

		if (!$this->upload->do_upload('file_name')) {
			$status = false;
			echo json_encode(array('status' => $status,  'message' => $this->upload->display_errors(), 'path' => $config['upload_path']));
		} else {
			$data = array('upload_data' => $this->upload->data());
			// $downloadFile=$todayFile.".".$fileData[1];
			// $this->loadExcelToDB($todayFile."."$fileData[1]);
			echo json_encode(array('status' => $status, 'message' => $data['upload_data']['file_name'], 'path' => $config['upload_path']));
		}
	}
	public function uploadContract()
	{
		$todayFile = date("YmdHis");
		// $config['upload_path']          = base_url('uploads');// $_SERVER["DOCUMENT_ROOT"]. '/uploads/';
		// echo FCPATH;
		$config['upload_path']          = FCPATH . 'upload/contract';
		// $config['upload_path']          = '/Applications/MAMP/htdocs/training/uploads/attendance_list/';
		$config['allowed_types']        = 'pdf|xlsx|docx';
		$config['max_size']             = 1000000;
		$config['max_width']            = 3000;
		$config['max_height']           = 3000;
		$config['file_name']   = $todayFile;


		$this->load->library('upload', $config);
		$this->upload->initialize($config);
		// echo json_encode($config);
		$status = true;

		if (!$this->upload->do_upload('file_name')) {
			$status = false;
			echo json_encode(array('status' => $status,  'message' => $this->upload->display_errors(), 'path' => $config['upload_path']));
		} else {
			$data = array('upload_data' => $this->upload->data());
			// $downloadFile=$todayFile.".".$fileData[1];
			// $this->loadExcelToDB($todayFile."."$fileData[1]);
			echo json_encode(array('status' => $status, 'message' => $data['upload_data']['file_name'], 'path' => $config['upload_path']));
		}
	}
	public function uploadIdcard()
	{
		$todayFile = date("YmdHis");
		// $config['upload_path']          = base_url('uploads');// $_SERVER["DOCUMENT_ROOT"]. '/uploads/';
		// echo FCPATH;
		$config['upload_path']          = FCPATH . 'upload/id_card';
		// $config['upload_path']          = '/Applications/MAMP/htdocs/training/uploads/attendance_list/';
		$config['allowed_types']        = 'jpg|png';
		$config['max_size']             = 1000000;
		$config['max_width']            = 3000;
		$config['max_height']           = 3000;
		$config['file_name']   = $todayFile;


		$this->load->library('upload', $config);
		$this->upload->initialize($config);
		// echo json_encode($config);
		$status = true;

		if (!$this->upload->do_upload('file_name')) {
			$status = false;
			echo json_encode(array('status' => $status,  'message' => $this->upload->display_errors(), 'path' => $config['upload_path']));
		} else {
			$data = array('upload_data' => $this->upload->data());
			// $downloadFile=$todayFile.".".$fileData[1];
			// $this->loadExcelToDB($todayFile."."$fileData[1]);
			echo json_encode(array('status' => $status, 'message' => $data['upload_data']['file_name'], 'path' => $config['upload_path']));
		}
	}
	public function uploadHouseRegis()
	{
		$todayFile = date("YmdHis");
		// $config['upload_path']          = base_url('uploads');// $_SERVER["DOCUMENT_ROOT"]. '/uploads/';
		// echo FCPATH;
		$config['upload_path']          = FCPATH . 'upload/house_registration';
		// $config['upload_path']          = '/Applications/MAMP/htdocs/training/uploads/attendance_list/';
		$config['allowed_types']        = 'jpg|png';
		$config['max_size']             = 1000000;
		$config['max_width']            = 3000;
		$config['max_height']           = 3000;
		$config['file_name']   = $todayFile;


		$this->load->library('upload', $config);
		$this->upload->initialize($config);
		// echo json_encode($config);
		$status = true;

		if (!$this->upload->do_upload('file_name')) {
			$status = false;
			echo json_encode(array('status' => $status,  'message' => $this->upload->display_errors(), 'path' => $config['upload_path']));
		} else {
			$data = array('upload_data' => $this->upload->data());
			// $downloadFile=$todayFile.".".$fileData[1];
			// $this->loadExcelToDB($todayFile."."$fileData[1]);
			echo json_encode(array('status' => $status, 'message' => $data['upload_data']['file_name'], 'path' => $config['upload_path']));
		}
	}
	public function uploadBookbank()
	{
		$todayFile = date("YmdHis");
		// $config['upload_path']          = base_url('uploads');// $_SERVER["DOCUMENT_ROOT"]. '/uploads/';
		// echo FCPATH;
		$config['upload_path']          = FCPATH . 'upload/book_bank';
		// $config['upload_path']          = '/Applications/MAMP/htdocs/training/uploads/attendance_list/';
		$config['allowed_types']        = 'jpg|png';
		$config['max_size']             = 1000000;
		$config['max_width']            = 3000;
		$config['max_height']           = 3000;
		$config['file_name']   = $todayFile;


		$this->load->library('upload', $config);
		$this->upload->initialize($config);
		// echo json_encode($config);
		$status = true;

		if (!$this->upload->do_upload('file_name')) {
			$status = false;
			echo json_encode(array('status' => $status,  'message' => $this->upload->display_errors(), 'path' => $config['upload_path']));
		} else {
			$data = array('upload_data' => $this->upload->data());
			// $downloadFile=$todayFile.".".$fileData[1];
			// $this->loadExcelToDB($todayFile."."$fileData[1]);
			echo json_encode(array('status' => $status, 'message' => $data['upload_data']['file_name'], 'path' => $config['upload_path']));
		}
	}
}
