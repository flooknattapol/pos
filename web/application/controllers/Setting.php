<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Setting extends CI_Controller
{
	public function __construct()
	{
		date_default_timezone_set('Asia/Bangkok');
		parent::__construct();
		if (!$this->session->userdata('validated')) {
			redirect('login');
		}
	}

	public function index()
	{
		$this->load->view('share/head');
		$this->load->view('share/sidebar');
		$this->load->view('setting/setting_view');
		$this->load->view('share/footer');
	}

	public function get(){
	 
		try{
			$this->load->model('SettingModel','',TRUE); 
			$dataPost = json_decode( $this->input->raw_input_stream , true);
			$result = $this->SettingModel->get($dataPost);
		}catch(Exception $ex){
			$result['status'] = false;
			$result['message'] = "exception: ".$ex;
		}
		
		echo json_encode($result, JSON_UNESCAPED_UNICODE);		
	}
	public function add() {

		try{
			$this->load->model('SettingModel','',TRUE); 
			$dataPost = json_decode( $this->input->raw_input_stream , true);
			$result = $this->SettingModel->add($dataPost);
						
		}catch(Exception $ex){
			$result['status'] = false;
			$result['message'] = "exception: ".$ex;
		}
		
		echo json_encode($result, JSON_UNESCAPED_UNICODE);
	}
}
