<?php
defined('BASEPATH') or exit('No direct script access allowed');

use \setasign\Fpdi\Fpdi;

class Dashboard extends CI_Controller
{
	public function __construct()
	{
		date_default_timezone_set('Asia/Bangkok');
		parent::__construct();
		if (!$this->session->userdata('validated')) {
			redirect('login');
		}
	}

	public function index()
	{
		
		//QRcode::png('PHP QR Code :)'); die();

		$this->load->view('share/head');
		$this->load->view('share/sidebar');
		$this->load->view('dashboard/dashboard_view');
		$this->load->view('share/footer');
	}
	
	public function getHeader(){
	 
		try{
			$this->load->model('DashboardModel','',TRUE); 
			$dataPost = json_decode( $this->input->raw_input_stream , true);

			$result = $this->DashboardModel->getHeader($dataPost);
		}catch(Exception $ex){
			$result['status'] = false;
			$result['message'] = "exception: ".$ex;
		}
		
		echo json_encode($result, JSON_UNESCAPED_UNICODE);		
	}
	public function getGraph(){
	 
		try{
			$this->load->model('DashboardModel','',TRUE); 
			$dataPost = json_decode( $this->input->raw_input_stream , true);

			$result = $this->DashboardModel->getGraph($dataPost);
		}catch(Exception $ex){
			$result['status'] = false;
			$result['message'] = "exception: ".$ex;
		}
		
		echo json_encode($result, JSON_UNESCAPED_UNICODE);		
	}
	
}
