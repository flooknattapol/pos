<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Customercategory extends CI_Controller
{
	public function __construct()
	{
		date_default_timezone_set('Asia/Bangkok');
		parent::__construct();
		if (!$this->session->userdata('validated')) {
			redirect('login');
		}
	}

	public function index()
	{
		$this->load->view('share/head');
		$this->load->view('share/sidebar');
		$this->load->view('customercategory/customercategory_view');
		$this->load->view('share/footer');
	}

	
	public function add() {

		try{
		  $this->load->model('CustomercategoryModel','',TRUE); 
		  $dataPost = json_decode( $this->input->raw_input_stream , true);
		  $result = $this->CustomercategoryModel->add($dataPost);
						
	  }catch(Exception $ex){
		  $result['status'] = false;
		  $result['message'] = "exception: ".$ex;
	  }
	  
	  echo json_encode($result, JSON_UNESCAPED_UNICODE);
  	}

  	public function delete(){ #edit
	  try{
		  $this->load->model('CustomercategoryModel','',TRUE);
		  $dataPost = json_decode( $this->input->raw_input_stream , true);
		  $result = $this->CustomercategoryModel->delete($dataPost);	
			   
	  }catch(Exception $ex){
		  $result['status'] = false;
		  $result['message'] = "exception: ".$ex;
	  }
	  
	  echo json_encode($result, JSON_UNESCAPED_UNICODE);
  	}
  	public function getList(){#edit
	 
	try{
		$this->load->model('CustomercategoryModel','',TRUE); 
		$dataPost = json_decode( $this->input->raw_input_stream , true);
		$result = $this->CustomercategoryModel->getList($dataPost);
	}catch(Exception $ex){
		$result['status'] = false;
		$result['message'] = "exception: ".$ex;
	}
	
	echo json_encode($result, JSON_UNESCAPED_UNICODE);		
	}
	public function getComboList(){
	 
		try{ 
			$this->load->model('CustomercategoryModel','',TRUE);
			$dataPost = json_decode( $this->input->raw_input_stream , true);
			$result = $this->CustomercategoryModel->getComboList($dataPost);
		}catch(Exception $ex){
			$result['status'] = false;
			$result['message'] = "exception: ".$ex;
		}
		
		echo json_encode($result, JSON_UNESCAPED_UNICODE);		
	}
}
