<?php
defined('BASEPATH') OR exit('No direct script access allowed');
// header('Access-Control-Allow-Origin:*');
use  setasign\Fpdi;
class Api extends CI_Controller {
	public function __construct() {
		date_default_timezone_set('Asia/Bangkok');
		parent::__construct();
		header('Access-Control-Allow-Origin: *');
		header("Access-Control-Allow-Headers: X-API-KEY, Origin, X-Requested-With, Content-Type, Accept, Access-Control-Request-Method");
		header("Access-Control-Allow-Methods: GET, POST, PUT, DELETE");
		$authen = $this->input->get('authen');
		$version = $this->input->get('version');
		if($authen != 'POS2020DEVDEETHAILAND.COM'){
			$result['status'] = false;
			$result['code'] = '101';
			$result['message'] = 'Please input token key or token is mistake';
			echo json_encode($result,JSON_UNESCAPED_UNICODE);
			exit();
		}
		if($version != '20.05.19.002'){
			$result['status'] = false;
			$result['code'] = '102';
			$result['message'] = "Please update version! [Current Version : $version]";
			echo json_encode($result,JSON_UNESCAPED_UNICODE);
			exit();
		}
		
	}
	
	public function Login()
	{
		try {
			$dataStr = $this->input->get('JsonData');
			$dataPost = json_decode($dataStr, true);

			$this->load->model('UserModel', '', TRUE);
			$result = $this->UserModel->validate($dataPost['EMAIL'],$dataPost['PASSWORD']);

		} catch (Exception $ex) {
			$result['status'] = false;
			$result['message'] = "exception: " . $ex;
		}

		echo json_encode($result, JSON_UNESCAPED_UNICODE);
	}
	public function getListGoods(){
	 
		try{
			$this->load->model('GoodsModel','',TRUE); 
			$dataStr = $this->input->get('JsonData');
			$dataPost = json_decode($dataStr, true);
			$result = $this->GoodsModel->getList($dataPost);
		}catch(Exception $ex){
			$result['status'] = false;
			$result['message'] = "exception: ".$ex;
		}
		
		echo json_encode($result, JSON_UNESCAPED_UNICODE);		
	}
	public function getGoodsByAdmin(){
	 
		try{
			$this->load->model('GoodsModel','',TRUE); 
			$dataStr = $this->input->get('JsonData');
			$dataPost = json_decode($dataStr, true);
			$result = $this->GoodsModel->getListByAdmin($dataPost);
		}catch(Exception $ex){
			$result['status'] = false;
			$result['message'] = "exception: ".$ex;
		}
		
		echo json_encode($result, JSON_UNESCAPED_UNICODE);		
	}
	public function getListEmployee(){
	 
		try{
			$this->load->model('EmployeeModel','',TRUE); 
			$dataStr = $this->input->get('JsonData');
			$dataPost = json_decode($dataStr, true);
			$result = $this->EmployeeModel->getList($dataPost);
		}catch(Exception $ex){
			$result['status'] = false;
			$result['message'] = "exception: ".$ex;
		}
		
		echo json_encode($result, JSON_UNESCAPED_UNICODE);		
	}
	public function getComboListGoods(){
	 
		try{
			$this->load->model('GoodscategoryModel','',TRUE); 
			$dataStr = $this->input->get('JsonData');
			$dataPost = json_decode($dataStr, true);
			$result = $this->GoodscategoryModel->getComboList($dataPost);
		}catch(Exception $ex){
			$result['status'] = false;
			$result['message'] = "exception: ".$ex;
		}
		
		echo json_encode($result, JSON_UNESCAPED_UNICODE);		
	}
	public function getComboListBranch(){
	 
		try{
			$this->load->model('BranchModel','',TRUE); 
			$dataStr = $this->input->get('JsonData');
			$dataPost = json_decode($dataStr, true);
			$result = $this->BranchModel->getComboList($dataPost);
		}catch(Exception $ex){
			$result['status'] = false;
			$result['message'] = "exception: ".$ex;
		}
		
		echo json_encode($result, JSON_UNESCAPED_UNICODE);		
	}
	public function getComboListBusinessCategory(){
	 
		try{
			$this->load->model('BusinesscategoryModel','',TRUE); 
			$dataStr = $this->input->get('JsonData');
			$dataPost = json_decode($dataStr, true);
			$result = $this->BusinesscategoryModel->getComboList($dataPost);
		}catch(Exception $ex){
			$result['status'] = false;
			$result['message'] = "exception: ".$ex;
		}
		
		echo json_encode($result, JSON_UNESCAPED_UNICODE);		
	}
	public function getComboListBusinessType(){
	 
		try{
			$this->load->model('BusinessTypeModel','',TRUE); 
			$dataStr = $this->input->get('JsonData');
			$dataPost = json_decode($dataStr, true);
			$result = $this->BusinessTypeModel->getComboList($dataPost);
		}catch(Exception $ex){
			$result['status'] = false;
			$result['message'] = "exception: ".$ex;
		}
		
		echo json_encode($result, JSON_UNESCAPED_UNICODE);		
	}
	public function getComboListRole(){
	 
		try{
			$this->load->model('RoleModel','',TRUE); 
			$dataStr = $this->input->get('JsonData');
			$dataPost = json_decode($dataStr, true);
			$result = $this->RoleModel->getComboList($dataPost);
		}catch(Exception $ex){
			$result['status'] = false;
			$result['message'] = "exception: ".$ex;
		}
		
		echo json_encode($result, JSON_UNESCAPED_UNICODE);		
	}
	public function saveGoods(){
	 
		try{
			$this->load->model('GoodsModel','',TRUE); 
			$dataStr = $this->input->get('JsonData');
			$dataPost = json_decode($dataStr, true);
			$result = $this->GoodsModel->add($dataPost);
		}catch(Exception $ex){
			$result['status'] = false;
			$result['message'] = "exception: ".$ex;
		}
		
		echo json_encode($result, JSON_UNESCAPED_UNICODE);		
	}
	public function saveEmployee(){
	 
		try{
			$this->load->model('EmployeeModel','',TRUE); 
			$dataStr = $this->input->get('JsonData');
			$dataPost = json_decode($dataStr, true);
			$result = $this->EmployeeModel->add($dataPost);
		}catch(Exception $ex){
			$result['status'] = false;
			$result['message'] = "exception: ".$ex;
		}
		
		echo json_encode($result, JSON_UNESCAPED_UNICODE);		
	}
	public function saveGoodscategory(){
	 
		try{
			$this->load->model('GoodscategoryModel','',TRUE); 
			$dataStr = $this->input->get('JsonData');
			$dataPost = json_decode($dataStr, true);
			$result = $this->GoodscategoryModel->add($dataPost);
		}catch(Exception $ex){
			$result['status'] = false;
			$result['message'] = "exception: ".$ex;
		}
		
		echo json_encode($result, JSON_UNESCAPED_UNICODE);		
	}
	public function adddiscount(){
	 
		try{
			$this->load->model('DiscountModel','',TRUE); 
			$dataStr = $this->input->get('JsonData');
			$dataPost = json_decode($dataStr, true);
			$result = $this->DiscountModel->add($dataPost);
		}catch(Exception $ex){
			$result['status'] = false;
			$result['message'] = "exception: ".$ex;
		}
		
		echo json_encode($result, JSON_UNESCAPED_UNICODE);		
	}
	public function getListDiscount(){
	 
		try{
			$this->load->model('DiscountModel','',TRUE); 
			$dataStr = $this->input->get('JsonData');
			$dataPost = json_decode($dataStr, true);
			$result = $this->DiscountModel->getList($dataPost);
		}catch(Exception $ex){
			$result['status'] = false;
			$result['message'] = "exception: ".$ex;
		}
		
		echo json_encode($result, JSON_UNESCAPED_UNICODE);		
	}
	public function getListDiscountCampaign(){
	 
		try{
			$this->load->model('DiscountModel','',TRUE); 
			$dataStr = $this->input->get('JsonData');
			$dataPost = json_decode($dataStr, true);
			$result = $this->DiscountModel->getListCampaign($dataPost);
		}catch(Exception $ex){
			$result['status'] = false;
			$result['message'] = "exception: ".$ex;
		}
		
		echo json_encode($result, JSON_UNESCAPED_UNICODE);		
	}
	public function getListTransection(){
	 
		try{
			$this->load->model('TransectionModel','',TRUE); 
			$dataStr = $this->input->get('JsonData');
			$dataPost = json_decode($dataStr, true);
			$result = $this->TransectionModel->getList($dataPost);
		}catch(Exception $ex){
			$result['status'] = false;
			$result['message'] = "exception: ".$ex;
		}
		
		echo json_encode($result, JSON_UNESCAPED_UNICODE);		
	}
	public function getListTempOrder(){
	 
		try{
			$this->load->model('TransectionModel','',TRUE); 
			$dataStr = $this->input->get('JsonData');
			$dataPost = json_decode($dataStr, true);
			$result = $this->TransectionModel->getListTempOrder($dataPost);
		}catch(Exception $ex){
			$result['status'] = false;
			$result['message'] = "exception: ".$ex;
		}
		
		echo json_encode($result, JSON_UNESCAPED_UNICODE);		
	}
	public function getListDetailTransection(){
	 
		try{
			$this->load->model('TransectionDetailModel','',TRUE); 
			$dataStr = $this->input->get('JsonData');
			$dataPost = json_decode($dataStr, true);
			$result = $this->TransectionDetailModel->getListDetail($dataPost);
		}catch(Exception $ex){
			$result['status'] = false;
			$result['message'] = "exception: ".$ex;
		}
		
		echo json_encode($result, JSON_UNESCAPED_UNICODE);		
	}
	public function getListCustomer(){
	 
		try{
			$this->load->model('CustomerModel','',TRUE); 
			$dataStr = $this->input->get('JsonData');
			$dataPost = json_decode($dataStr, true);
			$result = $this->CustomerModel->getList($dataPost);
		}catch(Exception $ex){
			$result['status'] = false;
			$result['message'] = "exception: ".$ex;
		}
		
		echo json_encode($result, JSON_UNESCAPED_UNICODE);		
	}
	public function addCustomer(){
	 
		try{
			$dataPost = json_decode( $this->input->raw_input_stream , true);
			$this->load->model('CustomerModel','',TRUE); 
			// $dataStr = $this->input->post('JsonData');
			// $dataPost = json_decode($dataStr, true);
			$result = $this->CustomerModel->add($dataPost);
		}catch(Exception $ex){
			$result['status'] = false;
			$result['message'] = "exception: ".$ex;
		}
		
		echo json_encode($result, JSON_UNESCAPED_UNICODE);		
	}
	public function addCustomerGet(){
	 
		try{
			$this->load->model('CustomerModel','',TRUE); 
			$dataStr = $this->input->get('JsonData');
			$dataPost = json_decode($dataStr, true);
			$result = $this->CustomerModel->add($dataPost);
		}catch(Exception $ex){
			$result['status'] = false;
			$result['message'] = "exception: ".$ex;
		}
		
		echo json_encode($result, JSON_UNESCAPED_UNICODE);		
	}
	public function addCustomerToBusiness(){
	 
		try{
			$this->load->model('CustomerModel','',TRUE); 
			$dataStr = $this->input->get('JsonData');
			$dataPost = json_decode($dataStr, true);
			$result = $this->CustomerModel->addtobusiness($dataPost);
		}catch(Exception $ex){
			$result['status'] = false;
			$result['message'] = "exception: ".$ex;
		}
		
		echo json_encode($result, JSON_UNESCAPED_UNICODE);		
	}
	public function getComboList(){
	 
		try{ 
			$dataStr = $this->input->get('JsonData');
			$dataPost = json_decode($dataStr, true);
			$this->load->model('CustomercategoryModel','',TRUE);
			$result = $this->CustomercategoryModel->getComboList($dataPost);
		}catch(Exception $ex){
			$result['status'] = false;
			$result['message'] = "exception: ".$ex;
		}
		
		echo json_encode($result, JSON_UNESCAPED_UNICODE);		
	}
	public function addTransection(){
		try{
			$this->load->model('TransectionModel','',TRUE); 
			$dataStr = $this->input->get('JsonData');
			$dataPost = json_decode($dataStr, true);
			$result = $this->TransectionModel->add($dataPost);
		}catch(Exception $ex){
			$result['status'] = false;
			$result['message'] = "exception: ".$ex;
		}
		
		echo json_encode($result, JSON_UNESCAPED_UNICODE);		
	}
	public function addTempOrder(){
		try{
			$this->load->model('TransectionModel','',TRUE); 
			$dataStr = $this->input->get('JsonData');
			$dataPost = json_decode($dataStr, true);
			$result = $this->TransectionModel->addTempOrder($dataPost);
		}catch(Exception $ex){
			$result['status'] = false;
			$result['message'] = "exception: ".$ex;
		}
		
		echo json_encode($result, JSON_UNESCAPED_UNICODE);		
	}
	public function Forgetpassworduser(){
		try{
			$this->load->model('UserModel','',TRUE); 
			$dataStr = $this->input->get('JsonData');
			$dataPost = json_decode($dataStr, true);
			$result = $this->UserModel->ForgetPass($dataPost);
		}catch(Exception $ex){
			$result['status'] = false;
			$result['message'] = "exception: ".$ex;
		}
		
		echo json_encode($result, JSON_UNESCAPED_UNICODE);		
	}
	public function Changepassworduser(){
		try{
			$this->load->model('UserModel','',TRUE); 
			$dataStr = $this->input->get('JsonData');
			$dataPost = json_decode($dataStr, true);
			$result = $this->UserModel->ChangePass($dataPost);
		}catch(Exception $ex){
			$result['status'] = false;
			$result['message'] = "exception: ".$ex;
		}
		
		echo json_encode($result, JSON_UNESCAPED_UNICODE);		
	}
	public function addbusiness(){
		try{
			$this->load->model('BusinessModel','',TRUE); 
			$dataStr = $this->input->get('JsonData');
			$dataPost = json_decode($dataStr, true);
			$result = $this->BusinessModel->add($dataPost);
		}catch(Exception $ex){
			$result['status'] = false;
			$result['message'] = "exception: ".$ex;
		}
		
		echo json_encode($result, JSON_UNESCAPED_UNICODE);		
	}
	public function deleteTransection(){
		try{
			$this->load->model('TransectionModel','',TRUE); 
			$dataStr = $this->input->get('JsonData');
			$dataPost = json_decode($dataStr, true);
			$result = $this->TransectionModel->delete($dataPost);
		}catch(Exception $ex){
			$result['status'] = false;
			$result['message'] = "exception: ".$ex;
		}
		
		echo json_encode($result, JSON_UNESCAPED_UNICODE);		
	}
	public function getTempOrder(){
		try{
			$this->load->model('TransectionModel','',TRUE); 
			$dataStr = $this->input->get('JsonData');
			$dataPost = json_decode($dataStr, true);
			$result = $this->TransectionModel->getTempOrder($dataPost);
		}catch(Exception $ex){
			$result['status'] = false;
			$result['message'] = "exception: ".$ex;
		}
		
		echo json_encode($result, JSON_UNESCAPED_UNICODE);		
	}
	public function deletetemporder(){
		try{
			$this->load->model('TransectionModel','',TRUE); 
			$dataStr = $this->input->get('JsonData');
			$dataPost = json_decode($dataStr, true);
			$result = $this->TransectionModel->deletetemporder($dataPost);
		}catch(Exception $ex){
			$result['status'] = false;
			$result['message'] = "exception: ".$ex;
		}
		
		echo json_encode($result, JSON_UNESCAPED_UNICODE);		
	}
	public function deleteDetailTransection(){
		try{
			$this->load->model('TransectionDetailModel','',TRUE); 
			$dataStr = $this->input->get('JsonData');
			$dataPost = json_decode($dataStr, true);
			$result = $this->TransectionDetailModel->delete($dataPost);
		}catch(Exception $ex){
			$result['status'] = false;
			$result['message'] = "exception: ".$ex;
		}
		
		echo json_encode($result, JSON_UNESCAPED_UNICODE);		
	}
	public function getGraphDashboard(){
		try{
			$this->load->model('DashboardModel','',TRUE); 
			$dataStr = $this->input->get('JsonData');
			$dataPost = json_decode($dataStr, true);
			$result = $this->DashboardModel->getGraph($dataPost);
		}catch(Exception $ex){
			$result['status'] = false;
			$result['message'] = "exception: ".$ex;
		}
		
		echo json_encode($result, JSON_UNESCAPED_UNICODE);		
	}
	public function getHeaderDashboard(){
		try{
			$this->load->model('DashboardModel','',TRUE); 
			$dataStr = $this->input->get('JsonData');
			$dataPost = json_decode($dataStr, true);
			$result = $this->DashboardModel->getHeader($dataPost);
		}catch(Exception $ex){
			$result['status'] = false;
			$result['message'] = "exception: ".$ex;
		}
		
		echo json_encode($result, JSON_UNESCAPED_UNICODE);		
	}
	public function getHeaderDashboardbygoods(){
		try{
			$this->load->model('DashboardByGoodsModel','',TRUE); 
			$dataStr = $this->input->get('JsonData');
			$dataPost = json_decode($dataStr, true);
			$result = $this->DashboardByGoodsModel->getHeader($dataPost);
		}catch(Exception $ex){
			$result['status'] = false;
			$result['message'] = "exception: ".$ex;
		}
		
		echo json_encode($result, JSON_UNESCAPED_UNICODE);		
	}
	public function getGraphDashboardbygoods(){
		try{
			$this->load->model('DashboardByGoodsModel','',TRUE); 
			$dataStr = $this->input->get('JsonData');
			$dataPost = json_decode($dataStr, true);
			$result = $this->DashboardByGoodsModel->getList($dataPost);
		}catch(Exception $ex){
			$result['status'] = false;
			$result['message'] = "exception: ".$ex;
		}
		
		echo json_encode($result, JSON_UNESCAPED_UNICODE);		
	}
	public function getAllBusiness(){
	 
		try{
			$this->load->model('BusinessModel','',TRUE); 
			$dataStr = $this->input->get('JsonData');
			$dataPost = json_decode($dataStr, true);
			$result = $this->BusinessModel->getList($dataPost);
		}catch(Exception $ex){
			$result['status'] = false;
			$result['message'] = "exception: ".$ex;
		}
		
		echo json_encode($result, JSON_UNESCAPED_UNICODE);		
	}
	public function getBusinessCode(){
	 
		try{
			$this->load->model('BusinessCodeModel','',TRUE); 
			$dataStr = $this->input->get('JsonData');
			$dataPost = json_decode($dataStr, true);
			$result = $this->BusinessCodeModel->getList($dataPost);
		}catch(Exception $ex){
			$result['status'] = false;
			$result['message'] = "exception: ".$ex;
		}
		
		echo json_encode($result, JSON_UNESCAPED_UNICODE);		
	}
	public function saveBusinessCode(){
	 
		try{
			$this->load->model('BusinessCodeModel','',TRUE); 
			$dataStr = $this->input->get('JsonData');
			$dataPost = json_decode($dataStr, true);
			$result = $this->BusinessCodeModel->add($dataPost);
		}catch(Exception $ex){
			$result['status'] = false;
			$result['message'] = "exception: ".$ex;
		}
		
		echo json_encode($result, JSON_UNESCAPED_UNICODE);		
	}
	public function renewbusiness(){
	 
		try{
			$this->load->model('BusinessModel','',TRUE); 
			$dataStr = $this->input->get('JsonData');
			$dataPost = json_decode($dataStr, true);
			$result = $this->BusinessModel->renewbusiness($dataPost);
		}catch(Exception $ex){
			$result['status'] = false;
			$result['message'] = "exception: ".$ex;
		}
		
		echo json_encode($result, JSON_UNESCAPED_UNICODE);		
	}
	public function getBusinessSetting(){
	 
		try{
			$this->load->model('SettingModel','',TRUE); 
			$dataStr = $this->input->get('JsonData');
			$dataPost = json_decode($dataStr, true);
			$result = $this->SettingModel->get($dataPost);
		}catch(Exception $ex){
			$result['status'] = false;
			$result['message'] = "exception: ".$ex;
		}
		
		echo json_encode($result, JSON_UNESCAPED_UNICODE);		
	}
	public function saveBusinessSetting(){
	 
		try{
			$this->load->model('SettingModel','',TRUE); 
			$dataStr = $this->input->get('JsonData');
			$dataPost = json_decode($dataStr, true);
			$result = $this->SettingModel->add($dataPost);
		}catch(Exception $ex){
			$result['status'] = false;
			$result['message'] = "exception: ".$ex;
		}
		
		echo json_encode($result, JSON_UNESCAPED_UNICODE);		
	}
	public function getListRelation(){
	 
		try{
			$this->load->model('DiscountModel','',TRUE); 
			$dataStr = $this->input->get('JsonData');
			$dataPost = json_decode($dataStr, true);
			$result = $this->DiscountModel->getListRelation($dataPost);
		}catch(Exception $ex){
			$result['status'] = false;
			$result['message'] = "exception: ".$ex;
		}
		
		echo json_encode($result, JSON_UNESCAPED_UNICODE);		
	}
	public function getListVoucher(){
	 
		try{
			$dataPost = json_decode( $this->input->raw_input_stream , true);
			$this->load->model('DiscountModel','',TRUE); 

			$result = $this->DiscountModel->getListVoucher($dataPost);
		}catch(Exception $ex){
			$result['status'] = false;
			$result['message'] = "exception: ".$ex;
		}
		
		echo json_encode($result, JSON_UNESCAPED_UNICODE);		
	}
	public function addDiscountRelation(){
	 
		try{
			$dataPost = json_decode( $this->input->raw_input_stream , true);
			$this->load->model('DiscountModel','',TRUE); 
			$result = $this->DiscountModel->addDiscountRelation($dataPost);
		}catch(Exception $ex){
			$result['status'] = false;
			$result['message'] = "exception: ".$ex;
		}
		
		echo json_encode($result, JSON_UNESCAPED_UNICODE);		
	}
	public function getListVoucherInCustomer(){
	 
		try{
			$dataPost = json_decode( $this->input->raw_input_stream , true);
			$this->load->model('DiscountModel','',TRUE); 
			$result = $this->DiscountModel->getListVoucherInCustomer($dataPost);
		}catch(Exception $ex){
			$result['status'] = false;
			$result['message'] = "exception: ".$ex;
		}
		
		echo json_encode($result, JSON_UNESCAPED_UNICODE);		
	}
	public function getListPointReward(){
	 
		try{
			$dataPost = json_decode( $this->input->raw_input_stream , true);
			$this->load->model('DiscountModel','',TRUE); 
			$result = $this->DiscountModel->getListPointReward($dataPost);
		}catch(Exception $ex){
			$result['status'] = false;
			$result['message'] = "exception: ".$ex;
		}
		
		echo json_encode($result, JSON_UNESCAPED_UNICODE);		
	}
}
