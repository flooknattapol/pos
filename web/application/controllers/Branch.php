<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Branch extends CI_Controller {
	public function __construct() {
		parent::__construct();
		if(! $this->session->userdata('validated')){
			redirect('login');
		}
    }
	 
	public function index()
	{
		$this->load->view('share/head');
		$this->load->view('share/sidebar');
		$this->load->view('Branch/Branch_view'); 
		$this->load->view('share/footer');
	}
	
	
	public function add() {

	  	try{
			$this->load->model('BranchModel','',TRUE); 
			$dataPost = json_decode( $this->input->raw_input_stream , true);
			$result = $this->BranchModel->add($dataPost);
	  					
    	}catch(Exception $ex){
    		$result['status'] = false;
			$result['message'] = "exception: ".$ex;
    	}
	    
		echo json_encode($result, JSON_UNESCAPED_UNICODE);
    }
	
	public function delete(){ #edit
		try{
			$this->load->model('BranchModel','',TRUE);
			$dataPost = json_decode( $this->input->raw_input_stream , true);
			$result = $this->BranchModel->delete($dataPost);	
			 	
		}catch(Exception $ex){
			$result['status'] = false;
			$result['message'] = "exception: ".$ex;
		}
		
		echo json_encode($result, JSON_UNESCAPED_UNICODE);
	}
	public function undelete(){ #edit
		try{
			$this->load->model('BranchModel','',TRUE);
			$dataPost = json_decode( $this->input->raw_input_stream , true);
			$result = $this->BranchModel->undelete($dataPost);	
			 	
		}catch(Exception $ex){
			$result['status'] = false;
			$result['message'] = "exception: ".$ex;
		}
		
		echo json_encode($result, JSON_UNESCAPED_UNICODE);
	}
	public function getDetail()
	{

		try {
			$this->load->model('BranchModel', '', TRUE);
			$dataPost = json_decode($this->input->raw_input_stream, true);
			$result = $this->BranchModel->getDetail($dataPost);	

		} catch (Exception $ex) {
			$result['status'] = false;
			$result['message'] = "exception: " . $ex;
		}

		echo json_encode($result, JSON_UNESCAPED_UNICODE);
	}
	
	public function getList(){#edit
	 
		try{
			$this->load->model('BranchModel','',TRUE); 
			$dataPost = json_decode( $this->input->raw_input_stream , true);
			$result = $this->BranchModel->getList($dataPost);
		}catch(Exception $ex){
			$result['status'] = false;
			$result['message'] = "exception: ".$ex;
		}
		
		echo json_encode($result, JSON_UNESCAPED_UNICODE);		
	}
	
	public function getComboList(){
	 
		try{ 
			$this->load->model('BranchModel','',TRUE);
			$dataPost = json_decode($this->input->raw_input_stream, true);
			$result = $this->BranchModel->getComboList($dataPost);
		}catch(Exception $ex){
			$result['status'] = false;
			$result['message'] = "exception: ".$ex;
		}
		
		echo json_encode($result, JSON_UNESCAPED_UNICODE);		
	}
	
	
}
