<?php
 
class LanguageLoader
 
{
 
   function initialize() {
 
       $ci =& get_instance();
 
       $ci->load->helper('language');
	  
	   $default_lang = 'thai';//'english';//'thai';
       $siteLang = $ci->session->userdata('site_lang');
    //    print_r($ci->session->userdata())  ;
       if ($siteLang) {
 
           $ci->lang->load('information',$siteLang);
 
       } else {
		   
           $ci->lang->load('information', $default_lang);
 
       }
 
   }
 
}