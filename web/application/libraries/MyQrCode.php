<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

require_once APPPATH."/third_party/phpqrcode/qrlib.php";

// outputs image directly into browser, as PNG stream

class MyQrCode  {
    public function __construct() {
        //parent::__construct();
    }

    public function SaveQRCode($codeContents){

        //echo "Save QRCode"; die();
         // how to save PNG codes to server  
         
        $tempDir =  $_SERVER["DOCUMENT_ROOT"]."/urvr/web/upload/qrcode/";
        // $tempDir =  "https://www.devdeethailand.com/urvr/web/upload/qrcode/";
        if(!is_dir($tempDir)){
            mkdir($tempDir);
        }
        // $tempDir =  base_url('/upload/');
        $protocol = stripos($_SERVER['SERVER_PROTOCOL'],'https') === 0 ? 'https://' : 'http://';
        // $protocol = 'https://';
        // echo $protocol;die();
        $url = $protocol .$_SERVER['HTTP_HOST']."/urvr/web/upload/qrcode/";
        // $url = "https://www.devdeethailand.com/urvr/web/upload/qrcode/";
        //EXAMPLE_TMP_SERVERPATH; // $codeContents = 'This Goes From File';
        
        // we need to generate filename somehow, 
        // with md5 or with database ID used to obtains $codeContents...
        $fileName = 'qr_code'.md5($codeContents).'.png';
        
        $pngAbsoluteFilePath = $tempDir.$fileName;
        $urlRelativeFilePath = $url.$fileName;
        try{
              // generating
            if (!file_exists($pngAbsoluteFilePath)) {
                QRcode::png($codeContents, $pngAbsoluteFilePath, QR_ECLEVEL_L, 4);
                //echo 'File generated!';
                //echo '<hr />';
            }

        } catch (Exception $e) {
            echo 'Caught exception: ',  $e->getMessage(), "\n";
        }
      
        //echo "<br/>".$pngAbsoluteFilePath ;
        //echo  "<br/>".$urlRelativeFilePath;
        return $urlRelativeFilePath;
      
    }


}