<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

require_once APPPATH."/third_party/phpqrcode/qrlib.php";

// outputs image directly into browser, as PNG stream

class MyLog  {
    public function __construct() {
        //parent::__construct();
    }

    public function SaveLog($message){

        //$tempDir =  $_SERVER["DOCUMENT_ROOT"]."/urvr/web/upload/log/";
        $tempDir =  $_SERVER["DOCUMENT_ROOT"]."/upload/log/";
        if(!is_dir($tempDir)){
            mkdir($tempDir);
        }
        $fileName =date("Ymd").'.log';
        
        $fileAbsoluteFilePath = $tempDir.$fileName;
        //echo $fileAbsoluteFilePath;
        try{


            $myfile = fopen($fileAbsoluteFilePath, "a") or die("Unable to open file!");
            $txt = date("Y/m/d H:i:m")." => ".$message . "\r\n";
            fwrite($myfile, $txt);
            fclose($myfile);
             
            return true;
        } catch (Exception $e) {
          //  echo 'Caught exception: ',  $e->getMessage(), "\n";

            return false;
        }
      
      
    }


}