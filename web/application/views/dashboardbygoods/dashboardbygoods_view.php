<script src="<?php echo base_url('asset/dashboardbygoodsController.js'); ?>"></script>
<script src="<?php echo base_url('dist/Chart.min.js'); ?>"></script>
<script src="<?php echo base_url('utils.js'); ?>"></script>
<script src="<?php echo base_url('chartjs-plugin-datalabels.js'); ?>"></script>
<div ng-controller="dashboardbygoodsController" ng-init="onInit()">

    <div class="row">
        <ul class="navigator">
            <?php /*<li class="nav"><a href="/Rooms"><?php echo $this->lang->line('event');?></a></li>*/ ?>
            <li class="nav_active">สรุปยอดขาย</li>
        </ul>
        <!-- /.col-lg-12 -->
    </div>

    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">สรุปยอดขาย</h1>
        </div>
        <!-- /.col-lg-12 -->
    </div>

    
    <!-- /List.row types-->
    <div class="row DisplayDevice">
        <div class="col-lg-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    สรุปยอดขาย
                </div>
                <div class="panel-body">
                <div class="col-lg-12 col-md-12 col-xs-12">
                        <div class="col-lg-3 col-md-3 col-xs-3">
                            <small>วันที่เริ่มต้น</small>
                            <input  class="form-control"ng-model="modelSearch.DateStart" data-date-format="dd-MM-yyyy" bs-datepicker>
                        </div>
                        <div class="col-lg-3 col-md-3 col-xs-3">
                            <small>วันที่สิ้นสุด</small>
                            <input  class="form-control"ng-model="modelSearch.DateEnd" data-date-format="dd-MM-yyyy" bs-datepicker>
                        </div>
                        <div class="col-lg-3 col-md-3 col-xs-3"><br>
                            <button class="btn btn-block btn-primary" ng-click="reload()">ค้นหา</button>
                        </div>
                    </div>
                    <div class="col-lg-12 col-md-12 col-xs-12">
                        <hr>
                    </div>
                    <div class="col-lg-12 col-md-12 col-xs-12">
                        <div class="col-lg-6 col-md-6 col-xs-12">
                            <div class="col-lg-12 col-md-12 col-xs-12">
                                <div class="table-responsive">
                                    <table class="table table-striped">
                                        <thead>
                                            <tr>
                                                <th><b>สินค้าขายดี 5 อันดับแรก</b></th>
                                                <th style="text-align:right">รายได้รวม</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr ng-repeat="item in GoodsTop5 ">
                                                <td ng-bind="item.GOODS_NAME">ขนมปัง</td>
                                                <td ng-bind="item.TotalGoods"style="text-align:right">100.00</td>
                                            </tr>
                                            
                                        </tbody>
                                    </table>
                                </div>
                                <!-- /.table-responsive -->
                            </div>
                            
                        </div>
                        <div class="col-lg-6 col-md-6 col-xs-12">
                            <div style="width:100%">
                                <canvas id="canvas"></canvas>
                            </div>	
                        </div> 					
                    </div>
                    <div class="col-lg-12 col-md-12 col-xs-12">
                        <hr>
                        <button class="btn btn-success"><i class="glyphicon glyphicon-print"></i> พิมพ์รายการ</button>
                    </div>
                    <div class="col-lg-12 col-md-12 col-xs-12"></hr>
                        <h3>รายการขาย</h3>
                    </div>
                    <div class="col-lg-12 col-md-12 col-xs-12">
                        <div class="table-responsive">
                            <table class="table table-striped">
                                <thead>
                                    <tr>
                                        <th width="10%"class="sorting" ng-click="sort($event)" sort="Date">วันที่</th>
                                        <th width="15%"class="sorting" ng-click="sort($event)" sort="NAME">ประเภท</th>
                                        <th width="15%"class="sorting" ng-click="sort($event)" sort="GOODS_NAME">สินค้าที่ขาย</th>
                                        <th width="15%"class="sorting" ng-click="sort($event)" sort="TotalGoodsPrice">ยอดขายสุทธิ</th>
                                        <th width="15%"class="sorting" ng-click="sort($event)" sort="TotalCOST">ต้นทุนของสินค้า</th>
                                        <th width="15%">กำไรรวม</th>
                                        <th width="15%">สถานะ</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr ng-repeat="item in modelDeviceList">
                                        <td ng-bind="item.Date"></td>
                                        <td ng-bind="item.NAME"></td>
                                        <td ng-bind="item.GOODS_NAME"></td>
                                        <td ng-bind="item.TotalGoodsPrice"></td>
                                        <td ng-bind="item.TotalCOST"></td>
                                        <td>{{item.TotalGoodsPrice-item.TotalCOST}}</td>
                                        <td><b ng-show="item.IsActive == 0" style="color:red">รายการคืน</b></td>
                                    </tr>  
                                </tbody>
                            </table>
                        </div>
                        <!-- /.table-responsive -->
                    </div>
                    <!-- ทำหน้า -->
                    <div class="row tblResult small">
                        <div class="col-md-7 col-sm-7 col-xs-12 ">
                            <label class="col-md-4 col-sm-4 col-xs-12">
                                <?php echo $this->lang->line('Total'); ?> {{totalRecords}} <?php echo $this->lang->line('Records'); ?>
                            </label>
                            <label class="col-md-4 col-sm-4 col-xs-12">
                                <?php echo $this->lang->line('ResultsPerPage'); ?>
                            </label>
                            <div class="col-md-4 col-sm-4 col-xs-12 ">
                                <ui-select ng-model="TempPageSize.selected" ng-change="loadByPageSize()" theme="selectize">
                                    <ui-select-match>{{$select.selected.Value}}</ui-select-match>
                                    <ui-select-choices repeat="pSize in listPageSize | filter: $select.search">
                                        <span ng-bind-html="pSize.Text | highlight: $select.search"></span>
                                    </ui-select-choices>
                                </ui-select>
                            </div>
                        </div>
                        <div class="col-md-5 col-sm-5 col-xs-12  ">
                            <label class="col-md-4 col-sm-4 col-xs-12">
                                <span ng-click="getBackPage()" class="set-pointer"><i class="fa fa-chevron-left"></i> <span class="hidden-xs"><?php echo $this->lang->line('Previous'); ?></span></span>
                            </label>
                            <div class="col-md-3 col-sm-3 col-xs-12">
                                <ui-select ng-model="TempPageIndex.selected" ng-change="searchByPage()" theme="selectize">
                                    <ui-select-match>{{$select.selected.PageIndex}}</ui-select-match>
                                    <ui-select-choices repeat="pIndex in listPageIndex | filter: $select.search">
                                        <span ng-bind-html="pIndex.PageIndex | highlight: $select.search"></span>
                                    </ui-select-choices>
                                </ui-select>
                            </div>
                            <label class="col-md-4 col-sm-4 col-xs-12">
                                / {{ totalPage }} <span ng-click="getNextPage()" class="set-pointer"><?php echo $this->lang->line('Next'); ?><i class="fa fa-chevron-right set-pointer"></i></span>
                            </label>
                        </div>
                    </div>
                    <!-- ทำหน้า -->
                   
                </div>
                <!-- /.panel-body -->
            </div>
            <!-- /.panel -->
        </div>
        <!-- /.col-lg-12 -->
    </div>
    <!-- /List.row types-->
</div>
</div>