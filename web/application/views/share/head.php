<?php
$this->load->helper('url');
?>

<!DOCTYPE html>
<html lang="en">

<head>
	
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="description" content="">
	<meta name="author" content="">
	<title>POS</title>
	<!-- <link rel = "icon" href = "<?php echo base_url('/theme/image/logo_urvr.jpg');?>" type = "image/x-icon">  -->

	<!-- Bootstrap Core CSS -->
	<link href="<?php echo base_url('theme/bootstrap/css/bootstrap.min.css'); ?>" rel="stylesheet">

	<!-- MetisMenu CSS -->
	<link href="<?php echo base_url('theme/metisMenu/metisMenu.min.css'); ?>" rel="stylesheet">

	<!-- Custom CSS -->
	<link href="<?php echo base_url('theme/dist/css/sb-admin-2.css'); ?>" rel="stylesheet">

	<!-- Morris Charts CSS -->
	<link href="<?php echo base_url('theme/morrisjs/morris.css'); ?>" rel="stylesheet">

	<!-- Custom Fonts -->
	<link href="<?php echo base_url('theme/font-awesome/css/font-awesome.min.css'); ?>" rel="stylesheet" type="text/css">

	<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
	<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
	<!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

	<!-- Angular strap -->
	<link href="<?php echo base_url('theme/ng/css/angular-strap.css'); ?>" rel="stylesheet" type="text/css">
	<link href="<?php echo base_url('theme/ng/css/select.css'); ?>" rel="stylesheet" type="text/css">
	<link href="<?php echo base_url('theme/ng/css/selectize.css'); ?>" rel="stylesheet" type="text/css">
	<link rel="stylesheet" href="http://cdnjs.cloudflare.com/ajax/libs/select2/3.4.5/select2.css">
	<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-select@1.13.9/dist/css/bootstrap-select.min.css">
	<!-- jQuery -->
	<script src="<?php echo base_url('theme/jquery/jquery.min.js'); ?>"></script>

	<!-- Bootstrap Core JavaScript -->
	<script src="<?php echo base_url('theme/bootstrap/js/bootstrap.min.js'); ?>"></script>

	<!-- Metis Menu Plugin JavaScript -->
	<script src="<?php echo base_url('theme/metisMenu/metisMenu.min.js'); ?>"></script>

	<!-- Morris Charts JavaScript -->
	<script src="<?php echo base_url('theme/raphael/raphael.min.js'); ?>"></script>
	<script src="<?php echo base_url('theme/morrisjs/morris.min.js'); ?>"></script>

	<!-- Custom Theme JavaScript -->
	<script src="<?php echo base_url('theme/dist/js/sb-admin-2.js'); ?>"></script>

	

	<script src="<?php echo base_url('/dist/js/bootstrap-select.min.js'); ?>"></script>
	<!-- Angular js -->
	<script src="<?php echo base_url('theme/ng/angular.js'); ?>"></script>
	<script src="<?php echo base_url('theme/ng/Utility.js'); ?>"></script>
	<script src="<?php echo base_url('theme/ng/app.js'); ?>"></script>
	<script src="<?php echo base_url('theme/ng/json-formatter.js'); ?>"></script>
	<script src="<?php echo base_url('theme/ng/angular-resource.js'); ?>"></script>
	<script src="<?php echo base_url('theme/ng/ui-bootstrap-tpls-2.5.0.js'); ?>"></script>
	<script src="<?php echo base_url('theme/ng/angular-sanitize.js'); ?>"></script>
	<script src="<?php echo base_url('theme/ng/select.js'); ?>"></script>
	<script src="<?php echo base_url('theme/ng/ng-file-upload.js'); ?>"></script>
	<script src="<?php echo base_url('theme/ng/angular-strap.js'); ?>"></script>
	


	<!-- Service js --->
	<script src="<?php echo base_url('theme/ng/baseService.js'); ?>"></script>
	<script src="<?php echo base_url('theme/ng/systemApiService.js'); ?>"></script>
	<script src="<?php echo base_url('theme/ng/eventApiService.js'); ?>"></script>
	<script src="<?php echo base_url('theme/ng/eventmemberApiService.js'); ?>"></script>
	<script src="<?php echo base_url('theme/ng/memberApiService.js'); ?>"></script>
	<script src="<?php echo base_url('theme/ng/goodsApiService.js'); ?>"></script>
	<script src="<?php echo base_url('theme/ng/voucherlogApiService.js'); ?>"></script>
	<script src="<?php echo base_url('theme/ng/vendorgoodsApiService.js'); ?>"></script>
	<script src="<?php echo base_url('theme/ng/vendorsalelogApiService.js'); ?>"></script>
	<script src="<?php echo base_url('theme/ng/memberregisterlogApiService.js'); ?>"></script>
	<script src="<?php echo base_url('theme/ng/relationstampApiService.js'); ?>"></script>
	<script src="<?php echo base_url('theme/ng/memberfriendApiService.js'); ?>"></script>
	<script src="<?php echo base_url('theme/ng/userApiService.js'); ?>"></script>
	<script src="<?php echo base_url('theme/ng/vendorApiService.js'); ?>"></script>
	<script src="<?php echo base_url('theme/ng/dashboardApiService.js'); ?>"></script>
	<script src="<?php echo base_url('theme/ng/goodstypeApiService.js'); ?>"></script>

	<script src="<?php echo base_url('theme/ng/branchApiService.js'); ?>"></script>
	<script src="<?php echo base_url('theme/ng/businesstypeApiService.js'); ?>"></script>
	<script src="<?php echo base_url('theme/ng/businesscodeApiService.js'); ?>"></script>
	<script src="<?php echo base_url('theme/ng/businesscategoryApiService.js'); ?>"></script>
	<script src="<?php echo base_url('theme/ng/businessApiService.js'); ?>"></script>
	<script src="<?php echo base_url('theme/ng/goodscategoryApiService.js'); ?>"></script>
	<script src="<?php echo base_url('theme/ng/employeeApiService.js'); ?>"></script>
	<script src="<?php echo base_url('theme/ng/roleApiService.js'); ?>"></script>
	<script src="<?php echo base_url('theme/ng/customerApiService.js'); ?>"></script>
	<script src="<?php echo base_url('theme/ng/discountApiService.js'); ?>"></script>
	<script src="<?php echo base_url('theme/ng/orderApiService.js'); ?>"></script>
	<script src="<?php echo base_url('theme/ng/createadjustmentApiService.js'); ?>"></script>

	<script src="<?php echo base_url('theme/ng/customercategoryApiService.js'); ?>"></script>

	<script src="<?php echo base_url('theme/ng/settingApiService.js'); ?>"></script>
	<script src="<?php echo base_url('theme/ng/dashboardbygoodsApiService.js'); ?>"></script>
	<script src="<?php echo base_url('theme/ng/dashboardbycategoryApiService.js'); ?>"></script>
	<script src="<?php echo base_url('theme/ng/dashboardbyemployeeApiService.js'); ?>"></script>
	<script src="<?php echo base_url('theme/ng/reportreceiptApiService.js'); ?>"></script>
	<script src="<?php echo base_url('theme/ng/reportSaleApiService.js'); ?>"></script>


	<script type="text/javascript">
		function get_base_url(url) {
			return <?php echo "'" . base_url() . "'"; ?> + url;
		}
	</script>
	<style>
		#overlay {
			background: rgba(0, 0, 0, 0.4);
			width: 100%;
			height: 100%;
			min-height: 100%;
			position: fixed;
			top: 0;
			left: 0;
			z-index: 10000;
		}

		img.overlay {
			left: 0;
			right: 0;
			top: 0;
			bottom: 0;
			margin: auto;
			position: absolute;
			max-height: 100px;
		}

		.panel.with-nav-tabs .panel-heading {
			padding: 5px 5px 0 5px;
		}

		.panel.with-nav-tabs .nav-tabs {
			border-bottom: none;
		}

		.panel.with-nav-tabs .nav-justified {
			margin-bottom: -1px;
		}

		/********************************************************************/
		/*** PANEL DEFAULT ***/
		.with-nav-tabs.panel-default .nav-tabs>li>a,
		.with-nav-tabs.panel-default .nav-tabs>li>a:hover,
		.with-nav-tabs.panel-default .nav-tabs>li>a:focus {
			color: #777;
		}

		.with-nav-tabs.panel-default .nav-tabs>.open>a,
		.with-nav-tabs.panel-default .nav-tabs>.open>a:hover,
		.with-nav-tabs.panel-default .nav-tabs>.open>a:focus,
		.with-nav-tabs.panel-default .nav-tabs>li>a:hover,
		.with-nav-tabs.panel-default .nav-tabs>li>a:focus {
			color: #777;
			background-color: #ddd;
			border-color: transparent;
		}

		.with-nav-tabs.panel-default .nav-tabs>li.active>a,
		.with-nav-tabs.panel-default .nav-tabs>li.active>a:hover,
		.with-nav-tabs.panel-default .nav-tabs>li.active>a:focus {
			color: #555;
			background-color: #fff;
			border-color: #ddd;
			border-bottom-color: transparent;
		}

		.with-nav-tabs.panel-default .nav-tabs>li.dropdown .dropdown-menu {
			background-color: #f5f5f5;
			border-color: #ddd;
		}

		.with-nav-tabs.panel-default .nav-tabs>li.dropdown .dropdown-menu>li>a {
			color: #777;
		}

		.with-nav-tabs.panel-default .nav-tabs>li.dropdown .dropdown-menu>li>a:hover,
		.with-nav-tabs.panel-default .nav-tabs>li.dropdown .dropdown-menu>li>a:focus {
			background-color: #ddd;
		}

		.with-nav-tabs.panel-default .nav-tabs>li.dropdown .dropdown-menu>.active>a,
		.with-nav-tabs.panel-default .nav-tabs>li.dropdown .dropdown-menu>.active>a:hover,
		.with-nav-tabs.panel-default .nav-tabs>li.dropdown .dropdown-menu>.active>a:focus {
			color: #fff;
			background-color: #555;
		}
		canvas {
		-moz-user-select: none;
		-webkit-user-select: none;
		-ms-user-select: none;
		}
	</style>
</head>

<body>
	<div id="overlay" style="display:none;">
		<img src="<?php echo base_url('theme/image/loading.gif'); ?>" class="overlay" />
	</div>
	<div id="wrapper" ng-app="myApp">