<!-- Navigation -->
<nav class="navbar navbar-default navbar-static-top" role="navigation" style="margin-bottom: 0">
	<div class="navbar-header">
		<button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
			<span class="sr-only">Toggle navigation</span>
			<span class="icon-bar"></span>
			<span class="icon-bar"></span>
			<span class="icon-bar"></span>
		</button>
		<a class="navbar-brand">
			<?php 
				if($this->session->userdata('user') == "admin"){
					echo "POS";
				}else{
					echo $this->session->userdata('business_name');
				}
			?>
		</a>
	</div>
	<!-- /.navbar-header -->
	<ul class="nav navbar-top-links navbar-right">
		<li class="dropdown">
			<a class="dropdown-toggle" data-toggle="dropdown" href="#">
				<i class="fa fa-user fa-fw"></i> <i class="fa fa-caret-down"></i>
			</a>
			<ul class="dropdown-menu dropdown-user">
				<li><a href="#"><i class="fa fa-user fa-fw"></i><?php echo $this->session->userdata('user') ?></a>
				</li>
				</li> 
				<li class="divider"></li>
				<li><a href="<?php echo base_url('/Login/logout'); ?>"><i class="fa fa-sign-out fa-fw"></i>Logout</a>
				</li>
			</ul>
			<!-- /.dropdown-user -->
		</li>
	</ul>
	<!-- /.navbar-top-links -->

	<!-- /.navbar-top-links -->

	<div class="navbar-default sidebar" role="navigation">
		<div class="sidebar-nav navbar-collapse">
			<ul class="nav" id="side-menu">
				<?php 
					if($this->session->userdata('user') !== "admin"){
				?>
				<li>
					<a href="#"><i class="fa fa-dashboard fa-fw"></i>รายงาน<span class="fa arrow"></span></a>
					<ul class="nav nav-second-level  ">
						<li>
							<a href="<?php echo base_url('/Dashboard'); ?>"><i class="fa fa-dashboard fa-fw"></i>สรุปยอดขาย</a>
						</li>
						<li>
							<a href="<?php echo base_url('/DashboardByGoods'); ?>"><i class="fa fa-dashboard fa-fw"></i>ยอดขายตามสินค้า</a>
						</li>
						<!-- <li>
							<a href="<?php echo base_url('/DashboardByCategory'); ?>"><i class="fa fa-dashboard fa-fw"></i>ยอดขายตามหมวดหมู่</a>
						</li>
						<li>
							<a href="<?php echo base_url('/DashboardByEmployee'); ?>"><i class="fa fa-dashboard fa-fw"></i>ยอดขายตามพนักงาน</a>
						</li> -->
						<li>
							<a href="<?php echo base_url('/ReportReceipt'); ?>"><i class="fa fa-dashboard fa-fw"></i>ใบเสร็จรับเงิน</a>
						</li>
					</ul>
				</li>
				<li>
					<a href="<?php echo base_url('/Customer'); ?>"><i class="fa fa-dashboard fa-fw"></i>รายชื่อลูกค้า</a>
				</li>
				<li>
					<a href="#"><i class="fa fa-dashboard fa-fw"></i>จัดการสินค้า<span class="fa arrow"></span></a>
					<ul class="nav nav-second-level  ">
						<li>
							<a href="<?php echo base_url('/Goods'); ?>"><i class="fa fa-dashboard fa-fw"></i>รายการสินค้า</a>
						</li>
						<li>
							<a href="<?php echo base_url('/Goodscategory'); ?>"><i class="fa fa-dashboard fa-fw"></i>หมวดหมู่</a>
						</li>
						<!-- <li>
							<a href="<?php echo base_url('/'); ?>"><i class="fa fa-dashboard fa-fw"></i>ตัวเลือกเพิ่มเติม</a>
						</li> -->
						<li>
							<a href="<?php echo base_url('/Discount'); ?>"><i class="fa fa-dashboard fa-fw"></i>ส่วนลด</a>
						</li>
					</ul>
				</li>
				
				<li>
					<a href="#"><i class="fa fa-dashboard fa-fw"></i>จัดการสต็อคสินค้า<span class="fa arrow"></span></a>
					<ul class="nav nav-second-level  ">
						<li>
							<a href="<?php echo base_url('/Order'); ?>"><i class="fa fa-dashboard fa-fw"></i>สั่งซื้อ</a>
						</li>
						<li>
							<a href="<?php echo base_url('/Createadjustment'); ?>"><i class="fa fa-dashboard fa-fw"></i>ปรับปรุงสต็อคสินค้า</a>
						</li>
						<li>
							<a href="<?php echo base_url('/Vendor'); ?>"><i class="fa fa-dashboard fa-fw"></i>ซัพพลายเออร์</a>
						</li>
					</ul>
				</li>
				<li>
					<a href="#"><i class="fa fa-dashboard fa-fw"></i>จัดการพนักงาน<span class="fa arrow"></span></a>
					<ul class="nav nav-second-level  ">
						<li>
							<a href="<?php echo base_url('/Employee'); ?>"><i class="fa fa-dashboard fa-fw"></i>รายชื่อพนักงาน</a>
						</li>
						<li>
							<a href="<?php echo base_url('/Role'); ?>"><i class="fa fa-dashboard fa-fw"></i>สิทธิ์การเข้าถึง</a>
						</li>
					</ul>
				</li>
				
				<li>
					<a href="<?php echo base_url('/Setting'); ?>"><i class="fa fa-dashboard fa-fw"></i>ตั้งค่า</a>
				</li>

				<?php }else{ ?>
				<li>
					<a href="#"><i class="fa fa-dashboard fa-fw"></i>จัดการร้านค้า<span class="fa arrow"></span></a>
					<ul class="nav nav-second-level  ">
						<li>
							<a href="<?php echo base_url('/Business'); ?>"><i class="fa fa-dashboard fa-fw"></i>รายชื่อร้านค้า</a>
						</li>
						<li>
							<a href="<?php echo base_url('/BusinessType'); ?>"><i class="fa fa-dashboard fa-fw"></i>ประเภทร้านค้า</a>
						</li>
						<li>
							<a href="<?php echo base_url('/Businesscategory'); ?>"><i class="fa fa-dashboard fa-fw"></i>หมวดหมู่ร้านค้า</a>
						</li>
						<li>
							<a href="<?php echo base_url('/BusinessCode'); ?>"><i class="fa fa-dashboard fa-fw"></i>โค๊ดสำหรับร้านค้า</a>
						</li>
					</ul>
				</li>
				<li>
					<a href="<?php echo base_url('/User'); ?>"><i class="fa fa-dashboard fa-fw"></i>จัดการผู้ใช้งาน</a>
				</li>
				<?php } ?>
				<?php if($this->session->userdata('user') == "admin"){ ?>
				<li>
					<a href="#"><i class="fa fa-dashboard fa-fw"></i>จัดการลูกค้า<span class="fa arrow"></span></a>
					<ul class="nav nav-second-level  ">
						<li>
							<a href="<?php echo base_url('/Customer'); ?>"><i class="fa fa-dashboard fa-fw"></i>รายชื่อลูกค้า</a>
						</li>
						<li>
							<a href="<?php echo base_url('/Customercategory'); ?>"><i class="fa fa-dashboard fa-fw"></i>ประเภทลูกค้า</a>
						</li>
					</ul>
				</li>
				<?php }?>
				<!-- <li>
					<a href="#"><i class="fa fa-dashboard fa-fw"></i> Manager Member<span class="fa arrow"></span></a>
					<ul class="nav nav-second-level  ">
						
						
					</ul>
				</li> -->
				<!-- <li>
					<a href="<?php echo base_url('/Member'); ?>"><i class="fa fa-dashboard fa-fw"></i> Manager Member</a>
				</li>
				
				

				<li>
					<a href="#"><i class="fa fa-dashboard fa-fw"></i> Report<span class="fa arrow"></span></a>
					<ul class="nav nav-second-level  ">
						<li>
							<a href="<?php echo base_url('/MemberRegisterLog'); ?>"><i class="fa fa-dashboard fa-fw"></i> Member Register</a>
						</li>
						<li>
							<a href="<?php echo base_url('/VoucherLog'); ?>"><i class="fa fa-dashboard fa-fw"></i> Voucher</a>
						</li>
						
						<li>
							<a href="<?php echo base_url('/VendorSaleLog'); ?>"><i class="fa fa-dashboard fa-fw"></i> Vendor Sale</a>
						</li>
						<li>
							<a href="<?php echo base_url('/RelationStamp'); ?>"><i class="fa fa-dashboard fa-fw"></i> Relation Stamp</a>
						</li>					
					</ul>
				</li>		 -->
			</ul>
		</div>
		<!-- /.sidebar-collapse -->
	</div>
	<!-- /.navbar-static-side -->
</nav>


<div id="page-wrapper">