<script src="<?php echo base_url('asset/goodsController.js'); ?>"></script>
<div ng-controller="goodsController" ng-init="onInit()">

    <div class="row">
        <ul class="navigator">
            <?php /*<li class="nav"><a href="/Rooms"><?php echo $this->lang->line('event');?></a></li>*/ ?>
            <li class="nav_active">สินค้า</li>
        </ul>
        <!-- /.col-lg-12 -->
    </div>

    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">สินค้า</h1>
        </div>
        <!-- /.col-lg-12 -->
    </div>

    <!-- /List.row types-->
    <div class="row  SearchDevice" style="display:none;">
        <div class="col-lg-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <?php echo $this->lang->line('Search'); ?>
                </div>
                <div class="panel-body">
                    <div class="form-group col-lg-12 col-md-12 col-xs-12">
                        <div class="col-lg-6 col-md-6 col-xs-12">
                            <label><span class="text-danger">*</span>สินค้า</label>
                            <input class="form-control" ng-model="modelSearch.NAME" maxlength="80">
                            <p class="help-block"></p>
                        </div>
                        
                    </div>
                
                    <div class="col-lg-12 col-md-12 col-xs-12">
                        <button type="button" class="btn btn-primary waves-effect waves-light m-b-5" ng-click="resetSearch()"><i class="glyphicon glyphicon-repeat"></i> <span class="hidden-xs"><?php echo $this->lang->line('ResetSearch'); ?></span></button>
                        <button type="button" class="btn btn-primary waves-effect waves-light m-b-5" ng-click="LoadSearch()"><i class="fa fa-search"></i> <span class="hidden-xs"><?php echo $this->lang->line('Search'); ?></span></button>
                        <button type="button" class="btn btn-danger waves-effect waves-light m-b-5" ng-click="ShowDevice()"><i class="fa fa-times"></i> <span class="hidden-xs"><?php echo $this->lang->line('Cancel'); ?></span></button>
                    </div>
                </div>
                <!-- /.panel-body -->
            </div>
            <!-- /.panel -->
        </div>
        <!-- /.col-lg-12 -->
    </div>
    <!-- /List.row types-->


    <!-- / create room types  -->
    <div class="row addDevice" style="display:none;">
        <div class="col-lg-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    จัดการสินค้า
                </div>
                <div class="panel-body">
                    <div class="row">

                        <div class="col-lg-8 col-md-8 col-xs-12" style="padding-right:0px">
                            <div role="form">
                                <div class="panel-group" >
                                    <div class="form-group col-lg-12 col-md-12 col-xs-12" style="text-align: center">   
                                        <div class="form-group col-lg-6 col-md-6 col-xs-6">
                                            <img ng-src="{{PreviewImage}}" ng-show="ViewImgPreview == true" alt="" style="width:250px;border: 1px solid #ddd; border-radius: 4px;padding:5px;" />   
                                            <img src="<?php echo base_url('/upload/Goods/');?>{{CreateModel.IMAGE}}" ng-show="ViewImgPreview == false" alt="" style="width:250px;border: 1px solid #ddd; border-radius: 4px;padding:5px;"  />                                        
                                        </div>
                                        <div class="form-group col-lg-6 col-md-6 col-xs-6">
                                            <img src="<?php echo base_url('/upload/barcode/');?>{{CreateModel.BARCODE}}">
                                        </div>
                                    </div> 
                                    <div class="form-group col-lg-12 col-md-12 col-xs-12">
                                        <div class="col-lg-3 col-md-3 col-xs-3">
                                            <h5><span class="text-danger"></span>รูปภาพ</h5>
                                        </div>
                                        <div class="col-lg-8 col-md-8 col-xs-8">
                                        <input type="file" onchange="angular.element(this).scope().SelectFile(event)" ngf-select ng-model="filegeneralinfo" name="filegeneralinfo" ngf-pattern="'.png'" ngf-accept="'.png'" ngf-max-size="10MB" ngf-min-height="100"/>
                                        </div>
                                    </div>
                                    <div class="form-group col-lg-12 col-md-12 col-xs-12">
										<div class="col-lg-6 col-md-6 col-xs-12">
											<label><span class="text-danger" >*</span>ชื่อสินค้า</label>
											<input class="form-control" ng-model="CreateModel.NAME">
											<p class="CreateModel_NAME require text-danger"><?php echo $this->lang->line('Require');?></p>
										</div> 
										<div class="col-lg-6 col-md-6 col-xs-12">
											<label><span class="text-danger" >*</span>ประเภทสินค้า</label>
                                            <ui-select ng-model="TempGoodscategoryIndex.selected" theme="selectize">
                                                <ui-select-match>{{$select.selected.NAME}}</ui-select-match>
                                                <ui-select-choices repeat="Item in ListGoodscategory  | filter: $select.search">
                                                    <span ng-bind-html="Item.NAME | highlight: $select.search"></span>
                                                </ui-select-choices>
                                            </ui-select>
											<p class="CreateModel_CATEGORY require text-danger"><?php echo $this->lang->line('Require');?></p>
										</div> 
									</div>
                                </div>
                                <div class="panel-group" >
                                    <div class="form-group col-lg-12 col-md-12 col-xs-12">
										<div class="col-lg-6 col-md-6 col-xs-12">
											<input type="checkbox" ng-model="CreateModel.STATUS"><b> สินค้าพร้อมจำหน่าย</b>
										</div> 
										<div class="col-lg-6 col-md-6 col-xs-12">
											
										</div> 
									</div>
                                </div>
                                <div class="panel-group">
                                    <div class="form-group col-lg-12 col-md-12 col-xs-12">
										<div class="col-lg-6 col-md-6 col-xs-12">
                                            <label><span class="text-danger" >*</span>ราคาขาย</label>
                                            <input class="form-control" ng-model="CreateModel.PRICE">
                                            <p class="CreateModel_PRICE require text-danger"><?php echo $this->lang->line('Require');?></p>
										</div> 
										<div class="col-lg-6 col-md-6 col-xs-12">
                                            <label><span class="text-danger" >*</span>ต้นทุน</label>
                                            <input class="form-control" ng-model="CreateModel.COST">
                                            <p class="CreateModel_COST require text-danger"><?php echo $this->lang->line('Require');?></p>
										</div> 
									</div>
                                </div>
                                <div class="panel-group">
                                    <div class="form-group col-lg-12 col-md-12 col-xs-12">
										<div class="col-lg-6 col-md-6 col-xs-12">
                                            <label><span class="text-danger" >*</span>SKU</label>
                                            <input class="form-control"ng-model="CreateModel.SKU" >
                                            <p class="CreateModel_SKU require text-danger"><?php echo $this->lang->line('Require');?></p>
										</div> 
										<div class="col-lg-6 col-md-6 col-xs-12">
                                            <label>บาร์โค๊ด</label>
                                          <input class="form-control"ng-model="CreateModel.BARCODE">
										</div> 
                                    </div>
                                    <div class="form-group col-lg-12 col-md-12 col-xs-12">
										<div class="col-lg-12 col-md-12 col-xs-12">
                                            <label>รายละเอียด</label>
                                            <textarea class="form-control"ng-model="CreateModel.DETAIL" ></textarea>
										</div> 
		
									</div>
                                </div>
                                
                            </div>
                        </div>

                        <div class="col-lg-12 col-md-12 col-xs-12">
                            <div class="form-group col-lg-12 col-md-12 col-xs-12  text-right">
                                <br>
                                <div class="col-lg-12 col-md-12 col-xs-12">
                                    <button class="btn btn-primary" ng-click="upload()"><i class="fa fa-save"></i> <span class="hidden-xs"><?php echo $this->lang->line('Save'); ?></span></button>
                                    <button class="btn btn-danger" ng-click="ShowDevice()"><i class="fa fa-times "></i> <span class="hidden-xs"><?php echo $this->lang->line('Cancel'); ?></span></button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <!-- /.col-lg-12 -->
    </div>
    <!-- /.create room types -->


    <!-- /List.row types-->
    <div class="row DisplayDevice">
        <div class="col-lg-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    รายการสินค้า
                </div>
                <div class="panel-body">
                    <div class="col-lg-12 col-md-12 col-xs-12">
                        <button class="btn btn-primary"ng-click="AddNewDevice()"><i class="fa fa-plus  "></i> <span class="hidden-xs"><?php echo $this->lang->line('Add'); ?></span></button>
                        <button class="btn btn-info" ng-click="ShowSearch()"><i class="fa fa-search  "></i> <span class="hidden-xs"><?php echo $this->lang->line('Search'); ?></span></button>
                    </div>
                    <div class="col-lg-12 col-md-12 col-xs-12">
                        <div class="table-responsive">
                            <table class="table table-striped">
                                <thead>
                                    <tr>
                                        <th width="5%">#</th>
                                        <th class="sorting" ng-click="sort($event)">ชื่อสินค้า</th>
                                        <th class="sorting" ng-click="sort($event)" sort="CATEGORYNAME">ประเภทสินค้า</th>
                                        <th class="sorting" ng-click="sort($event)" sort="PRICE">ราคาขาย</th>
                                        <th class="sorting" ng-click="sort($event)" sort="COST">ต้นทุน</th>
                                        <th>ผลต่าง</th>
                                        <th class="sorting" ng-click="sort($event)" sort="STOCK">อยู่ในคลัง</th>
                                        <th >สถานะ</th>
                                        <th><?php echo $this->lang->line('Option'); ?></th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr ng-repeat="item in modelDeviceList">
                                        <td>{{$index+1}}</td>
                                        <td ng-bind="item.NAME"></td>
                                        <td ng-bind="item.CATEGORYNAME"></td>
                                        <td ng-bind="item.PRICE"></td>
                                        <td ng-bind="item.COST"></td>
                                        <td ng-bind="item.Difference"></td>
                                        <td ng-bind="item.STOCK"></td>
                                        <td><b ng-show="item.STATUS == 1" style="color:green">พร้อมขาย</b><b style="color:red" ng-hide="item.STATUS == 1">ไม่พร้อมขาย</b></td>
                                        <td>
                                            <button  ng-click="onEditTagClick(item)" class="btn btn-primary waves-effect waves-light btn-sm m-b-5"><i class="glyphicon glyphicon-edit"></i> <span class="hidden-xs"><?php echo $this->lang->line('Edit'); ?></span></button>
                                            <button   my-confirm-click="onDeleteTagClick(item)" my-confirm-click-message="<?php echo $this->lang->line('DoYouWantToDelete'); ?>" class="btn btn-danger waves-effect waves-light btn-sm m-b-5"><i class="glyphicon glyphicon-trash"></i> <span class="hidden-xs">ยกเลิกใช้งาน<?php //echo $this->lang->line('Delete'); ?></span></button>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                        <!-- /.table-responsive -->
                    </div>

                    <!-- ทำหน้า -->
                    <div class="row tblResult small">
                        <div class="col-md-7 col-sm-7 col-xs-12 ">
                            <label class="col-md-4 col-sm-4 col-xs-12">
                                <?php echo $this->lang->line('Total'); ?> {{totalRecords}} <?php echo $this->lang->line('Records'); ?>
                            </label>
                            <label class="col-md-4 col-sm-4 col-xs-12">
                                <?php echo $this->lang->line('ResultsPerPage'); ?>
                            </label>
                            <div class="col-md-4 col-sm-4 col-xs-12 ">
                                <ui-select ng-model="TempPageSize.selected" ng-change="loadByPageSize()" theme="selectize">
                                    <ui-select-match>{{$select.selected.Value}}</ui-select-match>
                                    <ui-select-choices repeat="pSize in listPageSize | filter: $select.search">
                                        <span ng-bind-html="pSize.Text | highlight: $select.search"></span>
                                    </ui-select-choices>
                                </ui-select>
                            </div>
                        </div>
                        <div class="col-md-5 col-sm-5 col-xs-12  ">
                            <label class="col-md-4 col-sm-4 col-xs-12">
                                <span ng-click="getBackPage()" class="set-pointer"><i class="fa fa-chevron-left"></i> <span class="hidden-xs"><?php echo $this->lang->line('Previous'); ?></span></span>
                            </label>
                            <div class="col-md-3 col-sm-3 col-xs-12">
                                <ui-select ng-model="TempPageIndex.selected" ng-change="searchByPage()" theme="selectize">
                                    <ui-select-match>{{$select.selected.PageIndex}}</ui-select-match>
                                    <ui-select-choices repeat="pIndex in listPageIndex | filter: $select.search">
                                        <span ng-bind-html="pIndex.PageIndex | highlight: $select.search"></span>
                                    </ui-select-choices>
                                </ui-select>
                            </div>
                            <label class="col-md-4 col-sm-4 col-xs-12">
                                / {{ totalPage }} <span ng-click="getNextPage()" class="set-pointer"><?php echo $this->lang->line('Next'); ?><i class="fa fa-chevron-right set-pointer"></i></span>
                            </label>
                        </div>
                    </div>
                    <!-- ทำหน้า -->

                </div>
                <!-- /.panel-body -->
            </div>
            <!-- /.panel -->
        </div>
        <!-- /.col-lg-12 -->
    </div>
    <!-- /List.row types-->
</div>
</div>