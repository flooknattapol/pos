<script src="<?php echo base_url('asset/eventmemberController.js'); ?>"></script>
<div ng-controller="eventmemberController" ng-init="onInit()">

    <div class="row">
        <ul class="navigator">
            <?php /*<li class="nav"><a href="/Rooms"><?php echo $this->lang->line('eventmember');?></a></li>*/ ?>
            <li class="nav_active">Event Member & Goods</li>
        </ul>
        <!-- /.col-lg-12 -->
    </div>

    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">Event Member & Goods</h1>
        </div>
        <!-- /.col-lg-12 -->
    </div>

    <!-- /List.row types-->
    <div class="row  SearchDevice" style="display:none;">
        <div class="col-lg-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <?php echo $this->lang->line('Search'); ?>
                </div>
                <div class="panel-body">
                    <div class="form-group col-lg-12 col-md-12 col-xs-12">
                        <div class="col-lg-6 col-md-6 col-xs-12">
                            <label><span class="text-danger" >*</span>Event</label>
                            <input list="EventList" ng-model="modelSearch.NAME"class="form-control" >
                            <datalist id="EventList">
                            <option ng-repeat="event in ListEventModel" value="{{event.NAME}}">
                            </datalist>
                            <p class="help-block"></p>
                        </div>
                        <div class="col-lg-6 col-md-6 col-xs-12">
                        </div>
                    </div>
                    <div class="form-group col-lg-12 col-md-12 col-xs-12">


                    </div>
                    <div class="col-lg-12 col-md-12 col-xs-12">
                        <button type="button" class="btn btn-primary waves-effect waves-light m-b-5" ng-click="resetSearch()"><i class="glyphicon glyphicon-repeat"></i> <span class="hidden-xs"><?php echo $this->lang->line('ResetSearch'); ?></span></button>
                        <button type="button" class="btn btn-primary waves-effect waves-light m-b-5" ng-click="LoadSearch()"><i class="fa fa-search"></i> <span class="hidden-xs"><?php echo $this->lang->line('Search'); ?></span></button>
                        <button type="button" class="btn btn-danger waves-effect waves-light m-b-5" ng-click="ShowDevice()"><i class="fa fa-times"></i> <span class="hidden-xs"><?php echo $this->lang->line('Cancel'); ?></span></button>
                    </div>
                </div>
                <!-- /.panel-body -->
            </div>
            <!-- /.panel -->
        </div>
        <!-- /.col-lg-12 -->
    </div>
    <!-- /List.row types-->


    <!-- / create room types  -->
    <div class="row addDevice" style="display:none;">
        <div class="col-lg-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    Manage Event member & Goods
                </div>
                <div class="panel-body">
                    <div class="row">

                        <div class="col-lg-8 col-md-8 col-xs-12" style="padding-right:0px">
                            <div role="form">
                                <br>
                                <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
                                    <div class="form-group col-lg-12 col-md-12 col-xs-12">
                                    </div>
                                    <div class="form-group col-lg-12 col-md-12 col-xs-12">
                                        <div class="col-lg-3 col-md-3 col-xs-3">
                                            <h5><span class="text-danger"></span>ชื่ออีเว้นท์</h5>
                                        </div>
                                        <div class="col-lg-8 col-md-8 col-xs-8">
                                            <ui-select ng-model="TempEventIndex.selected" ng-change="loadByPageSize()" theme="selectize" ng-disabled="Edit == true">
                                                <ui-select-match>{{$select.selected.NAME}}</ui-select-match>
                                                <ui-select-choices repeat="Item in ListEventModel | filter: $select.search">
                                                    <span ng-bind-html="Item.NAME | highlight: $select.search"></span>
                                                </ui-select-choices>
                                            </ui-select>
                                            <p class="CreateModel_EVENT_ID require text-danger"><?php echo $this->lang->line('Require'); ?></p>
                                        </div>
                                    </div>

                                    <div class="form-group col-lg-12 col-md-12 col-xs-12">
                                        <div class="col-lg-3 col-md-3 col-xs-3">
                                            <h5><span class="text-danger"></span>Member</h5>
                                        </div>
                                        <div class="col-lg-8 col-md-8 col-xs-8">
                                            <ui-select ng-model="TempMemberIndex.selected" ng-change="LoadShopNameByCus()" theme="selectize"ng-disabled="Edit == true">
                                                <ui-select-match>{{$select.selected.NAME}}</ui-select-match>
                                                <ui-select-choices repeat="Item in ListMemberModel | filter: $select.search">
                                                    <span ng-bind-html="Item.NAME | highlight: $select.search"></span>
                                                </ui-select-choices>
                                            </ui-select>
                                            <p class="CreateModel_MEMBER_ID require text-danger"><?php echo $this->lang->line('Require'); ?></p>
                                        </div>
                                    </div>     
                                    <div class="form-group col-lg-12 col-md-12 col-xs-12">
                                        <div class="col-lg-3 col-md-3 col-xs-3">
                                            <h5><span class="text-danger"></span>Vendor</h5>
                                        </div>
                                        <div class="col-lg-1 col-md-1 col-xs-1">
                                            <input  type="checkbox"ng-model="CreateModel.VENDOR"ng-disabled="Edit == true">
                                        </div>
                                        <div class="col-lg-1 col-md-1 col-xs-1">
                                            <h5 ng-show="CreateModel.VENDOR == true"><span class="text-danger"></span>ชื่อร้าน</h5>
                                        </div>
                                        <div class="col-lg-5 col-md-5 col-xs-5">
                                            <input ng-show="CreateModel.VENDOR == true" type="text" class="form-control" ng-model="CreateModel.VENDOR_NAME"ng-disabled="Edit == true">
                                            <p class="CreateModel_VENDOR_NAME require text-danger"><?php echo $this->lang->line('Require'); ?></p>
                                        </div>
                                    </div>  
                                    <div class="form-group col-lg-12 col-md-12 col-xs-12">
                                        <div class="col-lg-3 col-md-3 col-xs-3">
                                            <h5><span class="text-danger"></span>Buyer</h5>
                                        </div>
                                        <div class="col-lg-8 col-md-8 col-xs-8">
                                            <input type="checkbox" ng-model="CreateModel.BUYER"ng-disabled="Edit == true">
                                        </div>
                                    </div>  
                                </div>
                            </div>
                        </div>
                        <div class="form-group col-lg-12 col-md-12 col-xs-12 " ng-show="CreateModel.VENDOR == true">
                            <button class="btn btn-primary" ng-click="AddNewLine()" ><i class="fa fa-plus  "></i> <span class="hidden-xs">เพิ่มรายการสินค้า</span></button> 
                            <button class="btn btn-primary" ng-click="loadGoodsDetailByMember()">โหลดข้อมูลสินค้า</button>
                        </div>
                        <div class="form-group col-lg-12 col-md-12 col-xs-12 "ng-show="CreateModel.VENDOR == true">
                            <div class="col-lg-3 col-md-3 col-xs-3"> 
                                <label>สินค้า</label> 
                            </div>
                            <div class="col-lg-1 col-md-1 col-xs-1"> 
                                <label>ประเภทอาหาร</label> 
                            </div>
                            <div class="col-lg-1 col-md-1 col-xs-1"> 
                                <label>รหัสสินค้า</label> 
                            </div>
                            <div class="col-lg-1 col-md-1 col-xs-1"> 
                                <label>จำนวน</label> 
                            </div>
                            <div class="col-lg-1 col-md-1 col-xs-1"> 
                                <label>ส่วนลด</label> 
                            </div>
                            <div class="col-lg-1 col-md-1 col-xs-1"> 
                                <label>จำนวนสิทธิ์</label> 
                            </div>
                            <div class="col-lg-1 col-md-1 col-xs-1">
                                <label>ราคา</label> 
                            </div>
                            <div class="col-lg-1 col-md-1 col-xs-1">
                                <label>รูปภาพ</label> 
                            </div>
                            <div class="col-lg-1 col-md-1 col-xs-1">
                                <label>Upload</label>
                                
                            </div>
                            <div class="col-lg-1 col-md-1 col-xs-1">
                                <label>ยกเลิกใช้</label>
                                
                            </div>
                        </div>
                        <div class="form-group col-lg-12 col-md-12 col-xs-12" ng-repeat="item in vendorDetailModel"ng-show="CreateModel.VENDOR == true" >
                            <div class="col-lg-3 col-md-3 col-xs-3"  style="padding-right: 2px;padding-left: 2px;">
                                <input class="form-control"  ng-model="item.NAME" type="text">
                            </div>
                            <div  class="col-lg-1 col-md-1 col-xs-1"   style="padding-right: 2px;padding-left: 2px;">
                                <select class="form-control"  ng-model="item.TYPE">
                                    <option ng-repeat="type in TypeGoods" ng-value="type.ID">{{type.NAME}}</option>
                                </select>
                            </div>
                            <div  class="col-lg-1 col-md-1 col-xs-1"    style="padding-right: 1px;padding-left: 1px;">
                                <input class="form-control"  ng-model="item.SKU" type="text">
                            </div>
                            <div  class="col-lg-1 col-md-1 col-xs-1"    style="padding-right: 1px;padding-left: 12px;">
                                <input class="form-control" dir="rtl" ng-model="item.UNIT" type="text">
                            </div>	
                            <div  class="col-lg-1 col-md-1 col-xs-1"    style="padding-right: 1px;padding-left: 12px;">
                                <input class="form-control" dir="rtl" ng-model="item.discount" type="text">
                            </div>
                            <div  class="col-lg-1 col-md-1 col-xs-1"    style="padding-right: 1px;padding-left: 1px;">
                                <input class="form-control" dir="rtl" ng-model="item.qty_privilege" type="text">
                            </div>
                            <div class="col-lg-1 col-md-1 col-xs-1"    style="padding-right: 1px;padding-left: 12px;">
                                <input class="form-control" dir="rtl" ng-model="item.PRICE" type="text">
                            </div>
                            <div  class="col-lg-1 col-md-1 col-xs-1"    style="padding-right: 1px;padding-left: 12px;">
                            <img ng-src="<?php echo base_url('/upload/Goods/');?>{{item.IMAGE}}" style="height:40px;">
                            </div>
                            <div  class="col-lg-1 col-md-1 col-xs-1"    style="padding-right: 1px;padding-left: 12px;">
                                <button ng-click="UploadImage(item)" data-toggle="modal" data-target="#UploadImg" class="btn btn-primary" ><i class="fa fa-upload"></i> <span class="hidden-xs">Upload</span></button>
                            </div>
                            <div class="col-lg-1 col-md-1 col-xs-1"    style="padding-right: 1px;padding-left: 12px;">
                                <input type="checkbox"  ng-model="item.GoodsActive" >
                            </div>
                        </div>
                        <div class="col-lg-12 col-md-12 col-xs-12">
                            <div class="form-group col-lg-12 col-md-12 col-xs-12  text-right">
                                <br>
                                <div class="col-lg-12 col-md-12 col-xs-12">
                                    <button class="btn btn-primary" ng-click="onSaveTagClick()"><i class="fa fa-save"></i> <span class="hidden-xs"><?php echo $this->lang->line('Save'); ?></span></button>
                                    <button class="btn btn-danger" ng-click="ShowDevice()"><i class="fa fa-times "></i> <span class="hidden-xs"><?php echo $this->lang->line('Cancel'); ?></span></button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <!-- /.col-lg-12 -->
    </div>
    <!-- /.create room types -->

    <!-- Modal -->
    <div id="UploadImg" class="modal fade" role="dialog">
        <div class="modal-dialog">

            <!-- Modal content-->
            <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Upload Image</h4>
            </div>
            <div class="modal-body">
                <div class="form-group col-lg-12 col-md-12 col-xs-12">
                    <div class="col-lg-9 col-md-9 col-xs-12">
                        <input class="form-control" readonly ng-model="filegeneralinfo.name" ng-show="filegeneralinfo != undefined">
                    </div>
                    <div class="col-md-1 col-sm-1 col-xs-12" ngf-select ng-model="filegeneralinfo" name="filegeneralinfo" ngf-pattern="'.png'" ngf-accept="'.png'" ngf-max-size="10MB" ngf-min-height="100">
                        <span class="btn btn-primary">Browse File</span>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" ng-click="upload()"class="btn btn-primary" >Save</button>
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
            </div>

        </div>
    </div>
    <!-- /List.row types-->
    <div class="row DisplayDevice">
        <div class="col-lg-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    Manage Event Member & Goods
                </div>
                <div class="panel-body">
                    <div class="col-lg-12 col-md-12 col-xs-12">
                        <button class="btn btn-primary"ng-click="AddNewDevice()"><i class="fa fa-plus  "></i> <span class="hidden-xs"><?php echo $this->lang->line('Add'); ?></span></button>
                        <button class="btn btn-info" ng-click="ShowSearch()"><i class="fa fa-search  "></i> <span class="hidden-xs"><?php echo $this->lang->line('Search'); ?></span></button>
                    </div>
                    <div class="col-lg-12 col-md-12 col-xs-12">
                        <div class="table-responsive">
                            <table class="table table-striped">
                                <thead>
                                    <tr>
                                        <th width="5%">#</th>
                                        <th class="sorting_asc" ng-click="sort($event)" sort="NAME">ชื่ออีเว้นท์</th>
                                        <th class="sorting" ng-click="sort($event)" sort="Member_Name">Member</th>
                                        <th class="sorting" ng-click="sort($event)" sort="VENDOR_NAME">ร้านค้า</th>
                                        <th>Vendor</th>
                                        <th>Buyer</th>
                                        <th><?php echo $this->lang->line('Option'); ?></th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr ng-repeat="item in modelDeviceList">
                                        <td>{{$index+1}}</td>
                                        <td ng-bind="item.NAME"></td>
                                        <td>{{item.Member_Name}} {{item.Member_LastName}}</td>
                                        <td>
                                            <span ng-show="item.VENDOR == true">{{item.VENDOR_NAME}}</span>
                                            <font style="color:red" ng-hide="item.VENDOR == true">ไม่ได้เป็นร้านค้า</font>
                                        </td>
                                        <td>
                                            <i ng-show="item.VENDOR == true" class="fa fa-check"style="font-size:30px;color:green;"></i>
                                            <i ng-hide="item.VENDOR == true"class="fa fa-close"style="font-size:30px;color:red;">
                                        </td>
                                        <td>
                                            <i ng-show="item.BUYER == true" class="fa fa-check"style="font-size:30px;color:green;"></i>
                                            <i ng-hide="item.BUYER == true"class="fa fa-close"style="font-size:30px;color:red;"></i>
                                            
                                        </td>
                                        <td>
                                            <button ng-click="onEditTagClick(item)" class="btn btn-primary waves-effect waves-light btn-sm m-b-5"><i class="glyphicon glyphicon-edit"></i> <span class="hidden-xs"><?php echo $this->lang->line('Edit'); ?></span></button>
                                            <button my-confirm-click="onDeleteTagClick(item)" my-confirm-click-message="<?php echo $this->lang->line('DoYouWantToDelete'); ?>" class="btn btn-danger waves-effect waves-light btn-sm m-b-5"><i class="glyphicon glyphicon-trash"></i> <span class="hidden-xs"><?php echo $this->lang->line('Delete'); ?></span></button>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                        <!-- /.table-responsive -->
                    </div>

                    <!-- ทำหน้า -->
                    <div class="row tblResult small">
                        <div class="col-md-7 col-sm-7 col-xs-12 ">
                            <label class="col-md-4 col-sm-4 col-xs-12">
                                <?php echo $this->lang->line('Total'); ?> {{totalRecords}} <?php echo $this->lang->line('Records'); ?>
                            </label>
                            <label class="col-md-4 col-sm-4 col-xs-12">
                                <?php echo $this->lang->line('ResultsPerPage'); ?>
                            </label>
                            <div class="col-md-4 col-sm-4 col-xs-12 ">
                                <ui-select ng-model="TempPageSize.selected" ng-change="loadByPageSize()" theme="selectize">
                                    <ui-select-match>{{$select.selected.Value}}</ui-select-match>
                                    <ui-select-choices repeat="pSize in listPageSize | filter: $select.search">
                                        <span ng-bind-html="pSize.Text | highlight: $select.search"></span>
                                    </ui-select-choices>
                                </ui-select>
                            </div>
                        </div>
                        <div class="col-md-5 col-sm-5 col-xs-12  ">
                            <label class="col-md-4 col-sm-4 col-xs-12">
                                <span ng-click="getBackPage()" class="set-pointer"><i class="fa fa-chevron-left"></i> <span class="hidden-xs"><?php echo $this->lang->line('Previous'); ?></span></span>
                            </label>
                            <div class="col-md-3 col-sm-3 col-xs-12">
                                <ui-select ng-model="TempPageIndex.selected" ng-change="searchByPage()" theme="selectize">
                                    <ui-select-match>{{$select.selected.PageIndex}}</ui-select-match>
                                    <ui-select-choices repeat="pIndex in listPageIndex | filter: $select.search">
                                        <span ng-bind-html="pIndex.PageIndex | highlight: $select.search"></span>
                                    </ui-select-choices>
                                </ui-select>
                            </div>
                            <label class="col-md-4 col-sm-4 col-xs-12">
                                / {{ totalPage }} <span ng-click="getNextPage()" class="set-pointer"><?php echo $this->lang->line('Next'); ?><i class="fa fa-chevron-right set-pointer"></i></span>
                            </label>
                        </div>
                    </div>
                    <!-- ทำหน้า -->

                </div>
                <!-- /.panel-body -->
            </div>
            <!-- /.panel -->
        </div>
        <!-- /.col-lg-12 -->
    </div>
    <!-- /List.row types-->
</div>
</div>