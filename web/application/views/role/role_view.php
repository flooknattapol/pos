<script src="<?php echo base_url('asset/roleController.js'); ?>"></script>
<div ng-controller="roleController" ng-init="onInit()">

    <div class="row">
        <ul class="navigator">
            <?php /*<li class="nav"><a href="/Rooms"><?php echo $this->lang->line('event');?></a></li>*/ ?>
            <li class="nav_active">สิทธิ์การใช้งาน</li>
        </ul>
        <!-- /.col-lg-12 -->
    </div>

    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">สิทธิ์การใช้งาน</h1>
        </div>
        <!-- /.col-lg-12 -->
    </div>

    <!-- /List.row types-->
    <div class="row  SearchDevice" style="display:none;">
        <div class="col-lg-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <?php echo $this->lang->line('Search'); ?>
                </div>
                <div class="panel-body">
                    <div class="form-group col-lg-12 col-md-12 col-xs-12">
                        <div class="col-lg-6 col-md-6 col-xs-12">
                            <label><span class="text-danger">*</span>สิทธิ์การใช้งาน</label>
                            <input class="form-control" ng-model="modelSearch.NAME" maxlength="80">
                            <p class="help-block"></p>
                        </div>
                    </div>
                    <div class="form-group col-lg-12 col-md-12 col-xs-12">


                    </div>
                    <div class="col-lg-12 col-md-12 col-xs-12">
                        <button type="button" class="btn btn-primary waves-effect waves-light m-b-5" ng-click="resetSearch()"><i class="glyphicon glyphicon-repeat"></i> <span class="hidden-xs"><?php echo $this->lang->line('ResetSearch'); ?></span></button>
                        <button type="button" class="btn btn-primary waves-effect waves-light m-b-5" ng-click="LoadSearch()"><i class="fa fa-search"></i> <span class="hidden-xs"><?php echo $this->lang->line('Search'); ?></span></button>
                        <button type="button" class="btn btn-danger waves-effect waves-light m-b-5" ng-click="ShowDevice()"><i class="fa fa-times"></i> <span class="hidden-xs"><?php echo $this->lang->line('Cancel'); ?></span></button>
                    </div>
                </div>
                <!-- /.panel-body -->
            </div>
            <!-- /.panel -->
        </div>
        <!-- /.col-lg-12 -->
    </div>
    <!-- /List.row types-->


    <!-- / create room types  -->
    <div class="row addDevice" style="display:none;">
        <div class="col-lg-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    จัดการสิทธิ์การใช้งาน
                </div>
                <div class="panel-body">
                    <div class="row">

                        <div class="col-lg-8 col-md-8 col-xs-12" style="padding-right:0px">
                            <div role="form">
                                <br>
                                <div class="panel-group"> 
                                    <div class="form-group col-lg-12 col-md-12 col-xs-12">
                                        <div class="col-lg-6 col-md-6 col-xs-12">
                                            <label><span class="text-danger" >*</span>ชื่อ</label>
                                            <input class="form-control" ng-model="CreateModel.NAME">
                                            <p class="CreateModel_NAME require text-danger"><?php echo $this->lang->line('Require');?></p>

										</div>
                                    </div>  
                                </div>
                                <div class="panel-group"> 
                                    <div class="col-lg-12 col-md-12 col-xs-12">
                                        <input type="checkbox" ng-model="CreateModel.PERMISSION_POS_IsActive"> <b>POS</b><br>
                                        <small>พนักงานสามารถเข้าสู่ระบบแอพโดยใช้รหัสส่วนตัว</small> 
                                        <div class="form-group col-lg-12 col-md-12 col-xs-12" ng-show="CreateModel.PERMISSION_POS_IsActive == true">
                                            <div class="form-group col-lg-12 col-md-12 col-xs-12">
                                                <input type="checkbox"> <b>ดูใบเสร็จรับเงินทั้งหมด</b><br>
                                                <small>เมื่อตัวเลือกนี้ถูกปิดใช้งาน แคชเชียร์สามารถดูใบเสร็จรับเงินได้ 5 ใบล่าสุด</small> 
                                            </div>
                                            <div class="form-group col-lg-12 col-md-12 col-xs-12">
                                                <input type="checkbox"> <b>ใช้ส่วนลดด้วยการจำกัดการเข้าถึง</b><br>
                                            </div>
                                            <div class="form-group col-lg-12 col-md-12 col-xs-12">
                                                <input type="checkbox"> <b>เปลี่ยนภาษีในการขาย</b><br>
                                            </div>
                                            <div class="form-group col-lg-12 col-md-12 col-xs-12">
                                                <input type="checkbox"> <b>ผลตอบแทนที่ได้จากการจัดซื้อ</b><br>
                                            </div>
                                            <div class="form-group col-lg-12 col-md-12 col-xs-12">
                                                <input type="checkbox"> <b>เปิดลิ้นชักเก็บเงิน โดยไม่มีการขาย</b><br>
                                            </div>
                                            <div class="form-group col-lg-12 col-md-12 col-xs-12">
                                                <input type="checkbox"> <b>แก้ไขสินค้า</b><br>
                                            </div>
                                            <div class="form-group col-lg-12 col-md-12 col-xs-12">
                                                <input type="checkbox"> <b>เปลี่ยนการตั้งค่า</b><br>
                                            </div>
                                            <div class="form-group col-lg-12 col-md-12 col-xs-12">
                                                <input type="checkbox"> <b>เข้าถึงการสนับสนุนทางแชทสด</b><br>
                                            </div>
                                        </div>
                                    </div>  
                                    <div class="col-lg-12 col-md-12 col-xs-12">
                                        <input type="checkbox" ng-model="CreateModel.PERMISSION_DASHBOARD_IsActive"> <b>ระบบหลังร้าน</b><br>
                                        <small>พนักงานสามารถเข้าสู่ระบบหลังร้านโดยที่ใช้อีเมล์และรหัสผ่าน</small> 
                                        <div class="form-group col-lg-12 col-md-12 col-xs-12" ng-show="CreateModel.PERMISSION_DASHBOARD_IsActive == true">
                                            <div class="form-group col-lg-12 col-md-12 col-xs-12">
                                                <input type="checkbox"> <b>แสดงรายงานการขายและการแจ้งเตือน</b><br>
                                            </div>
                                            <div class="form-group col-lg-12 col-md-12 col-xs-12">
                                                <input type="checkbox"> <b>Cancel receipts</b><br>
                                            </div>
                                            <div class="form-group col-lg-12 col-md-12 col-xs-12">
                                                <input type="checkbox"> <b>รายการสินค้า</b><br>
                                            </div>
                                            <div class="form-group col-lg-12 col-md-12 col-xs-12">
                                                <input type="checkbox"> <b>จัดการพนักงาน</b><br>
                                            </div>
                                            <div class="form-group col-lg-12 col-md-12 col-xs-12">
                                                <input type="checkbox"> <b>การเข้าถึงฐานลูกค้าและการแจ้งเตือน</b><br>
                                            </div>
                                            <div class="form-group col-lg-12 col-md-12 col-xs-12">
                                                <input type="checkbox"> <b>แก้ไขข้อมูลส่วนตัว</b><br>
                                            </div>
                                        </div>  
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="col-lg-12 col-md-12 col-xs-12">
                            <div class="form-group col-lg-12 col-md-12 col-xs-12  text-right">
                                <br>
                                <div class="col-lg-12 col-md-12 col-xs-12">
                                    <button class="btn btn-primary" ng-click="onSaveTagClick()"><i class="fa fa-save"></i> <span class="hidden-xs"><?php echo $this->lang->line('Save'); ?></span></button>
                                    <button class="btn btn-danger" ng-click="ShowDevice()"><i class="fa fa-times "></i> <span class="hidden-xs"><?php echo $this->lang->line('Cancel'); ?></span></button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <!-- /.col-lg-12 -->
    </div>
    <!-- /.create room types -->


    <!-- /List.row types-->
    <div class="row DisplayDevice">
        <div class="col-lg-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    จัดการสิทธิ์การใช้งาน
                </div>
                <div class="panel-body">
                    <div class="col-lg-12 col-md-12 col-xs-12">
                        <!-- <button class="btn btn-primary"ng-click="AddNewDevice()"><i class="fa fa-plus  "></i> <span class="hidden-xs"><?php echo $this->lang->line('Add'); ?></span></button> -->
                        <!-- <button class="btn btn-info" ng-click="ShowSearch()"><i class="fa fa-search  "></i> <span class="hidden-xs"><?php echo $this->lang->line('Search'); ?></span></button> -->
                    </div>
                    <div class="col-lg-12 col-md-12 col-xs-12">
                        <div class="table-responsive">
                            <table class="table table-striped">
                                <thead>
                                    <tr>
                                        <th width="5%">#</th>
                                        <th width="20%"class="sorting" ng-click="sort($event)" sort="NAME">สิทธิ์การใช้งาน</th>
                                        <th>การเข้าถึง</th>
                                        <th>จำนวนพนักงาน</th>
                                        <th><?php echo $this->lang->line('Option'); ?></th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr ng-repeat="item in modelDeviceList">
                                        <td>{{$index+1}}</td>
                                        <td ng-bind="item.NAME">เจ้าของ</td>
                                        <td>
                                            <p ng-show="item.PERMISSION_DASHBOARD_IsActive == 1 && item.PERMISSION_POS_IsActive == 1">ระบบหลังร้านและ POS</p>
                                            <p ng-show="item.PERMISSION_DASHBOARD_IsActive == 1 && item.PERMISSION_POS_IsActive == 0">ระบบหลังร้าน</p>
                                            <p ng-show="item.PERMISSION_DASHBOARD_IsActive == 0 && item.PERMISSION_POS_IsActive == 1">ระบบขายหน้าร้าน</p>
                                            <p ng-show="item.PERMISSION_DASHBOARD_IsActive == 0 && item.PERMISSION_POS_IsActive == 0">ไม่มี</p>
                                        </td>
                                        <td ng-bind="item.totalemp"></td>
                                        <td>
                                            <button  ng-click="onEditTagClick(item)" class="btn btn-primary waves-effect waves-light btn-sm m-b-5"><i class="glyphicon glyphicon-edit"></i> <span class="hidden-xs"><?php echo $this->lang->line('Edit'); ?></span></button>
                                            <button  my-confirm-click="onDeleteTagClick(item)" my-confirm-click-message="<?php echo $this->lang->line('DoYouWantToDelete'); ?>" class="btn btn-danger waves-effect waves-light btn-sm m-b-5"><i class="glyphicon glyphicon-trash"></i> <span class="hidden-xs"><?php echo $this->lang->line('Delete'); ?></span></button>
                                        </td>
                                    </tr> 
                                </tbody>
                            </table>
                        </div>
                        <!-- /.table-responsive -->
                    </div>

                    <!-- ทำหน้า -->
                    <div class="row tblResult small">
                        <div class="col-md-7 col-sm-7 col-xs-12 ">
                            <label class="col-md-4 col-sm-4 col-xs-12">
                                <?php echo $this->lang->line('Total'); ?> {{totalRecords}} <?php echo $this->lang->line('Records'); ?>
                            </label>
                            <label class="col-md-4 col-sm-4 col-xs-12">
                                <?php echo $this->lang->line('ResultsPerPage'); ?>
                            </label>
                            <div class="col-md-4 col-sm-4 col-xs-12 ">
                                <ui-select ng-model="TempPageSize.selected" ng-change="loadByPageSize()" theme="selectize">
                                    <ui-select-match>{{$select.selected.Value}}</ui-select-match>
                                    <ui-select-choices repeat="pSize in listPageSize | filter: $select.search">
                                        <span ng-bind-html="pSize.Text | highlight: $select.search"></span>
                                    </ui-select-choices>
                                </ui-select>
                            </div>
                        </div>
                        <div class="col-md-5 col-sm-5 col-xs-12  ">
                            <label class="col-md-4 col-sm-4 col-xs-12">
                                <span ng-click="getBackPage()" class="set-pointer"><i class="fa fa-chevron-left"></i> <span class="hidden-xs"><?php echo $this->lang->line('Previous'); ?></span></span>
                            </label>
                            <div class="col-md-3 col-sm-3 col-xs-12">
                                <ui-select ng-model="TempPageIndex.selected" ng-change="searchByPage()" theme="selectize">
                                    <ui-select-match>{{$select.selected.PageIndex}}</ui-select-match>
                                    <ui-select-choices repeat="pIndex in listPageIndex | filter: $select.search">
                                        <span ng-bind-html="pIndex.PageIndex | highlight: $select.search"></span>
                                    </ui-select-choices>
                                </ui-select>
                            </div>
                            <label class="col-md-4 col-sm-4 col-xs-12">
                                / {{ totalPage }} <span ng-click="getNextPage()" class="set-pointer"><?php echo $this->lang->line('Next'); ?><i class="fa fa-chevron-right set-pointer"></i></span>
                            </label>
                        </div>
                    </div>
                    <!-- ทำหน้า -->

                </div>
                <!-- /.panel-body -->
            </div>
            <!-- /.panel -->
        </div>
        <!-- /.col-lg-12 -->
    </div>
    <!-- /List.row types-->
</div>
</div>