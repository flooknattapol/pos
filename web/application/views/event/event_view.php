<script src="<?php echo base_url('asset/eventController.js'); ?>"></script>
<div ng-controller="eventController" ng-init="onInit()">

    <div class="row">
        <ul class="navigator">
            <?php /*<li class="nav"><a href="/Rooms"><?php echo $this->lang->line('event');?></a></li>*/ ?>
            <li class="nav_active">Event</li>
        </ul>
        <!-- /.col-lg-12 -->
    </div>

    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">Event</h1>
        </div>
        <!-- /.col-lg-12 -->
    </div>

    <!-- /List.row types-->
    <div class="row  SearchDevice" style="display:none;">
        <div class="col-lg-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <?php echo $this->lang->line('Search'); ?>
                </div>
                <div class="panel-body">
                    <div class="form-group col-lg-12 col-md-12 col-xs-12">
                        <div class="col-lg-6 col-md-6 col-xs-12">
                            <label><span class="text-danger">*</span>ชื่ออีเว้นท์</label>
                            <!-- <input class="form-control" ng-model="modelSearch.NAME" maxlength="80"> -->
                            <input list="EventList" ng-model="modelSearch.NAME"class="form-control" >
                            <datalist id="EventList">
                            <option ng-repeat="event in ListEventModel" value="{{event.NAME}}">
                            </datalist>
                            <p class="help-block"></p>
                        </div>
                        <div class="col-lg-6 col-md-6 col-xs-12">
                        </div>
                    </div>
                    <div class="form-group col-lg-12 col-md-12 col-xs-12">


                    </div>
                    <div class="col-lg-12 col-md-12 col-xs-12">
                        <button type="button" class="btn btn-primary waves-effect waves-light m-b-5" ng-click="resetSearch()"><i class="glyphicon glyphicon-repeat"></i> <span class="hidden-xs"><?php echo $this->lang->line('ResetSearch'); ?></span></button>
                        <button type="button" class="btn btn-primary waves-effect waves-light m-b-5" ng-click="LoadSearch()"><i class="fa fa-search"></i> <span class="hidden-xs"><?php echo $this->lang->line('Search'); ?></span></button>
                        <button type="button" class="btn btn-danger waves-effect waves-light m-b-5" ng-click="ShowDevice()"><i class="fa fa-times"></i> <span class="hidden-xs"><?php echo $this->lang->line('Cancel'); ?></span></button>
                    </div>
                </div>
                <!-- /.panel-body -->
            </div>
            <!-- /.panel -->
        </div>
        <!-- /.col-lg-12 -->
    </div>
    <!-- /List.row types-->


    <!-- / create room types  -->
    <div class="row addDevice" style="display:none;">
        <div class="col-lg-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    Manage Event
                </div>
                <div class="panel-body">
                    <div class="row">

                        <div class="col-lg-8 col-md-8 col-xs-12" style="padding-right:0px">
                            <div role="form">
                                <br>
                                <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
                                    <div class="form-group col-lg-12 col-md-12 col-xs-12">
                                    </div>
                                    <div class="form-group col-lg-12 col-md-12 col-xs-12">
                                        <div class="col-lg-3 col-md-3 col-xs-3">
                                            <h5><span class="text-danger"></span>ชื่ออีเว้นท์</h5>
                                        </div>
                                        <div class="col-lg-8 col-md-8 col-xs-8">
                                            <input class="form-control" ng-model="CreateModel.NAME" maxlength="80">
                                            <p class="CreateModel_NAME require text-danger"><?php echo $this->lang->line('Require'); ?></p>
                                        </div>
                                    </div>

                                    <div class="form-group col-lg-12 col-md-12 col-xs-12">
                                        <div class="col-lg-3 col-md-3 col-xs-3">
                                            <h5><span class="text-danger"></span>รายละเอียด</h5>
                                        </div>
                                        <div class="col-lg-8 col-md-8 col-xs-8">
                                            <textarea class="form-control" ng-model="CreateModel.DESCRIPTION" rows="5">
                                            </textarea>
                                            <!-- <input class="form-control" ng-model="CreateModel.DESCRIPTION" maxlength="80"> -->
                                            <p class="CreateModel_DESCRIPTION require text-danger"><?php echo $this->lang->line('Require'); ?></p>
                                        </div>
                                    </div>       
                                    <div class="form-group col-lg-12 col-md-12 col-xs-12">
                                        <div class="col-lg-3 col-md-3 col-xs-3">
                                            <h5><span class="text-danger"></span>วันที่</h5>
                                        </div>
                                        <div class="col-lg-4 col-md-4 col-xs-4">
                                            <input class="form-control"  ng-model="CreateModel.DATE_START"data-date-format="dd-MM-yyyy" bs-datepicker> 
                                            <p class="CreateModel_DATE_START require text-danger"><?php echo $this->lang->line('Require'); ?></p>
                                        </div>
                                    </div>
                                    <div class="form-group col-lg-12 col-md-12 col-xs-12">
                                        <div class="col-lg-3 col-md-3 col-xs-3">
                                            <h5><span class="text-danger"></span>ถึงวันที่</h5>
                                        </div>
                                        <div class="col-lg-4 col-md-4 col-xs-4">
                                            <input class="form-control" ng-model="CreateModel.DATE_END"data-date-format="dd-MM-yyyy" bs-datepicker> 
                                            <p class="CreateModel_DATE_END require text-danger"><?php echo $this->lang->line('Require'); ?></p>
                                        </div>

                                    </div>
                                    <div class="form-group col-lg-12 col-md-12 col-xs-12">
                                        <div class="col-lg-3 col-md-3 col-xs-3">
                                            <h5><span class="text-danger"></span>เวลา</h5>
                                        </div>
                                        <div class="col-lg-4 col-md-4 col-xs-4">
                                            <input class="form-control" type="time"ng-model="CreateModel.TIME"> 
                                            <p class="CreateModel_DATE_END require text-danger"><?php echo $this->lang->line('Require'); ?></p>
                                        </div>

                                    </div>
                                    <!-- <div class="form-group col-lg-12 col-md-12 col-xs-12">
                                        <div class="col-lg-3 col-md-3 col-xs-3">
                                            <h5><span class="text-danger"></span>เวลาเริ่มงาน</h5>
                                        </div>
                                        <div class="col-lg-3 col-md-3 col-xs-3">
                                            <select class="form-control" ng-model="CreateModel.TIME">
                                                <option value="0800">08:00</option>
                                                <option value="0830">08:30</option>
                                                <option value="0900">08:00</option>
                                                <option value="0930">09:30</option>
                                                <option value="1000">10:00</option>
                                                <option value="1030">10:30</option>
                                                <option value="1100">11:00</option>
                                                <option value="1130">11:30</option>
                                                <option value="1200">12:00</option>
                                                <option value="1230">12:30</option>
                                                <option value="1300">13:00</option>
                                                <option value="1330">13:30</option>

                                            </select>
                                            <p class="CreateModel_TIME require text-danger"><?php echo $this->lang->line('Require'); ?></p>
                                        </div>
                                    </div>  -->
                                    
                                    <div class="form-group col-lg-12 col-md-12 col-xs-12">
                                        <div class="col-lg-3 col-md-3 col-xs-3">
                                            <h5><span class="text-danger"></span>สถานที่จัด</h5>
                                        </div>
                                        <div class="col-lg-8 col-md-8 col-xs-8">
                                            <input class="form-control" ng-model="CreateModel.LOCATION">
                                            <p class="CreateModel_LOCATION require text-danger"><?php echo $this->lang->line('Require'); ?></p>
                                        </div>
                                    </div>     
                                    
                                    <div class="form-group col-lg-12 col-md-12 col-xs-12">
                                        <div class="col-lg-3 col-md-3 col-xs-3">
                                            <h5><span class="text-danger"></span>เว็ปไซต์</h5>
                                        </div>
                                        <div class="col-lg-8 col-md-8 col-xs-8">
                                            <input class="form-control" ng-model="CreateModel.WEBSITE">
                                             
           
           
                                            <!-- <p class="CreateModel_name require text-danger"><?php echo $this->lang->line('Require'); ?></p> -->
                                        </div>
                                    </div>  
                                    <div class="form-group col-lg-12 col-md-12 col-xs-12">
                                        <div class="col-lg-3 col-md-3 col-xs-3">
                                            <h5><span class="text-danger"></span>รูปภาพ</h5>
                                        </div>
                                        <div class="col-lg-7 col-md-7 col-xs-12">
                                            <input type="file" onchange="angular.element(this).scope().SelectFile(event)" ngf-select ng-model="filegeneralinfo" name="filegeneralinfo" ngf-pattern="'.png'" ngf-accept="'.png'" ngf-max-size="10MB" ngf-min-height="100"/>
                                        </div>
    
                                    </div>   
                                    <div class="form-group col-lg-12 col-md-12 col-xs-12">
                                        <div class="col-lg-3 col-md-3 col-xs-3">

                                        </div>
                                        <div class="col-lg-7 col-md-7 col-xs-12">
                                        <hr />
                                        <img ng-src="{{PreviewImage}}" ng-show="ViewImgPreview == true" alt="" style="height:300px;width:500px" />   
                                        <img src="<?php echo base_url('/upload/Event/');?>{{CreateModel.IMAGE}}" ng-show="ViewImgPreview == false" alt="" style="height:200px;width:500px" />                                        </div>
                                    </div> 
                                </div>
                            </div>
                        </div>

                        <div class="col-lg-12 col-md-12 col-xs-12">
                            <div class="form-group col-lg-12 col-md-12 col-xs-12  text-right">
                                <br>
                                <div class="col-lg-12 col-md-12 col-xs-12">
                                    <button class="btn btn-primary" ng-click="upload()"><i class="fa fa-save"></i> <span class="hidden-xs"><?php echo $this->lang->line('Save'); ?></span></button>
                                    <button class="btn btn-danger" ng-click="ShowDevice()"><i class="fa fa-times "></i> <span class="hidden-xs"><?php echo $this->lang->line('Cancel'); ?></span></button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <!-- /.col-lg-12 -->
    </div>
    <!-- /.create room types -->


    <!-- /List.row types-->
    <div class="row DisplayDevice">
        <div class="col-lg-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    Manage Event
                </div>
                <div class="panel-body">
                    <div class="col-lg-12 col-md-12 col-xs-12">
                        <button class="btn btn-primary"ng-click="AddNewDevice()"><i class="fa fa-plus  "></i> <span class="hidden-xs"><?php echo $this->lang->line('Add'); ?></span></button>
                        <button class="btn btn-info" ng-click="ShowSearch()"><i class="fa fa-search  "></i> <span class="hidden-xs"><?php echo $this->lang->line('Search'); ?></span></button>
                    </div>
                    <div class="col-lg-12 col-md-12 col-xs-12">
                        <div class="table-responsive">
                            <table class="table table-striped">
                                <thead>
                                    <tr>
                                        <th width="5%">#</th>
                                        <th>รูปภาพ</th>
                                        <th class="sorting" ng-click="sort($event)" sort="DATE_START">วันที่เริ่มงาน</th>
                                        <th class="sorting" ng-click="sort($event)" sort="DATE_END">วันที่สิ้นสุด</th>
                                        <th class="sorting_asc" ng-click="sort($event)" sort="NAME">ชื่ออีเว้นท์</th>
                                        <th class="sorting" ng-click="sort($event)" sort="DESCRIPTION">รายละเอียด</th>
                                        <th class="sorting" ng-click="sort($event)" sort="LOCATION">สถานที่จัด</th>
                                        <th><?php echo $this->lang->line('Option'); ?></th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr ng-repeat="item in modelDeviceList">
                                        <td>{{$index+1}}</td>
                                        <td><img ng-src="<?php echo base_url('/upload/Event/');?>{{item.IMAGE}}" style="height:100px;"></td>
                                        <td ng-bind="item.DATE_START"></td>
                                        <td ng-bind="item.DATE_END "></td>
                                        <td ng-bind="item.NAME"></td>
                                        <td ng-bind="item.DESCRIPTION "></td>
                                        <td ng-bind="item.LOCATION "></td>
                                        <td>
                                            <button ng-click="onEditTagClick(item)" class="btn btn-primary waves-effect waves-light btn-sm m-b-5"><i class="glyphicon glyphicon-edit"></i> <span class="hidden-xs"><?php echo $this->lang->line('Edit'); ?></span></button>
                                            <button my-confirm-click="onDeleteTagClick(item)" my-confirm-click-message="<?php echo $this->lang->line('DoYouWantToDelete'); ?>" class="btn btn-danger waves-effect waves-light btn-sm m-b-5"><i class="glyphicon glyphicon-trash"></i> <span class="hidden-xs"><?php echo $this->lang->line('Delete'); ?></span></button>
                                            <a href="{{item.QRURL}}" target="_blank"><button  class="btn btn-success waves-effect waves-light btn-sm m-b-5"><i class="glyphicon glyphicon-print"></i> <span class="hidden-xs">พิมพ์ QR-Code</span></button></a>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                        <!-- /.table-responsive -->
                    </div>

                    <!-- ทำหน้า -->
                    <div class="row tblResult small">
                        <div class="col-md-7 col-sm-7 col-xs-12 ">
                            <label class="col-md-4 col-sm-4 col-xs-12">
                                <?php echo $this->lang->line('Total'); ?> {{totalRecords}} <?php echo $this->lang->line('Records'); ?>
                            </label>
                            <label class="col-md-4 col-sm-4 col-xs-12">
                                <?php echo $this->lang->line('ResultsPerPage'); ?>
                            </label>
                            <div class="col-md-4 col-sm-4 col-xs-12 ">
                                <ui-select ng-model="TempPageSize.selected" ng-change="loadByPageSize()" theme="selectize">
                                    <ui-select-match>{{$select.selected.Value}}</ui-select-match>
                                    <ui-select-choices repeat="pSize in listPageSize | filter: $select.search">
                                        <span ng-bind-html="pSize.Text | highlight: $select.search"></span>
                                    </ui-select-choices>
                                </ui-select>
                            </div>
                        </div>
                        <div class="col-md-5 col-sm-5 col-xs-12  ">
                            <label class="col-md-4 col-sm-4 col-xs-12">
                                <span ng-click="getBackPage()" class="set-pointer"><i class="fa fa-chevron-left"></i> <span class="hidden-xs"><?php echo $this->lang->line('Previous'); ?></span></span>
                            </label>
                            <div class="col-md-3 col-sm-3 col-xs-12">
                                <ui-select ng-model="TempPageIndex.selected" ng-change="searchByPage()" theme="selectize">
                                    <ui-select-match>{{$select.selected.PageIndex}}</ui-select-match>
                                    <ui-select-choices repeat="pIndex in listPageIndex | filter: $select.search">
                                        <span ng-bind-html="pIndex.PageIndex | highlight: $select.search"></span>
                                    </ui-select-choices>
                                </ui-select>
                            </div>
                            <label class="col-md-4 col-sm-4 col-xs-12">
                                / {{ totalPage }} <span ng-click="getNextPage()" class="set-pointer"><?php echo $this->lang->line('Next'); ?><i class="fa fa-chevron-right set-pointer"></i></span>
                            </label>
                        </div>
                    </div>
                    <!-- ทำหน้า -->

                </div>
                <!-- /.panel-body -->
            </div>
            <!-- /.panel -->
        </div>
        <!-- /.col-lg-12 -->
    </div>
    <!-- /List.row types-->
</div>
</div>