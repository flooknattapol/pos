<script src="<?php echo base_url('asset/customerController.js'); ?>"></script>
<div ng-controller="customerController" ng-init="onInit()">

    <div class="row">
        <ul class="navigator">
            <?php /*<li class="nav"><a href="/Rooms"><?php echo $this->lang->line('event');?></a></li>*/ ?>
            <li class="nav_active">รายชื่อลูกค้า</li>
        </ul>
        <!-- /.col-lg-12 -->
    </div>

    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">รายชื่อลูกค้า</h1>
        </div>
        <!-- /.col-lg-12 -->
    </div>

    <!-- /List.row types-->
    <div class="row  SearchDevice" style="display:none;">
        <div class="col-lg-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <?php echo $this->lang->line('Search'); ?>
                </div>
                <div class="panel-body">
                    <div class="form-group col-lg-12 col-md-12 col-xs-12">
                        <div class="col-lg-6 col-md-6 col-xs-12">
                            <label><span class="text-danger">*</span>ชื่อลูกค้า</label>
                            <input class="form-control" ng-model="modelSearch.NAME" maxlength="80">
                            <p class="help-block"></p>
                        </div>
                    </div>
                    <div class="form-group col-lg-12 col-md-12 col-xs-12">


                    </div>
                    <div class="col-lg-12 col-md-12 col-xs-12">
                        <button type="button" class="btn btn-primary waves-effect waves-light m-b-5" ng-click="resetSearch()"><i class="glyphicon glyphicon-repeat"></i> <span class="hidden-xs"><?php echo $this->lang->line('ResetSearch'); ?></span></button>
                        <button type="button" class="btn btn-primary waves-effect waves-light m-b-5" ng-click="LoadSearch()"><i class="fa fa-search"></i> <span class="hidden-xs"><?php echo $this->lang->line('Search'); ?></span></button>
                        <button type="button" class="btn btn-danger waves-effect waves-light m-b-5" ng-click="ShowDevice()"><i class="fa fa-times"></i> <span class="hidden-xs"><?php echo $this->lang->line('Cancel'); ?></span></button>
                    </div>
                </div>
                <!-- /.panel-body -->
            </div>
            <!-- /.panel -->
        </div>
        <!-- /.col-lg-12 -->
    </div>
    <!-- /List.row types-->


    <!-- / create room types  -->
    <div class="row addDevice" style="display:none;">
        <div class="col-lg-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    จัดการรายชื่อลูกค้า
                </div>
                <div class="panel-body">
                    <div class="row">

                        <div class="col-lg-8 col-md-8 col-xs-12" style="padding-right:0px">
                            <div role="form">
                                <br>
                                <?php if($this->session->userdata('user') == "admin"){ ?>    
                                    <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true"> 
                                        <div class="form-group col-lg-12 col-md-12 col-xs-12">
                                            <div class="col-lg-3 col-md-3 col-xs-3">
                                                <h5><span class="text-danger"></span>ชื่อ</h5>
                                            </div>
                                            <div class="col-lg-8 col-md-8 col-xs-8">
                                                <input class="form-control" ng-model="CreateModel.NAME" >
                                                <p class="CreateModel_NAME require text-danger"><?php echo $this->lang->line('Require'); ?></p>
                                            </div>
                                        </div>  
                                        <div class="form-group col-lg-12 col-md-12 col-xs-12">
                                            <div class="col-lg-3 col-md-3 col-xs-3">
                                                <h5><span class="text-danger"></span>E-Mail</h5>
                                            </div>
                                            <div class="col-lg-8 col-md-8 col-xs-8">
                                                <input class="form-control" ng-model="CreateModel.EMAIL" >
                                                <p class="CreateModel_EMAIL require text-danger"><?php echo $this->lang->line('Require'); ?></p>
                                            </div>
                                        </div>  
                                        <div class="form-group col-lg-12 col-md-12 col-xs-12">
                                            <div class="col-lg-3 col-md-3 col-xs-3">
                                                <h5><span class="text-danger"></span>เบอร์โทรศัพท์</h5>
                                            </div>
                                            <div class="col-lg-8 col-md-8 col-xs-8">
                                                <input class="form-control" ng-model="CreateModel.TEL" >
                                                <p class="CreateModel_TEL require text-danger"><?php echo $this->lang->line('Require'); ?></p>
                                            </div>
                                        </div>  
                                        <div class="form-group col-lg-12 col-md-12 col-xs-12">
                                            <div class="col-lg-3 col-md-3 col-xs-3">
                                                <h5><span class="text-danger"></span>ประเภทลูกค้า</h5>
                                            </div>
                                            <div class="col-lg-8 col-md-8 col-xs-8">
                                            <ui-select ng-model="TempCustomercategoryIndex.selected" theme="selectize" >
                                                    <ui-select-match>{{$select.selected.NAME}}</ui-select-match>
                                                    <ui-select-choices repeat="Item in ListCustomercategory  | filter: $select.search">
                                                        <span ng-bind-html="Item.NAME | highlight: $select.search"></span>
                                                    </ui-select-choices>
                                                </ui-select>
                                                <p class="CreateModel_CATEGORY require text-danger"><?php echo $this->lang->line('Require');?></p>
                                            </div>
                                        </div>  
                                        <div class="form-group col-lg-12 col-md-12 col-xs-12">
                                            <div class="col-lg-3 col-md-3 col-xs-3">
                                                <h5><span class="text-danger"></span>หมายเหตุ</h5>
                                            </div>
                                            <div class="col-lg-8 col-md-8 col-xs-8">
                                                <textarea class="form-control" ng-model="CreateModel.NOTE" rows="3" ></textarea>
                                            </div>
                                        </div>  
                                    </div>
                                <?php }else{?>
                                    <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true"> 
                                        <div class="form-group col-lg-12 col-md-12 col-xs-12">
                                            <div class="col-lg-3 col-md-3 col-xs-3">
                                                <h5><span class="text-danger"></span>ชื่อ</h5>
                                            </div>
                                            <div class="col-lg-8 col-md-8 col-xs-8">
                                                <input class="form-control" ng-model="CreateModel.NAME" disabled>
                                                <p class="CreateModel_NAME require text-danger"><?php echo $this->lang->line('Require'); ?></p>
                                            </div>
                                        </div>  
                                        <div class="form-group col-lg-12 col-md-12 col-xs-12">
                                            <div class="col-lg-3 col-md-3 col-xs-3">
                                                <h5><span class="text-danger"></span>E-Mail</h5>
                                            </div>
                                            <div class="col-lg-8 col-md-8 col-xs-8">
                                                <input class="form-control" ng-model="CreateModel.EMAIL" disabled>
                                                <p class="CreateModel_EMAIL require text-danger"><?php echo $this->lang->line('Require'); ?></p>
                                            </div>
                                        </div>  
                                        <div class="form-group col-lg-12 col-md-12 col-xs-12">
                                            <div class="col-lg-3 col-md-3 col-xs-3">
                                                <h5><span class="text-danger"></span>เบอร์โทรศัพท์</h5>
                                            </div>
                                            <div class="col-lg-8 col-md-8 col-xs-8">
                                                <input class="form-control" ng-model="CreateModel.TEL" disabled>
                                                <p class="CreateModel_TEL require text-danger"><?php echo $this->lang->line('Require'); ?></p>
                                            </div>
                                        </div>  
                                        <div class="form-group col-lg-12 col-md-12 col-xs-12">
                                            <div class="col-lg-3 col-md-3 col-xs-3">
                                                <h5><span class="text-danger"></span>ประเภทลูกค้า</h5>
                                            </div>
                                            <div class="col-lg-8 col-md-8 col-xs-8">
                                            <ui-select ng-model="TempCustomercategoryIndex.selected" theme="selectize" disabled>
                                                    <ui-select-match>{{$select.selected.NAME}}</ui-select-match>
                                                    <ui-select-choices repeat="Item in ListCustomercategory  | filter: $select.search">
                                                        <span ng-bind-html="Item.NAME | highlight: $select.search"></span>
                                                    </ui-select-choices>
                                                </ui-select>
                                                <p class="CreateModel_CATEGORY require text-danger"><?php echo $this->lang->line('Require');?></p>
                                            </div>
                                        </div>  
                                        <div class="form-group col-lg-12 col-md-12 col-xs-12">
                                            <div class="col-lg-3 col-md-3 col-xs-3">
                                                <h5><span class="text-danger"></span>หมายเหตุ</h5>
                                            </div>
                                            <div class="col-lg-8 col-md-8 col-xs-8">
                                                <textarea class="form-control" ng-model="CreateModel.NOTE" rows="3" disabled></textarea>
                                            </div>
                                        </div>  
                                    </div>
                                <?php }?>
                                
                            </div>
                        </div>

                        <div class="col-lg-12 col-md-12 col-xs-12">
                            <div class="form-group col-lg-12 col-md-12 col-xs-12  text-right">
                                <br>
                                <div class="col-lg-12 col-md-12 col-xs-12">
                                <?php if($this->session->userdata('user') == "admin"){ ?>
                                    <button class="btn btn-primary" ng-click="onSaveTagClick()"><i class="fa fa-save"></i> <span class="hidden-xs"><?php echo $this->lang->line('Save'); ?></span></button>
                                <?php }?>
                                    <button class="btn btn-danger" ng-click="ShowDevice()"><i class="fa fa-times "></i> <span class="hidden-xs">ย้อนกลับ</span></button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <!-- /.col-lg-12 -->
    </div>
    <!-- /.create room types -->


    <!-- /List.row types-->
    <div class="row DisplayDevice">
        <div class="col-lg-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    จัดการรายชื่อลูกค้า
                </div>
                <div class="panel-body">
                    <div class="col-lg-12 col-md-12 col-xs-12">
                    <?php if($this->session->userdata('user') == "admin"){ ?>         
                        <button class="btn btn-primary"ng-click="AddNewDevice()"><i class="fa fa-plus  "></i> <span class="hidden-xs"><?php echo $this->lang->line('Add'); ?></span></button>
                    <?php }?>
                        <button class="btn btn-info" ng-click="ShowSearch()"><i class="fa fa-search  "></i> <span class="hidden-xs"><?php echo $this->lang->line('Search'); ?></span></button>
                    </div>
                    <div class="col-lg-12 col-md-12 col-xs-12">
                        <div class="table-responsive">
                            <table class="table table-striped">
                                <thead>
                                    <tr>
                                        <th width="5%">#</th>
                                        <th width="15%"class="sorting" ng-click="sort($event)" sort="NAME">ลูกค้า</th>
                                        <th width="15%">E-Mail</th>
                                        <th width="15%">เบอร์โทรศัพท์</th>
                                        <th width="10%">ประเภทลูกค้า</th>
                                        <!-- <th width="15%"class="sorting" ng-click="sort($event)" sort="UNIT">เยี่ยมชมครั้งแรก</th>
                                        <th width="15%"class="sorting" ng-click="sort($event)" sort="UNIT">เยี่ยมชมครั้งล่าสุด</th>
                                        <th width="15%"class="sorting" ng-click="sort($event)" sort="UNIT">เข้าชมทั้งหมด</th> -->
                                        <th width="10%">ยอดจ่ายทั้งหมด</th>
                                        <th width="10%">แต้มสะสม</th>

                                        <th><?php echo $this->lang->line('Option'); ?></th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr ng-repeat="item in modelDeviceList">
                                        <td>{{$index+1}}</td>
                                        <td ng-bind="item.NAME"></td>
                                        <td ng-bind="item.EMAIL"></td>
                                        <td ng-bind="item.TEL"></td>
                                        <td ng-bind="item.CATEGORYNAME"></td>
                                        <td ng-bind="item.AMOUNTBUY"></td>
                                        <td ng-bind="item.REWARDPOINT"></td>
                                        <td>
                                            <button  ng-click="onEditTagClick(item)" class="btn btn-primary waves-effect waves-light btn-sm m-b-5"><i class="glyphicon glyphicon-edit"></i> <span class="hidden-xs"><?php echo $this->lang->line('Edit'); ?></span></button>
                                            <button  my-confirm-click="onDeleteTagClick(item)" my-confirm-click-message="<?php echo $this->lang->line('DoYouWantToDelete'); ?>" class="btn btn-danger waves-effect waves-light btn-sm m-b-5"><i class="glyphicon glyphicon-trash"></i> <span class="hidden-xs"><?php echo $this->lang->line('Delete'); ?></span></button>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                        <!-- /.table-responsive -->
                    </div>

                    <!-- ทำหน้า -->
                    <div class="row tblResult small">
                        <div class="col-md-7 col-sm-7 col-xs-12 ">
                            <label class="col-md-4 col-sm-4 col-xs-12">
                                <?php echo $this->lang->line('Total'); ?> {{totalRecords}} <?php echo $this->lang->line('Records'); ?>
                            </label>
                            <label class="col-md-4 col-sm-4 col-xs-12">
                                <?php echo $this->lang->line('ResultsPerPage'); ?>
                            </label>
                            <div class="col-md-4 col-sm-4 col-xs-12 ">
                                <ui-select ng-model="TempPageSize.selected" ng-change="loadByPageSize()" theme="selectize">
                                    <ui-select-match>{{$select.selected.Value}}</ui-select-match>
                                    <ui-select-choices repeat="pSize in listPageSize | filter: $select.search">
                                        <span ng-bind-html="pSize.Text | highlight: $select.search"></span>
                                    </ui-select-choices>
                                </ui-select>
                            </div>
                        </div>
                        <div class="col-md-5 col-sm-5 col-xs-12  ">
                            <label class="col-md-4 col-sm-4 col-xs-12">
                                <span ng-click="getBackPage()" class="set-pointer"><i class="fa fa-chevron-left"></i> <span class="hidden-xs"><?php echo $this->lang->line('Previous'); ?></span></span>
                            </label>
                            <div class="col-md-3 col-sm-3 col-xs-12">
                                <ui-select ng-model="TempPageIndex.selected" ng-change="searchByPage()" theme="selectize">
                                    <ui-select-match>{{$select.selected.PageIndex}}</ui-select-match>
                                    <ui-select-choices repeat="pIndex in listPageIndex | filter: $select.search">
                                        <span ng-bind-html="pIndex.PageIndex | highlight: $select.search"></span>
                                    </ui-select-choices>
                                </ui-select>
                            </div>
                            <label class="col-md-4 col-sm-4 col-xs-12">
                                / {{ totalPage }} <span ng-click="getNextPage()" class="set-pointer"><?php echo $this->lang->line('Next'); ?><i class="fa fa-chevron-right set-pointer"></i></span>
                            </label>
                        </div>
                    </div>
                    <!-- ทำหน้า -->

                </div>
                <!-- /.panel-body -->
            </div>
            <!-- /.panel -->
        </div>
        <!-- /.col-lg-12 -->
    </div>
    <!-- /List.row types-->
</div>
</div>