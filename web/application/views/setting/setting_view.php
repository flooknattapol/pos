<script src="<?php echo base_url('asset/settingController.js'); ?>"></script>
<div ng-controller="settingController" ng-init="onInit()">

    <div class="row">
        <ul class="navigator">
            <?php /*<li class="nav"><a href="/Rooms"><?php echo $this->lang->line('event');?></a></li>*/ ?>
            <li class="nav_active">ตั้งค่า</li>
        </ul>
        <!-- /.col-lg-12 -->
    </div>

    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">ตั้งค่า</h1>
        </div>
        <!-- /.col-lg-12 -->
    </div>

    <!-- /List.row types-->
    <div class="row  SearchDevice" style="display:none;">

    </div>
    <!-- /List.row types-->


    <!-- / create room types  -->
    <div class="row addDevice" style="display:none;">
        
    </div>
    <!-- /.create room types -->


    <!-- /List.row types-->
    <div class="row DisplayDevice">
        <div class="col-lg-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    ตั้งค่า
                </div>
                <div class="panel-body">
                    <div class="col-lg-12 col-md-12 col-xs-12">
                        <div class="panel-group">
                            <div class="form-group col-lg-12 col-md-12 col-xs-12">
                                <div class="col-lg-3 col-md-3 col-xs-3">
                                    <h5><span class="text-danger"></span>E-Mail ร้านค้า</h5>
                                </div>
                                <div class="col-lg-8 col-md-8 col-xs-8">
                                    <input class="form-control" ng-model="CreateModel.EMAIL">
                                    <p class="CreateModel_EMAIL require text-danger"><?php echo $this->lang->line('Require'); ?></p>
                                </div>
                            </div>  
                            <div class="form-group col-lg-12 col-md-12 col-xs-12">
                                <div class="col-lg-3 col-md-3 col-xs-3">
                                    <h5><span class="text-danger"></span>ชื่อร้านค้า</h5>
                                </div>
                                <div class="col-lg-8 col-md-8 col-xs-8">
                                    <input class="form-control"ng-model="CreateModel.NAME" >
                                    <p class="CreateModel_NAME require text-danger"><?php echo $this->lang->line('Require'); ?></p>
                                </div>
                            </div> 
                            <div class="form-group col-lg-12 col-md-12 col-xs-12">
                                <div class="col-lg-3 col-md-3 col-xs-3">
                                    <h5><span class="text-danger"></span>เปิดใช้งานคะแนน</h5>
                                </div>
                                <div class="col-lg-8 col-md-8 col-xs-8">
                                    <select class="form-control" ng-model="CreateModel.SETTING_BOOLEAN_POINT" >
                                        <option value="1">เปิด</option>
                                        <option value="0">ปิด</option>
                                    </select>
                                    <p class="CreateModel_SETTING_BOOLEAN_POINT require text-danger"><?php echo $this->lang->line('Require'); ?></p>
                                </div>
                            </div> 
                            <div class="form-group col-lg-12 col-md-12 col-xs-12" ng-show="CreateModel.SETTING_BOOLEAN_POINT == 1">
                                <div class="col-lg-3 col-md-3 col-xs-3">
                                    <h5><span class="text-danger"></span>1 คะแนน เท่ากับ</h5>
                                </div>
                                <div class="col-lg-8 col-md-8 col-xs-8">
                                    <input class="form-control" ng-model="CreateModel.SETTING_BAHTTOPOINT">
                                    <p class="CreateModel_SETTING_BAHTTOPOINT require text-danger"><?php echo $this->lang->line('Require'); ?></p>
                                </div>
                            </div> 
                            <div class="form-group col-lg-12 col-md-12 col-xs-12">
                                <div class="col-lg-3 col-md-3 col-xs-3">
                                    <h5><span class="text-danger"></span>เปิดคืนคะแนนหลังจากยกเลิกใบเสร็จ</h5>
                                </div>
                                <div class="col-lg-8 col-md-8 col-xs-8">
                                    <select class="form-control" ng-model="CreateModel.SETTING_REFUND_POINT">
                                        <option value="1">เปิด</option>
                                        <option value="0">ปิด</option>
                                    </select>
                                    <p class="CreateModel_SETTING_REFUND_POINT require text-danger"><?php echo $this->lang->line('Require'); ?></p>
                                </div>
                            </div> 
                        </div>
                        <div class="col-lg-12 col-md-12 col-xs-12">
                            <div class="form-group col-lg-12 col-md-12 col-xs-12  text-right">
                                <br>
                                <div class="col-lg-12 col-md-12 col-xs-12">
                                    <button class="btn btn-primary" ng-click="onSaveTagClick()"><i class="fa fa-save"></i> <span class="hidden-xs"><?php echo $this->lang->line('Save'); ?></span></button>
                                </div>
                            </div>
                        </div>
                        <!-- <ul class="nav nav-tabs">
                            <li class="active"><a data-toggle="tab" href="#home">ตั้งค่าทั่วไป</a></li>
                            <li><a data-toggle="tab" href="#menu1">ระบบบิลลิ่งและสมัครเป็นสมาชิก</a></li>
                            <li><a data-toggle="tab" href="#menu2">ประเภทการชำระเงิน</a></li>

                        </ul>

                        <div class="tab-content">
                            <div id="home" class="tab-pane fade in active">
                                <div class="panel-group">
                                    <div class="form-group col-lg-12 col-md-12 col-xs-12">
                                        <div class="col-lg-3 col-md-3 col-xs-3">
                                            <h5><span class="text-danger"></span>E-Mail</h5>
                                        </div>
                                        <div class="col-lg-8 col-md-8 col-xs-8">
                                            <input class="form-control">
                                        </div>
                                    </div>  
                                    <div class="form-group col-lg-12 col-md-12 col-xs-12">
                                        <div class="col-lg-3 col-md-3 col-xs-3">
                                            <h5><span class="text-danger"></span>ชื่อร้านค้า</h5>
                                        </div>
                                        <div class="col-lg-8 col-md-8 col-xs-8">
                                            <input class="form-control" >
                                        </div>
                                    </div> 
                                    <div class="form-group col-lg-12 col-md-12 col-xs-12">
                                        <input type="checkbox"> <b>กะ</b><br>
                                        <small>ติดตามเงินที่เข้าออกของลิ้นชักเก็บเงิน <a>เรียน​รู้​เพิ่ม​เติม</a></small>
                                    </div>
                                    <div class="form-group col-lg-12 col-md-12 col-xs-12">
                                        <input type="checkbox"> <b>เปิดตั๋ว</b><br>
                                        <small>อนุญาตให้บันทึก และแก้ไขใบออเดอร์ก่อนการชำระเงิน <a>เรียน​รู้​เพิ่ม​เติม</a></small>
                                    </div>
                                    <div class="form-group col-lg-12 col-md-12 col-xs-12">
                                        <input type="checkbox"> <b>เครื่องพิมพ์ในห้องครัว</b><br>
                                        <small>ส่งใบสั่งไปยังเครื่องพิมพ์ในห้องครัว หรือจอภาพแสดงผล</a></small>
                                    </div>
                                    <div class="form-group col-lg-12 col-md-12 col-xs-12">
                                        <input type="checkbox"> <b>ระบบจอแสดงผลฝั่งลูกค้า</b><br>
                                        <small>แสดงหน้าจอขายที่กำลังสั่งซื้อให้กับลูกค้าเห็นอย่างชัดเจน <a>เรียน​รู้​เพิ่ม​เติม</a></small>
                                    </div>
                                    <div class="form-group col-lg-12 col-md-12 col-xs-12">
                                        <input type="checkbox"> <b>ทางเลือกสั่งออเดอร์</b><br>
                                        <small>ทำหมายเหตุในใบออเดอร์เป็น รับประทานในร้าน สั่งซื้อกลับบ้าน หรือบริการดิลิเวอรี่ <a>เรียน​รู้​เพิ่ม​เติม</a></small>
                                    </div>
                                </div>
                                 
                            </div>
                            <div id="menu1" class="tab-pane fade">
                                <h3>Menu 1</h3>
                                <p>Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>
                            </div>
                            <div id="menu2" class="tab-pane fade">
                                <h3>Menu 2</h3>
                                <p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam.</p>
                            </div>
                        </div> -->
                    </div>
                </div>
                <!-- /.panel-body -->
            </div>
            <!-- /.panel -->
        </div>
        <!-- /.col-lg-12 -->
    </div>
    <!-- /List.row types-->
</div>
</div>