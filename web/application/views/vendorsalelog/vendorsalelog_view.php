<script src="<?php echo base_url('asset/vendorsalelogController.js'); ?>"></script>
<div ng-controller="vendorsalelogController" ng-init="onInit()">

    <div class="row">
        <ul class="navigator">
            <?php /*<li class="nav"><a href="/Rooms"><?php echo $this->lang->line('member');?></a></li>*/ ?>
            <li class="nav_active">Report Vendor Sale</li>
        </ul>
        <!-- /.col-lg-12 -->
    </div>

    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">Report Vendor Sale</h1>
        </div>
        <!-- /.col-lg-12 -->
    </div>

    <!-- /List.row types-->
    <div class="row  SearchDevice" style="display:none;">
        <div class="col-lg-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <?php echo $this->lang->line('Search'); ?>
                </div>
                <div class="panel-body">
                    <div class="form-group col-lg-12 col-md-12 col-xs-12">
                        <div class="col-lg-6 col-md-6 col-xs-12">
                            <label><span class="text-danger" >*</span>อีเว้นท์</label>
                            <input list="EventList" ng-model="modelSearch.NAME"class="form-control" >
                            <datalist id="EventList">
                            <option ng-repeat="event in ListEventModel" value="{{event.NAME}}">
                            </datalist>
                            <p class="help-block"></p>
                        </div>
                        <div class="col-lg-6 col-md-6 col-xs-12">
                        </div>
                    </div>
                    <div class="form-group col-lg-12 col-md-12 col-xs-12">


                    </div>
                    <div class="col-lg-12 col-md-12 col-xs-12">
                        <button type="button" class="btn btn-primary waves-effect waves-light m-b-5" ng-click="resetSearch()"><i class="glyphicon glyphicon-repeat"></i> <span class="hidden-xs"><?php echo $this->lang->line('ResetSearch'); ?></span></button>
                        <button type="button" class="btn btn-primary waves-effect waves-light m-b-5" ng-click="LoadSearch()"><i class="fa fa-search"></i> <span class="hidden-xs"><?php echo $this->lang->line('Search'); ?></span></button>
                        <button type="button" class="btn btn-danger waves-effect waves-light m-b-5" ng-click="ShowDevice()"><i class="fa fa-times"></i> <span class="hidden-xs"><?php echo $this->lang->line('Cancel'); ?></span></button>
                    </div>
                </div>
                <!-- /.panel-body -->
            </div>
            <!-- /.panel -->
        </div>
        <!-- /.col-lg-12 -->
    </div>
    <!-- /List.row types-->


    <!-- / create room types  -->
    <div class="row addDevice" style="display:none;">
        <div class="col-lg-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    Manage User
                </div>
                <div class="panel-body">
                    <div class="row">

                        <div class="col-lg-8 col-md-8 col-xs-12" style="padding-right:0px">
                            <div role="form">
                                <br>
                                <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
                                    <div class="form-group col-lg-12 col-md-12 col-xs-12">
                                    </div>
                                    <div class="form-group col-lg-12 col-md-12 col-xs-12">
                                        <div class="col-lg-3 col-md-3 col-xs-3">
                                            <h5><span class="text-danger"></span>ชื่ออีเว้นท์</h5>
                                        </div>
                                        <div class="col-lg-8 col-md-8 col-xs-8">
                                            <input class="form-control" ng-model="CreateModel.NAME" maxlength="80">
                                            <p class="CreateModel_NAME require text-danger"><?php echo $this->lang->line('Require'); ?></p>
                                        </div>
                                    </div>

                                    <div class="form-group col-lg-12 col-md-12 col-xs-12">
                                        <div class="col-lg-3 col-md-3 col-xs-3">
                                            <h5><span class="text-danger"></span>รายละเอียด</h5>
                                        </div>
                                        <div class="col-lg-8 col-md-8 col-xs-8">
                                            <textarea class="form-control" ng-model="CreateModel.DESCRIPTION" rows="5">
                                            </textarea>
                                            <!-- <input class="form-control" ng-model="CreateModel.DESCRIPTION" maxlength="80"> -->
                                            <p class="CreateModel_DESCRIPTION require text-danger"><?php echo $this->lang->line('Require'); ?></p>
                                        </div>
                                    </div>       
                                    <div class="form-group col-lg-12 col-md-12 col-xs-12">
                                        <div class="col-lg-3 col-md-3 col-xs-3">
                                            <h5><span class="text-danger"></span>สถานที่จัด</h5>
                                        </div>
                                        <div class="col-lg-8 col-md-8 col-xs-8">
                                            <input class="form-control" ng-model="CreateModel.LOCATION">
                                            <p class="CreateModel_LOCATION require text-danger"><?php echo $this->lang->line('Require'); ?></p>
                                        </div>
                                    </div>     
                                    <div class="form-group col-lg-12 col-md-12 col-xs-12">
                                        <div class="col-lg-3 col-md-3 col-xs-3">
                                            <h5><span class="text-danger"></span>รูปภาพงาน</h5>
                                        </div>
                                        <div class="col-lg-8 col-md-8 col-xs-8">
                                            <input class="form-control" ng-model="CreateModel.IMAGE">
                                            <!-- <p class="CreateModel_name require text-danger"><?php echo $this->lang->line('Require'); ?></p> -->
                                        </div>
                                    </div>  
                                    <div class="form-group col-lg-12 col-md-12 col-xs-12">
                                        <div class="col-lg-3 col-md-3 col-xs-3">
                                            <h5><span class="text-danger"></span>รูปภาพที่จัดงาน</h5>
                                        </div>
                                        <div class="col-lg-8 col-md-8 col-xs-8">
                                            <input class="form-control" ng-model="CreateModel.PICTURE">
                                            <!-- <p class="CreateModel_name require text-danger"><?php echo $this->lang->line('Require'); ?></p> -->
                                        </div>
                                    </div>  
                                    <div class="form-group col-lg-12 col-md-12 col-xs-12">
                                        <div class="col-lg-3 col-md-3 col-xs-3">
                                            <h5><span class="text-danger"></span>เว็ปไซต์</h5>
                                        </div>
                                        <div class="col-lg-8 col-md-8 col-xs-8">
                                            <input class="form-control" ng-model="CreateModel.WEBSITE">
                                            <!-- <p class="CreateModel_name require text-danger"><?php echo $this->lang->line('Require'); ?></p> -->
                                        </div>
                                    </div>   
                                </div>
                            </div>
                        </div>

                        <div class="col-lg-12 col-md-12 col-xs-12">
                            <div class="form-group col-lg-12 col-md-12 col-xs-12  text-right">
                                <br>
                                <div class="col-lg-12 col-md-12 col-xs-12">
                                    <button class="btn btn-primary" ng-click="onSaveTagClick()"><i class="fa fa-save"></i> <span class="hidden-xs"><?php echo $this->lang->line('Save'); ?></span></button>
                                    <button class="btn btn-danger" ng-click="ShowDevice()"><i class="fa fa-times "></i> <span class="hidden-xs"><?php echo $this->lang->line('Cancel'); ?></span></button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <!-- /.col-lg-12 -->
    </div>
    <!-- /.create room types -->


    <!-- /List.row types-->
    <div class="row DisplayDevice">
        <div class="col-lg-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                Report Vendor Sale 
                </div>
                <div class="panel-body">
                    <div class="col-lg-12 col-md-12 col-xs-12">
                        <!-- <button class="btn btn-primary"ng-click="AddNewDevice()"><i class="fa fa-plus  "></i> <span class="hidden-xs"><?php echo $this->lang->line('Add'); ?></span></button> -->
                        <button class="btn btn-info" ng-click="ShowSearch()"><i class="fa fa-search  "></i> <span class="hidden-xs"><?php echo $this->lang->line('Search'); ?></span></button>
                    </div>
                    <div>
                        <br>
                    </div>
                    <div class="col-lg-12 col-md-12 col-xs-12">
                        <div class="table-responsive">
                            <table class="table table-striped">
                                <thead>
                                    <tr>
                                        <th width="5%">#</th>
                                        <th width="15%"class="sorting_asc" ng-click="sort($event)" sort="Event_NAME">ชื่ออีเว้นท์</th>
                                        <th width="15%"class="sorting" ng-click="sort($event)" sort="Member_NAME">Member</th>
                                        <th width="10%"class="sorting" ng-click="sort($event)" sort="Goods_NAME">สินค้า</th>
                                        <th width="5%"class="sorting" ng-click="sort($event)" sort="QTY">จำนวน</th>
                                        <th width="15%"class="sorting" ng-click="sort($event)" sort="VENDORSHOP">ร้านค้า</th>
                                        <th width="15%"class="sorting" ng-click="sort($event)" sort="Vendor_NAME">คนขาย</th>
                                        <th width="10%"class="sorting" ng-click="sort($event)" sort="VOUCHER">ใช้ส่วนลดไป</th>
                                        <th width="10%"class="sorting" ng-click="sort($event)" sort="TimeStamp">เวลาบันทึก</th>
                                        
                                        <!-- <th><?php echo $this->lang->line('Option'); ?></th> -->
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr ng-repeat="item in modelDeviceList">
                                        <td>{{$index+1}}</td>
                                        <td ng-bind="item.Event_NAME "></td>
                                        <td ng-bind="item.Member_NAME"></td>                                 
                                        <td ng-bind="item.Goods_NAME "></td>
                                        <td align="right"ng-bind="item.QTY | number:0"></td>
                                        <td ng-bind="item.VENDORSHOP "></td>
                                        <td ng-bind="item.Vendor_NAME "></td>
                                        <td align="right"ng-bind="item.VOUCHER | number:0"></td>
                                        <td ng-bind="item.TimeStamp "></td>

                                        <!-- <td>
                                            <button ng-click="onEditTagClick(item)" class="btn btn-primary waves-effect waves-light btn-sm m-b-5"><i class="glyphicon glyphicon-edit"></i> <span class="hidden-xs"><?php echo $this->lang->line('Edit'); ?></span></button>
                                            <button my-confirm-click="onDeleteTagClick(item)" my-confirm-click-message="<?php echo $this->lang->line('DoYouWantToDelete'); ?>" class="btn btn-danger waves-effect waves-light btn-sm m-b-5"><i class="glyphicon glyphicon-trash"></i> <span class="hidden-xs"><?php echo $this->lang->line('Delete'); ?></span></button>
                                            <button my-confirm-click="onDeleteTagClick(item)" my-confirm-click-message="<?php echo $this->lang->line('DoYouWantToDelete'); ?>" class="btn btn-danger waves-effect waves-light btn-sm m-b-5"><i class="glyphicon glyphicon-trash"></i> <span class="hidden-xs">ปิดการใช้งาน</span></button>
                                        </td> -->
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                        <!-- /.table-responsive -->
                    </div>

                    <!-- ทำหน้า -->
                    <div class="row tblResult small">
                        <div class="col-md-7 col-sm-7 col-xs-12 ">
                            <label class="col-md-4 col-sm-4 col-xs-12">
                                <?php echo $this->lang->line('Total'); ?> {{totalRecords}} <?php echo $this->lang->line('Records'); ?>
                            </label>
                            <label class="col-md-4 col-sm-4 col-xs-12">
                                <?php echo $this->lang->line('ResultsPerPage'); ?>
                            </label>
                            <div class="col-md-4 col-sm-4 col-xs-12 ">
                                <ui-select ng-model="TempPageSize.selected" ng-change="loadByPageSize()" theme="selectize">
                                    <ui-select-match>{{$select.selected.Value}}</ui-select-match>
                                    <ui-select-choices repeat="pSize in listPageSize | filter: $select.search">
                                        <span ng-bind-html="pSize.Text | highlight: $select.search"></span>
                                    </ui-select-choices>
                                </ui-select>
                            </div>
                        </div>
                        <div class="col-md-5 col-sm-5 col-xs-12  ">
                            <label class="col-md-4 col-sm-4 col-xs-12">
                                <span ng-click="getBackPage()" class="set-pointer"><i class="fa fa-chevron-left"></i> <span class="hidden-xs"><?php echo $this->lang->line('Previous'); ?></span></span>
                            </label>
                            <div class="col-md-3 col-sm-3 col-xs-12">
                                <ui-select ng-model="TempPageIndex.selected" ng-change="searchByPage()" theme="selectize">
                                    <ui-select-match>{{$select.selected.PageIndex}}</ui-select-match>
                                    <ui-select-choices repeat="pIndex in listPageIndex | filter: $select.search">
                                        <span ng-bind-html="pIndex.PageIndex | highlight: $select.search"></span>
                                    </ui-select-choices>
                                </ui-select>
                            </div>
                            <label class="col-md-4 col-sm-4 col-xs-12">
                                / {{ totalPage }} <span ng-click="getNextPage()" class="set-pointer"><?php echo $this->lang->line('Next'); ?><i class="fa fa-chevron-right set-pointer"></i></span>
                            </label>
                        </div>
                    </div>
                    <!-- ทำหน้า -->

                </div>
                <!-- /.panel-body -->
            </div>
            <!-- /.panel -->
        </div>
        <!-- /.col-lg-12 -->
    </div>
    <!-- /List.row types-->
</div>
</div>