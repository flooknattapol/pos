<script src="<?php echo base_url('asset/dashboardController.js'); ?>"></script>
<script src="<?php echo base_url('dist/Chart.min.js'); ?>"></script>
<script src="<?php echo base_url('utils.js'); ?>"></script>
<script src="<?php echo base_url('chartjs-plugin-datalabels.js'); ?>"></script>
<div ng-controller="dashboardController" ng-init="onInit()">

    <div class="row">
        <ul class="navigator">
            <?php /*<li class="nav"><a href="/Rooms"><?php echo $this->lang->line('event');?></a></li>*/ ?>
            <li class="nav_active">สรุปยอดขาย</li>
        </ul>
        <!-- /.col-lg-12 -->
    </div>

    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">สรุปยอดขาย</h1>
        </div>
        <!-- /.col-lg-12 -->
    </div>
    <!-- /List.row types-->
    <div class="row DisplayDevice">
        <div class="col-lg-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    สรุปยอดขาย
                </div>
                <div class="panel-body">
                    <div class="col-lg-12 col-md-12 col-xs-12">
                    <div class="col-lg-12 col-md-12 col-xs-12">
                        <div class="col-lg-3 col-md-3 col-xs-3">
                            <small>วันที่เริ่มต้น</small>
                            <input  class="form-control"ng-model="modelSearch.DateStart" data-date-format="dd-MM-yyyy" bs-datepicker>
                        </div>
                        <div class="col-lg-3 col-md-3 col-xs-3">
                            <small>วันที่สิ้นสุด</small>
                            <input  class="form-control"ng-model="modelSearch.DateEnd" data-date-format="dd-MM-yyyy" bs-datepicker>
                        </div>

                        <div class="col-lg-3 col-md-3 col-xs-3"><br>
                            <button class="btn btn-block btn-primary" ng-click="reload()">ค้นหา</button>
                        </div>
                    </div>
                    </div>
                    <div class="col-lg-12 col-md-12 col-xs-12">
                        <hr>
                    </div>
                    <div class="col-lg-12 col-md-12 col-xs-12">
                        <div class="col-lg-2 col-md-2 col-xs-12">
                            <div class="card"style="background-color:#0099CC">
                                <div class="row" >
                                    <div style="text-align: center">
                                        <h3 style="color:#FFFFFF;"><b>ยอดขาย</b></h3> 
                                        <h4 style="color:#FFFFFF;"><b>{{Header.Total}}</b></h4> 
                                    </div>									
                                </div>
                            </div>
                        </div>	
                        <div class="col-lg-2 col-md-2 col-xs-12">
                            <div class="card"style="background-color:#009900;">
                                <div class="row">
                                    <div style="text-align: center">
                                        <h3 style="color:#FFFFFF;"><b>คืนเงิน</b></h3> 
                                        <h4 style="color:#FFFFFF;"><b>{{Header.Refund}}</b></h4> 
                                    </div>
                                </div>	
                            </div>
                        </div>		
                        <div class="col-lg-2 col-md-2 col-xs-12">
                            <div class="card"style="background-color:#528B8B;">
                                <div class="row">
                                    <div style="text-align: center">
                                        <h3 style="color:#FFFFFF;"><b>ส่วนลด</b></h3> 
                                        <h4 style="color:#FFFFFF;"><b>{{Header.Discount}}</b></h4> 
                                    </div>									
                                </div>
                            </div>
                        </div>	
                        <div class="col-lg-2 col-md-2 col-xs-12">
                            <div class="card"style="background-color:#FF6699;">
                                <div class="row">
                                    <div style="text-align: center">
                                        <h3 style="color:#FFFFFF;"><b>กำไรสุทธิ</b></h3> 
                                        <h4 style="color:#FFFFFF;"><b>{{Header.Total-Header.Refund-Header.Discount}}</b></h4> 
                                    </div>									
                                </div>
                            </div>
                        </div>	
                        <div class="col-lg-2 col-md-2 col-xs-12">
                            <div class="card"style="background-color:#FF9900;">
                                <div class="row">
                                    <div style="text-align: center">
                                        <h3 style="color:#FFFFFF;"><b>กำไรรวม</b></h3> 
                                        <h4 style="color:#FFFFFF;"><b>{{(Header.Total-Header.Refund-Header.Discount)-(Header.Cost- Header.TotalCostRefund)}}</b></h4> 
                                    </div>									
                                </div>
                            </div>
                        </div>	
                        <div class="col-lg-2 col-md-2 col-xs-12">
                            
                        </div>			
                    </div>
                    <div class="col-lg-12 col-md-12 col-xs-12">
                        <hr>
                    </div>
                    <div class="col-lg-12 col-md-12 col-xs-12">
                        <div style="width:100%">
                            <canvas id="canvas"></canvas>
                        </div>						
                    </div>
                    <div class="col-lg-12 col-md-12 col-xs-12"></hr>
                        <h3>รายการขาย</h3>
                    </div>
                    <div class="col-lg-12 col-md-12 col-xs-12">
                        <div class="table-responsive">
                            <table class="table table-striped">
                                <thead>
                                    <tr>
                                        <th width="10%"class="sorting" ng-click="sort($event)" sort="UNIT">วันที่</th>
                                        <th width="15%"class="sorting" ng-click="sort($event)" sort="UNIT">ยอดขาย</th>
                                        <th width="15%"class="sorting" ng-click="sort($event)" sort="UNIT">คืนเงิน</th>
                                        <th width="15%"class="sorting" ng-click="sort($event)" sort="UNIT">ส่วนลด</th>
                                        <th width="15%"class="sorting" ng-click="sort($event)" sort="UNIT">ยอดขายสุทธิ</th>
                                        <th width="15%"class="sorting" ng-click="sort($event)" sort="UNIT">ต้นทุนของสินค้า</th>
                                        <th width="15%"class="sorting" ng-click="sort($event)" sort="UNIT">กำไรรวม</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr ng-repeat="item in ReportList">
                                        <td ng-bind="item.Day"></td>
                                        <td ng-bind="item.Total"></td>
                                        <td ng-bind="item.Refund"></td>
                                        <td ng-bind="item.Discount"></td>
                                        <td>{{item.Total - item.Refund -item.Discount}}</td>
                                        <td>{{item.Cost - item.TotalCostRefund}}</td>
                                        <td>{{(item.Total - item.Refund - item.Discount)-(item.Cost - item.TotalCostRefund)}}</td>
                                    </tr>
                                   
                                </tbody>
                            </table>
                        </div>
                        <!-- /.table-responsive -->
                    </div>
                    <!-- ทำหน้า -->
                    <div class="row tblResult small">
                        <div class="col-md-7 col-sm-7 col-xs-12 ">
                            <label class="col-md-4 col-sm-4 col-xs-12">
                                <?php echo $this->lang->line('Total'); ?> {{totalRecords}} <?php echo $this->lang->line('Records'); ?>
                            </label>
                            <label class="col-md-4 col-sm-4 col-xs-12">
                                <?php echo $this->lang->line('ResultsPerPage'); ?>
                            </label>
                            <div class="col-md-4 col-sm-4 col-xs-12 ">
                                <ui-select ng-model="TempPageSize.selected" ng-change="loadByPageSize()" theme="selectize">
                                    <ui-select-match>{{$select.selected.Value}}</ui-select-match>
                                    <ui-select-choices repeat="pSize in listPageSize | filter: $select.search">
                                        <span ng-bind-html="pSize.Text | highlight: $select.search"></span>
                                    </ui-select-choices>
                                </ui-select>
                            </div>
                        </div>
                        <div class="col-md-5 col-sm-5 col-xs-12  ">
                            <label class="col-md-4 col-sm-4 col-xs-12">
                                <span ng-click="getBackPage()" class="set-pointer"><i class="fa fa-chevron-left"></i> <span class="hidden-xs"><?php echo $this->lang->line('Previous'); ?></span></span>
                            </label>
                            <div class="col-md-3 col-sm-3 col-xs-12">
                                <ui-select ng-model="TempPageIndex.selected" ng-change="searchByPage()" theme="selectize">
                                    <ui-select-match>{{$select.selected.PageIndex}}</ui-select-match>
                                    <ui-select-choices repeat="pIndex in listPageIndex | filter: $select.search">
                                        <span ng-bind-html="pIndex.PageIndex | highlight: $select.search"></span>
                                    </ui-select-choices>
                                </ui-select>
                            </div>
                            <label class="col-md-4 col-sm-4 col-xs-12">
                                / {{ totalPage }} <span ng-click="getNextPage()" class="set-pointer"><?php echo $this->lang->line('Next'); ?><i class="fa fa-chevron-right set-pointer"></i></span>
                            </label>
                        </div>
                    </div>
                    <!-- ทำหน้า -->
                </div>
                <!-- /.panel-body -->
            </div>
            <!-- /.panel -->
        </div>
        <!-- /.col-lg-12 -->
    </div>
    <!-- /List.row types-->
</div>
</div>