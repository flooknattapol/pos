<!DOCTYPE html>
<html>
<head> 
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="description" content="">
<meta name="author" content="">
<title>urvr</title>
<link rel = "icon" href = "<?php echo base_url('/theme/image/logo_urvr.jpg');?>" type = "image/x-icon"> 
<style>
body {font-family: Arial, Helvetica, sans-serif;  }
form {border: 3px solid #f1f1f1; width: 300px;
 margin: auto; padding-top:20px;margin-top: 50px; }

input[type=text], input[type=password] {
    width: 100%;
    padding: 12px 20px;
    margin: 8px 0;
    display: inline-block;
    border: 1px solid #ccc;
    box-sizing: border-box;
}

button {
    background-color: #4CAF50;
    color: white;
    padding: 14px 20px;
    margin: 8px 0;
    border: none;
    cursor: pointer;
    width: 100%;
}

button:hover {
    opacity: 0.8;
}

.cancelbtn {
    width: auto;
    padding: 10px 18px;
    background-color: #f44336;
}

.imgcontainer {
    text-align: center;
    margin: 24px 0 12px 0;
}

img.avatar {
    width: 60%;
    border-radius: 50%;
}

.container {
    padding: 16px;
}

span.psw {
    float: right;
    padding-top: 16px;
}

/* Change styles for span and cancel button on extra small screens */
@media screen and (max-width: 300px) {
    span.psw {
       display: block;
       float: none;
    }
    .cancelbtn {
       width: 100%;
    }
}
</style>
</head>
<body>
<script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.6.9/angular.min.js"></script>

<script>
var app = angular.module("forgetpass", []);
app.controller("myCtrl", function($scope, $http) {
    $scope.forgetpass = function () {
        var base_url = window.location.origin;
        $scope.url = base_url + '/urvr/web/Login/forgetpassforsentmail';
        console.log("Test",  $scope.url);
        $http({
            url: $scope.url,
            method: "POST",
            data: {EMAIL:$scope.EMAIL}
        }).then(function (response) {
            console.log(response);
        },
        function (response) { // optional
            console.log(response);        // failed
        });
    }
});
</script>

<div ng-app="forgetpass" ng-controller="myCtrl">
    <form  method="POST">

        <div class="container">
            <h2 style="text-align:center" >ลืมรหัสผ่าน</h2>
        
            <label for="uname"><b>กรุณาใส่ Email</b></label>
            <input type="text" ng-model="EMAIL" required>

        <table width="100%"> 
            <tr>
                <td><button type="button" ng-click="forgetpass()">ลืมรหัสผ่าน</button></td>
                <td><a href="<?php echo base_url('/Login'); ?>"><button type="button">Login</button></a></td>
            </tr>
        
        </table>
        <font style="color:red;">*ถ้าคุณลืมรหัสผ่านระบบจะทำการเปลี่ยนรหัสผ่านพร้อมส่งรหัสผ่านเข้า E-mail ของคุณ</font>
        </div> 
    </form>
</div>

</body>
</html>