<script src="<?php echo base_url('asset/reportreceiptController.js'); ?>"></script>
<script src="<?php echo base_url('dist/Chart.min.js'); ?>"></script>
<script src="<?php echo base_url('utils.js'); ?>"></script>
<script src="<?php echo base_url('chartjs-plugin-datalabels.js'); ?>"></script>
<div ng-controller="reportreceiptController" ng-init="onInit()">

    <div class="row">
        <ul class="navigator">
            <?php /*<li class="nav"><a href="/Rooms"><?php echo $this->lang->line('event');?></a></li>*/ ?>
            <li class="nav_active">ใบเสร็จรับเงิน</li>
        </ul>
        <!-- /.col-lg-12 -->
    </div>

    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">ใบเสร็จรับเงิน</h1>
        </div>
        <!-- /.col-lg-12 -->
    </div>

    
    <!-- /List.row types-->
    <div class="row DisplayDevice">
        <div class="col-lg-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    สรุปใบเสร็จรับเงิน
                </div>
                <div class="panel-body">
                    <div class="col-lg-12 col-md-12 col-xs-12">
                        <div class="col-lg-3 col-md-3 col-xs-3">
                            <small>วันที่เริ่มต้น</small>
                            <input  class="form-control"ng-model="modelSearch.DateStart" data-date-format="dd-MM-yyyy" bs-datepicker>
                        </div>
                        <div class="col-lg-3 col-md-3 col-xs-3">
                            <small>วันที่สิ้นสุด</small>
                            <input  class="form-control"ng-model="modelSearch.DateEnd" data-date-format="dd-MM-yyyy" bs-datepicker>
                        </div>
                        <div class="col-lg-3 col-md-3 col-xs-3">
                            <small>เลขที่ใบเสร็จ</small>
                            <input type="text" class="form-control" ng-model="modelSearch.NAME">
                        </div>
                        <div class="col-lg-3 col-md-3 col-xs-3"><br>
                            <button class="btn btn-block btn-primary" ng-click="reload()">ค้นหา</button>
                        </div>
                    </div>
                    <div class="col-lg-12 col-md-12 col-xs-12">
                        <hr>
                    </div>
                    <div class="col-lg-12 col-md-12 col-xs-12">
                        <div class="col-lg-4 col-md-4 col-xs-12">
                            <div class="card"style="background-color:#0099CC">
                                <div class="row" >
                                    <div style="text-align: center">
                                        <h3 style="color:#FFFFFF;"><b><i class="fa fa-file-text" aria-hidden="true"></i> ใบเสร็จทั้งหมด</b></h3> 
                                        <h4 style="color:#FFFFFF;"><b>{{Total}}</b></h4> 
                                    </div>									
                                </div>
                            </div>
                        </div>	
                        <div class="col-lg-4 col-md-4 col-xs-12">
                            <div class="card"style="background-color:#009900;">
                                <div class="row">
                                    <div style="text-align: center">
                                        <h3 style="color:#FFFFFF;"><b><i class="fa fa-money" aria-hidden="true"></i> ยอดขาย</b></h3> 
                                        <h4 style="color:#FFFFFF;"><b>{{Total-refund}}</b></h4> 
                                    </div>
                                </div>	
                            </div>
                        </div>		
                        <div class="col-lg-4 col-md-4 col-xs-12">
                            <div class="card"style="background-color:#528B8B;">
                                <div class="row">
                                    <div style="text-align: center">
                                        <h3 style="color:#FFFFFF;"><b><i class="fa fa-undo" aria-hidden="true"></i> ยกเลิก</b></h3> 
                                        <h4 style="color:#FFFFFF;"><b>{{refund}}</b></h4> 
                                    </div>									
                                </div>
                            </div>
                        </div>			
                    </div>
                    <div class="col-lg-12 col-md-12 col-xs-12">
                        <hr>
                        <button class="btn btn-success"><i class="glyphicon glyphicon-print"></i> พิมพ์รายการ</button>
                    </div>
                    <div class="col-lg-12 col-md-12 col-xs-12"></hr>
                        <h3>รายการใบเสร็จ</h3>
                    </div>
                    <div class="col-lg-12 col-md-12 col-xs-12">
                        <div class="table-responsive">
                            <table class="table table-striped">
                                <thead>
                                    <tr>
                                        <th width="15%"class="sorting" ng-click="sort($event)" sort="ORDER_NAME">เลขที่ใบเสร็จ</th>
                                        <th width="10%"class="sorting" ng-click="sort($event)" sort="TimeStamp">วันที่</th>
                                        <th width="15%"class="sorting" ng-click="sort($event)" sort="SELLER_ID">พนักงาน</th>
                                        <th width="15%"class="sorting" ng-click="sort($event)" sort="CUSTOMER_NAME">ลูกค้า</th>
                                        <th width="15%"class="sorting" ng-click="sort($event)" sort="CATEGORYPAYMENT">ชนิด</th>
                                        <th width="15%"class="sorting" ng-click="sort($event)" sort="TotalOrder">รวมทั้งหมด</th>

                                    </tr>
                                </thead>
                                <tbody>
                                    <tr data-toggle="modal" data-target="#detail" ng-repeat="item in modelDeviceList" ng-click="onLoadDetail(item)">
                                        <td ng-bind="item.ORDER_NAME"></td>
                                        <td ng-bind="item.TimeStamp"></td>
                                        <td ng-bind="item.SELLER_ID"></td>
                                        <td ng-bind="item.CUSTOMER_NAME"></td>
                                        <td ng-bind="item.CATEGORYPAYMENT"></td>
                                        <td><span ng-show="item.IsActive == 0" style="color:red">ยกเลิก</span> <span ng-hide="item.IsActive == 0" ng-bind="item.TotalOrder"></span></td>
                                    </tr>
                                   
                                </tbody>
                            </table>
                        </div>
                        <!-- /.table-responsive -->
                    </div>
                    <!-- Modal -->
                    <div id="detail" class="modal fade" role="dialog">
                        <div class="modal-dialog">

                            <!-- Modal content-->
                            <div class="modal-content">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                                    <small style="color:#D5DBDB">{{CreateModel.TimeStamp}}</small>

                                    <h3 class="modal-title"style="text-align:center">ใบเสร็จ #{{CreateModel.ORDER_NAME}}</h3>
                                </div>
                                <div class="modal-body">
                                    <h3 style="text-align:center">{{CreateModel.TotalOrder}}</h3>
                                    <h5 style="text-align:center">รวมทั้งหมด</h5>
                                    <table width="100%">
                                        <tr ng-repeat="item in DataDetailModel">
                                            <td>
                                                <span>{{item.GOODS_NAME}} x 1 <span ng-show="item.IsActive == 0" style="color:red">ยกเลิกรายการ</span>
                                            </div>
                                            <td  style="text-align:right">
                                                {{item.PRICE}}
                                            </td>
                                        </tr> 
                                        <tr ng-show="CreateModel.AMOUNT_DISCOUNT > 0">
                                            <td>
                                                <span>ส่วนลด</span>
                                            </div>
                                            <td  style="text-align:right">
                                                -{{CreateModel.AMOUNT_DISCOUNT}}
                                            </td>
                                        </tr>  
                                    </table>
                                </div>
                                <div class="modal-footer">
                                    <div class="col-lg-12 col-md-12 col-xs-12">
                                        <div class="col-lg-6 col-md-6 col-xs-6">
                                        <b>รวมทั้งหมด</b>
                                        </div>
                                        <div class="col-lg-6 col-md-6 col-xs-6" style="text-align:right">
                                            {{CreateModel.TotalOrder}} บาท
                                        </div>
                                    </div>
                                    <div class="col-lg-12 col-md-12 col-xs-12">
                                        <div class="col-lg-6 col-md-6 col-xs-6">
                                        <b>รับเงิน</b>
                                        </div>
                                        <div class="col-lg-6 col-md-6 col-xs-6" style="text-align:right">
                                            {{CreateModel.Received}} บาท
                                        </div>
                                    </div>
                                    <div class="col-lg-12 col-md-12 col-xs-12">
                                        <div class="col-lg-6 col-md-6 col-xs-6">
                                        <b>เงินทอน</b>
                                        </div>
                                        <div class="col-lg-6 col-md-6 col-xs-6" style="text-align:right">
                                            {{CreateModel.Change}} บาท
                                        </div>
                                    </div>
                                    <div class="col-lg-12 col-md-12 col-xs-12">
                                        <div class="col-lg-6 col-md-6 col-xs-6">

                                        </div>
                                        <div class="col-lg-6 col-md-6 col-xs-6" style="text-align:right">
                                            
                                        </div>
                                    </div>
                                    <!-- data-dismiss="modal" -->
                                    <!-- <button type="button" class="btn btn-default btn-xs" >Cancel Receipt</button> -->
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- ทำหน้า -->
                    <div class="row tblResult small">
                        <div class="col-md-7 col-sm-7 col-xs-12 ">
                            <label class="col-md-4 col-sm-4 col-xs-12">
                                <?php echo $this->lang->line('Total'); ?> {{totalRecords}} <?php echo $this->lang->line('Records'); ?>
                            </label>
                            <label class="col-md-4 col-sm-4 col-xs-12">
                                <?php echo $this->lang->line('ResultsPerPage'); ?>
                            </label>
                            <div class="col-md-4 col-sm-4 col-xs-12 ">
                                <ui-select ng-model="TempPageSize.selected" ng-change="loadByPageSize()" theme="selectize">
                                    <ui-select-match>{{$select.selected.Value}}</ui-select-match>
                                    <ui-select-choices repeat="pSize in listPageSize | filter: $select.search">
                                        <span ng-bind-html="pSize.Text | highlight: $select.search"></span>
                                    </ui-select-choices>
                                </ui-select>
                            </div>
                        </div>
                        <div class="col-md-5 col-sm-5 col-xs-12  ">
                            <label class="col-md-4 col-sm-4 col-xs-12">
                                <span ng-click="getBackPage()" class="set-pointer"><i class="fa fa-chevron-left"></i> <span class="hidden-xs"><?php echo $this->lang->line('Previous'); ?></span></span>
                            </label>
                            <div class="col-md-3 col-sm-3 col-xs-12">
                                <ui-select ng-model="TempPageIndex.selected" ng-change="searchByPage()" theme="selectize">
                                    <ui-select-match>{{$select.selected.PageIndex}}</ui-select-match>
                                    <ui-select-choices repeat="pIndex in listPageIndex | filter: $select.search">
                                        <span ng-bind-html="pIndex.PageIndex | highlight: $select.search"></span>
                                    </ui-select-choices>
                                </ui-select>
                            </div>
                            <label class="col-md-4 col-sm-4 col-xs-12">
                                / {{ totalPage }} <span ng-click="getNextPage()" class="set-pointer"><?php echo $this->lang->line('Next'); ?><i class="fa fa-chevron-right set-pointer"></i></span>
                            </label>
                        </div>
                    </div>
                    <!-- ทำหน้า -->
                </div>
                <!-- /.panel-body -->
            </div>
            <!-- /.panel -->
        </div>
        <!-- /.col-lg-12 -->
    </div>
    <!-- /List.row types-->
</div>
</div>