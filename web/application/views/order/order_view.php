<script src="<?php echo base_url('asset/orderController.js'); ?>"></script>
<script src="<?php echo base_url('js/md5.min.js'); ?>"></script>
<div ng-controller="orderController" ng-init="onInit()">

    <div class="row">
        <ul class="navigator">
            <?php /*<li class="nav"><a href="/Rooms"><?php echo $this->lang->line('event');?></a></li>*/ ?>
            <li class="nav_active">สั่งซื้อ</li>
        </ul>
        <!-- /.col-lg-12 -->
    </div>

    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">สั่งซื้อ</h1>
        </div>
        <!-- /.col-lg-12 -->
    </div>

    <!-- /List.row types-->
    <div class="row  SearchDevice" style="display:none;">
        <div class="col-lg-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <?php echo $this->lang->line('Search'); ?>
                </div>
                <div class="panel-body">
                    <div class="form-group col-lg-12 col-md-12 col-xs-12">
                        <div class="col-lg-6 col-md-6 col-xs-12">
                            <label><span class="text-danger">*</span>เลขที่สั่งซื้อ</label>
                            <input class="form-control" ng-model="modelSearch.NAME" maxlength="80">
                            <p class="help-block"></p>
                        </div>
                        <div class="col-lg-6 col-md-6 col-xs-12">
                        </div>
                    </div>
                    <div class="form-group col-lg-12 col-md-12 col-xs-12">


                    </div>
                    <div class="col-lg-12 col-md-12 col-xs-12">
                        <button type="button" class="btn btn-primary waves-effect waves-light m-b-5" ng-click="resetSearch()"><i class="glyphicon glyphicon-repeat"></i> <span class="hidden-xs"><?php echo $this->lang->line('ResetSearch'); ?></span></button>
                        <button type="button" class="btn btn-primary waves-effect waves-light m-b-5" ng-click="LoadSearch()"><i class="fa fa-search"></i> <span class="hidden-xs"><?php echo $this->lang->line('Search'); ?></span></button>
                        <button type="button" class="btn btn-danger waves-effect waves-light m-b-5" ng-click="ShowDevice()"><i class="fa fa-times"></i> <span class="hidden-xs"><?php echo $this->lang->line('Cancel'); ?></span></button>
                    </div>
                </div>
                <!-- /.panel-body -->
            </div>
            <!-- /.panel -->
        </div>
        <!-- /.col-lg-12 -->
    </div>
    <!-- /List.row types-->


    <!-- / create room types  -->
    <div class="row addDevice" style="display:none;">
        <div class="col-lg-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    Manager Data
                </div>
                <div class="panel-body">
                    <div class="row">

                        <div class="col-lg-12 col-md-12 col-xs-12" style="padding-right:0px">
                            <div role="form">
                                <br>
                                <div class="form-group col-lg-12 col-md-12 col-xs-12">
                                    <div class="form-group col-lg-12 col-md-12 col-xs-12">
                                        <div class="col-lg-2 col-md-2 col-xs-2">
                                            <h5><span class="text-danger"></span>ซัพพลายเออร์</h5>
                                        </div>
                                        <div class="col-lg-4 col-md-4 col-xs-4">
                                            <ui-select ng-model="TempVendorIndex.selected" theme="selectize">
                                                <ui-select-match>{{$select.selected.NAME}}</ui-select-match>
                                                <ui-select-choices repeat="item in ListVendor  | filter: $select.search">
                                                    <span ng-bind-html="item.NAME | highlight: $select.search"></span>
                                                </ui-select-choices>
                                            </ui-select>
                                            <p class="CreateModel_VENDOR_ID require text-danger"><?php echo $this->lang->line('Require'); ?></p>
                                        </div>
                                    </div>
                                    <div class="form-group col-lg-12 col-md-12 col-xs-12">
                                        <div class="col-lg-2 col-md-2 col-xs-2">
                                            <h5><span class="text-danger"></span>วันที่สั่งซื้อ</h5>
                                        </div>
                                        <div class="col-lg-4 col-md-4 col-xs-4">
                                            <input class="form-control" ng-model="CreateModel.DATE_ORDER" data-date-format="dd-MM-yyyy" bs-datepicker>
                                            <p class="CreateModel_DATE_ORDER require text-danger"><?php echo $this->lang->line('Require'); ?></p>
                                        </div>
                                        <div class="col-lg-2 col-md-2 col-xs-2">
                                            <h5><span class="text-danger"></span>วันที่คาดว่าได้รับ</h5>
                                        </div>
                                        <div class="col-lg-4 col-md-4 col-xs-4">
                                            <input class="form-control" ng-model="CreateModel.ENDDATE_ORDER" data-date-format="dd-MM-yyyy" bs-datepicker>
                                            <p class="CreateModel_ENDDATE_ORDER require text-danger"><?php echo $this->lang->line('Require'); ?></p>
                                        </div>
                                    </div> 
                                    <div class="form-group col-lg-12 col-md-12 col-xs-12">
                                        <div class="col-lg-2 col-md-2 col-xs-2">
                                            <h5><span class="text-danger"></span>NOTE</h5>
                                        </div>
                                        <div class="col-lg-4 col-md-4 col-xs-4">
                                            <textarea class="form-control" ng-model="CreateModel.NOTE"></textarea>
                                        </div>
                                    </div>
                                    <div class="form-group col-lg-12 col-md-12 col-xs-12 ">
                                        <button class="btn btn-primary" ng-click="AddNewLine()" ><i class="fa fa-plus  "></i> <span class="hidden-xs">เพิ่มรายการ</span></button> 
                                    </div>
                                    <div class="form-group col-lg-12 col-md-12 col-xs-12 ">
                                        <div class="col-lg-2 col-md-2 col-xs-2"> 
                                            <label>สินค้า</label> 
                                        </div>
                                        <div class="col-lg-2 col-md-2 col-xs-2"> 
                                            <label>สินค้าในสต็อค</label> 
                                        </div>
                                        <div class="col-lg-2 col-md-2 col-xs-2"> 
                                            <label>จำนวน</label> 
                                        </div>
                                        <div class="col-lg-2 col-md-2 col-xs-2"> 
                                            <label>ต้นทุนซื้อ</label> 
                                        </div>
                                        <div class="col-lg-2 col-md-2 col-xs-2"> 
                                             <label>รวมจำนวนเงิน</label> 
                                        </div>
                                        <div class="col-lg-2 col-md-2 col-xs-2"> 
                                        </div>
								    </div>

								    <div class="form-group col-lg-12 col-md-12 col-xs-12" ng-repeat="item in DataDetailModel" >
									
                                        <div class="col-lg-2 col-md-2 col-xs-2"  style="padding-right: 2px;padding-left: 2px;">
                                            <ui-select ng-model="item.TempGoodsIndex.selected" theme="selectize"ng-change="getDataInComboList()">
                                                <ui-select-match>{{$select.selected.NAME}}</ui-select-match>
                                                <ui-select-choices repeat="item in ListGoods  | filter: $select.search">
                                                    <span ng-bind-html="item.NAME | highlight: $select.search"></span>
                                                </ui-select-choices>
                                            </ui-select>
                                        </div>
                                        <div class="col-lg-2 col-md-2 col-xs-2"  style="padding-right: 2px;padding-left: 2px;">
                                            <input class="form-control"  ng-model="item.INSTOCK" type="text" disabled="true">
                                        </div>
                                        <div class="col-lg-2 col-md-2 col-xs-2"  style="padding-right: 2px;padding-left: 2px;">
                                            <input class="form-control"  ng-model="item.ITEMAMOUNT" ng-change="getsumtotol()">
                                        </div>
                                        <div class="col-lg-2 col-md-2 col-xs-2"  style="padding-right: 2px;padding-left: 2px;">
                                            <input class="form-control"  ng-model="item.COST"  ng-change="getsumtotol()">
                                        </div>
                                        <div class="col-lg-2 col-md-2 col-xs-2"  style="padding-right: 2px;padding-left: 2px;">
                                            <input class="form-control"  ng-model="item.TOTAL" type="text" disabled="true">
                                        </div>
                                        <div class="col-lg-2 col-md-2 col-xs-2"  style="padding-right: 2px;padding-left: 2px;">
                                            <button title="ลบรายการ"my-confirm-click="onDeleteLine(item)" my-confirm-click-message="<?php echo $this->lang->line('DoYouWantToDelete');?>" class="btn btn-danger waves-effect waves-light btn-sm m-b-5"><i class="glyphicon glyphicon-trash"></i></button>
                                        </div> 
									
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="col-lg-12 col-md-12 col-xs-12">
                            <div class="form-group col-lg-12 col-md-12 col-xs-12  text-right">
                                <br>
                                <div class="col-lg-12 col-md-12 col-xs-12">
                                    <button ng-hide="CreateModel.IsActive == 0"class="btn btn-primary" ng-click="onSaveTagClick()"><i class="fa fa-save"></i> <span class="hidden-xs"><?php echo $this->lang->line('Save'); ?></span></button>
                                    <button class="btn btn-danger" ng-click="ShowDevice()"><i class="fa fa-times "></i> <span class="hidden-xs"><?php echo $this->lang->line('Cancel'); ?></span></button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <!-- /.col-lg-12 -->
    </div>
    <!-- /.create room types -->


    <!-- /List.row types-->
    <div class="row DisplayDevice">
        <div class="col-lg-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    รายการสั่งซื้อ
                </div>
                <div class="panel-body">
                    <div class="col-lg-12 col-md-12 col-xs-12">
                        <button class="btn btn-primary"ng-click="AddNewDevice()"><i class="fa fa-plus  "></i> <span class="hidden-xs"><?php echo $this->lang->line('Add'); ?></span></button>
                        <button class="btn btn-info" ng-click="ShowSearch()"><i class="fa fa-search  "></i> <span class="hidden-xs"><?php echo $this->lang->line('Search'); ?></span></button>
                    </div>
                    <div class="col-lg-12 col-md-12 col-xs-12">
                        <div class="table-responsive">
                            <table class="table table-striped">
                                <thead>
                                    <tr>
                                        <th width="5%">#</th>
                                        <th class="sorting" ng-click="sort($event)" sort="ORDERNAME">เลขที่สั่งซื้อ</th>  
                                        <th class="sorting" ng-click="sort($event)" sort="VENDOR_NAME">ซัพพลายเออร์</th>     
                                        <th class="sorting" ng-click="sort($event)" sort="DATE_ORDER">วันที่สั่งซื้อ</th>  
                                        <th class="sorting" ng-click="sort($event)" sort="ENDDATE_ORDER">วันที่คาดว่าได้รับ</th>  
                                        <th class="sorting" ng-click="sort($event)" sort="EMPLOYEE_NAME">ผู้ทำรายการ</th> 
                                        <th>Status</th>
                                        <th ><?php echo $this->lang->line('Option'); ?></th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr ng-repeat="item in modelDeviceList">
                                        <td>{{$index+1}}</td>
                                        <td ng-bind="item.ORDERNAME "></td>
                                        <td ng-bind="item.VENDOR_NAME "></td>
                                        <td ng-bind="item.DATE_ORDER "></td>
                                        <td ng-bind="item.ENDDATE_ORDER "></td>
                                        <td ng-bind="item.EMPLOYEE_NAME "></td>
                                        <td><b style="color:green" ng-show="item.IsActive == 0">รับสินค้าแล้ว</b></td>
                                        <td>
                                            <button ng-click="onEditTagClick(item )" class="btn btn-primary waves-effect waves-light btn-sm m-b-5"  ><i class="glyphicon glyphicon-edit"></i> <span class="hidden-xs"><?php echo $this->lang->line('Edit');?></span></button>
                                            <button my-confirm-click="onDeleteTagClick(item)" my-confirm-click-message="คุณต้องการลบข้อมูลนี้หรือไม่ ?" class="btn btn-danger waves-effect waves-light btn-sm m-b-5"><i class="glyphicon glyphicon-remove"></i> <span class="hidden-xs">ลบ</span></button>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                        <!-- /.table-responsive -->
                    </div>
                    <!-- ทำหน้า -->
                    <div class="row tblResult small">
                        <div class="col-md-7 col-sm-7 col-xs-12 ">
                            <label class="col-md-4 col-sm-4 col-xs-12">
                                <?php echo $this->lang->line('Total'); ?> {{totalRecords}} <?php echo $this->lang->line('Records'); ?>
                            </label>
                            <label class="col-md-4 col-sm-4 col-xs-12">
                                <?php echo $this->lang->line('ResultsPerPage'); ?>
                            </label>
                            <div class="col-md-4 col-sm-4 col-xs-12 ">
                                <ui-select ng-model="TempPageSize.selected" ng-change="loadByPageSize()" theme="selectize">
                                    <ui-select-match>{{$select.selected.Value}}</ui-select-match>
                                    <ui-select-choices repeat="pSize in listPageSize | filter: $select.search">
                                        <span ng-bind-html="pSize.Text | highlight: $select.search"></span>
                                    </ui-select-choices>
                                </ui-select>
                            </div>
                        </div>
                        <div class="col-md-5 col-sm-5 col-xs-12  ">
                            <label class="col-md-4 col-sm-4 col-xs-12">
                                <span ng-click="getBackPage()" class="set-pointer"><i class="fa fa-chevron-left"></i> <span class="hidden-xs"><?php echo $this->lang->line('Previous'); ?></span></span>
                            </label>
                            <div class="col-md-3 col-sm-3 col-xs-12">
                                <ui-select ng-model="TempPageIndex.selected" ng-change="searchByPage()" theme="selectize">
                                    <ui-select-match>{{$select.selected.PageIndex}}</ui-select-match>
                                    <ui-select-choices repeat="pIndex in listPageIndex | filter: $select.search">
                                        <span ng-bind-html="pIndex.PageIndex | highlight: $select.search"></span>
                                    </ui-select-choices>
                                </ui-select>
                            </div>
                            <label class="col-md-4 col-sm-4 col-xs-12">
                                / {{ totalPage }} <span ng-click="getNextPage()" class="set-pointer"><?php echo $this->lang->line('Next'); ?><i class="fa fa-chevron-right set-pointer"></i></span>
                            </label>
                        </div>
                    </div>
                    <!-- ทำหน้า -->

                </div>
                <!-- /.panel-body -->
            </div>
            <!-- /.panel -->
        </div>
        <!-- /.col-lg-12 -->
    </div>
    <!-- /List.row types-->
</div>
</div>