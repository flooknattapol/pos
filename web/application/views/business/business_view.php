<script src="<?php echo base_url('asset/businessController.js'); ?>"></script>
<script src="<?php echo base_url('js/md5.min.js'); ?>"></script>
<div ng-controller="businessController" ng-init="onInit()">

    <div class="row">
        <ul class="navigator">
            <?php /*<li class="nav"><a href="/Rooms"><?php echo $this->lang->line('event');?></a></li>*/ ?>
            <li class="nav_active">รายชื่อร้านค้า</li>
        </ul>
        <!-- /.col-lg-12 -->
    </div>

    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">รายชื่อร้านค้า</h1>
        </div>
        <!-- /.col-lg-12 -->
    </div>

    <!-- /List.row types-->
    <div class="row  SearchDevice" style="display:none;">
        <div class="col-lg-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <?php echo $this->lang->line('Search'); ?>
                </div>
                <div class="panel-body">
                    <div class="form-group col-lg-12 col-md-12 col-xs-12">
                        <div class="col-lg-6 col-md-6 col-xs-12">
                            <label><span class="text-danger">*</span>ร้านค้า</label>
                            <input class="form-control" ng-model="modelSearch.NAME" maxlength="80">
                            <p class="help-block"></p>
                        </div>
                        <div class="col-lg-6 col-md-6 col-xs-12">
                        </div>
                    </div>
                    <div class="form-group col-lg-12 col-md-12 col-xs-12">


                    </div>
                    <div class="col-lg-12 col-md-12 col-xs-12">
                        <button type="button" class="btn btn-primary waves-effect waves-light m-b-5" ng-click="resetSearch()"><i class="glyphicon glyphicon-repeat"></i> <span class="hidden-xs"><?php echo $this->lang->line('ResetSearch'); ?></span></button>
                        <button type="button" class="btn btn-primary waves-effect waves-light m-b-5" ng-click="LoadSearch()"><i class="fa fa-search"></i> <span class="hidden-xs"><?php echo $this->lang->line('Search'); ?></span></button>
                        <button type="button" class="btn btn-danger waves-effect waves-light m-b-5" ng-click="ShowDevice()"><i class="fa fa-times"></i> <span class="hidden-xs"><?php echo $this->lang->line('Cancel'); ?></span></button>
                    </div>
                </div>
                <!-- /.panel-body -->
            </div>
            <!-- /.panel -->
        </div>
        <!-- /.col-lg-12 -->
    </div>
    <!-- /List.row types-->


    <!-- / create room types  -->
    <div class="row addDevice" style="display:none;">
        <div class="col-lg-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    Manager Data
                </div>
                <div class="panel-body">
                    <div class="row">

                        <div class="col-lg-12 col-md-12 col-xs-12" style="padding-right:0px">
                            <div role="form">
                                <br>
                                <div class="form-group col-lg-12 col-md-12 col-xs-12">
                                    <div class="form-group col-lg-12 col-md-12 col-xs-12" style="text-align: center">   
                                        <img ng-src="{{PreviewImage}}" ng-show="ViewImgPreview == true" alt="" style="width:250px;border: 1px solid #ddd; border-radius: 4px;padding:5px;" />   
                                        <img src="<?php echo base_url('/upload/Business/');?>{{CreateModel.IMAGE}}" ng-show="ViewImgPreview == false" alt="" style="width:250px;border: 1px solid #ddd; border-radius: 4px;padding:5px;"  />                                        
                                    </div> 
                                    <div class="form-group col-lg-12 col-md-12 col-xs-12">
                                        <div class="col-lg-3 col-md-3 col-xs-3">
                                            <h5><span class="text-danger"></span>รูปภาพโลโก้</h5>
                                        </div>
                                        <div class="col-lg-8 col-md-8 col-xs-8">
                                        <input type="file" onchange="angular.element(this).scope().SelectFile(event)" ngf-select ng-model="filegeneralinfo" name="filegeneralinfo" ngf-pattern="'.png'" ngf-accept="'.png'" ngf-max-size="10MB" ngf-min-height="100"/>
                                        </div>
                                    </div>
                                    <div class="form-group col-lg-12 col-md-12 col-xs-12">
                                        <div class="col-lg-2 col-md-2 col-xs-2">
                                            <h5><span class="text-danger"></span>ชื่อร้านค้า</h5>
                                        </div>
                                        <div class="col-lg-4 col-md-4 col-xs-4">
                                            <input class="form-control" ng-model="CreateModel.NAME">
                                            <p class="CreateModel_NAME require text-danger"><?php echo $this->lang->line('Require'); ?></p>
                                        </div>
                                        <div class="col-lg-2 col-md-2 col-xs-2">
                                            <h5><span class="text-danger"></span>หมวดหมู่</h5>
                                        </div>
                                        <div class="col-lg-4 col-md-4 col-xs-4">
                                            <ui-select ng-model="TempCategoryBusinessIndex.selected" theme="selectize">
                                                <ui-select-match>{{$select.selected.NAME}}</ui-select-match>
                                                <ui-select-choices repeat="Item in ListCategoryBusiness  | filter: $select.search">
                                                    <span ng-bind-html="Item.NAME | highlight: $select.search"></span>
                                                </ui-select-choices>
                                            </ui-select>
                                            <p class="CreateModel_CATEGORY_ID require text-danger"><?php echo $this->lang->line('Require'); ?></p>
                                        </div>
                                    </div>
                                    <div class="form-group col-lg-12 col-md-12 col-xs-12">
                                        <div class="col-lg-2 col-md-2 col-xs-2">
                                            <h5><span class="text-danger"></span>เบอร์โทรศัพท์</h5>
                                        </div>
                                        <div class="col-lg-4 col-md-4 col-xs-4">
                                            <input class="form-control" ng-model="CreateModel.TEL" type="text">
                                            <p class="CreateModel_TEL require text-danger"><?php echo $this->lang->line('Require'); ?></p>
                                        </div>
                                        <div class="col-lg-2 col-md-2 col-xs-2">
                                            <h5><span class="text-danger"></span>ประเภทร้านค้า</h5>
                                        </div>
                                        <div class="col-lg-4 col-md-4 col-xs-4">
                                            <ui-select ng-model="TempTypeBusinessIndex.selected" theme="selectize">
                                                <ui-select-match>{{$select.selected.NAME}}</ui-select-match>
                                                <ui-select-choices repeat="Item in ListTypeBusiness  | filter: $select.search">
                                                    <span ng-bind-html="Item.NAME | highlight: $select.search"></span>
                                                </ui-select-choices>
                                            </ui-select>
                                            <p class="CreateModel_TYPE require text-danger"><?php echo $this->lang->line('Require'); ?></p>
                                        </div>
                                    </div> 
                                    <div class="form-group col-lg-12 col-md-12 col-xs-12">
                                        <div class="col-lg-2 col-md-2 col-xs-2">
                                            <h5><span class="text-danger"></span>E-Mail (Username)</h5>
                                        </div>
                                        <div class="col-lg-4 col-md-4 col-xs-4">
                                            <input class="form-control" ng-model="CreateModel.EMAIL" type="text">
                                            <p class="CreateModel_EMAIL require text-danger"><?php echo $this->lang->line('Require'); ?></p>
                                        </div>
                                        <div class="col-lg-2 col-md-2 col-xs-2">
                                            <h5><span class="text-danger"></span>ชื่อผู้ติดต่อ</h5>
                                        </div>
                                        <div class="col-lg-4 col-md-4 col-xs-4">
                                            <input class="form-control" ng-model="CreateModel.CONTACT" type="text">
                                            <p class="CreateModel_CONTACT require text-danger"><?php echo $this->lang->line('Require'); ?></p>
                                        </div>
                                    </div> 
                                    <div class="form-group col-lg-12 col-md-12 col-xs-12">
                                        <div class="col-lg-2 col-md-2 col-xs-2">
                                            <h5><span class="text-danger"></span>เลขประจำตัวผู้เสียภาษี 10 หลัก</h5>
                                        </div>
                                        <div class="col-lg-4 col-md-4 col-xs-4">
                                            <input class="form-control" ng-model="CreateModel.TAXID10" type="text" maxlength="10">
                                        </div>
                                        <div class="col-lg-2 col-md-2 col-xs-2">
                                            <h5><span class="text-danger"></span>เลขประจำตัวผู้เสียภาษี 13 หลัก</h5>
                                        </div>
                                        <div class="col-lg-4 col-md-4 col-xs-4">
                                            <input class="form-control" ng-model="CreateModel.TAXID13" type="text"maxlength="13">
                                        </div>
                                    </div> 
                                    <div class="form-group col-lg-12 col-md-12 col-xs-12">
                                        <div class="col-lg-2 col-md-2 col-xs-2">
                                            <h5><span class="text-danger"></span>วันหมดอายุร้านค้า</h5>
                                        </div>
                                        <div class="col-lg-4 col-md-4 col-xs-4">
                                            <select  class="form-control" ng-model="CreateModel.EXPIRED_FLAG">
                                                <option value="1">มี</option>
                                                <option value="0">ไม่มี</option>
                                            </select>
                                            <p class="CreateModel_EXPIRED_FLAG require text-danger"><?php echo $this->lang->line('Require'); ?></p>
                                        </div>
                                        <div class="col-lg-2 col-md-2 col-xs-2" ng-show="CreateModel.EXPIRED_FLAG == 1">
                                            <h5><span class="text-danger"></span>โค๊ดร้านค้า</h5>
                                        </div>
                                        <div class="col-lg-4 col-md-4 col-xs-4" ng-show="CreateModel.EXPIRED_FLAG == 1">
                                            <input class="form-control" ng-model="CreateModel.CODE" type="text"maxlength="8">
                                            <p style="color: red">***ถ้าไม่ใส่โค๊ดวันหมดอายุจะนับจากวันปัจจุบัน ไป 30 วัน***</p>
                                        </div>
                                    </div> 
                                    <div class="form-group col-lg-12 col-md-12 col-xs-12 ">
                                        <button class="btn btn-primary" ng-click="AddNewLine()" ><i class="fa fa-plus  "></i> <span class="hidden-xs">เพิ่มสาขา</span></button> 
                                    </div>
                                    <div class="form-group col-lg-12 col-md-12 col-xs-12 ">
                                        <div class="col-lg-2 col-md-2 col-xs-2"> 
                                            <label>ชื่อสาขา</label> 
                                        </div>
                                        <div class="col-lg-2 col-md-2 col-xs-2"> 
                                            <label>เบอร์โทรศัพ์</label> 
                                        </div>
                                        <div class="col-lg-2 col-md-2 col-xs-2"> 
                                             <label>E-Mail</label> 
                                        </div>
                                        <div class="col-lg-5 col-md-5 col-xs-5"> 
                                            <label>ที่อยู่</label> 
                                        </div>
                                        <div class="col-lg-1 col-md-1 col-xs-1"> 
                                            
                                        </div>
								    </div>

								    <div class="form-group col-lg-12 col-md-12 col-xs-12" ng-repeat="item in DataDetailModel" >
									
                                        <div class="col-lg-2 col-md-2 col-xs-2"  style="padding-right: 2px;padding-left: 2px;">
                                            <input class="form-control"  ng-model="item.NAME" type="text" ng-disabled="item.NAME == 'สำนักงานใหญ่'">
                                        </div>
                                        <div class="col-lg-2 col-md-2 col-xs-2"  style="padding-right: 2px;padding-left: 2px;">
                                            <input class="form-control"  ng-model="item.TEL" type="text">
                                        </div>
                                        <div class="col-lg-2 col-md-2 col-xs-2"  style="padding-right: 2px;padding-left: 2px;">
                                            <input class="form-control"  ng-model="item.EMAIL" type="text">
                                        </div>
                                        <div class="col-lg-5 col-md-5 col-xs-5"  style="padding-right: 2px;padding-left: 2px;">
                                            <input class="form-control"  ng-model="item.ADDRESS" type="text">
                                        </div>
                                        <div class="col-lg-1 col-md-1 col-xs-1"  style="padding-right: 2px;padding-left: 2px;">
                                            <button title="ลบรายการ"my-confirm-click="onDeleteLine(item)" my-confirm-click-message="<?php echo $this->lang->line('DoYouWantToDelete');?>" class="btn btn-danger waves-effect waves-light btn-sm m-b-5"><i class="glyphicon glyphicon-trash"></i></button>
                                        </div> 
									
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="col-lg-12 col-md-12 col-xs-12">
                            <div class="form-group col-lg-12 col-md-12 col-xs-12  text-right">
                                <br>
                                <div class="col-lg-12 col-md-12 col-xs-12">
                                    <button ng-hide="CreateModel.IsActive == 0"class="btn btn-primary" ng-click="upload()"><i class="fa fa-save"></i> <span class="hidden-xs"><?php echo $this->lang->line('Save'); ?></span></button>
                                    <button class="btn btn-danger" ng-click="ShowDevice()"><i class="fa fa-times "></i> <span class="hidden-xs"><?php echo $this->lang->line('Cancel'); ?></span></button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <!-- /.col-lg-12 -->
    </div>
    <!-- /.create room types -->


    <!-- /List.row types-->
    <div class="row DisplayDevice">
        <div class="col-lg-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    รายชื่อร้านค้า
                </div>
                <div class="panel-body">
                    <div class="col-lg-12 col-md-12 col-xs-12">
                        <button class="btn btn-primary"ng-click="AddNewDevice()"><i class="fa fa-plus  "></i> <span class="hidden-xs"><?php echo $this->lang->line('Add'); ?></span></button>
                        <button class="btn btn-info" ng-click="ShowSearch()"><i class="fa fa-search  "></i> <span class="hidden-xs"><?php echo $this->lang->line('Search'); ?></span></button>
                    </div>
                    <div class="col-lg-12 col-md-12 col-xs-12">
                        <div class="table-responsive">
                            <table class="table table-striped">
                                <thead>
                                    <tr>
                                        <th width="5%">#</th>
                                        <th width="25%"class="sorting" ng-click="sort($event)" sort="NAME">ชื่อร้านค้า</th>     
                                        <th width="20%"class="sorting" ng-click="sort($event)" sort="EMAIL">E-Mail</th>   
                                        <th width="20%"class="sorting" ng-click="sort($event)" sort="TEL">เบอร์โทรศัพ์</th> 
                                        <th width="10%">สถานะ</th>                       
                                        <th width="20%"><?php echo $this->lang->line('Option'); ?></th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr ng-repeat="item in modelDeviceList">
                                        <td>{{$index+1}}</td>
                                        <td ng-bind="item.NAME "></td>
                                        <td ng-bind="item.EMAIL "></td>
                                        <td ng-bind="item.TEL "></td>
                                        <td><b ng-hide="item.IsActive == 1" style="color:red">ระงับใช้งาน</b><b style="color:green"ng-show="item.IsActive == 1">ปกติ</b></td>
                                        <td>
                                            <button ng-click="onEditTagClick(item )" class="btn btn-primary waves-effect waves-light btn-sm m-b-5"  ><i class="glyphicon glyphicon-edit"></i> <span class="hidden-xs"><?php echo $this->lang->line('Edit');?></span></button>
                                            <button ng-show="item.IsActive == 1"my-confirm-click="onDeleteTagClick(item)" my-confirm-click-message="คุณต้องการระงับร้านค้านี้หรือไม่ ?" class="btn btn-danger waves-effect waves-light btn-sm m-b-5"><i class="glyphicon glyphicon-remove"></i> <span class="hidden-xs">ระงับใช้งาน</span></button>
                                            <button ng-hide="item.IsActive == 1"my-confirm-click="onUnDeleteTagClick(item)" my-confirm-click-message="คุณต้องเปิดใช้งานร้านค้านี้หรือไม่ ?" class="btn btn-success waves-effect waves-light btn-sm m-b-5"><i class="glyphicon glyphicon-ok"></i> <span class="hidden-xs">เปิดใช้งาน</span></button>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                        <!-- /.table-responsive -->
                    </div>
                    <!-- ทำหน้า -->
                    <div class="row tblResult small">
                        <div class="col-md-7 col-sm-7 col-xs-12 ">
                            <label class="col-md-4 col-sm-4 col-xs-12">
                                <?php echo $this->lang->line('Total'); ?> {{totalRecords}} <?php echo $this->lang->line('Records'); ?>
                            </label>
                            <label class="col-md-4 col-sm-4 col-xs-12">
                                <?php echo $this->lang->line('ResultsPerPage'); ?>
                            </label>
                            <div class="col-md-4 col-sm-4 col-xs-12 ">
                                <ui-select ng-model="TempPageSize.selected" ng-change="loadByPageSize()" theme="selectize">
                                    <ui-select-match>{{$select.selected.Value}}</ui-select-match>
                                    <ui-select-choices repeat="pSize in listPageSize | filter: $select.search">
                                        <span ng-bind-html="pSize.Text | highlight: $select.search"></span>
                                    </ui-select-choices>
                                </ui-select>
                            </div>
                        </div>
                        <div class="col-md-5 col-sm-5 col-xs-12  ">
                            <label class="col-md-4 col-sm-4 col-xs-12">
                                <span ng-click="getBackPage()" class="set-pointer"><i class="fa fa-chevron-left"></i> <span class="hidden-xs"><?php echo $this->lang->line('Previous'); ?></span></span>
                            </label>
                            <div class="col-md-3 col-sm-3 col-xs-12">
                                <ui-select ng-model="TempPageIndex.selected" ng-change="searchByPage()" theme="selectize">
                                    <ui-select-match>{{$select.selected.PageIndex}}</ui-select-match>
                                    <ui-select-choices repeat="pIndex in listPageIndex | filter: $select.search">
                                        <span ng-bind-html="pIndex.PageIndex | highlight: $select.search"></span>
                                    </ui-select-choices>
                                </ui-select>
                            </div>
                            <label class="col-md-4 col-sm-4 col-xs-12">
                                / {{ totalPage }} <span ng-click="getNextPage()" class="set-pointer"><?php echo $this->lang->line('Next'); ?><i class="fa fa-chevron-right set-pointer"></i></span>
                            </label>
                        </div>
                    </div>
                    <!-- ทำหน้า -->

                </div>
                <!-- /.panel-body -->
            </div>
            <!-- /.panel -->
        </div>
        <!-- /.col-lg-12 -->
    </div>
    <!-- /List.row types-->
</div>
</div>