<script src="<?php echo base_url('asset/userController.js'); ?>"></script>
<script src="<?php echo base_url('js/md5.min.js'); ?>"></script>
<div ng-controller="userController" ng-init="onInit()">

    <div class="row">
        <ul class="navigator">
            <?php /*<li class="nav"><a href="/Rooms"><?php echo $this->lang->line('event');?></a></li>*/ ?>
            <li class="nav_active">จัดการผู้ใช้งาน</li>
        </ul>
        <!-- /.col-lg-12 -->
    </div>

    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">จัดการผู้ใช้งาน</h1>
        </div>
        <!-- /.col-lg-12 -->
    </div>

    <!-- /List.row types-->
    <div class="row  SearchDevice" style="display:none;">
        <div class="col-lg-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <?php echo $this->lang->line('Search'); ?>
                </div>
                <div class="panel-body">
                    <div class="form-group col-lg-12 col-md-12 col-xs-12">
                        <div class="col-lg-6 col-md-6 col-xs-12">
                            <label><span class="text-danger">*</span>ผู้ใช้งาน</label>
                            <input class="form-control" ng-model="modelSearch.NAME" >
                            <p class="help-block"></p>
                        </div>
                        <div class="col-lg-6 col-md-6 col-xs-12">
                        </div>
                    </div>
                    <div class="form-group col-lg-12 col-md-12 col-xs-12">


                    </div>
                    <div class="col-lg-12 col-md-12 col-xs-12">
                        <button type="button" class="btn btn-primary waves-effect waves-light m-b-5" ng-click="resetSearch()"><i class="glyphicon glyphicon-repeat"></i> <span class="hidden-xs"><?php echo $this->lang->line('ResetSearch'); ?></span></button>
                        <button type="button" class="btn btn-primary waves-effect waves-light m-b-5" ng-click="LoadSearch()"><i class="fa fa-search"></i> <span class="hidden-xs"><?php echo $this->lang->line('Search'); ?></span></button>
                        <button type="button" class="btn btn-danger waves-effect waves-light m-b-5" ng-click="ShowDevice()"><i class="fa fa-times"></i> <span class="hidden-xs"><?php echo $this->lang->line('Cancel'); ?></span></button>
                    </div>
                </div>
                <!-- /.panel-body -->
            </div>
            <!-- /.panel -->
        </div>
        <!-- /.col-lg-12 -->
    </div>
    <!-- /List.row types-->


    <!-- / create room types  -->
    <div class="row addDevice" style="display:none;">
        <div class="col-lg-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    Manager 
                </div>
                <div class="panel-body">
                    <div class="row">

                        <div class="col-lg-12 col-md-12 col-xs-12" style="padding-right:0px">
                            <div role="form">
                                <br>
                                <div class="col-lg-12 col-md-12 col-xs-12">
                                    <div class="form-group col-lg-12 col-md-12 col-xs-12">
                                    </div>
                                    <div class="form-group col-lg-12 col-md-12 col-xs-12">
                                        <div class="col-lg-3 col-md-3 col-xs-3">
                                            <h5><span class="text-danger"></span>ชื่อผู้ใช้งาน</h5>
                                        </div>
                                        <div class="col-lg-8 col-md-8 col-xs-8">
                                            <input class="form-control" ng-model="CreateModel.NAME" >
                                            <p class="CreateModel_USER require text-danger"><?php echo $this->lang->line('Require'); ?></p>
                                        </div>
                                    </div>
                                    <div class="form-group col-lg-12 col-md-12 col-xs-12">
                                        <div class="col-lg-3 col-md-3 col-xs-3">
                                            <h5><span class="text-danger"></span>อีเมลล์</h5>
                                        </div>
                                        <div class="col-lg-8 col-md-8 col-xs-8">
                                            <input class="form-control" ng-model="CreateModel.EMAIL" type="text">
                                            <p class="CreateModel_EMAIL require text-danger"><?php echo $this->lang->line('Require'); ?></p>
                                        </div>
                                    </div> 
                                    <div class="form-group col-lg-12 col-md-12 col-xs-12">
                                        <div class="col-lg-3 col-md-3 col-xs-3">
                                            <h5><span class="text-danger"></span>โทรศัพท์</h5>
                                        </div>
                                        <div class="col-lg-8 col-md-8 col-xs-8">
                                            <input class="form-control" ng-model="CreateModel.TEL" type="text">
                                            <p class="CreateModel_TEL require text-danger"><?php echo $this->lang->line('Require'); ?></p>
                                        </div>
                                    </div> 
                                    <div class="form-group col-lg-12 col-md-12 col-xs-12 ">
                                        <button class="btn btn-primary" ng-click="AddNewLine()" ><i class="fa fa-plus  "></i> <span class="hidden-xs">เพิ่มตำแหน่ง</span></button> 
                                    </div>
                                    <div class="form-group col-lg-12 col-md-12 col-xs-12 ">
                                        <div class="col-lg-4 col-md-4 col-xs-4"> 
                                            <label>ชื่อร้านค้า</label> 
                                        </div>
                                        <div class="col-lg-4 col-md-4 col-xs-4"> 
                                            <label>สาขา</label> 
                                        </div>
                                        <div class="col-lg-3 col-md-3 col-xs-3"> 
                                            <label>ตำแหน่ง</label> 
                                        </div>
                                        <div class="col-lg-1 col-md-1 col-xs-1"> 
                                            
                                        </div>
								    </div>

								    <div class="form-group col-lg-12 col-md-12 col-xs-12" ng-repeat="item in DataDetailModel" >
                                        <div class="col-lg-4 col-md-4 col-xs-4"  style="padding-right: 2px;padding-left: 2px;">
                                            <!-- <input class="form-control"  ng-model="item.BUSINESS_ID" type="text"> -->
                                            <ui-select ng-model="item.TempBusinessIndex" theme="selectize" ng-change="getDetailCombobox(item)">
                                                <ui-select-match>{{$select.selected.NAME}}</ui-select-match>
                                                <ui-select-choices repeat="Item in item.ListBusiness  | filter: $select.search">
                                                    <span ng-bind-html="Item.NAME | highlight: $select.search"></span>
                                                </ui-select-choices>
                                            </ui-select>
                                        </div>
                                        <div class="col-lg-4 col-md-4 col-xs-4"  style="padding-right: 2px;padding-left: 2px;">
                                            <!-- <input class="form-control"  ng-model="item.BRANCH_ID   " type="text"> -->
                                            <ui-select ng-model="item.TempBranchIndex" theme="selectize">
                                                <ui-select-match>{{$select.selected.NAME}}</ui-select-match>
                                                <ui-select-choices repeat="Item in item.ListBranch  | filter: $select.search">
                                                    <span ng-bind-html="Item.NAME | highlight: $select.search"></span>
                                                </ui-select-choices>
                                            </ui-select>
                                        </div>
                                        <div class="col-lg-3 col-md-3 col-xs-3"  style="padding-right: 2px;padding-left: 2px;">
                                            <!-- <input class="form-control"  ng-model="item.ROLE_ID   " type="text"> -->
                                            <ui-select ng-model="item.TempRoleIndex" theme="selectize">
                                                <ui-select-match>{{$select.selected.NAME}}</ui-select-match>
                                                <ui-select-choices repeat="Item in item.ListRole  | filter: $select.search">
                                                    <span ng-bind-html="Item.NAME | highlight: $select.search"></span>
                                                </ui-select-choices>
                                            </ui-select>
                                        </div>
                                        <div class="col-lg-1 col-md-1 col-xs-1"  style="padding-right: 2px;padding-left: 2px;">
                                            <button title="ลบรายการ"my-confirm-click="onDeleteLine(item)" my-confirm-click-message="<?php echo $this->lang->line('DoYouWantToDelete');?>" class="btn btn-danger waves-effect waves-light btn-sm m-b-5"><i class="glyphicon glyphicon-trash"></i></button>
                                        </div> 
									
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="col-lg-12 col-md-12 col-xs-12">
                            <div class="form-group col-lg-12 col-md-12 col-xs-12  text-right">
                                <br>
                                <div class="col-lg-12 col-md-12 col-xs-12">
                                    <button class="btn btn-primary" ng-click="onSaveTagClick()"><i class="fa fa-save"></i> <span class="hidden-xs"><?php echo $this->lang->line('Save'); ?></span></button>
                                    <button class="btn btn-danger" ng-click="ShowDevice()"><i class="fa fa-times "></i> <span class="hidden-xs"><?php echo $this->lang->line('Cancel'); ?></span></button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <!-- /.col-lg-12 -->
    </div>
    <!-- /.create room types -->


    <!-- /List.row types-->
    <div class="row DisplayDevice">
        <div class="col-lg-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    Manager 
                </div>
                <div class="panel-body">
                    <div class="col-lg-12 col-md-12 col-xs-12">
                        <button class="btn btn-primary"ng-click="AddNewDevice()"><i class="fa fa-plus  "></i> <span class="hidden-xs"><?php echo $this->lang->line('Add'); ?></span></button>
                        <button class="btn btn-info" ng-click="ShowSearch()"><i class="fa fa-search  "></i> <span class="hidden-xs"><?php echo $this->lang->line('Search'); ?></span></button>
                    </div>
                    <div class="col-lg-12 col-md-12 col-xs-12">
                        <div class="table-responsive">
                            <table class="table table-striped">
                                <thead>
                                    <tr>
                                        <th width="5%">#</th>
                                        <th class="sorting" ng-click="sort($event)" sort="NAME">ชื่อผู้ใช้งาน</th>    
                                        <th class="sorting" ng-click="sort($event)" sort="EMAIL">E-Mail</th>     
                                        <th>สถานะ</th>                   
                                        <th><?php echo $this->lang->line('Option'); ?></th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr ng-repeat="item in modelDeviceList">
                                        <td>{{$index+1}}</td>
                                        <td ng-bind="item.NAME"></td>
                                        <td ng-bind="item.EMAIL"></td>
                                        <td><b ng-show="item.IsActive == 1" style="color:green">ปกติ</b><b style="color:red"ng-hide="item.IsActive == 1">ระงับใช้งาน</b></td>
                                        <td>
                                            <button ng-hide="item.NAME == 'admin' "ng-click="onEditTagClick(item )" class="btn btn-primary waves-effect waves-light btn-sm m-b-5"  ><i class="glyphicon glyphicon-edit"></i> <span class="hidden-xs"><?php echo $this->lang->line('Edit');?></span></button>
                                            <button ng-hide="item.NAME == 'admin' || item.IsActive == 0"my-confirm-click="onDeleteTagClick(item)" my-confirm-click-message="คุณต้องการระงับข้อมูลนี้หรือไม่ ?" class="btn btn-danger waves-effect waves-light btn-sm m-b-5"><i class="glyphicon glyphicon-remove"></i> <span class="hidden-xs">ปิดใช้งาน</span></button>
                                            <button ng-hide="item.NAME == 'admin' ||item.IsActive == 1"my-confirm-click="onUnDeleteTagClick(item)" my-confirm-click-message="คุณต้องเปิดใช้งานข้อมูลนี้หรือไม่ ?" class="btn btn-success waves-effect waves-light btn-sm m-b-5"><i class="glyphicon glyphicon-ok"></i> <span class="hidden-xs">เปิดใช้งาน</span></button>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                        <!-- /.table-responsive -->
                    </div>
                    <div id="changepassword" class="modal fade" role="dialog">
                        <div class="modal-dialog">

                            <!-- Modal content-->
                            <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal">&times;</button>
                                <h4 class="modal-title">Change Password</h4>
                            </div>
                            <div class="modal-body">
                                <div class="form-group col-lg-12 col-md-12 col-xs-12">
                                    <div class="col-lg-6 col-md-6 col-xs-12">
                                        <label>ใส่รหัสผ่านของคุณ</label>
                                    </div>
                                    <div class="col-lg-6 col-md-6 col-xs-12">
                                        <input type="password" class="form-control" ng-model="OldPass"> 
                                    </div>
                                </div>
                                <div class="form-group col-lg-12 col-md-12 col-xs-12">
                                    <div class="col-lg-6 col-md-6 col-xs-12">
                                        <label>ใส่รหัสผ่านใหม่</label>
                                    </div>
                                    <div class="col-lg-6 col-md-6 col-xs-12">
                                        <input type="password" class="form-control" ng-model="NewPass"> 
                                    </div>
                                </div>
                                <div class="form-group col-lg-12 col-md-12 col-xs-12">
                                    <div class="col-lg-6 col-md-6 col-xs-12">
                                        <label>ใส่รหัสผ่านใหม่อีกครั้ง</label>
                                    </div>
                                    <div class="col-lg-6 col-md-6 col-xs-12">
                                        <input type="password" class="form-control" ng-model="ReNewPass"> 
                                    </div>
                                </div>
                            </div>
                            <div class="modal-footer">
                                <button type="button" ng-click="savePass()"class="btn btn-primary">Save</button>
                                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                            </div>
                            </div>

                        </div>
                    </div>
                    <!-- ทำหน้า -->
                    <div class="row tblResult small">
                        <div class="col-md-7 col-sm-7 col-xs-12 ">
                            <label class="col-md-4 col-sm-4 col-xs-12">
                                <?php echo $this->lang->line('Total'); ?> {{totalRecords}} <?php echo $this->lang->line('Records'); ?>
                            </label>
                            <label class="col-md-4 col-sm-4 col-xs-12">
                                <?php echo $this->lang->line('ResultsPerPage'); ?>
                            </label>
                            <div class="col-md-4 col-sm-4 col-xs-12 ">
                                <ui-select ng-model="TempPageSize.selected" ng-change="loadByPageSize()" theme="selectize">
                                    <ui-select-match>{{$select.selected.Value}}</ui-select-match>
                                    <ui-select-choices repeat="pSize in listPageSize | filter: $select.search">
                                        <span ng-bind-html="pSize.Text | highlight: $select.search"></span>
                                    </ui-select-choices>
                                </ui-select>
                            </div>
                        </div>
                        <div class="col-md-5 col-sm-5 col-xs-12  ">
                            <label class="col-md-4 col-sm-4 col-xs-12">
                                <span ng-click="getBackPage()" class="set-pointer"><i class="fa fa-chevron-left"></i> <span class="hidden-xs"><?php echo $this->lang->line('Previous'); ?></span></span>
                            </label>
                            <div class="col-md-3 col-sm-3 col-xs-12">
                                <ui-select ng-model="TempPageIndex.selected" ng-change="searchByPage()" theme="selectize">
                                    <ui-select-match>{{$select.selected.PageIndex}}</ui-select-match>
                                    <ui-select-choices repeat="pIndex in listPageIndex | filter: $select.search">
                                        <span ng-bind-html="pIndex.PageIndex | highlight: $select.search"></span>
                                    </ui-select-choices>
                                </ui-select>
                            </div>
                            <label class="col-md-4 col-sm-4 col-xs-12">
                                / {{ totalPage }} <span ng-click="getNextPage()" class="set-pointer"><?php echo $this->lang->line('Next'); ?><i class="fa fa-chevron-right set-pointer"></i></span>
                            </label>
                        </div>
                    </div>
                    <!-- ทำหน้า -->

                </div>
                <!-- /.panel-body -->
            </div>
            <!-- /.panel -->
        </div>
        <!-- /.col-lg-12 -->
    </div>
    <!-- /List.row types-->
</div>
</div>