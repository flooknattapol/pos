<script src="<?php echo base_url('asset/memberController.js'); ?>"></script>
<div ng-controller="memberController" ng-init="onInit()">

    <div class="row">
        <ul class="navigator">
            <?php /*<li class="nav"><a href="/Rooms"><?php echo $this->lang->line('member');?></a></li>*/ ?>
            <li class="nav_active">Member</li>
        </ul>
        <!-- /.col-lg-12 -->
    </div>

    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">Member</h1>
        </div>
        <!-- /.col-lg-12 -->
    </div>

    <!-- /List.row types-->
    <div class="row  SearchDevice" style="display:none;">
        <div class="col-lg-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <?php echo $this->lang->line('Search'); ?>
                </div>
                <div class="panel-body">
                    <div class="form-group col-lg-12 col-md-12 col-xs-12">
                        <div class="col-lg-6 col-md-6 col-xs-12">
                            <label><span class="text-danger">*</span>Member</label>
                            <input class="form-control" ng-model="modelSearch.NAME" maxlength="80">
                            <p class="help-block"></p>
                        </div>
                        <div class="col-lg-6 col-md-6 col-xs-12">
                        </div>
                    </div>
                    <div class="form-group col-lg-12 col-md-12 col-xs-12">


                    </div>
                    <div class="col-lg-12 col-md-12 col-xs-12">
                        <button type="button" class="btn btn-primary waves-effect waves-light m-b-5" ng-click="resetSearch()"><i class="glyphicon glyphicon-repeat"></i> <span class="hidden-xs"><?php echo $this->lang->line('ResetSearch'); ?></span></button>
                        <button type="button" class="btn btn-primary waves-effect waves-light m-b-5" ng-click="LoadSearch()"><i class="fa fa-search"></i> <span class="hidden-xs"><?php echo $this->lang->line('Search'); ?></span></button>
                        <button type="button" class="btn btn-danger waves-effect waves-light m-b-5" ng-click="ShowDevice()"><i class="fa fa-times"></i> <span class="hidden-xs"><?php echo $this->lang->line('Cancel'); ?></span></button>
                    </div>
                </div>
                <!-- /.panel-body -->
            </div>
            <!-- /.panel -->
        </div>
        <!-- /.col-lg-12 -->
    </div>
    <!-- /List.row types-->


    <!-- / create room types  -->
    <div class="row addDevice" style="display:none;">
        <div class="col-lg-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    Manage User
                </div>
                <div class="panel-body">
                    <div class="row">

                        <div class="col-lg-8 col-md-8 col-xs-12" style="padding-right:0px">
                            <div role="form">
                                <br>
                                <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
                                    <div class="form-group col-lg-12 col-md-12 col-xs-12">
                                        <div class="col-lg-6 col-md-6 col-xs-12">
                                            <div style="text-align:center">
                                             <img src="<?php echo base_url('/upload/Member/Avatar/');?>{{CreateModel.IMAGE}}"  border="5"alt="" style="height:50%;width:50%" />       
                                             <br><small>รูปภาพ</small>   
                                            </div>                      
                                        </div>
                                        <div class="col-lg-6 col-md-6 col-xs-12">
                                            <div style="text-align:center">
                                             <img src="{{CreateModel.QRURL}}"  border="5"alt="" style="height:50%;width:50%" />       
                                             <br><small>QR Code</small>   
                                            </div> 
                                        </div>
                                    </div>
                                    <div class="form-group col-lg-12 col-md-12 col-xs-12">
                                        <div class="col-lg-2 col-md-2 col-xs-12">
                                            <h5><span class="text-danger"></span>ชื่อ</h5>
                                            
                                        </div>
                                        <div class="col-lg-4 col-md-4 col-xs-12">
                                            <input class="form-control" ng-model="CreateModel.NAME" >
                                            <p class="CreateModel_NAME require text-danger"><?php echo $this->lang->line('Require'); ?></p>
                                        </div>
                                        <div class="col-lg-2 col-md-2 col-xs-12">
                                            <h5><span class="text-danger"></span>นามสกุล</h5>
                                        </div>
                                        <div class="col-lg-4 col-md-4 col-xs-12">
                                            <input class="form-control" ng-model="CreateModel.LASTNAME" >
                                            <p class="CreateModel_LASTNAME require text-danger"><?php echo $this->lang->line('Require'); ?></p>
                                        </div>
                                    </div>
                                    <div class="form-group col-lg-12 col-md-12 col-xs-12">
                                        <div class="col-lg-2 col-md-2 col-xs-12">
                                            <h5><span class="text-danger"></span>E-Mail</h5>
                                        </div>
                                        <div class="col-lg-4 col-md-4 col-xs-12">
                                            <input class="form-control" ng-model="CreateModel.EMAIL" >
                                        </div>
                                        <div class="col-lg-2 col-md-2 col-xs-12">
                                            <h5><span class="text-danger"></span>เบอร์โทร</h5>
                                        </div>
                                        <div class="col-lg-4 col-md-4 col-xs-12">
                                            <input class="form-control" ng-model="CreateModel.TEL" >
                                        </div>
                                    </div>
                                    <div class="form-group col-lg-12 col-md-12 col-xs-12">
                                        <div class="col-lg-2 col-md-2 col-xs-12">
                                            <h5><span class="text-danger"></span>บริษัท</h5>
                                        </div>
                                        <div class="col-lg-4 col-md-4 col-xs-12">
                                            <input class="form-control" ng-model="CreateModel.COMPANY">
                                        </div>
                                        <div class="col-lg-2 col-md-2 col-xs-12">
                                            <h5><span class="text-danger"></span>ตำแหน่ง</h5>
                                        </div>
                                        <div class="col-lg-4 col-md-4 col-xs-12">
                                            <input class="form-control" ng-model="CreateModel.POSITION">
                                        </div>
                                    </div>
                                    <div class="form-group col-lg-12 col-md-12 col-xs-12">
                                        <div class="col-lg-2 col-md-2 col-xs-12">
                                            <h5><span class="text-danger"></span>ติดต่อ</h5>
                                        </div>
                                        <div class="col-lg-4 col-md-4 col-xs-12">
                                            <input class="form-control" ng-model="CreateModel.CONTACT">
                                        </div>
                                    </div>
                                    <div class="form-group col-lg-12 col-md-12 col-xs-12">
                                    <label>Voucher List</label>
                                        <hr>              
                                        <div class="table-responsive">
                                            <table class="table table-striped">
                                                <thead>
                                                    <tr>
                                                        <th>อีเว้นท์</th>
                                                        <th>Voucher</th>
                                                        <th>รวม</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <tr ng-repeat="item in VoucherDetailModel">
                                                        <td ng-bind="item.event_NAME"></td>
                                                        <td ng-bind="item.VOUCHER "></td>
                                                        <td ng-bind="item.TOTAL "></td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                        <!-- /.table-responsive -->
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="col-lg-12 col-md-12 col-xs-12">
                            <div class="form-group col-lg-12 col-md-12 col-xs-12  text-right">
                                <br>
                                <div class="col-lg-12 col-md-12 col-xs-12">
                                    <button class="btn btn-primary" ng-click="onSaveTagClick()"><i class="fa fa-save"></i> <span class="hidden-xs"><?php echo $this->lang->line('Save'); ?></span></button>
                                    <button class="btn btn-danger" ng-click="ShowDevice()"><i class="fa fa-arrow-left "></i> <span class="hidden-xs">ย้อนกลับ</span></button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <!-- /.col-lg-12 -->
    </div>
    <!-- /.create room types -->


    <!-- /List.row types-->
    <div class="row DisplayDevice">
        <div class="col-lg-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    Manage Member
                </div>
                <div class="panel-body">
                    <div class="col-lg-12 col-md-12 col-xs-12">
                        <!-- <button class="btn btn-primary"ng-click="AddNewDevice()"><i class="fa fa-plus  "></i> <span class="hidden-xs"><?php echo $this->lang->line('Add'); ?></span></button> -->
                        <button class="btn btn-info" ng-click="ShowSearch()"><i class="fa fa-search  "></i> <span class="hidden-xs"><?php echo $this->lang->line('Search'); ?></span></button>
                    </div>
                    <div class="col-lg-12 col-md-12 col-xs-12">
                        <div class="table-responsive">
                            <table class="table table-striped">
                                <thead>
                                    <tr>
                                        <th witdh="5%">#</th>
                                        <th>QR code</th>
                                        <th>รูป</th>
                                        <th class="sorting_asc" ng-click="sort($event)" sort="NAME">ชื่อ-สกุล</th>
                                        <th class="sorting" ng-click="sort($event)" sort="TEL">เบอร์โทร</th>
                                        <th class="sorting" ng-click="sort($event)" sort="EMAIL">อีเมลล์</th>
                                        <th class="sorting" ng-click="sort($event)" sort="CONTACT">ติตต่อ</th>
                                        <th>เพื่อน</th>
                                        <th><?php echo $this->lang->line('Option'); ?></th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr ng-repeat="item in modelDeviceList">
                                        <td>{{$index+1}}</td>
                                        <td><img ng-src="{{item.QRURL}}"style="height:100px;"></td>
                                        <td><img ng-src="<?php echo base_url('/upload/Member/Avatar/');?>{{item.IMAGE}}" style="height:100px;"></td>
                                        <td>{{item.NAME}} {{item.LASTNAME}}</td>
                                        <td ng-bind="item.TEL "></td>
                                        <td ng-bind="item.EMAIL "></td>
                                        <td ng-bind="item.CONTACT "></td>
                                        <td ><u data-toggle="modal" data-target="#Viewfriend" ng-click="viewFriend(item)"><b ng-hide="item.IsActive == 0"style="cursor: pointer">ดูรายชื่อเพื่อนและเพิ่มเพื่อน</b></u></td>
                                        <td>
                                            <font ng-show="item.IsActive == 0"style="color:red">ถูกปิดใช้งาน</font>
                                            <button ng-click="onEditTagClick(item)" class="btn btn-primary waves-effect waves-light btn-sm m-b-5"><i class="glyphicon glyphicon-th-large"></i> <span class="hidden-xs">ดูรายละเอียด</span></button>
                                            <!-- <button my-confirm-click="onDeleteTagClick(item)" my-confirm-click-message="<?php echo $this->lang->line('DoYouWantToDelete'); ?>" class="btn btn-danger waves-effect waves-light btn-sm m-b-5"><i class="glyphicon glyphicon-trash"></i> <span class="hidden-xs"><?php echo $this->lang->line('Delete'); ?></span></button> -->
                                            <button ng-hide="item.IsActive == 0"my-confirm-click="onDeleteTagClick(item)" my-confirm-click-message="<?php echo $this->lang->line('DoYouWantToDelete'); ?>" class="btn btn-danger waves-effect waves-light btn-sm m-b-5"><i class="glyphicon glyphicon-trash"></i> <span class="hidden-xs">ปิดการใช้งาน</span></button>
                                           
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                        <!-- /.table-responsive -->
                    </div>
                    <!-- The Modal -->
                    <div class="modal" id="Viewfriend">
                        <div class="modal-dialog modal-lg">
                            <div class="modal-content">

                            <!-- Modal body -->
                            <div class="modal-body">
                                
                                <div class="col-lg-12 col-md-12 col-xs-12">
                                    <h3>รายชื่อเพื่อนของ{{NameMemberHeaderViewFriend}}</h3>
                                    <hr>
                                    <table class="table table-striped">
                                        <thead>
                                            <tr>
                                                <th width="5%">#</th>
                                                <th width="30%">รูปภาพ</th>
                                                <th width="30%"class="sorting_asc" ng-click="sort($event)" sort="Member_NAME">ชื่อ-สกุล</th>
                                                <th>โทร.</th>
                                                <th>E-mail</th>
                                                <th width="20%">Status</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr ng-repeat="item in FrindList">
                                                <td>{{$index+1}}</td>
                                                <td><img ng-src="<?php echo base_url('/upload/Member/Avatar/');?>{{item.IMAGE}}" style="height:50px;"></td>
                                                <td>{{item.NAME}} {{item.LASTNAME}}</td>        
                                                <td>{{item.TEL}}</td>      
                                                <td>{{item.EMAIL}}</td>      
                                                <td>
                                                    <font ng-show="item.IsActive == 1" style="color:green">ปกติ</font>
                                                    <font ng-show="item.IsActive == 0" style="color:red">ถูกปิดใช้งาน</font>
                                                </td>            
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>   
                                <div class="col-lg-12 col-md-12 col-xs-12">
                                    <div class="col-lg-12 col-md-12 col-xs-12">
                                        <label>เพิ่มเพื่อน</label>
                                    </div>
                                    <div class="col-lg-12 col-md-12 col-xs-12">
                                        <div class="col-lg-3 col-md-3 col-xs-3">
                                            <img ng-src="<?php echo base_url('/upload/Member/Avatar/');?>{{imgaddFrd}}" style="height:50px;">
                                        </div>
                                        <div class="col-lg-5 col-md-5 col-xs-5">
                                            <ui-select ng-model="TempMemberIndex.selected" theme="selectize" ng-change="LoadImgAdd()">
                                                <ui-select-match>{{$select.selected.NAME}}</ui-select-match>
                                                <ui-select-choices repeat="item in ListMemberModel | filter: $select.search">
                                                    <span ng-bind-html="item.NAME | highlight: $select.search"></span>
                                                </ui-select-choices>
                                            </ui-select>
                                            <p class="Input_friend require text-danger">กรุณาเลือกชื่อเพื่อน</p>
                                            <p class="Input_friendInlist require text-danger">คุณเป็นเพื่อนกันแล้ว</p>
                                        </div>
                                        <div class="col-lg-4 col-md-4 col-xs-4">
                                            <button class="btn btn-primary"ng-click="AddFriend()"><i class="fa fa-plus  "></i> <span class="hidden-xs"><?php echo $this->lang->line('Add'); ?></span></button>
                                        </div>
                                    </div>          
                                </div>
                            </div>

                            <!-- Modal footer -->
                            <div class="modal-footer">
                                <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                            </div>

                            </div>
                        </div>
                    </div>
                    <!-- ทำหน้า -->
                    <div class="row tblResult small">
                        <div class="col-md-7 col-sm-7 col-xs-12 ">
                            <label class="col-md-4 col-sm-4 col-xs-12">
                                <?php echo $this->lang->line('Total'); ?> {{totalRecords}} <?php echo $this->lang->line('Records'); ?>
                            </label>
                            <label class="col-md-4 col-sm-4 col-xs-12">
                                <?php echo $this->lang->line('ResultsPerPage'); ?>
                            </label>
                            <div class="col-md-4 col-sm-4 col-xs-12 ">
                                <ui-select ng-model="TempPageSize.selected" ng-change="loadByPageSize()" theme="selectize">
                                    <ui-select-match>{{$select.selected.Value}}</ui-select-match>
                                    <ui-select-choices repeat="pSize in listPageSize | filter: $select.search">
                                        <span ng-bind-html="pSize.Text | highlight: $select.search"></span>
                                    </ui-select-choices>
                                </ui-select>
                            </div>
                        </div>
                        <div class="col-md-5 col-sm-5 col-xs-12  ">
                            <label class="col-md-4 col-sm-4 col-xs-12">
                                <span ng-click="getBackPage()" class="set-pointer"><i class="fa fa-chevron-left"></i> <span class="hidden-xs"><?php echo $this->lang->line('Previous'); ?></span></span>
                            </label>
                            <div class="col-md-3 col-sm-3 col-xs-12">
                                <ui-select ng-model="TempPageIndex.selected" ng-change="searchByPage()" theme="selectize">
                                    <ui-select-match>{{$select.selected.PageIndex}}</ui-select-match>
                                    <ui-select-choices repeat="pIndex in listPageIndex | filter: $select.search">
                                        <span ng-bind-html="pIndex.PageIndex | highlight: $select.search"></span>
                                    </ui-select-choices>
                                </ui-select>
                            </div>
                            <label class="col-md-4 col-sm-4 col-xs-12">
                                / {{ totalPage }} <span ng-click="getNextPage()" class="set-pointer"><?php echo $this->lang->line('Next'); ?><i class="fa fa-chevron-right set-pointer"></i></span>
                            </label>
                        </div>
                    </div>
                    <!-- ทำหน้า -->

                </div>
                <!-- /.panel-body -->
            </div>
            <!-- /.panel -->
        </div>
        <!-- /.col-lg-12 -->
    </div>
    <!-- /List.row types-->
</div>
</div>