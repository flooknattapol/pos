myApp.controller('dashboardbygoodsController', ['$scope', '$filter', 'Upload', 'baseService', 'dashboardbygoodsApiService','reportSaleApiService', function ($scope, $filter, Upload, baseService, dashboardbygoodsApiService,reportSaleApiService) {

	$scope.ViewImgPreview = false;
	$scope.modelDeviceList = [];
	$scope.CreateModel = {};
	$scope.modelSearch = {};
	$scope.GoodsTop5 = [];
	$scope.salaryModel = {};
	$scope.salaryModelList = [];
	$scope.oldSalaryModelList = [];
	$scope.TempNameTitleTH = {};
	$scope.TempNameTitleEN = {};
	$scope.listNameTitle = [{ th: "นาย", en: "Mr." }, { th: "นาง", en: "Mrs." }, { th: "นางสาว", en: "Miss" }];
	var d = new Date();
        var year = d.getFullYear();
        var month = d.getMonth();
        var day = d.getDate();
	$scope.input_dateStart = new Date(year, month - 1, day);
	$scope.input_dateEnd = new Date(year, month, day);
	$scope.modelSearch = {
		"DateStart":formatDate($scope.input_dateStart),
		"DateEnd":formatDate($scope.input_dateEnd)
	};
	function formatDate(date){
		var d = new Date(date),
			month = '' + (d.getMonth() + 1),
			day = '' + d.getDate(),
			year = d.getFullYear();
	
		if (month.length < 2) 
			month = '0' + month;
		if (day.length < 2) 
			day = '0' + day;
	
		return [year, month, day].join('-');
	}
	$scope.TempEventIndex = {selected:""};
	$scope.listEmpType = [];
	$scope.TempEmpTypeIndex = {}

	//page system 
	$scope.listPageSize = baseService.getListPageSize();
	$scope.TempPageSize = {};
	$scope.TempPageIndex = {};
	$scope.PageSize = baseService.setPageSize(20);;
	$scope.totalPage = 1; //init;
	$scope.totalRecords = 0;
	$scope.PageIndex = 1;
	$scope.SortColumn = baseService.setSortColumn('id');
	$scope.SortOrder = baseService.setSortOrder('asc');

	$scope.salaryTab = "tab";
	$scope.isView = false;

	$scope.sort = function (e) {
		baseService.sort(e); $scope.SortColumn = baseService.getSortColumn();
		$scope.SortOrder = baseService.getSortOrder();
		$scope.reload();
	}

	$scope.getFirstPage = function () { $scope.PageIndex = baseService.getFirstPage(); $scope.reload(); }
	$scope.getBackPage = function () { $scope.PageIndex = baseService.getBackPage(); $scope.reload(); }
	$scope.getNextPage = function () { $scope.PageIndex = baseService.getNextPage(); $scope.reload(); }
	$scope.getLastPage = function () { $scope.PageIndex = baseService.getLastPage(); $scope.reload(); }
	$scope.searchByPage = function () { $scope.PageIndex = baseService.setPageIndex($scope.TempPageIndex.selected.PageIndex); $scope.reload(); }
	$scope.setPageSize = function (data) { $scope.PageSize = baseService.setPageSize($scope.TempPageSize.selected.Value); }
	$scope.loadByPageSize = function () { $scope.PageIndex = baseService.setPageIndex(1); $scope.setPageSize(); $scope.reload(); }
	//page system


	$scope.ShowDevice = function () {
		$(".DisplayDevice").show();
		$(".SearchDevice").hide();
		$(".addDevice").hide();
		$("#saveChange").modal('hide');
		$scope.reload();
	}
	$scope.onChange = function () {
		// console.log(JSON.stringify($scope.CreateModel),JSON.stringify($scope.oldCreateModel))
		var resultObj = JSON.stringify($scope.CreateModel) === JSON.stringify($scope.oldCreateModel)
		var resultArray = JSON.stringify(JSON.parse(angular.toJson($scope.salaryModelList))) === JSON.stringify($scope.oldSalaryModelList)

		console.log($scope.oldSalaryModelList)
		console.log($scope.salaryModelList)
		console.log(resultArray)
		if (resultObj == true && resultArray == true) {
			$scope.ShowDevice();
		} else {
			$("#saveChange").modal('show');
		}

	}
	$scope.ShowSearch = function () {
		$(".DisplayDevice").hide();
		$(".SearchDevice").show();
		$(".addDevice").hide();
	}

	$scope.LoadSearch = function () {
		$scope.ShowDevice();

	}

	$scope.AddNewDevice = function () {
		$scope.resetModel();
		$(".require").hide();
		$(".DisplayDevice").hide();
		$(".SearchDevice").hide();
		$(".addDevice").show();
		$scope.salaryTab = "";
	}

	$scope.onEditTagClick = function (item) {
		$scope.AddNewDevice();
		$scope.loadEditData(item);

	}

	$scope.loadEditData = function (item) {

		$scope.CreateModel = angular.copy(item);
	}
	
	$scope.resetModel = function () {

		$scope.CreateModel = {
			ID: 0,
		};
		
	}
	


	////////////////////////////////////////////////////////////////////////////////////////
	// Event
	$scope.onInit = function () {

		$scope.resetModel();
		$scope.reload();
	}

	$scope.viewDashboardbygoods = function(){
		if($scope.TempEventIndex.selected != ""){
			$scope.CreateModel.ID = $scope.TempEventIndex.selected.ID;
			$scope.reload();
		}else{
			baseService.showMessage("กรุณาเลือก Event");
		}
	}
	$scope.dynamicColors = function() {
		var r = Math.floor(Math.random() * 255);
		var g = Math.floor(Math.random() * 255);
		var b = Math.floor(Math.random() * 255);
		return "rgb(" + r + "," + g + "," + b + ")";
	 };
	 
	$scope.reload = function () {
		dashboardbygoodsApiService.getHeader($scope.modelSearch, function (results) {
			console.log(results);
			$scope.GoodsTop5 = results;
		});
		dashboardbygoodsApiService.getList($scope.modelSearch, function (result) {
			console.log(result);
			$scope.totalPage = result.data.toTalPage;
			$scope.listPageIndex = baseService.getListPage(result.data.toTalPage);
			$scope.listPageIndex.forEach(function (entry, index) {
				if ($scope.PageIndex === entry.Value)
					$scope.TempPageIndex.selected = entry;
			});
			$scope.totalRecords = result.data.totalRecords;
			$scope.modelDeviceList = result.data.message;
		});
		dashboardbygoodsApiService.getListRefund($scope.modelSearch, function (result) {
			console.log(result);
			$scope.totalPage = result.data.toTalPage;
			$scope.listPageIndex = baseService.getListPage(result.data.toTalPage);
			$scope.listPageIndex.forEach(function (entry, index) {
				if ($scope.PageIndex === entry.Value)
					$scope.TempPageIndex.selected = entry;
			});
			$scope.totalRecordsRefund = result.data.totalRecords;
			$scope.modelDeviceListRefund = result.data.message;
		});
		reportSaleApiService.getGraph($scope.modelSearch, function (result) {
			$scope.label = [];
			$scope.data = [];
			$scope.ReportList = result.data.message;
			$scope.ReportList.forEach(function (entry, index) {
				$scope.label.push(entry.Day);
				$scope.data.push(entry.Total);
			});
			var color = Chart.helpers.color;
		if (typeof window.myBar !== "undefined") {
			window.myBar.destroy();
		}
		var barChartData = {
			labels: $scope.label,
			datasets: [{
				label: 'ยอดขาย ',
				backgroundColor: color(window.chartColors.red).alpha(0.5).rgbString(),
				borderColor: window.chartColors.red,
				borderWidth: 1,
				data: $scope.data
			}]

		};
	
		var ctx = document.getElementById('canvas').getContext('2d');
		window.myBar = new Chart(ctx, {
			type: 'bar',
			data: barChartData,
			options: {
				responsive: true,
				plugins: {
					datalabels: {
						display: function (context) {
							//console.log(context.dataset.data[context.dataIndex]);
							if(parseInt(context.dataset.data[context.dataIndex]) >= 1000){
								//console.log(context.dataset.data[context.dataIndex].toString().replace(/\B(?=(\d{3})+(?!\d))/g, ","));
								return context.dataset.data[context.dataIndex].toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
							} else {
								return context.dataset.data[context.dataIndex];
							}
							//return context.dataset.data[context.dataIndex] !== 0; // or >= 1 or ...
						},
						fontsize: 2,
						color: '#fff'
					}
				},
				legend: {
					position: 'bottom',
				},
				title: {
					display: true,
					text: 'ยอดขายรวมสินค้ารายวัน'
				},
				tooltips: {
					"enabled": true,
					callbacks: {
						label: function(tooltipItem, data) {
							var value = data.datasets[0].data[tooltipItem.index];
							if(parseInt(value) >= 1000){
								return value.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
							} else {
								return value;
							}
						}
				  	} 
					
				},
				scales: {
				  yAxes: [{
					display: true,
					gridLines: {
						display: true
					},
					ticks: {
						display: true,
						beginAtZero: true,
						userCallback: function(value, index, values) {
							value = value.toString();
							value = value.split(/(?=(?:...)*$)/);
							value = value.join(',');
							return value;
						}
					}
				  }],
				  xAxes: [{
					gridLines: {
						display: true
					},
					ticks: {
					 	beginAtZero: true
					}
				  }]
				}
			}
		});

		});
	}
	

}]); 