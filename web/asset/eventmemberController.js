myApp.controller('eventmemberController', ['$scope', '$filter', 'Upload', 'baseService', 'eventmemberApiService','eventApiService','memberApiService','goodsApiService','vendorApiService', function ($scope, $filter, Upload, baseService, eventmemberApiService,eventApiService,memberApiService,goodsApiService,vendorApiService) {

	$scope.modelDeviceList = [];
	$scope.CreateModel = {};
	$scope.modelSearch = {};
	$scope.vendorDetailModel = [];
	$scope.salaryModel = {};
	$scope.salaryModelList = [];
	$scope.oldSalaryModelList = [];
	$scope.TempNameTitleTH = {};
	$scope.TempNameTitleEN = {};
	$scope.listNameTitle = [{ th: "นาย", en: "Mr." }, { th: "นาง", en: "Mrs." }, { th: "นางสาว", en: "Miss" }];

	$scope.listEmpType = [];
	$scope.TempEmpTypeIndex = {};
	$scope.TempEventIndex = {};
	$scope.TempMemberIndex = {};

	$scope.Edit = false;
	//page system 
	$scope.listPageSize = baseService.getListPageSize();
	$scope.TempPageSize = {};
	$scope.TempPageIndex = {};
	$scope.PageSize = baseService.setPageSize(20);;
	$scope.totalPage = 1; //init;
	$scope.totalRecords = 0;
	$scope.PageIndex = 1;
	$scope.SortColumn = baseService.setSortColumn('NAME');
	$scope.SortOrder = baseService.setSortOrder('asc');

	$scope.salaryTab = "tab";
	$scope.isView = false;

	$scope.sort = function (e) {
		baseService.sort(e); $scope.SortColumn = baseService.getSortColumn();
		$scope.SortOrder = baseService.getSortOrder();
		$scope.reload();
	}

	$scope.getFirstPage = function () { $scope.PageIndex = baseService.getFirstPage(); $scope.reload(); }
	$scope.getBackPage = function () { $scope.PageIndex = baseService.getBackPage(); $scope.reload(); }
	$scope.getNextPage = function () { $scope.PageIndex = baseService.getNextPage(); $scope.reload(); }
	$scope.getLastPage = function () { $scope.PageIndex = baseService.getLastPage(); $scope.reload(); }
	$scope.searchByPage = function () { $scope.PageIndex = baseService.setPageIndex($scope.TempPageIndex.selected.PageIndex); $scope.reload(); }
	$scope.setPageSize = function (data) { $scope.PageSize = baseService.setPageSize($scope.TempPageSize.selected.Value); }
	$scope.loadByPageSize = function () { $scope.PageIndex = baseService.setPageIndex(1); $scope.setPageSize(); $scope.reload(); }
	//page system


	$scope.ShowDevice = function () {
		$(".DisplayDevice").show();
		$(".SearchDevice").hide();
		$(".addDevice").hide();
		$("#saveChange").modal('hide');
		$scope.reload();
	}
	$scope.onChange = function () {
		// console.log(JSON.stringify($scope.CreateModel),JSON.stringify($scope.oldCreateModel))
		var resultObj = JSON.stringify($scope.CreateModel) === JSON.stringify($scope.oldCreateModel)
		var resultArray = JSON.stringify(JSON.parse(angular.toJson($scope.salaryModelList))) === JSON.stringify($scope.oldSalaryModelList)

		console.log($scope.oldSalaryModelList)
		console.log($scope.salaryModelList)
		console.log(resultArray)
		if (resultObj == true && resultArray == true) {
			$scope.ShowDevice();
		} else {
			$("#saveChange").modal('show');
		}

	}
	$scope.ShowSearch = function () {
		$(".DisplayDevice").hide();
		$(".SearchDevice").show();
		$(".addDevice").hide();
	}

	$scope.LoadSearch = function () {
		$scope.ShowDevice();

	}

	$scope.AddNewDevice = function () {
		$scope.Edit = false;
		$scope.resetModel();
		$(".require").hide();
		$(".DisplayDevice").hide();
		$(".SearchDevice").hide();
		$(".addDevice").show();
		$scope.salaryTab = "";
	}

	$scope.onEditTagClick = function (item) {
		$scope.AddNewDevice();
		$scope.loadEditData(item);

	}

	$scope.loadEditData = function (item) {
		$scope.Edit = true;
		$scope.CreateModel = angular.copy(item);
		$scope.ListEventModel.forEach(function (entry, index) {
			if ($scope.CreateModel.EVENT_ID === entry.ID)
				$scope.TempEventIndex.selected = entry;
		});
		$scope.ListMemberModel.forEach(function (entry, index) {
			if ($scope.CreateModel.MEMBER_ID === entry.ID)
				$scope.TempMemberIndex.selected = entry;
		});
		vendorApiService.listvendorDetail({ MEMBER_ID: item.MEMBER_ID,EVENT_ID:item.EVENT_ID }, function (result) {
			$scope.vendorDetailModelTemp = result.message
			console.log($scope.vendorDetailModelTemp);
			$scope.vendorDetailModelTemp.forEach(function (entry, index) {
				console.log(entry);
				if ( entry.IsActiveNotShow === false){
					
					$scope.vendorDetailModel.push(entry);
				}
			});
			console.log($scope.vendorDetailModel);
		});
	}
	
	$scope.resetModel = function () {

		$scope.CreateModel = {
			ID: 0, MEMBER_ID: "", EVENT_ID: "", BUYER: true,
			VENDOR: "",VENDOR_NAME:""
		};
		
		$scope.vendorDetailModel = [];
		$scope.TempMemberIndex = {};
		$scope.TempEventIndex = {};
	}
	

	$scope.resetSearch = function () {
		$scope.modelSearch = {
			"code_id": "",
			"code": "",
			"name": "",
			"lastname": "",
			"flag":false,
		};
		$scope.LoadSearch();
	}

	////////////////////////////////////////////////////////////////////////////////////////
	// Event
	$scope.onInit = function () {

		$scope.resetModel();
		$scope.resetSearch();
		if ($scope.listNameTitle.length > 0) {
			$scope.TempNameTitleEN.selected = $scope.listNameTitle[0];
			$scope.TempNameTitleTH.selected = $scope.listNameTitle[0];
		}


		$scope.listPageSize.forEach(function (entry, index) {
			if (0 === index)
				$scope.TempPageSize.selected = entry;
		});

		//$scope.reload();
	}
	$scope.AddNewLine = function () {

		var lineNo = $scope.vendorDetailModel.length + 1;

		if (lineNo < 26) {
			$scope.vendorDetailModel.push(
				{  ID: 0, SKU: "", MEMBER_ID:$scope.CreateModel.MEMBER_ID, EVENT_ID:$scope.CreateModel.EVENT_ID,NAME:"", UNIT: "", PRICE: "", IMAGE: "" ,TYPE:1,EventNotActive:false,qty_privilege:0,discount:0}
			)
		} else {
			baseService.showMessage("maximum is 26 lines");
		}
		console.log($scope.vendorDetailModel);
	}
	$scope.reload = function () {
		eventApiService.getComboBox(null, function (result) {
			if (result.data.status === true) {
				$scope.ListEventModel = result.data.message;
			} else {
				baseService.showMessage(result.message);
			}
		});
		memberApiService.getComboBox(null, function (result) {
			if (result.data.status === true) {
				$scope.ListMemberModel = result.data.message;
			} else {
				baseService.showMessage(result.message);
			}
		});
		goodsApiService.getTypeComboBox(null, function (result) {
			if (result.data.status === true) {
				$scope.TypeGoods = result.data.message;
				console.log($scope.TypeGoods);
			} else {
				baseService.showMessage(result.message);
			}
		});
		eventmemberApiService.listEventMember($scope.modelSearch, function (results) {
			var result = results.data;
			if (result.status === true) {

				$scope.totalPage = result.toTalPage;
				$scope.listPageIndex = baseService.getListPage(result.toTalPage);
				$scope.listPageIndex.forEach(function (entry, index) {
					if ($scope.PageIndex === entry.Value)
						$scope.TempPageIndex.selected = entry;
				});
				$scope.totalRecords = result.totalRecords;
				$scope.modelDeviceList = result.message;
				console.log(result.message);
				$scope.modelDeviceList.forEach(function (entry, index) {
					if (entry.BUYER == 1){
						entry.BUYER = true;
					}
					if (entry.VENDOR == 1){
						entry.VENDOR = true;
					}	
				});

			} else {

			}
		});

		
		$('#collapseOne').collapse('show')
		$('#collapseTwo').collapse('hide')
		$('.nav-tabs a[href="#tab1default"]').tab('show');
	}

	$scope.onDeleteTagClick = function (item) {
		eventmemberApiService.deleteEventMember({ ID: item.ID }, function (result) {
			if (result.status === true) {
				$scope.reload();
			} else {
				baseService.showMessage(result.message);
			}
		});

	}

	$scope.validatecheck = function () {
		var bResult = true;
		$(".require").hide();

		// console.log($scope.CreateModel.VENDOR , $scope.CreateModel.VENDOR_NAME);
		if ($scope.CreateModel.VENDOR == true && $scope.CreateModel.VENDOR_NAME == "") {
			$(".CreateModel_VENDOR_NAME").show();
			bResult = false;
		}
		if ($scope.TempMemberIndex.selected == undefined) {
			$(".CreateModel_MEMBER_ID").show();
			bResult = false;
		}

		if ($scope.TempEventIndex.selected == undefined) {
			$(".CreateModel_EVENT_ID").show();
			bResult = false;
		}
		return bResult;
	}
	$scope.LoadShopNameByCus = function(){
		console.log($scope.TempMemberIndex.selected.VENDOR_NAME);
		$scope.CreateModel.VENDOR_NAME = $scope.TempMemberIndex.selected.VENDOR_NAME;
	}
	$scope.onSaveTagClick = function (i) {
		
		var bValid = $scope.validatecheck();
		if (true == bValid) {
		$scope.CreateModel.MEMBER_ID = $scope.TempMemberIndex.selected.ID;
		$scope.CreateModel.EVENT_ID = $scope.TempEventIndex.selected.ID;
		if($scope.CreateModel.BUYER == true){$scope.CreateModel.BUYER = 1;}else{$scope.CreateModel.BUYER = 0;}
		if($scope.CreateModel.VENDOR == true){$scope.CreateModel.VENDOR = 1;}else{$scope.CreateModel.VENDOR = 0;}

		$scope.vendorDetailModel.forEach(function (entry, index) {
			$scope.EventNotActive = [];
			if(entry.EventNotActive != ""){
				$scope.EventNotActive = JSON.parse(entry.EventNotActive);
				$scope.EventNotActive.forEach(function (entry, index) {
					if(entry.EVENT_ID === $scope.TempEventIndex.selected.ID){
						console.log(entry.EVENT_ID,$scope.TempEventIndex.selected.ID);
						$scope.EventNotActive.splice(index,1);
					}
				});
			}
			console.log("step1",$scope.EventNotActive);
			if ( entry.GoodsActive === true){
				
				$scope.EventNotActive.push({EVENT_ID:$scope.TempEventIndex.selected.ID});
				console.log("step2",$scope.EventNotActive);
				entry.EventNotActive = $scope.EventNotActive;
				console.log("สถานะลบ",entry);
			}else{
				entry.EventNotActive = $scope.EventNotActive;
			}
			console.log("สถานะลบ",entry);
		});
		
		$scope.CreateModel.DetailGoods = $scope.vendorDetailModel;
		console.log($scope.CreateModel.DetailGoods);
		
			eventmemberApiService.saveEventMember($scope.CreateModel, function (result) {
				if (result.status == true) {
					// $scope.ShowDevice();
					vendorApiService.listvendorDetail({ MEMBER_ID: $scope.TempMemberIndex.selected.ID,EVENT_ID:$scope.TempEventIndex.selected.ID }, function (result) {
						$scope.vendorDetailModelTemp = result.message
						$scope.vendorDetailModel = [];
						$scope.vendorDetailModelTemp.forEach(function (entry, index) {
			
							if ( entry.IsActiveNotShow !== true){
								console.log("dada");
								$scope.vendorDetailModel.push(entry);
							}
						});
					});
					baseService.showMessage("บันทึกเสร็จสิ้น");
				} else {
					baseService.showMessage(result.message);
				}
			});

		}
	}	
	$scope.UploadImage = function(item){
		$scope.filegeneralinfo = "";
		console.log(item)
		$scope.modelUpload = item;
	}	
	$scope.upload = function () {
		console.log($scope.modelUpload);
        
        if(undefined != $scope.filegeneralinfo){   

			var filegeneralinfo = $scope.filegeneralinfo.name;
            Upload.upload({
                url: 'Vendor/upload_file',
                data: {
                    filegeneralinfo: $scope.filegeneralinfo
                }
            })
            .then(function (resp) {
                if (resp.status == 200) {
					// console.log(resp.data.message);
                    $scope.modelUpload.IMAGE = resp.data.message;
					$('#UploadImg').modal('hide');
                    // baseService.showMessage("Save success");
                } else {
                    baseService.showMessage("Can not upload file");
                }
            });
        }else{
            
        }
    }	
	$scope.loadGoodsDetailByMember = function () {

		if($scope.TempMemberIndex.selected == undefined){
			baseService.showMessage("กรุณาเลือกผู้ขาย");
		}else{
			$scope.CreateModel.MEMBER_ID = $scope.TempMemberIndex.selected.ID;

			vendorApiService.loadGoodsDetailByMember($scope.CreateModel, function (result) {
				console.log(result.message);
				if (result.status == true) {
					$scope.vendorDetailModel = result.message;
					console.log(result.message);
				} else {
					baseService.showMessage(result.message);
				}
			});
		}
		

		
	}	
}]); 