myApp.controller('customerController', ['$scope', '$filter', 'Upload', 'baseService', 'customerApiService','customercategoryApiService', function ($scope, $filter, Upload, baseService, customerApiService,customercategoryApiService ) {

	$scope.TempCustomercategoryIndex = {};
	$scope.modelDeviceList = [];
	$scope.CreateModel = {};
	$scope.modelSearch = {};


	
	$scope.TempMemberIndex = {};
	$scope.TempcategoryGoodsIndex = {};
	//page system 
	$scope.listPageSize = baseService.getListPageSize();
	$scope.TempPageSize = {};
	$scope.TempPageIndex = {};
	$scope.PageSize = baseService.setPageSize(20);;
	$scope.totalPage = 1; //init;
	$scope.totalRecords = 0;
	$scope.PageIndex = 1;
	$scope.SortColumn = baseService.setSortColumn('id');
	$scope.SortOrder = baseService.setSortOrder('asc');

	$scope.salaryTab = "tab";
	$scope.isView = false;

	$scope.sort = function (e) {
		baseService.sort(e); $scope.SortColumn = baseService.getSortColumn();
		$scope.SortOrder = baseService.getSortOrder();
		$scope.reload();
	}

	$scope.getFirstPage = function () { $scope.PageIndex = baseService.getFirstPage(); $scope.reload(); }
	$scope.getBackPage = function () { $scope.PageIndex = baseService.getBackPage(); $scope.reload(); }
	$scope.getNextPage = function () { $scope.PageIndex = baseService.getNextPage(); $scope.reload(); }
	$scope.getLastPage = function () { $scope.PageIndex = baseService.getLastPage(); $scope.reload(); }
	$scope.searchByPage = function () { $scope.PageIndex = baseService.setPageIndex($scope.TempPageIndex.selected.PageIndex); $scope.reload(); }
	$scope.setPageSize = function (data) { $scope.PageSize = baseService.setPageSize($scope.TempPageSize.selected.Value); }
	$scope.loadByPageSize = function () { $scope.PageIndex = baseService.setPageIndex(1); $scope.setPageSize(); $scope.reload(); }
	//page system


	$scope.ShowDevice = function () {
		$(".DisplayDevice").show();
		$(".SearchDevice").hide();
		$(".addDevice").hide();
		$("#saveChange").modal('hide');
		$scope.reload();
	}
	
	$scope.ShowSearch = function () {
		$(".DisplayDevice").hide();
		$(".SearchDevice").show();
		$(".addDevice").hide();
	}

	$scope.LoadSearch = function () {
		$scope.ShowDevice();

	}

	$scope.AddNewDevice = function () {
		$scope.resetModel();
		$(".require").hide();
		$(".DisplayDevice").hide();
		$(".SearchDevice").hide();
		$(".addDevice").show();
		$scope.salaryTab = "";
	}

	$scope.onEditTagClick = function (item) {
		$scope.AddNewDevice();
		$scope.loadEditData(item);

	}

	$scope.loadEditData = function (item) {

		$scope.CreateModel = angular.copy(item);
		$scope.ListCustomercategory.forEach(function (entry, index) {
			console.log($scope.CreateModel.CATEGORY , entry.ID)
			if ($scope.CreateModel.CATEGORY_ID === entry.ID)
				$scope.TempCustomercategoryIndex.selected = entry;
			});
	}
	
	$scope.resetModel = function () {

		$scope.CreateModel = {
			ID: 0,  NAME: "", TEL: "",
			EMAIL: "", CATEGORY_ID: "",  NOTE: ""
		};
		
	}
	

	$scope.resetSearch = function () {
		$scope.modelSearch = {
			"NAME": "",
			
		};
		$scope.LoadSearch();
	}

	////////////////////////////////////////////////////////////////////////////////////////
	// Event
	$scope.onInit = function () {

		$scope.resetModel();
		$scope.resetSearch();
		
		$scope.listPageSize.forEach(function (entry, index) {
			if (0 === index)
				$scope.TempPageSize.selected = entry;
		});

		$scope.reload();
	}

	$scope.reload = function () {
		customercategoryApiService.getComboList(null, function (result) {
			if (result.status === true) {
				$scope.ListCustomercategory = result.message;
			} else {
				baseService.showMessage(result.message);
			}
		});
		customerApiService.getList($scope.modelSearch, function (results) {
			var result = results.data;
			if (result.status === true) {

				$scope.totalPage = result.toTalPage;
				$scope.listPageIndex = baseService.getListPage(result.toTalPage);
				$scope.listPageIndex.forEach(function (entry, index) {
					if ($scope.PageIndex === entry.Value)
						$scope.TempPageIndex.selected = entry;
				});

				$scope.totalRecords = result.totalRecords;
				$scope.modelDeviceList = result.message;
				console.log($scope.modelDeviceList);
			} else {

			}
		});
		$('#collapseOne').collapse('show')
		$('#collapseTwo').collapse('hide')
		$('.nav-tabs a[href="#tab1default"]').tab('show');
	}

	$scope.onDeleteTagClick = function (item) {
		customerApiService.delete({ ID: item.ID }, function (result) {
			if (result.status === true) {
				$scope.reload();
			} else {
				baseService.showMessage(result.message);
			}
		});

	}

	$scope.validatecheck = function () {
		var bResult = true;
		$(".require").hide();

		if ($scope.CreateModel.NAME == "") {
			$(".CreateModel_NAME").show();
			bResult = false;
		}

		if ($scope.CreateModel.TEL == "") {
			$(".CreateModel_TEL").show();
			bResult = false;
		}
		if ($scope.CreateModel.EMAIL == "") {
			$(".CreateModel_EMAIL").show();
			bResult = false;
		}
		if ($scope.TempCustomercategoryIndex.selected == undefined) {
			$(".CreateModel_CATEGORY_ID").show();
			bResult = false;
		}
		return bResult;
	}
	
	$scope.onSaveTagClick = function () {

		var bValid = $scope.validatecheck();
		if (true == bValid) {
			$scope.CreateModel.CATEGORY_ID = $scope.TempCustomercategoryIndex.selected.ID;
			console.log($scope.CreateModel);
			customerApiService.save($scope.CreateModel, function (result) {
				if (result.status == true) {
					$scope.ShowDevice();
				} else {
					baseService.showMessage(result.message);
				}
			});

		}
	}	
	
}]); 