myApp.controller('settingController', ['$scope', '$filter', 'Upload', 'baseService', 'settingApiService','memberApiService', function ($scope, $filter, Upload, baseService, settingApiService,memberApiService) {

	$scope.modelDeviceList = [];
	$scope.CreateModel = {};
	$scope.modelSearch = {};
	$scope.ViewImgPreview = false;
	$scope.salaryModel = {};
	$scope.salaryModelList = [];
	$scope.oldSalaryModelList = [];
	$scope.TempNameTitleTH = {};
	$scope.TempNameTitleEN = {};
	$scope.listNameTitle = [{ th: "นาย", en: "Mr." }, { th: "นาง", en: "Mrs." }, { th: "นางสาว", en: "Miss" }];

	
	$scope.TempMemberIndex = {};
	$scope.TempcategoryGoodsIndex = {};
	//page system 
	$scope.listPageSize = baseService.getListPageSize();
	$scope.TempPageSize = {};
	$scope.TempPageIndex = {};
	$scope.PageSize = baseService.setPageSize(20);;
	$scope.totalPage = 1; //init;
	$scope.totalRecords = 0;
	$scope.PageIndex = 1;
	$scope.SortColumn = baseService.setSortColumn('id');
	$scope.SortOrder = baseService.setSortOrder('asc');

	$scope.salaryTab = "tab";
	$scope.isView = false;

	$scope.sort = function (e) {
		baseService.sort(e); $scope.SortColumn = baseService.getSortColumn();
		$scope.SortOrder = baseService.getSortOrder();
		$scope.reload();
	}

	$scope.getFirstPage = function () { $scope.PageIndex = baseService.getFirstPage(); $scope.reload(); }
	$scope.getBackPage = function () { $scope.PageIndex = baseService.getBackPage(); $scope.reload(); }
	$scope.getNextPage = function () { $scope.PageIndex = baseService.getNextPage(); $scope.reload(); }
	$scope.getLastPage = function () { $scope.PageIndex = baseService.getLastPage(); $scope.reload(); }
	$scope.searchByPage = function () { $scope.PageIndex = baseService.setPageIndex($scope.TempPageIndex.selected.PageIndex); $scope.reload(); }
	$scope.setPageSize = function (data) { $scope.PageSize = baseService.setPageSize($scope.TempPageSize.selected.Value); }
	$scope.loadByPageSize = function () { $scope.PageIndex = baseService.setPageIndex(1); $scope.setPageSize(); $scope.reload(); }
	//page system


	$scope.ShowDevice = function () {
		$(".DisplayDevice").show();
		$(".SearchDevice").hide();
		$(".addDevice").hide();
		$("#saveChange").modal('hide');
		$scope.reload();
	}
	$scope.onChange = function () {
		// console.log(JSON.stringify($scope.CreateModel),JSON.stringify($scope.oldCreateModel))
		var resultObj = JSON.stringify($scope.CreateModel) === JSON.stringify($scope.oldCreateModel)
		var resultArray = JSON.stringify(JSON.parse(angular.toJson($scope.salaryModelList))) === JSON.stringify($scope.oldSalaryModelList)

		console.log($scope.oldSalaryModelList)
		console.log($scope.salaryModelList)
		console.log(resultArray)
		if (resultObj == true && resultArray == true) {
			$scope.ShowDevice();
		} else {
			$("#saveChange").modal('show');
		}

	}
	$scope.ShowSearch = function () {
		$(".DisplayDevice").hide();
		$(".SearchDevice").show();
		$(".addDevice").hide();
	}

	$scope.LoadSearch = function () {
		$scope.ShowDevice();

	}

	$scope.AddNewDevice = function () {
		$scope.resetModel();
		$(".require").hide();
		$(".DisplayDevice").hide();
		$(".SearchDevice").hide();
		$(".addDevice").show();
		$scope.salaryTab = "";
	}

	$scope.onEditTagClick = function (item) {
		$scope.AddNewDevice();
		$scope.loadEditData(item);

	}

	$scope.loadEditData = function (item) {

		$scope.CreateModel = angular.copy(item);
	}
	
	$scope.resetModel = function () {

		$scope.CreateModel = {
			ID: 0,  NAME: "", EMAIL: "",
			SETTING_BOOLEAN_POINT: "", SETTING_BAHTTOPOINT: "",  SETTING_REFUND_POINT: ""
		};
		
	}
	


	////////////////////////////////////////////////////////////////////////////////////////
	// Event
	$scope.onInit = function () {

		$scope.resetModel();
		// $scope.resetSearch();
		// if ($scope.listNameTitle.length > 0) {
		// 	$scope.TempNameTitleEN.selected = $scope.listNameTitle[0];
		// 	$scope.TempNameTitleTH.selected = $scope.listNameTitle[0];
		// }


		// $scope.listPageSize.forEach(function (entry, index) {
		// 	if (0 === index)
		// 		$scope.TempPageSize.selected = entry;
		// });

		$scope.reload();
	}

	$scope.reload = function () {
		settingApiService.get(null, function (result) {
			if (result.status === true) {
				$scope.CreateModel = result.message;
			} else {
				alert(result.message);
			}
		});
		$(".require").hide();
		$('#collapseOne').collapse('show')
		$('#collapseTwo').collapse('hide')
		$('.nav-tabs a[href="#tab1default"]').tab('show');
	}

	
	$scope.validatecheck = function () {
		var bResult = true;
		$(".require").hide();

		if ($scope.CreateModel.EMAIL == "") {
			$(".CreateModel_EMAIL").show();
			bResult = false;
		}

		if ($scope.CreateModel.NAME == "") {
			$(".CreateModel_NAME").show();
			bResult = false;
		}
		if ($scope.CreateModel.SETTING_BAHTTOPOINT == 0 && $scope.CreateModel.SETTING_BOOLEAN_POINT == 1) {
			$(".CreateModel_SETTING_BAHTTOPOINT").show();
			bResult = false;
		}
		if ($scope.CreateModel.SETTING_BOOLEAN_POINT == "") {
			$(".CreateModel_SETTING_BOOLEAN_POINT").show();
			bResult = false;
		}
		if ($scope.CreateModel.SETTING_REFUND_POINT == "") {
			$(".CreateModel_SETTING_REFUND_POINT").show();
			bResult = false;
		}
		return bResult;
	}
	
	$scope.onSaveTagClick = function () {

		var bValid = $scope.validatecheck();
		if (true == bValid) {
			console.log($scope.CreateModel);
			settingApiService.save($scope.CreateModel, function (result) {
				if (result.status == true) {
					alert("บันทึกเสร็จสิ้น");
				} else {
					baseService.showMessage(result.message);
				}
			});
		}
	}	
	
}]); 