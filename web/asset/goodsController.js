myApp.controller('goodsController', ['$scope', '$filter', 'Upload', 'baseService', 'goodsApiService','goodscategoryApiService', function ($scope, $filter, Upload, baseService, goodsApiService,goodscategoryApiService) {

	$scope.TempGoodscategoryIndex = {};
	$scope.modelDeviceList = [];
	$scope.CreateModel = {};
	$scope.modelSearch = {};
	$scope.ViewImgPreview = false;


	//page system 
	$scope.listPageSize = baseService.getListPageSize();
	$scope.TempPageSize = {};
	$scope.TempPageIndex = {};
	$scope.PageSize = baseService.setPageSize(20);;
	$scope.totalPage = 1; //init;
	$scope.totalRecords = 0;
	$scope.PageIndex = 1;
	$scope.SortColumn = baseService.setSortColumn('id');
	$scope.SortOrder = baseService.setSortOrder('asc');

	$scope.salaryTab = "tab";
	$scope.isView = false;

	$scope.sort = function (e) {
		baseService.sort(e); $scope.SortColumn = baseService.getSortColumn();
		$scope.SortOrder = baseService.getSortOrder();
		$scope.reload();
	}

	$scope.getFirstPage = function () { $scope.PageIndex = baseService.getFirstPage(); $scope.reload(); }
	$scope.getBackPage = function () { $scope.PageIndex = baseService.getBackPage(); $scope.reload(); }
	$scope.getNextPage = function () { $scope.PageIndex = baseService.getNextPage(); $scope.reload(); }
	$scope.getLastPage = function () { $scope.PageIndex = baseService.getLastPage(); $scope.reload(); }
	$scope.searchByPage = function () { $scope.PageIndex = baseService.setPageIndex($scope.TempPageIndex.selected.PageIndex); $scope.reload(); }
	$scope.setPageSize = function (data) { $scope.PageSize = baseService.setPageSize($scope.TempPageSize.selected.Value); }
	$scope.loadByPageSize = function () { $scope.PageIndex = baseService.setPageIndex(1); $scope.setPageSize(); $scope.reload(); }
	//page system


	$scope.ShowDevice = function () {
		$(".DisplayDevice").show();
		$(".SearchDevice").hide();
		$(".addDevice").hide();
		$("#saveChange").modal('hide');
		$scope.reload();
	}
	
	$scope.ShowSearch = function () {
		$(".DisplayDevice").hide();
		$(".SearchDevice").show();
		$(".addDevice").hide();
	}

	$scope.LoadSearch = function () {
		$scope.ShowDevice();

	}

	$scope.AddNewDevice = function () {
		$scope.resetModel();
		$(".require").hide();
		$(".DisplayDevice").hide();
		$(".SearchDevice").hide();
		$(".addDevice").show();
	}

	$scope.onEditTagClick = function (item) {
		$scope.AddNewDevice();
		$scope.loadEditData(item);

	}

	$scope.loadEditData = function (item) {

		$scope.CreateModel = angular.copy(item);
		console.log($scope.CreateModel);
		if($scope.CreateModel.STATUS  == 1){$scope.CreateModel.STATUS = true}
		$scope.ListGoodscategory.forEach(function (entry, index) {
			if ($scope.CreateModel.CATEGORY_ID === entry.ID)
				$scope.TempGoodscategoryIndex.selected = entry;
			});
	}
	
	$scope.resetModel = function () {

		$scope.CreateModel = {
			ID: 0, DETAIL: "", SKU: "", TYPE: "",IMAGE:"",
			COST: "", PRICE: "",  IsActive:1 ,STATUS:true,BARCODE:""
		};
		
	}
	

	$scope.resetSearch = function () {
		$scope.modelSearch = {
			"NAME": "",
		};
		$scope.LoadSearch();
	}

	////////////////////////////////////////////////////////////////////////////////////////
	// Event
	$scope.onInit = function () {

		$scope.resetModel();
		$scope.resetSearch();

		$scope.listPageSize.forEach(function (entry, index) {
			if (0 === index)
				$scope.TempPageSize.selected = entry;
		});

		$scope.reload();
	}

	$scope.reload = function () {

		goodscategoryApiService.getComboList(null, function (result) {
			if (result.status === true) {
				$scope.ListGoodscategory = result.message;
			} else {
				baseService.showMessage(result.message);
			}
		});
		goodsApiService.getList($scope.modelSearch, function (results) {
			var result = results.data;
			if (result.status === true) {

				$scope.totalPage = result.toTalPage;
				$scope.listPageIndex = baseService.getListPage(result.toTalPage);
				$scope.listPageIndex.forEach(function (entry, index) {
					if ($scope.PageIndex === entry.Value)
						$scope.TempPageIndex.selected = entry;
				});

				$scope.totalRecords = result.totalRecords;
				$scope.modelDeviceList = result.message;
				$scope.modelDeviceList.forEach(function (entry, index) {
					entry.Difference = (entry.PRICE-entry.COST);
				});
				
			} else {

			}
		});
		$('#collapseOne').collapse('show')
		$('#collapseTwo').collapse('hide')
		$('.nav-tabs a[href="#tab1default"]').tab('show');
	}

	$scope.onDeleteTagClick = function (item) {
		goodsApiService.delete({ ID: item.ID }, function (result) {
			if (result.status === true) {
				$scope.reload();
			} else {
				baseService.showMessage(result.message);
			}
		});

	}

	$scope.validatecheck = function () {
		var bResult = true;
		$(".require").hide();

		if ($scope.CreateModel.NAME == "") {
			$(".CreateModel_NAME").show();
			bResult = false;
		}

		if ($scope.TempGoodscategoryIndex.selected == undefined) {
			$(".CreateModel_TYPE").show();
			bResult = false;
		}
		// if ($scope.CreateModel.SKU == "") {
		// 	$(".CreateModel_SKU").show();
		// 	bResult = false;
		// }
		if ($scope.CreateModel.COST == "") {
			$(".CreateModel_COST").show();
			bResult = false;
		}
		if ($scope.CreateModel.PRICE == "") {
			$(".CreateModel_PRICE").show();
			bResult = false;
		}
		return bResult;
	}
	
	$scope.onSaveTagClick = function (i) {

		var bValid = $scope.validatecheck();
		if($scope.CreateModel.STATUS == true || $scope.CreateModel.STATUS == 1){
			$scope.CreateModel.STATUS = 1;
		}else{
			$scope.CreateModel.STATUS = 0;
		}
		if (true == bValid) {
			$scope.CreateModel.CATEGORY_ID = $scope.TempGoodscategoryIndex.selected.ID;
			goodsApiService.save($scope.CreateModel, function (result) {
				if (result.status == true) {
					$scope.ShowDevice();
				} else {
					baseService.showMessage(result.message);
				}
			});

		}
	}	
	$scope.SelectFile = function (e) {
		$scope.ViewImgPreview = true;
		var reader = new FileReader();
		reader.onload = function (e) {
			$scope.PreviewImage = e.target.result;
			$scope.$apply();
		};

		reader.readAsDataURL(e.target.files[0]);
	};
	$scope.upload = function () {
		console.log($scope.filegeneralinfo)
        if(undefined != $scope.filegeneralinfo){   
            var filegeneralinfo = $scope.filegeneralinfo.name;
            Upload.upload({
                url: 'Goods/upload_file',
                data: {
                    filegeneralinfo: $scope.filegeneralinfo
                }
            })
            .then(function (resp) {
				console.log(resp)
                if (resp.status == 200) {
					$scope.CreateModel.IMAGE = resp.data.message;
					$scope.ViewImgPreview = false;
                    $scope.onSaveTagClick();
                } else {
                    baseService.showMessage("Can not upload file");
                }
            });
        }else{
            $scope.onSaveTagClick();
        }
    }
}]); 