myApp.controller('reportreceiptController', ['$scope', '$filter', 'Upload', 'baseService', 'reportreceiptApiService','eventApiService', function ($scope, $filter, Upload, baseService, reportreceiptApiService,eventApiService) {

	$scope.ViewImgPreview = false;
	$scope.modelDeviceList = [];
	$scope.CreateModel = {};
	
	$scope.input_dateStart = new Date();
	$scope.input_dateEnd = new Date();
	var d = new Date();
        var year = d.getFullYear();
        var month = d.getMonth();
        var day = d.getDate();
	// $scope.input_dateStart = new Date(year, month - 1, day);
	var dateStart = new Date();
	var dateEnd = new Date();
	$scope.input_dateStart = new Date(year, month - 1, day);
	$scope.input_dateEnd = new Date(year, month, day);
	$scope.modelSearch = {
		"NAME":"",
		"DateStart":formatDate($scope.input_dateStart),
		"DateEnd":formatDate($scope.input_dateEnd)
	};
	console.log($scope.input_dateEnd ,$scope.input_dateStart)
    function formatDate(date){
		var d = new Date(date),
			month = '' + (d.getMonth() + 1),
			day = '' + d.getDate(),
			year = d.getFullYear();
	
		if (month.length < 2) 
			month = '0' + month;
		if (day.length < 2) 
			day = '0' + day;
	
		return [year, month, day].join('-');
	}
	$scope.salaryModel = {};
	$scope.salaryModelList = [];
	$scope.oldSalaryModelList = [];
	$scope.TempNameTitleTH = {};
	$scope.TempNameTitleEN = {};
	$scope.listNameTitle = [{ th: "นาย", en: "Mr." }, { th: "นาง", en: "Mrs." }, { th: "นางสาว", en: "Miss" }];

	$scope.TempEventIndex = {selected:""};
	$scope.listEmpType = [];
	$scope.TempEmpTypeIndex = {}

	//page system 
	$scope.listPageSize = baseService.getListPageSize();
	$scope.TempPageSize = {};
	$scope.TempPageIndex = {};
	$scope.PageSize = baseService.setPageSize(20);;
	$scope.totalPage = 1; //init;
	$scope.totalRecords = 0;
	$scope.PageIndex = 1;
	$scope.SortColumn = baseService.setSortColumn('ts_transection.ORDER_NAME');
	$scope.SortOrder = baseService.setSortOrder('DESC');

	$scope.salaryTab = "tab";
	$scope.isView = false;

	$scope.sort = function (e) {
		baseService.sort(e); $scope.SortColumn = baseService.getSortColumn();
		$scope.SortOrder = baseService.getSortOrder();
		$scope.reload();
	}

	$scope.getFirstPage = function () { $scope.PageIndex = baseService.getFirstPage(); $scope.reload(); }
	$scope.getBackPage = function () { $scope.PageIndex = baseService.getBackPage(); $scope.reload(); }
	$scope.getNextPage = function () { $scope.PageIndex = baseService.getNextPage(); $scope.reload(); }
	$scope.getLastPage = function () { $scope.PageIndex = baseService.getLastPage(); $scope.reload(); }
	$scope.searchByPage = function () { $scope.PageIndex = baseService.setPageIndex($scope.TempPageIndex.selected.PageIndex); $scope.reload(); }
	$scope.setPageSize = function (data) { $scope.PageSize = baseService.setPageSize($scope.TempPageSize.selected.Value); }
	$scope.loadByPageSize = function () { $scope.PageIndex = baseService.setPageIndex(1); $scope.setPageSize(); $scope.reload(); }
	//page system


	$scope.ShowDevice = function () {
		$(".DisplayDevice").show();
		$(".SearchDevice").hide();
		$(".addDevice").hide();
		$("#saveChange").modal('hide');
		$scope.reload();
	}
	$scope.onChange = function () {
		// console.log(JSON.stringify($scope.CreateModel),JSON.stringify($scope.oldCreateModel))
		var resultObj = JSON.stringify($scope.CreateModel) === JSON.stringify($scope.oldCreateModel)
		var resultArray = JSON.stringify(JSON.parse(angular.toJson($scope.salaryModelList))) === JSON.stringify($scope.oldSalaryModelList)

		console.log($scope.oldSalaryModelList)
		console.log($scope.salaryModelList)
		console.log(resultArray)
		if (resultObj == true && resultArray == true) {
			$scope.ShowDevice();
		} else {
			$("#saveChange").modal('show');
		}

	}
	$scope.ShowSearch = function () {
		$(".DisplayDevice").hide();
		$(".SearchDevice").show();
		$(".addDevice").hide();
	}

	$scope.LoadSearch = function () {
		$scope.ShowDevice();

	}

	$scope.AddNewDevice = function () {
		$scope.resetModel();
		$(".require").hide();
		$(".DisplayDevice").hide();
		$(".SearchDevice").hide();
		$(".addDevice").show();
		$scope.salaryTab = "";
	}

	$scope.onEditTagClick = function (item) {
		$scope.AddNewDevice();
		$scope.loadEditData(item);

	}

	$scope.loadEditData = function (item) {

		$scope.CreateModel = angular.copy(item);
	}
	
	$scope.resetModel = function () {

		$scope.CreateModel = {
			ID: 0,
		};
		
	}
	


	////////////////////////////////////////////////////////////////////////////////////////
	// Event
	$scope.onInit = function () {

		$scope.resetModel();
		$scope.reload();
	}
	$scope.resetSearch = function () {
		$scope.modelSearch = {
			"NAME":"",
			"DateStart":$scope.input_dateStart,
			"DateEnd":$scope.input_dateEnd
		};
		$scope.LoadSearch();
	}
	$scope.viewreportreceipt = function(){
		if($scope.TempEventIndex.selected != ""){
			$scope.CreateModel.ID = $scope.TempEventIndex.selected.ID;
			$scope.reload();
		}else{
			baseService.showMessage("กรุณาเลือก Event");
		}
	}
	$scope.dynamicColors = function() {
		var r = Math.floor(Math.random() * 255);
		var g = Math.floor(Math.random() * 255);
		var b = Math.floor(Math.random() * 255);
		return "rgb(" + r + "," + g + "," + b + ")";
	 };
	 
	$scope.reload = function () {

		console.log($scope.modelSearch);
		reportreceiptApiService.getList($scope.modelSearch, function (results) {
			console.log(results);
			var result = results.data;
			if (result.status === true) {

				$scope.totalPage = result.toTalPage;
				$scope.listPageIndex = baseService.getListPage(result.toTalPage);
				$scope.listPageIndex.forEach(function (entry, index) {
					if ($scope.PageIndex === entry.Value)
						$scope.TempPageIndex.selected = entry;
				});

				$scope.totalRecords = result.totalRecords;
				$scope.modelDeviceList = result.message;
				console.log($scope.modelDeviceList);
				$scope.Total = 0;
				$scope.refund = 0;
				$scope.modelDeviceList.forEach(function (entry, index) {
						$scope.Total++;
						if(entry.IsActive == 0){
							$scope.refund ++;
						}
				});
			} else {

			}
		});
		
	}
	$scope.onLoadDetail = function(item){
		$scope.CreateModel = angular.copy(item);
		reportreceiptApiService.getDetail({TRANSECTION_ID:item.ID}, function (results) {
			console.log(results);
			if(results.status == true){
				$scope.DataDetailModel = results.message;
			}else{
				alert(results.message)
			}
		});
	
	}
	

}]); 