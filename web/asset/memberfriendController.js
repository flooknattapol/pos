myApp.controller('memberfriendController', ['$scope', '$filter', 'Upload', 'baseService', 'memberfriendApiService','memberApiService', function ($scope, $filter, Upload, baseService, memberfriendApiService,memberApiService) {

	$scope.modelDeviceList = [];
	$scope.CreateModel = {};
	$scope.modelSearch = {};

	$scope.salaryModel = {};
	$scope.salaryModelList = [];
	$scope.oldSalaryModelList = [];
	$scope.TempNameTitleTH = {};
	$scope.TempNameTitleEN = {};
	$scope.listNameTitle = [{ th: "นาย", en: "Mr." }, { th: "นาง", en: "Mrs." }, { th: "นางสาว", en: "Miss" }];

	$scope.listEmpType = [];
	$scope.TempEmpTypeIndex = {}
	$scope.TempMemberIndex = {}
	$scope.FrindList = [];
	//page system 
	$scope.listPageSize = baseService.getListPageSize();
	$scope.TempPageSize = {};
	$scope.TempPageIndex = {};
	$scope.PageSize = baseService.setPageSize(20);;
	$scope.totalPage = 1; //init;
	$scope.totalRecords = 0;
	$scope.PageIndex = 1;
	$scope.SortColumn = baseService.setSortColumn('id');
	$scope.SortOrder = baseService.setSortOrder('asc');

	$scope.salaryTab = "tab";
	$scope.isView = false;

	$scope.sort = function (e) {
		baseService.sort(e); $scope.SortColumn = baseService.getSortColumn();
		$scope.SortOrder = baseService.getSortOrder();
		$scope.reload();
	}

	$scope.getFirstPage = function () { $scope.PageIndex = baseService.getFirstPage(); $scope.reload(); }
	$scope.getBackPage = function () { $scope.PageIndex = baseService.getBackPage(); $scope.reload(); }
	$scope.getNextPage = function () { $scope.PageIndex = baseService.getNextPage(); $scope.reload(); }
	$scope.getLastPage = function () { $scope.PageIndex = baseService.getLastPage(); $scope.reload(); }
	$scope.searchByPage = function () { $scope.PageIndex = baseService.setPageIndex($scope.TempPageIndex.selected.PageIndex); $scope.reload(); }
	$scope.setPageSize = function (data) { $scope.PageSize = baseService.setPageSize($scope.TempPageSize.selected.Value); }
	$scope.loadByPageSize = function () { $scope.PageIndex = baseService.setPageIndex(1); $scope.setPageSize(); $scope.reload(); }
	//page system


	$scope.ShowDevice = function () {
		$(".DisplayDevice").show();
		$(".SearchDevice").hide();
		$(".addDevice").hide();
		$("#saveChange").modal('hide');
		$scope.reload();
	}
	$scope.onChange = function () {
		// console.log(JSON.stringify($scope.CreateModel),JSON.stringify($scope.oldCreateModel))
		var resultObj = JSON.stringify($scope.CreateModel) === JSON.stringify($scope.oldCreateModel)
		var resultArray = JSON.stringify(JSON.parse(angular.toJson($scope.salaryModelList))) === JSON.stringify($scope.oldSalaryModelList)

		console.log($scope.oldSalaryModelList)
		console.log($scope.salaryModelList)
		console.log(resultArray)
		if (resultObj == true && resultArray == true) {
			$scope.ShowDevice();
		} else {
			$("#saveChange").modal('show');
		}

	}
	$scope.ShowSearch = function () {
		$(".DisplayDevice").hide();
		$(".SearchDevice").show();
		$(".addDevice").hide();
	}

	$scope.LoadSearch = function () {
		$scope.ShowDevice();

	}

	$scope.AddNewDevice = function () {
		$scope.resetModel();
		$(".require").hide();
		$(".DisplayDevice").hide();
		$(".SearchDevice").hide();
		$(".addDevice").show();
		$scope.salaryTab = "";
	}

	$scope.onEditTagClick = function (item) {
		$scope.AddNewDevice();
		$scope.loadEditData(item);

	}

	$scope.loadEditData = function (item) {

		$scope.CreateModel = angular.copy(item);
	}
	
	$scope.resetModel = function () {

		$scope.CreateModel = {
			ID: 0, MEMBER_ID: "", FRIEND_ID:""
		};
		
	}
	

	$scope.resetSearch = function () {
		$scope.modelSearch = {
			"code_id": "",
			"code": "",
			"name": "",
			"lastname": "",
			"flag":false,
		};
		$scope.LoadSearch();
	}

	////////////////////////////////////////////////////////////////////////////////////////
	// Event
	$scope.onInit = function () {

		$scope.resetModel();
		$scope.resetSearch();
		if ($scope.listNameTitle.length > 0) {
			$scope.TempNameTitleEN.selected = $scope.listNameTitle[0];
			$scope.TempNameTitleTH.selected = $scope.listNameTitle[0];
		}


		$scope.listPageSize.forEach(function (entry, index) {
			if (0 === index)
				$scope.TempPageSize.selected = entry;
		});

		//$scope.reload();
	}

	$scope.reload = function () {
		memberApiService.getComboBox(null, function (result) {
			if (result.data.status === true) {
				$scope.ListMemberModel = result.data.message;
			} else {
				baseService.showMessage(result.message);
			}
		});
		memberfriendApiService.listMemberFriend($scope.modelSearch, function (results) {
			console.log(results);
			var result = results.data;
			if (result.status === true) {

				$scope.totalPage = result.toTalPage;
				$scope.listPageIndex = baseService.getListPage(result.toTalPage);
				$scope.listPageIndex.forEach(function (entry, index) {
					if ($scope.PageIndex === entry.Value)
						$scope.TempPageIndex.selected = entry;
				});

				$scope.totalRecords = result.totalRecords;
				$scope.modelDeviceList = result.message;
				console.log($scope.modelDeviceList);
			} else {

			}
		})
		$('#collapseOne').collapse('show')
		$('#collapseTwo').collapse('hide')
		$('.nav-tabs a[href="#tab1default"]').tab('show');
	}

	$scope.onDeleteTagClick = function (item) {
		memberfriendApiService.deleteMemberFriend({ ID: item.ID }, function (result) {
			if (result.status === true) {
				$scope.reload();
			} else {
				baseService.showMessage(result.message);
			}
		});

	}

	$scope.validatecheck = function () {
		var bResult = true;
		$(".require").hide();



		if ($scope.CreateModel.NAME == "") {
			$(".CreateModel_NAME").show();
			bResult = false;
		}

		if ($scope.CreateModel.DESCRIPTION == "") {
			$(".CreateModel_DESCRIPTION").show();
			bResult = false;
		}
		if ($scope.CreateModel.LOCATION == "") {
			$(".CreateModel_LOCATION").show();
			bResult = false;
		}
		return bResult;
	}
	
	$scope.onSaveTagClick = function (i) {

		var bValid = $scope.validatecheck();
		if (true == bValid) {
			memberfriendApiService.saveMemberFriend($scope.CreateModel, function (result) {
				if (result.status == true) {
					$scope.ShowDevice();
				} else {
					baseService.showMessage(result.message);
				}
			});

		}
	}	
	$scope.viewFriend = function(item){
		$(".require").hide();
		$scope.CreateModel.MEMBER_ID = item.MEMBER_ID;
		$scope.CreateModel.ID = item.ID;
		memberfriendApiService.getFriendMemberList({FRIEND_ID:item.FRIEND_ID}, function (result) {
			if (result.status == true) {
				$scope.FrindList = result.message;
				console.log($scope.FrindList);
			} else {
				baseService.showMessage(result.message);
			}
		});
	}

	$scope.LoadImgAdd = function(){
		$scope.imgaddFrd = $scope.TempMemberIndex.selected.IMAGE;
	}
	$scope.AddFriend = function(){
		var bValiFrd = true;
		if($scope.TempMemberIndex.selected == undefined){
			$(".Input_friend").show();
		}else{
			$scope.FrindList.forEach(function (entry, index) {
				if ($scope.TempMemberIndex.selected.ID === entry.ID){
					$(".Input_friendInlist").show();
					bValiFrd = false;
				}
			});
		}
		if(bValiFrd == true){
			$scope.FrindList.push($scope.TempMemberIndex.selected);
			$scope.CreateModel.FRIEND_ID = $scope.FrindList;
			console.log($scope.CreateModel);
			memberfriendApiService.saveMemberFriend($scope.CreateModel, function (result) {
				if (result.status == true) {
					$scope.ShowDevice();
					$(".require").hide();
				} else {
					baseService.showMessage(result.message);
				}
			});
		}
	
	}
}]); 