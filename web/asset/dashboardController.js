myApp.controller('dashboardController', ['$scope', '$filter', 'Upload', 'baseService', 'dashboardApiService','reportSaleApiService', function ($scope, $filter, Upload, baseService, dashboardApiService,reportSaleApiService) {

	$scope.ViewImgPreview = false;
	$scope.modelDeviceList = [];
	$scope.CreateModel = {};
	$scope.modelSearch = {};
	$scope.Header = {};
	$scope.ReportList = [];
	$scope.salaryModel = {};
	$scope.salaryModelList = [];
	$scope.oldSalaryModelList = [];
	$scope.TempNameTitleTH = {};
	$scope.TempNameTitleEN = {};
	$scope.listNameTitle = [{ th: "นาย", en: "Mr." }, { th: "นาง", en: "Mrs." }, { th: "นางสาว", en: "Miss" }];
	var d = new Date();
        var year = d.getFullYear();
        var month = d.getMonth();
        var day = d.getDate();
	$scope.input_dateStart = new Date(year, month - 1, day);
	$scope.input_dateEnd = new Date(year, month, day);
	$scope.modelSearch = {
		"DateStart":formatDate($scope.input_dateStart),
		"DateEnd":formatDate($scope.input_dateEnd)
	};
	function formatDate(date){
		var d = new Date(date),
			month = '' + (d.getMonth() + 1),
			day = '' + d.getDate(),
			year = d.getFullYear();
	
		if (month.length < 2) 
			month = '0' + month;
		if (day.length < 2) 
			day = '0' + day;
	
		return [year, month, day].join('-');
	}
	$scope.TempEventIndex = {selected:""};
	$scope.listEmpType = [];
	$scope.TempEmpTypeIndex = {}

	//page system 
	$scope.listPageSize = baseService.getListPageSize();
	$scope.TempPageSize = {};
	$scope.TempPageIndex = {};
	$scope.PageSize = baseService.setPageSize(20);;
	$scope.totalPage = 1; //init;
	$scope.totalRecords = 0;
	$scope.PageIndex = 1;
	$scope.SortColumn = baseService.setSortColumn('id');
	$scope.SortOrder = baseService.setSortOrder('asc');

	$scope.salaryTab = "tab";
	$scope.isView = false;

	$scope.sort = function (e) {
		baseService.sort(e); $scope.SortColumn = baseService.getSortColumn();
		$scope.SortOrder = baseService.getSortOrder();
		$scope.reload();
	}

	$scope.getFirstPage = function () { $scope.PageIndex = baseService.getFirstPage(); $scope.reload(); }
	$scope.getBackPage = function () { $scope.PageIndex = baseService.getBackPage(); $scope.reload(); }
	$scope.getNextPage = function () { $scope.PageIndex = baseService.getNextPage(); $scope.reload(); }
	$scope.getLastPage = function () { $scope.PageIndex = baseService.getLastPage(); $scope.reload(); }
	$scope.searchByPage = function () { $scope.PageIndex = baseService.setPageIndex($scope.TempPageIndex.selected.PageIndex); $scope.reload(); }
	$scope.setPageSize = function (data) { $scope.PageSize = baseService.setPageSize($scope.TempPageSize.selected.Value); }
	$scope.loadByPageSize = function () { $scope.PageIndex = baseService.setPageIndex(1); $scope.setPageSize(); $scope.reload(); }
	//page system


	$scope.ShowDevice = function () {
		$(".DisplayDevice").show();
		$(".SearchDevice").hide();
		$(".addDevice").hide();
		$("#saveChange").modal('hide');
		$scope.reload();
	}
	$scope.onChange = function () {
		// console.log(JSON.stringify($scope.CreateModel),JSON.stringify($scope.oldCreateModel))
		var resultObj = JSON.stringify($scope.CreateModel) === JSON.stringify($scope.oldCreateModel)
		var resultArray = JSON.stringify(JSON.parse(angular.toJson($scope.salaryModelList))) === JSON.stringify($scope.oldSalaryModelList)

		console.log($scope.oldSalaryModelList)
		console.log($scope.salaryModelList)
		console.log(resultArray)
		if (resultObj == true && resultArray == true) {
			$scope.ShowDevice();
		} else {
			$("#saveChange").modal('show');
		}

	}
	$scope.ShowSearch = function () {
		$(".DisplayDevice").hide();
		$(".SearchDevice").show();
		$(".addDevice").hide();
	}

	$scope.LoadSearch = function () {
		$scope.ShowDevice();

	}

	$scope.AddNewDevice = function () {
		$scope.resetModel();
		$(".require").hide();
		$(".DisplayDevice").hide();
		$(".SearchDevice").hide();
		$(".addDevice").show();
		$scope.salaryTab = "";
	}

	$scope.onEditTagClick = function (item) {
		$scope.AddNewDevice();
		$scope.loadEditData(item);

	}

	$scope.loadEditData = function (item) {

		$scope.CreateModel = angular.copy(item);
	}
	
	$scope.resetModel = function () {

		$scope.CreateModel = {
			ID: 0,
		};
		
	}
	


	////////////////////////////////////////////////////////////////////////////////////////
	// Event
	$scope.onInit = function () {

		$scope.resetModel();
		$scope.reload();
	}

	$scope.viewDashboard = function(){
		if($scope.TempEventIndex.selected != ""){
			$scope.CreateModel.ID = $scope.TempEventIndex.selected.ID;
			$scope.reload();
		}else{
			baseService.showMessage("กรุณาเลือก Event");
		}
	}
	$scope.dynamicColors = function() {
		var r = Math.floor(Math.random() * 255);
		var g = Math.floor(Math.random() * 255);
		var b = Math.floor(Math.random() * 255);
		return "rgb(" + r + "," + g + "," + b + ")";
	 };
	 
	$scope.reload = function () {
		$scope.label = [];
		$scope.data = [];
		reportSaleApiService.getHeader($scope.modelSearch , function (results) {
			console.log(results);
			$scope.Header = results[0];
			console.log($scope.Header);
		});
		reportSaleApiService.getGraph($scope.modelSearch , function (result) {
			console.log(result);
			$scope.totalPage = result.data.toTalPage;
			$scope.listPageIndex = baseService.getListPage(result.data.toTalPage);
			$scope.listPageIndex.forEach(function (entry, index) {
				if ($scope.PageIndex === entry.Value)
					$scope.TempPageIndex.selected = entry;
			});
			$scope.totalRecords = result.data.totalRecords;
			$scope.ReportList = result.data.message;
			$scope.ReportList.forEach(function (entry, index) {
				$scope.label.push(entry.Day);
				$scope.data.push(entry.Total);
			});
			if (typeof window.myLine !== "undefined") {
				window.myLine.destroy();
			}
			var config = {
			type: 'line',
			yAxisID: 'y-axis-1',
			data: {
				labels: $scope.label,
				datasets: [
				{
					
					label: 'วันที่	',
					yAxisID: 'y-axis-2',
					fill: false,
					showLine: true,
					backgroundColor: window.chartColors.blue,
					borderColor: window.chartColors.blue,
					data: $scope.data,
				}]
			},
			options: {
				// elements: { 
				// 	point: { radius: 10 } ,
				// 	line : { tension : 1 }
				// 	},
				responsive: true,
				title: {
					display: true,
					text: 'ยอดขายรายวัน'
				},
				tooltips: {
					mode: 'index',
					intersect: false,
				},
				hover: {
					mode: 'nearest',
					intersect: true
				},
				legend: {
					position: 'bottom',
				},
				scales: {
					yAxes: [
					// 	{
					// 		type: 'linear', // only linear but allow scale type registration. This allows extensions to exist solely for log scale for instance
					// 		display: true,
					// 		position: 'left',
					// 		id: 'y-axis-1',
					// 	}, 
						{
							type: 'linear', // only linear but allow scale type registration. This allows extensions to exist solely for log scale for instance
							display: true,
							position: 'left',
							id: 'y-axis-2',
							gridLines: {
								drawOnChartArea: false
							}
						}],
				}
			}
		};
		var ctx = document.getElementById('canvas').getContext('2d');
		window.myLine = new Chart(ctx, config);
		});

	}


}]); 