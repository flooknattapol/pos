myApp.controller('employeeController', ['$scope', '$filter', 'Upload', 'baseService', 'employeeApiService','roleApiService','branchApiService' ,function ($scope, $filter, Upload, baseService, employeeApiService,roleApiService,branchApiService) {
	$scope.TempBranchIndex = {};
	$scope.TempRoleIndex = {};
	$scope.modelDeviceList = [];
	$scope.CreateModel = {};
	$scope.modelSearch = {};
	$scope.ViewImgPreview = false;
	$scope.salaryModel = {};
	$scope.salaryModelList = [];
	$scope.oldSalaryModelList = [];
	$scope.TempNameTitleTH = {};
	$scope.TempNameTitleEN = {};
	$scope.listNameTitle = [{ th: "นาย", en: "Mr." }, { th: "นาง", en: "Mrs." }, { th: "นางสาว", en: "Miss" }];
	$scope.listRole = [{ NAME: "เจ้าของร้าน"},{ NAME: "แคชเชียร์"}];

	
	$scope.TempMemberIndex = {};
	
	$scope.TempcategoryGoodsIndex = {};
	//page system 
	$scope.listPageSize = baseService.getListPageSize();
	$scope.TempPageSize = {};
	$scope.TempPageIndex = {};
	$scope.PageSize = baseService.setPageSize(20);;
	$scope.totalPage = 1; //init;
	$scope.totalRecords = 0;
	$scope.PageIndex = 1;
	$scope.SortColumn = baseService.setSortColumn('ID');
	$scope.SortOrder = baseService.setSortOrder('asc');

	$scope.salaryTab = "tab";
	$scope.isView = false;

	$scope.sort = function (e) {
		baseService.sort(e); $scope.SortColumn = baseService.getSortColumn();
		$scope.SortOrder = baseService.getSortOrder();
		$scope.reload();
	}

	$scope.getFirstPage = function () { $scope.PageIndex = baseService.getFirstPage(); $scope.reload(); }
	$scope.getBackPage = function () { $scope.PageIndex = baseService.getBackPage(); $scope.reload(); }
	$scope.getNextPage = function () { $scope.PageIndex = baseService.getNextPage(); $scope.reload(); }
	$scope.getLastPage = function () { $scope.PageIndex = baseService.getLastPage(); $scope.reload(); }
	$scope.searchByPage = function () { $scope.PageIndex = baseService.setPageIndex($scope.TempPageIndex.selected.PageIndex); $scope.reload(); }
	$scope.setPageSize = function (data) { $scope.PageSize = baseService.setPageSize($scope.TempPageSize.selected.Value); }
	$scope.loadByPageSize = function () { $scope.PageIndex = baseService.setPageIndex(1); $scope.setPageSize(); $scope.reload(); }
	//page system


	$scope.ShowDevice = function () {
		$(".DisplayDevice").show();
		$(".SearchDevice").hide();
		$(".addDevice").hide();
		$("#saveChange").modal('hide');
		$scope.reload();
	}
	$scope.onChange = function () {
		// console.log(JSON.stringify($scope.CreateModel),JSON.stringify($scope.oldCreateModel))
		var resultObj = JSON.stringify($scope.CreateModel) === JSON.stringify($scope.oldCreateModel)
		var resultArray = JSON.stringify(JSON.parse(angular.toJson($scope.salaryModelList))) === JSON.stringify($scope.oldSalaryModelList)

		console.log($scope.oldSalaryModelList)
		console.log($scope.salaryModelList)
		console.log(resultArray)
		if (resultObj == true && resultArray == true) {
			$scope.ShowDevice();
		} else {
			$("#saveChange").modal('show');
		}

	}
	$scope.ShowSearch = function () {
		$(".DisplayDevice").hide();
		$(".SearchDevice").show();
		$(".addDevice").hide();
	}

	$scope.LoadSearch = function () {
		$scope.ShowDevice();

	}

	$scope.AddNewDevice = function () {
		$scope.resetModel();
		$(".require").hide();
		$(".DisplayDevice").hide();
		$(".SearchDevice").hide();
		$(".addDevice").show();
		$scope.salaryTab = "";
	}

	$scope.onEditTagClick = function (item) {
		$scope.AddNewDevice();
		$scope.loadEditData(item);

	}

	$scope.loadEditData = function (item) {

		$scope.CreateModel = angular.copy(item);
		console.log(item);
		$scope.ListRole.forEach(function (entry, index) {
			if(entry.ID === item.ROLE_ID){
				$scope.TempRoleIndex.selected = entry;
			}
		});
		$scope.ListBranch.forEach(function (entry, index) {
			if(entry.ID === item.BRANCH_ID){
				$scope.TempBranchIndex.selected = entry;
			}
		});
	}
	
	$scope.resetModel = function () {

		$scope.CreateModel = {
			ID: 0, NAME: "", EMAIL: "",
			TEL: "",IMAGE: "",ROLE:"",ID_POSITION:"",BRANCH_ID:""
		};
		
	}
	

	$scope.resetSearch = function () {
		$scope.modelSearch = {
			"NAME": "",
		};
		$scope.LoadSearch();
	}

	////////////////////////////////////////////////////////////////////////////////////////
	// Event
	$scope.onInit = function () {

		$scope.resetModel();
		$scope.resetSearch();

		$scope.listPageSize.forEach(function (entry, index) {
			if (0 === index)
				$scope.TempPageSize.selected = entry;
		});

		$scope.reload();
	}

	$scope.reload = function () {

		roleApiService.getComboList({ID:0}, function (results) {
			if (results.data.status == true) {
				$scope.ListRole = results.data.message;
			} else {
				baseService.showMessage(result.message);
			}
		});
		branchApiService.getComboList({ID:0}, function (results) {
			if (results.status == true) {
				$scope.ListBranch = results.message;
			} else {
				baseService.showMessage(result.message);
			}
		});
		employeeApiService.getList($scope.modelSearch, function (results) {
			console.log(results);
			var result = results.data;
			if (result.status === true) {

				$scope.totalPage = result.toTalPage;
				$scope.listPageIndex = baseService.getListPage(result.toTalPage);
				$scope.listPageIndex.forEach(function (entry, index) {
					if ($scope.PageIndex === entry.Value)
						$scope.TempPageIndex.selected = entry;
				});

				$scope.totalRecords = result.totalRecords;
				$scope.modelDeviceList = result.message;
				console.log($scope.modelDeviceList);
			} else {

			}
		});
		$('#collapseOne').collapse('show')
		$('#collapseTwo').collapse('hide')
		$('.nav-tabs a[href="#tab1default"]').tab('show');
	}

	$scope.onDeleteTagClick = function (item) {
		employeeApiService.delete({ ID: item.ID_POSITION }, function (result) {
			if (result.status === true) {
				$scope.reload();
			} else {
				baseService.showMessage(result.message);
			}
		});

	}

	$scope.validatecheck = function () {
		var bResult = true;
		$(".require").hide();

		if ($scope.CreateModel.NAME == "") {
			$(".CreateModel_NAME").show();
			bResult = false;
		}
		if ($scope.CreateModel.TEL == "") {
			$(".CreateModel_TEL").show();
			bResult = false;
		}
		if ($scope.CreateModel.EMAIL == "") {
			$(".CreateModel_EMAIL").show();
			bResult = false;
		}
		if ($scope.TempRoleIndex.selected == undefined) {
			$(".CreateModel_ROLE_ID").show();
			bResult = false;
		}
		if ($scope.TempBranchIndex.selected == undefined) {
			$(".CreateModel_BRANCH").show();
			bResult = false;
		}
		return bResult;
	}
	
	$scope.onSaveTagClick = function () {

		// console.log($scope.TempRoleIndex.selected);
		if($scope.TempRoleIndex.selected != undefined){
			$scope.CreateModel.ROLE_ID = $scope.TempRoleIndex.selected.ID;
		}
		var bValid = $scope.validatecheck();
		if (true == bValid) {
			console.log($scope.CreateModel);
			if($scope.TempBranchIndex.selected != undefined){
				$scope.CreateModel.BRANCH_ID = $scope.TempBranchIndex.selected.ID;
			}
			employeeApiService.save($scope.CreateModel, function (result) {
				if (result.status == true) {
					$scope.ShowDevice();
				} else {
					baseService.showMessage(result.message);
				}
			});

		}
	}	
	$scope.SelectFile = function (e) {
		$scope.ViewImgPreview = true;
		var reader = new FileReader();
		reader.onload = function (e) {
			$scope.PreviewImage = e.target.result;
			$scope.$apply();
		};

		reader.readAsDataURL(e.target.files[0]);
	};
	$scope.upload = function () {
		console.log($scope.filegeneralinfo)
        if(undefined != $scope.filegeneralinfo){   
            var filegeneralinfo = $scope.filegeneralinfo.name;
            Upload.upload({
                url: 'Employee/upload_file',
                data: {
                    filegeneralinfo: $scope.filegeneralinfo
                }
            })
            .then(function (resp) {
				console.log(resp)
                if (resp.status == 200) {
					$scope.CreateModel.IMAGE = resp.data.message;
					$scope.ViewImgPreview = false;
                    $scope.onSaveTagClick();
                } else {
                    baseService.showMessage("Can not upload file");
                }
            });
        }else{
            $scope.onSaveTagClick();
        }
    }
}]); 