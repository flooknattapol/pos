myApp.controller('orderController', ['$scope', '$filter', 'Upload', 'baseService', 'orderApiService','vendorApiService','goodsApiService', function ($scope, $filter, Upload, baseService, orderApiService,vendorApiService,goodsApiService) {


	$scope.modelDeviceList = [];
	$scope.CreateModel = {};
	$scope.modelSearch = {};
	$scope.TempVendorIndex = {};


	//page system 
	$scope.listPageSize = baseService.getListPageSize();
	$scope.TempPageSize = {};
	$scope.TempPageIndex = {};
	$scope.PageSize = baseService.setPageSize(20);;
	$scope.totalPage = 1; //init;
	$scope.totalRecords = 0;
	$scope.PageIndex = 1;
	$scope.SortColumn = baseService.setSortColumn('ID');
	$scope.SortOrder = baseService.setSortOrder('asc');

	$scope.salaryTab = "tab";
	$scope.isView = false;

	$scope.sort = function (e) {
		baseService.sort(e); $scope.SortColumn = baseService.getSortColumn();
		$scope.SortOrder = baseService.getSortOrder();
		$scope.reload();
	}

	$scope.getFirstPage = function () { $scope.PageIndex = baseService.getFirstPage(); $scope.reload(); }
	$scope.getBackPage = function () { $scope.PageIndex = baseService.getBackPage(); $scope.reload(); }
	$scope.getNextPage = function () { $scope.PageIndex = baseService.getNextPage(); $scope.reload(); }
	$scope.getLastPage = function () { $scope.PageIndex = baseService.getLastPage(); $scope.reload(); }
	$scope.searchByPage = function () { $scope.PageIndex = baseService.setPageIndex($scope.TempPageIndex.selected.PageIndex); $scope.reload(); }
	$scope.setPageSize = function (data) { $scope.PageSize = baseService.setPageSize($scope.TempPageSize.selected.Value); }
	$scope.loadByPageSize = function () { $scope.PageIndex = baseService.setPageIndex(1); $scope.setPageSize(); $scope.reload(); }
	//page system


	$scope.ShowDevice = function () {
		$(".DisplayDevice").show();
		$(".SearchDevice").hide();
		$(".addDevice").hide();
		$("#saveChange").modal('hide');
		$scope.reload();
	}
	$scope.onChange = function () {
		// console.log(JSON.stringify($scope.CreateModel),JSON.stringify($scope.oldCreateModel))
		var resultObj = JSON.stringify($scope.CreateModel) === JSON.stringify($scope.oldCreateModel)
		var resultArray = JSON.stringify(JSON.parse(angular.toJson($scope.salaryModelList))) === JSON.stringify($scope.oldSalaryModelList)

		console.log($scope.oldSalaryModelList)
		console.log($scope.salaryModelList)
		console.log(resultArray)
		if (resultObj == true && resultArray == true) {
			$scope.ShowDevice();
		} else {
			$("#saveChange").modal('show');
		}

	}
	$scope.ShowSearch = function () {
		$(".DisplayDevice").hide();
		$(".SearchDevice").show();
		$(".addDevice").hide();
	}

	$scope.LoadSearch = function () {
		$scope.ShowDevice();

	}

	$scope.AddNewDevice = function () {
		$scope.resetModel();
		$(".require").hide();
		$(".DisplayDevice").hide();
		$(".SearchDevice").hide();
		$(".addDevice").show();
		$scope.addDataDetail();
	}

	$scope.onEditTagClick = function (item) {
		$scope.AddNewDevice();
		$scope.loadEditData(item);

	}

	$scope.loadEditData = function (item) {
		$scope.CreateModel = angular.copy(item);
		$scope.ListVendor.forEach(function (entry, index) {
			if (entry.ID === item.VENDOR_ID) {
				$scope.TempVendorIndex.selected = entry;
			}
		});
		orderApiService.getDetail({ ID: item.ID }, function (result) {
			console.log(result);
			if (true == result.status) {
				$scope.DataDetailModel = result.message
				$scope.DataDetailModel.forEach(function (entrylist, indexlist) {
					$scope.ListGoods.forEach(function (entry, index) {
						if (entry.ID === entrylist.GOODS_ID) {
							console.log(entry.ID === entrylist.GOODS_ID)
							$scope.DataDetailModel[indexlist].TempGoodsIndex = {};
							$scope.DataDetailModel[indexlist].TempGoodsIndex.selected = entry;
							$scope.getDataInComboList();
						}
					});
				})
			} else {
				baseService.showMessage(result.message);
			}
		});
	}
	
	$scope.resetModel = function () {

		$scope.CreateModel = { ID: 0, DATE_ORDER:"",ORDERNAME: "",ENDDATE_ORDER:"",NOTE:"",VENDOR_ID:0};
		var d = new Date();
		var year = d.getFullYear();
        var month = d.getMonth();
        var day = d.getDate();
		$scope.CreateModel.DATE_ORDER = new Date(year, month, day);
		$scope.CreateModel.ENDDATE_ORDER = new Date(year, month+1, day);
	}
	$scope.addDataDetail = function () {
		
		$scope.DataDetailModel = [];

		// for (var i = 1; i < 2; i++) {
		// 	$scope.DataDetailModel.push(
		// 		{ ID: 0,line_no:"", GOODS_ID:0,COST: "",TOTAL:"",ITEMAMOUNT:""}
		// 	);
		// }

	}
	$scope.AddNewLine = function () {
		var lineNo = $scope.DataDetailModel.length + 1;
		$scope.DataDetailModel.push(
			{ ID: 0,line_no:lineNo, GOODS_ID:0,COST: "",TOTAL:"",ITEMAMOUNT:""}
		);
	}
	$scope.updateList = function (line_no, listNewDetial) {
		console.log(line_no, listNewDetial)
		var lineCount = 1;
		listNewDetial.forEach(function (entry, index) {
			if (entry.line_no !== line_no) {
				entry.line_no = lineCount;
				$scope.DataDetailModel.push(entry);
				lineCount++;
			}
		});
		$scope.$apply();
	}

	$scope.onDeleteLine = function (item) {
		$scope.CreateModel.DeleteValueInList = [];
		$scope.CreateModel.DeleteValueInList.push({id:item.ID});
		var line_no = item.line_no;
		var listNewDetial = $scope.DataDetailModel; 

		$scope.DataDetailModel = [];
		$scope.updateList(line_no, listNewDetial);
	}
	$scope.resetSearch = function () {
		$scope.modelSearch = {
			"NAME": "",
		};
		$scope.LoadSearch();
	}

	////////////////////////////////////////////////////////////////////////////////////////
	// Event
	$scope.onInit = function () {

		$scope.resetModel();
		$scope.resetSearch();
		
		$scope.listPageSize.forEach(function (entry, index) {
			if (0 === index)
				$scope.TempPageSize.selected = entry;
		});

		$scope.reload();
	}
	$scope.getDataInComboList = function(){
		for (i = 0; i < $scope.DataDetailModel.length; i++) {
			if($scope.DataDetailModel[i].TempGoodsIndex !== undefined){
				$scope.DataDetailModel[i].INSTOCK = $scope.DataDetailModel[i].TempGoodsIndex.selected.STOCK;
			}
		}
	}
	$scope.getsumtotol = function(){
		for (i = 0; i < $scope.DataDetailModel.length; i++) {
			$scope.DataDetailModel[i].TOTAL = $scope.DataDetailModel[i].COST*$scope.DataDetailModel[i].ITEMAMOUNT;
		}
	}
	$scope.reload = function () {
		
		vendorApiService.getComboList(null, function (result) {
			if (result.status === true) {
				$scope.ListVendor = result.message;
			} else {
				baseService.showMessage(result.message);
			}
		});
		goodsApiService.getComboList(null, function (result) {
			if (result.status === true) {
				$scope.ListGoods = result.message;
			} else {
				baseService.showMessage(result.message);
			}
		});
		orderApiService.getList($scope.modelSearch, function (results) {
			console.log(results);
			var result = results.data;
			if (result.status === true) {

				$scope.totalPage = result.toTalPage;
				$scope.listPageIndex = baseService.getListPage(result.toTalPage);
				$scope.listPageIndex.forEach(function (entry, index) {
					if ($scope.PageIndex === entry.Value)
						$scope.TempPageIndex.selected = entry;
				});
				$scope.Session = result.Session;
				$scope.totalRecords = result.totalRecords;
				$scope.modelDeviceList = result.message;
				console.log($scope.modelDeviceList);
			} else {

			}
		})
		$('#collapseOne').collapse('show')
		$('#collapseTwo').collapse('hide')
		$('.nav-tabs a[href="#tab1default"]').tab('show');
	}

	$scope.onDeleteTagClick = function (item) {
		orderApiService.delete({ ID: item.ID }, function (result) {
			if (result.status === true) {
				$scope.reload();
			} else {
				baseService.showMessage(result.message);
			}
		});

	}
	$scope.onUnDeleteTagClick = function (item) {
		orderApiService.undelete({ ID: item.ID }, function (result) {
			if (result.status === true) {
				$scope.reload();
			} else {
				baseService.showMessage(result.message);
			}
		});

	}
	$scope.validatecheck = function () {
		var bResult = true;
		$(".require").hide();

		if ($scope.CreateModel.DATE_ORDER == "") {
			$(".CreateModel_DATE_ORDER").show();
			bResult = false;
		}
		if ($scope.CreateModel.ENDDATE_ORDER == "") {
			$(".CreateModel_ENDDATE_ORDER").show();
			bResult = false;
		}
		if ($scope.CreateModel.NAME == "") {
			$(".CreateModel_NAME").show();
			bResult = false;
		}
		return bResult;
	}
	
	$scope.onSaveTagClick = function () {
		if($scope.TempVendorIndex.selected != undefined){
			$scope.CreateModel.VENDOR_ID = $scope.TempVendorIndex.selected.ID;
		}
		var bValidDetail = true;
		var bValid = $scope.validatecheck();
		console.log(bValid)
		$scope.DataDetailModel.forEach(function (entry, index) {
			entry.GOODS_ID = entry.TempGoodsIndex.selected.ID;
			if(entry.GOODS_ID == "" || entry.COST== "" || entry.ITEMAMOUNT == ""){
				bValidDetail = false;
			}
		});
		if($scope.DataDetailModel.length == 0 ){
			baseService.showMessage("ต้องมีอย่างน้อย 1 รายการ");
		}else if (true == bValid && bValidDetail == true) {
			$scope.CreateModel.DATE_ORDER = formatDate($scope.CreateModel.DATE_ORDER)
			$scope.CreateModel.ENDDATE_ORDER = formatDate($scope.CreateModel.ENDDATE_ORDER)
			// var dateobj = Date.now();
			// var datestr = dateobj.toString(); 
			// $scope.CreateModel.ORDERNAME = datestr;
			$scope.CreateModel.detail = $scope.DataDetailModel;
			console.log($scope.CreateModel);
			orderApiService.save($scope.CreateModel, function (result) {
				if (result.status == true) {
					$scope.ShowDevice();
				} else {
					baseService.showMessage(result.message);
				}
			});
		}else{
			baseService.showMessage("กรุณาใส่ข้อมูลให้ครบถ้วน");
		}
	}
	function formatDate(date){
		var d = new Date(date),
			month = '' + (d.getMonth() + 1),
			day = '' + d.getDate(),
			year = d.getFullYear();
	
		if (month.length < 2) 
			month = '0' + month;
		if (day.length < 2) 
			day = '0' + day;
	
		return [year, month, day].join('-');
	}
}]); 