myApp.controller('eventController', ['$scope', '$filter', 'Upload', 'baseService', 'eventApiService', function ($scope, $filter, Upload, baseService, eventApiService) {

	$scope.ViewImgPreview = false;
	$scope.modelDeviceList = [];
	$scope.CreateModel = {};
	$scope.modelSearch = {};

	$scope.salaryModel = {};
	$scope.salaryModelList = [];
	$scope.oldSalaryModelList = [];
	$scope.TempNameTitleTH = {};
	$scope.TempNameTitleEN = {};
	$scope.listNameTitle = [{ th: "นาย", en: "Mr." }, { th: "นาง", en: "Mrs." }, { th: "นางสาว", en: "Miss" }];

	$scope.listEmpType = [];
	$scope.TempEmpTypeIndex = {}

	//page system 
	$scope.listPageSize = baseService.getListPageSize();
	$scope.TempPageSize = {};
	$scope.TempPageIndex = {};
	$scope.PageSize = baseService.setPageSize(20);;
	$scope.totalPage = 1; //init;
	$scope.totalRecords = 0;
	$scope.PageIndex = 1;
	$scope.SortColumn = baseService.setSortColumn('DATE_START');
	$scope.SortOrder = baseService.setSortOrder('DESC');

	$scope.salaryTab = "tab";
	$scope.isView = false;

	$scope.sort = function (e) {
		baseService.sort(e); $scope.SortColumn = baseService.getSortColumn();
		$scope.SortOrder = baseService.getSortOrder();
		$scope.reload();
	}

	$scope.getFirstPage = function () { $scope.PageIndex = baseService.getFirstPage(); $scope.reload(); }
	$scope.getBackPage = function () { $scope.PageIndex = baseService.getBackPage(); $scope.reload(); }
	$scope.getNextPage = function () { $scope.PageIndex = baseService.getNextPage(); $scope.reload(); }
	$scope.getLastPage = function () { $scope.PageIndex = baseService.getLastPage(); $scope.reload(); }
	$scope.searchByPage = function () { $scope.PageIndex = baseService.setPageIndex($scope.TempPageIndex.selected.PageIndex); $scope.reload(); }
	$scope.setPageSize = function (data) { $scope.PageSize = baseService.setPageSize($scope.TempPageSize.selected.Value); }
	$scope.loadByPageSize = function () { $scope.PageIndex = baseService.setPageIndex(1); $scope.setPageSize(); $scope.reload(); }
	//page system


	$scope.ShowDevice = function () {
		$(".DisplayDevice").show();
		$(".SearchDevice").hide();
		$(".addDevice").hide();
		$("#saveChange").modal('hide');
		$scope.reload();
	}
	$scope.onChange = function () {
		// console.log(JSON.stringify($scope.CreateModel),JSON.stringify($scope.oldCreateModel))
		var resultObj = JSON.stringify($scope.CreateModel) === JSON.stringify($scope.oldCreateModel)
		var resultArray = JSON.stringify(JSON.parse(angular.toJson($scope.salaryModelList))) === JSON.stringify($scope.oldSalaryModelList)

		console.log($scope.oldSalaryModelList)
		console.log($scope.salaryModelList)
		console.log(resultArray)
		if (resultObj == true && resultArray == true) {
			$scope.ShowDevice();
		} else {
			$("#saveChange").modal('show');
		}

	}
	$scope.ShowSearch = function () {
		$(".DisplayDevice").hide();
		$(".SearchDevice").show();
		$(".addDevice").hide();
	}

	$scope.LoadSearch = function () {
		$scope.ShowDevice();

	}

	$scope.AddNewDevice = function () {
		$scope.resetModel();
		$(".require").hide();
		$(".DisplayDevice").hide();
		$(".SearchDevice").hide();
		$(".addDevice").show();
		$scope.salaryTab = "";
	}

	$scope.onEditTagClick = function (item) {
		$scope.AddNewDevice();
		$scope.loadEditData(item);

	}

	$scope.loadEditData = function (item) {

		$scope.CreateModel = angular.copy(item);
		$scope.time = new Date('1970-01-01T' + item.TIME + 'Z');
		$newdate =  new Date(2018, 11, 24, $scope.time.getHours()-7, $scope.time.getMinutes(), 0);
		$scope.CreateModel.TIME = $newdate;
	}
	
	$scope.resetModel = function () {

		$scope.CreateModel = {
			ID: 0, LOCATION: "", NAME: "", DESCRIPTION: "",TIME:new Date(2018, 11, 24, 8, 0, 0, 0),
			WEBSITE: "", PICTURE: "",  IMAGE: ""
		};
		
	}
	

	$scope.resetSearch = function () {
		$scope.modelSearch = {
			"code_id": "",
			"code": "",
			"name": "",
			"lastname": "",
			"flag":false,
		};
		$scope.LoadSearch();
	}

	////////////////////////////////////////////////////////////////////////////////////////
	// Event
	$scope.onInit = function () {

		$scope.resetModel();
		$scope.resetSearch();
		if ($scope.listNameTitle.length > 0) {
			$scope.TempNameTitleEN.selected = $scope.listNameTitle[0];
			$scope.TempNameTitleTH.selected = $scope.listNameTitle[0];
		}


		$scope.listPageSize.forEach(function (entry, index) {
			if (0 === index)
				$scope.TempPageSize.selected = entry;
		});

		//$scope.reload();
	}

	$scope.reload = function () {

		eventApiService.getComboBox(null, function (result) {
			if (result.data.status === true) {
				$scope.ListEventModel = result.data.message;
			} else {
				baseService.showMessage(result.message);
			}
		});
		eventApiService.listEvent($scope.modelSearch, function (results) {
			console.log(results);
			var result = results.data;
			if (result.status === true) {

				$scope.totalPage = result.toTalPage;
				$scope.listPageIndex = baseService.getListPage(result.toTalPage);
				$scope.listPageIndex.forEach(function (entry, index) {
					if ($scope.PageIndex === entry.Value)
						$scope.TempPageIndex.selected = entry;
				});
		
				$scope.totalRecords = result.totalRecords;
				$scope.modelDeviceList = result.message;
				console.log($scope.modelDeviceList);
			} else {

			}
		})
		$('#collapseOne').collapse('show')
		$('#collapseTwo').collapse('hide')
		$('.nav-tabs a[href="#tab1default"]').tab('show');
	}

	$scope.onDeleteTagClick = function (item) {
		eventApiService.deleteEvent({ ID: item.ID }, function (result) {
			if (result.status === true) {
				$scope.reload();
			} else {
				baseService.showMessage(result.message);
			}
		});

	}

	$scope.validatecheck = function () {
		var bResult = true;
		$(".require").hide();



		if ($scope.CreateModel.NAME == "") {
			$(".CreateModel_NAME").show();
			bResult = false;
		}
		if ($scope.CreateModel.DATE_END == "0000-00-00" || $scope.CreateModel.DATE_END == null) {
			$(".CreateModel_DATE_END").show();
			bResult = false;
		}
		if ($scope.CreateModel.DATE_START == "0000-00-00" || $scope.CreateModel.DATE_START == null) {
			$(".CreateModel_DATE_START").show();
			bResult = false;
		}
		if ($scope.CreateModel.TIME == "00:00:00") {
			$(".CreateModel_TIME").show();
			bResult = false;
		}
		if ($scope.CreateModel.DESCRIPTION == "") {
			$(".CreateModel_DESCRIPTION").show();
			bResult = false;
		}
		if ($scope.CreateModel.LOCATION == "") {
			$(".CreateModel_LOCATION").show();
			bResult = false;
		}
		return bResult;
	}
	
	$scope.onSaveTagClick = function (i) {

		// console.log($scope.CreateModel.TIME);
		$scope.TIME = $scope.CreateModel.TIME.getHours() +':'+ $scope.CreateModel.TIME.getMinutes() +':00';
		// console.log($scope.TIME);
		// $scope.CreateModel.TIME =  undefined;
		$scope.CreateModel.TIME_tmp = $scope.TIME;

		// console.log($scope.CreateModel.TIME_tmp);
		var bValid = $scope.validatecheck();
		if (true == bValid) {
			$scope.CreateModel.DATE_START = $filter('date')($scope.CreateModel.DATE_START, "yyyy-MM-dd");
			$scope.CreateModel.DATE_END = $filter('date')($scope.CreateModel.DATE_END, "yyyy-MM-dd");
			eventApiService.saveEvent($scope.CreateModel, function (result) {
				if (result.status == true) {
					$scope.ViewImgPreview = false;
					$scope.ShowDevice();
				} else {
					baseService.showMessage(result.message);
				}
			});

		}
	}	
	$scope.upload = function () {
		
		console.log("dawdwa",$scope.filegeneralinfo);

		
        if(undefined != $scope.filegeneralinfo){   
			console.log("หลัง if");
            var filegeneralinfo = $scope.filegeneralinfo.name;
            Upload.upload({
                url: 'Event/upload_file',
                data: {
                    filegeneralinfo: $scope.filegeneralinfo
                }
            })
            .then(function (resp) {
				console.log(resp)
                if (resp.status == 200) {
					$scope.CreateModel.IMAGE = resp.data.message;
					
                    $scope.onSaveTagClick();
                    // baseService.showMessage("Save success");
                } else {
                    baseService.showMessage("Can not upload file");
                }
            });
        }else{
            $scope.onSaveTagClick();
        }
    }
	$scope.SelectFile = function (e) {
		$scope.ViewImgPreview = true;
		var reader = new FileReader();
		reader.onload = function (e) {
			$scope.PreviewImage = e.target.result;
			$scope.$apply();
		};

		reader.readAsDataURL(e.target.files[0]);
	};
}]); 