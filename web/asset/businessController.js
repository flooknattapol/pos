myApp.controller('businessController', ['$scope', '$filter', 'Upload', 'baseService', 'businessApiService','businesstypeApiService','businesscategoryApiService', function ($scope, $filter, Upload, baseService, businessApiService,businesstypeApiService,businesscategoryApiService) {

	$scope.DataDetailModel = [];
	$scope.ViewImgPreview = false;
	$scope.modelDeviceList = [];
	$scope.CreateModel = {};
	$scope.modelSearch = {};
	$scope.TempTypeBusinessIndex = {};
	$scope.TempCategoryBusinessIndex = {};

	$scope.ListTypeBusiness = [];


	//page system 
	$scope.listPageSize = baseService.getListPageSize();
	$scope.TempPageSize = {};
	$scope.TempPageIndex = {};
	$scope.PageSize = baseService.setPageSize(20);;
	$scope.totalPage = 1; //init;
	$scope.totalRecords = 0;
	$scope.PageIndex = 1;
	$scope.SortColumn = baseService.setSortColumn('id');
	$scope.SortOrder = baseService.setSortOrder('asc');

	$scope.salaryTab = "tab";
	$scope.isView = false;

	$scope.sort = function (e) {
		baseService.sort(e); $scope.SortColumn = baseService.getSortColumn();
		$scope.SortOrder = baseService.getSortOrder();
		$scope.reload();
	}

	$scope.getFirstPage = function () { $scope.PageIndex = baseService.getFirstPage(); $scope.reload(); }
	$scope.getBackPage = function () { $scope.PageIndex = baseService.getBackPage(); $scope.reload(); }
	$scope.getNextPage = function () { $scope.PageIndex = baseService.getNextPage(); $scope.reload(); }
	$scope.getLastPage = function () { $scope.PageIndex = baseService.getLastPage(); $scope.reload(); }
	$scope.searchByPage = function () { $scope.PageIndex = baseService.setPageIndex($scope.TempPageIndex.selected.PageIndex); $scope.reload(); }
	$scope.setPageSize = function (data) { $scope.PageSize = baseService.setPageSize($scope.TempPageSize.selected.Value); }
	$scope.loadByPageSize = function () { $scope.PageIndex = baseService.setPageIndex(1); $scope.setPageSize(); $scope.reload(); }
	//page system


	$scope.ShowDevice = function () {
		$(".DisplayDevice").show();
		$(".SearchDevice").hide();
		$(".addDevice").hide();
		$("#saveChange").modal('hide');
		$scope.reload();
	}
	$scope.onChange = function () {
		// console.log(JSON.stringify($scope.CreateModel),JSON.stringify($scope.oldCreateModel))
		var resultObj = JSON.stringify($scope.CreateModel) === JSON.stringify($scope.oldCreateModel)
		var resultArray = JSON.stringify(JSON.parse(angular.toJson($scope.salaryModelList))) === JSON.stringify($scope.oldSalaryModelList)

		console.log($scope.oldSalaryModelList)
		console.log($scope.salaryModelList)
		console.log(resultArray)
		if (resultObj == true && resultArray == true) {
			$scope.ShowDevice();
		} else {
			$("#saveChange").modal('show');
		}

	}
	$scope.ShowSearch = function () {
		$(".DisplayDevice").hide();
		$(".SearchDevice").show();
		$(".addDevice").hide();
	}

	$scope.LoadSearch = function () {
		$scope.ShowDevice();

	}

	$scope.AddNewDevice = function () {
		$scope.resetModel();
		$(".require").hide();
		$(".DisplayDevice").hide();
		$(".SearchDevice").hide();
		$(".addDevice").show();
		$scope.addDataDetail();
	}

	$scope.onEditTagClick = function (item) {
		$scope.AddNewDevice();
		$scope.loadEditData(item);

	}

	$scope.loadEditData = function (item) {
		$scope.CreateModel = angular.copy(item);
		$scope.ListTypeBusiness.forEach(function (entry, index) {
			if (entry.ID === item.TYPE_ID) {
				$scope.TempTypeBusinessIndex.selected = entry;
			}
		});
		$scope.ListCategoryBusiness.forEach(function (entry, index) {
			if (entry.ID === item.CATEGORY_ID) {
				$scope.TempCategoryBusinessIndex.selected = entry;
			}
		});
		businessApiService.getDetail({ ID: item.ID }, function (result) {
			console.log(result);
			if (true == result.status) {
				$scope.DataDetailModel = result.message
			} else {
				baseService.showMessage(result.message);
			}
		});
	}
	
	$scope.resetModel = function () {

		$scope.CreateModel = { EXPIRED_FLAG: "", CODE:"",ID: 0, IMAGE:"",NAME: "",EMAIL:"",TEL:"",TYPE_ID:"",TAXID13:"",TAXID10:"",CONTACT:"",CATEGORY_ID:"",IsActive:true};
		
	}
	$scope.addDataDetail = function () {

		$scope.DataDetailModel = [];

		for (var i = 1; i < 2; i++) {
			$scope.DataDetailModel.push(
				{ ID: 0, BUSINESS_ID:0,NAME: "สำนักงานใหญ่",EMAIL:"",TEL:"",ADDRESS:"",IsActive:true }
			)
		}

	}
	$scope.AddNewLine = function () {
		var lineNo = $scope.DataDetailModel.length + 1; console.log(lineNo);
		$scope.DataDetailModel.push(
			{ ID: 0,line_no:lineNo, BUSINESS_ID:0,NAME: "",EMAIL:"",TEL:"",ADDRESS:"",IsActive:true}
		);
	}
	$scope.updateList = function (lineNo, listNewDetial) {

		var lineCount = 1;
		listNewDetial.forEach(function (entry, index) {

			console.log(entry.line_no, lineNo)
			if (entry.line_no !== lineNo) {
				entry.line_no = lineCount;
				$scope.DataDetailModel.push(entry);
				lineCount++;
			}
		});
		$scope.$apply();
	}

	$scope.onDeleteLine = function (item) {
		$scope.CreateModel.DeleteValueInList = [];
		$scope.CreateModel.DeleteValueInList.push({id:item.id});
		var lineNo = item.line_no;
		var listNewDetial = $scope.DataDetailModel;

		$scope.DataDetailModel = [];
		$scope.updateList(lineNo, listNewDetial);
	}
	$scope.resetSearch = function () {
		$scope.modelSearch = {
			"ID": "",
			"NAME": "",
			"flag":false,
		};
		$scope.LoadSearch();
	}

	////////////////////////////////////////////////////////////////////////////////////////
	// Event
	$scope.onInit = function () {

		$scope.resetModel();
		$scope.resetSearch();
		
		$scope.listPageSize.forEach(function (entry, index) {
			if (0 === index)
				$scope.TempPageSize.selected = entry;
		});

		//$scope.reload();
	}

	$scope.reload = function () {
		businesstypeApiService.getComboList(null, function (results) {
			console.log(results);
			if (results.data.status === true) {
				$scope.ListTypeBusiness = results.data.message;
			}
			else{

			}
		});
		businesscategoryApiService.getComboList(null, function (results) {
			console.log(results);
			if (results.data.status === true) {
				$scope.ListCategoryBusiness = results.data.message;
			}
			else{

			}
		});
		businessApiService.getList($scope.modelSearch, function (results) {
			console.log(results);
			var result = results.data;
			if (result.status === true) {

				$scope.totalPage = result.toTalPage;
				$scope.listPageIndex = baseService.getListPage(result.toTalPage);
				$scope.listPageIndex.forEach(function (entry, index) {
					if ($scope.PageIndex === entry.Value)
						$scope.TempPageIndex.selected = entry;
				});
				$scope.Session = result.Session;
				$scope.totalRecords = result.totalRecords;
				$scope.modelDeviceList = result.message;
				console.log($scope.modelDeviceList);
			} else {

			}
		})
		$('#collapseOne').collapse('show')
		$('#collapseTwo').collapse('hide')
		$('.nav-tabs a[href="#tab1default"]').tab('show');
	}

	$scope.onDeleteTagClick = function (item) {
		businessApiService.delete({ ID: item.ID }, function (result) {
			if (result.status === true) {
				$scope.reload();
			} else {
				baseService.showMessage(result.message);
			}
		});

	}
	$scope.onUnDeleteTagClick = function (item) {
		businessApiService.undelete({ ID: item.ID }, function (result) {
			if (result.status === true) {
				$scope.reload();
			} else {
				baseService.showMessage(result.message);
			}
		});

	}
	$scope.validatecheck = function () {
		var bResult = true;
		$(".require").hide();

		if ($scope.CreateModel.NAME == "") {
			console.log("dawda")
			$(".CreateModel_NAME").show();
			bResult = false;
		}
		if ($scope.CreateModel.TEL == "") {
			$(".CreateModel_TEL").show();
			bResult = false;
		}
		if ($scope.CreateModel.EXPIRED_FLAG == "") {
			$(".CreateModel_EXPIRED_FLAG").show();
			bResult = false;
		}
		// if ($scope.CreateModel.EXPIRED_FLAG == 1 && $scope.CreateModel.CODE == "") {
		// 	$(".CreateModel_CODE").show();
		// 	bResult = false;
		// }
		if ($scope.CreateModel.EMAIL == "") {
			$(".CreateModel_EMAIL").show();
			bResult = false;
		}
		if ($scope.CreateModel.TYPE_ID == "") {
			$(".CreateModel_TYPE").show();
			bResult = false;
		}
		if ($scope.CreateModel.CONTACT == "") {
			$(".CreateModel_CONTACT").show();
			bResult = false;
		}
		if ($scope.CreateModel.CATEGORY_ID == "") {
			$(".CreateModel_CATEGORY_ID").show();
			bResult = false;
		}
		return bResult;
	}
	
	$scope.onSaveTagClick = function () {
		console.log($scope.CreateModel);
		if($scope.TempTypeBusinessIndex.selected != undefined){
			$scope.CreateModel.TYPE_ID = $scope.TempTypeBusinessIndex.selected.ID;
		}
		if($scope.TempCategoryBusinessIndex.selected != undefined){
			$scope.CreateModel.CATEGORY_ID = $scope.TempCategoryBusinessIndex.selected.ID;
		}
		var bValidDetail = true;
		var bValid = $scope.validatecheck();
		console.log(bValid)
		
		$scope.DataDetailModel.forEach(function (entry, index) {
			if(entry.NAME == "" || entry.TEL== "" || entry.ADDRESS == ""|| entry.EMAIL== ""){
				bValidDetail = false;
			}
		});
		if($scope.DataDetailModel.length == 0 ){
			baseService.showMessage("ต้องมีอย่างน้อย 1 สาขา");
		}else if (true == bValid && bValidDetail == true) {
			$scope.CreateModel.detail = $scope.DataDetailModel;
			businessApiService.save($scope.CreateModel, function (result) {
				if (result.status == true) {
					$scope.ShowDevice();
				} else {
					baseService.showMessage(result.message);
				}
			});
		}else{
			baseService.showMessage("กรุณาใส่ข้อมูลให้ครบถ้วน");
		}
	}
	$scope.SelectFile = function (e) {
		$scope.ViewImgPreview = true;
		var reader = new FileReader();
		reader.onload = function (e) {
			$scope.PreviewImage = e.target.result;
			$scope.$apply();
		};

		reader.readAsDataURL(e.target.files[0]);
	};
	$scope.upload = function () {
		
        if(undefined != $scope.filegeneralinfo){   
            var filegeneralinfo = $scope.filegeneralinfo.name;
            Upload.upload({
                url: 'Business/upload_file',
                data: {
                    filegeneralinfo: $scope.filegeneralinfo
                }
            })
            .then(function (resp) {
				console.log(resp)
                if (resp.status == 200) {
					$scope.CreateModel.IMAGE = resp.data.message;
					$scope.ViewImgPreview = false;
                    $scope.onSaveTagClick();
                } else {
                    baseService.showMessage("Can not upload file");
                }
            });
        }else{
            $scope.onSaveTagClick();
        }
    }
}]); 