myApp.controller('userController', ['$scope', '$filter', 'Upload', 'baseService', 'userApiService','branchApiService','businessApiService','roleApiService', function ($scope, $filter, Upload, baseService, userApiService,branchApiService,businessApiService,roleApiService) {

	$scope.TempBusinessIndex ={};
	$scope.TempBranchIndex ={};
	$scope.TempRoleIndex ={};
	$scope.DataDetailModel = [];
	$scope.ViewImgPreview = false;
	$scope.modelDeviceList = [];
	$scope.CreateModel = {};
	$scope.modelSearch = {};

	$scope.salaryModel = {};
	$scope.salaryModelList = [];
	$scope.oldSalaryModelList = [];
	$scope.TempNameTitleTH = {};
	$scope.TempNameTitleEN = {};
	$scope.listNameTitle = [{ th: "นาย", en: "Mr." }, { th: "นาง", en: "Mrs." }, { th: "นางสาว", en: "Miss" }];

	$scope.listEmpType = [];
	$scope.TempEmpTypeIndex = {}

	//page system 
	$scope.listPageSize = baseService.getListPageSize();
	$scope.TempPageSize = {};
	$scope.TempPageIndex = {};
	$scope.PageSize = baseService.setPageSize(20);;
	$scope.totalPage = 1; //init;
	$scope.totalRecords = 0;
	$scope.PageIndex = 1;
	$scope.SortColumn = baseService.setSortColumn('id');
	$scope.SortOrder = baseService.setSortOrder('asc');

	$scope.salaryTab = "tab";
	$scope.isView = false;

	$scope.sort = function (e) {
		baseService.sort(e); $scope.SortColumn = baseService.getSortColumn();
		$scope.SortOrder = baseService.getSortOrder();
		$scope.reload();
	}

	$scope.getFirstPage = function () { $scope.PageIndex = baseService.getFirstPage(); $scope.reload(); }
	$scope.getBackPage = function () { $scope.PageIndex = baseService.getBackPage(); $scope.reload(); }
	$scope.getNextPage = function () { $scope.PageIndex = baseService.getNextPage(); $scope.reload(); }
	$scope.getLastPage = function () { $scope.PageIndex = baseService.getLastPage(); $scope.reload(); }
	$scope.searchByPage = function () { $scope.PageIndex = baseService.setPageIndex($scope.TempPageIndex.selected.PageIndex); $scope.reload(); }
	$scope.setPageSize = function (data) { $scope.PageSize = baseService.setPageSize($scope.TempPageSize.selected.Value); }
	$scope.loadByPageSize = function () { $scope.PageIndex = baseService.setPageIndex(1); $scope.setPageSize(); $scope.reload(); }
	//page system


	$scope.ShowDevice = function () {
		$(".DisplayDevice").show();
		$(".SearchDevice").hide();
		$(".addDevice").hide();
		$("#saveChange").modal('hide');
		$scope.reload();
	}
	
	$scope.ShowSearch = function () {
		$(".DisplayDevice").hide();
		$(".SearchDevice").show();
		$(".addDevice").hide();
	}

	$scope.LoadSearch = function () {
		$scope.ShowDevice();

	}

	$scope.AddNewDevice = function () {
		$scope.fagEdit = false;
		$scope.resetModel();
		$(".require").hide();
		$(".DisplayDevice").hide();
		$(".SearchDevice").hide();
		$(".addDevice").show();
		$scope.salaryTab = "";
	}

	$scope.onEditTagClick = function (item) {
		$scope.AddNewDevice();
		$scope.loadEditData(item);

	}

	$scope.loadEditData = function (item) {

		$scope.fagEdit = true;
		$scope.CreateModel = angular.copy(item);
		userApiService.getDetail({ ID: item.ID }, function (result) {
			console.log(result);
			if (true == result.status) {
				$scope.DataDetailModel = result.message
				$scope.DataDetailModel.forEach(function (entrylist, indexlist) {
					$scope.DataDetailModel[indexlist].ListBusiness = $scope.ListBusinessTemp;
					$scope.DataDetailModel[indexlist].ListBusiness.forEach(function (entrybusiness, indexbusiness) {
						if (entrybusiness.ID === entrylist.BUSINESS_ID) {
							$scope.DataDetailModel[indexlist].TempBusinessIndex = entrybusiness;
						}
					});

					console.log(entrylist.ID)
					branchApiService.getComboList({ID:entrylist.BUSINESS_ID}, function (results) {
						if (results.status === true) {
							// $scope.ListBranch = results.message;
							$scope.DataDetailModel[indexlist].ListBranch = results.message;
							$scope.DataDetailModel[indexlist].ListBranch.forEach(function (entrybranch, indexbranch) {
								// console.log("data",entrybranch.ID,entrylist.BRANCH_ID);
								if (entrybranch.ID === entrylist.BRANCH_ID) {
									$scope.DataDetailModel[indexlist].TempBranchIndex = entrybranch;
								}
							});
		
						}
						else{

						}
					});
					roleApiService.getComboList({ID:entrylist.BUSINESS_ID}, function (results) {
						if (results.data.status === true) {
							// $scope.ListRole = results.message;
							$scope.DataDetailModel[indexlist].ListRole = results.data.message;
							$scope.DataDetailModel[indexlist].ListRole.forEach(function (entryrole, indexrole) {
								if (entryrole.ID === entrylist.ROLE_ID) {
									$scope.DataDetailModel[indexlist].TempRoleIndex = entryrole;
								}
							});
		
						}
						else{

						}
					});
				});
			} else {
				baseService.showMessage(result.message);
			}
		});
	}
	
	$scope.getDetailCombobox = function(item){
		console.log(item.TempBusinessIndex.ID);
		branchApiService.getComboList({ID:item.TempBusinessIndex.ID}, function (results) {
			if (results.status === true) {
				$scope.DataDetailModel[indexlist].ListBranch = results.message;
				$scope.DataDetailModel[indexlist].ListBranch.forEach(function (entrybranch, indexbranch) {
					if (entrybranch.ID === entrylist.BRANCH_ID) {
						$scope.DataDetailModel[indexlist].TempBranchIndex = entrybranch;
					}
				});

			}
			else{

			}
		});
	}
	$scope.resetModel = function () {

		$scope.CreateModel = {
			ID: 0, NAME: "", PASSWORD: "",
			TEL: "",  EMAIL: ""
		};
		
	}
	$scope.AddNewLine = function () {
		var lineNo = $scope.DataDetailModel.length + 1; console.log(lineNo);
		$scope.DataDetailModel.push(
			{ ID: 0,ListBusiness:$scope.ListBusinessTemp, USER_ID:0,BUSINESS_ID:0,BRANCH_ID: 0,ROLE_ID:0,IsActive:true}
		);
	}

	$scope.resetSearch = function () {
		$scope.modelSearch = {
			"NAME": ""
		};
		$scope.LoadSearch();
	}

	////////////////////////////////////////////////////////////////////////////////////////
	// Event
	$scope.onInit = function () {

		$scope.resetModel();
		$scope.resetSearch();
	
		$scope.listPageSize.forEach(function (entry, index) {
			if (0 === index)
				$scope.TempPageSize.selected = entry;
		});

		//$scope.reload();
	}

	$scope.reload = function () {
		
		businessApiService.getComboList(null, function (results) {
			
			if (results.data.status === true) {
				$scope.ListBusinessTemp = results.data.message;
				console.log("business",$scope.ListBusiness);
			}
			else{

			}
		});
		
		userApiService.getList($scope.modelSearch, function (results) {
			// console.log(results);
			var result = results.data;
			if (result.status === true) {

				$scope.totalPage = result.toTalPage;
				$scope.listPageIndex = baseService.getListPage(result.toTalPage);
				$scope.listPageIndex.forEach(function (entry, index) {
					if ($scope.PageIndex === entry.Value)
						$scope.TempPageIndex.selected = entry;
				});
				$scope.Session = result.Session;
				$scope.totalRecords = result.totalRecords;
				$scope.modelDeviceList = result.message;
				console.log($scope.modelDeviceList);
			} else {

			}
		})
		$('#collapseOne').collapse('show')
		$('#collapseTwo').collapse('hide')
		$('.nav-tabs a[href="#tab1default"]').tab('show');
	}

	$scope.onDeleteTagClick = function (item) {
		userApiService.delete({ ID: item.ID }, function (result) {
			if (result.status === true) {
				$scope.reload();
			} else {
				baseService.showMessage(result.message);
			}
		});

	}
	$scope.onUnDeleteTagClick = function (item) {
		userApiService.undelete({ ID: item.ID }, function (result) {
			if (result.status === true) {
				$scope.reload();
			} else {
				baseService.showMessage(result.message);
			}
		});

	}
	$scope.validatecheck = function () {
		var bResult = true;
		$(".require").hide();

		
		if ($scope.CreateModel.NAME == "") {
			$(".CreateModel_NAME").show();
			bResult = false;
		}
		
		if ($scope.CreateModel.TEL == "") {
			$(".CreateModel_TEL").show();
			bResult = false;
		}
		if ($scope.CreateModel.EMAIL == "") {
			$(".CreateModel_EMAIL").show();
			bResult = false;
		}
		
		return bResult;
	}

	$scope.onSaveTagClick = function () {

		var bValid = $scope.validatecheck();
		$scope.CreateModel.detail = [];
		for(i = 0; i < $scope.DataDetailModel.length; i++){
			$scope.CreateModel.detail.push(
				{ 
				ID: $scope.DataDetailModel[i].ID, 
				USER_ID:$scope.CreateModel.ID,
				BUSINESS_ID:$scope.DataDetailModel[i].TempBusinessIndex.ID,
				ROLE_ID: $scope.DataDetailModel[i].TempRoleIndex.ID,
				BRANCH_ID: $scope.DataDetailModel[i].TempBranchIndex.ID,
				IsActive:true}
			);
		}
		// $scope.CreateModel.detail = $scope.DataDetailModel;
		console.log($scope.CreateModel);
		if (true == bValid) {
			// userApiService.save($scope.CreateModel, function (result) {
			// 	if (result.status == true) {
			// 		$scope.ViewImgPreview = false;
			// 		$scope.ShowDevice();
			// 	} else {
			// 		baseService.showMessage(result.message);
			// 	}
			// });

		}
	}

}]); 