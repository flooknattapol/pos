<?Php
require_once("lib/PromptPayQR.php");
header('Access-Control-Allow-Origin: *');
header("Access-Control-Allow-Headers: X-API-KEY, Origin, X-Requested-With, Content-Type, Accept, Access-Control-Request-Method");
header("Access-Control-Allow-Methods: GET, POST, PUT, DELETE");

$Tel = $_GET['Tel'];
$TotalOrder = $_GET['TotalOrder'];
$PromptPayQR = new PromptPayQR(); // new object
$PromptPayQR->size = 8; // Set QR code size to 8
$PromptPayQR->id = $Tel; // PromptPay ID
$PromptPayQR->amount = $TotalOrder; // Set amount (not necessary)
// echo $PromptPayQR->generate() ;die();
$result =  $PromptPayQR->generate();
echo json_encode($result,JSON_UNESCAPED_UNICODE);
?>