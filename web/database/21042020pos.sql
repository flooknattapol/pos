-- phpMyAdmin SQL Dump
-- version 4.7.0
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Apr 21, 2020 at 12:15 PM
-- Server version: 5.6.34-log
-- PHP Version: 7.1.5

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `pos`
--

-- --------------------------------------------------------

--
-- Table structure for table `ma_branch`
--

CREATE TABLE `ma_branch` (
  `ID` int(11) NOT NULL,
  `line_no` int(11) NOT NULL,
  `BUSINESS_ID` int(11) NOT NULL,
  `UID` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `EMAIL` varchar(300) COLLATE utf8_unicode_ci NOT NULL,
  `NAME` varchar(300) COLLATE utf8_unicode_ci NOT NULL,
  `ADDRESS` varchar(300) COLLATE utf8_unicode_ci NOT NULL,
  `TEL` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `CreateDate` datetime NOT NULL,
  `IsActive` int(11) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `ma_branch`
--

INSERT INTO `ma_branch` (`ID`, `line_no`, `BUSINESS_ID`, `UID`, `EMAIL`, `NAME`, `ADDRESS`, `TEL`, `CreateDate`, `IsActive`) VALUES
(7, 1, 7, 'c9f0f895fb98ab9159f51fd0297e236d', 'ponlamaisaka1@gmail.com', 'สาขา1', 'siam', '02', '1970-01-01 00:00:00', 1),
(8, 2, 7, 'c9f0f895fb98ab9159f51fd0297e236d', 'ponlamaisaka2@gmail.com', 'สาขา2', 'central', '02', '1970-01-01 00:00:00', 1),
(9, 0, 8, '45c48cce2e2d7fbdea1afc51c7c6ad26', 'kaisaka1@gmail.com', 'saka1', 'the mall', '02', '1970-01-01 00:00:00', 1),
(10, 0, 9, 'd3d9446802a44259755d38e6d163e820', 'moosaka1@gmail.com', 'saka1', 'the mall', '02', '1970-01-01 00:00:00', 1),
(11, 0, 10, '6512bd43d9caa6e02c990b0a82652dca', 'kainoksaka1@gmail.com', 'saka1', 'siam', '02', '0000-00-00 00:00:00', 1),
(14, 3, 7, 'c20ad4d76fe97759aa27a0c99bff6710', 'ponlamaisaka3@gmail.com', 'สาขา3', 'the mall', '02', '0000-00-00 00:00:00', 1),
(15, 0, 11, '9bf31c7ff062936a96d3c8bd1f8f2ff3', 'email1@gmail.com', 'สาขา1', 'siam', '02', '0000-00-00 00:00:00', 1);

-- --------------------------------------------------------

--
-- Table structure for table `ma_business`
--

CREATE TABLE `ma_business` (
  `ID` int(11) NOT NULL,
  `UID` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `EMAIL` varchar(300) COLLATE utf8_unicode_ci NOT NULL,
  `NAME` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `TEL` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `CreateDate` datetime NOT NULL,
  `IsActive` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `ma_business`
--

INSERT INTO `ma_business` (`ID`, `UID`, `EMAIL`, `NAME`, `TEL`, `CreateDate`, `IsActive`) VALUES
(7, 'c4ca4238a0b923820dcc509a6f75849b', 'ponlamai@gmail.com', 'ขายผลไม้', '02', '0000-00-00 00:00:00', 1),
(8, 'c9f0f895fb98ab9159f51fd0297e236d', 'kai@gmail.com', 'ขายไก่', '02', '0000-00-00 00:00:00', 1),
(9, '45c48cce2e2d7fbdea1afc51c7c6ad26', 'moo@gmail.com', 'ขายหมู', '02', '0000-00-00 00:00:00', 1),
(10, 'd3d9446802a44259755d38e6d163e820', 'nok@gmail.com', 'ขายนก', '02', '0000-00-00 00:00:00', 1),
(11, '6512bd43d9caa6e02c990b0a82652dca', 'email@gmail.com', 'ขายของ', '02', '0000-00-00 00:00:00', 1);

-- --------------------------------------------------------

--
-- Table structure for table `ma_goods`
--

CREATE TABLE `ma_goods` (
  `ID` int(11) NOT NULL,
  `NAME` varchar(300) COLLATE utf8_unicode_ci NOT NULL,
  `CATEGORY` int(11) NOT NULL,
  `SOLDBY` int(11) NOT NULL,
  `PRICE` int(11) NOT NULL,
  `COST` int(11) NOT NULL,
  `SKU` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `COMPOSITE` varchar(300) COLLATE utf8_unicode_ci NOT NULL,
  `STOCK` int(11) NOT NULL,
  `BRANCH_ID` int(11) NOT NULL,
  `IsActive` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `ma_goods_category`
--

CREATE TABLE `ma_goods_category` (
  `ID` int(11) NOT NULL,
  `BRANCH_ID` int(11) NOT NULL,
  `NAME` varchar(300) COLLATE utf8_unicode_ci NOT NULL,
  `COLOR` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `IsActive` int(11) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `ma_permission`
--

CREATE TABLE `ma_permission` (
  `ID` int(11) NOT NULL,
  `NAME` varchar(300) COLLATE utf8_unicode_ci NOT NULL,
  `DETAIL` varchar(300) COLLATE utf8_unicode_ci NOT NULL,
  `TYPE` int(11) NOT NULL,
  `IsActive` int(11) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `ma_role`
--

CREATE TABLE `ma_role` (
  `ID` int(11) NOT NULL,
  `NAME` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `BRANCH_ID` int(11) NOT NULL,
  `PERMISSION_DASHBOARD_IsActive` int(11) NOT NULL DEFAULT '0',
  `PERMISSION_POS_IsActive` int(11) NOT NULL DEFAULT '0',
  `PERMISSION_POS` varchar(300) COLLATE utf8_unicode_ci NOT NULL,
  `PERMISSION_DASHBOARD` varchar(300) COLLATE utf8_unicode_ci NOT NULL,
  `MASTER` int(11) NOT NULL DEFAULT '0',
  `IsActive` int(11) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `ma_role`
--

INSERT INTO `ma_role` (`ID`, `NAME`, `BRANCH_ID`, `PERMISSION_DASHBOARD_IsActive`, `PERMISSION_POS_IsActive`, `PERMISSION_POS`, `PERMISSION_DASHBOARD`, `MASTER`, `IsActive`) VALUES
(10, 'เจ้าของ', 7, 1, 1, '', '', 1, 1),
(11, 'ผู้ดูแลระบบ', 7, 1, 1, '', '', 0, 1),
(12, 'ผู้จัดการ', 7, 1, 1, '', '', 0, 1),
(13, 'ผู้จัดการ', 7, 0, 1, '', '', 0, 1),
(14, 'เจ้าของ', 8, 1, 1, '', '', 1, 1),
(15, 'ผู้ดูแลระบบ', 8, 1, 1, '', '', 0, 1),
(16, 'ผู้จัดการ', 8, 1, 1, '', '', 0, 1),
(17, 'ผู้จัดการ', 8, 0, 1, '', '', 0, 1),
(18, 'เจ้าของ', 9, 1, 1, '', '', 1, 1),
(19, 'ผู้ดูแลระบบ', 9, 1, 1, '', '', 0, 1),
(20, 'ผู้จัดการ', 9, 1, 1, '', '', 0, 1),
(21, 'ผู้จัดการ', 9, 0, 1, '', '', 0, 1),
(22, 'เจ้าของ', 10, 1, 1, '', '', 1, 1),
(23, 'ผู้ดูแลระบบ', 10, 1, 1, '', '', 0, 1),
(24, 'ผู้จัดการ', 10, 1, 1, '', '', 0, 1),
(25, 'ผู้จัดการ', 10, 0, 1, '', '', 0, 1),
(26, 'เจ้าของ', 11, 1, 1, '', '', 1, 1),
(27, 'ผู้ดูแลระบบ', 11, 1, 1, '', '', 0, 1),
(28, 'ผู้จัดการ', 11, 1, 1, '', '', 0, 1),
(29, 'ผู้จัดการ', 11, 0, 1, '', '', 0, 1),
(30, 'เจ้าของ', 12, 1, 1, '', '', 1, 1),
(31, 'ผู้ดูแลระบบ', 12, 1, 1, '', '', 0, 1),
(32, 'ผู้จัดการ', 12, 1, 1, '', '', 0, 1),
(33, 'ผู้จัดการ', 12, 0, 1, '', '', 0, 1),
(34, 'เจ้าของ', 13, 1, 1, '', '', 1, 1),
(35, 'ผู้ดูแลระบบ', 13, 1, 1, '', '', 0, 1),
(36, 'ผู้จัดการ', 13, 1, 1, '', '', 0, 1),
(37, 'ผู้จัดการ', 13, 0, 1, '', '', 0, 1),
(38, 'เจ้าของ', 14, 1, 1, '', '', 1, 1),
(39, 'ผู้ดูแลระบบ', 14, 1, 1, '', '', 0, 1),
(40, 'ผู้จัดการ', 14, 1, 1, '', '', 0, 1),
(41, 'ผู้จัดการ', 14, 0, 1, '', '', 0, 1),
(42, 'เจ้าของ', 15, 1, 1, '', '', 1, 1),
(43, 'ผู้ดูแลระบบ', 15, 1, 1, '', '', 0, 1),
(44, 'ผู้จัดการ', 15, 1, 1, '', '', 0, 1),
(45, 'ผู้จัดการ', 15, 0, 1, '', '', 0, 1);

-- --------------------------------------------------------

--
-- Table structure for table `ma_user`
--

CREATE TABLE `ma_user` (
  `ID` int(11) NOT NULL,
  `EMAIL` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `PASSWORD` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `NAME` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `TEL` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `ROLE_ID` int(11) NOT NULL,
  `BRANCH_ID` int(11) NOT NULL,
  `IsActive` int(11) DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `ma_user`
--

INSERT INTO `ma_user` (`ID`, `EMAIL`, `PASSWORD`, `NAME`, `TEL`, `ROLE_ID`, `BRANCH_ID`, `IsActive`) VALUES
(5, 'ponlamaisaka1@gmail.com', '', 'เจ้าของ', '', 10, 7, 1),
(6, 'ponlamaisaka2@gmail.com', '', 'เจ้าของ', '', 14, 8, 1),
(7, 'ponlamaisaka1@gmail.com', '', 'เจ้าของ', '', 18, 9, 1),
(8, 'kaisaka1@gmail.com', '', 'เจ้าของ', '', 22, 10, 1),
(9, 'admin@pos.com', '202cb962ac59075b964b07152d234b70', 'admin', '', 0, 0, 1),
(10, 'kainoksaka1@gmail.com', 'a2ef406e2c2351e0b9e80029c909242d', 'เจ้าของ', '', 26, 11, 1),
(11, 'moosaka1@gmail.com', '2e0aca891f2a8aedf265edf533a6d9a8', 'เจ้าของ', '', 30, 12, 1),
(12, 'moosaka2@gmail.com', 'a2ef406e2c2351e0b9e80029c909242d', 'เจ้าของ', '', 34, 13, 1),
(13, 'ponlamaisaka3@gmail.com', 'a2ef406e2c2351e0b9e80029c909242d', 'เจ้าของ', '', 38, 14, 1),
(14, 'email1@gmail.com', 'a2ef406e2c2351e0b9e80029c909242d', 'เจ้าของ', '', 42, 15, 1);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `ma_branch`
--
ALTER TABLE `ma_branch`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `ma_business`
--
ALTER TABLE `ma_business`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `ma_goods`
--
ALTER TABLE `ma_goods`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `ma_goods_category`
--
ALTER TABLE `ma_goods_category`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `ma_permission`
--
ALTER TABLE `ma_permission`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `ma_role`
--
ALTER TABLE `ma_role`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `ma_user`
--
ALTER TABLE `ma_user`
  ADD PRIMARY KEY (`ID`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `ma_branch`
--
ALTER TABLE `ma_branch`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;
--
-- AUTO_INCREMENT for table `ma_business`
--
ALTER TABLE `ma_business`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;
--
-- AUTO_INCREMENT for table `ma_goods`
--
ALTER TABLE `ma_goods`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `ma_goods_category`
--
ALTER TABLE `ma_goods_category`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `ma_permission`
--
ALTER TABLE `ma_permission`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `ma_role`
--
ALTER TABLE `ma_role`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=46;
--
-- AUTO_INCREMENT for table `ma_user`
--
ALTER TABLE `ma_user`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
