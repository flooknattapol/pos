-- phpMyAdmin SQL Dump
-- version 4.7.0
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: May 13, 2020 at 02:32 PM
-- Server version: 5.6.34-log
-- PHP Version: 7.1.5

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `pos`
--

-- --------------------------------------------------------

--
-- Table structure for table `ma_branch`
--

CREATE TABLE `ma_branch` (
  `ID` int(11) NOT NULL,
  `line_no` int(11) NOT NULL,
  `BUSINESS_ID` int(11) NOT NULL,
  `UID` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `EMAIL` varchar(300) COLLATE utf8_unicode_ci NOT NULL,
  `NAME` varchar(300) COLLATE utf8_unicode_ci NOT NULL,
  `ADDRESS` varchar(300) COLLATE utf8_unicode_ci NOT NULL,
  `TEL` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `CreateDate` datetime NOT NULL,
  `IsActive` int(11) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `ma_branch`
--

INSERT INTO `ma_branch` (`ID`, `line_no`, `BUSINESS_ID`, `UID`, `EMAIL`, `NAME`, `ADDRESS`, `TEL`, `CreateDate`, `IsActive`) VALUES
(20, 0, 18, '877fdd7d875265e4d413f869573e5d36', 'f@gmail.com', 'สำนักงานใหญ่', 'siam', '02', '1970-01-01 00:00:00', 1),
(21, 2, 18, '5c60df043789319b9b3e2cfd72a5b746', 'f@gmail.com', 'สาขา2', 'siam', '03', '1970-01-01 00:00:00', 1),
(22, 0, 19, '8ee9b6950fefdf2dea04f8502af0e256', 'game@gmail.com', 'สำนักงานใหญ่', 'siam', '02', '1970-01-01 00:00:00', 1),
(23, 3, 18, '90392f30bef22945bd808ac0e71522d9', 'f@gmail.com', 'สาขา3', 'siam', '03', '1970-01-01 00:00:00', 1),
(24, 4, 18, '2be187bdd2a3e7cc73ffa04ac2de6ecb', 'f@gmail.com', 'สาขา4', 'siam', '04', '1970-01-01 00:00:00', 1),
(25, 0, 20, '0d3106907ea2bb8bdc3f81a7b9862cf5', 'japan@gmail.com', 'สำนักงานใหญ่', 'japan', '02', '1970-01-01 00:00:00', 1),
(26, 0, 21, '40cacb0c808d0e5711776150c76a93da', 'mon@a.com', 'สำนักงานใหญ่', 'siam', '02', '0000-00-00 00:00:00', 1),
(27, 0, 22, '6b4dcfdcefcf01b5ee465694a6238825', 'fff@gaa.com', 'สำนักงานใหญ่', 'siam', '02', '0000-00-00 00:00:00', 1),
(28, 0, 23, 'a406a3a560cc7662744a84f853e82ee3', 'flook', 'สำนักงานใหญ่', 'siam', '02', '1970-01-01 00:00:00', 1),
(29, 0, 24, '19e828d7cc8807d9ec7447700d4706e6', 'flook@mgail', 'สำนักงานใหญ่', 'siam', '02', '0000-00-00 00:00:00', 1);

-- --------------------------------------------------------

--
-- Table structure for table `ma_business`
--

CREATE TABLE `ma_business` (
  `ID` int(11) NOT NULL,
  `UID` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `EMAIL` varchar(300) COLLATE utf8_unicode_ci NOT NULL,
  `NAME` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `TEL` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `IMAGE` varchar(300) COLLATE utf8_unicode_ci NOT NULL,
  `CONTACT` varchar(2000) COLLATE utf8_unicode_ci NOT NULL,
  `CATEGORY_ID` int(11) NOT NULL,
  `TYPE_ID` int(11) NOT NULL,
  `TAXID10` varchar(10) COLLATE utf8_unicode_ci NOT NULL,
  `TAXID13` varchar(13) COLLATE utf8_unicode_ci NOT NULL,
  `CreateDate` datetime NOT NULL,
  `IsActive` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `ma_business`
--

INSERT INTO `ma_business` (`ID`, `UID`, `EMAIL`, `NAME`, `TEL`, `IMAGE`, `CONTACT`, `CATEGORY_ID`, `TYPE_ID`, `TAXID10`, `TAXID13`, `CreateDate`, `IsActive`) VALUES
(18, '8a5c2418e2c9b0bca99075d6ef84647c', 'f@gmail.com', 'อาหารตามสั่ง', '0628842544', 'imgBusiness_2020-04-23-10-44-53.png', 'me', 1, 2, '', '', '0000-00-00 00:00:00', 1),
(19, '7da5a35218d438fecafc7a3591271d15', 'game@gmail.com', 'ร้านเกมส์', '02', '', 'aaa', 1, 1, '', '', '0000-00-00 00:00:00', 1),
(20, '4973cc7158ec39756d58092d2e1c0075', 'japan@gmail.com', 'อาหารญี่ปุ่น', '02', '', 'aa', 1, 2, '', '', '0000-00-00 00:00:00', 1),
(21, 'e2c7f99d64b563dc59b8fb0a93eb233e', 'mon@a.com', 'มนนมสด', '02', '', '', 0, 1, '', '', '0000-00-00 00:00:00', 1),
(22, 'b23a385251a9decfb2a8fef7714334a1', 'gggg@gmad.com', 'ร้านอาหาร2', '02', '', '', 0, 1, '', '', '0000-00-00 00:00:00', 0),
(23, 'e18e90149a3452e2ca50b9ab2d7ef7fd', 'nom@gmail.com', 'ร้านนม', '02', '', 'tt', 0, 1, '', '', '0000-00-00 00:00:00', 1),
(24, '01adba99f817adcac1dc48ae24f298d7', 'na@mail.com', 'ร้านนาย', '02', '', 'na', 1, 1, '', '', '0000-00-00 00:00:00', 1);

-- --------------------------------------------------------

--
-- Table structure for table `ma_business_category`
--

CREATE TABLE `ma_business_category` (
  `ID` int(11) NOT NULL,
  `NAME` varchar(300) COLLATE utf8_unicode_ci NOT NULL,
  `DETAIL` varchar(300) COLLATE utf8_unicode_ci NOT NULL,
  `IsActive` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `ma_business_category`
--

INSERT INTO `ma_business_category` (`ID`, `NAME`, `DETAIL`, `IsActive`) VALUES
(1, 'ทั่วไป', '', 1);

-- --------------------------------------------------------

--
-- Table structure for table `ma_business_type`
--

CREATE TABLE `ma_business_type` (
  `ID` int(11) NOT NULL,
  `NAME` varchar(300) COLLATE utf8_unicode_ci NOT NULL,
  `DETAIL` varchar(300) COLLATE utf8_unicode_ci NOT NULL,
  `IsActive` int(11) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `ma_business_type`
--

INSERT INTO `ma_business_type` (`ID`, `NAME`, `DETAIL`, `IsActive`) VALUES
(1, 'ร้านอาหาร', 'ขายอาหารทั่วไป', 1),
(2, 'ของของชำ', 'ขายของทั่วไป', 1),
(3, 'ของเล่น', '', 1);

-- --------------------------------------------------------

--
-- Table structure for table `ma_customer`
--

CREATE TABLE `ma_customer` (
  `ID` int(11) NOT NULL,
  `NAME` varchar(300) COLLATE utf8_unicode_ci NOT NULL,
  `TEL` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `EMAIL` varchar(300) COLLATE utf8_unicode_ci NOT NULL,
  `CATEGORY_ID` int(11) NOT NULL,
  `BUSINESS_ID` int(11) NOT NULL,
  `AMOUNTBUY` int(11) NOT NULL,
  `REWARDPOINT` int(11) NOT NULL,
  `NOTE` varchar(300) COLLATE utf8_unicode_ci NOT NULL,
  `IsActive` int(11) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `ma_customer`
--

INSERT INTO `ma_customer` (`ID`, `NAME`, `TEL`, `EMAIL`, `CATEGORY_ID`, `BUSINESS_ID`, `AMOUNTBUY`, `REWARDPOINT`, `NOTE`, `IsActive`) VALUES
(1, 'ลูกค้า A', '02', 'f@gmail.com', 1, 18, 0, 10, '', 1),
(2, 'นาย b', '02', 'bb@gmail.com', 1, 18, 0, 45, '', 1),
(3, 'นาย C', '02', 'ccc@gmad.com', 1, 18, 0, 45, '', 1),
(4, '213', '123', '123', 1, 18, 0, 0, '123', 1),
(5, 'dwdw', 'dwdw', 'dwd', 1, 18, 0, 0, 'dwdw', 1),
(6, 'กไไก', 'กไก', 'กไก', 1, 18, 0, 0, 'กไกไ', 1);

-- --------------------------------------------------------

--
-- Table structure for table `ma_customer_category`
--

CREATE TABLE `ma_customer_category` (
  `ID` int(11) NOT NULL,
  `BUSINESS_ID` int(11) NOT NULL,
  `NAME` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `DETAIL` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `IsActive` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `ma_customer_category`
--

INSERT INTO `ma_customer_category` (`ID`, `BUSINESS_ID`, `NAME`, `DETAIL`, `IsActive`) VALUES
(1, 18, 'ทั่วไป', '', 1),
(2, 18, 'พิเศษ', '', 1);

-- --------------------------------------------------------

--
-- Table structure for table `ma_customer_relation`
--

CREATE TABLE `ma_customer_relation` (
  `ID` int(11) NOT NULL,
  `CUSTOMER_ID` int(11) NOT NULL,
  `BUSINESS_ID` int(11) NOT NULL,
  `IsActive` int(11) DEFAULT '1',
  `TimeStamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `ma_discount`
--

CREATE TABLE `ma_discount` (
  `ID` int(11) NOT NULL,
  `UID` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `BARCODE` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `NAME` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `DETAIL` varchar(300) COLLATE utf8_unicode_ci NOT NULL,
  `DISCOUNT` int(11) NOT NULL,
  `BAHTORPERCENT` int(11) NOT NULL,
  `BUSINESS_ID` int(11) NOT NULL,
  `EXPIRATIONDATE` date NOT NULL,
  `IsActive` int(11) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `ma_discount`
--

INSERT INTO `ma_discount` (`ID`, `UID`, `BARCODE`, `NAME`, `DETAIL`, `DISCOUNT`, `BAHTORPERCENT`, `BUSINESS_ID`, `EXPIRATIONDATE`, `IsActive`) VALUES
(1, '1115889357911', '15889357911115889357911.png', 'วันเกิด', '', 100, 0, 18, '0000-00-00', 1),
(2, '1115889362232', '15889362231115889362232.png', 'วันปีใหม่', '', 20, 1, 18, '0000-00-00', 1),
(10, '11158893578510', '158893578511158893578510.png', 'วันสงกานต์', '', 10, 0, 18, '0000-00-00', 1),
(11, '11158925596311', '158925596311158925596311.png', 'ส่วนลดพิเศษ', '', 5, 0, 18, '0000-00-00', 1);

-- --------------------------------------------------------

--
-- Table structure for table `ma_goods`
--

CREATE TABLE `ma_goods` (
  `ID` int(11) NOT NULL,
  `UID` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `NAME` varchar(300) COLLATE utf8_unicode_ci NOT NULL,
  `IMAGE` varchar(300) COLLATE utf8_unicode_ci NOT NULL,
  `DETAIL` varchar(300) COLLATE utf8_unicode_ci NOT NULL,
  `STATUS` tinyint(1) DEFAULT '1',
  `CATEGORY_ID` int(11) NOT NULL,
  `SOLDBY` int(11) NOT NULL,
  `PRICE` int(11) NOT NULL,
  `COST` int(11) NOT NULL,
  `SKU` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `BARCODE` varchar(300) COLLATE utf8_unicode_ci NOT NULL,
  `COMPOSITE` varchar(300) COLLATE utf8_unicode_ci NOT NULL,
  `STOCK` int(11) NOT NULL,
  `BUSINESS_ID` int(11) NOT NULL,
  `IsActive` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `ma_goods`
--

INSERT INTO `ma_goods` (`ID`, `UID`, `NAME`, `IMAGE`, `DETAIL`, `STATUS`, `CATEGORY_ID`, `SOLDBY`, `PRICE`, `COST`, `SKU`, `BARCODE`, `COMPOSITE`, `STOCK`, `BUSINESS_ID`, `IsActive`) VALUES
(1, '8815889289511', 'น้ำส้มปั่น2', 'imgGoods_2020-04-29-16-57-27.png', '', 1, 2, 0, 30, 10, '101', '15889289528815889289511.png', '', 0, 18, 1),
(2, '8815889298502', 'ขนมไทย1', 'imgGoods_2020-04-29-16-57-27.png', '', 1, 3, 0, 5, 1, '102', '15889298508815889298502.png', '', 0, 18, 1),
(3, '8815888352623', 'น้ำมะนาว', 'imgGoods_2020-04-29-16-57-31.png', '', 1, 2, 0, 15, 10, '10', '15888352628815888352623.png', '', 0, 18, 1),
(4, '8815888352634', 'น้ำเขียว', 'imgGoods_2020-05-03-12-52-20.png', '', 1, 3, 0, 100, 10, '11', '15888352638815888352634.png', '', 0, 18, 1),
(5, '8815888352655', 'น้ำปีโป้นมสด', 'imgGoods_2020-05-03-12-52-43.png', '', 1, 1, 0, 60, 20, '111', '15888352658815888352655.png', '', 0, 18, 1),
(6, '8815888352666', 'น้ำแดงมะนาว', 'imgGoods_2020-05-05-14-22-19.png', '', 1, 2, 0, 30, 20, '100', '15888352668815888352666.png', '', 0, 18, 1),
(7, '8815888352687', 'น้ำเขียวมะนาว', 'imgGoods_2020-05-05-13-38-30.png', '', 1, 2, 0, 50, 20, '111', '15888352688815888352687.png', '', 0, 18, 1),
(8, '8815888352698', 'น้ำส้มมะนาว', 'imgGoods_2020-05-05-13-40-56.png', '', 1, 2, 0, 10, 5, '111', '15888352698815888352698.png', '', 0, 18, 1),
(9, '8815888352699', 'น้ำปั๊นปั่น', 'imgGoods_2020-05-05-13-43-12.png', '', 1, 2, 0, 20, 10, '200', '15888352698815888352699.png', '', 0, 18, 1);

-- --------------------------------------------------------

--
-- Table structure for table `ma_goods_category`
--

CREATE TABLE `ma_goods_category` (
  `ID` int(11) NOT NULL,
  `BUSINESS_ID` int(11) NOT NULL,
  `NAME` varchar(300) COLLATE utf8_unicode_ci NOT NULL,
  `DETAIL` varchar(300) COLLATE utf8_unicode_ci NOT NULL,
  `COLOR` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `IsActive` int(11) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `ma_goods_category`
--

INSERT INTO `ma_goods_category` (`ID`, `BUSINESS_ID`, `NAME`, `DETAIL`, `COLOR`, `IsActive`) VALUES
(1, 18, 'อาหาร', 'อาหารทั่ว ๆไป', '', 1),
(2, 18, 'น้ำ', 'น้ำทั่วๆไป', '', 1),
(3, 18, 'ขนม', '', '', 1),
(4, 18, 'ขนมปัง', 'test', '', 1);

-- --------------------------------------------------------

--
-- Table structure for table `ma_permission`
--

CREATE TABLE `ma_permission` (
  `ID` int(11) NOT NULL,
  `NAME` varchar(300) COLLATE utf8_unicode_ci NOT NULL,
  `DETAIL` varchar(300) COLLATE utf8_unicode_ci NOT NULL,
  `TYPE` int(11) NOT NULL,
  `IsActive` int(11) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `ma_role`
--

CREATE TABLE `ma_role` (
  `ID` int(11) NOT NULL,
  `NAME` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `BUSINESS_ID` int(11) NOT NULL,
  `PERMISSION_DASHBOARD_IsActive` int(11) NOT NULL DEFAULT '0',
  `PERMISSION_POS_IsActive` int(11) NOT NULL DEFAULT '0',
  `PERMISSION_POS` varchar(300) COLLATE utf8_unicode_ci NOT NULL,
  `PERMISSION_DASHBOARD` varchar(300) COLLATE utf8_unicode_ci NOT NULL,
  `MASTER` int(11) NOT NULL DEFAULT '0',
  `IsActive` int(11) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `ma_role`
--

INSERT INTO `ma_role` (`ID`, `NAME`, `BUSINESS_ID`, `PERMISSION_DASHBOARD_IsActive`, `PERMISSION_POS_IsActive`, `PERMISSION_POS`, `PERMISSION_DASHBOARD`, `MASTER`, `IsActive`) VALUES
(64, 'เจ้าของ', 18, 1, 1, '', '', 1, 1),
(66, 'ผู้จัดการ', 18, 1, 1, '', '', 0, 1),
(67, 'แคชเชียร์', 18, 0, 1, '', '', 0, 1),
(68, 'เจ้าของ', 19, 1, 1, '', '', 1, 1),
(70, 'ผู้จัดการ', 19, 1, 1, '', '', 0, 1),
(71, 'แคชเชียร์', 19, 0, 1, '', '', 0, 1),
(72, 'เจ้าของ', 20, 1, 1, '', '', 1, 1),
(74, 'ผู้จัดการ', 20, 1, 1, '', '', 0, 1),
(75, 'แคชเชียร์', 20, 0, 1, '', '', 0, 1),
(77, 'เจ้าของ', 21, 1, 1, '', '', 1, 1),
(79, 'ผู้จัดการ', 21, 1, 1, '', '', 0, 1),
(80, 'แคชเชียร์', 21, 0, 1, '', '', 0, 1),
(81, 'เจ้าของ', 22, 1, 1, '', '', 1, 1),
(83, 'ผู้จัดการ', 22, 1, 1, '', '', 0, 1),
(84, 'แคชเชียร์', 22, 0, 1, '', '', 0, 1),
(85, 'เจ้าของ', 23, 1, 1, '', '', 1, 1),
(87, 'ผู้จัดการ', 23, 1, 1, '', '', 0, 1),
(88, 'แคชเชียร์', 23, 0, 1, '', '', 0, 1),
(89, 'เจ้าของ', 24, 1, 1, '', '', 1, 1),
(91, 'ผู้จัดการ', 24, 1, 1, '', '', 0, 1),
(92, 'แคชเชียร์', 24, 0, 1, '', '', 0, 1);

-- --------------------------------------------------------

--
-- Table structure for table `ma_user`
--

CREATE TABLE `ma_user` (
  `ID` int(11) NOT NULL,
  `EMAIL` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `PASSWORD` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `NAME` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `TEL` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `IMAGE` varchar(300) COLLATE utf8_unicode_ci NOT NULL,
  `IsActive` int(11) DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `ma_user`
--

INSERT INTO `ma_user` (`ID`, `EMAIL`, `PASSWORD`, `NAME`, `TEL`, `IMAGE`, `IsActive`) VALUES
(9, 'admin@pos.com', '202cb962ac59075b964b07152d234b70', 'admin', '', '', 1),
(20, 'f@gmail.com', '202cb962ac59075b964b07152d234b70', 'เจ้าของ', '034', 'imgUser_2020-04-23-17-56-14.png', 1),
(21, 'game@gmail.com', '202cb962ac59075b964b07152d234b70', 'เจ้าของ', '', '', 1),
(22, 'japan@gmail.com', '202cb962ac59075b964b07152d234b70', 'เจ้าของ', '', '', 1),
(23, 'mon@a.com', '202cb962ac59075b964b07152d234b70', 'เจ้าของ', '', '', 1),
(29, 'ff@gmail.com', '202cb962ac59075b964b07152d234b70', 'นาย A', '02', 'imgUser_2020-04-23-17-56-44.png', 1),
(30, 'fff@gmail.com', '202cb962ac59075b964b07152d234b70', 'นาย b', '02', '', 1),
(31, 'gggg@gmad.com', '202cb962ac59075b964b07152d234b70', 'เจ้าของ', '', '', 1),
(32, 'nom@gmail.com', '202cb962ac59075b964b07152d234b70', '', '', '', 1),
(33, 'na@mail.com', '202cb962ac59075b964b07152d234b70', 'na', '', '', 1),
(34, 'wwfff@gmail.com', '202cb962ac59075b964b07152d234b70', 'นาย b', '02', 'imgUser_2020-04-23-17-54-34.png', 1),
(35, 'test@', '202cb962ac59075b964b07152d234b70', 'test', '02', '', 1),
(36, 'dad@', '202cb962ac59075b964b07152d234b70', 'พนักงาน', '02', '', 1);

-- --------------------------------------------------------

--
-- Table structure for table `ma_user_position`
--

CREATE TABLE `ma_user_position` (
  `ID` int(11) NOT NULL,
  `USER_ID` int(11) NOT NULL,
  `BUSINESS_ID` int(11) NOT NULL,
  `BRANCH_ID` int(11) NOT NULL,
  `ROLE_ID` int(11) NOT NULL,
  `IsActive` int(11) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `ma_user_position`
--

INSERT INTO `ma_user_position` (`ID`, `USER_ID`, `BUSINESS_ID`, `BRANCH_ID`, `ROLE_ID`, `IsActive`) VALUES
(5, 20, 18, 21, 64, 1),
(7, 21, 19, 22, 68, 1),
(9, 22, 20, 25, 72, 1),
(11, 23, 21, 26, 77, 1),
(14, 30, 18, 20, 67, 1),
(15, 31, 22, 27, 81, 1),
(16, 32, 23, 28, 85, 1),
(17, 33, 24, 29, 89, 1),
(18, 34, 18, 23, 66, 1),
(19, 35, 18, 24, 66, 1),
(20, 36, 18, 21, 67, 1);

-- --------------------------------------------------------

--
-- Table structure for table `report_refund`
--

CREATE TABLE `report_refund` (
  `ID` int(11) NOT NULL,
  `TRANSECTION_ID` int(11) NOT NULL,
  `AMOUNT` int(11) NOT NULL,
  `TimeStamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `report_refund`
--

INSERT INTO `report_refund` (`ID`, `TRANSECTION_ID`, `AMOUNT`, `TimeStamp`) VALUES
(1, 62, 5, '2020-05-10 11:27:20'),
(2, 62, 100, '2020-05-10 11:27:20'),
(3, 79, 100, '2020-05-10 12:19:45'),
(4, 79, 20, '2020-05-10 12:19:45'),
(5, 79, 10, '2020-05-10 12:19:45'),
(6, 79, 10, '2020-05-10 12:19:45'),
(7, 79, 10, '2020-05-10 12:19:45');

-- --------------------------------------------------------

--
-- Table structure for table `temp_order`
--

CREATE TABLE `temp_order` (
  `ID` int(11) NOT NULL,
  `BUSINESS_ID` int(11) NOT NULL,
  `SELLER_ID` int(11) NOT NULL,
  `ORDER_NAME` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `TimeStamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `temp_order_detail`
--

CREATE TABLE `temp_order_detail` (
  `ID` int(11) NOT NULL,
  `TRANSECTION_ID` int(11) NOT NULL,
  `GOODS_ID` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `temp_order_discount`
--

CREATE TABLE `temp_order_discount` (
  `ID` int(11) NOT NULL,
  `TRANSECTION_ID` int(11) NOT NULL,
  `DISCOUNT_ID` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `ts_discount`
--

CREATE TABLE `ts_discount` (
  `ID` int(11) NOT NULL,
  `TRANSECTION_ID` int(11) NOT NULL,
  `AMOUNT_DISCOUNT` int(11) NOT NULL,
  `TimeStamp` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `ts_discount`
--

INSERT INTO `ts_discount` (`ID`, `TRANSECTION_ID`, `AMOUNT_DISCOUNT`, `TimeStamp`) VALUES
(36, 60, 10, 0),
(37, 61, 29, 0),
(38, 62, 0, 0),
(39, 63, 25, 0),
(40, 64, 0, 0),
(41, 65, 0, 0),
(42, 66, 10, 0),
(43, 67, 10, 0),
(44, 68, 0, 0),
(45, 69, 0, 0),
(46, 70, 35, 0),
(47, 71, 0, 0),
(48, 72, 10, 0),
(49, 73, 22, 0),
(50, 74, 20, 0),
(51, 75, 10, 0),
(52, 76, 59, 0),
(53, 77, 0, 0),
(54, 78, 0, 0),
(55, 79, 0, 0),
(56, 80, 0, 0),
(57, 81, 100, 0),
(58, 82, 50, 0),
(59, 83, 100, 0),
(60, 84, 100, 0),
(61, 85, 100, 0),
(62, 86, 14, 0),
(63, 87, 10, 0),
(64, 88, 9, 0),
(65, 89, 10, 0),
(66, 90, 0, 0),
(67, 91, 0, 0),
(68, 92, 0, 0),
(69, 93, 0, 0),
(70, 94, 0, 0),
(71, 95, 0, 0),
(72, 96, 0, 0),
(73, 97, 0, 0),
(74, 98, 10, 0),
(75, 99, 0, 0),
(76, 100, 5, 0),
(77, 101, 0, 0),
(78, 102, 0, 0),
(79, 103, 5, 0);

-- --------------------------------------------------------

--
-- Table structure for table `ts_pointreward`
--

CREATE TABLE `ts_pointreward` (
  `ID` int(11) NOT NULL,
  `BUSINESS_ID` int(11) NOT NULL,
  `POINTREWARD` int(11) NOT NULL,
  `CUSTOMER_ID` int(11) NOT NULL,
  `TotalOrder` int(11) NOT NULL,
  `TimeStamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `ts_pointreward`
--

INSERT INTO `ts_pointreward` (`ID`, `BUSINESS_ID`, `POINTREWARD`, `CUSTOMER_ID`, `TotalOrder`, `TimeStamp`) VALUES
(1, 18, 45, 2, 45, '2020-05-11 12:21:23'),
(2, 18, 45, 3, 45, '2020-05-12 11:14:51');

-- --------------------------------------------------------

--
-- Table structure for table `ts_transection`
--

CREATE TABLE `ts_transection` (
  `ID` int(11) NOT NULL,
  `BUSINESS_ID` int(11) NOT NULL,
  `CUSTOMER_ID` int(11) NOT NULL,
  `ORDER_NAME` varchar(300) COLLATE utf8_unicode_ci NOT NULL,
  `CATEGORYPAYMENT` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `SELLER_ID` int(11) NOT NULL,
  `TotalCost` int(11) NOT NULL,
  `TotalOrder` int(11) NOT NULL,
  `ORDERTYPE` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `Received` int(11) NOT NULL,
  `Change` int(11) NOT NULL,
  `TimeStamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `IsActive` int(11) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `ts_transection`
--

INSERT INTO `ts_transection` (`ID`, `BUSINESS_ID`, `CUSTOMER_ID`, `ORDER_NAME`, `CATEGORYPAYMENT`, `SELLER_ID`, `TotalCost`, `TotalOrder`, `ORDERTYPE`, `Received`, `Change`, `TimeStamp`, `IsActive`) VALUES
(69, 18, 0, '201589112967551', 'เงินสด', 20, 21, 50, 'To Stay', 100, 50, '2020-05-08 11:45:14', 1),
(70, 18, 0, '201589112979380', 'เงินสด', 20, 40, 140, 'To Stay', 200, 60, '2020-05-08 11:45:15', 1),
(71, 18, 0, '201589112987704', 'พร้อมเพย์', 20, 25, 130, 'Delivery', 130, 0, '2020-05-10 11:45:02', 1),
(72, 18, 0, '201589112994092', 'พร้อมเพย์', 20, 31, 60, 'Delivery', 60, 0, '2020-05-10 11:44:59', 1),
(73, 18, 0, '201589113003433', 'เงินสด', 20, 46, 88, 'To Stay', 1000, 912, '2020-05-11 11:45:17', 1),
(74, 18, 0, '201589113011041', 'เงินสด', 20, 37, 80, 'To Stay', 500, 420, '2020-05-11 11:45:18', 1),
(75, 18, 0, '201589113020458', 'พร้อมเพย์', 20, 37, 145, 'To Stay', 145, 0, '2020-05-09 11:45:21', 1),
(76, 18, 0, '201589113026747', 'เงินสด', 20, 85, 236, 'Delivery', 500, 264, '2020-05-09 11:44:57', 1),
(77, 18, 0, '201589113032653', 'พร้อมเพย์', 20, 56, 210, 'To Stay', 210, 0, '2020-05-11 11:45:22', 1),
(78, 18, 0, '201589113037746', 'พร้อมเพย์', 20, 52, 210, 'To Stay', 210, 0, '2020-05-11 11:45:26', 1),
(79, 18, 0, '201589113042899', 'พร้อมเพย์', 20, 35, 150, 'To Stay', 150, 0, '2020-05-07 11:45:24', 0),
(80, 18, 0, '201589113050728', 'เงินสด', 20, 67, 240, 'Delivery', 1000, 760, '2020-05-11 11:44:56', 1),
(81, 18, 0, '201589113066654', 'เงินสด', 20, 30, 50, 'To Stay', 100, 50, '2020-05-05 11:45:28', 1),
(82, 18, 0, '201589113073264', 'เงินสด', 20, 41, 200, 'To Stay', 200, 0, '2020-05-11 11:45:30', 1),
(83, 18, 0, '201589113080326', 'เงินสด', 20, 40, 20, 'Delivery', 200, 180, '2020-05-04 11:44:54', 1),
(84, 18, 0, '201589113080641', 'เงินสด', 20, 40, 20, 'Take Away', 200, 180, '2020-05-11 11:44:47', 1),
(85, 18, 0, '201589113686966', 'เงินสด', 20, 12, 10, 'Take Away', 100, 90, '2020-05-04 11:44:44', 1),
(86, 18, 0, '201589113694843', 'พร้อมเพย์', 20, 30, 56, 'Take Away', 56, 0, '2020-05-11 11:44:41', 1),
(87, 18, 0, '201589119696829', 'เงินสด', 20, 12, 100, 'Take Away', 500, 400, '2020-05-03 11:44:38', 1),
(88, 18, 0, '201589197469323', 'เงินสด', 20, 16, 36, 'Take Away', 100, 64, '2020-05-02 11:44:29', 1),
(89, 18, 0, '201589197628001', 'เงินสด', 20, 26, 40, 'To Stay', 100, 60, '2020-05-11 11:47:08', 1),
(90, 18, 0, '201589197640300', 'พร้อมเพย์', 20, 10, 20, 'To Stay', 20, 0, '2020-05-01 11:47:20', 1),
(91, 18, 3, '201589197805695', 'พร้อมเพย์', 20, 6, 15, 'To Stay', 15, 0, '2020-04-30 11:50:05', 1),
(92, 18, 0, '201589198250142', 'เงินสด', 20, 16, 35, 'Delivery', 100, 65, '2020-05-11 11:57:30', 1),
(93, 18, 3, '201589198283230', 'เงินสด', 20, 40, 75, 'Take Away', 100, 25, '2020-05-01 11:58:03', 1),
(94, 18, 0, '201589198582407', 'พร้อมเพย์', 20, 10, 15, 'Take Away', 15, 0, '2020-05-11 12:03:02', 1),
(95, 18, 3, '201589198601522', 'เงินสด', 20, 16, 30, 'Delivery', 100, 70, '2020-05-11 12:03:21', 1),
(96, 18, 3, '201589198638729', 'เงินสด', 20, 11, 20, 'To Stay', 100, 80, '2020-05-11 12:03:59', 1),
(97, 18, 1, '201589199045648', 'พร้อมเพย์', 20, 2, 10, 'To Stay', 10, 0, '2020-05-11 12:10:45', 1),
(98, 18, 0, '201589199659922', 'เงินสด', 20, 30, 125, 'To Stay', 200, 75, '2020-05-11 12:21:00', 1),
(99, 18, 2, '201589199682940', 'เงินสด', 20, 20, 45, 'To Stay', 100, 55, '2020-05-11 12:21:23', 1),
(100, 18, 0, '201589282060845', 'พร้อมเพย์', 20, 11, 20, 'Take Away', 20, 0, '2020-05-12 11:14:20', 1),
(101, 18, 3, '201589282091458', 'เงินสด', 20, 25, 45, 'To Stay', 100, 55, '2020-05-12 11:14:51', 1),
(102, 18, 0, '201589379929141', 'เงินสด', 20, 25, 45, 'Take Away', 100, 55, '2020-05-13 14:25:29', 1),
(103, 18, 0, '201589379942781', 'พร้อมเพย์', 20, 35, 55, 'Take Away', 55, 0, '2020-05-13 14:25:42', 1);

-- --------------------------------------------------------

--
-- Table structure for table `ts_transection_detail`
--

CREATE TABLE `ts_transection_detail` (
  `ID` int(11) NOT NULL,
  `TRANSECTION_ID` int(11) NOT NULL,
  `GOODS_ID` int(11) NOT NULL,
  `PRICE` int(11) NOT NULL,
  `COST` int(11) NOT NULL,
  `AMOUNT` int(11) NOT NULL,
  `IsActive` int(11) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `ts_transection_detail`
--

INSERT INTO `ts_transection_detail` (`ID`, `TRANSECTION_ID`, `GOODS_ID`, `PRICE`, `COST`, `AMOUNT`, `IsActive`) VALUES
(207, 69, 1, 30, 10, 0, 1),
(208, 69, 2, 5, 1, 0, 1),
(209, 69, 3, 15, 10, 0, 1),
(210, 70, 3, 15, 10, 0, 1),
(211, 70, 4, 100, 10, 0, 1),
(212, 70, 5, 60, 20, 0, 1),
(213, 71, 4, 100, 10, 0, 1),
(214, 71, 9, 20, 10, 0, 1),
(215, 71, 8, 10, 5, 0, 1),
(216, 72, 3, 15, 10, 0, 1),
(217, 72, 2, 5, 1, 0, 1),
(218, 72, 1, 30, 10, 0, 1),
(219, 72, 9, 20, 10, 0, 1),
(220, 73, 3, 15, 10, 0, 1),
(221, 73, 2, 5, 1, 0, 1),
(222, 73, 5, 60, 20, 0, 1),
(223, 73, 9, 20, 10, 0, 1),
(224, 73, 8, 10, 5, 0, 1),
(225, 74, 2, 5, 1, 0, 1),
(226, 74, 2, 5, 1, 0, 1),
(227, 74, 5, 60, 20, 0, 1),
(228, 74, 9, 20, 10, 0, 1),
(229, 74, 8, 10, 5, 0, 1),
(230, 75, 4, 100, 10, 0, 1),
(231, 75, 3, 15, 10, 0, 1),
(232, 75, 2, 5, 1, 0, 1),
(233, 75, 2, 5, 1, 0, 1),
(234, 75, 8, 10, 5, 0, 1),
(235, 75, 9, 20, 10, 0, 1),
(236, 76, 4, 100, 10, 0, 1),
(237, 76, 3, 15, 10, 0, 1),
(238, 76, 1, 30, 10, 0, 1),
(239, 76, 1, 30, 10, 0, 1),
(240, 76, 5, 60, 20, 0, 1),
(241, 76, 8, 10, 5, 0, 1),
(242, 76, 7, 50, 20, 0, 1),
(243, 77, 3, 15, 10, 0, 1),
(244, 77, 5, 60, 20, 0, 1),
(245, 77, 4, 100, 10, 0, 1),
(246, 77, 2, 5, 1, 0, 1),
(247, 77, 8, 10, 5, 0, 1),
(248, 77, 9, 20, 10, 0, 1),
(249, 78, 2, 5, 1, 0, 1),
(250, 78, 2, 5, 1, 0, 1),
(251, 78, 4, 100, 10, 0, 1),
(252, 78, 5, 60, 20, 0, 1),
(253, 78, 9, 20, 10, 0, 1),
(254, 78, 9, 20, 10, 0, 1),
(255, 79, 4, 100, 10, 0, 0),
(256, 79, 9, 20, 10, 0, 0),
(257, 79, 8, 10, 5, 0, 0),
(258, 79, 8, 10, 5, 0, 0),
(259, 79, 8, 10, 5, 0, 0),
(260, 80, 2, 5, 1, 0, 1),
(261, 80, 2, 5, 1, 0, 1),
(262, 80, 4, 100, 10, 0, 1),
(263, 80, 9, 20, 10, 0, 1),
(264, 80, 9, 20, 10, 0, 1),
(265, 80, 8, 10, 5, 0, 1),
(266, 80, 7, 50, 20, 0, 1),
(267, 80, 1, 30, 10, 0, 1),
(268, 81, 1, 30, 10, 0, 1),
(269, 81, 4, 100, 10, 0, 1),
(270, 81, 9, 20, 10, 0, 1),
(271, 82, 4, 100, 10, 0, 1),
(272, 82, 4, 100, 10, 0, 1),
(273, 82, 3, 15, 10, 0, 1),
(274, 82, 2, 5, 1, 0, 1),
(275, 82, 1, 30, 10, 0, 1),
(276, 83, 5, 60, 20, 0, 1),
(277, 83, 5, 60, 20, 0, 1),
(278, 84, 5, 60, 20, 0, 1),
(279, 84, 5, 60, 20, 0, 1),
(280, 85, 4, 100, 10, 0, 1),
(281, 85, 2, 5, 1, 0, 1),
(282, 85, 2, 5, 1, 0, 1),
(283, 86, 9, 20, 10, 0, 1),
(284, 86, 7, 50, 20, 0, 1),
(285, 87, 2, 5, 1, 0, 1),
(286, 87, 4, 100, 10, 0, 1),
(287, 87, 2, 5, 1, 0, 1),
(288, 88, 8, 10, 5, 0, 1),
(289, 88, 2, 5, 1, 0, 1),
(290, 88, 1, 30, 10, 0, 1),
(291, 89, 9, 20, 10, 0, 1),
(292, 89, 3, 15, 10, 0, 1),
(293, 89, 2, 5, 1, 0, 1),
(294, 89, 8, 10, 5, 0, 1),
(295, 90, 9, 20, 10, 0, 1),
(296, 91, 2, 5, 1, 0, 1),
(297, 91, 8, 10, 5, 0, 1),
(298, 92, 9, 20, 10, 0, 1),
(299, 92, 8, 10, 5, 0, 1),
(300, 92, 2, 5, 1, 0, 1),
(301, 93, 9, 20, 10, 0, 1),
(302, 93, 3, 15, 10, 0, 1),
(303, 93, 9, 20, 10, 0, 1),
(304, 93, 9, 20, 10, 0, 1),
(305, 94, 3, 15, 10, 0, 1),
(306, 95, 8, 10, 5, 0, 1),
(307, 95, 3, 15, 10, 0, 1),
(308, 95, 2, 5, 1, 0, 1),
(309, 96, 3, 15, 10, 0, 1),
(310, 96, 2, 5, 1, 0, 1),
(311, 97, 2, 5, 1, 0, 1),
(312, 97, 2, 5, 1, 0, 1),
(313, 98, 4, 100, 10, 0, 1),
(314, 98, 3, 15, 10, 0, 1),
(315, 98, 9, 20, 10, 0, 1),
(316, 99, 1, 30, 10, 0, 1),
(317, 99, 3, 15, 10, 0, 1),
(318, 100, 9, 20, 10, 0, 1),
(319, 100, 2, 5, 1, 0, 1),
(320, 101, 8, 10, 5, 0, 1),
(321, 101, 9, 20, 10, 0, 1),
(322, 101, 3, 15, 10, 0, 1),
(323, 102, 3, 15, 10, 0, 1),
(324, 102, 9, 20, 10, 0, 1),
(325, 102, 8, 10, 5, 0, 1),
(326, 103, 3, 15, 10, 0, 1),
(327, 103, 3, 15, 10, 0, 1),
(328, 103, 9, 20, 10, 0, 1),
(329, 103, 8, 10, 5, 0, 1);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `ma_branch`
--
ALTER TABLE `ma_branch`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `ma_business`
--
ALTER TABLE `ma_business`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `ma_business_category`
--
ALTER TABLE `ma_business_category`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `ma_business_type`
--
ALTER TABLE `ma_business_type`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `ma_customer`
--
ALTER TABLE `ma_customer`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `ma_customer_category`
--
ALTER TABLE `ma_customer_category`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `ma_customer_relation`
--
ALTER TABLE `ma_customer_relation`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `ma_discount`
--
ALTER TABLE `ma_discount`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `ma_goods`
--
ALTER TABLE `ma_goods`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `ma_goods_category`
--
ALTER TABLE `ma_goods_category`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `ma_permission`
--
ALTER TABLE `ma_permission`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `ma_role`
--
ALTER TABLE `ma_role`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `ma_user`
--
ALTER TABLE `ma_user`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `ma_user_position`
--
ALTER TABLE `ma_user_position`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `report_refund`
--
ALTER TABLE `report_refund`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `temp_order`
--
ALTER TABLE `temp_order`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `temp_order_detail`
--
ALTER TABLE `temp_order_detail`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `temp_order_discount`
--
ALTER TABLE `temp_order_discount`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `ts_discount`
--
ALTER TABLE `ts_discount`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `ts_pointreward`
--
ALTER TABLE `ts_pointreward`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `ts_transection`
--
ALTER TABLE `ts_transection`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `ts_transection_detail`
--
ALTER TABLE `ts_transection_detail`
  ADD PRIMARY KEY (`ID`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `ma_branch`
--
ALTER TABLE `ma_branch`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=30;
--
-- AUTO_INCREMENT for table `ma_business`
--
ALTER TABLE `ma_business`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=25;
--
-- AUTO_INCREMENT for table `ma_business_category`
--
ALTER TABLE `ma_business_category`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `ma_business_type`
--
ALTER TABLE `ma_business_type`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `ma_customer`
--
ALTER TABLE `ma_customer`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `ma_customer_category`
--
ALTER TABLE `ma_customer_category`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `ma_customer_relation`
--
ALTER TABLE `ma_customer_relation`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `ma_discount`
--
ALTER TABLE `ma_discount`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;
--
-- AUTO_INCREMENT for table `ma_goods`
--
ALTER TABLE `ma_goods`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;
--
-- AUTO_INCREMENT for table `ma_goods_category`
--
ALTER TABLE `ma_goods_category`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `ma_permission`
--
ALTER TABLE `ma_permission`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `ma_role`
--
ALTER TABLE `ma_role`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=93;
--
-- AUTO_INCREMENT for table `ma_user`
--
ALTER TABLE `ma_user`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=37;
--
-- AUTO_INCREMENT for table `ma_user_position`
--
ALTER TABLE `ma_user_position`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;
--
-- AUTO_INCREMENT for table `report_refund`
--
ALTER TABLE `report_refund`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `temp_order`
--
ALTER TABLE `temp_order`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `temp_order_detail`
--
ALTER TABLE `temp_order_detail`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `temp_order_discount`
--
ALTER TABLE `temp_order_discount`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `ts_discount`
--
ALTER TABLE `ts_discount`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=80;
--
-- AUTO_INCREMENT for table `ts_pointreward`
--
ALTER TABLE `ts_pointreward`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `ts_transection`
--
ALTER TABLE `ts_transection`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=104;
--
-- AUTO_INCREMENT for table `ts_transection_detail`
--
ALTER TABLE `ts_transection_detail`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=330;COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
