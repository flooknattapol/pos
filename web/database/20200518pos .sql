-- phpMyAdmin SQL Dump
-- version 4.7.0
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: May 18, 2020 at 08:51 AM
-- Server version: 5.6.34-log
-- PHP Version: 7.1.5

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `pos`
--

-- --------------------------------------------------------

--
-- Table structure for table `ma_branch`
--

CREATE TABLE `ma_branch` (
  `ID` int(11) NOT NULL,
  `line_no` int(11) NOT NULL,
  `BUSINESS_ID` int(11) NOT NULL,
  `UID` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `EMAIL` varchar(300) COLLATE utf8_unicode_ci NOT NULL,
  `NAME` varchar(300) COLLATE utf8_unicode_ci NOT NULL,
  `ADDRESS` varchar(300) COLLATE utf8_unicode_ci NOT NULL,
  `TEL` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `CreateDate` datetime NOT NULL,
  `IsActive` int(11) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `ma_branch`
--

INSERT INTO `ma_branch` (`ID`, `line_no`, `BUSINESS_ID`, `UID`, `EMAIL`, `NAME`, `ADDRESS`, `TEL`, `CreateDate`, `IsActive`) VALUES
(20, 0, 18, '877fdd7d875265e4d413f869573e5d36', 'f@gmail.com', 'สำนักงานใหญ่', 'siam', '02', '1970-01-01 00:00:00', 1),
(30, 0, 25, 'e121a7968eab924115c3439ba0f7bb54', 'shop@acsp.com', 'สำนักงานใหญ่', '419/1389 หมู่5 ถ.เทพารักษ์ ต.เทพารักษ์ อ.เมือง จ.สมุทรปราการ 10270', '0851330199', '0000-00-00 00:00:00', 1),
(31, 0, 26, '43bc965bd344ff5bf94c36c0cb9c3bf8', 'shop@devdeethailand.com', 'สำนักงานใหญ่', 'Test', '0851330199', '0000-00-00 00:00:00', 1),
(35, 0, 32, 'ef3987269e65c90038104f7cbe183a50', 'acsp@acsp.com', 'สำนักงานใหญ่', '12', 'โรงเรียน', '0000-00-00 00:00:00', 1);

-- --------------------------------------------------------

--
-- Table structure for table `ma_business`
--

CREATE TABLE `ma_business` (
  `ID` int(11) NOT NULL,
  `UID` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `EMAIL` varchar(300) COLLATE utf8_unicode_ci NOT NULL,
  `NAME` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `TEL` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `IMAGE` varchar(300) COLLATE utf8_unicode_ci NOT NULL,
  `CONTACT` varchar(2000) COLLATE utf8_unicode_ci NOT NULL,
  `CATEGORY_ID` int(11) NOT NULL,
  `TYPE_ID` int(11) NOT NULL,
  `TAXID10` varchar(10) COLLATE utf8_unicode_ci NOT NULL,
  `TAXID13` varchar(13) COLLATE utf8_unicode_ci NOT NULL,
  `CreateDate` datetime NOT NULL,
  `IsActive` int(11) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `ma_business`
--

INSERT INTO `ma_business` (`ID`, `UID`, `EMAIL`, `NAME`, `TEL`, `IMAGE`, `CONTACT`, `CATEGORY_ID`, `TYPE_ID`, `TAXID10`, `TAXID13`, `CreateDate`, `IsActive`) VALUES
(18, '8a5c2418e2c9b0bca99075d6ef84647c', 'f@gmail.com', 'Demo Shop', '0628842544', 'imgBusiness_2020-04-23-10-44-53.png', 'me', 1, 2, '', '', '0000-00-00 00:00:00', 1),
(25, '249f9a3e1ef4f4113a4ba293d5915f8d', 'shop@acsp.com', 'ACSP shop', '0851330199', 'imgBusiness_2020-05-14-10-22-11.png', 'ome', 1, 1, '', '', '0000-00-00 00:00:00', 1),
(26, '9707ab8643e14011c02bd3434d782b9a', 'shop@devdeethailand.com', 'devdee shop', '0851330199', 'imgBusiness_2020-05-14-12-23-21.png', 'ฟลุ๊ค', 1, 1, '', '', '0000-00-00 00:00:00', 1),
(32, '46df640b78cf790c8c9f9332d5b62feb', 'acsp@acsp.com', 'ACSP KITCHEN', '0861658473', '', 'แบงค์', 1, 1, '-', '-', '0000-00-00 00:00:00', 1);

-- --------------------------------------------------------

--
-- Table structure for table `ma_business_category`
--

CREATE TABLE `ma_business_category` (
  `ID` int(11) NOT NULL,
  `NAME` varchar(300) COLLATE utf8_unicode_ci NOT NULL,
  `DETAIL` varchar(300) COLLATE utf8_unicode_ci NOT NULL,
  `IsActive` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `ma_business_category`
--

INSERT INTO `ma_business_category` (`ID`, `NAME`, `DETAIL`, `IsActive`) VALUES
(1, 'ทั่วไป', '', 1);

-- --------------------------------------------------------

--
-- Table structure for table `ma_business_type`
--

CREATE TABLE `ma_business_type` (
  `ID` int(11) NOT NULL,
  `NAME` varchar(300) COLLATE utf8_unicode_ci NOT NULL,
  `DETAIL` varchar(300) COLLATE utf8_unicode_ci NOT NULL,
  `IsActive` int(11) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `ma_business_type`
--

INSERT INTO `ma_business_type` (`ID`, `NAME`, `DETAIL`, `IsActive`) VALUES
(1, 'ร้านอาหาร', 'ขายอาหารทั่วไป', 1),
(2, 'ร้าน mini mart', 'ขายของทั่วไป', 1),
(3, 'ร้านขายเกมส์', '', 1);

-- --------------------------------------------------------

--
-- Table structure for table `ma_customer`
--

CREATE TABLE `ma_customer` (
  `ID` int(11) NOT NULL,
  `NAME` varchar(300) COLLATE utf8_unicode_ci NOT NULL,
  `TEL` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `userID` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `BIRTHDAY` date NOT NULL,
  `EMAIL` varchar(300) COLLATE utf8_unicode_ci NOT NULL,
  `CATEGORY_ID` int(11) NOT NULL,
  `AMOUNTBUY` int(11) NOT NULL,
  `REWARDPOINT` int(11) NOT NULL,
  `NOTE` varchar(300) COLLATE utf8_unicode_ci NOT NULL,
  `IsActive` int(11) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `ma_customer`
--

INSERT INTO `ma_customer` (`ID`, `NAME`, `TEL`, `userID`, `BIRTHDAY`, `EMAIL`, `CATEGORY_ID`, `AMOUNTBUY`, `REWARDPOINT`, `NOTE`, `IsActive`) VALUES
(1, 'ลูกค้า A', '01', '', '0000-00-00', 'f@gmail.com', 1, 0, 233, '', 1),
(2, 'นาย b', '02', '', '0000-00-00', 'bb@gmail.com', 1, 0, 45, '', 1),
(3, 'นาย C', '03', '', '0000-00-00', 'ccc@gmad.com', 1, 0, 45, '', 1),
(4, 'นาย D', '04', '', '0000-00-00', 'ddd@gmad.com', 1, 0, 0, '', 1),
(5, 'นาย F', '05', '', '0000-00-00', 'fff@gmad.com', 1, 0, 0, '', 1),
(6, 'นาย E', '06', '', '0000-00-00', 'eee@gmad.com', 1, 0, 0, '', 1);

-- --------------------------------------------------------

--
-- Table structure for table `ma_customer_category`
--

CREATE TABLE `ma_customer_category` (
  `ID` int(11) NOT NULL,
  `NAME` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `DETAIL` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `IsActive` int(11) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `ma_customer_category`
--

INSERT INTO `ma_customer_category` (`ID`, `NAME`, `DETAIL`, `IsActive`) VALUES
(1, 'ทั่วไป', '', 1),
(2, 'พิเศษ', '', 1);

-- --------------------------------------------------------

--
-- Table structure for table `ma_customer_relation`
--

CREATE TABLE `ma_customer_relation` (
  `ID` int(11) NOT NULL,
  `CUSTOMER_ID` int(11) NOT NULL,
  `BUSINESS_ID` int(11) NOT NULL,
  `IsActive` int(11) DEFAULT '1',
  `TimeStamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `ma_customer_relation`
--

INSERT INTO `ma_customer_relation` (`ID`, `CUSTOMER_ID`, `BUSINESS_ID`, `IsActive`, `TimeStamp`) VALUES
(9, 1, 18, 1, '2020-05-13 19:35:59'),
(10, 2, 18, 1, '2020-05-13 19:36:24'),
(11, 2, 25, 1, '2020-05-14 03:54:36'),
(12, 1, 25, 1, '2020-05-14 03:54:54');

-- --------------------------------------------------------

--
-- Table structure for table `ma_discount`
--

CREATE TABLE `ma_discount` (
  `ID` int(11) NOT NULL,
  `UID` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `BARCODE` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `NAME` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `DETAIL` varchar(300) COLLATE utf8_unicode_ci NOT NULL,
  `DISCOUNT` int(11) NOT NULL,
  `BAHTORPERCENT` int(11) NOT NULL,
  `BUSINESS_ID` int(11) NOT NULL,
  `EXPIRATIONDATE` date NOT NULL,
  `IsActive` int(11) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `ma_discount`
--

INSERT INTO `ma_discount` (`ID`, `UID`, `BARCODE`, `NAME`, `DETAIL`, `DISCOUNT`, `BAHTORPERCENT`, `BUSINESS_ID`, `EXPIRATIONDATE`, `IsActive`) VALUES
(1, '1115889357911', '15889357911115889357911.png', 'วันเกิด', '', 100, 0, 18, '0000-00-00', 1),
(2, '1115889362232', '15889362231115889362232.png', 'วันปีใหม่', '', 20, 1, 18, '0000-00-00', 1),
(10, '11158893578510', '158893578511158893578510.png', 'วันสงกานต์', '', 10, 0, 18, '0000-00-00', 1),
(11, '11158925596311', '158925596311158925596311.png', 'ส่วนลดพิเศษ', '', 5, 0, 18, '0000-00-00', 1),
(12, '11158952219312', '158952219311158952219312.png', 'ลด​ 10​ บาท', '', 10, 0, 32, '0000-00-00', 1),
(13, '11158952220913', '158952220911158952220913.png', 'ลด​ 5​ บาท', '', 5, 0, 32, '0000-00-00', 1),
(14, '11158952222114', '158952222111158952222114.png', 'ลด​ 15​ บาท', '', 15, 0, 32, '0000-00-00', 1),
(15, '11158952223215', '158952223211158952223215.png', 'ลด​ 20​ บาท', '', 20, 0, 32, '0000-00-00', 1);

-- --------------------------------------------------------

--
-- Table structure for table `ma_goods`
--

CREATE TABLE `ma_goods` (
  `ID` int(11) NOT NULL,
  `UID` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `NAME` varchar(300) COLLATE utf8_unicode_ci NOT NULL,
  `IMAGE` varchar(300) COLLATE utf8_unicode_ci NOT NULL,
  `DETAIL` varchar(300) COLLATE utf8_unicode_ci NOT NULL,
  `STATUS` tinyint(1) DEFAULT '1',
  `CATEGORY_ID` int(11) NOT NULL,
  `SOLDBY` int(11) NOT NULL,
  `PRICE` int(11) NOT NULL,
  `COST` int(11) NOT NULL,
  `SKU` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `BARCODE` varchar(300) COLLATE utf8_unicode_ci NOT NULL,
  `COMPOSITE` varchar(300) COLLATE utf8_unicode_ci NOT NULL,
  `STOCK` int(11) NOT NULL,
  `BUSINESS_ID` int(11) NOT NULL,
  `IsActive` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `ma_goods`
--

INSERT INTO `ma_goods` (`ID`, `UID`, `NAME`, `IMAGE`, `DETAIL`, `STATUS`, `CATEGORY_ID`, `SOLDBY`, `PRICE`, `COST`, `SKU`, `BARCODE`, `COMPOSITE`, `STOCK`, `BUSINESS_ID`, `IsActive`) VALUES
(1, '8815897138801', 'น้ำส้มปั่น22', 'imgGoods_2020-04-29-16-57-27.png', '', 1, 2, 0, 30, 10, '101', '15897138808815897138801.png', '', 0, 18, 1),
(2, '8815889298502', 'ขนมไทย1', 'imgGoods_2020-04-29-16-57-27.png', '', 1, 3, 0, 5, 1, '102', '15889298508815889298502.png', '', 0, 18, 1),
(3, '8815888352623', 'น้ำมะนาว', 'imgGoods_2020-04-29-16-57-31.png', '', 1, 2, 0, 15, 10, '10', '15888352628815888352623.png', '', 0, 18, 1),
(4, '8815897150644', 'น้ำเขียว', 'imgGoods_2020-05-03-12-52-20.png', '', 1, 3, 0, 100, 10, '11', '15897150648815897150644.png', '', 0, 18, 1),
(5, '8815888352655', 'น้ำปีโป้นมสด', 'imgGoods_2020-05-03-12-52-43.png', '', 1, 1, 0, 60, 20, '111', '15888352658815888352655.png', '', 0, 18, 1),
(6, '8815888352666', 'น้ำแดงมะนาว', 'imgGoods_2020-05-05-14-22-19.png', '', 0, 2, 0, 30, 20, '100', '15888352668815888352666.png', '', 0, 18, 1),
(7, '8815888352687', 'น้ำเขียวมะนาว', 'imgGoods_2020-05-05-13-38-30.png', '', 1, 2, 0, 50, 20, '111', '15888352688815888352687.png', '', 0, 18, 1),
(8, '8815888352698', 'น้ำส้มมะนาว', 'imgGoods_2020-05-05-13-40-56.png', '', 1, 2, 0, 10, 5, '111', '15888352698815888352698.png', '', 0, 18, 1),
(9, '8815888352699', 'น้ำปั๊นปั่น', 'imgGoods_2020-05-05-13-43-12.png', '', 1, 2, 0, 20, 10, '200', '15888352698815888352699.png', '', 0, 18, 1),
(10, '88158952162410', 'สินค้าตัวอย่าง', 'goods1589428408346.jpg', '', 1, 5, 0, 150, 50, '-', '158952162488158952162410.png', '', 0, 25, 1),
(11, '88158951889311', 'กุนเชียง', 'goods1589518891959.jpg', '', 1, 5, 0, 250, 0, '0', '158951889388158951889311.png', '', 0, 25, 1),
(12, '88158953422012', 'กับข้าว', 'goods1589520967068.jpg', '', 1, 1, 0, 40, 0, '11', '158953422088158953422012.png', '', 0, 18, 1),
(13, '88158952209813', 'กับข้าว', 'goods1589522096498.jpg', '', 1, 15, 0, 40, 0, '0', '158952209888158952209813.png', '', 0, 32, 1),
(14, '88158952212914', 'ไก่ทอด', 'goods1589522127543.jpg', '', 1, 16, 0, 40, 0, '0', '158952212988158952212914.png', '', 0, 32, 1),
(15, '88158952214915', 'น้ำส้ม', 'goods1589522148281.jpg', '', 1, 17, 0, 20, 0, '0', '158952214988158952214915.png', '', 0, 32, 1),
(16, '88158952404216', 'หมี่กรอบ รสสมุนไพร', 'goods1589524041316.jpg', '', 1, 5, 0, 40, 30, '0', '158952404288158952404216.png', '', 0, 25, 1),
(17, '88158952407817', 'หมี่กรอบ รสมันกุ้ง', 'goods1589524076988.jpg', '', 1, 5, 0, 40, 30, '0', '158952407888158952407817.png', '', 0, 25, 1),
(18, '88158952413018', 'หมี่กรอบ รสโบราณ', 'goods1589524129372.jpg', '', 1, 5, 0, 40, 30, '0', '158952413088158952413018.png', '', 0, 25, 1);

-- --------------------------------------------------------

--
-- Table structure for table `ma_goods_category`
--

CREATE TABLE `ma_goods_category` (
  `ID` int(11) NOT NULL,
  `BUSINESS_ID` int(11) NOT NULL,
  `NAME` varchar(300) COLLATE utf8_unicode_ci NOT NULL,
  `DETAIL` varchar(300) COLLATE utf8_unicode_ci NOT NULL,
  `COLOR` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `IsActive` int(11) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `ma_goods_category`
--

INSERT INTO `ma_goods_category` (`ID`, `BUSINESS_ID`, `NAME`, `DETAIL`, `COLOR`, `IsActive`) VALUES
(1, 18, 'อาหาร', 'อาหารทั่ว ๆไป', '', 1),
(2, 18, 'น้ำ', 'น้ำทั่วๆไป', '', 1),
(3, 18, 'ขนม', '', '', 1),
(4, 18, 'ขนมปัง', 'test', '', 1),
(5, 25, 'อาหาร', '', '', 1),
(6, 26, 'ทั่วไป', '', '', 1),
(7, 27, 'ทั่วไป', '', '', 1),
(8, 28, 'ทั่วไป', '', '', 1),
(9, 29, 'ทั่วไป', '', '', 1),
(10, 30, 'ทั่วไป', '', '', 1),
(11, 31, 'ทั่วไป', '', '', 1),
(12, 25, 'ของใช้', '', '', 1),
(13, 25, 'ค่าจัดส่ง', '', '', 1),
(14, 32, 'ทั่วไป', '', '', 1),
(15, 32, 'กับข้าว', '', '', 1),
(16, 32, 'ไก่ทอด', '', '', 1),
(17, 32, 'น้ำส้ม', '', '', 1);

-- --------------------------------------------------------

--
-- Table structure for table `ma_permission`
--

CREATE TABLE `ma_permission` (
  `ID` int(11) NOT NULL,
  `NAME` varchar(300) COLLATE utf8_unicode_ci NOT NULL,
  `DETAIL` varchar(300) COLLATE utf8_unicode_ci NOT NULL,
  `TYPE` int(11) NOT NULL,
  `IsActive` int(11) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `ma_role`
--

CREATE TABLE `ma_role` (
  `ID` int(11) NOT NULL,
  `NAME` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `BUSINESS_ID` int(11) NOT NULL,
  `PERMISSION_DASHBOARD_IsActive` int(11) NOT NULL DEFAULT '0',
  `PERMISSION_POS_IsActive` int(11) NOT NULL DEFAULT '0',
  `PERMISSION_POS` varchar(300) COLLATE utf8_unicode_ci NOT NULL,
  `PERMISSION_DASHBOARD` varchar(300) COLLATE utf8_unicode_ci NOT NULL,
  `MASTER` int(11) NOT NULL DEFAULT '0',
  `IsActive` int(11) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `ma_role`
--

INSERT INTO `ma_role` (`ID`, `NAME`, `BUSINESS_ID`, `PERMISSION_DASHBOARD_IsActive`, `PERMISSION_POS_IsActive`, `PERMISSION_POS`, `PERMISSION_DASHBOARD`, `MASTER`, `IsActive`) VALUES
(64, 'เจ้าของ', 18, 1, 1, '', '', 1, 1),
(66, 'ผู้จัดการ', 18, 1, 1, '', '', 0, 1),
(67, 'แคชเชียร์', 18, 0, 1, '', '', 0, 1),
(68, 'เจ้าของ', 19, 1, 1, '', '', 1, 1),
(70, 'ผู้จัดการ', 19, 1, 1, '', '', 0, 1),
(71, 'แคชเชียร์', 19, 0, 1, '', '', 0, 1),
(72, 'เจ้าของ', 20, 1, 1, '', '', 1, 1),
(74, 'ผู้จัดการ', 20, 1, 1, '', '', 0, 1),
(75, 'แคชเชียร์', 20, 0, 1, '', '', 0, 1),
(77, 'เจ้าของ', 21, 1, 1, '', '', 1, 1),
(79, 'ผู้จัดการ', 21, 1, 1, '', '', 0, 1),
(80, 'แคชเชียร์', 21, 0, 1, '', '', 0, 1),
(81, 'เจ้าของ', 22, 1, 1, '', '', 1, 1),
(83, 'ผู้จัดการ', 22, 1, 1, '', '', 0, 1),
(84, 'แคชเชียร์', 22, 0, 1, '', '', 0, 1),
(85, 'เจ้าของ', 23, 1, 1, '', '', 1, 1),
(87, 'ผู้จัดการ', 23, 1, 1, '', '', 0, 1),
(88, 'แคชเชียร์', 23, 0, 1, '', '', 0, 1),
(89, 'เจ้าของ', 24, 1, 1, '', '', 1, 1),
(91, 'ผู้จัดการ', 24, 1, 1, '', '', 0, 1),
(92, 'แคชเชียร์', 24, 0, 1, '', '', 0, 1),
(93, 'เจ้าของ', 25, 1, 1, '', '', 1, 1),
(94, 'แคชเชียร์', 25, 1, 1, '', '', 0, 1),
(95, 'ผู้จัดการ', 25, 1, 1, '', '', 0, 1),
(96, 'เจ้าของ', 26, 1, 1, '', '', 1, 1),
(97, 'แคชเชียร์', 26, 1, 1, '', '', 0, 1),
(98, 'ผู้จัดการ', 26, 1, 1, '', '', 0, 1),
(102, 'เจ้าของ', 28, 1, 1, '', '', 1, 1),
(103, 'แคชเชียร์', 28, 1, 1, '', '', 0, 1),
(104, 'ผู้จัดการ', 28, 1, 1, '', '', 0, 1),
(105, 'เจ้าของ', 29, 1, 1, '', '', 1, 1),
(106, 'แคชเชียร์', 29, 1, 1, '', '', 0, 1),
(107, 'ผู้จัดการ', 29, 1, 1, '', '', 0, 1),
(108, 'เจ้าของ', 30, 1, 1, '', '', 1, 1),
(109, 'แคชเชียร์', 30, 1, 1, '', '', 0, 1),
(110, 'ผู้จัดการ', 30, 1, 1, '', '', 0, 1),
(111, 'เจ้าของ', 31, 1, 1, '', '', 1, 1),
(112, 'แคชเชียร์', 31, 1, 1, '', '', 0, 1),
(113, 'ผู้จัดการ', 31, 1, 1, '', '', 0, 1),
(114, 'เจ้าของ', 32, 1, 1, '', '', 1, 1),
(115, 'แคชเชียร์', 32, 1, 1, '', '', 0, 1),
(116, 'ผู้จัดการ', 32, 1, 1, '', '', 0, 1);

-- --------------------------------------------------------

--
-- Table structure for table `ma_user`
--

CREATE TABLE `ma_user` (
  `ID` int(11) NOT NULL,
  `EMAIL` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `PASSWORD` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `NAME` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `TEL` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `IMAGE` varchar(300) COLLATE utf8_unicode_ci NOT NULL,
  `IsActive` int(11) DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `ma_user`
--

INSERT INTO `ma_user` (`ID`, `EMAIL`, `PASSWORD`, `NAME`, `TEL`, `IMAGE`, `IsActive`) VALUES
(9, 'admin@pos.com', '202cb962ac59075b964b07152d234b70', 'admin', '', '', 1),
(20, 'f@gmail.com', '202cb962ac59075b964b07152d234b70', 'เจ้าของ', '034', 'imgUser_2020-04-23-17-56-14.png', 1),
(21, 'game@gmail.com', '202cb962ac59075b964b07152d234b70', 'เจ้าของ', '', '', 1),
(22, 'japan@gmail.com', '202cb962ac59075b964b07152d234b70', 'เจ้าของ', '', '', 1),
(23, 'mon@a.com', '202cb962ac59075b964b07152d234b70', 'เจ้าของ', '', '', 1),
(29, 'ff@gmail.com', '202cb962ac59075b964b07152d234b70', 'นาย A', '02', 'imgUser_2020-04-23-17-56-44.png', 1),
(30, 'fff@gmail.com', '202cb962ac59075b964b07152d234b70', 'นาย b', '02', '', 1),
(31, 'gggg@gmad.com', '202cb962ac59075b964b07152d234b70', 'เจ้าของ', '', '', 1),
(32, 'nom@gmail.com', '202cb962ac59075b964b07152d234b70', '', '', '', 1),
(33, 'na@mail.com', '202cb962ac59075b964b07152d234b70', 'na', '', '', 1),
(34, 'wwfff@gmail.com', '202cb962ac59075b964b07152d234b70', 'นาย b', '02', 'imgUser_2020-04-23-17-54-34.png', 1),
(35, 'test@', '202cb962ac59075b964b07152d234b70', 'test', '02', '', 1),
(36, 'dad@', '202cb962ac59075b964b07152d234b70', 'พนักงาน', '02', '', 1),
(37, 'shop@acsp.com', '202cb962ac59075b964b07152d234b70', 'ome', '', '', 1),
(38, 'shop@devdeethailand.com', '202cb962ac59075b964b07152d234b70', 'ฟลุ๊ค', '', '', 1),
(41, 'flook@shop.com', '81dc9bdb52d04dc20036dbd8313ed055', 'flook', '', '', 1),
(42, 'ff@shop.com', '827ccb0eea8a706c4c34a16891f84e7b', 'ff', '', '', 1),
(43, 'flookplayzz@gmail.com', '202cb962ac59075b964b07152d234b70', 'aa', '', '', 1),
(44, 'flooknattapol1@hotmail.com', '202cb962ac59075b964b07152d234b70', 'flook', '02', 'imgUser_2020-05-15-10-52-02.png', 1),
(45, 'akarapon.n@gmail.com', '202cb962ac59075b964b07152d234b70', 'ome', '0851330199', '', 1),
(46, 'acsp@acsp.com', '202cb962ac59075b964b07152d234b70', 'แบงค์', '', '', 1),
(47, 'acsp1@acsp.com', '202cb962ac59075b964b07152d234b70', 'cashier1', '0861658473', '', 1);

-- --------------------------------------------------------

--
-- Table structure for table `ma_user_position`
--

CREATE TABLE `ma_user_position` (
  `ID` int(11) NOT NULL,
  `USER_ID` int(11) NOT NULL,
  `BUSINESS_ID` int(11) NOT NULL,
  `BRANCH_ID` int(11) NOT NULL,
  `ROLE_ID` int(11) NOT NULL,
  `IsActive` int(11) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `ma_user_position`
--

INSERT INTO `ma_user_position` (`ID`, `USER_ID`, `BUSINESS_ID`, `BRANCH_ID`, `ROLE_ID`, `IsActive`) VALUES
(5, 20, 18, 25, 72, 1),
(7, 21, 19, 22, 68, 1),
(9, 22, 20, 25, 72, 1),
(11, 23, 21, 26, 77, 1),
(14, 30, 18, 20, 67, 1),
(15, 31, 22, 27, 81, 1),
(16, 32, 23, 28, 85, 1),
(17, 33, 24, 29, 89, 1),
(18, 34, 18, 23, 66, 1),
(19, 35, 18, 24, 66, 1),
(20, 36, 18, 21, 67, 1),
(21, 37, 25, 30, 93, 1),
(22, 38, 26, 31, 96, 1),
(23, 41, 29, 32, 105, 1),
(24, 42, 30, 33, 108, 1),
(25, 43, 31, 34, 111, 1),
(26, 44, 18, 20, 64, 1),
(27, 45, 18, 20, 67, 1),
(28, 46, 32, 35, 114, 1),
(29, 47, 32, 35, 115, 1);

-- --------------------------------------------------------

--
-- Table structure for table `report_refund`
--

CREATE TABLE `report_refund` (
  `ID` int(11) NOT NULL,
  `TRANSECTION_ID` int(11) NOT NULL,
  `AMOUNT` int(11) NOT NULL,
  `TimeStamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `report_refund`
--

INSERT INTO `report_refund` (`ID`, `TRANSECTION_ID`, `AMOUNT`, `TimeStamp`) VALUES
(1, 1, 30, '2020-05-18 05:50:21'),
(4, 5, 35, '2020-05-18 05:40:13'),
(15, 1, 15, '2020-05-18 05:50:19'),
(16, 1, 15, '2020-05-18 05:50:17'),
(17, 8, 5, '2020-05-18 06:16:22'),
(18, 8, 30, '2020-05-18 06:28:23'),
(19, 11, 100, '2020-05-18 06:44:09');

-- --------------------------------------------------------

--
-- Table structure for table `temp_order`
--

CREATE TABLE `temp_order` (
  `ID` int(11) NOT NULL,
  `BUSINESS_ID` int(11) NOT NULL,
  `SELLER_ID` int(11) NOT NULL,
  `ORDER_NAME` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `TimeStamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `temp_order_detail`
--

CREATE TABLE `temp_order_detail` (
  `ID` int(11) NOT NULL,
  `TRANSECTION_ID` int(11) NOT NULL,
  `GOODS_ID` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `temp_order_discount`
--

CREATE TABLE `temp_order_discount` (
  `ID` int(11) NOT NULL,
  `TRANSECTION_ID` int(11) NOT NULL,
  `DISCOUNT_ID` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `ts_discount`
--

CREATE TABLE `ts_discount` (
  `ID` int(11) NOT NULL,
  `TRANSECTION_ID` int(11) NOT NULL,
  `AMOUNT_DISCOUNT` int(11) NOT NULL,
  `TimeStamp` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `ts_discount`
--

INSERT INTO `ts_discount` (`ID`, `TRANSECTION_ID`, `AMOUNT_DISCOUNT`, `TimeStamp`) VALUES
(1, 1, 0, 0),
(2, 5, 0, 0),
(3, 7, 5, 0),
(4, 8, 0, 0),
(5, 11, 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `ts_pointreward`
--

CREATE TABLE `ts_pointreward` (
  `ID` int(11) NOT NULL,
  `BUSINESS_ID` int(11) NOT NULL,
  `POINTREWARD` int(11) NOT NULL,
  `CUSTOMER_ID` int(11) NOT NULL,
  `TotalOrder` int(11) NOT NULL,
  `TimeStamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `ts_transection`
--

CREATE TABLE `ts_transection` (
  `ID` int(11) NOT NULL,
  `BUSINESS_ID` int(11) NOT NULL,
  `CUSTOMER_ID` int(11) NOT NULL,
  `ORDER_NAME` varchar(300) COLLATE utf8_unicode_ci NOT NULL,
  `CATEGORYPAYMENT` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `SELLER_ID` int(11) NOT NULL,
  `TotalCost` int(11) NOT NULL,
  `TotalOrder` int(11) NOT NULL,
  `ORDERTYPE` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `Received` int(11) NOT NULL,
  `Change` int(11) NOT NULL,
  `TimeStamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `STATUS` int(11) DEFAULT '1',
  `IsActive` int(11) NOT NULL DEFAULT '1',
  `REFERENCE_ORDER_NAME` varchar(255) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `ts_transection`
--

INSERT INTO `ts_transection` (`ID`, `BUSINESS_ID`, `CUSTOMER_ID`, `ORDER_NAME`, `CATEGORYPAYMENT`, `SELLER_ID`, `TotalCost`, `TotalOrder`, `ORDERTYPE`, `Received`, `Change`, `TimeStamp`, `STATUS`, `IsActive`, `REFERENCE_ORDER_NAME`) VALUES
(1, 18, 0, '201589778296942', 'พร้อมเพย์', 20, 30, 60, 'To Stay', 60, 0, '2020-05-18 05:04:57', 1, 1, ''),
(2, 18, 0, '120200518', 'พร้อมเพย์', 20, 10, 30, 'To Stay', 60, 0, '2020-05-18 05:04:57', 0, 0, '201589778296942'),
(3, 18, 0, '220200518', 'พร้อมเพย์', 20, 10, 15, 'To Stay', 60, 0, '2020-05-18 05:04:57', 0, 0, '201589778296942'),
(4, 18, 0, '320200518', 'พร้อมเพย์', 20, 10, 15, 'To Stay', 60, 0, '2020-05-18 05:04:57', 0, 0, '201589778296942'),
(5, 18, 0, '201589778781203', 'พร้อมเพย์', 20, 11, 35, 'To Stay', 35, 0, '2020-05-18 05:13:21', 0, 1, ''),
(6, 18, 0, '520200518', 'พร้อมเพย์', 20, 11, 35, 'To Stay', 35, 0, '2020-05-18 05:13:01', 0, 0, '201589778781203'),
(7, 18, 0, '201589780804722', 'เงินสด', 20, 11, 30, 'To Stay', 100, 70, '2020-05-18 05:46:45', 1, 1, ''),
(8, 18, 0, '201589782319224', 'พร้อมเพย์', 20, 11, 35, 'To Stay', 35, 0, '2020-05-18 06:11:59', 1, 1, ''),
(9, 18, 0, '1420200518', 'พร้อมเพย์', 20, 1, 5, 'To Stay', 35, 0, '2020-05-18 06:11:59', 0, 0, '201589782319224'),
(10, 18, 0, '1320200518', 'พร้อมเพย์', 20, 10, 30, 'To Stay', 35, 0, '2020-05-18 06:11:59', 0, 0, '201589782319224'),
(11, 18, 0, '201589784233342', 'พร้อมเพย์', 20, 10, 100, 'To Stay', 100, 0, '2020-05-18 06:44:09', 0, 1, ''),
(12, 18, 0, '1120200518', 'พร้อมเพย์', 20, 10, 100, 'To Stay', 100, 0, '2020-05-18 06:43:53', 0, 0, '201589784233342');

-- --------------------------------------------------------

--
-- Table structure for table `ts_transection_detail`
--

CREATE TABLE `ts_transection_detail` (
  `ID` int(11) NOT NULL,
  `TRANSECTION_ID` int(11) NOT NULL,
  `GOODS_ID` int(11) NOT NULL,
  `PRICE` int(11) NOT NULL,
  `COST` int(11) NOT NULL,
  `AMOUNT` int(11) NOT NULL,
  `STATUS` int(11) DEFAULT '1',
  `IsActive` int(11) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `ts_transection_detail`
--

INSERT INTO `ts_transection_detail` (`ID`, `TRANSECTION_ID`, `GOODS_ID`, `PRICE`, `COST`, `AMOUNT`, `STATUS`, `IsActive`) VALUES
(1, 1, 1, 30, 10, 0, 0, 1),
(2, 1, 3, 15, 10, 0, 0, 1),
(3, 1, 3, 15, 10, 0, 0, 1),
(4, 2, 1, 30, 10, 0, 0, 0),
(5, 3, 3, 15, 10, 0, 0, 0),
(6, 4, 3, 15, 10, 0, 0, 0),
(7, 5, 1, 30, 10, 0, 0, 1),
(8, 5, 2, 5, 1, 0, 0, 1),
(9, 6, 1, 30, 10, 0, 0, 0),
(10, 6, 2, 5, 1, 0, 0, 0),
(11, 7, 1, 30, 10, 0, 1, 1),
(12, 7, 2, 5, 1, 0, 1, 1),
(13, 8, 1, 30, 10, 0, 0, 1),
(14, 8, 2, 5, 1, 0, 0, 1),
(15, 9, 2, 5, 1, 0, 0, 0),
(16, 10, 1, 30, 10, 0, 0, 0),
(17, 11, 4, 100, 10, 0, 0, 1),
(18, 12, 4, 100, 10, 0, 0, 0);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `ma_branch`
--
ALTER TABLE `ma_branch`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `ma_business`
--
ALTER TABLE `ma_business`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `ma_business_category`
--
ALTER TABLE `ma_business_category`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `ma_business_type`
--
ALTER TABLE `ma_business_type`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `ma_customer`
--
ALTER TABLE `ma_customer`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `ma_customer_category`
--
ALTER TABLE `ma_customer_category`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `ma_customer_relation`
--
ALTER TABLE `ma_customer_relation`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `ma_discount`
--
ALTER TABLE `ma_discount`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `ma_goods`
--
ALTER TABLE `ma_goods`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `ma_goods_category`
--
ALTER TABLE `ma_goods_category`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `ma_permission`
--
ALTER TABLE `ma_permission`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `ma_role`
--
ALTER TABLE `ma_role`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `ma_user`
--
ALTER TABLE `ma_user`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `ma_user_position`
--
ALTER TABLE `ma_user_position`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `report_refund`
--
ALTER TABLE `report_refund`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `temp_order`
--
ALTER TABLE `temp_order`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `temp_order_detail`
--
ALTER TABLE `temp_order_detail`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `temp_order_discount`
--
ALTER TABLE `temp_order_discount`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `ts_discount`
--
ALTER TABLE `ts_discount`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `ts_pointreward`
--
ALTER TABLE `ts_pointreward`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `ts_transection`
--
ALTER TABLE `ts_transection`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `ts_transection_detail`
--
ALTER TABLE `ts_transection_detail`
  ADD PRIMARY KEY (`ID`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `ma_branch`
--
ALTER TABLE `ma_branch`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=36;
--
-- AUTO_INCREMENT for table `ma_business`
--
ALTER TABLE `ma_business`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=33;
--
-- AUTO_INCREMENT for table `ma_business_category`
--
ALTER TABLE `ma_business_category`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `ma_business_type`
--
ALTER TABLE `ma_business_type`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `ma_customer`
--
ALTER TABLE `ma_customer`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `ma_customer_category`
--
ALTER TABLE `ma_customer_category`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `ma_customer_relation`
--
ALTER TABLE `ma_customer_relation`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;
--
-- AUTO_INCREMENT for table `ma_discount`
--
ALTER TABLE `ma_discount`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;
--
-- AUTO_INCREMENT for table `ma_goods`
--
ALTER TABLE `ma_goods`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;
--
-- AUTO_INCREMENT for table `ma_goods_category`
--
ALTER TABLE `ma_goods_category`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;
--
-- AUTO_INCREMENT for table `ma_permission`
--
ALTER TABLE `ma_permission`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `ma_role`
--
ALTER TABLE `ma_role`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=117;
--
-- AUTO_INCREMENT for table `ma_user`
--
ALTER TABLE `ma_user`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=48;
--
-- AUTO_INCREMENT for table `ma_user_position`
--
ALTER TABLE `ma_user_position`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=30;
--
-- AUTO_INCREMENT for table `report_refund`
--
ALTER TABLE `report_refund`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;
--
-- AUTO_INCREMENT for table `temp_order`
--
ALTER TABLE `temp_order`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `temp_order_detail`
--
ALTER TABLE `temp_order_detail`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `temp_order_discount`
--
ALTER TABLE `temp_order_discount`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `ts_discount`
--
ALTER TABLE `ts_discount`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `ts_pointreward`
--
ALTER TABLE `ts_pointreward`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `ts_transection`
--
ALTER TABLE `ts_transection`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;
--
-- AUTO_INCREMENT for table `ts_transection_detail`
--
ALTER TABLE `ts_transection_detail`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
