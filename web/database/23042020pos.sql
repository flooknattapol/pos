-- phpMyAdmin SQL Dump
-- version 4.7.0
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Apr 23, 2020 at 12:17 PM
-- Server version: 5.6.34-log
-- PHP Version: 7.1.5

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `pos`
--

-- --------------------------------------------------------

--
-- Table structure for table `ma_branch`
--

CREATE TABLE `ma_branch` (
  `ID` int(11) NOT NULL,
  `line_no` int(11) NOT NULL,
  `BUSINESS_ID` int(11) NOT NULL,
  `UID` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `EMAIL` varchar(300) COLLATE utf8_unicode_ci NOT NULL,
  `NAME` varchar(300) COLLATE utf8_unicode_ci NOT NULL,
  `ADDRESS` varchar(300) COLLATE utf8_unicode_ci NOT NULL,
  `TEL` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `CreateDate` datetime NOT NULL,
  `IsActive` int(11) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `ma_branch`
--

INSERT INTO `ma_branch` (`ID`, `line_no`, `BUSINESS_ID`, `UID`, `EMAIL`, `NAME`, `ADDRESS`, `TEL`, `CreateDate`, `IsActive`) VALUES
(20, 0, 18, '877fdd7d875265e4d413f869573e5d36', 'f@gmail.com', 'สำนักงานใหญ่', 'siam', '02', '1970-01-01 00:00:00', 1),
(21, 2, 18, '5c60df043789319b9b3e2cfd72a5b746', 'f@gmail.com', 'สาขา2', 'siam', '03', '1970-01-01 00:00:00', 1),
(22, 0, 19, '8ee9b6950fefdf2dea04f8502af0e256', 'game@gmail.com', 'สำนักงานใหญ่', 'siam', '02', '1970-01-01 00:00:00', 1),
(23, 3, 18, '90392f30bef22945bd808ac0e71522d9', 'f@gmail.com', 'สาขา3', 'siam', '03', '1970-01-01 00:00:00', 1),
(24, 4, 18, '2be187bdd2a3e7cc73ffa04ac2de6ecb', 'f@gmail.com', 'สาขา4', 'siam', '04', '1970-01-01 00:00:00', 1),
(25, 0, 20, '0d3106907ea2bb8bdc3f81a7b9862cf5', 'japan@gmail.com', 'สำนักงานใหญ่', 'japan', '02', '1970-01-01 00:00:00', 1),
(26, 0, 21, '40cacb0c808d0e5711776150c76a93da', 'mon@a.com', 'สำนักงานใหญ่', 'siam', '02', '0000-00-00 00:00:00', 1),
(27, 0, 22, '6b4dcfdcefcf01b5ee465694a6238825', 'fff@gaa.com', 'สำนักงานใหญ่', 'siam', '02', '0000-00-00 00:00:00', 1),
(28, 0, 23, 'a406a3a560cc7662744a84f853e82ee3', 'flook', 'สำนักงานใหญ่', 'siam', '02', '1970-01-01 00:00:00', 1),
(29, 0, 24, '19e828d7cc8807d9ec7447700d4706e6', 'flook@mgail', 'สำนักงานใหญ่', 'siam', '02', '0000-00-00 00:00:00', 1);

-- --------------------------------------------------------

--
-- Table structure for table `ma_business`
--

CREATE TABLE `ma_business` (
  `ID` int(11) NOT NULL,
  `UID` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `EMAIL` varchar(300) COLLATE utf8_unicode_ci NOT NULL,
  `NAME` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `TEL` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `IMAGE` varchar(300) COLLATE utf8_unicode_ci NOT NULL,
  `CONTACT` varchar(2000) COLLATE utf8_unicode_ci NOT NULL,
  `CATEGORY_ID` int(11) NOT NULL,
  `TYPE_ID` int(11) NOT NULL,
  `TAXID10` varchar(10) COLLATE utf8_unicode_ci NOT NULL,
  `TAXID13` varchar(13) COLLATE utf8_unicode_ci NOT NULL,
  `CreateDate` datetime NOT NULL,
  `IsActive` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `ma_business`
--

INSERT INTO `ma_business` (`ID`, `UID`, `EMAIL`, `NAME`, `TEL`, `IMAGE`, `CONTACT`, `CATEGORY_ID`, `TYPE_ID`, `TAXID10`, `TAXID13`, `CreateDate`, `IsActive`) VALUES
(18, '8a5c2418e2c9b0bca99075d6ef84647c', 'f@gmail.com', 'อาหารตามสั่ง', '02', 'imgBusiness_2020-04-23-10-44-53.png', 'me', 0, 1, '', '', '0000-00-00 00:00:00', 1),
(19, '7da5a35218d438fecafc7a3591271d15', 'game@gmail.com', 'ร้านเกมส์', '02', '', 'aaa', 0, 1, '', '', '0000-00-00 00:00:00', 1),
(20, '4973cc7158ec39756d58092d2e1c0075', 'japan@gmail.com', 'อาหารญี่ปุ่น', '02', '', 'aa', 1, 2, '', '', '0000-00-00 00:00:00', 1),
(21, 'e2c7f99d64b563dc59b8fb0a93eb233e', 'mon@a.com', 'มนนมสด', '02', '', '', 0, 1, '', '', '0000-00-00 00:00:00', 1),
(22, 'b23a385251a9decfb2a8fef7714334a1', 'gggg@gmad.com', 'ร้านอาหาร2', '02', '', '', 0, 1, '', '', '0000-00-00 00:00:00', 0),
(23, 'e18e90149a3452e2ca50b9ab2d7ef7fd', 'nom@gmail.com', 'ร้านนม', '02', '', 'tt', 0, 1, '', '', '0000-00-00 00:00:00', 1),
(24, '01adba99f817adcac1dc48ae24f298d7', 'na@mail.com', 'ร้านนาย', '02', '', 'na', 1, 1, '', '', '0000-00-00 00:00:00', 1);

-- --------------------------------------------------------

--
-- Table structure for table `ma_business_category`
--

CREATE TABLE `ma_business_category` (
  `ID` int(11) NOT NULL,
  `NAME` varchar(300) COLLATE utf8_unicode_ci NOT NULL,
  `DETAIL` varchar(300) COLLATE utf8_unicode_ci NOT NULL,
  `IsActive` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `ma_business_category`
--

INSERT INTO `ma_business_category` (`ID`, `NAME`, `DETAIL`, `IsActive`) VALUES
(1, 'ทั่วไป', '', 1);

-- --------------------------------------------------------

--
-- Table structure for table `ma_business_type`
--

CREATE TABLE `ma_business_type` (
  `ID` int(11) NOT NULL,
  `NAME` varchar(300) COLLATE utf8_unicode_ci NOT NULL,
  `DETAIL` varchar(300) COLLATE utf8_unicode_ci NOT NULL,
  `IsActive` int(11) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `ma_business_type`
--

INSERT INTO `ma_business_type` (`ID`, `NAME`, `DETAIL`, `IsActive`) VALUES
(1, 'ร้านอาหาร', 'ขายอาหารทั่วไป', 1),
(2, 'ของของชำ', 'ขายของทั่วไป', 1),
(3, 'ของเล่น', '', 1);

-- --------------------------------------------------------

--
-- Table structure for table `ma_customer`
--

CREATE TABLE `ma_customer` (
  `ID` int(11) NOT NULL,
  `NAME` varchar(300) COLLATE utf8_unicode_ci NOT NULL,
  `TEL` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `EMAIL` varchar(300) COLLATE utf8_unicode_ci NOT NULL,
  `CATEGORY_ID` int(11) NOT NULL,
  `BUSINESS_ID` int(11) NOT NULL,
  `AMOUNTBUY` int(11) NOT NULL,
  `REWARDPOINT` int(11) NOT NULL,
  `NOTE` varchar(300) COLLATE utf8_unicode_ci NOT NULL,
  `IsActive` int(11) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `ma_customer`
--

INSERT INTO `ma_customer` (`ID`, `NAME`, `TEL`, `EMAIL`, `CATEGORY_ID`, `BUSINESS_ID`, `AMOUNTBUY`, `REWARDPOINT`, `NOTE`, `IsActive`) VALUES
(1, 'ลูกค้า A', '02', 'f@gmail.com', 2, 18, 0, 0, '', 1),
(2, 'นาย b', '02', 'bb@gmail.com', 1, 18, 0, 0, '', 1),
(3, 'นาย C', '02', 'ccc@gmad.com', 1, 18, 0, 0, '', 1);

-- --------------------------------------------------------

--
-- Table structure for table `ma_customer_category`
--

CREATE TABLE `ma_customer_category` (
  `ID` int(11) NOT NULL,
  `BUSINESS_ID` int(11) NOT NULL,
  `NAME` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `DETAIL` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `IsActive` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `ma_customer_category`
--

INSERT INTO `ma_customer_category` (`ID`, `BUSINESS_ID`, `NAME`, `DETAIL`, `IsActive`) VALUES
(1, 18, 'ทั่วไป', '', 1),
(2, 18, 'พิเศษ', '', 1);

-- --------------------------------------------------------

--
-- Table structure for table `ma_goods`
--

CREATE TABLE `ma_goods` (
  `ID` int(11) NOT NULL,
  `NAME` varchar(300) COLLATE utf8_unicode_ci NOT NULL,
  `IMAGE` varchar(300) COLLATE utf8_unicode_ci NOT NULL,
  `DETAIL` varchar(300) COLLATE utf8_unicode_ci NOT NULL,
  `STATUS` tinyint(1) DEFAULT '1',
  `CATEGORY_ID` int(11) NOT NULL,
  `SOLDBY` int(11) NOT NULL,
  `PRICE` int(11) NOT NULL,
  `COST` int(11) NOT NULL,
  `SKU` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `BARCODE` varchar(300) COLLATE utf8_unicode_ci NOT NULL,
  `COMPOSITE` varchar(300) COLLATE utf8_unicode_ci NOT NULL,
  `STOCK` int(11) NOT NULL,
  `BUSINESS_ID` int(11) NOT NULL,
  `IsActive` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `ma_goods`
--

INSERT INTO `ma_goods` (`ID`, `NAME`, `IMAGE`, `DETAIL`, `STATUS`, `CATEGORY_ID`, `SOLDBY`, `PRICE`, `COST`, `SKU`, `BARCODE`, `COMPOSITE`, `STOCK`, `BUSINESS_ID`, `IsActive`) VALUES
(1, 'น้ำส้มปั่น', 'imgGoods_2020-04-23-17-42-15.png', '', 1, 2, 0, 30, 10, '101', '', '', 0, 18, 1),
(2, 'ขนมไทย', 'imgGoods_2020-04-23-17-46-40.png', '', 0, 3, 0, 5, 1, '102', '', '', 0, 18, 1),
(3, 'น้ำมะนาว', '', '', 1, 2, 0, 30, 10, '10', '', '', 0, 18, 1);

-- --------------------------------------------------------

--
-- Table structure for table `ma_goods_category`
--

CREATE TABLE `ma_goods_category` (
  `ID` int(11) NOT NULL,
  `BUSINESS_ID` int(11) NOT NULL,
  `NAME` varchar(300) COLLATE utf8_unicode_ci NOT NULL,
  `DETAIL` varchar(300) COLLATE utf8_unicode_ci NOT NULL,
  `COLOR` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `IsActive` int(11) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `ma_goods_category`
--

INSERT INTO `ma_goods_category` (`ID`, `BUSINESS_ID`, `NAME`, `DETAIL`, `COLOR`, `IsActive`) VALUES
(1, 18, 'อาหาร', 'อาหารทั่ว ๆไป', '', 1),
(2, 18, 'น้ำ', 'น้ำทั่วๆไป', '', 1),
(3, 18, 'ขนม', '', '', 1);

-- --------------------------------------------------------

--
-- Table structure for table `ma_permission`
--

CREATE TABLE `ma_permission` (
  `ID` int(11) NOT NULL,
  `NAME` varchar(300) COLLATE utf8_unicode_ci NOT NULL,
  `DETAIL` varchar(300) COLLATE utf8_unicode_ci NOT NULL,
  `TYPE` int(11) NOT NULL,
  `IsActive` int(11) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `ma_role`
--

CREATE TABLE `ma_role` (
  `ID` int(11) NOT NULL,
  `NAME` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `BUSINESS_ID` int(11) NOT NULL,
  `PERMISSION_DASHBOARD_IsActive` int(11) NOT NULL DEFAULT '0',
  `PERMISSION_POS_IsActive` int(11) NOT NULL DEFAULT '0',
  `PERMISSION_POS` varchar(300) COLLATE utf8_unicode_ci NOT NULL,
  `PERMISSION_DASHBOARD` varchar(300) COLLATE utf8_unicode_ci NOT NULL,
  `MASTER` int(11) NOT NULL DEFAULT '0',
  `IsActive` int(11) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `ma_role`
--

INSERT INTO `ma_role` (`ID`, `NAME`, `BUSINESS_ID`, `PERMISSION_DASHBOARD_IsActive`, `PERMISSION_POS_IsActive`, `PERMISSION_POS`, `PERMISSION_DASHBOARD`, `MASTER`, `IsActive`) VALUES
(64, 'เจ้าของ', 18, 1, 1, '', '', 1, 1),
(65, 'ผู้ดูแลระบบ', 18, 1, 1, '', '', 0, 1),
(66, 'ผู้จัดการ', 18, 1, 1, '', '', 0, 1),
(67, 'แคชเชียร์', 18, 0, 1, '', '', 0, 1),
(68, 'เจ้าของ', 19, 1, 1, '', '', 1, 1),
(69, 'ผู้ดูแลระบบ', 19, 1, 1, '', '', 0, 1),
(70, 'ผู้จัดการ', 19, 1, 1, '', '', 0, 1),
(71, 'แคชเชียร์', 19, 0, 1, '', '', 0, 1),
(72, 'เจ้าของ', 20, 1, 1, '', '', 1, 1),
(73, 'ผู้ดูแลระบบ', 20, 1, 1, '', '', 0, 1),
(74, 'ผู้จัดการ', 20, 1, 1, '', '', 0, 1),
(75, 'แคชเชียร์', 20, 0, 1, '', '', 0, 1),
(76, 'พนักงานเสิร์ฟ', 18, 0, 1, '', '', 0, 1),
(77, 'เจ้าของ', 21, 1, 1, '', '', 1, 1),
(78, 'ผู้ดูแลระบบ', 21, 1, 1, '', '', 0, 1),
(79, 'ผู้จัดการ', 21, 1, 1, '', '', 0, 1),
(80, 'แคชเชียร์', 21, 0, 1, '', '', 0, 1),
(81, 'เจ้าของ', 22, 1, 1, '', '', 1, 1),
(82, 'ผู้ดูแลระบบ', 22, 1, 1, '', '', 0, 1),
(83, 'ผู้จัดการ', 22, 1, 1, '', '', 0, 1),
(84, 'แคชเชียร์', 22, 0, 1, '', '', 0, 1),
(85, 'เจ้าของ', 23, 1, 1, '', '', 1, 1),
(86, 'ผู้ดูแลระบบ', 23, 1, 1, '', '', 0, 1),
(87, 'ผู้จัดการ', 23, 1, 1, '', '', 0, 1),
(88, 'แคชเชียร์', 23, 0, 1, '', '', 0, 1),
(89, 'เจ้าของ', 24, 1, 1, '', '', 1, 1),
(90, 'ผู้ดูแลระบบ', 24, 1, 1, '', '', 0, 1),
(91, 'ผู้จัดการ', 24, 1, 1, '', '', 0, 1),
(92, 'แคชเชียร์', 24, 0, 1, '', '', 0, 1),
(93, 'คนรับรถ', 18, 0, 0, '', '', 0, 0),
(94, 'เชฟ', 18, 0, 0, '', '', 0, 1);

-- --------------------------------------------------------

--
-- Table structure for table `ma_user`
--

CREATE TABLE `ma_user` (
  `ID` int(11) NOT NULL,
  `EMAIL` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `PASSWORD` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `NAME` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `TEL` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `IMAGE` varchar(300) COLLATE utf8_unicode_ci NOT NULL,
  `IsActive` int(11) DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `ma_user`
--

INSERT INTO `ma_user` (`ID`, `EMAIL`, `PASSWORD`, `NAME`, `TEL`, `IMAGE`, `IsActive`) VALUES
(9, 'admin@pos.com', '202cb962ac59075b964b07152d234b70', 'admin', '', '', 1),
(20, 'f@gmail.com', '202cb962ac59075b964b07152d234b70', 'เจ้าของ', '034', 'imgUser_2020-04-23-17-56-14.png', 1),
(21, 'game@gmail.com', '202cb962ac59075b964b07152d234b70', 'เจ้าของ', '', '', 1),
(22, 'japan@gmail.com', '202cb962ac59075b964b07152d234b70', 'เจ้าของ', '', '', 1),
(23, 'mon@a.com', '202cb962ac59075b964b07152d234b70', 'เจ้าของ', '', '', 1),
(29, 'ff@gmail.com', '202cb962ac59075b964b07152d234b70', 'นาย A', '02', 'imgUser_2020-04-23-17-56-44.png', 1),
(30, 'fff@gmail.com', '202cb962ac59075b964b07152d234b70', 'นาย b', '02', '', 1),
(31, 'gggg@gmad.com', '202cb962ac59075b964b07152d234b70', 'เจ้าของ', '', '', 1),
(32, 'nom@gmail.com', '202cb962ac59075b964b07152d234b70', '', '', '', 1),
(33, 'na@mail.com', '202cb962ac59075b964b07152d234b70', 'na', '', '', 1),
(34, 'wwfff@gmail.com', '202cb962ac59075b964b07152d234b70', 'นาย b', '02', 'imgUser_2020-04-23-17-54-34.png', 1);

-- --------------------------------------------------------

--
-- Table structure for table `ma_user_position`
--

CREATE TABLE `ma_user_position` (
  `ID` int(11) NOT NULL,
  `USER_ID` int(11) NOT NULL,
  `BUSINESS_ID` int(11) NOT NULL,
  `BRANCH_ID` int(11) NOT NULL,
  `ROLE_ID` int(11) NOT NULL,
  `IsActive` int(11) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `ma_user_position`
--

INSERT INTO `ma_user_position` (`ID`, `USER_ID`, `BUSINESS_ID`, `BRANCH_ID`, `ROLE_ID`, `IsActive`) VALUES
(5, 20, 18, 20, 64, 1),
(7, 21, 19, 22, 68, 1),
(9, 22, 20, 25, 72, 1),
(11, 23, 21, 26, 77, 1),
(13, 29, 18, 20, 65, 1),
(14, 30, 18, 20, 67, 0),
(15, 31, 22, 27, 81, 1),
(16, 32, 23, 28, 85, 1),
(17, 33, 24, 29, 89, 1),
(18, 34, 18, 20, 66, 1);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `ma_branch`
--
ALTER TABLE `ma_branch`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `ma_business`
--
ALTER TABLE `ma_business`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `ma_business_category`
--
ALTER TABLE `ma_business_category`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `ma_business_type`
--
ALTER TABLE `ma_business_type`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `ma_customer`
--
ALTER TABLE `ma_customer`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `ma_customer_category`
--
ALTER TABLE `ma_customer_category`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `ma_goods`
--
ALTER TABLE `ma_goods`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `ma_goods_category`
--
ALTER TABLE `ma_goods_category`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `ma_permission`
--
ALTER TABLE `ma_permission`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `ma_role`
--
ALTER TABLE `ma_role`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `ma_user`
--
ALTER TABLE `ma_user`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `ma_user_position`
--
ALTER TABLE `ma_user_position`
  ADD PRIMARY KEY (`ID`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `ma_branch`
--
ALTER TABLE `ma_branch`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=30;
--
-- AUTO_INCREMENT for table `ma_business`
--
ALTER TABLE `ma_business`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=25;
--
-- AUTO_INCREMENT for table `ma_business_category`
--
ALTER TABLE `ma_business_category`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `ma_business_type`
--
ALTER TABLE `ma_business_type`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `ma_customer`
--
ALTER TABLE `ma_customer`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `ma_customer_category`
--
ALTER TABLE `ma_customer_category`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `ma_goods`
--
ALTER TABLE `ma_goods`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `ma_goods_category`
--
ALTER TABLE `ma_goods_category`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `ma_permission`
--
ALTER TABLE `ma_permission`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `ma_role`
--
ALTER TABLE `ma_role`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=95;
--
-- AUTO_INCREMENT for table `ma_user`
--
ALTER TABLE `ma_user`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=35;
--
-- AUTO_INCREMENT for table `ma_user_position`
--
ALTER TABLE `ma_user_position`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
