-- phpMyAdmin SQL Dump
-- version 4.7.0
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Apr 22, 2020 at 11:37 AM
-- Server version: 5.6.34-log
-- PHP Version: 7.1.5

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `pos`
--

-- --------------------------------------------------------

--
-- Table structure for table `ma_branch`
--

CREATE TABLE `ma_branch` (
  `ID` int(11) NOT NULL,
  `line_no` int(11) NOT NULL,
  `BUSINESS_ID` int(11) NOT NULL,
  `UID` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `EMAIL` varchar(300) COLLATE utf8_unicode_ci NOT NULL,
  `NAME` varchar(300) COLLATE utf8_unicode_ci NOT NULL,
  `ADDRESS` varchar(300) COLLATE utf8_unicode_ci NOT NULL,
  `TEL` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `CreateDate` datetime NOT NULL,
  `IsActive` int(11) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `ma_branch`
--

INSERT INTO `ma_branch` (`ID`, `line_no`, `BUSINESS_ID`, `UID`, `EMAIL`, `NAME`, `ADDRESS`, `TEL`, `CreateDate`, `IsActive`) VALUES
(20, 0, 18, '877fdd7d875265e4d413f869573e5d36', 'f@gmail.com', 'สำนักงานใหญ่', 'siam', '02', '1970-01-01 00:00:00', 1),
(21, 2, 18, '5c60df043789319b9b3e2cfd72a5b746', 'f@gmail.com', 'สาขา2', 'siam', '03', '1970-01-01 00:00:00', 1),
(22, 0, 19, '8ee9b6950fefdf2dea04f8502af0e256', 'game@gmail.com', 'สำนักงานใหญ่', 'siam', '02', '0000-00-00 00:00:00', 1),
(23, 3, 18, '90392f30bef22945bd808ac0e71522d9', 'f@gmail.com', 'สาขา3', 'siam', '03', '1970-01-01 00:00:00', 1),
(24, 4, 18, '2be187bdd2a3e7cc73ffa04ac2de6ecb', 'f@gmail.com', 'สาขา4', 'siam', '04', '0000-00-00 00:00:00', 1),
(25, 0, 20, '0d3106907ea2bb8bdc3f81a7b9862cf5', 'japan@gmail.com', 'สำนักงานใหญ่', 'japan', '02', '0000-00-00 00:00:00', 1),
(26, 0, 21, '40cacb0c808d0e5711776150c76a93da', 'mon@a.com', 'สำนักงานใหญ่', 'siam', '02', '0000-00-00 00:00:00', 1);

-- --------------------------------------------------------

--
-- Table structure for table `ma_business`
--

CREATE TABLE `ma_business` (
  `ID` int(11) NOT NULL,
  `UID` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `EMAIL` varchar(300) COLLATE utf8_unicode_ci NOT NULL,
  `NAME` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `TEL` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `TYPE` int(11) NOT NULL,
  `TAXID10` varchar(10) COLLATE utf8_unicode_ci NOT NULL,
  `TAXID13` varchar(13) COLLATE utf8_unicode_ci NOT NULL,
  `CreateDate` datetime NOT NULL,
  `IsActive` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `ma_business`
--

INSERT INTO `ma_business` (`ID`, `UID`, `EMAIL`, `NAME`, `TEL`, `TYPE`, `TAXID10`, `TAXID13`, `CreateDate`, `IsActive`) VALUES
(18, '8a5c2418e2c9b0bca99075d6ef84647c', 'f@gmail.com', 'อาหารตามสั่ง', '02', 1, '', '', '0000-00-00 00:00:00', 1),
(19, '7da5a35218d438fecafc7a3591271d15', 'game@gmail.com', 'ร้านเกมส์', '02', 3, '', '', '0000-00-00 00:00:00', 1),
(20, '4973cc7158ec39756d58092d2e1c0075', 'japan@gmail.com', 'อาหารญี่ปุ่น', '02', 1, '', '', '0000-00-00 00:00:00', 1),
(21, 'e2c7f99d64b563dc59b8fb0a93eb233e', 'mon@a.com', 'มนนมสด', '02', 1, '', '', '0000-00-00 00:00:00', 1);

-- --------------------------------------------------------

--
-- Table structure for table `ma_business_type`
--

CREATE TABLE `ma_business_type` (
  `ID` int(11) NOT NULL,
  `NAME` varchar(300) COLLATE utf8_unicode_ci NOT NULL,
  `DETAIL` varchar(300) COLLATE utf8_unicode_ci NOT NULL,
  `IsActive` int(11) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `ma_business_type`
--

INSERT INTO `ma_business_type` (`ID`, `NAME`, `DETAIL`, `IsActive`) VALUES
(1, 'ร้านอาหาร', 'ขายอาหารทั่วไป', 1),
(2, 'ของของชำ', 'ขายของทั่วไป', 1),
(3, 'ของเล่น', '', 1);

-- --------------------------------------------------------

--
-- Table structure for table `ma_goods`
--

CREATE TABLE `ma_goods` (
  `ID` int(11) NOT NULL,
  `NAME` varchar(300) COLLATE utf8_unicode_ci NOT NULL,
  `CATEGORY` int(11) NOT NULL,
  `SOLDBY` int(11) NOT NULL,
  `PRICE` int(11) NOT NULL,
  `COST` int(11) NOT NULL,
  `SKU` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `COMPOSITE` varchar(300) COLLATE utf8_unicode_ci NOT NULL,
  `STOCK` int(11) NOT NULL,
  `BRANCH_ID` int(11) NOT NULL,
  `IsActive` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `ma_goods_category`
--

CREATE TABLE `ma_goods_category` (
  `ID` int(11) NOT NULL,
  `BRANCH_ID` int(11) NOT NULL,
  `NAME` varchar(300) COLLATE utf8_unicode_ci NOT NULL,
  `COLOR` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `IsActive` int(11) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `ma_permission`
--

CREATE TABLE `ma_permission` (
  `ID` int(11) NOT NULL,
  `NAME` varchar(300) COLLATE utf8_unicode_ci NOT NULL,
  `DETAIL` varchar(300) COLLATE utf8_unicode_ci NOT NULL,
  `TYPE` int(11) NOT NULL,
  `IsActive` int(11) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `ma_role`
--

CREATE TABLE `ma_role` (
  `ID` int(11) NOT NULL,
  `NAME` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `BUSINESS_ID` int(11) NOT NULL,
  `PERMISSION_DASHBOARD_IsActive` int(11) NOT NULL DEFAULT '0',
  `PERMISSION_POS_IsActive` int(11) NOT NULL DEFAULT '0',
  `PERMISSION_POS` varchar(300) COLLATE utf8_unicode_ci NOT NULL,
  `PERMISSION_DASHBOARD` varchar(300) COLLATE utf8_unicode_ci NOT NULL,
  `MASTER` int(11) NOT NULL DEFAULT '0',
  `IsActive` int(11) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `ma_role`
--

INSERT INTO `ma_role` (`ID`, `NAME`, `BUSINESS_ID`, `PERMISSION_DASHBOARD_IsActive`, `PERMISSION_POS_IsActive`, `PERMISSION_POS`, `PERMISSION_DASHBOARD`, `MASTER`, `IsActive`) VALUES
(64, 'เจ้าของ', 18, 1, 1, '', '', 1, 1),
(65, 'ผู้ดูแลระบบ', 18, 1, 1, '', '', 0, 1),
(66, 'ผู้จัดการ', 18, 1, 1, '', '', 0, 1),
(67, 'แคชเชียร์', 18, 0, 1, '', '', 0, 1),
(68, 'เจ้าของ', 19, 1, 1, '', '', 1, 1),
(69, 'ผู้ดูแลระบบ', 19, 1, 1, '', '', 0, 1),
(70, 'ผู้จัดการ', 19, 1, 1, '', '', 0, 1),
(71, 'แคชเชียร์', 19, 0, 1, '', '', 0, 1),
(72, 'เจ้าของ', 20, 1, 1, '', '', 1, 1),
(73, 'ผู้ดูแลระบบ', 20, 1, 1, '', '', 0, 1),
(74, 'ผู้จัดการ', 20, 1, 1, '', '', 0, 1),
(75, 'แคชเชียร์', 20, 0, 1, '', '', 0, 1),
(76, 'พนักงานเสิร์ฟ', 18, 0, 1, '', '', 0, 1),
(77, 'เจ้าของ', 21, 1, 1, '', '', 1, 1),
(78, 'ผู้ดูแลระบบ', 21, 1, 1, '', '', 0, 1),
(79, 'ผู้จัดการ', 21, 1, 1, '', '', 0, 1),
(80, 'แคชเชียร์', 21, 0, 1, '', '', 0, 1);

-- --------------------------------------------------------

--
-- Table structure for table `ma_user`
--

CREATE TABLE `ma_user` (
  `ID` int(11) NOT NULL,
  `EMAIL` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `PASSWORD` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `NAME` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `TEL` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `IsActive` int(11) DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `ma_user`
--

INSERT INTO `ma_user` (`ID`, `EMAIL`, `PASSWORD`, `NAME`, `TEL`, `IsActive`) VALUES
(9, 'admin@pos.com', '202cb962ac59075b964b07152d234b70', 'admin', '', 1),
(20, 'f@gmail.com', '1cc39ffd758234422e1f75beadfc5fb2', 'เจ้าของ', '03', 1),
(21, 'game@gmail.com', '202cb962ac59075b964b07152d234b70', 'เจ้าของ', '', 1),
(22, 'japan@gmail.com', '202cb962ac59075b964b07152d234b70', 'เจ้าของ', '', 1),
(23, 'mon@a.com', '202cb962ac59075b964b07152d234b70', 'เจ้าของ', '', 1);

-- --------------------------------------------------------

--
-- Table structure for table `ma_user_position`
--

CREATE TABLE `ma_user_position` (
  `ID` int(11) NOT NULL,
  `USER_ID` int(11) NOT NULL,
  `BUSINESS_ID` int(11) NOT NULL,
  `BRANCH_ID` int(11) NOT NULL,
  `ROLE_ID` int(11) NOT NULL,
  `IsActive` int(11) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `ma_user_position`
--

INSERT INTO `ma_user_position` (`ID`, `USER_ID`, `BUSINESS_ID`, `BRANCH_ID`, `ROLE_ID`, `IsActive`) VALUES
(5, 20, 18, 20, 64, 1),
(7, 21, 19, 22, 68, 1),
(9, 22, 20, 25, 72, 1),
(10, 20, 19, 22, 68, 1),
(11, 23, 21, 26, 77, 1);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `ma_branch`
--
ALTER TABLE `ma_branch`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `ma_business`
--
ALTER TABLE `ma_business`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `ma_business_type`
--
ALTER TABLE `ma_business_type`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `ma_goods`
--
ALTER TABLE `ma_goods`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `ma_goods_category`
--
ALTER TABLE `ma_goods_category`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `ma_permission`
--
ALTER TABLE `ma_permission`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `ma_role`
--
ALTER TABLE `ma_role`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `ma_user`
--
ALTER TABLE `ma_user`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `ma_user_position`
--
ALTER TABLE `ma_user_position`
  ADD PRIMARY KEY (`ID`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `ma_branch`
--
ALTER TABLE `ma_branch`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=27;
--
-- AUTO_INCREMENT for table `ma_business`
--
ALTER TABLE `ma_business`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=22;
--
-- AUTO_INCREMENT for table `ma_business_type`
--
ALTER TABLE `ma_business_type`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `ma_goods`
--
ALTER TABLE `ma_goods`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `ma_goods_category`
--
ALTER TABLE `ma_goods_category`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `ma_permission`
--
ALTER TABLE `ma_permission`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `ma_role`
--
ALTER TABLE `ma_role`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=81;
--
-- AUTO_INCREMENT for table `ma_user`
--
ALTER TABLE `ma_user`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=24;
--
-- AUTO_INCREMENT for table `ma_user_position`
--
ALTER TABLE `ma_user_position`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
