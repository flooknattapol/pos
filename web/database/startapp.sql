-- phpMyAdmin SQL Dump
-- version 4.7.0
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Apr 20, 2020 at 11:42 AM
-- Server version: 5.6.34-log
-- PHP Version: 7.1.5

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `pos`
--

-- --------------------------------------------------------

--
-- Table structure for table `ma_business`
--

CREATE TABLE `ma_business` (
  `ID` int(11) NOT NULL,
  `NAME` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `CreateDate` date NOT NULL,
  `IsActive` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `ma_business`
--

INSERT INTO `ma_business` (`ID`, `NAME`, `CreateDate`, `IsActive`) VALUES
(1, 'ร้านขนมหวาน', '2020-04-20', 1);

-- --------------------------------------------------------

--
-- Table structure for table `ma_goods`
--

CREATE TABLE `ma_goods` (
  `ID` int(11) NOT NULL,
  `NAME` varchar(300) COLLATE utf8_unicode_ci NOT NULL,
  `CATEGORY` int(11) NOT NULL,
  `SOLDBY` int(11) NOT NULL,
  `PRICE` int(11) NOT NULL,
  `COST` int(11) NOT NULL,
  `SKU` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `COMPOSITE` varchar(300) COLLATE utf8_unicode_ci NOT NULL,
  `STOCK` int(11) NOT NULL,
  `BUSINESS_ID` int(11) NOT NULL,
  `IsActive` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `ma_goods_category`
--

CREATE TABLE `ma_goods_category` (
  `ID` int(11) NOT NULL,
  `BUSINESS_ID` int(11) NOT NULL,
  `NAME` varchar(300) COLLATE utf8_unicode_ci NOT NULL,
  `COLOR` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `IsActive` int(11) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `ma_permission`
--

CREATE TABLE `ma_permission` (
  `ID` int(11) NOT NULL,
  `NAME` varchar(300) COLLATE utf8_unicode_ci NOT NULL,
  `DETAIL` varchar(300) COLLATE utf8_unicode_ci NOT NULL,
  `TYPE` int(11) NOT NULL,
  `IsActive` int(11) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `ma_role`
--

CREATE TABLE `ma_role` (
  `ID` int(11) NOT NULL,
  `NAME` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `BUSINESS_ID` int(11) NOT NULL,
  `PERMISSION_DASHBOARD_IsActive` int(11) NOT NULL DEFAULT '0',
  `PERMISSION_POS_IsActive` int(11) NOT NULL DEFAULT '0',
  `PERMISSION_POS` varchar(300) COLLATE utf8_unicode_ci NOT NULL,
  `PERMISSION_DASHBOARD` varchar(300) COLLATE utf8_unicode_ci NOT NULL,
  `IsActive` int(11) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `ma_role`
--

INSERT INTO `ma_role` (`ID`, `NAME`, `BUSINESS_ID`, `PERMISSION_DASHBOARD_IsActive`, `PERMISSION_POS_IsActive`, `PERMISSION_POS`, `PERMISSION_DASHBOARD`, `IsActive`) VALUES
(1, 'admin', 1, 0, 0, '', '', 1),
(2, 'เจ้าของ', 1, 0, 0, '', '', 1),
(3, 'แคชเชียร์', 1, 0, 0, '', '', 1),
(4, 'ผู้ดูแลระบบ', 1, 0, 0, '', '', 1),
(5, 'ผู้จัดการ', 1, 0, 0, '', '', 1);

-- --------------------------------------------------------

--
-- Table structure for table `ma_user`
--

CREATE TABLE `ma_user` (
  `ID` int(11) NOT NULL,
  `EMAIL` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `PASSWORD` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `NAME` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `TEL` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `TYPE` int(11) NOT NULL,
  `BUSINESS_ID` int(11) NOT NULL,
  `IsActive` int(11) DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `ma_user`
--

INSERT INTO `ma_user` (`ID`, `EMAIL`, `PASSWORD`, `NAME`, `TEL`, `TYPE`, `BUSINESS_ID`, `IsActive`) VALUES
(1, 'admin@pos.com', '202cb962ac59075b964b07152d234b70', 'admin', '01', 1, 0, 1),
(2, 'vendor1@gmail.com', '202cb962ac59075b964b07152d234b70', 'Vendor1', '01', 2, 1, 1),
(3, 'cashier1@gmail.com', '202cb962ac59075b964b07152d234b70', 'cashier1', '', 3, 1, 1);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `ma_business`
--
ALTER TABLE `ma_business`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `ma_goods`
--
ALTER TABLE `ma_goods`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `ma_goods_category`
--
ALTER TABLE `ma_goods_category`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `ma_permission`
--
ALTER TABLE `ma_permission`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `ma_role`
--
ALTER TABLE `ma_role`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `ma_user`
--
ALTER TABLE `ma_user`
  ADD PRIMARY KEY (`ID`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `ma_business`
--
ALTER TABLE `ma_business`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `ma_goods`
--
ALTER TABLE `ma_goods`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `ma_goods_category`
--
ALTER TABLE `ma_goods_category`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `ma_permission`
--
ALTER TABLE `ma_permission`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `ma_role`
--
ALTER TABLE `ma_role`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `ma_user`
--
ALTER TABLE `ma_user`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
