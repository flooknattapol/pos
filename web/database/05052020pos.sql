-- phpMyAdmin SQL Dump
-- version 4.7.0
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: May 05, 2020 at 08:18 AM
-- Server version: 5.6.34-log
-- PHP Version: 7.1.5

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `pos`
--

-- --------------------------------------------------------

--
-- Table structure for table `ma_branch`
--

CREATE TABLE `ma_branch` (
  `ID` int(11) NOT NULL,
  `line_no` int(11) NOT NULL,
  `BUSINESS_ID` int(11) NOT NULL,
  `UID` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `EMAIL` varchar(300) COLLATE utf8_unicode_ci NOT NULL,
  `NAME` varchar(300) COLLATE utf8_unicode_ci NOT NULL,
  `ADDRESS` varchar(300) COLLATE utf8_unicode_ci NOT NULL,
  `TEL` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `CreateDate` datetime NOT NULL,
  `IsActive` int(11) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `ma_branch`
--

INSERT INTO `ma_branch` (`ID`, `line_no`, `BUSINESS_ID`, `UID`, `EMAIL`, `NAME`, `ADDRESS`, `TEL`, `CreateDate`, `IsActive`) VALUES
(20, 0, 18, '877fdd7d875265e4d413f869573e5d36', 'f@gmail.com', 'สำนักงานใหญ่', 'siam', '02', '1970-01-01 00:00:00', 1),
(21, 2, 18, '5c60df043789319b9b3e2cfd72a5b746', 'f@gmail.com', 'สาขา2', 'siam', '03', '1970-01-01 00:00:00', 1),
(22, 0, 19, '8ee9b6950fefdf2dea04f8502af0e256', 'game@gmail.com', 'สำนักงานใหญ่', 'siam', '02', '1970-01-01 00:00:00', 1),
(23, 3, 18, '90392f30bef22945bd808ac0e71522d9', 'f@gmail.com', 'สาขา3', 'siam', '03', '1970-01-01 00:00:00', 1),
(24, 4, 18, '2be187bdd2a3e7cc73ffa04ac2de6ecb', 'f@gmail.com', 'สาขา4', 'siam', '04', '1970-01-01 00:00:00', 1),
(25, 0, 20, '0d3106907ea2bb8bdc3f81a7b9862cf5', 'japan@gmail.com', 'สำนักงานใหญ่', 'japan', '02', '1970-01-01 00:00:00', 1),
(26, 0, 21, '40cacb0c808d0e5711776150c76a93da', 'mon@a.com', 'สำนักงานใหญ่', 'siam', '02', '0000-00-00 00:00:00', 1),
(27, 0, 22, '6b4dcfdcefcf01b5ee465694a6238825', 'fff@gaa.com', 'สำนักงานใหญ่', 'siam', '02', '0000-00-00 00:00:00', 1),
(28, 0, 23, 'a406a3a560cc7662744a84f853e82ee3', 'flook', 'สำนักงานใหญ่', 'siam', '02', '1970-01-01 00:00:00', 1),
(29, 0, 24, '19e828d7cc8807d9ec7447700d4706e6', 'flook@mgail', 'สำนักงานใหญ่', 'siam', '02', '0000-00-00 00:00:00', 1);

-- --------------------------------------------------------

--
-- Table structure for table `ma_business`
--

CREATE TABLE `ma_business` (
  `ID` int(11) NOT NULL,
  `UID` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `EMAIL` varchar(300) COLLATE utf8_unicode_ci NOT NULL,
  `NAME` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `TEL` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `IMAGE` varchar(300) COLLATE utf8_unicode_ci NOT NULL,
  `CONTACT` varchar(2000) COLLATE utf8_unicode_ci NOT NULL,
  `CATEGORY_ID` int(11) NOT NULL,
  `TYPE_ID` int(11) NOT NULL,
  `TAXID10` varchar(10) COLLATE utf8_unicode_ci NOT NULL,
  `TAXID13` varchar(13) COLLATE utf8_unicode_ci NOT NULL,
  `CreateDate` datetime NOT NULL,
  `IsActive` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `ma_business`
--

INSERT INTO `ma_business` (`ID`, `UID`, `EMAIL`, `NAME`, `TEL`, `IMAGE`, `CONTACT`, `CATEGORY_ID`, `TYPE_ID`, `TAXID10`, `TAXID13`, `CreateDate`, `IsActive`) VALUES
(18, '8a5c2418e2c9b0bca99075d6ef84647c', 'f@gmail.com', 'อาหารตามสั่ง', '0628842544', 'imgBusiness_2020-04-23-10-44-53.png', 'me', 1, 2, '', '', '0000-00-00 00:00:00', 1),
(19, '7da5a35218d438fecafc7a3591271d15', 'game@gmail.com', 'ร้านเกมส์', '02', '', 'aaa', 1, 1, '', '', '0000-00-00 00:00:00', 1),
(20, '4973cc7158ec39756d58092d2e1c0075', 'japan@gmail.com', 'อาหารญี่ปุ่น', '02', '', 'aa', 1, 2, '', '', '0000-00-00 00:00:00', 1),
(21, 'e2c7f99d64b563dc59b8fb0a93eb233e', 'mon@a.com', 'มนนมสด', '02', '', '', 0, 1, '', '', '0000-00-00 00:00:00', 1),
(22, 'b23a385251a9decfb2a8fef7714334a1', 'gggg@gmad.com', 'ร้านอาหาร2', '02', '', '', 0, 1, '', '', '0000-00-00 00:00:00', 0),
(23, 'e18e90149a3452e2ca50b9ab2d7ef7fd', 'nom@gmail.com', 'ร้านนม', '02', '', 'tt', 0, 1, '', '', '0000-00-00 00:00:00', 1),
(24, '01adba99f817adcac1dc48ae24f298d7', 'na@mail.com', 'ร้านนาย', '02', '', 'na', 1, 1, '', '', '0000-00-00 00:00:00', 1);

-- --------------------------------------------------------

--
-- Table structure for table `ma_business_category`
--

CREATE TABLE `ma_business_category` (
  `ID` int(11) NOT NULL,
  `NAME` varchar(300) COLLATE utf8_unicode_ci NOT NULL,
  `DETAIL` varchar(300) COLLATE utf8_unicode_ci NOT NULL,
  `IsActive` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `ma_business_category`
--

INSERT INTO `ma_business_category` (`ID`, `NAME`, `DETAIL`, `IsActive`) VALUES
(1, 'ทั่วไป', '', 1);

-- --------------------------------------------------------

--
-- Table structure for table `ma_business_type`
--

CREATE TABLE `ma_business_type` (
  `ID` int(11) NOT NULL,
  `NAME` varchar(300) COLLATE utf8_unicode_ci NOT NULL,
  `DETAIL` varchar(300) COLLATE utf8_unicode_ci NOT NULL,
  `IsActive` int(11) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `ma_business_type`
--

INSERT INTO `ma_business_type` (`ID`, `NAME`, `DETAIL`, `IsActive`) VALUES
(1, 'ร้านอาหาร', 'ขายอาหารทั่วไป', 1),
(2, 'ของของชำ', 'ขายของทั่วไป', 1),
(3, 'ของเล่น', '', 1);

-- --------------------------------------------------------

--
-- Table structure for table `ma_customer`
--

CREATE TABLE `ma_customer` (
  `ID` int(11) NOT NULL,
  `NAME` varchar(300) COLLATE utf8_unicode_ci NOT NULL,
  `TEL` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `EMAIL` varchar(300) COLLATE utf8_unicode_ci NOT NULL,
  `CATEGORY_ID` int(11) NOT NULL,
  `BUSINESS_ID` int(11) NOT NULL,
  `AMOUNTBUY` int(11) NOT NULL,
  `REWARDPOINT` int(11) NOT NULL,
  `NOTE` varchar(300) COLLATE utf8_unicode_ci NOT NULL,
  `IsActive` int(11) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `ma_customer`
--

INSERT INTO `ma_customer` (`ID`, `NAME`, `TEL`, `EMAIL`, `CATEGORY_ID`, `BUSINESS_ID`, `AMOUNTBUY`, `REWARDPOINT`, `NOTE`, `IsActive`) VALUES
(1, 'ลูกค้า A', '02', 'f@gmail.com', 1, 18, 0, 0, '', 1),
(2, 'นาย b', '02', 'bb@gmail.com', 1, 18, 0, 0, '', 1),
(3, 'นาย C', '02', 'ccc@gmad.com', 1, 18, 0, 0, '', 1),
(4, '213', '123', '123', 1, 18, 0, 0, '123', 1),
(5, 'dwdw', 'dwdw', 'dwd', 1, 18, 0, 0, 'dwdw', 1),
(6, 'กไไก', 'กไก', 'กไก', 1, 18, 0, 0, 'กไกไ', 1);

-- --------------------------------------------------------

--
-- Table structure for table `ma_customer_category`
--

CREATE TABLE `ma_customer_category` (
  `ID` int(11) NOT NULL,
  `BUSINESS_ID` int(11) NOT NULL,
  `NAME` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `DETAIL` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `IsActive` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `ma_customer_category`
--

INSERT INTO `ma_customer_category` (`ID`, `BUSINESS_ID`, `NAME`, `DETAIL`, `IsActive`) VALUES
(1, 18, 'ทั่วไป', '', 1),
(2, 18, 'พิเศษ', '', 1);

-- --------------------------------------------------------

--
-- Table structure for table `ma_discount`
--

CREATE TABLE `ma_discount` (
  `ID` int(11) NOT NULL,
  `NAME` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `DETAIL` varchar(300) COLLATE utf8_unicode_ci NOT NULL,
  `DISCOUNT` int(11) NOT NULL,
  `BAHTORPERCENT` int(11) NOT NULL,
  `BUSINESS_ID` int(11) NOT NULL,
  `EXPIRATIONDATE` date NOT NULL,
  `IsActive` int(11) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `ma_discount`
--

INSERT INTO `ma_discount` (`ID`, `NAME`, `DETAIL`, `DISCOUNT`, `BAHTORPERCENT`, `BUSINESS_ID`, `EXPIRATIONDATE`, `IsActive`) VALUES
(1, 'วันเกิด', '', 10, 0, 18, '2020-04-24', 1),
(2, 'วันปีใหม่', '', 20, 1, 18, '2020-04-29', 1);

-- --------------------------------------------------------

--
-- Table structure for table `ma_goods`
--

CREATE TABLE `ma_goods` (
  `ID` int(11) NOT NULL,
  `UID` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `NAME` varchar(300) COLLATE utf8_unicode_ci NOT NULL,
  `IMAGE` varchar(300) COLLATE utf8_unicode_ci NOT NULL,
  `DETAIL` varchar(300) COLLATE utf8_unicode_ci NOT NULL,
  `STATUS` tinyint(1) DEFAULT '1',
  `CATEGORY_ID` int(11) NOT NULL,
  `SOLDBY` int(11) NOT NULL,
  `PRICE` int(11) NOT NULL,
  `COST` int(11) NOT NULL,
  `SKU` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `BARCODE` varchar(300) COLLATE utf8_unicode_ci NOT NULL,
  `COMPOSITE` varchar(300) COLLATE utf8_unicode_ci NOT NULL,
  `STOCK` int(11) NOT NULL,
  `BUSINESS_ID` int(11) NOT NULL,
  `IsActive` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `ma_goods`
--

INSERT INTO `ma_goods` (`ID`, `UID`, `NAME`, `IMAGE`, `DETAIL`, `STATUS`, `CATEGORY_ID`, `SOLDBY`, `PRICE`, `COST`, `SKU`, `BARCODE`, `COMPOSITE`, `STOCK`, `BUSINESS_ID`, `IsActive`) VALUES
(1, '15886650371', 'น้ำส้มปั่น', 'imgGoods_2020-04-29-16-57-19.png', '', 1, 2, 0, 30, 10, '101', '158866503715886650371.png', '', 0, 18, 1),
(2, '15886650402', 'ขนมไทย', 'imgGoods_2020-04-29-16-57-27.png', '', 1, 3, 0, 5, 1, '102', '158866504015886650402.png', '', 0, 18, 1),
(3, '15886650453', 'น้ำมะนาว', 'imgGoods_2020-04-29-16-57-31.png', '', 1, 2, 0, 15, 10, '10', '158866504615886650453.png', '', 0, 18, 1),
(4, '15886650704', 'น้ำเขียว', 'imgGoods_2020-05-03-12-52-20.png', '', 1, 3, 0, 100, 10, '11', '158866507015886650704.png', '', 0, 18, 1),
(5, '15886650685', 'น้ำปีโป้นมสด', 'imgGoods_2020-05-03-12-52-43.png', '', 1, 1, 0, 60, 20, '111', '158866506815886650685.png', '', 0, 18, 1),
(6, '15886650676', 'น้ำแดงมะนาว', 'imgGoods_2020-05-05-14-22-19.png', '', 1, 2, 0, 30, 20, '100', '158866506715886650676.png', '', 0, 18, 1),
(7, '15886650657', 'น้ำเขียวมะนาว', 'imgGoods_2020-05-05-13-38-30.png', '', 1, 2, 0, 50, 20, '111', '158866506515886650657.png', '', 0, 18, 1),
(8, '15886650648', 'น้ำส้มมะนาว', 'imgGoods_2020-05-05-13-40-56.png', '', 1, 2, 0, 10, 5, '111', '158866506415886650648.png', '', 0, 18, 1),
(9, '15886650619', 'น้ำปั๊นปั่น', 'imgGoods_2020-05-05-13-43-12.png', '', 1, 2, 0, 20, 10, '200', '158866506115886650619.png', '', 0, 18, 1);

-- --------------------------------------------------------

--
-- Table structure for table `ma_goods_category`
--

CREATE TABLE `ma_goods_category` (
  `ID` int(11) NOT NULL,
  `BUSINESS_ID` int(11) NOT NULL,
  `NAME` varchar(300) COLLATE utf8_unicode_ci NOT NULL,
  `DETAIL` varchar(300) COLLATE utf8_unicode_ci NOT NULL,
  `COLOR` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `IsActive` int(11) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `ma_goods_category`
--

INSERT INTO `ma_goods_category` (`ID`, `BUSINESS_ID`, `NAME`, `DETAIL`, `COLOR`, `IsActive`) VALUES
(1, 18, 'อาหาร', 'อาหารทั่ว ๆไป', '', 1),
(2, 18, 'น้ำ', 'น้ำทั่วๆไป', '', 1),
(3, 18, 'ขนม', '', '', 1);

-- --------------------------------------------------------

--
-- Table structure for table `ma_permission`
--

CREATE TABLE `ma_permission` (
  `ID` int(11) NOT NULL,
  `NAME` varchar(300) COLLATE utf8_unicode_ci NOT NULL,
  `DETAIL` varchar(300) COLLATE utf8_unicode_ci NOT NULL,
  `TYPE` int(11) NOT NULL,
  `IsActive` int(11) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `ma_role`
--

CREATE TABLE `ma_role` (
  `ID` int(11) NOT NULL,
  `NAME` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `BUSINESS_ID` int(11) NOT NULL,
  `PERMISSION_DASHBOARD_IsActive` int(11) NOT NULL DEFAULT '0',
  `PERMISSION_POS_IsActive` int(11) NOT NULL DEFAULT '0',
  `PERMISSION_POS` varchar(300) COLLATE utf8_unicode_ci NOT NULL,
  `PERMISSION_DASHBOARD` varchar(300) COLLATE utf8_unicode_ci NOT NULL,
  `MASTER` int(11) NOT NULL DEFAULT '0',
  `IsActive` int(11) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `ma_role`
--

INSERT INTO `ma_role` (`ID`, `NAME`, `BUSINESS_ID`, `PERMISSION_DASHBOARD_IsActive`, `PERMISSION_POS_IsActive`, `PERMISSION_POS`, `PERMISSION_DASHBOARD`, `MASTER`, `IsActive`) VALUES
(64, 'เจ้าของ', 18, 1, 1, '', '', 1, 1),
(66, 'ผู้จัดการ', 18, 1, 1, '', '', 0, 1),
(67, 'แคชเชียร์', 18, 0, 1, '', '', 0, 1),
(68, 'เจ้าของ', 19, 1, 1, '', '', 1, 1),
(70, 'ผู้จัดการ', 19, 1, 1, '', '', 0, 1),
(71, 'แคชเชียร์', 19, 0, 1, '', '', 0, 1),
(72, 'เจ้าของ', 20, 1, 1, '', '', 1, 1),
(74, 'ผู้จัดการ', 20, 1, 1, '', '', 0, 1),
(75, 'แคชเชียร์', 20, 0, 1, '', '', 0, 1),
(77, 'เจ้าของ', 21, 1, 1, '', '', 1, 1),
(79, 'ผู้จัดการ', 21, 1, 1, '', '', 0, 1),
(80, 'แคชเชียร์', 21, 0, 1, '', '', 0, 1),
(81, 'เจ้าของ', 22, 1, 1, '', '', 1, 1),
(83, 'ผู้จัดการ', 22, 1, 1, '', '', 0, 1),
(84, 'แคชเชียร์', 22, 0, 1, '', '', 0, 1),
(85, 'เจ้าของ', 23, 1, 1, '', '', 1, 1),
(87, 'ผู้จัดการ', 23, 1, 1, '', '', 0, 1),
(88, 'แคชเชียร์', 23, 0, 1, '', '', 0, 1),
(89, 'เจ้าของ', 24, 1, 1, '', '', 1, 1),
(91, 'ผู้จัดการ', 24, 1, 1, '', '', 0, 1),
(92, 'แคชเชียร์', 24, 0, 1, '', '', 0, 1);

-- --------------------------------------------------------

--
-- Table structure for table `ma_user`
--

CREATE TABLE `ma_user` (
  `ID` int(11) NOT NULL,
  `EMAIL` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `PASSWORD` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `NAME` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `TEL` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `IMAGE` varchar(300) COLLATE utf8_unicode_ci NOT NULL,
  `IsActive` int(11) DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `ma_user`
--

INSERT INTO `ma_user` (`ID`, `EMAIL`, `PASSWORD`, `NAME`, `TEL`, `IMAGE`, `IsActive`) VALUES
(9, 'admin@pos.com', '202cb962ac59075b964b07152d234b70', 'admin', '', '', 1),
(20, 'f@gmail.com', '202cb962ac59075b964b07152d234b70', 'เจ้าของ', '034', 'imgUser_2020-04-23-17-56-14.png', 1),
(21, 'game@gmail.com', '202cb962ac59075b964b07152d234b70', 'เจ้าของ', '', '', 1),
(22, 'japan@gmail.com', '202cb962ac59075b964b07152d234b70', 'เจ้าของ', '', '', 1),
(23, 'mon@a.com', '202cb962ac59075b964b07152d234b70', 'เจ้าของ', '', '', 1),
(29, 'ff@gmail.com', '202cb962ac59075b964b07152d234b70', 'นาย A', '02', 'imgUser_2020-04-23-17-56-44.png', 1),
(30, 'fff@gmail.com', '202cb962ac59075b964b07152d234b70', 'นาย b', '02', '', 1),
(31, 'gggg@gmad.com', '202cb962ac59075b964b07152d234b70', 'เจ้าของ', '', '', 1),
(32, 'nom@gmail.com', '202cb962ac59075b964b07152d234b70', '', '', '', 1),
(33, 'na@mail.com', '202cb962ac59075b964b07152d234b70', 'na', '', '', 1),
(34, 'wwfff@gmail.com', '202cb962ac59075b964b07152d234b70', 'นาย b', '02', 'imgUser_2020-04-23-17-54-34.png', 1),
(35, 'test@', '202cb962ac59075b964b07152d234b70', 'test', '02', '', 1),
(36, 'dad@', '202cb962ac59075b964b07152d234b70', 'พนักงาน', '02', '', 1);

-- --------------------------------------------------------

--
-- Table structure for table `ma_user_position`
--

CREATE TABLE `ma_user_position` (
  `ID` int(11) NOT NULL,
  `USER_ID` int(11) NOT NULL,
  `BUSINESS_ID` int(11) NOT NULL,
  `BRANCH_ID` int(11) NOT NULL,
  `ROLE_ID` int(11) NOT NULL,
  `IsActive` int(11) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `ma_user_position`
--

INSERT INTO `ma_user_position` (`ID`, `USER_ID`, `BUSINESS_ID`, `BRANCH_ID`, `ROLE_ID`, `IsActive`) VALUES
(5, 20, 18, 21, 64, 1),
(7, 21, 19, 22, 68, 1),
(9, 22, 20, 25, 72, 1),
(11, 23, 21, 26, 77, 1),
(14, 30, 18, 20, 67, 1),
(15, 31, 22, 27, 81, 1),
(16, 32, 23, 28, 85, 1),
(17, 33, 24, 29, 89, 1),
(18, 34, 18, 23, 66, 1),
(19, 35, 18, 24, 66, 1),
(20, 36, 18, 21, 67, 1);

-- --------------------------------------------------------

--
-- Table structure for table `report_refund`
--

CREATE TABLE `report_refund` (
  `ID` int(11) NOT NULL,
  `TRANSECTION_ID` int(11) NOT NULL,
  `AMOUNT` int(11) NOT NULL,
  `TimeStamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `report_refund`
--

INSERT INTO `report_refund` (`ID`, `TRANSECTION_ID`, `AMOUNT`, `TimeStamp`) VALUES
(2, 34, 5, '2020-05-03 16:17:39'),
(3, 34, 15, '2020-05-03 16:17:39'),
(4, 34, 100, '2020-05-03 16:17:39'),
(5, 34, 100, '2020-05-03 16:17:39'),
(6, 34, 100, '2020-05-03 16:17:39'),
(7, 42, 60, '2020-05-04 05:11:23'),
(8, 46, 5, '2020-05-04 05:11:38'),
(9, 29, 30, '2020-05-04 05:50:31'),
(10, 29, 5, '2020-05-04 05:50:31'),
(11, 29, 15, '2020-05-04 05:50:31');

-- --------------------------------------------------------

--
-- Table structure for table `ts_discount`
--

CREATE TABLE `ts_discount` (
  `ID` int(11) NOT NULL,
  `TRANSECTION_ID` int(11) NOT NULL,
  `AMOUNT_DISCOUNT` int(11) NOT NULL,
  `TimeStamp` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `ts_discount`
--

INSERT INTO `ts_discount` (`ID`, `TRANSECTION_ID`, `AMOUNT_DISCOUNT`, `TimeStamp`) VALUES
(5, 29, 10, 0),
(6, 30, 10, 0),
(7, 31, 0, 0),
(8, 32, 113, 0),
(9, 33, 0, 0),
(10, 34, 64, 0),
(11, 35, 10, 0),
(12, 36, 28, 0),
(13, 37, 28, 0),
(14, 38, 28, 0),
(15, 39, 28, 0),
(16, 40, 12, 0),
(17, 41, 12, 0),
(18, 42, 12, 0),
(19, 43, 12, 0),
(20, 44, 10, 0),
(21, 45, 10, 0),
(22, 46, 0, 0),
(23, 47, 10, 0),
(24, 48, 10, 0),
(25, 49, 10, 0),
(26, 50, 10, 0),
(27, 51, 10, 0),
(28, 52, 10, 0),
(29, 53, 12, 0),
(30, 54, 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `ts_transection`
--

CREATE TABLE `ts_transection` (
  `ID` int(11) NOT NULL,
  `BUSINESS_ID` int(11) NOT NULL,
  `CUSTOMER_ID` int(11) NOT NULL,
  `ORDER_NAME` varchar(300) COLLATE utf8_unicode_ci NOT NULL,
  `CATEGORYPAYMENT` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `SELLER_ID` int(11) NOT NULL,
  `TotalCost` int(11) NOT NULL,
  `TotalOrder` int(11) NOT NULL,
  `Received` int(11) NOT NULL,
  `Change` int(11) NOT NULL,
  `TimeStamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `IsActive` int(11) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `ts_transection`
--

INSERT INTO `ts_transection` (`ID`, `BUSINESS_ID`, `CUSTOMER_ID`, `ORDER_NAME`, `CATEGORYPAYMENT`, `SELLER_ID`, `TotalCost`, `TotalOrder`, `Received`, `Change`, `TimeStamp`, `IsActive`) VALUES
(29, 18, 0, '201588508985434', 'เงินสด', 20, 0, 20, 100, 60, '2020-05-04 05:50:31', 0),
(30, 18, 0, '201588509025719', 'พร้อมเพย์', 20, 0, 240, 240, 0, '2020-05-01 12:30:25', 1),
(31, 18, 0, '201588509054875', 'เงินสด', 20, 0, 95, 200, 90, '2020-05-03 12:32:03', 1),
(32, 18, 0, '201588509096352', 'พร้อมเพย์', 20, 0, 452, 452, 0, '2020-04-30 12:31:36', 1),
(33, 18, 0, '201588509143665', 'เงินสด', 20, 0, 330, 500, 170, '2020-04-29 12:32:24', 1),
(34, 18, 0, '201588509155974', 'เงินสด', 20, 0, 256, 500, 244, '2020-05-03 16:17:39', 0),
(35, 18, 2, '201588523272700', 'พร้อมเพย์', 20, 0, 130, 130, 0, '2020-05-03 16:27:52', 1),
(36, 18, 0, '201588564846601', 'เงินสด', 20, 0, 112, 200, 88, '2020-05-04 04:00:46', 1),
(37, 18, 0, '201588564910492', 'พร้อมเพย์', 20, 0, 112, 112, 0, '2020-05-04 04:01:50', 1),
(38, 18, 0, '201588564977003', 'พร้อมเพย์', 20, 0, 112, 112, 0, '2020-05-03 04:02:57', 1),
(39, 18, 0, '201588565016342', 'เงินสด', 20, 0, 112, 200, 88, '2020-05-04 04:03:36', 1),
(40, 18, 0, '201588565067407', 'เงินสด', 20, 0, 33, 100, 67, '2020-05-03 04:04:27', 1),
(41, 18, 0, '201588565122077', 'เงินสด', 20, 0, 48, 100, 52, '2020-05-04 04:05:22', 1),
(42, 18, 4, '201588565150828', 'เงินสด', 20, 0, 48, 100, 52, '2020-05-04 05:11:23', 0),
(43, 18, 0, '201588565186122', 'เงินสด', 20, 0, 48, 100, 52, '2020-05-04 04:06:26', 1),
(44, 18, 0, '201588565290146', 'เงินสด', 20, 0, 45, 100, 55, '2020-03-04 04:08:10', 1),
(45, 18, 4, '201588565404537', 'เงินสด', 20, 0, 40, 100, 60, '2020-02-04 04:10:04', 1),
(46, 18, 0, '201588565646610', 'เงินสด', 20, 0, 5, 50, 45, '2020-05-04 05:11:38', 0),
(47, 18, 0, '201588565731463', 'เงินสด', 20, 0, 125, 200, 75, '2020-05-04 04:15:31', 1),
(48, 18, 3, '201588565902048', 'พร้อมเพย์', 20, 0, 0, 0, 0, '2020-05-04 04:18:22', 1),
(49, 18, 2, '201588565916223', 'พร้อมเพย์', 20, 0, 90, 90, 0, '2020-05-04 04:18:36', 1),
(50, 18, 0, '201588571364834', 'พร้อมเพย์', 20, 0, 25, 25, 0, '2020-05-04 05:49:25', 1),
(51, 18, 0, '201588571398456', 'พร้อมเพย์', 20, 0, 20, 20, 0, '2020-05-04 05:49:58', 1),
(52, 18, 0, '201588571503565', 'เงินสด', 20, 0, 390, 500, 110, '2020-05-04 05:51:43', 1),
(53, 18, 0, '201588572116928', 'เงินสด', 20, 0, 48, 100, 52, '2020-05-04 06:01:57', 1),
(54, 18, 0, '201588585368903', 'พร้อมเพย์', 20, 0, 100, 100, 0, '2020-05-04 09:42:48', 1);

-- --------------------------------------------------------

--
-- Table structure for table `ts_transection_detail`
--

CREATE TABLE `ts_transection_detail` (
  `ID` int(11) NOT NULL,
  `TRANSECTION_ID` int(11) NOT NULL,
  `GOODS_ID` int(11) NOT NULL,
  `PRICE` int(11) NOT NULL,
  `AMOUNT` int(11) NOT NULL,
  `IsActive` int(11) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `ts_transection_detail`
--

INSERT INTO `ts_transection_detail` (`ID`, `TRANSECTION_ID`, `GOODS_ID`, `PRICE`, `AMOUNT`, `IsActive`) VALUES
(83, 29, 1, 30, 0, 0),
(84, 29, 2, 5, 0, 0),
(85, 29, 3, 15, 0, 0),
(86, 30, 1, 30, 0, 1),
(87, 30, 2, 5, 0, 1),
(88, 30, 3, 15, 0, 1),
(89, 30, 4, 100, 0, 1),
(90, 30, 4, 100, 0, 1),
(91, 31, 1, 30, 0, 1),
(92, 31, 1, 30, 0, 1),
(93, 31, 2, 5, 0, 1),
(94, 31, 2, 5, 0, 1),
(95, 31, 2, 5, 0, 1),
(96, 31, 2, 5, 0, 1),
(97, 31, 3, 15, 0, 1),
(98, 31, 3, 15, 0, 0),
(99, 32, 3, 15, 0, 1),
(100, 32, 3, 15, 0, 1),
(101, 32, 4, 100, 0, 1),
(102, 32, 4, 100, 0, 1),
(103, 32, 4, 100, 0, 1),
(104, 32, 4, 100, 0, 1),
(105, 32, 4, 100, 0, 1),
(106, 32, 3, 15, 0, 1),
(107, 32, 3, 15, 0, 1),
(108, 32, 2, 5, 0, 1),
(109, 33, 2, 5, 0, 1),
(110, 33, 5, 60, 0, 1),
(111, 33, 5, 60, 0, 1),
(112, 33, 2, 5, 0, 1),
(113, 33, 4, 100, 0, 1),
(114, 33, 4, 100, 0, 1),
(115, 34, 2, 5, 0, 0),
(116, 34, 3, 15, 0, 0),
(117, 34, 4, 100, 0, 0),
(118, 34, 4, 100, 0, 0),
(119, 34, 4, 100, 0, 0),
(120, 35, 2, 5, 0, 1),
(121, 35, 2, 5, 0, 1),
(122, 35, 3, 15, 0, 1),
(123, 35, 4, 100, 0, 1),
(124, 35, 3, 15, 0, 1),
(125, 36, 5, 60, 0, 1),
(126, 36, 1, 30, 0, 1),
(127, 36, 2, 5, 0, 1),
(128, 36, 3, 15, 0, 1),
(129, 36, 1, 30, 0, 1),
(130, 37, 5, 60, 0, 1),
(131, 37, 1, 30, 0, 1),
(132, 37, 2, 5, 0, 1),
(133, 37, 3, 15, 0, 1),
(134, 37, 1, 30, 0, 1),
(135, 38, 5, 60, 0, 1),
(136, 38, 1, 30, 0, 1),
(137, 38, 2, 5, 0, 1),
(138, 38, 3, 15, 0, 1),
(139, 38, 1, 30, 0, 1),
(140, 39, 5, 60, 0, 1),
(141, 39, 1, 30, 0, 1),
(142, 39, 2, 5, 0, 1),
(143, 39, 3, 15, 0, 1),
(144, 39, 1, 30, 0, 1),
(145, 40, 5, 60, 0, 1),
(146, 41, 5, 60, 0, 1),
(147, 42, 5, 60, 0, 0),
(148, 43, 5, 60, 0, 1),
(149, 44, 2, 5, 0, 1),
(150, 44, 1, 30, 0, 1),
(151, 44, 2, 5, 0, 1),
(152, 44, 3, 15, 0, 1),
(153, 45, 1, 30, 0, 1),
(154, 45, 2, 5, 0, 1),
(155, 45, 3, 15, 0, 1),
(156, 46, 2, 5, 0, 0),
(157, 47, 1, 30, 0, 1),
(158, 47, 2, 5, 0, 1),
(159, 47, 4, 100, 0, 1),
(160, 48, 2, 5, 0, 1),
(161, 48, 2, 5, 0, 1),
(162, 49, 5, 60, 0, 1),
(163, 49, 1, 30, 0, 1),
(164, 49, 2, 5, 0, 1),
(165, 49, 2, 5, 0, 1),
(166, 50, 2, 5, 0, 1),
(167, 50, 1, 30, 0, 1),
(168, 51, 1, 30, 0, 1),
(169, 52, 4, 100, 0, 1),
(170, 52, 4, 100, 0, 1),
(171, 52, 4, 100, 0, 1),
(172, 52, 4, 100, 0, 1),
(173, 53, 5, 60, 0, 1),
(174, 54, 4, 100, 0, 1);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `ma_branch`
--
ALTER TABLE `ma_branch`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `ma_business`
--
ALTER TABLE `ma_business`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `ma_business_category`
--
ALTER TABLE `ma_business_category`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `ma_business_type`
--
ALTER TABLE `ma_business_type`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `ma_customer`
--
ALTER TABLE `ma_customer`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `ma_customer_category`
--
ALTER TABLE `ma_customer_category`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `ma_discount`
--
ALTER TABLE `ma_discount`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `ma_goods`
--
ALTER TABLE `ma_goods`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `ma_goods_category`
--
ALTER TABLE `ma_goods_category`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `ma_permission`
--
ALTER TABLE `ma_permission`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `ma_role`
--
ALTER TABLE `ma_role`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `ma_user`
--
ALTER TABLE `ma_user`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `ma_user_position`
--
ALTER TABLE `ma_user_position`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `report_refund`
--
ALTER TABLE `report_refund`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `ts_discount`
--
ALTER TABLE `ts_discount`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `ts_transection`
--
ALTER TABLE `ts_transection`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `ts_transection_detail`
--
ALTER TABLE `ts_transection_detail`
  ADD PRIMARY KEY (`ID`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `ma_branch`
--
ALTER TABLE `ma_branch`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=30;
--
-- AUTO_INCREMENT for table `ma_business`
--
ALTER TABLE `ma_business`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=25;
--
-- AUTO_INCREMENT for table `ma_business_category`
--
ALTER TABLE `ma_business_category`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `ma_business_type`
--
ALTER TABLE `ma_business_type`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `ma_customer`
--
ALTER TABLE `ma_customer`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `ma_customer_category`
--
ALTER TABLE `ma_customer_category`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `ma_discount`
--
ALTER TABLE `ma_discount`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `ma_goods`
--
ALTER TABLE `ma_goods`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;
--
-- AUTO_INCREMENT for table `ma_goods_category`
--
ALTER TABLE `ma_goods_category`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `ma_permission`
--
ALTER TABLE `ma_permission`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `ma_role`
--
ALTER TABLE `ma_role`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=93;
--
-- AUTO_INCREMENT for table `ma_user`
--
ALTER TABLE `ma_user`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=37;
--
-- AUTO_INCREMENT for table `ma_user_position`
--
ALTER TABLE `ma_user_position`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;
--
-- AUTO_INCREMENT for table `report_refund`
--
ALTER TABLE `report_refund`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;
--
-- AUTO_INCREMENT for table `ts_discount`
--
ALTER TABLE `ts_discount`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=31;
--
-- AUTO_INCREMENT for table `ts_transection`
--
ALTER TABLE `ts_transection`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=55;
--
-- AUTO_INCREMENT for table `ts_transection_detail`
--
ALTER TABLE `ts_transection_detail`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=175;COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
