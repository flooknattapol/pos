'use strict';
myApp.factory('vendorsalelogApiService', ['$http', '$resource', 'baseService', function ($http, $resource, baseService) {
    var servicebase = get_base_url("VendorSaleLog/");
    return {
          
        //VendorSaleLog 
	    listVendorSaleLog: function (model, onComplete) {
            baseService.searchObject(servicebase + 'getVendorSaleLogModel', model, function (result) {
                if (undefined != onComplete) onComplete.call(this, result);
            });
        },
		getVendorSaleLog: function (model, onComplete) {
            baseService.postObject(servicebase + 'getVendorSaleLogModel', model, function (result) {
                if (undefined != onComplete) onComplete.call(this, result);
            });
        },
        saveVendorSaleLog: function (model, onComplete) {
            baseService.postObject(servicebase + 'addVendorSaleLog', model, function (result) {
                if (undefined != onComplete) onComplete.call(this, result);
            });
        },
        deleteVendorSaleLog: function (model, onComplete) {
            baseService.postObject(servicebase + 'deleteVendorSaleLog', model, function (result) {
                if (undefined != onComplete) onComplete.call(this, result);
            });
        },
		getComboBox: function (model, onComplete) {
            baseService.searchObject(servicebase + 'getVendorSaleLogComboList', model, function (result) {
                if (undefined != onComplete) onComplete.call(this, result);
            });
        },
        getComboBoxVat: function (model, onComplete) {
            baseService.searchObject(servicebase + 'getVendorSaleLogComboListVat', model, function (result) {
                if (undefined != onComplete) onComplete.call(this, result);
            });
        },
        calDuration: function (model, onComplete) {
            baseService.postObject(servicebase + 'calDuration', model, function (result) {
                if (undefined != onComplete) onComplete.call(this, result);
            });
        },
        
    };
}]);