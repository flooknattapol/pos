'use strict';
myApp.factory('goodstypeApiService', ['$http', '$resource', 'baseService', function ($http, $resource, baseService) {
    var servicebase = get_base_url("GoodsType/");
    return {
          
        //GoodsType 
	    // listGoodsType: function (model, onComplete) {
        //     baseService.searchObject(servicebase + 'getGoodsTypeModel', model, function (result) {
        //         if (undefined != onComplete) onComplete.call(this, result);
        //     });
        // },
		// getGoodsType: function (model, onComplete) {
        //     baseService.postObject(servicebase + 'getGoodsTypeModel', model, function (result) {
        //         if (undefined != onComplete) onComplete.call(this, result);
        //     });
        // },
        saveGoodsType: function (model, onComplete) {
            baseService.postObject(servicebase + 'addGoodsType', model, function (result) {
                if (undefined != onComplete) onComplete.call(this, result);
            });
        },
        deleteGoodsType: function (model, onComplete) {
            baseService.postObject(servicebase + 'deleteGoodsType', model, function (result) {
                if (undefined != onComplete) onComplete.call(this, result);
            });
        },
		getComboBox: function (model, onComplete) {
            baseService.searchObject(servicebase + 'getGoodsTypeComboList', model, function (result) {
                if (undefined != onComplete) onComplete.call(this, result);
            });
        },
        getTypeComboBox: function (model, onComplete) {
            baseService.searchObject(servicebase + 'getTypeGoodsTypeComboList', model, function (result) {
                if (undefined != onComplete) onComplete.call(this, result);
            });
        },
        getComboBoxVat: function (model, onComplete) {
            baseService.searchObject(servicebase + 'getGoodsTypeComboListVat', model, function (result) {
                if (undefined != onComplete) onComplete.call(this, result);
            });
        },
        calDuration: function (model, onComplete) {
            baseService.postObject(servicebase + 'calDuration', model, function (result) {
                if (undefined != onComplete) onComplete.call(this, result);
            });
        },
        
    };
}]);