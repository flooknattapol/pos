'use strict';
myApp.factory('memberfriendApiService', ['$http', '$resource', 'baseService', function ($http, $resource, baseService) {
    var servicebase = get_base_url("MemberFriend/");
    return {
          
        //MemberFriend 
	    listMemberFriend: function (model, onComplete) {
            baseService.searchObject(servicebase + 'getMemberFriendModel', model, function (result) {
                if (undefined != onComplete) onComplete.call(this, result);
            });
        },
		getMemberFriend: function (model, onComplete) {
            baseService.postObject(servicebase + 'getMemberFriendModel', model, function (result) {
                if (undefined != onComplete) onComplete.call(this, result);
            });
        },
        saveMemberFriend: function (model, onComplete) {
            baseService.postObject(servicebase + 'addMemberFriend', model, function (result) {
                if (undefined != onComplete) onComplete.call(this, result);
            });
        },
        deleteMemberFriend: function (model, onComplete) {
            baseService.postObject(servicebase + 'deleteMemberFriend', model, function (result) {
                if (undefined != onComplete) onComplete.call(this, result);
            });
        },
		getComboBox: function (model, onComplete) {
            baseService.searchObject(servicebase + 'getMemberFriendComboList', model, function (result) {
                if (undefined != onComplete) onComplete.call(this, result);
            });
        },
        getComboBoxVat: function (model, onComplete) {
            baseService.searchObject(servicebase + 'getMemberFriendComboListVat', model, function (result) {
                if (undefined != onComplete) onComplete.call(this, result);
            });
        },
        calDuration: function (model, onComplete) {
            baseService.postObject(servicebase + 'calDuration', model, function (result) {
                if (undefined != onComplete) onComplete.call(this, result);
            });
        }, 
        getFriendMemberList: function (model, onComplete) {
            baseService.postObject(servicebase + 'getFriendMemberList', model, function (result) {
                if (undefined != onComplete) onComplete.call(this, result);
            });
        },
        
    };
}]);