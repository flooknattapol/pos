'use strict';
myApp.factory('memberregisterlogApiService', ['$http', '$resource', 'baseService', function ($http, $resource, baseService) {
    var servicebase = get_base_url("MemberRegisterLog/");
    return {
          
        //MemberRegisterLog 
	    listMemberRegisterLog: function (model, onComplete) {
            baseService.searchObject(servicebase + 'getMemberRegisterLogModel', model, function (result) {
                if (undefined != onComplete) onComplete.call(this, result);
            });
        },
		getMemberRegisterLog: function (model, onComplete) {
            baseService.postObject(servicebase + 'getMemberRegisterLogModel', model, function (result) {
                if (undefined != onComplete) onComplete.call(this, result);
            });
        },
        saveMemberRegisterLog: function (model, onComplete) {
            baseService.postObject(servicebase + 'addMemberRegisterLog', model, function (result) {
                if (undefined != onComplete) onComplete.call(this, result);
            });
        },
        deleteMemberRegisterLog: function (model, onComplete) {
            baseService.postObject(servicebase + 'deleteMemberRegisterLog', model, function (result) {
                if (undefined != onComplete) onComplete.call(this, result);
            });
        },
		getComboBox: function (model, onComplete) {
            baseService.searchObject(servicebase + 'getMemberRegisterLogComboList', model, function (result) {
                if (undefined != onComplete) onComplete.call(this, result);
            });
        },
        getComboBoxVat: function (model, onComplete) {
            baseService.searchObject(servicebase + 'getMemberRegisterLogComboListVat', model, function (result) {
                if (undefined != onComplete) onComplete.call(this, result);
            });
        },
        calDuration: function (model, onComplete) {
            baseService.postObject(servicebase + 'calDuration', model, function (result) {
                if (undefined != onComplete) onComplete.call(this, result);
            });
        },
        
    };
}]);