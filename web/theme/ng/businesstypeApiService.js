'use strict';
myApp.factory('businesstypeApiService', ['$http', '$resource', 'baseService', function ($http, $resource, baseService) {
    var servicebase = get_base_url("BusinessType/");
    return {
          
       //BusinessType
	    getList: function (model, onComplete) {
            baseService.searchObject(servicebase + 'getList', model, function (result) {
                if (undefined != onComplete) onComplete.call(this, result);
            });
        },
		getDetail: function (model, onComplete) {
            baseService.postObject(servicebase + 'getDetail', model, function (result) {
                if (undefined != onComplete) onComplete.call(this, result);
            });
        },
        save: function (model, onComplete) {
            baseService.postObject(servicebase + 'add', model, function (result) {
                if (undefined != onComplete) onComplete.call(this, result);
            });
        },
        delete: function (model, onComplete) {
            baseService.postObject(servicebase + 'delete', model, function (result) {
                if (undefined != onComplete) onComplete.call(this, result);
            });
        },
		getComboList: function (model, onComplete) {
            baseService.searchObject(servicebase + 'getComboList', model, function (result) {
                if (undefined != onComplete) onComplete.call(this, result);
            });
        }
        
    };
}]);