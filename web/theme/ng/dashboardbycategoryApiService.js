'use strict';
myApp.factory('dashboardbycategoryApiService', ['$http', '$resource', 'baseService', function ($http, $resource, baseService) {
    var servicebase = get_base_url("DashboardByCategory/");
    return {
          
        //dashboardbycategorybygoods 
	    listdashboardbycategory: function (model, onComplete) {
            baseService.searchObject(servicebase + 'getdashboardbycategoryModel', model, function (result) {
                if (undefined != onComplete) onComplete.call(this, result);
            });
        },
		getdashboardbycategoryAlll: function (model, onComplete) {
            baseService.postObject(servicebase + 'getdashboardbycategoryAlll', model, function (result) {
                if (undefined != onComplete) onComplete.call(this, result);
            });
        }
    };
}]);