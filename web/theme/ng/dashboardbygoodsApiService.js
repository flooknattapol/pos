'use strict';
myApp.factory('dashboardbygoodsApiService', ['$http', '$resource', 'baseService', function ($http, $resource, baseService) {
    var servicebase = get_base_url("DashboardByGoods/");
    return {
          
        getHeader: function (model, onComplete) {
            baseService.postObject(servicebase + 'getHeader', model, function (result) {
                if (undefined != onComplete) onComplete.call(this, result);
            });
        },
        getList: function (model, onComplete) {
            baseService.searchObject(servicebase + 'getList', model, function (result) {
                if (undefined != onComplete) onComplete.call(this, result);
            });
        },
        getListRefund: function (model, onComplete) {
            baseService.searchObject(servicebase + 'getListRefund', model, function (result) {
                if (undefined != onComplete) onComplete.call(this, result);
            });
        }
    };
}]);