'use strict';
myApp.factory('eventmemberApiService', ['$http', '$resource', 'baseService', function ($http, $resource, baseService) {
    var servicebase = get_base_url("EventMember/");
    return {
          
        //EventMember 
	    listEventMember: function (model, onComplete) {
            baseService.searchObject(servicebase + 'getEventMemberModel', model, function (result) {
                if (undefined != onComplete) onComplete.call(this, result);
            });
        },
		getEventMember: function (model, onComplete) {
            baseService.postObject(servicebase + 'getEventMemberModel', model, function (result) {
                if (undefined != onComplete) onComplete.call(this, result);
            });
        },
        saveEventMember: function (model, onComplete) {
            baseService.postObject(servicebase + 'addEventMember', model, function (result) {
                if (undefined != onComplete) onComplete.call(this, result);
            });
        },
        deleteEventMember: function (model, onComplete) {
            baseService.postObject(servicebase + 'deleteEventMember', model, function (result) {
                if (undefined != onComplete) onComplete.call(this, result);
            });
        },
		getComboBox: function (model, onComplete) {
            baseService.searchObject(servicebase + 'getEventMemberComboList', model, function (result) {
                if (undefined != onComplete) onComplete.call(this, result);
            });
        },
        getComboBoxVat: function (model, onComplete) {
            baseService.searchObject(servicebase + 'getEventMemberComboListVat', model, function (result) {
                if (undefined != onComplete) onComplete.call(this, result);
            });
        },
        calDuration: function (model, onComplete) {
            baseService.postObject(servicebase + 'calDuration', model, function (result) {
                if (undefined != onComplete) onComplete.call(this, result);
            });
        },
        
    };
}]);