'use strict';
myApp.factory('reportSaleApiService', ['$http', '$resource', 'baseService', function ($http, $resource, baseService) {
    var servicebase = get_base_url("Dashboard/");
    return {
          
        getHeader: function (model, onComplete) {
            baseService.postObject(servicebase + 'getHeader', model, function (result) {
                if (undefined != onComplete) onComplete.call(this, result);
            });
        },
        getGraph: function (model, onComplete) {
            baseService.searchObject(servicebase + 'getGraph', model, function (result) {
                if (undefined != onComplete) onComplete.call(this, result);
            });
        }
    };
}]);