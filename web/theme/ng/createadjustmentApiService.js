'use strict';
myApp.factory('createadjustmentApiService', ['$http', '$resource', 'baseService', function ($http, $resource, baseService) {
    var servicebase = get_base_url("Createadjustment/");
    return {
          
        //Business 
	    getList: function (model, onComplete) {
            baseService.searchObject(servicebase + 'getList', model, function (result) {
                if (undefined != onComplete) onComplete.call(this, result);
            });
        },
		getDetail: function (model, onComplete) {
            baseService.postObject(servicebase + 'getDetail', model, function (result) {
                if (undefined != onComplete) onComplete.call(this, result);
            });
        },
        save: function (model, onComplete) {
            baseService.postObject(servicebase + 'add', model, function (result) {
                if (undefined != onComplete) onComplete.call(this, result);
            });
        },
        delete: function (model, onComplete) {
            baseService.postObject(servicebase + 'delete', model, function (result) {
                if (undefined != onComplete) onComplete.call(this, result);
            });
        },
        undelete: function (model, onComplete) {
            baseService.postObject(servicebase + 'undelete', model, function (result) {
                if (undefined != onComplete) onComplete.call(this, result);
            });
        },
		getComboListType: function (model, onComplete) {
            baseService.searchObject(servicebase + 'getComboListType', model, function (result) {
                if (undefined != onComplete) onComplete.call(this, result);
            });
        }
    };
}]);