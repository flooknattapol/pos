'use strict';
myApp.service('baseService', ['$http', '$resource', function ($http, $resource) {

    var PageIndex = 1;
    var PageSize = 20;
    var SortColumn = '';
    var SortOrder = 'asc';
    var modelSearch;
    var totalPage = 0;
    var totalRecords = 0;
    var overlayCount = 0;

    var MyBase = this;

    this.showOverlay = function () {  
        overlayCount++;
        $("#overlay").show();  
    }
  
    this.hideOverlay = function () {
        overlayCount--;  

        if (overlayCount <= 0) { 
            overlayCount = 0;
            $("#overlay").hide();
        }

       
    }

    this.get = function (url, param, onComplete, nocache, isArray) {

        MyBase.showOverlay();

        if (undefined === nocache) nocache = false;
        if (undefined === isArray) isArray = true;
        if (nocache) {
            if (undefined === param || null === param) param = {};
            param.time = new Date().getTime();
        }
        var resource = $resource(url, param, { query: { method: "GET", isArray: isArray } });
        resource.query().$promise.then(
            function (data) {  
                MyBase.hideOverlay();
                if (undefined !==  onComplete) onComplete.call(this, data);
            },
            function (ex) {
                MyBase.hideOverlay();
                AlertJSON(ex);
                if (window.console) {
                    console.log(ex);
                }
            });
    }

    this.getWithOutLoading = function (url, param, onComplete, nocache, isArray) {

        //MyBase.showOverlay();

        if (undefined === nocache) nocache = false;
        if (undefined === isArray) isArray = true;
        if (nocache) {
            if (undefined === param || null === param) param = {};
            param.time = new Date().getTime();
        }
        var resource = $resource(url, param, { query: { method: "GET", isArray: isArray } });
        resource.query().$promise.then(
            function (data) {
                //MyBase.hideOverlay();
                if (undefined !== onComplete) onComplete.call(this, data);
            },
            function (ex) {
                //MyBase.hideOverlay();
                AlertJSON(ex);
                if (window.console) {
                    console.log(ex);
                }
            });
    }

    this.postObject = function (url, param, onComplete, nocache) { 
         
        MyBase.showOverlay();

        if (undefined === nocache) nocache = false;
        $http({
            method: 'POST',
            url: url + (nocache ? ((occurrences(url, "?", false) > 0 ? '&' : '?') + 'time=' + new Date().getTime()) : ''),
            data: param,
            headers: {
                'Content-type': 'application/json'
            }
        }).then(
            function (response) {
                MyBase.hideOverlay();
                var data = response.data;
                var status = response.status;
                var headers = response.headers;
                var config = response.config;
                var statusText = response.statusText;
                if (undefined !==  onComplete) onComplete.call(this, data);
            },
            function (response) {
                MyBase.hideOverlay();
                var data = response.data;
                var status = response.status;
                var headers = response.headers;
                var config = response.config;
                var statusText = response.statusText;
                //alert("postObject error " + status + " : " + data.Message);
                if (window.console) {
                    console.log(status);
                }
            });
    }
     
    this.postIntervalObject = function (url, param, onComplete, nocache) {

       // $("#overlay").show();

        if (undefined === nocache) nocache = false;
        $http({
            method: 'POST',
            url: url + (nocache ? ((occurrences(url, "?", false) > 0 ? '&' : '?') + 'time=' + new Date().getTime()) : ''),
            data: param,
            headers: {
                'Content-type': 'application/json'
            }
        }).then(
            function (response) {
                //$("#overlay").hide();
                var data = response.data;
                var status = response.status;
                var headers = response.headers;
                var config = response.config;
                var statusText = response.statusText;
                if (undefined !== onComplete) onComplete.call(this, data);
            },
            function (response) {
                //$("#overlay").hide();
                var data = response.data;
                var status = response.status;
                var headers = response.headers;
                var config = response.config;
                var statusText = response.statusText;
                //alert("postObject error " + status + " : " + data.Message);
                if (window.console) {
                    console.log(status);
                }
            });
    }

    this.setSearchModel = function (mSearch) {
        modelSearch = mSearch;
    }

    this.setSortColumn = function (sortColumn) {
        SortColumn = sortColumn;
        return SortColumn;
    }

    this.setSortOrder = function (sortOrder) {
        SortOrder = sortOrder;
        return SortOrder;
    }

    this.getSortColumn = function () {
        return SortColumn;
    }

    this.getSortOrder = function () {
        return SortOrder;
    }

    this.getFirstPage = function () {
        if (parseInt(PageIndex, 10) !==  1) {
            PageIndex = 1;
        }
        return PageIndex;
    }

    this.getNextPage = function () {
        console.log("PageIndex", PageIndex, parseInt(totalPage, 10), parseInt(PageIndex, 10));
        if (parseInt(PageIndex, 10) < parseInt(totalPage, 10)) {
            PageIndex += 1; 
        }
        return PageIndex;
    }

    this.getBackPage = function () {
        if (parseInt(PageIndex, 10) > 1) {
            PageIndex -= 1;
        }
        return PageIndex;
    }

    this.getLastPage = function () {
        if (parseInt(PageIndex, 10) <= parseInt(totalPage, 10)) {
            PageIndex = totalPage;
        }
        return PageIndex;
    }

    this.searchByPage = function () {
        try {
            if ($.isNumeric(PageIndex) === false) {
                PageIndex = 1;
            } else if (PageIndex < 1) {
                PageIndex = 1;
                getFirstPage();
            } else if (PageIndex > totalPage) {
                PageIndex = totalPage;
            } else {
                //loadExportPrefix();
            }
        } catch (e) {
            console.log(e);
            return false;
        }
        return PageIndex;
    }

    this.setPageSize = function (size) {
        PageSize = size;
        return PageSize;
    }

    this.setPageIndex = function (page) {
        PageIndex = page;
        return PageIndex;
    }

    this.setSortIcon = function () {

        $(".icon").remove();
        $(".sorting").append('<i class="icon fa fa-sort"></i>');
        $(".sorting_asc").append('<i class="icon fa fa-sort-asc"></i>');
        $(".sorting_desc").append('<i class="icon fa fa-sort-desc"></i>');

    }

    this.showError = function (msg) {
        $('#overlay').hide(); //$('#myWaitModal').modal('hide');
        $('#error_message').html(msg);
        $('#myErrorModal').modal('show');
    }

    this.showMessage = function (msg) {
        $('#overlay').hide(); //$('#myWaitModal').modal('hide');
        $('#show_message').html(msg);
        $('#myMessageModal').modal('show');
    }

    this.showReloadMessage = function (msg) {
        $('#overlay').hide(); //$('#myWaitModal').modal('hide');
        $('#show_reload_message').html(msg);
        $('#myRelaodMessageModal').modal('show');
    }

    this.sort = function (event) {
        var nextSortColumn = $(event.target).attr('sort'); 
        $(".sorting_asc").removeClass("sorting_asc").addClass("sorting");
        $(".sorting_desc").removeClass("sorting_desc").addClass("sorting");

        if (SortColumn === nextSortColumn) {

            if (SortOrder === "desc") {
                SortOrder = "asc";
                $(event.target).removeClass("sorting").addClass("sorting_asc");
            } else {
                SortOrder = "desc";
                $(event.target).removeClass("sorting").addClass("sorting_desc");
            }

        } else {
            SortColumn = nextSortColumn;
            SortOrder = "desc";
            $(event.target).removeClass("sorting").addClass("sorting_desc");
        }

        this.setSortIcon();
    }

    this.searchObject = function (url, mSearch, callback) {
        var jsonData = {
            "PageIndex": PageIndex,
            "PageSize": PageSize,
            "SortColumn": SortColumn,
            "SortOrder": SortOrder,
            "mSearch": mSearch
        }; 

        MyBase.showOverlay(); //$('#myWaitModal').modal('show');
		MyBase.setSortIcon();
        $http({
            url: url,
            method: "POST", 
            data: jsonData, 
            headers: { 'Content-Type': 'application/json;charset=UTF-8' }
        }).then(function (response) {
            MyBase.hideOverlay();
            totalPage = response.data.toTalPage; //init;
            totalRecords = response.data.totalRecords;
			 
            //callback(response.data.message);
            callback(response);
            //if ('1' == response.data.result) {
            //    totalPage = response.data.toTalPage; //init;
            //    totalRecords = response.data.totalRecords;
            //    callback(response);
            //} else {
            //    this.showError(response.data.message);
            //} 
        }, function (response) {
            MyBase.hideOverlay(); //$('#myWaitModal').modal('hide');
            //this.showError(response.data.message);
        });
    }

    this.searchObjectNonOverlay = function (url, mSearch, callback) {
        var jsonData = {
            "PageIndex": PageIndex,
            "PageSize": PageSize,
            "SortColumn": SortColumn,
            "SortOrder": SortOrder,
            "mSearch": mSearch
        }; 

        //MyBase.showOverlay(); //$('#myWaitModal').modal('show');
		MyBase.setSortIcon();
        $http({
            url: url,
            method: "POST", 
            data: jsonData, 
            headers: { 'Content-Type': 'application/json;charset=UTF-8' }
        }).then(function (response) {
            //MyBase.hideOverlay();
            totalPage = response.data.toTalPage; //init;
            totalRecords = response.data.totalRecords;
			 
            //callback(response.data.message);
            callback(response);
            //if ('1' == response.data.result) {
            //    totalPage = response.data.toTalPage; //init;
            //    totalRecords = response.data.totalRecords;
            //    callback(response);
            //} else {
            //    this.showError(response.data.message);
            //} 
        }, function (response) {
            //MyBase.hideOverlay(); //$('#myWaitModal').modal('hide');
            //this.showError(response.data.message);
        });
    }

    this.searchObjectWithoutLoading = function (url, mSearch, callback) {
        var jsonData = {
            "PageIndex": PageIndex,
            "PageSize": PageSize,
            "SortColumn": SortColumn,
            "SortOrder": SortOrder,
            "mSearch": mSearch
        };

        //MyBase.showOverlay(); //$('#myWaitModal').modal('show');

        $http({
            url: url,
            method: "POST",
            data: jsonData,
            headers: { 'Content-Type': 'application/json;charset=UTF-8' }
        }).then(function (response) {
            //MyBase.hideOverlay();
            totalPage = response.data.toTalPage; //init;
            totalRecords = response.data.totalRecords;
			
			
            //callback(response.data.message);
            callback(response);
            //if ('1' == response.data.result) {
            //    totalPage = response.data.toTalPage; //init;
            //    totalRecords = response.data.totalRecords;
            //    callback(response);
            //} else {
            //    this.showError(response.data.message);
            //} 
        }, function (response) {
            //MyBase.hideOverlay(); //$('#myWaitModal').modal('hide');
            //this.showError(response.data.message);
        });
    }

    this.searchAllObject = function (url, mSearch, callback) {
        var jsonData = {
            "PageIndex": 1,
            "PageSize": -1,
            "SortColumn": "",
            "SortOrder": "",
            "mSearch": mSearch
        };

        MyBase.showOverlay(); 

        $http({
            url: url,
            method: "POST",
            data: jsonData,
            headers: { 'Content-Type': 'application/json;charset=UTF-8' }
        }).then(function (response) {
            MyBase.hideOverlay();
            totalPage = response.data.toTalPage; //init;
            totalRecords = response.data.totalRecords; 
            callback(response);  
        }, function (response) {
            MyBase.hideOverlay();
        });
    }

    this.getListPageSize = function (){ 
        var lstData = [{ "Selected": true, "Text": "20 items", "Value": "20" }, { "Selected": false, "Text": "30  items", "Value": "30" }, { "Selected": false, "Text": "50 items", "Value": "50" }, { "Selected": false, "Text": "100 items", "Value": "100" }];
        return lstData;
    }
	
	this.getListPage = function (max){ 
        var lstData = [];
		
		for(var i = 0; i < max; i++){
			var tmpData = {"Text": (i+1), "Value": (i+1), "PageIndex" : (i+1)};
			
			lstData.push(tmpData);
		}
		
        return lstData;
    }

    this.SelectAllCheck = function (listData, selected) {
        listData.forEach(function (entry) {
            entry.ITEM_SELECTED = selected;
        });
        return true;
    }

    this.SelectAllCheckEx = function (listData, selected) {
        listData.forEach(function (entry) {
            if (entry.ITEM_DISABLE == false) {
                entry.ITEM_SELECTED = selected;
            }
        });
        return true;
    }
 
    //Search Model  
    this.getCustomerSearch = function () { 
        return { "customerId": "" };
    }
     
}]);
function ThaiNumberToText(Number)
{
	Number = Number.replace (/๐/gi,'0');  
	Number = Number.replace (/๑/gi,'1');  
	Number = Number.replace (/๒/gi,'2');
	Number = Number.replace (/๓/gi,'3');
	Number = Number.replace (/๔/gi,'4');
	Number = Number.replace (/๕/gi,'5');
	Number = Number.replace (/๖/gi,'6');
	Number = Number.replace (/๗/gi,'7');
	Number = Number.replace (/๘/gi,'8');
	Number = Number.replace (/๙/gi,'9');
	return 	ArabicNumberToText(Number);
}

function ArabicNumberToText(Number)
{
	var Number = CheckNumber(Number);
	var NumberArray = new Array ("ศูนย์", "หนึ่ง", "สอง", "สาม", "สี่", "ห้า", "หก", "เจ็ด", "แปด", "เก้า", "สิบ");
	var DigitArray = new Array ("", "สิบ", "ร้อย", "พัน", "หมื่น", "แสน", "ล้าน");
	var BahtText = "";
	if (isNaN(Number))
	{
		return "ข้อมูลนำเข้าไม่ถูกต้อง";
	} else
	{
		if ((Number - 0) > 9999999.9999)
		{
			return "ข้อมูลนำเข้าเกินขอบเขตที่ตั้งไว้";
		} else
		{
			Number = Number.split (".");
			if (Number[1].length > 0)
			{
				Number[1] = Number[1].substring(0, 2);
			}
			var NumberLen = Number[0].length - 0;
			for(var i = 0; i < NumberLen; i++)
			{
				var tmp = Number[0].substring(i, i + 1) - 0;
				if (tmp != 0)
				{
					if ((i == (NumberLen - 1)) && (tmp == 1))
					{
						BahtText += "เอ็ด";
					} else
					if ((i == (NumberLen - 2)) && (tmp == 2))
					{
						BahtText += "ยี่";
					} else
					if ((i == (NumberLen - 2)) && (tmp == 1))
					{
						BahtText += "";
					} else
					{
						BahtText += NumberArray[tmp];
					}
					BahtText += DigitArray[NumberLen - i - 1];
				}
			}
			BahtText += "บาท";
			if ((Number[1] == "0") || (Number[1] == "00"))
			{
				BahtText += "ถ้วน";
			} else
			{ 
				var DecimalLen = Number[1].length - 0;
				for (var i = 0; i < DecimalLen; i++)
				{
					var tmp = Number[1].substring(i, i + 1) - 0;
					if (tmp != 0)
					{
						if ((i == (DecimalLen - 1)) && (tmp == 1))
						{
							BahtText += "เอ็ด";
						} else
						if ((i == (DecimalLen - 2)) && (tmp == 2))
						{
							BahtText += "ยี่";
						} else
						if ((i == (DecimalLen - 2)) && (tmp == 1))
						{
							BahtText += "";
						} else
						{
							BahtText += NumberArray[tmp];
						}
						BahtText += DigitArray[DecimalLen - i - 1];
					}
				}
				BahtText += "สตางค์";
			}
			return BahtText;
		}
	}
}

function CheckNumber(Number){
	var decimal = false; 
	Number = Number.toString();						
	Number = Number.replace (/ |,|บาท|฿/gi,'');  		
	for (var i = 0; i < Number.length; i++)
	{
		if(Number[i] =='.'){
			decimal = true;
		}
	}
	if(decimal == false){
		Number = Number+'.00';
	}
	return Number
}