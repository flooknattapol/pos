'use strict';
myApp.factory('eventApiService', ['$http', '$resource', 'baseService', function ($http, $resource, baseService) {
    var servicebase = get_base_url("Event/");
    return {
          
        //Event 
	    listEvent: function (model, onComplete) {
            baseService.searchObject(servicebase + 'getEventModel', model, function (result) {
                if (undefined != onComplete) onComplete.call(this, result);
            });
        },
		getEvent: function (model, onComplete) {
            baseService.postObject(servicebase + 'getEventModel', model, function (result) {
                if (undefined != onComplete) onComplete.call(this, result);
            });
        },
        saveEvent: function (model, onComplete) {
            baseService.postObject(servicebase + 'addEvent', model, function (result) {
                if (undefined != onComplete) onComplete.call(this, result);
            });
        },
        deleteEvent: function (model, onComplete) {
            baseService.postObject(servicebase + 'deleteEvent', model, function (result) {
                if (undefined != onComplete) onComplete.call(this, result);
            });
        },
		getComboBox: function (model, onComplete) {
            baseService.searchObject(servicebase + 'getEventComboList', model, function (result) {
                if (undefined != onComplete) onComplete.call(this, result);
            });
        },
        getComboBoxVat: function (model, onComplete) {
            baseService.searchObject(servicebase + 'getEventComboListVat', model, function (result) {
                if (undefined != onComplete) onComplete.call(this, result);
            });
        },
        calDuration: function (model, onComplete) {
            baseService.postObject(servicebase + 'calDuration', model, function (result) {
                if (undefined != onComplete) onComplete.call(this, result);
            });
        },
        
    };
}]);