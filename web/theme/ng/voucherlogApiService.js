'use strict';
myApp.factory('voucherlogApiService', ['$http', '$resource', 'baseService', function ($http, $resource, baseService) {
    var servicebase = get_base_url("VoucherLog/");
    return {
          
        //VoucherLog 
	    listVoucherLog: function (model, onComplete) {
            baseService.searchObject(servicebase + 'getVoucherLogModel', model, function (result) {
                if (undefined != onComplete) onComplete.call(this, result);
            });
        },
		getVoucherLog: function (model, onComplete) {
            baseService.postObject(servicebase + 'getVoucherLogModel', model, function (result) {
                if (undefined != onComplete) onComplete.call(this, result);
            });
        },
        saveVoucherLog: function (model, onComplete) {
            baseService.postObject(servicebase + 'addVoucherLog', model, function (result) {
                if (undefined != onComplete) onComplete.call(this, result);
            });
        },
        deleteVoucherLog: function (model, onComplete) {
            baseService.postObject(servicebase + 'deleteVoucherLog', model, function (result) {
                if (undefined != onComplete) onComplete.call(this, result);
            });
        },
		getComboBox: function (model, onComplete) {
            baseService.searchObject(servicebase + 'getVoucherLogComboList', model, function (result) {
                if (undefined != onComplete) onComplete.call(this, result);
            });
        },
        getComboBoxVat: function (model, onComplete) {
            baseService.searchObject(servicebase + 'getVoucherLogComboListVat', model, function (result) {
                if (undefined != onComplete) onComplete.call(this, result);
            });
        },
        calDuration: function (model, onComplete) {
            baseService.postObject(servicebase + 'calDuration', model, function (result) {
                if (undefined != onComplete) onComplete.call(this, result);
            });
        },
        
    };
}]);