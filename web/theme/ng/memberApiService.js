'use strict';
myApp.factory('memberApiService', ['$http', '$resource', 'baseService', function ($http, $resource, baseService) {
    var servicebase = get_base_url("Member/");
    return {
          
        //Member 
	    listMember: function (model, onComplete) {
            baseService.searchObject(servicebase + 'getMemberModel', model, function (result) {
                if (undefined != onComplete) onComplete.call(this, result);
            });
        },
		getMember: function (model, onComplete) {
            baseService.postObject(servicebase + 'getMemberModel', model, function (result) {
                if (undefined != onComplete) onComplete.call(this, result);
            });
        },
        saveMember: function (model, onComplete) {
            baseService.postObject(servicebase + 'addMember', model, function (result) {
                if (undefined != onComplete) onComplete.call(this, result);
            });
        },
        deleteMember: function (model, onComplete) {
            baseService.postObject(servicebase + 'deleteMember', model, function (result) {
                if (undefined != onComplete) onComplete.call(this, result);
            });
        },
		getComboBox: function (model, onComplete) {
            baseService.searchObject(servicebase + 'getMemberComboList', model, function (result) {
                if (undefined != onComplete) onComplete.call(this, result);
            });
        },
        getComboBoxVat: function (model, onComplete) {
            baseService.searchObject(servicebase + 'getMemberComboListVat', model, function (result) {
                if (undefined != onComplete) onComplete.call(this, result);
            });
        },
        calDuration: function (model, onComplete) {
            baseService.postObject(servicebase + 'calDuration', model, function (result) {
                if (undefined != onComplete) onComplete.call(this, result);
            });
        },
        listMemberVoucherDetail: function (model, onComplete) {
            baseService.postObject(servicebase + 'getMemberVoucherDetail', model, function (result) {
                if (undefined != onComplete) onComplete.call(this, result);
            });
        },
    };
}]);