'use strict';
myApp.factory('relationstampApiService', ['$http', '$resource', 'baseService', function ($http, $resource, baseService) {
    var servicebase = get_base_url("RelationStamp/");
    return {
          
        //RelationStamp 
	    listRelationStamp: function (model, onComplete) {
            baseService.searchObject(servicebase + 'getRelationStampModel', model, function (result) {
                if (undefined != onComplete) onComplete.call(this, result);
            });
        },
		getRelationStamp: function (model, onComplete) {
            baseService.postObject(servicebase + 'getRelationStampModel', model, function (result) {
                if (undefined != onComplete) onComplete.call(this, result);
            });
        },
        saveRelationStamp: function (model, onComplete) {
            baseService.postObject(servicebase + 'addRelationStamp', model, function (result) {
                if (undefined != onComplete) onComplete.call(this, result);
            });
        },
        deleteRelationStamp: function (model, onComplete) {
            baseService.postObject(servicebase + 'deleteRelationStamp', model, function (result) {
                if (undefined != onComplete) onComplete.call(this, result);
            });
        },
		getComboBox: function (model, onComplete) {
            baseService.searchObject(servicebase + 'getRelationStampComboList', model, function (result) {
                if (undefined != onComplete) onComplete.call(this, result);
            });
        },
        getComboBoxVat: function (model, onComplete) {
            baseService.searchObject(servicebase + 'getRelationStampComboListVat', model, function (result) {
                if (undefined != onComplete) onComplete.call(this, result);
            });
        },
        calDuration: function (model, onComplete) {
            baseService.postObject(servicebase + 'calDuration', model, function (result) {
                if (undefined != onComplete) onComplete.call(this, result);
            });
        },
        
    };
}]);