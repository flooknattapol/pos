'use strict';

var myApp = angular.module('myApp', ['mgcrea.ngStrap', 'jsonFormatter', 'ngResource', 'ui.bootstrap', 'ngSanitize', 'ui.select', 'ngFileUpload','ngSanitize' ]);

myApp.config(['$locationProvider', function ($locationProvider) {

}]);



/*myApp.service('fileUpload', ['$http', function ($http) {
    this.uploadFileToUrl = function (file, uploadUrl) {
        $('#overlay').show();//$('#myWaitModal').modal('show');
        var fd = new FormData();
        fd.append('file', file); //file เป็นชื่อ parameter ที่ส่งขึ้นไป 

        $http.post(uploadUrl, fd, {
            transformRequest: angular.identity,
            headers: { 'Content-Type': undefined }
        })
            .success(function (response) {
                if (true == response.status) {
                    location.reload();
                } else {
                    $('#error_message').html(response.message);
                    $('#overlay').hide();//$('#myWaitModal').modal('hide');
                    $('#myErrorModal').modal('show');
                }
            })
            .error(function (response) {
                console.log(response);
            });
    }
}]);*/


/********** Directive *************/

myApp.filter('myShowCostFormat', function() {
    return function(nStr) {
        try{ 
			nStr += '';
			var x = nStr.split('.');
			var x1 = x[0];
			var x2 = x.length > 1 ? '.' + x[1] : '.00';
			var rgx = /(\d+)(\d{3})/;
			while (rgx.test(x1)) {
				x1 = x1.replace(rgx, '$1' + ',' + '$2');
			}
			return x1 + x2;
		}catch(e){
			console.log(e);
			return "";
		}
    };
});

myApp.filter('myShowDateFormat', function() {
    return function(item) {
		try{
			var today = item;//new Date();
			var dd = today.getDate();
			var mm = today.getMonth()+1; //January is 0!

			var yyyy = today.getFullYear();
			if(dd<10){
				dd='0'+dd;
			} 
			if(mm<10){
				mm='0'+mm;
			} 
			var today = dd+'/'+mm+'/'+yyyy; 
			if(isNaN(dd)){
				return "" ;
			} 	
			
			return today;
		}catch(e){
			return "";
		}
    };
});

myApp.directive('convertToNumber', function () {
    return {
        require: 'ngModel',
        link: function (scope, element, attrs, ngModel) {
            ngModel.$parsers.push(function (val) {
                return val != null ? parseInt(val, 10) : null;
            });
            ngModel.$formatters.push(function (val) {
                return val != null ? '' + val : null;
            });
        }
    };
});

myApp.directive('convertToString', function () {
    return {
        require: 'ngModel',
        link: function (scope, element, attrs, ngModel) {
            ngModel.$parsers.push(function (val) {
                return val != null ? val+"" : null;
            });
            ngModel.$formatters.push(function (val) {
                return val != null ? '' + val : null;
            });
        }
    };
});

myApp.directive("myConfirmClick", [
    function () {
        return {
            priority: -1,
            restrict: 'A',
            scope: { confirmFunction: "&myConfirmClick" },
            link: function (scope, element, attrs) {
                element.bind('click', function (e) {
                    // message defaults to "Are you sure?"
                    console.log(attrs);
                    var message = attrs.myConfirmClickMessage ? attrs.myConfirmClickMessage : "Are you sure?";
                    document.getElementById("MessageValue").innerHTML = message;
                    $("#dialog-confirm").modal({                    // wire up the actual modal functionality and show the dialog
                        "backdrop": "static",
                        "keyboard": true,
                        "show": true                     // ensure the modal is shown immediately
                    }).one('click', '#btnYes', function () {
                        //console.log("button pressed");   // just as an example...
                        scope.confirmFunction();
                        $("#dialog-confirm").modal('hide');
                    });

                });
            }
        }
    }
]);

myApp.directive('numberOnlyInput', function () {
    return {
        require: 'ngModel',
        link: function (scope, element, attrs, modelCtrl) {

            modelCtrl.$parsers.push(function (inputValue) {
                var transformedInput = inputValue ? inputValue.replace(/[^\d.-]/g, '') : null;
                //var transformedInput = inputValue ? inputValue.replace("^\d+(\.\d+)+$",'') : null;

                if (transformedInput != inputValue) {
                    modelCtrl.$setViewValue(transformedInput);
                    modelCtrl.$render();
                }

                return transformedInput;
            });
        }
    };
});
 
myApp.directive("uiWizardForm", function () {
    var scope;

    return {
        restrict: "A",
        controller: function ($scope) {
            scope = $scope;
        },
        compile: function ($element) { 
            console.log($element);
            var steps = $element.children("div").steps({
                headerTag: "h3",
                bodyTag: "section",
                transitionEffect: "slideLeft",
                onStepChanging: function (event, currentIndex, newIndex) {
                    return scope.onStepChanging(event, currentIndex, newIndex);
                },
                onFinishing: function (event, currentIndex) {
                    return scope.onFinishing(event, currentIndex);
                },
                onFinished: function (event, currentIndex) {
                    scope.onFinished(event, currentIndex);
                } 
            });
        }
    };
});

myApp.directive('datepicker', [function () {
    return {
        restrict: 'E',
        replace: true,
        template: '<div class="input-group date">' +
        '<input type="text" class="form-control">' +
        '<span class="input-group-addon"><i class="fa fa-calendar"></i></span>' +
        '</div>',
        scope: {
            ngModel: '='
        },
        link: function (scope, element) {
            element.datepicker({
                format: 'yyyy-mm-dd',
                autoclose: true,
                todayHighlight: true
            });

            var input = element.find('input');

            input.on('change', function () {
                scope.$apply(function () {
                    scope.ngModel = input.val();
                })
            });

            element.datepicker('update', new Date());

            scope.ngModel = input.val();
        }
    }
}]);