﻿
function AlertJSON(data) {
    //alert(JSON.stringify(data, null, "    "));
    console.log(JSON.stringify(data, null, "    "));
}

serialize = function (obj) {
    var str = [];
    for (var p in obj)
        if (obj.hasOwnProperty(p)) {
            str.push(encodeURIComponent(p) + "=" + encodeURIComponent(obj[p]));
        }
    return str.join("&");
}

function loadResultTo(servicefunction, $scope, target, param) {
    //console.log(param);
    if (undefined !== param && null !== param)
    { 
        servicefunction(param,function (data) {
            $scope[target] = data; 
        });
    }
    else {
        servicefunction(function (data) {
            $scope[target] = data; 
        });
    }
}

function loadResultToWithCallback(servicefunction, $scope, target, param, callback) {
    //console.log(param);
  
    if (undefined !== param )
    {
        //console.log(1);
        servicefunction(param,function (data) {
            $scope[target] = data; 
             callback(data);
        });
    }
    else {
        //console.log(2);
        servicefunction(function (data) {
            $scope[target] = data; 
             callback(data);
        });
    }
}

function searchResultToWithCallback(servicefunction, $scope, target, param, callback) {
    

    if (undefined !== param && null !== param)
    { 
        servicefunction(param, function (response) {
            $scope[target] = response.data.message;
            if(null !== callback)
                callback(response);
        });
    }
    else {
        servicefunction(function (response) {
            $scope[target] = response.data.message;
            if(null !== callback)
                callback(response);
        });
    }
}

function loadCallback(servicefunction, $scope, param, callback) {
    //console.log(param);

    if (undefined !== param) {
        //console.log(1);
        servicefunction(param, function (data) { 
            callback(data);
        });
    }
    else {
        //console.log(2);
        servicefunction(function (data) { 
            callback(data);
        });
    }
}

function zeroPad(num, places) {
    var zero = places - num.toString().length + 1;
    return Array(+(zero > 0 && zero)).join("0") + num;
}

String.prototype.insert = function (index, string) {
    if (index > 0)
        return this.substring(0, index) + string + this.substring(index, this.length);
    else
        return string + this;
};


var logger = function()
{
    var oldConsoleLog = null;
    var pub = {};

    pub.enableLogger =  function enableLogger() 
                        {
                            if(oldConsoleLog == null)
                                return;

                            window['console']['log'] = oldConsoleLog;
                        };

    pub.disableLogger = function disableLogger()
                        {
                            oldConsoleLog = console.log;
                            window['console']['log'] = function() {};
                        };

    return pub;
}();

// console.log('test before');
// logger.disableLogger();
// console.log('test after'); 
