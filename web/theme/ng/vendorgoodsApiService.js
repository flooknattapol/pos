'use strict';
myApp.factory('vendorgoodsApiService', ['$http', '$resource', 'baseService', function ($http, $resource, baseService) {
    var servicebase = get_base_url("VendorGoods/");
    return {
          
        //VendorGoods 
	    listVendorGoods: function (model, onComplete) {
            baseService.searchObject(servicebase + 'getVendorGoodsModel', model, function (result) {
                if (undefined != onComplete) onComplete.call(this, result);
            });
        },
		getVendorGoods: function (model, onComplete) {
            baseService.postObject(servicebase + 'getVendorGoodsModel', model, function (result) {
                if (undefined != onComplete) onComplete.call(this, result);
            });
        },
        saveVendorGoods: function (model, onComplete) {
            baseService.postObject(servicebase + 'addVendorGoods', model, function (result) {
                if (undefined != onComplete) onComplete.call(this, result);
            });
        },
        deleteVendorGoods: function (model, onComplete) {
            baseService.postObject(servicebase + 'deleteVendorGoods', model, function (result) {
                if (undefined != onComplete) onComplete.call(this, result);
            });
        },
		getComboBox: function (model, onComplete) {
            baseService.searchObject(servicebase + 'getVendorGoodsComboList', model, function (result) {
                if (undefined != onComplete) onComplete.call(this, result);
            });
        },
        getComboBoxVat: function (model, onComplete) {
            baseService.searchObject(servicebase + 'getVendorGoodsComboListVat', model, function (result) {
                if (undefined != onComplete) onComplete.call(this, result);
            });
        },
        calDuration: function (model, onComplete) {
            baseService.postObject(servicebase + 'calDuration', model, function (result) {
                if (undefined != onComplete) onComplete.call(this, result);
            });
        },
        
    };
}]);