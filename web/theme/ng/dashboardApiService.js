'use strict';
myApp.factory('dashboardApiService', ['$http', '$resource', 'baseService', function ($http, $resource, baseService) {
    var servicebase = get_base_url("Dashboard/");
    return {
		getList: function (model, onComplete) {
            baseService.postObject(servicebase + 'getHeader', model, function (result) {
                if (undefined != onComplete) onComplete.call(this, result);
            });
        }
    };
}]);
