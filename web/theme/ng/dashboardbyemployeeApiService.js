'use strict';
myApp.factory('dashboardbyemployeeApiService', ['$http', '$resource', 'baseService', function ($http, $resource, baseService) {
    var servicebase = get_base_url("DashboardByEmployee/");
    return {
          
        //dashboardbyemployee 
	    listdashboardbyemployee: function (model, onComplete) {
            baseService.searchObject(servicebase + 'getdashboardbyemployeeModel', model, function (result) {
                if (undefined != onComplete) onComplete.call(this, result);
            });
        },
		getdashboardbyemployeeAlll: function (model, onComplete) {
            baseService.postObject(servicebase + 'getdashboardbyemployeeAlll', model, function (result) {
                if (undefined != onComplete) onComplete.call(this, result);
            });
        }
    };
}]);